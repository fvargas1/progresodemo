SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCNBV_Comercial](
	@numInstitucion VARCHAR(10),
	@fechaOtorgamiento DATETIME,
	@rfc VARCHAR(25),
	@ultimoConsecutivo INT
)
RETURNS VARCHAR(50)
AS
BEGIN

SET @numInstitucion = LTRIM(RTRIM(@numInstitucion));
SET @rfc = LTRIM(RTRIM(@rfc));

DECLARE @cnbv VARCHAR(50);
DECLARE @fechaFormat VARCHAR(6);

SET @fechaFormat = CAST( YEAR( @fechaOtorgamiento ) AS VARCHAR( 4 ) ) + RIGHT( '0' + CAST( MONTH( @fechaOtorgamiento ) AS VARCHAR(2) ), 2 );
SET @ultimoConsecutivo = RIGHT(@ultimoConsecutivo,3);

DECLARE @num_compuesto VARCHAR(5);
SET @num_compuesto = SUBSTRING('000000',0,17-LEN(@rfc)-LEN(@ultimoConsecutivo));

SET @cnbv = '2' +
	@numInstitucion +
	LEFT(CONVERT(VARCHAR, @fechaFormat, 112),6) +
	@rfc +
	@num_compuesto +
	LTRIM(CONVERT(varchar(50),@ultimoConsecutivo));

RETURN (@cnbv);
END;
GO
