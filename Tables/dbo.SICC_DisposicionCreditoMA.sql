CREATE TABLE [dbo].[SICC_DisposicionCreditoMA]
(
[IdDisposicion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_DisposicionCreditoMA] ADD CONSTRAINT [PK_SICC_DisposicionCreditoMA] PRIMARY KEY CLUSTERED  ([IdDisposicion]) ON [PRIMARY]
GO
