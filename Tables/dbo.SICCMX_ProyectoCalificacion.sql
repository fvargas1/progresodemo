CREATE TABLE [dbo].[SICCMX_ProyectoCalificacion]
(
[IdProyecto] [bigint] NOT NULL,
[Fecha] [datetime] NOT NULL,
[SobreCosto] [decimal] (23, 2) NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[Meses] [int] NULL,
[MesesAdicionales] [int] NULL,
[VPTotal] [decimal] (23, 2) NULL,
[UpAcumulada] [decimal] (23, 2) NULL,
[IdEtapa] [int] NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[SobrecostoGarantia] [decimal] (23, 2) NULL,
[ReservaConstruccion] [decimal] (23, 2) NULL,
[ReservaOperacion] [decimal] (23, 2) NULL,
[ExposicionSobrecosto] [decimal] (23, 2) NULL,
[PorcenajeSobrecosto] [decimal] (18, 10) NULL,
[IdCalificacionSobrecosto] [int] NULL,
[PorcentajeCorrida] [decimal] (18, 10) NULL,
[IdCalificacionCorrida] [int] NULL,
[PorcentajeFinal] [decimal] (18, 10) NULL,
[IdCalificacionFinal] [int] NULL,
[SaldoInsoluto] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
