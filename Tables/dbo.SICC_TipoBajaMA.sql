CREATE TABLE [dbo].[SICC_TipoBajaMA]
(
[IdTipoBaja] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoBajaMA] ADD CONSTRAINT [PK_SICC_TipoBajaMA] PRIMARY KEY CLUSTERED  ([IdTipoBaja]) ON [PRIMARY]
GO
