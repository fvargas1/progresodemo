CREATE TABLE [Historico].[SICCMX_CreditoGarantia_Portafolio]
(
[IdPeriodoHistorico] [int] NULL,
[CodigoPortafolio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
