CREATE TABLE [dbo].[RW_Auditoria_Avales_BANCREA]
(
[IdReporteLog] [bigint] NOT NULL,
[ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID_Linea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[S_Total] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDevNC_B] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Estatus_B6] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDC_C] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[N_A_C] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ING_C] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_C] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDC_G] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[N_A_G] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OS_AG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ING_G] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP_PC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_G] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TA_GTE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
