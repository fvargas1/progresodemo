CREATE TABLE [dbo].[SICC_DestinoHipotecario]
(
[IdDestinoHipotecario] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_DestinoHipotecario] ADD CONSTRAINT [PK_SICC_DestinoHipotecario] PRIMARY KEY CLUSTERED  ([IdDestinoHipotecario]) ON [PRIMARY]
GO
