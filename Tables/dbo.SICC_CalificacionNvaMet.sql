CREATE TABLE [dbo].[SICC_CalificacionNvaMet]
(
[IdCalificacion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RangoMenor] [decimal] (18, 6) NULL,
[RangoMayor] [decimal] (18, 6) NULL,
[IdClasificacionMet] [int] NOT NULL,
[Color] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_CalificacionNvaMet] ADD CONSTRAINT [PK_SICC_CalificacionNvaMet] PRIMARY KEY CLUSTERED  ([IdCalificacion]) ON [PRIMARY]
GO
