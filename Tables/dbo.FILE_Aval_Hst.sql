CREATE TABLE [dbo].[FILE_Aval_Hst]
(
[CodigoAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreAval] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domicilio] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonalidadJuridica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MunAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_Fitch] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_Moodys] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_HRRATINGS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_Otras] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCobertura] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FiguraGarantiza] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCliente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Localidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Estado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPostal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
