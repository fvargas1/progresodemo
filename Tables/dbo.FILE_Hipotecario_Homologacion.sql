CREATE TABLE [dbo].[FILE_Hipotecario_Homologacion]
(
[Campo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanco] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoBW] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[FechaAlta] [datetime] NOT NULL CONSTRAINT [DF__FILE_Hipo__Fecha__6934171D] DEFAULT (getdate()),
[Tipo] [int] NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
