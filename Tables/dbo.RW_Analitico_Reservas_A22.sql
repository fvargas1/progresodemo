CREATE TABLE [dbo].[RW_Analitico_Reservas_A22]
(
[IdReporteLog] [bigint] NOT NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaVencimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prct_Reserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoReserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Aval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_Aval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarantiaAjustado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaAdicional] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
