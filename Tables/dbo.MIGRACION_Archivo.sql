CREATE TABLE [dbo].[MIGRACION_Archivo]
(
[IdArchivo] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Codename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobCodename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorCodename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogCodename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearLogCodename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [int] NULL,
[Activo] [bit] NULL,
[ViewName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValidationStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastValidationExecuted] [datetime] NULL,
[GrupoArchivo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InitCodename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumRegistros] [int] NULL,
[FechaInicio] [datetime] NULL,
[FechaFin] [datetime] NULL,
[ErroresMigracion] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Archivo] ADD CONSTRAINT [PK_MIGRACION_Archivo] PRIMARY KEY CLUSTERED  ([IdArchivo]) ON [PRIMARY]
GO
