CREATE TABLE [dbo].[SICCMX_Reservas_Variacion]
(
[IdCredito] [bigint] NOT NULL,
[ReservaCalConsDesc] [decimal] (23, 2) NULL,
[ReservaAdicConsDesc] [decimal] (23, 2) NULL,
[MesesSP100] [int] NULL,
[CreditoRes5Prct] [int] NULL,
[CreditoSustPI] [int] NULL,
[ReservaPeriodoAnt] [decimal] (23, 2) NULL,
[ReservaAdicPeriodoAnt] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
