CREATE TABLE [R04].[0419Datos_2016]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_SaldoHistorico] DEFAULT ((0)),
[ReservaHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_ReservaHistorico] DEFAULT ((0)),
[IntCarVencHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_IntCarVencHistorico] DEFAULT ((0)),
[ReservasAdicHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_ReservasAdicHistorico] DEFAULT ((0)),
[PrctReservasHistorico] [decimal] (18, 10) NOT NULL CONSTRAINT [DF_0419Datos_2016_PrctReservasHistorico] DEFAULT ((0)),
[SaldoActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_SaldoActual] DEFAULT ((0)),
[ReservaActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_ReservaActual] DEFAULT ((0)),
[IntCarVencActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_IntCarVencActual] DEFAULT ((0)),
[ReservasAdicActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_ReservasAdicActual] DEFAULT ((0)),
[PrctReservasActual] [decimal] (18, 10) NOT NULL CONSTRAINT [DF_0419Datos_2016_PrctReservasActual] DEFAULT ((0)),
[SaldoActualOrigen] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_SaldoActualOrigen] DEFAULT ((0)),
[AjusteCambiario] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_AjusteCambiario] DEFAULT ((0)),
[AjusteCalificacion] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_2016_AjusteCalificacion] DEFAULT ((0)),
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAlta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0419Datos_2016] ADD CONSTRAINT [PK_0419Datos_2016] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
