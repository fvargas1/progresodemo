CREATE TABLE [dbo].[SICC_Sucursal]
(
[IdSucursal] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdZona] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Sucursal] ADD CONSTRAINT [PK_IdSucursal] PRIMARY KEY CLUSTERED  ([IdSucursal]) ON [PRIMARY]
GO
