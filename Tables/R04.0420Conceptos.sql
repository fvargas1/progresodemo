CREATE TABLE [R04].[0420Conceptos]
(
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
