CREATE TABLE [Historico].[SICCMX_CreditoAval]
(
[IdPeriodoHistorico] [int] NOT NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Aval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Porcentaje] [decimal] (18, 10) NULL,
[Monto] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL,
[PorcentajeOriginal] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
