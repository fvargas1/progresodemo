CREATE TABLE [dbo].[SICC_TipoBaja]
(
[IdTipoBaja] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoBaja] ADD CONSTRAINT [PK_SICCMX_TipoBaja] PRIMARY KEY CLUSTERED  ([IdTipoBaja]) ON [PRIMARY]
GO
