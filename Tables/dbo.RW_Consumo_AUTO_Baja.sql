CREATE TABLE [dbo].[RW_Consumo_AUTO_Baja]
(
[IdReporteLog] [bigint] NOT NULL,
[FolioCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaBajaCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBajaCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuitasCondonacionesBonificacionesDescuentos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
