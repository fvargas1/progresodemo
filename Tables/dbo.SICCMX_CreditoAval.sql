CREATE TABLE [dbo].[SICCMX_CreditoAval]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdCredito] [bigint] NOT NULL,
[IdAval] [bigint] NOT NULL,
[Porcentaje] [decimal] (18, 10) NULL,
[Monto] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL,
[PorcentajeOriginal] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_CreditoAval] ADD CONSTRAINT [PK_SICCMX_CreditoAval] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO
