CREATE TABLE [dbo].[SICC_TipoProductoSerie4_2016]
(
[IdTipoProducto] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCartera] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoProductoSerie4_2016] ADD CONSTRAINT [PK_SICC_TipoProductoSerie4_2016] PRIMARY KEY CLUSTERED  ([IdTipoProducto]) ON [PRIMARY]
GO
