CREATE TABLE [dbo].[SICCMX_Metodologia]
(
[IdMetodologia] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdClasificacion] [int] NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoMapR01] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Metodologia] ADD CONSTRAINT [PK_SICCMX_Metodologia] PRIMARY KEY CLUSTERED  ([IdMetodologia]) ON [PRIMARY]
GO
