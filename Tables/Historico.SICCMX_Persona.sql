CREATE TABLE [Historico].[SICCMX_Persona]
(
[IdPeriodoHistorico] [int] NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domicilio] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telefono] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Correo] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ejecutivo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Institucion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Localidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeudorRelacionadoMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeudorRelacionado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sucursal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCreacion] [datetime] NULL,
[FechaActualizacion] [datetime] NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InstitucionNombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EjecutivoNombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomicaNombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalidadNombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsFondo] [int] NULL,
[PI100] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
