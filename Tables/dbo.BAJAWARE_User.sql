CREATE TABLE [dbo].[BAJAWARE_User]
(
[IdUser] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Active] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Blocked] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Telefono] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mobil] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroControl] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [dbo].[BAJAWARE_utBinary] NOT NULL,
[UsuarioWindows] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdUserType] [dbo].[BAJAWARE_utID] NULL,
[UltimaFechaLogin] [datetime] NULL,
[FechaNacimiento] [datetime] NULL,
[NeverLogedIn] [bit] NULL CONSTRAINT [DF_BAJAWARE_User_NeverLogedIn] DEFAULT ((1)),
[LastLoginAccess] [datetime] NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_BAJAWARE_User_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_User] ADD CONSTRAINT [PK_BAJAWARE_User] PRIMARY KEY CLUSTERED  ([IdUser]) ON [PRIMARY]
GO
