CREATE TABLE [dbo].[SICCMX_Ayuda_Calculo_PI]
(
[IdMetodologia] [int] NOT NULL,
[IdVariable] [int] NOT NULL,
[Rango] [varchar] (600) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Puntos] [int] NULL,
[Orden] [int] NULL
) ON [PRIMARY]
GO
