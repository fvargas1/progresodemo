CREATE TABLE [dbo].[SICC_Pais]
(
[IdPais] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Pais] ADD CONSTRAINT [PK_SICCMX_Pais] PRIMARY KEY CLUSTERED  ([IdPais]) ON [PRIMARY]
GO
