CREATE TABLE [dbo].[SICCMX_Consumo_Reservas_Variables]
(
[IdConsumo] [bigint] NULL,
[FechaCalificacion] [datetime] NULL,
[PI] [decimal] (18, 12) NULL,
[E] [decimal] (23, 2) NULL,
[MontoGarantia] [decimal] (23, 2) NULL,
[ReservaTotal] [decimal] (23, 2) NULL,
[PorReserva] [decimal] (18, 12) NULL,
[IdCalificacion] [int] NULL,
[ECubierta] [decimal] (23, 2) NULL,
[SPCubierta] [decimal] (18, 12) NULL,
[ReservaCubierta] [decimal] (23, 2) NULL,
[PorReservaCubierta] [decimal] (18, 12) NULL,
[IdCalificacionCubierta] [int] NULL,
[EExpuesta] [decimal] (23, 2) NULL,
[SPExpuesta] [decimal] (18, 12) NULL,
[ReservaExpuesta] [decimal] (23, 2) NULL,
[PorReservaExpuesta] [decimal] (18, 12) NULL,
[IdCalificacionExpuesta] [int] NULL,
[MontoAdicional] [decimal] (23, 2) NULL,
[ReservaAdicional] [decimal] (23, 2) NULL,
[PorcentajeAdicional] [decimal] (18, 12) NULL,
[IdCalificacionAdicional] [int] NULL,
[Tipo_EX] [decimal] (4, 2) NULL
) ON [PRIMARY]
GO
