CREATE TABLE [dbo].[SICC_PeriodoHistorico]
(
[IdPeriodoHistorico] [bigint] NOT NULL IDENTITY(1, 1),
[Fecha] [datetime] NULL,
[Version] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCreacion] [datetime] NULL CONSTRAINT [DF_SICC_PeriodoHistorico_FechaCreacion] DEFAULT (getdate()),
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarteraComercial] [decimal] (23, 2) NULL,
[CastigosNetos] [decimal] (23, 2) NULL,
[Activo] [bit] NULL,
[Reporte] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_PeriodoHistorico] ADD CONSTRAINT [PK_SICC_PeriodoHistorico] PRIMARY KEY CLUSTERED  ([IdPeriodoHistorico]) ON [PRIMARY]
GO
