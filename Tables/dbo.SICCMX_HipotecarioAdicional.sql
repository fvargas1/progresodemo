CREATE TABLE [dbo].[SICCMX_HipotecarioAdicional]
(
[IdHipotecario] [bigint] NOT NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresesDevengados] [decimal] (23, 2) NULL,
[Comisiones] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_HipotecarioAdicional] ADD CONSTRAINT [PK_SICCMX_HipotecarioAdicional] PRIMARY KEY CLUSTERED  ([IdHipotecario]) ON [PRIMARY]
GO
