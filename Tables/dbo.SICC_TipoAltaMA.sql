CREATE TABLE [dbo].[SICC_TipoAltaMA]
(
[IdTipoAlta] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[GeneraCNBV] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoAltaMA] ADD CONSTRAINT [PK_SICC_TipoAltaMA] PRIMARY KEY CLUSTERED  ([IdTipoAlta]) ON [PRIMARY]
GO
