CREATE TABLE [Historico].[SICCMX_Proyecto]
(
[IdPeriodoHistorico] [int] NULL,
[Proyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineaCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaCreacion] [datetime] NULL,
[Periodo] [datetime] NULL,
[FechaInicioOper] [datetime] NULL,
[TasaDescuentoVP] [decimal] (18, 10) NULL,
[VentNetTotAnuales] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
