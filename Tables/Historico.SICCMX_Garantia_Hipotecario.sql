CREATE TABLE [Historico].[SICCMX_Garantia_Hipotecario]
(
[IdPeriodoHistorico] [int] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIGarante] [decimal] (14, 10) NULL
) ON [PRIMARY]
GO
