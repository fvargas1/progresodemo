CREATE TABLE [Historico].[RW_ACM_SICC]
(
[IdPeriodoHistorico] [bigint] NULL,
[Periodo_Actual] [datetime] NULL,
[Tipo_Cred_R04A] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha_Ven] [datetime] NULL,
[Sector_Lab] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Situacion_Cred] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Interes_Vig] [decimal] (23, 2) NULL,
[Interes_Ven] [decimal] (23, 2) NULL,
[Saldo_Total] [decimal] (23, 2) NULL,
[Moneda] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dias_Vencidos] [int] NULL,
[Num_Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
