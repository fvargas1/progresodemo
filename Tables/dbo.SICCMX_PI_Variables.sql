CREATE TABLE [dbo].[SICCMX_PI_Variables]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Orden] [int] NULL,
[IdSubFactor] [int] NULL,
[EsCuestionario] [bit] NULL,
[IdMetodologia] [int] NULL,
[Campo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VariableBuro] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PI_Variables] ADD CONSTRAINT [PK_SICCMX_PI_Variables_Cat] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
