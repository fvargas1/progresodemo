CREATE TABLE [dbo].[SICCMX_CreditoGarantia]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdCredito] [bigint] NOT NULL,
[IdGarantia] [bigint] NOT NULL,
[PorcCubiertoCredito] [decimal] (18, 10) NULL,
[MontoCubiertoCredito] [decimal] (23, 2) NULL,
[PorcUsadoGarantia] [decimal] (18, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL,
[PorcBanco] [decimal] (18, 10) NULL,
[MontoBanco] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_CreditoGarantia] ADD CONSTRAINT [PK_SICCMX_CreditoGarantia] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO
