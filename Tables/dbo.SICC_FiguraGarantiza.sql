CREATE TABLE [dbo].[SICC_FiguraGarantiza]
(
[IdFigura] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequiereCalificacion] [bit] NULL,
[CodigoCNBV] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportePI] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_FiguraGarantiza] ADD CONSTRAINT [PK_SICC_FiguraGarantiza] PRIMARY KEY CLUSTERED  ([IdFigura]) ON [PRIMARY]
GO
