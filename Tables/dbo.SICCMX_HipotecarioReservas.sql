CREATE TABLE [dbo].[SICCMX_HipotecarioReservas]
(
[IdHipotecario] [bigint] NOT NULL,
[FechaCalificacion] [datetime] NULL,
[MontoExpuesto] [decimal] (23, 2) NULL,
[SPExpuesto] [decimal] (18, 12) NULL,
[ReservaExpuesto] [decimal] (23, 2) NULL,
[PorcentajeExpuesto] [decimal] (18, 12) NULL,
[IdCalificacionExpuesto] [int] NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[SPCubierto] [decimal] (18, 12) NULL,
[ReservaCubierto] [decimal] (23, 2) NULL,
[PorcentajeCubierto] [decimal] (18, 12) NULL,
[IdCalificacionCubierto] [int] NULL,
[MontoAdicional] [decimal] (23, 2) NULL,
[ReservaAdicional] [decimal] (23, 2) NULL,
[PorcentajeAdicional] [decimal] (18, 12) NULL,
[IdCalificacionAdicional] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_HipotecarioReservas] ADD CONSTRAINT [PK_SICCMX_HipotecarioReservas] PRIMARY KEY CLUSTERED  ([IdHipotecario]) ON [PRIMARY]
GO
