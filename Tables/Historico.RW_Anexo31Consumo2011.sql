CREATE TABLE [Historico].[RW_Anexo31Consumo2011]
(
[IdPeriodoHistorico] [bigint] NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Importe] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorcentajeReservas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Metodologia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImporteSemanal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImporteQuincenal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaSemanal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaQuincenal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
