CREATE TABLE [dbo].[SICCMX_Consumo_Movimientos]
(
[IdConsumoMovimiento] [bigint] NOT NULL IDENTITY(1, 1),
[IdConsumo] [bigint] NOT NULL,
[IdTipoMovimiento] [int] NOT NULL,
[Monto] [decimal] (23, 2) NOT NULL,
[Fecha] [datetime] NOT NULL,
[CalificacionAfecto] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0419] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0420] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0424] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Consumo_Movimientos] ADD CONSTRAINT [PK_SICCMX_Consumo_Movimientos] PRIMARY KEY CLUSTERED  ([IdConsumoMovimiento]) ON [PRIMARY]
GO
