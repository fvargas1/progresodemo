CREATE TABLE [Historico].[RW_R2431]
(
[IdPeriodoHistorico] [bigint] NULL,
[Periodo_Actual] [datetime] NULL,
[Nombre_Per_Relacionada] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC_Per_Rel] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Per_Juridica] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tipo_Rel_Inst] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tipo_Ope_Bal] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Clase_Oper_Rel] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tipo_Moneda] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Importe] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto_Ope_Rel] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto_PRR] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nom_Grupo_Emp] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID_Ope_Rel] [varchar] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Caracteristicas] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha_Originacion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha_Vencimiento] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto_Autorizado] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha_Disposicion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Monto_Dispuesto] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Origen_Otros_Activos] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
