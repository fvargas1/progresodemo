CREATE TABLE [dbo].[SICC_TipoBajaConsumo]
(
[IdTipoBajaConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanxico] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoBajaConsumo] ADD CONSTRAINT [PK_SICC_TipoBajaConsumo] PRIMARY KEY CLUSTERED  ([IdTipoBajaConsumo]) ON [PRIMARY]
GO
