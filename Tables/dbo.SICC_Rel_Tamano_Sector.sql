CREATE TABLE [dbo].[SICC_Rel_Tamano_Sector]
(
[IdTamano] [int] NOT NULL,
[IdSector] [int] NOT NULL,
[RangoMenor] [decimal] (9, 1) NULL,
[RangoMayor] [decimal] (9, 1) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Rel_Tamano_Sector] ADD CONSTRAINT [PK_SICC_Rel_Tamano_Sector] PRIMARY KEY CLUSTERED  ([IdTamano], [IdSector]) ON [PRIMARY]
GO
