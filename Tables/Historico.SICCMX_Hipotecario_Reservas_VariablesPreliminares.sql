CREATE TABLE [Historico].[SICCMX_Hipotecario_Reservas_VariablesPreliminares]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Metodologia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorVivienda] [decimal] (23, 2) NULL,
[INTEXP] [bit] NULL,
[SUBCVi] [decimal] (18, 10) NULL,
[SDESi] [decimal] (18, 10) NULL,
[Convenio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorCOBPAMED] [decimal] (18, 10) NULL,
[PorCOBPP] [decimal] (18, 10) NULL,
[Estado] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Municipio] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasAtraso] [int] NULL,
[Veces] [decimal] (18, 10) NULL,
[ATR] [int] NULL,
[MaxATR] [int] NULL,
[VPago] [decimal] (18, 10) NULL,
[PorPago] [decimal] (18, 10) NULL,
[PorCLTVi] [decimal] (18, 10) NULL,
[PPagoIM] [decimal] (18, 10) NULL,
[TRi] [decimal] (18, 10) NULL,
[MON] [int] NULL,
[RAi] [decimal] (18, 10) NULL,
[PromedioDeRet] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
