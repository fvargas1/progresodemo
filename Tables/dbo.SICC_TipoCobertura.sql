CREATE TABLE [dbo].[SICC_TipoCobertura]
(
[IdTipoCobertura] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoCobertura] ADD CONSTRAINT [PK_SICC_TipoCobertura] PRIMARY KEY CLUSTERED  ([IdTipoCobertura]) ON [PRIMARY]
GO
