CREATE TABLE [dbo].[MIGRACION_Categoria]
(
[IdCategoria] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdParent] [int] NULL,
[NombreTabla] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Categoria] ADD CONSTRAINT [PK_MIGRACION_Categoria] PRIMARY KEY CLUSTERED  ([IdCategoria]) ON [PRIMARY]
GO
