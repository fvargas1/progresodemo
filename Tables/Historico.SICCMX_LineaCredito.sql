CREATE TABLE [Historico].[SICCMX_LineaCredito]
(
[IdPeriodoHistorico] [int] NULL,
[CodigoLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaDisposicion] [datetime] NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinoCreditoComercial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaReferencia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoLinea] [decimal] (23, 2) NULL,
[FrecuenciaRevisionTasa] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAlta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisposicionCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjusteTasaReferencia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InstitucionFondea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ComisionesCobradas] [decimal] (23, 2) NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImporteQuebranto] [decimal] (23, 2) NULL,
[SaldoInicial] [decimal] (23, 2) NULL,
[PeriodicidadCapital] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadInteres] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoReestructurado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductoComercial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Posicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoOperacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecMaxDis] [datetime] NULL,
[PorcPartFederal] [decimal] (23, 2) NULL,
[MontoPagEfeCap] [decimal] (23, 2) NULL,
[MontoPagEfeInt] [decimal] (23, 2) NULL,
[MontoPagEfeCom] [decimal] (23, 2) NULL,
[MontoPagEfeIntMor] [decimal] (23, 2) NULL,
[ComDispTasa] [decimal] (18, 10) NULL,
[ComDispMonto] [decimal] (23, 2) NULL,
[GastosOrigTasa] [decimal] (18, 10) NULL,
[ResTotalInicioPer] [decimal] (23, 2) NULL,
[MontoQuitasCastQue] [decimal] (23, 2) NULL,
[MontoBonificacionDesc] [decimal] (23, 2) NULL,
[MesesGraciaCap] [int] NULL,
[MesesGraciaIntereses] [int] NULL,
[NumEmpLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumEmpLocFedSHCP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecVenLinea] [datetime] NULL,
[CAT] [decimal] (18, 10) NULL,
[MontoLineaSinA] [decimal] (23, 2) NULL,
[MontoPrimasAnuales] [decimal] (23, 2) NULL,
[EsPadre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MunicipioDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomicaDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolioConsultaBuro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsMultimoneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinoCreditoMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAltaMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisposicionMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadCapitalMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadInteresMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBajaMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroAgrupacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBancaDesarrollo] [decimal] (23, 2) NULL,
[ComisionDelMes] [decimal] (23, 2) NULL,
[GrupalCNBV] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCastigos] [decimal] (23, 2) NULL,
[MontoQuebrantos] [decimal] (23, 2) NULL,
[MontoDescuentos] [decimal] (23, 2) NULL,
[MontoDacion] [decimal] (23, 2) NULL,
[LocalidadDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstadoDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCondonacion] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
