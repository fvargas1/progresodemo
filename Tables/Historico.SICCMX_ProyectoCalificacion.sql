CREATE TABLE [Historico].[SICCMX_ProyectoCalificacion]
(
[IdPeriodoHistorico] [int] NULL,
[Proyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fecha] [datetime] NOT NULL,
[SobreCosto] [decimal] (23, 2) NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[Meses] [int] NULL,
[MesesAdicionales] [int] NULL,
[VPTotal] [decimal] (23, 2) NULL,
[UpAcumulada] [decimal] (23, 2) NULL,
[Etapa] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[SobrecostoGarantia] [decimal] (23, 2) NULL,
[ReservaConstruccion] [decimal] (23, 2) NULL,
[ReservaOperacion] [decimal] (23, 2) NULL,
[ExposicionSobrecosto] [decimal] (23, 2) NULL,
[PorcenajeSobrecosto] [decimal] (18, 10) NULL,
[CalificacionSobrecosto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorcentajeCorrida] [decimal] (18, 10) NULL,
[CalificacionCorrida] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorcentajeFinal] [decimal] (18, 10) NULL,
[CalificacionFinal] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoInsoluto] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
