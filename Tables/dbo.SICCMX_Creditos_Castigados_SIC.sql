CREATE TABLE [dbo].[SICCMX_Creditos_Castigados_SIC]
(
[IdPersona] [bigint] NOT NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdPeriodo] [int] NULL,
[FechaIngreso] [datetime] NULL
) ON [PRIMARY]
GO
