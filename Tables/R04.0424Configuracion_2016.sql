CREATE TABLE [R04].[0424Configuracion_2016]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0424Configuracion_2016] ADD CONSTRAINT [PK_0424Configuracion_2016] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO
