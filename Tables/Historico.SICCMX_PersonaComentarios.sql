CREATE TABLE [Historico].[SICCMX_PersonaComentarios]
(
[IdPeriodoHistorico] [int] NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comentarios] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
