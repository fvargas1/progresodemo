CREATE TABLE [dbo].[SICC_DisposicionCredito]
(
[IdDisposicion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AplicaBaja] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_DisposicionCredito] ADD CONSTRAINT [PK_SICCMX_DisposicionCredito] PRIMARY KEY CLUSTERED  ([IdDisposicion]) ON [PRIMARY]
GO
