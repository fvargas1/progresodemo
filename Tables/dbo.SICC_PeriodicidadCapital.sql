CREATE TABLE [dbo].[SICC_PeriodicidadCapital]
(
[IdPeriodicidadCapital] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_PeriodicidadCapital] ADD CONSTRAINT [PK_SICCMX_PeriodicidadCapital] PRIMARY KEY CLUSTERED  ([IdPeriodicidadCapital]) ON [PRIMARY]
GO
