CREATE TABLE [dbo].[SICC_Rel_RCCalif_TipoPers]
(
[IdRCCalificacion] [int] NOT NULL,
[IdTipoPersona] [int] NOT NULL,
[PonderadorRiesgo] [decimal] (3, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Rel_RCCalif_TipoPers] ADD CONSTRAINT [PK_SICC_Rel_RCCalif_TipoPers] PRIMARY KEY CLUSTERED  ([IdRCCalificacion], [IdTipoPersona]) ON [PRIMARY]
GO
