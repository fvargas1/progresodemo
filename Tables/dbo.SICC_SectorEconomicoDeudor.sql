CREATE TABLE [dbo].[SICC_SectorEconomicoDeudor]
(
[IdSectorEconomico] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoRR] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SectorEconomicoDeudor] ADD CONSTRAINT [PK_SICC_SectorEconomicoDeudor] PRIMARY KEY CLUSTERED  ([IdSectorEconomico]) ON [PRIMARY]
GO
