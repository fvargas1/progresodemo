CREATE TABLE [dbo].[SICC_Fondeo]
(
[IdFondeo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Fondeo] ADD CONSTRAINT [PK_SICCMX_Fondeo] PRIMARY KEY CLUSTERED  ([IdFondeo]) ON [PRIMARY]
GO
