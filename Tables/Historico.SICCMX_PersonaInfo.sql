CREATE TABLE [Historico].[SICCMX_PersonaInfo]
(
[IdPeriodoHistorico] [int] NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoPersonaMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroEmpleados] [int] NULL,
[IngresosBrutos] [decimal] (23, 2) NULL,
[SectorLaboral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrupoRiesgo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoIngreso] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreCNBV] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicOrdenadasCNBV] [money] NOT NULL CONSTRAINT [DF_SICCMX_PersonaInfo_ReservasAdicOrdenadasCNBV_1] DEFAULT ((0)),
[Estado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Municipio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nacionalidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HITSIC] [int] NULL,
[PadreGrupoEconomico] [bit] NULL,
[SectorEconomico] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TamanoDeudor] [int] NULL,
[RCGrupoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCalificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TopeTamano] [decimal] (9, 1) NULL,
[CalificacionLargoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgenciaLargoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalificacionCortoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgenciaCortoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCalificacionCortoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPostal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
