CREATE TABLE [dbo].[SICC_LocalidadZona]
(
[IdLocalidadZona] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_LocalidadZona] ADD CONSTRAINT [PK_SICC_LocalidadZona] PRIMARY KEY CLUSTERED  ([IdLocalidadZona]) ON [PRIMARY]
GO
