CREATE TABLE [dbo].[SICC_EstadoRegion_Anx16]
(
[Estado] [smallint] NOT NULL,
[Region] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_EstadoRegion_Anx16] ADD CONSTRAINT [PK_SICC_EstadoRegion_Anx16] PRIMARY KEY CLUSTERED  ([Estado]) ON [PRIMARY]
GO
