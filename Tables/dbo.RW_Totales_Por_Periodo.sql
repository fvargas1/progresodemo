CREATE TABLE [dbo].[RW_Totales_Por_Periodo]
(
[IdPeriodo] [int] NOT NULL,
[FechaPeriodo] [datetime] NOT NULL,
[IdPersona] [bigint] NOT NULL,
[PI] [decimal] (18, 12) NULL,
[EI] [decimal] (23, 2) NULL,
[EI_Cubierta] [decimal] (23, 2) NULL,
[EI_Expuesta] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[Reserva_Cubierta] [decimal] (23, 2) NULL,
[Reserva_Expuesta] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
