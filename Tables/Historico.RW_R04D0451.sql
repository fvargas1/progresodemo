CREATE TABLE [Historico].[RW_R04D0451]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[ClaveInstitucion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subreporte] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MetodoCalificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCalificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoSaldo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
