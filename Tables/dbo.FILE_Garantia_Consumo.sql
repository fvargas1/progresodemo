CREATE TABLE [dbo].[FILE_Garantia_Consumo]
(
[CodigoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaValuacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BancoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantiaProyectado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VencimientoRestante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgenciaCalificadora] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Emisor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Escala] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsIPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCliente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndPerMorales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[errorFormato] [bit] NULL,
[errorCatalogo] [bit] NULL,
[Homologado] [bit] NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
