CREATE TABLE [dbo].[RW_Responsabilidades]
(
[IdReportelog] [bigint] NULL,
[INSTITUCIO] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCINSTITU] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ANO_INFO] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MES_INFO] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BLANCO_UNO] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECTOR] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MONEDA] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOMBRE] [char] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCDEU] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTDEU] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BLANCO_DOS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAL_RIESGO] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BLANCO_TRE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TPO_CREDIT] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SALDOTPOCR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPTE_6101] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DDOR_6101] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPTE_6102] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DDOR_6102] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVE_IMPTE1] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORTE_01] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVE_IMPTE2] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORTE_02] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVE_IMPTE3] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORTE_03] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CVE_IMPTE4] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORTE_04] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
