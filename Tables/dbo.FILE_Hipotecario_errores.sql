CREATE TABLE [dbo].[FILE_Hipotecario_errores]
(
[identificador] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nombreCampo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[valor] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tipoError] [int] NULL,
[description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
