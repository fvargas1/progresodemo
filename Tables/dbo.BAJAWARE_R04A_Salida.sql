CREATE TABLE [dbo].[BAJAWARE_R04A_Salida]
(
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubReporte] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCartera] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoSaldo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
