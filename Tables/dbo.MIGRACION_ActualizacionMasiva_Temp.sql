CREATE TABLE [dbo].[MIGRACION_ActualizacionMasiva_Temp]
(
[sessionID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[codigoBusqueda] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[campo1Usuario] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[campo1Original] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[campo2Usuario] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[campo2Original] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[campo3Usuario] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[campo3Original] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[valorEncontrado] [bit] NOT NULL CONSTRAINT [DF_MIGRACION_ActualizacionMasiva_Temp_valorEncontrado] DEFAULT ((0)),
[seActualiza] [bit] NOT NULL CONSTRAINT [DF_MIGRACION_ActualizacionMasiva_Temp_SeActualiza] DEFAULT ((0)),
[error] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
