CREATE TABLE [dbo].[SICC_Tasa]
(
[IdTasa] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdMoneda] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Tasa] ADD CONSTRAINT [PK_SICC_Tasa] PRIMARY KEY CLUSTERED  ([IdTasa]) ON [PRIMARY]
GO
