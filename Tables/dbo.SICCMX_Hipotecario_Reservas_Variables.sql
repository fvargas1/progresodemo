CREATE TABLE [dbo].[SICCMX_Hipotecario_Reservas_Variables]
(
[IdHipotecario] [bigint] NOT NULL,
[PI] [decimal] (18, 12) NULL,
[SP] [decimal] (18, 12) NULL,
[E] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[PorReserva] [decimal] (18, 12) NULL,
[IdCalificacion] [int] NULL,
[PerdidaEsperada] [decimal] (18, 12) NULL,
[ReservaOriginal] [decimal] (23, 2) NULL,
[PorCubiertoPP] [decimal] (18, 12) NULL,
[ECubiertaPP] [decimal] (23, 2) NULL,
[ReservaCubPP] [decimal] (23, 2) NULL,
[ReservaPP_Credito] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Hipotecario_Reservas_Variables] ADD CONSTRAINT [PK_SICCMX_Hipotecario_Reservas_Variables] PRIMARY KEY CLUSTERED  ([IdHipotecario]) ON [PRIMARY]
GO
