CREATE TABLE [dbo].[FILE_ConsumoAval_Hst]
(
[CodigoAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Porcentaje] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
