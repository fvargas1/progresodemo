CREATE TABLE [R04].[0415Configuracion]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SituacionCredito] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0415Configuracion] ADD CONSTRAINT [PK_0415Configuracion] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO
