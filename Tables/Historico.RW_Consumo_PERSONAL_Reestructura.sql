CREATE TABLE [Historico].[RW_Consumo_PERSONAL_Reestructura]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[FolioCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaReestructuraCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reestructura] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuitasCondonacionesBonificacionesDescuentos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Folio2Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
