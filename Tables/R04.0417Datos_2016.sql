CREATE TABLE [R04].[0417Datos_2016]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Moneda] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminacionCuenta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Saldo] [decimal] (23, 2) NULL,
[Estimacion] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CartaDeCredito] [bit] NULL,
[ReservasAdicOrdenadasCNBV] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0417Datos_ReservasAdicOrdenadasCNBV_2016] DEFAULT ((0)),
[SaldoValorizado] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0417Datos_2016] ADD CONSTRAINT [PK_0417Datos_2016] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
