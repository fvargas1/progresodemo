CREATE TABLE [Historico].[RW_R04C0450_2016]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreGarante] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActEconomica] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Localidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Municipio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Estado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreAcreditado] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonedaGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
