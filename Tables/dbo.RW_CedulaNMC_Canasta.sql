CREATE TABLE [dbo].[RW_CedulaNMC_Canasta]
(
[IdPersona] [bigint] NOT NULL,
[IdCredito] [bigint] NOT NULL,
[NumeroLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarantia] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctSinAjust] [decimal] (18, 10) NULL,
[PrctAjust] [decimal] (18, 10) NULL,
[PI] [decimal] (18, 10) NULL,
[SP] [decimal] (18, 10) NULL,
[MontoRecuperacion] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[Posicion] [smallint] NULL
) ON [PRIMARY]
GO
