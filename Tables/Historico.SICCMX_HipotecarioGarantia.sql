CREATE TABLE [Historico].[SICCMX_HipotecarioGarantia]
(
[IdPeriodoHistorico] [int] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PorUsadoGarantia] [decimal] (14, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
