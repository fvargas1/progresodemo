CREATE TABLE [R04].[0424Datos]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdMoneda] [bigint] NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionHistorica] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionActual] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoHistorico] [decimal] (23, 2) NULL,
[SaldoActual] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[InteresVencidoHistorico] [decimal] (23, 2) NULL,
[InteresVigenteHistorico] [decimal] (23, 2) NULL,
[InteresCarteraVencidaHistorico] [decimal] (23, 2) NULL,
[Reestructurado] [bit] NULL CONSTRAINT [DF__0424Datos__Reest__5FDFB70D] DEFAULT ((0)),
[CodigoCreditoReestructurado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0424Datos] ADD CONSTRAINT [PK_0424Datos] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
