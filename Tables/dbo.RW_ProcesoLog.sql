CREATE TABLE [dbo].[RW_ProcesoLog]
(
[Id_ProcesoLog] [int] NOT NULL IDENTITY(1, 1),
[Categoria] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Proceso] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ProcesoLog] ADD CONSTRAINT [PK_RW_ProcesoLog] PRIMARY KEY CLUSTERED  ([Id_ProcesoLog]) ON [PRIMARY]
GO
