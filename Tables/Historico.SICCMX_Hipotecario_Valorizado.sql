CREATE TABLE [Historico].[SICCMX_Hipotecario_Valorizado]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVigente] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[SaldoCapitalVencido] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[MontoGarantia] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
