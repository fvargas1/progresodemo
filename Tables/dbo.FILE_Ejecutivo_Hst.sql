CREATE TABLE [dbo].[FILE_Ejecutivo_Hst]
(
[CodigoEjecutivo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telefono] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Correo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
