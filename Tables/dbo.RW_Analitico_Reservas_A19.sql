CREATE TABLE [dbo].[RW_Analitico_Reservas_A19]
(
[IdReporteLog] [bigint] NOT NULL,
[Fecha] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombrePersona] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreProyecto] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaVecimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoReserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctReserva] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
