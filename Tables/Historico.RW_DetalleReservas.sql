CREATE TABLE [Historico].[RW_DetalleReservas]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Fecha] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoProducto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaFinal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cargo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Abono] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
