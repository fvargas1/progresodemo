CREATE TABLE [Historico].[RW_R04H0493_2016]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroSecuencia] [int] NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroAvaluo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPrincipalInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponsabilidadTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoPagoAcreditado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoQuitas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCondonaciones] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBonificaciones] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoDescuentos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorBienAdjudicado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoDacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasCalifCanceladas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicCanceladas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
