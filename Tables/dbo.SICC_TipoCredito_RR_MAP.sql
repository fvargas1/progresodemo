CREATE TABLE [dbo].[SICC_TipoCredito_RR_MAP]
(
[TipoSaldo] [int] NOT NULL,
[IdMetodologia] [int] NULL,
[TipoCreditoR04A] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCreditoRR] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
