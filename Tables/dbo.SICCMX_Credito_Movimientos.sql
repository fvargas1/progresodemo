CREATE TABLE [dbo].[SICCMX_Credito_Movimientos]
(
[IdCreditoMovimiento] [bigint] NOT NULL IDENTITY(1, 1),
[IdCredito] [bigint] NOT NULL,
[IdTipoMovimiento] [int] NOT NULL,
[Monto] [decimal] (23, 2) NOT NULL,
[Fecha] [datetime] NOT NULL,
[CalificacionAfecto] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0419] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0420] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto0424] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Credito_Movimientos] ADD CONSTRAINT [PK_SICCMX_Credito_Movimientos] PRIMARY KEY CLUSTERED  ([IdCreditoMovimiento]) ON [PRIMARY]
GO
