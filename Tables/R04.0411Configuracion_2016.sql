CREATE TABLE [R04].[0411Configuracion_2016]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0411Configuracion_2016] ADD CONSTRAINT [PK_0411Configuracion_2016] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO
