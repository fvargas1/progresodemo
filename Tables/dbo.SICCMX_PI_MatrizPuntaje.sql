CREATE TABLE [dbo].[SICCMX_PI_MatrizPuntaje]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[IdVariable] [int] NOT NULL,
[RangoMenor] [decimal] (26, 10) NULL,
[RangoMayor] [decimal] (26, 10) NULL,
[Puntos] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PI_MatrizPuntaje] ADD CONSTRAINT [PK_SICCMX_PI_MatrizPuntaje] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
