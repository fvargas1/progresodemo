CREATE TABLE [R04].[0411Datos]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Moneda] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionCredito] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Morosidad] [int] NULL,
[CapitalVigente] [decimal] (23, 2) NULL,
[CapitalVencido] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCartera] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0411Datos] ADD CONSTRAINT [PK_0411Datos] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
