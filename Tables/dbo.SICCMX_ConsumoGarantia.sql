CREATE TABLE [dbo].[SICCMX_ConsumoGarantia]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdConsumo] [bigint] NULL,
[IdGarantia] [bigint] NULL,
[PorUsadoGarantia] [decimal] (14, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_ConsumoGarantia] ADD CONSTRAINT [PK_SICCMX_ConsumoGarantia] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO
