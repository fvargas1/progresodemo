CREATE TABLE [Historico].[SICCMX_Garantia_Hipotecario_PP]
(
[IdPeriodoHistorico] [int] NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[MontoExpuesto] [decimal] (23, 2) NULL,
[PIGarante] [decimal] (14, 10) NULL,
[SPGarante] [decimal] (14, 10) NULL,
[ReservaCubierto] [decimal] (23, 2) NULL,
[ReservaExpuesto] [decimal] (23, 2) NULL,
[ReservaFinal] [decimal] (23, 2) NULL,
[PrctCubierto] [decimal] (22, 10) NULL
) ON [PRIMARY]
GO
