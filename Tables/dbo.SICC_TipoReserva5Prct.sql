CREATE TABLE [dbo].[SICC_TipoReserva5Prct]
(
[IdTipoReserva] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
