CREATE TABLE [dbo].[SICCMX_Anexo21_GP]
(
[IdGP] [bigint] NOT NULL,
[AntSocInfCredCredito] [int] NULL,
[QuitasCastRest] [bit] NULL,
[PorPagoTiemInstNoBanc] [decimal] (18, 10) NULL,
[PorPagEntCom60oMasAtraso] [decimal] (18, 10) NULL,
[CuenCredAbierInstBancUlt12mes] [int] NULL,
[MonMaxCredOtorInstBanExpUDIS] [decimal] (23, 2) NULL,
[NumMesUltCredAbiUlt12Meses] [int] NULL,
[PorPagIntBan60oMasAtraso] [decimal] (18, 10) NULL,
[PorPagIntBan1a29Atraso] [decimal] (18, 10) NULL,
[PorPagIntBan90oMasAtraso] [decimal] (18, 10) NULL,
[NumDiasMoraPromInstBanc] [decimal] (18, 10) NULL,
[NumPagTiemIntBanUlt12Meses] [int] NULL,
[MonTotPagInfonavitUltBimestre] [decimal] (23, 2) NULL,
[NumDiasAtrInfonavitUltBimestre] [decimal] (18, 10) NULL,
[TasaRetLab] [decimal] (18, 10) NULL,
[ProcOriAdmonCred] [int] NULL,
[SinAtrasos] [bit] NULL,
[VentNetTotAnuales] [decimal] (23, 2) NULL,
[OrgDescPartidoPolitico] [bit] NULL,
[FechaInfoFinanc] [datetime] NULL,
[FechaInfoBuro] [datetime] NULL,
[CalCredAgenciaCal] [bit] NULL,
[ExpNegativasPag] [bit] NULL,
[IndPerMorales] [bit] NULL,
[NumeroEmpleados] [int] NULL,
[LugarRadica] [int] NULL,
[EsGarante] [int] NULL,
[F_AntSocInfCredCredito] [datetime] NULL,
[F_QuitasCastRest] [datetime] NULL,
[F_PorPagoTiemInstNoBanc] [datetime] NULL,
[F_PorPagEntCom60oMasAtraso] [datetime] NULL,
[F_CuenCredAbierInstBancUlt12mes] [datetime] NULL,
[F_MonMaxCredOtorInstBanExpUDIS] [datetime] NULL,
[F_NumMesUltCredAbiUlt12Meses] [datetime] NULL,
[F_PorPagIntBan60oMasAtraso] [datetime] NULL,
[F_PorPagIntBan1a29Atraso] [datetime] NULL,
[F_PorPagIntBan90oMasAtraso] [datetime] NULL,
[F_NumDiasMoraPromInstBanc] [datetime] NULL,
[F_NumPagTiemIntBanUlt12Meses] [datetime] NULL,
[F_MonTotPagInfonavitUltBimestre] [datetime] NULL,
[F_NumDiasAtrInfonavitUltBimestre] [datetime] NULL,
[F_TasaRetLab] [datetime] NULL
) ON [PRIMARY]
GO
