CREATE TABLE [dbo].[SICCMX_Credito_Reservas_Variables_Log]
(
[IdCredito] [bigint] NULL,
[Fecha] [datetime] NULL,
[Usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comentarios] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
