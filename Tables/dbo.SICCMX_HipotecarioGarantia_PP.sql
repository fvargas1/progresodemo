CREATE TABLE [dbo].[SICCMX_HipotecarioGarantia_PP]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdHipotecario] [bigint] NOT NULL,
[IdGarantia] [bigint] NOT NULL,
[PrctReserva] [decimal] (14, 10) NULL,
[Reserva] [decimal] (23, 2) NULL,
[ReservaCubierta] [decimal] (23, 2) NULL,
[ReservaExpuesta] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_HipotecarioGarantia_PP] ADD CONSTRAINT [PK_SICCMX_HipotecarioGarantia_PP] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO
