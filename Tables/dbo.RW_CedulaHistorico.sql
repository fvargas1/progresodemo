CREATE TABLE [dbo].[RW_CedulaHistorico]
(
[IdCedula] [bigint] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportFolder] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Reportname] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Parameters] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Prefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Activo] [bit] NOT NULL,
[OutputParams] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_CedulaHistorico] ADD CONSTRAINT [PK_RW_CedulaHistorico] PRIMARY KEY CLUSTERED  ([IdCedula]) ON [PRIMARY]
GO
