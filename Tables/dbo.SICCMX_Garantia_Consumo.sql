CREATE TABLE [dbo].[SICCMX_Garantia_Consumo]
(
[IdGarantia] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdTipoGarantia] [int] NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[IdMoneda] [int] NULL,
[FechaValuacion] [datetime] NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdBancoGarantia] [bigint] NULL,
[ValorGarantiaProyectado] [decimal] (23, 2) NULL,
[Hc] [decimal] (18, 10) NULL,
[VencimientoRestante] [datetime] NULL,
[IdGradoRiesgo] [int] NULL,
[IdAgenciaCalificadora] [int] NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdEmisor] [int] NULL,
[IdEscala] [int] NULL,
[EsIPC] [bit] NULL,
[PIGarante] [decimal] (18, 10) NULL,
[IdPersona] [bigint] NULL,
[IndPerMorales] [bit] NULL,
[Aplica] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Garantia_Consumo] ADD CONSTRAINT [PK_SICCMX_Garantia_Consumo] PRIMARY KEY CLUSTERED  ([IdGarantia]) ON [PRIMARY]
GO
