CREATE TABLE [dbo].[SICCMX_Metodologia_General]
(
[IdMetodologia] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Metodologia_General] ADD CONSTRAINT [PK_SICCMX_Metodologia_General] PRIMARY KEY CLUSTERED  ([IdMetodologia]) ON [PRIMARY]
GO
