CREATE TABLE [dbo].[RW_R04C0464]
(
[IdReporteLog] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombrePersona] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoriaCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaVencDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaInteres] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoDispuesto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoPagoExigible] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCapitalPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoInteresPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoComisionPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoInteresMoratorio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoTotalPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBonificado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoFinal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCalculoInteres] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasCalculoInteres] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoInteresAplicar] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponsabilidadFinal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasVencidos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaUltimoPago] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProyectoInversion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBancaDesarrollo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InstitucionFondeo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaCubierta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaExpuesta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPCubierta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPExpuesta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EITotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EICubierta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EIExpuesta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PITotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PICubierta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIExpuesta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaTotalInterna] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPInterna] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EIInterna] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIInterna] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
