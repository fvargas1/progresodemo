CREATE TABLE [dbo].[SICCMX_Persona_PI_Log_GP]
(
[IdGP] [bigint] NOT NULL,
[EsGarante] [int] NOT NULL,
[FechaCalculo] [datetime] NULL,
[Usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
