CREATE TABLE [Historico].[RW_Analitico_A21]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PonderadoCuantitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PonderadoCualitativo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AntSocInfCred_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AntSocInfCred_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuitasCastReest_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QuitasCastReest_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstNoBanc_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstNoBanc_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagosEntComer_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagosEntComer_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CuentasCredAbiertosInstFin_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CuentasCredAbiertosInstFin_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoMaxInstFin_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoMaxInstFin_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesUltCredAbierto_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesUltCredAbierto_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFinBanc_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFinBanc_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFin29Atr_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFin29Atr_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFin90Atr_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPagoInstFin90Atr_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasMoraInstFinBanc_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasMoraInstFinBanc_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PagosInstFinBanc_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PagosInstFinBanc_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AportInfonavit_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AportInfonavit_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasAtrInfonavit_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasAtrInfonavit_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaRetLab_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaRetLab_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndPerMoralFid_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndPerMoralFid_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcOrigAdmon_V] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcOrigAdmon_P] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SinAtrasos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
