CREATE TABLE [dbo].[SICCMX_Proyecto]
(
[IdProyecto] [bigint] NOT NULL IDENTITY(1, 1),
[CodigoProyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdPersona] [bigint] NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdLineaCredito] [bigint] NULL,
[IdMoneda] [bigint] NULL,
[FechaCreacion] [datetime] NULL,
[Periodo] [datetime] NULL,
[FechaInicioOper] [datetime] NULL,
[TasaDescuentoVP] [decimal] (18, 10) NULL,
[VentNetTotAnuales] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Proyecto] ADD CONSTRAINT [PK_SICCMX_Proyecto] PRIMARY KEY CLUSTERED  ([IdProyecto]) ON [PRIMARY]
GO
