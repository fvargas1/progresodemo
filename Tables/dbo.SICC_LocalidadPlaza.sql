CREATE TABLE [dbo].[SICC_LocalidadPlaza]
(
[IdLocalidadPlaza] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdLocalidadPais] [bigint] NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_LocalidadPlaza] ADD CONSTRAINT [PK_SICC_LocalidadPlaza] PRIMARY KEY CLUSTERED  ([IdLocalidadPlaza]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_LocalidadPlaza] ADD CONSTRAINT [FK_SICC_LocalidadPlaza_SICC_LocalidadPais] FOREIGN KEY ([IdLocalidadPais]) REFERENCES [dbo].[SICC_LocalidadPais] ([IdLocalidadPais])
GO
