CREATE TABLE [dbo].[SICCMX_Consumo]
(
[IdConsumo] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdPersona] [bigint] NULL,
[SaldoCapitalVigente] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[SaldoCapitalVencido] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[Producto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdReestructura] [int] NULL,
[TasaInteres] [decimal] (18, 10) NULL,
[IdSituacionCredito] [int] NULL,
[IdPeriodicidadCapital] [int] NULL,
[IdTipoCredito] [int] NULL,
[EsRevolvente] [bit] NULL,
[IdPeriodicidadCalificacion] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Consumo] ADD CONSTRAINT [PK_SICCMX_Consumo] PRIMARY KEY CLUSTERED  ([IdConsumo]) ON [PRIMARY]
GO
