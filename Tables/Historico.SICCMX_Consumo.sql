CREATE TABLE [Historico].[SICCMX_Consumo]
(
[IdPeriodoHistorico] [int] NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVigente] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[SaldoCapitalVencido] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[Producto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reestructura] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaInteres] [decimal] (18, 10) NULL,
[SituacionCredito] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadCapital] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCredito] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EsRevolvente] [bit] NULL,
[PeriodicidadCalificacion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
