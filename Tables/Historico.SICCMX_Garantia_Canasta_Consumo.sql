CREATE TABLE [Historico].[SICCMX_Garantia_Canasta_Consumo]
(
[IdPeriodoHistorico] [int] NULL,
[Consumo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[He] [decimal] (18, 10) NULL,
[Hc] [decimal] (18, 10) NULL,
[Hfx] [decimal] (18, 10) NULL,
[C] [decimal] (23, 2) NULL,
[PrctCobSinAju] [decimal] (18, 10) NULL,
[PrctCobAjust] [decimal] (18, 10) NULL,
[MontoCobAjust] [decimal] (25, 2) NULL,
[PI] [decimal] (18, 10) NULL,
[EI_Ajust] [decimal] (25, 4) NULL,
[SeveridadCorresp] [decimal] (18, 12) NULL,
[MontoRecuperacion] [decimal] (25, 4) NULL,
[Reserva] [decimal] (27, 6) NULL,
[EsDescubierto] [bit] NULL
) ON [PRIMARY]
GO
