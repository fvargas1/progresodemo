CREATE TABLE [dbo].[SICCMX_Ejecutivo]
(
[IdEjecutivo] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telefono] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Correo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Ejecutivo] ADD CONSTRAINT [PK_SICCMX_Ejecutivo] PRIMARY KEY CLUSTERED  ([IdEjecutivo]) ON [PRIMARY]
GO
