CREATE TABLE [dbo].[FILE_Accionista_Hst]
(
[Deudor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreAccionista] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Porcentaje] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
