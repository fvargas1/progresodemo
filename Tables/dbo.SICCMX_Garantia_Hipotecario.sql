CREATE TABLE [dbo].[SICCMX_Garantia_Hipotecario]
(
[IdGarantia] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValorGarantia] [decimal] (23, 2) NULL,
[IdTipoGarantia] [int] NOT NULL,
[IdMoneda] [int] NOT NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIGarante] [decimal] (14, 10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Garantia_Hipotecario] ADD CONSTRAINT [PK_SICCMX_Garantia_Hipotecario] PRIMARY KEY CLUSTERED  ([IdGarantia]) ON [PRIMARY]
GO
