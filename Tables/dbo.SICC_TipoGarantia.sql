CREATE TABLE [dbo].[SICC_TipoGarantia]
(
[IdTipoGarantia] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [int] NULL,
[IdClasificacionNvaMet] [int] NULL,
[EsEfectivo] [bit] NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comercial] [bit] NULL,
[Hipotecario] [bit] NULL,
[Consumo] [bit] NULL,
[CodigoCR] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCRReal] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AplicaDist] [bit] NULL,
[AplicaSP100] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoGarantia] ADD CONSTRAINT [PK_SICCMX_TipoGarantia] PRIMARY KEY CLUSTERED  ([IdTipoGarantia]) ON [PRIMARY]
GO
