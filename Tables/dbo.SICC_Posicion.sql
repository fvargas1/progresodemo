CREATE TABLE [dbo].[SICC_Posicion]
(
[IdPosicion] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SP] [decimal] (3, 2) NULL
) ON [PRIMARY]
GO
