CREATE TABLE [dbo].[SICCMX_PI_ValoresDelta]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[IdMetodologia] [int] NULL,
[FactorCuantitativo] [decimal] (10, 2) NULL,
[FactorCualitativo] [decimal] (10, 2) NULL,
[X] [decimal] (10, 2) NULL,
[IdClasificacion] [int] NULL,
[NombreClasificacion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PI_ValoresDelta] ADD CONSTRAINT [PK_SICCMX_PI_ValoresDelta] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
