CREATE TABLE [dbo].[RW_INSUMOS_R01_SICC]
(
[IdReporteLog] [bigint] NOT NULL,
[FechaPeriodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonedaISO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoValorizado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
