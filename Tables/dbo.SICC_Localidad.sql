CREATE TABLE [dbo].[SICC_Localidad]
(
[IdLocalidad] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdLocalidadPlaza] [bigint] NULL,
[IdLocalidadPais] [bigint] NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Localidad] ADD CONSTRAINT [PK_SICC_Localidad] PRIMARY KEY CLUSTERED  ([IdLocalidad]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Localidad] ADD CONSTRAINT [FK_SICC_Localidad_SICC_LocalidadPais] FOREIGN KEY ([IdLocalidadPais]) REFERENCES [dbo].[SICC_LocalidadPais] ([IdLocalidadPais])
GO
ALTER TABLE [dbo].[SICC_Localidad] ADD CONSTRAINT [FK_SICC_Localidad_SICC_LocalidadPlaza] FOREIGN KEY ([IdLocalidadPlaza]) REFERENCES [dbo].[SICC_LocalidadPlaza] ([IdLocalidadPlaza])
GO
