CREATE TABLE [dbo].[SICCMX_Garantia_Canasta]
(
[IdRel] [bigint] NOT NULL IDENTITY(1, 1),
[IdCredito] [bigint] NOT NULL,
[IdTipoGarantia] [int] NULL,
[He] [decimal] (18, 10) NULL,
[Hc] [decimal] (18, 10) NULL,
[Hfx] [decimal] (18, 10) NULL,
[C] [decimal] (23, 2) NULL,
[PrctCobSinAju] [decimal] (18, 10) NULL,
[PrctCobAjust] [decimal] (18, 10) NULL,
[MontoCobAjust] [decimal] (25, 2) NULL,
[PI] [decimal] (18, 10) NULL,
[EI_Ajust] [decimal] (25, 4) NULL,
[SeveridadCorresp] [decimal] (18, 12) NULL,
[MontoRecuperacion] [decimal] (25, 4) NULL,
[Reserva] [decimal] (27, 6) NULL,
[EsDescubierto] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Garantia_Canasta] ADD CONSTRAINT [PK_SICCMX_Garantia_Canasta] PRIMARY KEY CLUSTERED  ([IdRel]) ON [PRIMARY]
GO
