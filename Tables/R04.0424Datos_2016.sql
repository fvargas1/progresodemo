CREATE TABLE [R04].[0424Datos_2016]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionHistorica] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SituacionActual] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_SaldoHistorico] DEFAULT ((0)),
[InteresVigenteHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_InteresVigenteHistorico] DEFAULT ((0)),
[InteresVencidoHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_InteresVencidoHistorico] DEFAULT ((0)),
[IntCarVencHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_IntCarVencHistorico] DEFAULT ((0)),
[IntDevengadoHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_IntDevengadoHistorico] DEFAULT ((0)),
[SaldoActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_SaldoActual] DEFAULT ((0)),
[InteresVigente] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_InteresVigente] DEFAULT ((0)),
[InteresVencido] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_InteresVencido] DEFAULT ((0)),
[IntCarVenc] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_IntCarVenc] DEFAULT ((0)),
[IntDevengado] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0424Datos_2016_IntDevengado] DEFAULT ((0)),
[TipoAlta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjusteCambiario] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0424Datos_2016] ADD CONSTRAINT [PK_0424Datos_2016] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
