CREATE TABLE [dbo].[SICCMX_PI_Variables_Validacion]
(
[IdVariable] [int] NOT NULL,
[CampoValidar] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AntiguedadValidar] [int] NULL,
[Activo] [bit] NULL,
[ValorDefault] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AntiguedadNoReg] [int] NULL
) ON [PRIMARY]
GO
