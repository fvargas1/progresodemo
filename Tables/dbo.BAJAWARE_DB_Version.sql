CREATE TABLE [dbo].[BAJAWARE_DB_Version]
(
[IdVersion] [int] NOT NULL IDENTITY(1, 1),
[Version] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FechaInicio] [datetime] NULL,
[FechaFin] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_DB_Version] ADD CONSTRAINT [PK_BAJAWARE_DB_Version] PRIMARY KEY CLUSTERED  ([IdVersion]) ON [PRIMARY]
GO
