CREATE TABLE [Historico].[RW_R04A0420_2016]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubReporte] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoDeCartera] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoDeSaldo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
