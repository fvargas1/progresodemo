CREATE TABLE [dbo].[MIGRACION_Carga_Errores]
(
[Codename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoError] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
