CREATE TABLE [Historico].[SICCMX_Consumo_Reservas_VariablesPreliminares]
(
[IdPeriodoHistorico] [int] NULL,
[Consumo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Metodologia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroIntegrantes] [int] NULL,
[Ciclos] [decimal] (18, 10) NULL,
[LimiteCredito] [decimal] (23, 2) NULL,
[SaldoFavor] [decimal] (23, 2) NULL,
[PorcentajePagoRealizado] [decimal] (18, 10) NULL,
[ATR] [decimal] (18, 10) NULL,
[MaxATR] [decimal] (18, 10) NULL,
[INDATR] [bit] NULL,
[VECES] [decimal] (18, 10) NULL,
[Impago] [decimal] (18, 10) NULL,
[NumeroImpagosConsecutivos] [decimal] (18, 10) NULL,
[NumeroImpagosHistoricos] [decimal] (18, 10) NULL,
[MesesTranscurridos] [decimal] (18, 10) NULL,
[PorPago] [decimal] (18, 10) NULL,
[PorSDOIMP] [decimal] (18, 10) NULL,
[PorPR] [decimal] (18, 10) NULL,
[PorcentajeUso] [decimal] (18, 10) NULL,
[TarjetaActiva] [bit] NULL,
[Alto] [int] NULL,
[Medio] [int] NULL,
[Bajo] [int] NULL,
[Zi] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
