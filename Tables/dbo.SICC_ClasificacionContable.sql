CREATE TABLE [dbo].[SICC_ClasificacionContable]
(
[IdClasificacionContable] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdClasificacionGrupo] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_ClasificacionContable] ADD CONSTRAINT [PK_SICCMX_ClasificacionGrupo] PRIMARY KEY CLUSTERED  ([IdClasificacionContable]) ON [PRIMARY]
GO
