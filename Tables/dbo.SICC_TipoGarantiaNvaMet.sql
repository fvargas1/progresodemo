CREATE TABLE [dbo].[SICC_TipoGarantiaNvaMet]
(
[IdGarantiaNvaMet] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoGarantiaNvaMet] ADD CONSTRAINT [PK_SICC_TipoGarantiaNvaMet] PRIMARY KEY CLUSTERED  ([IdGarantiaNvaMet]) ON [PRIMARY]
GO
