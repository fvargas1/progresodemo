CREATE TABLE [dbo].[SICC_ReestructuraConsumo]
(
[IdReestructuraConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoBanxico] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClasificacionReporte] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_ReestructuraConsumo] ADD CONSTRAINT [PK_SICC_ReestructuraConsumo] PRIMARY KEY CLUSTERED  ([IdReestructuraConsumo]) ON [PRIMARY]
GO
