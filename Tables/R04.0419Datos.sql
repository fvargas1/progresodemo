CREATE TABLE [R04].[0419Datos]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalifCubHistorico] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalifExpHistorico] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoReservaExpHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoReservaExpHistorico] DEFAULT ((0)),
[SaldoReservaCubHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoReservaCubHistorico] DEFAULT ((0)),
[SaldoHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoHistorico] DEFAULT ((0)),
[InteresCarteraVencidaHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_InteresCarteraVencidaHistorico] DEFAULT ((0)),
[ReservasAdicOrdenadasCNBVHistorico] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_ReservasAdicOrdenadasCNBVHistorico] DEFAULT ((0)),
[CalifCubActual] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CalifExpActual] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoReservaExpActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoReservaExpActual] DEFAULT ((0)),
[SaldoReservaCubActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoReservaCubActual] DEFAULT ((0)),
[SaldoActual] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_SaldoActual] DEFAULT ((0)),
[InteresCarteraVencida] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_InteresCarteraVencida] DEFAULT ((0)),
[ReservasAdicOrdenadasCNBV] [decimal] (23, 2) NOT NULL CONSTRAINT [DF_0419Datos_ReservasAdicOrdenadasCNBV] DEFAULT ((0)),
[IdTipoBaja] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0419Datos] ADD CONSTRAINT [PK_0419Datos] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
