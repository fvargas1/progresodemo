CREATE TABLE [dbo].[FILE_Garantia_Hipotecario]
(
[CodigoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descripcion] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[errorFormato] [bit] NULL,
[errorCatalogo] [bit] NULL,
[Homologado] [bit] NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
