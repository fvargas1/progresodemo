CREATE TABLE [dbo].[BAJAWARE_Config]
(
[IdConfig] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_Config] ADD CONSTRAINT [PK_BAJAWARE_Config] PRIMARY KEY CLUSTERED  ([IdConfig]) ON [PRIMARY]
GO
