CREATE TABLE [dbo].[SICC_Conceptos_A]
(
[IdConcepto] [int] NOT NULL IDENTITY(1, 1),
[Concepto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubReporte] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCartera] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoSaldo] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Orden] [int] NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Conceptos_A] ADD CONSTRAINT [PK_SICC_Conceptos_A] PRIMARY KEY CLUSTERED  ([IdConcepto]) ON [PRIMARY]
GO
