CREATE TABLE [Historico].[SICCMX_CreditoGarantia]
(
[IdPeriodoHistorico] [int] NOT NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PorcCubiertoCredito] [decimal] (18, 10) NULL,
[MontoCubiertoCredito] [decimal] (23, 2) NULL,
[PorcUsadoGarantia] [decimal] (18, 10) NULL,
[MontoUsadoGarantia] [decimal] (23, 2) NULL,
[Aplica] [bit] NULL,
[PorcBanco] [decimal] (18, 10) NULL,
[MontoBanco] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
