CREATE TABLE [dbo].[RW_R04A_Consolidado]
(
[IdReporteLog] [bigint] NOT NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubReporte] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Moneda] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoDeCartera] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoDeSaldo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
