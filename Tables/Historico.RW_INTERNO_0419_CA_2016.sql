CREATE TABLE [Historico].[RW_INTERNO_0419_CA_2016]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Producto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cargos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Abonos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaActual] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservaActualCalif] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Diferencia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
