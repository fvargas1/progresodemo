CREATE TABLE [dbo].[SICC_ActividadEconomica]
(
[IdActividadEconomica] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A18] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A20] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A21] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A22] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A21OD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[A22OD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL,
[CodigoRR] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdSector] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_ActividadEconomica] ADD CONSTRAINT [PK_SICC_ActividadEconomica] PRIMARY KEY CLUSTERED  ([IdActividadEconomica]) ON [PRIMARY]
GO
