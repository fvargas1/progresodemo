CREATE TABLE [dbo].[FILE_TipoCambio_errores]
(
[identificador] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nombreCampo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[valor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tipoError] [int] NOT NULL,
[description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
