CREATE TABLE [dbo].[SICCMX_Persona_PI_GP]
(
[IdGP] [bigint] NOT NULL,
[EsGarante] [int] NOT NULL,
[FactorCuantitativo] [decimal] (10, 6) NULL,
[PonderadoCuantitativo] [decimal] (10, 6) NULL,
[FactorCualitativo] [decimal] (10, 6) NULL,
[PonderadoCualitativo] [decimal] (10, 6) NULL,
[FactorTotal] [decimal] (10, 6) NULL,
[PI] [decimal] (18, 12) NULL,
[IdMetodologia] [int] NULL,
[IdClasificacion] [int] NULL,
[Comentarios] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Persona_PI_GP] ADD CONSTRAINT [PK_SICCMX_Persona_PI_GP] PRIMARY KEY CLUSTERED  ([IdGP], [EsGarante]) ON [PRIMARY]
GO
