CREATE TABLE [Historico].[RW_R04C0472_2016]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPrincipalInicio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoInsoluto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCastigos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCondonacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoQuitas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBonificaciones] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoDescuentos] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBienDacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasCalifCanceladas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicCanceladas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
