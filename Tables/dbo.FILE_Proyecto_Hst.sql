CREATE TABLE [dbo].[FILE_Proyecto_Hst]
(
[CodigoCliente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProyecto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreDelProyecto] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DescripciondelProyecto] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sobrecosto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoCubiertoPorTerceros] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesContemplados] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesAdicionales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VPTotal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UtilidadPerdidaAcumulada] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Etapa] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaInicioOper] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TasaDescuentoVP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VentNetTotAnuales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fuente] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
