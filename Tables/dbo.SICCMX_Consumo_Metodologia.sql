CREATE TABLE [dbo].[SICCMX_Consumo_Metodologia]
(
[IdMetodologiaConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Consumo_Metodologia] ADD CONSTRAINT [PK_SICCMX_Consumo_Metodologia] PRIMARY KEY CLUSTERED  ([IdMetodologiaConsumo]) ON [PRIMARY]
GO
