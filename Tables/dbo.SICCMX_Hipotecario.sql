CREATE TABLE [dbo].[SICCMX_Hipotecario]
(
[IdHipotecario] [bigint] NOT NULL IDENTITY(1, 1),
[IdPersona] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoCapitalVigente] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[SaldoCapitalVencido] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[MontoGarantia] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[IdTipoCreditoR04A] [int] NULL,
[IdEntidadCobertura] [int] NULL,
[IdTipoCreditoR04A_2016] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Hipotecario] ADD CONSTRAINT [PK_SICCMX_Hipotecario] PRIMARY KEY CLUSTERED  ([IdHipotecario]) ON [PRIMARY]
GO
