CREATE TABLE [R04].[0420Configuracion]
(
[IdConfiguracion] [int] NOT NULL IDENTITY(1, 1),
[TipoMovimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Concepto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoProducto] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [R04].[0420Configuracion] ADD CONSTRAINT [PK_0420Configuracion] PRIMARY KEY CLUSTERED  ([IdConfiguracion]) ON [PRIMARY]
GO
