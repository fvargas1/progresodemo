CREATE TABLE [dbo].[SICCMX_PersonaCalificacion]
(
[IdPersona] [bigint] NOT NULL,
[EI] [decimal] (23, 2) NULL,
[Reserva] [decimal] (23, 2) NULL,
[PrctReserva] [decimal] (18, 10) NULL,
[IdCalificacion] [int] NULL,
[EI_Consumo] [decimal] (23, 2) NULL,
[Reserva_Consumo] [decimal] (23, 2) NULL,
[PrctReserva_Consumo] [decimal] (18, 10) NULL,
[IdCalificacion_Consumo] [int] NULL,
[EI_Hipotecario] [decimal] (23, 2) NULL,
[Reserva_Hipotecario] [decimal] (23, 2) NULL,
[PrctReserva_Hipotecario] [decimal] (18, 10) NULL,
[IdCalificacion_Hipotecario] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PersonaCalificacion] ADD CONSTRAINT [PK_SICCMX_PersonaCalificacion] PRIMARY KEY CLUSTERED  ([IdPersona]) ON [PRIMARY]
GO
