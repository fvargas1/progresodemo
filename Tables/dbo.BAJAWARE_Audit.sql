CREATE TABLE [dbo].[BAJAWARE_Audit]
(
[IdAudit] [bigint] NOT NULL IDENTITY(1, 1),
[Object] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Field] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObjectId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldValue] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewValue] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL,
[Summary] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_Audit] ADD CONSTRAINT [PK_BAJAWARE_Audit] PRIMARY KEY CLUSTERED  ([IdAudit]) ON [PRIMARY]
GO
