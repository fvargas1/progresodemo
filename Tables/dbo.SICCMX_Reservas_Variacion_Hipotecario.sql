CREATE TABLE [dbo].[SICCMX_Reservas_Variacion_Hipotecario]
(
[IdHipotecario] [bigint] NOT NULL,
[ReservaCalConsDesc] [decimal] (23, 2) NULL,
[ReservaAdicConsDesc] [decimal] (23, 2) NULL,
[ReservaPeriodoAnt] [decimal] (23, 2) NULL,
[ReservaAdicPeriodoAnt] [decimal] (23, 2) NULL
) ON [PRIMARY]
GO
