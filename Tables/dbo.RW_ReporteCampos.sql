CREATE TABLE [dbo].[RW_ReporteCampos]
(
[IdReporteCampos] [int] NOT NULL IDENTITY(1, 1),
[IdReporte] [int] NOT NULL,
[Codename] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Orden] [int] NOT NULL,
[Afectable] [bit] NOT NULL,
[TipoDato] [int] NOT NULL,
[Longitud] [int] NULL,
[Decimales] [int] NULL,
[Predeterminado] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AplicaFormatoVP] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RW_ReporteCampos] ADD CONSTRAINT [PK_RW_ReporteCampos] PRIMARY KEY CLUSTERED  ([IdReporteCampos]) ON [PRIMARY]
GO
