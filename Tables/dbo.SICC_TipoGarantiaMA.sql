CREATE TABLE [dbo].[SICC_TipoGarantiaMA]
(
[IdTipoGarantia] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_TipoGarantiaMA] ADD CONSTRAINT [PK_SICC_TipoGarantiaMA] PRIMARY KEY CLUSTERED  ([IdTipoGarantia]) ON [PRIMARY]
GO
