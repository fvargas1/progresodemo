CREATE TABLE [dbo].[SICC_SubSector]
(
[IdSubSector] [bigint] NOT NULL,
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdSector] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SubSector] ADD CONSTRAINT [PK_SICC_SubSector] PRIMARY KEY CLUSTERED  ([IdSubSector]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_SubSector] ADD CONSTRAINT [FK_SICC_SubSector_SICC_Sector] FOREIGN KEY ([IdSector]) REFERENCES [dbo].[SICC_Sector] ([IdSector])
GO
