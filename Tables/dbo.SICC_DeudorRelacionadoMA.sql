CREATE TABLE [dbo].[SICC_DeudorRelacionadoMA]
(
[IdDeudorRelacionado] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCNBV] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_DeudorRelacionadoMA] ADD CONSTRAINT [PK_SICC_DeudorRelacionadoMA] PRIMARY KEY CLUSTERED  ([IdDeudorRelacionado]) ON [PRIMARY]
GO
