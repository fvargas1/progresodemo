CREATE TABLE [Historico].[RW_InsumoRC15]
(
[IdPeriodoHistorico] [bigint] NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarantiasReales] [decimal] (23, 2) NULL,
[MontoReservas] [decimal] (23, 2) NULL,
[MontoAvales] [decimal] (23, 2) NULL,
[FechaDisposicion] [datetime] NULL,
[SituacionCredito] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exposicion] [decimal] (23, 2) NULL,
[ImporteRC3] [decimal] (23, 2) NULL,
[ClienteRelacionado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
