CREATE TABLE [dbo].[SICCMX_Rel_Variable_Catalogo]
(
[IdVariable] [int] NOT NULL,
[IdCatalogo] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Rel_Variable_Catalogo] ADD CONSTRAINT [PK_SICCMX_Rel_Variable_Catalogo] PRIMARY KEY CLUSTERED  ([IdVariable], [IdCatalogo]) ON [PRIMARY]
GO
