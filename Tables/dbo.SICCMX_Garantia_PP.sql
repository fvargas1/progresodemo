CREATE TABLE [dbo].[SICCMX_Garantia_PP]
(
[IdGarantia] [bigint] NOT NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[MontoExpuesto] [decimal] (23, 2) NULL,
[PIGarante] [decimal] (18, 12) NULL,
[SPGarante] [decimal] (18, 12) NULL,
[ReservaCubierto] [decimal] (23, 2) NULL,
[ReservaExpuesto] [decimal] (23, 2) NULL,
[ReservaFinal] [decimal] (23, 2) NULL,
[PrctCubierto] [decimal] (18, 12) NULL
) ON [PRIMARY]
GO
