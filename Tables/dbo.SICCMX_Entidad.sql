CREATE TABLE [dbo].[SICCMX_Entidad]
(
[IdEntidad] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdTipoEntidad] [int] NOT NULL,
[IdEntidadPadre] [bigint] NULL,
[IdCalificacion] [bigint] NULL,
[FechaCreacion] [datetime] NULL,
[FechaActualizacion] [datetime] NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Entidad] ADD CONSTRAINT [PK_SICCMX_Entidad] PRIMARY KEY CLUSTERED  ([IdEntidad]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Entidad] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Entidad_SICC_TipoEntidad] FOREIGN KEY ([IdTipoEntidad]) REFERENCES [dbo].[SICC_TipoEntidad] ([IdTipoEntidad])
GO
ALTER TABLE [dbo].[SICCMX_Entidad] ADD CONSTRAINT [FK_SICCMX_Entidad_SICCMX_Entidad] FOREIGN KEY ([IdEntidadPadre]) REFERENCES [dbo].[SICCMX_Entidad] ([IdEntidad])
GO
