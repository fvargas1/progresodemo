CREATE TABLE [Bajaware].[Catalogos]
(
[IdCatalogo] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdAplicacion] [int] NULL,
[Usecase] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Codename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreVista] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [Bajaware].[Catalogos] ADD CONSTRAINT [PK_Catalogos] PRIMARY KEY CLUSTERED  ([IdCatalogo]) ON [PRIMARY]
GO
