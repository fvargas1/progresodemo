CREATE TABLE [dbo].[MIGRACION_Inconsistencia]
(
[IdInconsistencia] [bigint] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodenameCount] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdCategoria] [int] NULL,
[Activo] [bit] NOT NULL,
[Regulatorio] [bit] NOT NULL,
[FechaCreacion] [datetime] NOT NULL,
[FechaActualizacion] [datetime] NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Inconsistencia] ADD CONSTRAINT [PK_MIGRACION_Inconsistencia] PRIMARY KEY CLUSTERED  ([IdInconsistencia]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Inconsistencia] WITH NOCHECK ADD CONSTRAINT [FK_MIGRACION_Inconsistencia_MIGRACION_Categoria] FOREIGN KEY ([IdCategoria]) REFERENCES [dbo].[MIGRACION_Categoria] ([IdCategoria])
GO
