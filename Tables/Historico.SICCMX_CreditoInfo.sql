CREATE TABLE [Historico].[SICCMX_CreditoInfo]
(
[IdPeriodoHistorico] [int] NULL,
[Credito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAlta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AjusteTasaReferencia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBancaDesarrollo] [decimal] (23, 2) NULL,
[InstitucionFondeo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GastosOriginacion] [decimal] (23, 2) NULL,
[NumeroDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoDispuesto] [decimal] (23, 2) NULL,
[MontoPagoExigible] [decimal] (23, 2) NULL,
[MontoPagosRealizados] [decimal] (23, 2) NULL,
[MontoInteresPagado] [decimal] (23, 2) NULL,
[DiasVencidos] [int] NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBonificacion] [decimal] (23, 2) NULL,
[SaldoInicial] [decimal] (23, 2) NULL,
[SaldoFinal] [decimal] (23, 2) NULL,
[PeriodicidadCapital] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadInteres] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoReestructurado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoProductoSerie4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductoComercial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoPromedio] [decimal] (23, 2) NULL,
[InteresDelMes] [decimal] (23, 2) NULL,
[ComisionDelMes] [decimal] (23, 2) NULL,
[Posicion] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoLinea] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExposicionIncumplimiento] [decimal] (23, 2) NULL,
[TipoOperacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecMaxDis] [datetime] NULL,
[PorcPartFederal] [decimal] (23, 2) NULL,
[MontoPagEfeCap] [decimal] (23, 2) NULL,
[MontoPagEfeInt] [decimal] (23, 2) NULL,
[MontoPagEfeCom] [decimal] (23, 2) NULL,
[MontoPagEfeIntMor] [decimal] (23, 2) NULL,
[CredRepSIC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValMercDerivCred] [decimal] (23, 2) NULL,
[ConcursoMercantil] [bit] NULL,
[ComDispTasa] [decimal] (18, 10) NULL,
[ComDispMonto] [decimal] (23, 2) NULL,
[GastosOrigTasa] [decimal] (18, 10) NULL,
[ResTotalInicioPer] [decimal] (23, 2) NULL,
[MontoQuitasCastQue] [decimal] (23, 2) NULL,
[MontoBonificacionDesc] [decimal] (23, 2) NULL,
[NumEmpLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumEmpLocFedSHCP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesGraciaCap] [int] NULL,
[MesesGraciaIntereses] [int] NULL,
[SaldoParaCalculoInteres] [decimal] (23, 2) NULL,
[MontoIntereses] [decimal] (23, 2) NULL,
[FecPagExigibleRealizado] [datetime] NULL,
[FecVenLinea] [datetime] NULL,
[CAT] [decimal] (18, 10) NULL,
[MontoLineaSinA] [decimal] (23, 2) NULL,
[MontoPrimasAnuales] [decimal] (23, 2) NULL,
[Emproblemado] [bit] NULL,
[CumpleCritContGral] [int] NULL,
[EsPadre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MunicipioDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActividadEconomicaDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasCalculoInteres] [int] NULL,
[Categoria] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FolioConsultaBuro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAltaMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisposicionMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadCapitalMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeriodicidadInteresMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBajaMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiasMorosidad] [int] NULL,
[TipoProductoSerie4_2016] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeudorFactoraje] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeudorFactorajeRFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoOtrosPrincipal] [decimal] (23, 2) NULL,
[MontoCondonacion] [decimal] (23, 2) NULL,
[MontoCastigos] [decimal] (23, 2) NULL,
[MontoQuebrantos] [decimal] (23, 2) NULL,
[MontoDescuentos] [decimal] (23, 2) NULL,
[MontoDacion] [decimal] (23, 2) NULL,
[Mitigante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorConversion] [decimal] (18, 10) NULL,
[ExposicionAjuMit] [decimal] (23, 2) NULL,
[ExposicionNeta] [decimal] (23, 2) NULL,
[PonderadorRiesgo] [decimal] (18, 10) NULL,
[ReqCapital] [decimal] (23, 2) NULL,
[ReservasAdicionales] [decimal] (23, 2) NULL,
[LocalidadDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EstadoDestino] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
