CREATE TABLE [dbo].[RW_R04C0471_2016]
(
[IdReporteLog] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroDisposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoInsolutoFinal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctExpuesto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_Expuesto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_SinGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroGarRealFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctGarRealFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[He] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hfx] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorGarRealFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_GarRealFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_GarRealFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroGarRealNoFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctGarRealNoFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorDerechosCobro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorBienesInmuebles] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorBienesMuebles] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorFidPartFed] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorFidIngProp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorOtrasGar] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlujoEstimado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_DerechosCobro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_BienesInmuebles] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_BienesMuebles] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_FidPartFed] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_FidIngProp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_OtrasGar] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_GarRealNoFin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumeroGarPersonales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctGarPersonales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreAval] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoGarPersonal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFCAval] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValuacionDerCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonedaGarPersonal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreGaranteECPM] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreGarantePP] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctECPM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctPP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoECPM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoPP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID_ECPM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID_PP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasCalificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAcreditado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicionales] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasConstDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicConstDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Acreditado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_Acreditado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Acreditado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditoReserva5Prct] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditoSustPI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MesesSP100] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasMI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mitigante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrupoRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FactorConversion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpMitigantes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpNetaReservas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TablaAdeudo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradoRiesgoME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EscalaCalificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgenciaCalificadora] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calificacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PonderadorRiesgo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequerimientoCapital] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnfoqueBasico] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_ReqCap_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_ReqCap_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_ReqCap_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Vencimiento] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Correlacion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ponderador_ReqCap_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReqCap_MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
