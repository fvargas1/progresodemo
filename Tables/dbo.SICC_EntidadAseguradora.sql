CREATE TABLE [dbo].[SICC_EntidadAseguradora]
(
[IdEntidadAseguradora] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCNBV] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_EntidadAseguradora] ADD CONSTRAINT [PK_SICC_EntidadAseguradora] PRIMARY KEY CLUSTERED  ([IdEntidadAseguradora]) ON [PRIMARY]
GO
