CREATE TABLE [dbo].[SICCMX_Anexo18_GP_Preliminares]
(
[IdGP] [bigint] NOT NULL,
[EsGarante] [int] NULL,
[DeudaTotalPartEleg] [decimal] (18, 10) NULL,
[ServicioDeuda] [decimal] (23, 2) NULL,
[ServicioDeudaIngresosTotales] [decimal] (18, 10) NULL,
[DeudaCortoPlazoDeudaTotal] [decimal] (18, 10) NULL,
[IngresosTotalesGastoCorr] [decimal] (18, 10) NULL,
[InvIngresosTotales] [decimal] (18, 10) NULL,
[IngPropiosIngTotales] [decimal] (18, 10) NULL,
[ObligContDerivadasBenef] [decimal] (18, 10) NULL,
[BalanceOperativoPIB] [decimal] (18, 10) NULL,
[NivelEficienciaRec] [decimal] (18, 10) NULL
) ON [PRIMARY]
GO
