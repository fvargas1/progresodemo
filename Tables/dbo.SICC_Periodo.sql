CREATE TABLE [dbo].[SICC_Periodo]
(
[IdPeriodo] [int] NOT NULL IDENTITY(1, 1),
[Fecha] [datetime] NOT NULL,
[Trimestral] [bit] NOT NULL CONSTRAINT [DF_SICC_Periodo_Trimestral] DEFAULT ((0)),
[Activo] [bit] NOT NULL CONSTRAINT [DF_SICC_Periodo_Activo] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_Periodo] ADD CONSTRAINT [PK_SICC_Periodo] PRIMARY KEY CLUSTERED  ([IdPeriodo]) ON [PRIMARY]
GO
