CREATE TABLE [dbo].[MIGRACION_EntidadColumns]
(
[IdEntidadColumns] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdEntidad] [int] NOT NULL,
[NombreCampo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TipoDato] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsRequerido] [bit] NULL,
[IsKey] [bit] NULL,
[IsIdentidadBusqueda] [bit] NULL,
[IdCatalogo] [int] NULL,
[NombreIdCatalogo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_EntidadColumns] ADD CONSTRAINT [PK_MIGRACION_EntidadColumns] PRIMARY KEY CLUSTERED  ([IdEntidadColumns]) ON [PRIMARY]
GO
