CREATE TABLE [dbo].[FILE_TipoCambio]
(
[CodigoMonedaISO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaTipoCambio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValorTipoCambio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[errorCatalogo] [bit] NULL,
[errorFormato] [bit] NULL,
[Homologado] [bit] NULL
) ON [PRIMARY]
GO
