CREATE TABLE [Historico].[SICCMX_Accionista]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Porcentaje] [decimal] (18, 10) NOT NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
