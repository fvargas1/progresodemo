CREATE TABLE [dbo].[MIGRACION_Inicializador]
(
[IdInicializador] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codename] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL,
[FechaInicio] [datetime] NULL,
[FechaFin] [datetime] NULL,
[Estatus] [int] NULL,
[GrupoArchivo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activo] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MIGRACION_Inicializador] ADD CONSTRAINT [PK_MIGRACION_Inicializacion] PRIMARY KEY CLUSTERED  ([IdInicializador]) ON [PRIMARY]
GO
