CREATE TABLE [dbo].[BAJAWARE_UserPasswordIteration]
(
[IdPasswordIteration] [int] NOT NULL IDENTITY(1, 1),
[IdUser] [int] NULL,
[OldPassword] [dbo].[BAJAWARE_utBinary] NULL,
[FechaCambio] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UserPasswordIteration] ADD CONSTRAINT [PK_BAJAWARE_UserPasswordIteration] PRIMARY KEY CLUSTERED  ([IdPasswordIteration]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UserPasswordIteration] ADD CONSTRAINT [FK_BAJAWARE_UserPasswordIteration_BAJAWARE_UserPasswordIteration] FOREIGN KEY ([IdPasswordIteration]) REFERENCES [dbo].[BAJAWARE_UserPasswordIteration] ([IdPasswordIteration])
GO
