CREATE TABLE [dbo].[SICCMX_PersonaInfo]
(
[IdPersona] [bigint] NOT NULL,
[IdTipoPersonaMA] [bigint] NULL,
[IdTipoPersona] [bigint] NULL,
[NumeroEmpleados] [int] NULL,
[IngresosBrutos] [decimal] (23, 2) NULL,
[IdSectorLaboral] [bigint] NULL,
[GrupoRiesgo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdTipoIngreso] [bigint] NULL,
[NombreCNBV] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservasAdicOrdenadasCNBV] [decimal] (23, 2) NULL CONSTRAINT [DF_SICCMX_PersonaInfo_ReservasAdicOrdenadasCNBV] DEFAULT ((0)),
[Estado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Municipio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdNacionalidad] [bigint] NULL,
[CURP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HITSIC] [int] NULL,
[PadreGrupoEconomico] [bit] NULL,
[IdSectorEconomico] [int] NULL,
[IdTamanoDeudor] [int] NULL,
[IdRCGrupoRiesgo] [int] NULL,
[IdRCCalificacion] [int] NULL,
[TopeTamano] [decimal] (9, 1) NULL,
[CalificacionLargoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdAgenciaLargoPlazo] [int] NULL,
[CalificacionCortoPlazo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdAgenciaCortoPlazo] [int] NULL,
[IdRCCalificacionCortoPlazo] [int] NULL,
[CodigoPostal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PersonaInfo] ADD CONSTRAINT [PK_SICCMX_PersonaInfo] PRIMARY KEY CLUSTERED  ([IdPersona]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_PersonaInfo] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_PersonaInfo_SICC_SectorLaboral] FOREIGN KEY ([IdSectorLaboral]) REFERENCES [dbo].[SICC_SectorLaboral] ([IdSectorLaboral])
GO
ALTER TABLE [dbo].[SICCMX_PersonaInfo] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_PersonaInfo_SICC_TipoIngreso] FOREIGN KEY ([IdTipoIngreso]) REFERENCES [dbo].[SICC_TipoIngreso] ([IdTipoIngreso])
GO
ALTER TABLE [dbo].[SICCMX_PersonaInfo] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_PersonaInfo_SICC_TipoPersona] FOREIGN KEY ([IdTipoPersona]) REFERENCES [dbo].[SICC_TipoPersona] ([IdTipoPersona])
GO
ALTER TABLE [dbo].[SICCMX_PersonaInfo] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_PersonaInfo_SICCMX_Persona] FOREIGN KEY ([IdPersona]) REFERENCES [dbo].[SICCMX_Persona] ([IdPersona])
GO
