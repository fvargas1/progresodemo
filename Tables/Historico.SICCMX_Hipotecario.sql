CREATE TABLE [Historico].[SICCMX_Hipotecario]
(
[IdPeriodoHistorico] [int] NULL,
[Hipotecario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Persona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SaldoCapitalVigente] [decimal] (23, 2) NULL,
[InteresVigente] [decimal] (23, 2) NULL,
[SaldoCapitalVencido] [decimal] (23, 2) NULL,
[InteresVencido] [decimal] (23, 2) NULL,
[MontoGarantia] [decimal] (23, 2) NULL,
[InteresCarteraVencida] [decimal] (23, 2) NULL,
[TipoCreditoR04A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntidadCobertura] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCreditoR04A_2016] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
