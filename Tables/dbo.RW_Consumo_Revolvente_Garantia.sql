CREATE TABLE [dbo].[RW_Consumo_Revolvente_Garantia]
(
[IdReporteLog] [bigint] NOT NULL,
[FolioCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Garantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GarantiaReal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImporteGarantia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Garante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SP_SG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hfx] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrctCobertura] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exposicion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EI_Ajustada] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reservas] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaveGarante] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RUGM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdPortafolio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
