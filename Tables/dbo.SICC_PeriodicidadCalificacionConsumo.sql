CREATE TABLE [dbo].[SICC_PeriodicidadCalificacionConsumo]
(
[IdPeriodicidadCalificacionConsumo] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdMetodologia] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICC_PeriodicidadCalificacionConsumo] ADD CONSTRAINT [PK_SICC_PeriodicidadCalificacionConsumo] PRIMARY KEY CLUSTERED  ([IdPeriodicidadCalificacionConsumo]) ON [PRIMARY]
GO
