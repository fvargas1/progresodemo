CREATE TABLE [dbo].[SICC_TipoCredito_RR]
(
[IdTipoCredito] [int] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
