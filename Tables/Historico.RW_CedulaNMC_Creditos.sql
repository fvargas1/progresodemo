CREATE TABLE [Historico].[RW_CedulaNMC_Creditos]
(
[IdPeriodoHistorico] [bigint] NOT NULL,
[CodigoPersona] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumeroLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SaldoTotal] [decimal] (23, 2) NULL,
[MontoCubierto] [decimal] (23, 2) NULL,
[MontoExpuesto] [decimal] (23, 2) NULL,
[EI] [decimal] (23, 2) NULL,
[TipoLinea] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PI_Ponderada] [decimal] (18, 10) NULL,
[SP_Ponderada] [decimal] (18, 10) NULL,
[Reserva] [decimal] (23, 2) NULL,
[PrctReserva] [decimal] (18, 10) NULL,
[Calificacion] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
