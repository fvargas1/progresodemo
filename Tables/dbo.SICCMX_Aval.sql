CREATE TABLE [dbo].[SICCMX_Aval]
(
[IdAval] [bigint] NOT NULL IDENTITY(1, 1),
[Codigo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RFC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Domicilio] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdPersonalidadJuridica] [bigint] NULL,
[IdTipoAval] [int] NULL,
[IdMunAval] [bigint] NULL,
[PIAval] [decimal] (18, 10) NULL,
[IdMoneda] [bigint] NULL,
[Calif_Fitch] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_Moodys] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_SP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_HRRATINGS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Calif_Otras] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IdActividadEconomica] [bigint] NULL,
[LEI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoCobertura] [int] NULL,
[FiguraGarantiza] [int] NULL,
[IdPersona] [bigint] NULL,
[Aplica] [bit] NULL,
[Localidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Estado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoPostal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Aval] ADD CONSTRAINT [PK_SICCMX_Aval] PRIMARY KEY CLUSTERED  ([IdAval]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Aval] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Aval_SICC_ActividadEconomica] FOREIGN KEY ([IdActividadEconomica]) REFERENCES [dbo].[SICC_ActividadEconomica] ([IdActividadEconomica])
GO
ALTER TABLE [dbo].[SICCMX_Aval] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Aval_SICC_TipoAval] FOREIGN KEY ([IdTipoAval]) REFERENCES [dbo].[SICC_TipoGarante] ([IdTipoGarante])
GO
ALTER TABLE [dbo].[SICCMX_Aval] WITH NOCHECK ADD CONSTRAINT [FK_SICCMX_Aval_SICC_TipoPersona] FOREIGN KEY ([IdPersonalidadJuridica]) REFERENCES [dbo].[SICC_TipoPersona] ([IdTipoPersona])
GO
