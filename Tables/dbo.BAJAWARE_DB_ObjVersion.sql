CREATE TABLE [dbo].[BAJAWARE_DB_ObjVersion]
(
[IdObjVersion] [int] NOT NULL IDENTITY(1, 1),
[IdVersion] [int] NOT NULL,
[TipoActualizacion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Objeto] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_DB_ObjVersion] ADD CONSTRAINT [PK_BAJAWARE_DB_ObjVersion] PRIMARY KEY CLUSTERED  ([IdObjVersion]) ON [PRIMARY]
GO
