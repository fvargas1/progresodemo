CREATE TABLE [dbo].[RW_R04C0482]
(
[IdReporteLog] [bigint] NOT NULL,
[Periodo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Entidad] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Formulario] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCreditoCNBV] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCredito] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombrePersona] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoBaja] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaldoInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponsabilidadInicial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoPagado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoQuitCastReest] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MontoBonificaciones] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
