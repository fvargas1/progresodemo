CREATE TABLE [dbo].[SICCMX_Accionista]
(
[IdAccionista] [bigint] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Porcentaje] [decimal] (18, 10) NOT NULL,
[IdPersona] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SICCMX_Accionista] ADD CONSTRAINT [PK_SICCMX_Accionista] PRIMARY KEY CLUSTERED  ([IdAccionista]) ON [PRIMARY]
GO
