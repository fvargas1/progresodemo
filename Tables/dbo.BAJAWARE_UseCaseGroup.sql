CREATE TABLE [dbo].[BAJAWARE_UseCaseGroup]
(
[IdUseCaseGroup] [dbo].[BAJAWARE_utID] NOT NULL IDENTITY(1, 1),
[Active] [dbo].[BAJAWARE_utActivo] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdApplication] [dbo].[BAJAWARE_utID] NOT NULL,
[SortOrder] [dbo].[BAJAWARE_utInt] NOT NULL,
[StringKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAJAWARE_UseCaseGroup] ADD CONSTRAINT [PK_BAJAWARE_UseCaseGroup] PRIMARY KEY CLUSTERED  ([IdUseCaseGroup]) ON [PRIMARY]
GO
