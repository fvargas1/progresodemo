SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona_PI]
AS
SELECT 
 Persona_PI.IdPersona,
 Persona_PI.FactorCuantitativo,
 Persona_PI.PonderadoCuantitativo,
 Persona_PI.FactorCualitativo,
 Persona_PI.PonderadoCualitativo,
 Persona_PI.FactorTotal,
 Persona_PI.PI,
 Persona_PI.IdMetodologia,
 idmetodologia.Nombre AS IdMetodologiaNombre,
 val.FactorCuantitativo AS PrctCuantitativo,
 val.FactorCualitativo AS PrctCualitativo
FROM dbo.SICCMX_Persona_PI AS Persona_PI
INNER JOIN dbo.SICCMX_Metodologia idmetodologia ON Persona_PI.IdMetodologia = idmetodologia.IdMetodologia
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = Persona_PI.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(Persona_PI.IdClasificacion, '');
GO
