SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Entidad]
AS
SELECT
	Entidad.IdEntidad,
	Entidad.Nombre,
	Entidad.Descripcion,
	Entidad.NombreView
FROM dbo.MIGRACION_Entidad AS Entidad;
GO
