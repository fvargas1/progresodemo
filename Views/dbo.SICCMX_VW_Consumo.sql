SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[SICCMX_VW_Consumo]  
AS  
SELECT  
 con.IdConsumo,  
 con.Codigo,  
 con.IdPersona,  
 per.Codigo AS CodigoPersona,  
 per.Nombre AS IdPersonaNombre,  
 con.SaldoCapitalVigente,  
 CAST(con.SaldoCapitalVigente * tc.Valor AS DECIMAL(23,2)) AS SaldoCapitalVigenteValorizado,  
 con.InteresVigente,  
 CAST(con.InteresVigente * tc.Valor AS DECIMAL(23,2)) AS InteresVigenteValorizado,  
 con.SaldoCapitalVencido,  
 CAST(con.SaldoCapitalVencido * tc.Valor AS DECIMAL(23,2)) AS SaldoCapitalVencidoValorizado,  
 con.InteresVencido,  
 CAST(con.InteresVencido * tc.Valor AS DECIMAL(23,2)) AS InteresVencidoValorizado,  
 con.InteresCarteraVencida,  
 CAST(con.InteresCarteraVencida * tc.Valor AS DECIMAL(23,2)) AS InteresCarteraVencidaValorizado,  
 con.Producto AS IdProductoNombre,  
 con.IdReestructura,  
 con.TasaInteres,  
 con.IdSituacionCredito,  
 con.IdPeriodicidadCapital,  
 con.IdTipoCredito,  
 tpoCre.Nombre AS IdTipoCreditoNombre,  
 con.EsRevolvente AS Revolvente,  
 CAST(con.SaldoCapitalVigente + con.InteresVigente + con.SaldoCapitalVencido + con.InteresVencido AS DECIMAL(23,2)) AS SaldoTotal,  
 CAST((con.SaldoCapitalVigente * tc.Valor) + (con.InteresVigente * tc.Valor) + (con.SaldoCapitalVencido * tc.Valor) + (con.InteresVencido * tc.Valor) AS DECIMAL(23,2)) AS SaldoTotalValorizado,  
 CAST(CAST(con.SaldoCapitalVigente*tc.Valor AS DECIMAL)+CAST(con.InteresVigente*tc.Valor AS DECIMAL)+CAST(con.SaldoCapitalVencido*tc.Valor AS DECIMAL)+CAST(con.InteresVencido*tc.Valor AS DECIMAL) AS DECIMAL(23,2)) AS SaldoTotalValorizadoRound,  
 0 AS PeriodosIncumplimiento,  
 con.IdPeriodicidadCalificacion AS Periodicidad,  
 sit.Codigo AS SituacionCreditoCodigo,  
 sit.Nombre AS IdSituacionCreditoNombre,  
 reest.Nombre AS IdReestructuraNombre,  
 info.IdMoneda,
 info.FechaVencimiento  
FROM dbo.SICCMX_Consumo con  
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo  
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona  
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON con.IdSituacionCredito = sit.IdSituacionConsumo  
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo reest ON con.IdReestructura = reest.IdReestructuraConsumo  
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tpoCre ON con.IdTipoCredito = tpoCre.IdTipoCreditoConsumo  
LEFT OUTER JOIN dbo.SICC_TipoCambio tc ON info.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = ( SELECT IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1 );  
GO
