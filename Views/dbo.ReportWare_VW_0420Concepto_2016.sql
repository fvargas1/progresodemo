SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[ReportWare_VW_0420Concepto_2016]
AS
SELECT item.IdItemCatalogo, item.Codigo, item.Nombre, item.Padre, item.Orden
FROM Bajaware.ItemCatalogo item
INNER JOIN Bajaware.Catalogos cat ON cat.IdCatalogo = item.IdCatalogo
WHERE cat.Codename = 'RW_0420Concepto_2016';
GO
