SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_0419_TotalesCargos]
AS
SELECT tb.Codigo, SUM(tb.Monto) AS Monto
FROM (
	SELECT cre.Codigo, mov.Monto AS Monto
	FROM dbo.SICCMX_Credito_Movimientos mov
	INNER JOIN dbo.SICCMX_Credito cre ON mov.IdCredito = cre.IdCredito
	INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = cre.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
	WHERE conf.CargoAbono = 'C'
	UNION ALL
	SELECT cre.Codigo, dat.AjusteCambiario
	FROM dbo.SICCMX_Credito cre
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = cre.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCAMBCARGO'
	WHERE conf.CargoAbono = 'C' AND dat.AjusteCambiario > 0

	UNION ALL
	SELECT con.Codigo, mov.Monto AS Monto
	FROM dbo.SICCMX_Consumo_Movimientos mov
	INNER JOIN dbo.SICCMX_Consumo con ON mov.IdConsumo = con.IdConsumo
	INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = con.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
	WHERE conf.CargoAbono = 'C'
	UNION ALL
	SELECT con.Codigo, dat.AjusteCambiario
	FROM dbo.SICCMX_Consumo con
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = con.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCAMBCARGO'
	WHERE conf.CargoAbono = 'C' AND dat.AjusteCambiario > 0

	UNION ALL
	SELECT hip.Codigo, mov.Monto AS Monto
	FROM dbo.SICCMX_Hipotecario_Movimientos mov
	INNER JOIN dbo.SICCMX_Hipotecario hip ON mov.IdHipotecario = hip.IdHipotecario
	INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = hip.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
	WHERE conf.CargoAbono = 'C'
	UNION ALL
	SELECT hip.Codigo, dat.AjusteCambiario
	FROM dbo.SICCMX_Hipotecario hip
	INNER JOIN R04.[0419Datos_2016] dat ON dat.Codigo = hip.Codigo
	INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'AJCAMBCARGO'
	WHERE conf.CargoAbono = 'C' AND dat.AjusteCambiario > 0
) AS tb
GROUP BY tb.Codigo
GO
