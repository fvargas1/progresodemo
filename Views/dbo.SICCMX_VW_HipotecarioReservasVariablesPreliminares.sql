SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioReservasVariablesPreliminares]
AS
SELECT
 pre.IdHipotecario,
 pre.IdMetodologia,
 met.Nombre AS IdMetodologiaNombre,
 met.Codigo AS IdMetodologiaCodigo,
 MET.Descripcion AS IdMetodologiaDesc,
 pre.ATR,
 pre.MAXATR,
 pre.PorPago,
 pre.PorCLTVi,
 pre.INTEXP,
 pre.MON,
 pre.ValorVivienda,
 pre.TRi,
 pre.SUBCVi,
 pre.SDESi,
 pre.PorCOBPAMED,
 pre.PorCOBPP,
 NULL AS PorCOBEC,
 pre.DiasAtraso,
 info.ATR AS ATR_Info,
 info.MaxATR AS MAXATR_Info,
 info.PorPago AS PorPago_Info,
 info.PorCLTVi AS PorCLTV_Info,
 info.MON AS MON_Info,
 info.IdMonedaNombre AS MonedaNombre,
 pre.Veces,
 info.PPagoIM AS PPagoIM_Info,
 pre.PPagoIM,
 info.TRi AS TRi_Info,
 pre.Estado,
 pre.IdConvenio,
 pre.RAi,
 info.RAi AS RAi_Info,
 pre.PromedioDeRet,
 info.PromedioDeRet AS PromedioDeRet_Info
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_VW_HipotecarioInfo info ON pre.IdHipotecario = info.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario;
GO
