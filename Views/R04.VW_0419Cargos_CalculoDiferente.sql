SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [R04].[VW_0419Cargos_CalculoDiferente]
AS
-- Creditos que no cambiaron y que la diferencia es por tipo de cambio
SELECT dat.Codigo
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenActual ra ON ra.Codigo = dat.Codigo
INNER JOIN dbo.SICC_VW_CreditoReservasMonedaOrigenHistorico rh ON rh.Codigo = dat.Codigo
WHERE rh.SaldoReservaCubHistorico + rh.SaldoReservaExpHistorico = ra.SaldoReservaCubActual + ra.SaldoReservaExpActual

-- Creditos con movimietos CAST se calcularan diferente
UNION ALL
SELECT Codigo FROM dbo.RW_VW_R04AComercialCastigados
UNION ALL
SELECT Codigo FROM dbo.RW_VW_R04AConsumoCastigados 
UNION ALL
SELECT Codigo FROM dbo.RW_VW_R04AHipotecarioCastigados
UNION ALL

/*Creditos que en el periodo anterior tenian una calif y actualmente dos, y una de ellas se ha mantenido igual. Se calculan diferente.*/
SELECT dat.Codigo
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.CalifExpHistorico = dat.CalifExpActual AND dat.SaldoReservaExpHistorico - dat.SaldoReservaExpActual > 0
UNION ALL

/*Creditos que tienen actualmente dos calificaciones, y la que existe desde el periodo anterior cambio. Se calcula diferente*/
SELECT dat.Codigo
FROM R04.[0419Datos] dat
INNER JOIN dbo.RW_VW_A0419_CalifHistorico_NA calif ON calif.Codigo = dat.Codigo
INNER JOIN R04.[0419Configuracion] conf ON dat.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento IS NULL
WHERE dat.CalifExpHistorico <> dat.CalifExpActual AND dat.CalifCubHistorico IS NULL AND dat.CalifCubActual IS NOT NULL AND dat.SaldoReservaExpHistorico - dat.SaldoReservaExpActual > 0;
GO
