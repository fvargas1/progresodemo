SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415ME_2016_VW]
AS
SELECT
 concepto.Codigo,
 concepto.Nombre AS Concepto,
 concepto.Padre,
 CASE WHEN rep.Moneda = '2' AND rep.TipoDeSaldo = '4' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS InteresesPagados,
 CASE WHEN rep.Moneda = '2' AND rep.TipoDeSaldo = '5' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS Comisiones,
 CASE WHEN rep.Moneda = '2' AND rep.TipoDeSaldo = '6' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS SaldoPromedioDiario,
 CASE WHEN rep.Moneda = '4' AND rep.TipoDeSaldo = '4' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS InteresesPagadosValorizados,
 CASE WHEN rep.Moneda = '4' AND rep.TipoDeSaldo = '5' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS ComisionesValorizados,
 CASE WHEN rep.Moneda = '4' AND rep.TipoDeSaldo = '6' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS SaldoPromedioDiarioValorizados
FROM dbo.ReportWare_VW_0415Concepto_2016 concepto
LEFT OUTER JOIN dbo.RW_R04A0415ME_2016 rep ON concepto.Codigo = rep.Concepto;
GO
