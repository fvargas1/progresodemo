SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_ProcesoCalificacion]
AS
SELECT
	pc.IdProcesoCalificacion,
	pc.Nombre,
	pc.Descripcion,
	pc.Codename,
	pc.IdMetodologia,
	met.Nombre AS MetodologiaNombre,
	pc.Position,
	pc.Activo,
	gpo.IdGrupoProceso IdGrupoProceso,
	gpo.Nombre GrupoProcesoNombre,
	pc.Params
FROM dbo.SICC_ProcesoCalificacion pc
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON pc.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_GrupoProceso gpo ON gpo.IdGrupoProceso = pc.IdGrupoProceso;
GO
