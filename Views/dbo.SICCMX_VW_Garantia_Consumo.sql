SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantia_Consumo]
AS
SELECT
	gar.IdGarantia,
	gar.Codigo,
	gar.IdTipoGarantia,
	tg.Codigo AS TipoGarantia,
	tg.IdClasificacionNvaMet,
	tg.SortOrder,
	gar.ValorGarantia,
	CAST(gar.ValorGarantia * tc.Valor AS DECIMAL(23,2)) AS ValorGarantiaValorizado,
	gar.IdMoneda,
	mon.Codigo AS CodigoMoneda,
	gar.FechaValuacion,
	gar.Descripcion,
	gar.IdBancoGarantia,
	gar.ValorGarantiaProyectado,
	CAST(gar.ValorGarantiaProyectado * tc.Valor AS DECIMAL(23,2)) AS ValorGarantiaProyectadoValorizado,
	gar.Hc,
	gar.VencimientoRestante,
	gar.IdGradoRiesgo,
	gar.IdAgenciaCalificadora,
	gar.Calificacion,
	gar.IdEmisor,
	gar.IdEscala,
	gar.EsIPC,
	gar.PIGarante,
	gar.IdPersona,
	gar.IndPerMorales,
	gar.Aplica
FROM dbo.SICCMX_Garantia_Consumo gar
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoCambio tc ON gar.IdMoneda = tc.IdMoneda AND tc.IdPeriodo = ( SELECT IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1 );
GO
