SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Lineas_Bajas]
AS
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0457_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
UNION ALL
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0462_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
UNION ALL
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0467_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
UNION ALL
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0472_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
UNION ALL
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0477_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
UNION ALL
SELECT r.IdPeriodoHistorico, r.CodigoCredito
FROM Historico.RW_R04C0482_2016 r
INNER JOIN dbo.SICC_PeriodoHistorico p ON r.IdPeriodoHistorico = p.IdPeriodoHistorico AND p.Activo = 1
GO
