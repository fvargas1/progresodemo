SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCredito_Grupal_2016]
AS
SELECT
 lg.IdLineaCredito,
 ISNULL(STUFF((SELECT DISTINCT '@' + CNBV FROM dbo.SICCMX_VW_Reest_CNBV_Grupal WHERE IdLineaCredito = lin.IdLineaCredito FOR XML PATH ('')), 1, 1, ''), lg.GrupalCNBV) AS GrupalCNBV
FROM dbo.SICCMX_VW_LineaCredito_Grupal lg
INNER JOIN dbo.SICCMX_LineaCredito lin ON lg.IdLineaCredito = lin.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta AND ta.Codigo = '140';
GO
