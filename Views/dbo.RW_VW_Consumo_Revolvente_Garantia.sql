SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Consumo_Revolvente_Garantia]
AS
SELECT DISTINCT
	FolioCredito,
	Garantia,
	GarantiaReal,
	ImporteGarantia,
	PI_Garante,
	SP,
	SP_SG,
	Hc,
	Hfx,
	PrctCobertura,
	Exposicion,
	EI_Ajustada,
	Reservas,
	ClaveGarante,
	RUGM,
	IdPortafolio,
	ISNULL(FolioCredito,'')+'|'+ISNULL(ClaveGarante,'')+'|'+ ISNULL(RUGM,'')+'|'+ISNULL(IdPortafolio,'') AS EVERYTHING
FROM dbo.RW_Consumo_Revolvente_Garantia;
GO
