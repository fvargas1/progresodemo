SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Persona_CredRepSIC]
AS
SELECT
	cre.IdPersona,
	MAX(crs.CodigoCNBV) AS CredRepSICCNBV,
	MAX(ccc.CodigoCNBV) AS CumpleCritCNBV
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
LEFT OUTER JOIN dbo.SICC_CredRepSIC crs ON inf.CredRepSIC = crs.IdCredRepSIC
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral ccc ON inf.CumpleCritContGral = ccc.IdCumpleCritContGral
GROUP BY cre.IdPersona;
GO
