SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0417MN_VW]
AS
SELECT
	concepto.Codigo,
	concepto.Nombre AS Concepto,
	concepto.Padre,
	CASE WHEN rep.TipoDeCartera = '9' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END AS SaldoBase,
	CASE WHEN rep.TipoDeCartera = '10' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END AS Estimacion
FROM dbo.ReportWare_VW_0417Concepto concepto
LEFT OUTER JOIN dbo.RW_R04A0417MN rep ON rep.Concepto = concepto.Codigo;
GO
