SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Saldos_RR]
AS
SELECT
	tmp.IdPersona,
	tmp.TipoCreditoRR,
	SUM(ISNULL(tmp.Saldo,0)) / 1000 AS Saldo,
	tmp.IdMoneda
FROM (
	SELECT vwCre.IdPersona, map.TipoCreditoRR, vwCre.SaldoVigenteValorizado AS Saldo, vwCre.IdMoneda
	FROM dbo.SICCMX_VW_Credito_NMC vwCre
	INNER JOIN dbo.SICCMX_CreditoInfo info ON vwCre.IdCredito = info.IdCredito
	LEFT OUTER JOIN SICC_TipoProductoSerie4 tpoS4 ON info.IdTipoProductoSerie4 = tpoS4.IdTipoProducto
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON tpoS4.Codigo = map.TipoCreditoR04A AND map.IdMetodologia = 1 AND map.TipoSaldo = 1
	WHERE vwCre.SaldoVigenteValorizado > 0
	UNION ALL
	SELECT vwCre.IdPersona, map.TipoCreditoRR, vwCre.InteresVigente, vwCre.IdMoneda
	FROM dbo.SICCMX_VW_Credito_NMC vwCre
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 2 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCre.InteresVigente > 0
	UNION ALL
	SELECT vwCre.IdPersona, map.TipoCreditoRR, vwCre.SaldoVencidoValorizado, vwCre.IdMoneda
	FROM dbo.SICCMX_VW_Credito_NMC vwCre
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 3 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCre.SaldoVencidoValorizado > 0
	UNION ALL
	SELECT vwCre.IdPersona, map.TipoCreditoRR, vwCre.InteresVencidoValorizado, vwCre.IdMoneda
	FROM dbo.SICCMX_VW_Credito_NMC vwCre
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 4 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCre.InteresVencidoValorizado > 0
	UNION ALL
	SELECT vwCon.IdPersona, map.TipoCreditoRR, vwCon.SaldoCapitalVigenteValorizado, conInfo.IdMoneda
	FROM dbo.SICCMX_VW_Consumo vwCon
	INNER JOIN dbo.SICCMX_ConsumoInfo conInfo ON vwCon.IdConsumo = conInfo.IdConsumo
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 1 AND map.IdMetodologia = 3 AND map.TipoCreditoR04A IS NULL
	WHERE vwCon.SaldoCapitalVigenteValorizado > 0
	UNION ALL
	SELECT vwCon.IdPersona, map.TipoCreditoRR, vwCon.InteresVigenteValorizado, conInfo.IdMoneda
	FROM dbo.SICCMX_VW_Consumo vwCon
	INNER JOIN dbo.SICCMX_ConsumoInfo conInfo ON vwCon.IdConsumo = conInfo.IdConsumo
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 2 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCon.InteresVigenteValorizado > 0
	UNION ALL
	SELECT vwCon.IdPersona, map.TipoCreditoRR, vwCon.SaldoCapitalVencidoValorizado, conInfo.IdMoneda
	FROM dbo.SICCMX_VW_Consumo vwCon
	INNER JOIN dbo.SICCMX_ConsumoInfo conInfo ON vwCon.IdConsumo = conInfo.IdConsumo
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 3 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCon.SaldoCapitalVencidoValorizado > 0
	UNION ALL
	SELECT vwCon.IdPersona, map.TipoCreditoRR, vwCon.InteresVencidoValorizado, conInfo.IdMoneda
	FROM dbo.SICCMX_VW_Consumo vwCon
	INNER JOIN dbo.SICCMX_ConsumoInfo conInfo ON vwCon.IdConsumo = conInfo.IdConsumo
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 4 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwCon.InteresVencidoValorizado > 0
	UNION ALL
	SELECT vwHip.IdPersona, map.TipoCreditoRR, vwHip.SaldoCapitalVigenteValorizado, hipInfo.IdMoneda
	FROM dbo.SICCMX_VW_Hipotecario vwHip
	INNER JOIN dbo.SICCMX_HipotecarioInfo hipInfo ON vwHip.IdHipotecario = hipInfo.IdHipotecario
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 1 AND map.IdMetodologia = 2 AND map.TipoCreditoR04A IS NULL
	WHERE vwHip.SaldoCapitalVigenteValorizado > 0
	UNION ALL
	SELECT vwHip.IdPersona, map.TipoCreditoRR, vwHip.InteresVigenteValorizado, hipInfo.IdMoneda
	FROM dbo.SICCMX_VW_Hipotecario vwHip
	INNER JOIN dbo.SICCMX_HipotecarioInfo hipInfo ON vwHip.IdHipotecario = hipInfo.IdHipotecario
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 2 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwHip.InteresVigenteValorizado > 0
	UNION ALL
	SELECT vwHip.IdPersona, map.TipoCreditoRR, vwHip.SaldoCapitalVencidoValorizado, hipInfo.IdMoneda
	FROM dbo.SICCMX_VW_Hipotecario vwHip
	INNER JOIN dbo.SICCMX_HipotecarioInfo hipInfo ON vwHip.IdHipotecario = hipInfo.IdHipotecario
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 3 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwHip.SaldoCapitalVencidoValorizado > 0
	UNION ALL
	SELECT vwHip.IdPersona, map.TipoCreditoRR, vwHip.InteresVencidoValorizado, hipInfo.IdMoneda
	FROM dbo.SICCMX_VW_Hipotecario vwHip
	INNER JOIN dbo.SICCMX_HipotecarioInfo hipInfo ON vwHip.IdHipotecario = hipInfo.IdHipotecario
	LEFT OUTER JOIN dbo.SICC_TipoCredito_RR_MAP map ON map.TipoSaldo = 4 AND map.IdMetodologia IS NULL AND map.TipoCreditoR04A IS NULL
	WHERE vwHip.InteresVencidoValorizado > 0
) tmp
GROUP BY tmp.IdPersona, tmp.TipoCreditoRR, tmp.IdMoneda
GO
