SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_ProcesoHistorico]
AS
SELECT
	ProcesoHistorico.IdProcesoHistorico,
	ProcesoHistorico.Nombre,
	ProcesoHistorico.Descripcion,
	ProcesoHistorico.Codename,
	ProcesoHistorico.Position,
	ProcesoHistorico.Activo
FROM dbo.SICC_ProcesoHistorico AS ProcesoHistorico;
GO
