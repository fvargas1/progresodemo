SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_EntidadColumna]
AS
SELECT
	EntidadColumna.IdEntidadColumns,
	EntidadColumna.Nombre,
	EntidadColumna.Descripcion,
	EntidadColumna.IdEntidad,
	identidad.Nombre AS IdEntidadNombre,
	EntidadColumna.NombreCampo,
	EntidadColumna.TipoDato,
	EntidadColumna.IsRequerido,
	EntidadColumna.IsKey,
	EntidadColumna.IsIdentidadBusqueda,
	EntidadColumna.IdCatalogo,
	idcatalogo.Nombre AS IdCatalogoNombre,
	EntidadColumna.NombreIdCatalogo
FROM dbo.MIGRACION_EntidadColumns AS EntidadColumna
INNER JOIN dbo.MIGRACION_Entidad identidad ON EntidadColumna.IdEntidad = identidad.IdEntidad
LEFT OUTER JOIN dbo.MIGRACION_AdminCatalog idcatalogo ON EntidadColumna.IdCatalogo = idcatalogo.IdAdminCatalog;
GO
