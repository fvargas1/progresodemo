SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_CreditoReservasMonedaOrigenHistorico]
AS
SELECT
	dat.Codigo,
	ROUND(ISNULL(dat.SaldoReservaExpHistorico,0)/tc.Valor,2) AS SaldoReservaExpHistorico,
	ROUND(ISNULL(dat.SaldoReservaCubHistorico,0)/tc.Valor,2) AS SaldoReservaCubHistorico,
	tc.Valor AS ValorHistorico
FROM R04.[0419Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.Codigo = dat.Moneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo p ON p.IdPeriodo = tc.IdPeriodo
WHERE tc.IdPeriodo = (
	SELECT ant.IdPeriodo
	FROM dbo.SICC_Periodo act
	INNER JOIN dbo.SICC_Periodo ant ON YEAR(ant.Fecha) = YEAR(DATEADD(month, -1, act.Fecha)) AND MONTH(ant.Fecha) = MONTH(DATEADD(MONTH, -1, act.Fecha))
	WHERE act.Activo = 1
);
GO
