SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_AdminCatalogo]
AS
SELECT
	AdminCatalogo.IdAdminCatalog,
	AdminCatalogo.Nombre,
	AdminCatalogo.CodeName,
	AdminCatalogo.NombreTabla,
	AdminCatalogo.NombreIdentidad,
	AdminCatalogo.NombreBusqueda
FROM dbo.MIGRACION_AdminCatalog AS AdminCatalogo;
GO
