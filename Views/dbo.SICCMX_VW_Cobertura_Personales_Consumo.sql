SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Cobertura_Personales_Consumo]
AS
SELECT
	can.IdConsumo,
	MIN(can.[PI]) AS [PI],
	MIN(can.SeveridadCorresp) AS SP,
	CAST(SUM(ISNULL(can.C,0)) AS DECIMAL(23,2)) AS MontoCubierto
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo IN ('NMC-04','NMC-05','GP')
GROUP BY can.IdConsumo;
GO
