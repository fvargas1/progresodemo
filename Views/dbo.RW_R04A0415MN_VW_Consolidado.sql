SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415MN_VW_Consolidado]
AS
SELECT
	Codigo,
	Concepto,
	Padre,
	SUM(NoAplicaInteresesPagados) AS NoAplicaInteresesPagados,
	SUM(NoAplicaComisiones) AS NoAplicaComisiones,
	SUM(NoAplicaSaldoPromedioDiario) AS NoAplicaSaldoPromedioDiario
FROM dbo.RW_R04A0415MN_VW
GROUP BY Codigo, Concepto, Padre;
GO
