SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Proyecto_Garantia]
AS
SELECT pry.IdProyecto, SUM(cg.MontoUsadoGarantia) AS ValorGarantia, COUNT(cg.IdGarantia) AS Cantidad
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Credito cre ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Proyecto pry ON cre.IdLineaCredito = pry.IdLineaCredito
GROUP BY pry.IdProyecto;
GO
