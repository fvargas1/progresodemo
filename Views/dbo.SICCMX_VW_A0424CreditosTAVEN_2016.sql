SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_A0424CreditosTAVEN_2016]
AS
-- Creditos con Sitacion Historica Vigente y Actual Vencida
SELECT dat.Codigo
FROM R04.[0424Datos_2016] dat
WHERE (dat.SituacionHistorica = '1' AND dat.SituacionActual = '2');
GO
