SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_TotalConsumoRC]          
AS          
SELECT  per.Codigo AS CodigoCliente,                  
  cre.Codigo as CodigoCredito,      
  cre.FechaVencimiento,    
  ISNULL(cre.SaldoTotalValorizado,0)   AS Saldo,                          
  ISNULL(reser.ReservaExpuesta,0)+ ISNULL(reser.ReservaCubierta,0) AS Reserva,    
  periodo.Fecha AS Fecha,    
  CASE WHEN DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) <=0 
  THEN 1 
  ELSE DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) END AS Plazo,    
  mon.CodigoCNBV_Hipo AS Moneda    
FROM SICC_Periodo periodo,     
 dbo.SICCMX_Persona per                          
  INNER JOIN SICCMX_VW_Consumo cre ON per.IdPersona = cre.IdPersona                   
  INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables reser ON reser.IdConsumo = cre.IdConsumo      
  INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMoneda                      
WHERE periodo.Activo = 1

GO
