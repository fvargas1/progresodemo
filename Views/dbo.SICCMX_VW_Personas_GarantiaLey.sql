SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Personas_GarantiaLey]
AS
SELECT IdPersona FROM dbo.SICCMX_Persona WHERE EsFondo IS NOT NULL
UNION
SELECT c.IdPersona
FROM dbo.SICCMX_Credito c
INNER JOIN dbo.SICCMX_CreditoAval ca ON c.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
WHERE tg.Codigo='7'
UNION
SELECT c.IdPersona
FROM dbo.SICCMX_Credito c
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON c.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.SICC_TipoGarante tg ON g.IdTipoGarante = tg.IdTipoGarante
WHERE tg.Codigo='7'
GO
