SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_Cub]
AS
SELECT
 can.IdCredito,
 SUM(ISNULL(can.MontoCobAjust, 0)) AS Cobertura,
 SUM(ISNULL(can.PrctCobSinAju, 0)) AS PrctCobSinAju,
 SUM(ISNULL(can.PrctCobAjust, 0)) AS PrctCobAjust,
 ISNULL(cre.EI_Total, 0) AS MontoCredito
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
WHERE can.EsDescubierto IS NULL
GROUP BY can.IdCredito, cre.EI_Total;
GO
