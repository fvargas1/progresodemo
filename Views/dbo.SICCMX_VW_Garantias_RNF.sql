SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_RNF]
AS
SELECT
 vw.IdCredito,
 CAST(SUM(vw.ValorGarantia) AS DECIMAL(23,2)) AS ValorGarantia,
 vw.MontoCredito,
 sp.NivelMinimo,
 sp.NivelMaximo,
 sp.SeveridadPerdida,
 vw.IdTipoGarantia
FROM dbo.SICCMX_VW_Garantias_NM vw
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON vw.IdTipoGarantia = sp.IdTipoGarantia
WHERE vw.IdClasificacionNvaMet=2 AND vw.Aplica=1
GROUP BY vw.IdCredito, vw.MontoCredito, sp.NivelMinimo, sp.NivelMaximo, sp.SeveridadPerdida, vw.IdTipoGarantia
GO
