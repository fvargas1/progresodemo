SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [Historico].[SICCMX_VW_Credito]    
AS    
SELECT    
Credito.IdPeriodoHistorico,    
Credito.Codigo,    
--Credito.PeriodoAmortizacion,    
--Credito.PeriodoInteres,    
Credito.PorcentajeFondeo,    
Credito.SaldoCapitalVigente,    
Credito.InteresVigente,    
Credito.SaldoCapitalVencido,    
Credito.InteresVencido,    
Credito.FechaTraspasoVencida,    
Credito.FechaProximaAmortizacion,    
Credito.InteresRefinanciado,    
Credito.FechaAutorizacionOriginal,    
Credito.MontoOriginal,     
--Credito.Morosidad,    
Credito.Reestructura,    
Credito.SituacionCredito AS Situacion,    
--Credito.SituacionCredito AS Situacion,    
Credito.Persona,    
Persona.Nombre AS PersonaNombre,    
--Credito.TipoCredito,    
Credito.Moneda,    
--Moneda.Nombre AS MonedaNombre,    
Credito.Destino,    
Credito.TasaReferencia,    
--Credito.PeriodicidadCapital,    
--Credito.ClasificacionLegal,    
Credito.TasaBruta,    
Credito.MontoLineaCredito,    
Credito.NumeroAgrupacion,    
Credito.FechaDisposicion,    
Credito.FechaVencimiento,    
--Credito.MontoAmortizacion,    
--Credito.MontoInteresCobrar,    
Credito.FrecuenciaRevisionTasa,    
--Credito.NumeroLinea,    
--Credito.CuentaContable,    
Credito.FechaTraspasoVigente,    
--Credito.NumeroReestructuras,    
Credito.Formalizda,    
Credito.Recuperacion,    
Credito.Expediente,    
Credito.MesesIncumplimiento,    
Credito.Metodologia,    
metodologia.IdMetodologia,    
--Credito.FuentePagoFederal,    
Credito.Fondeo,    
Credito.SaldoCapitalVigente*tipocambio.Valor AS SaldoVigenteValorizado,    
Credito.InteresVigente*tipocambio.Valor AS InteresVigenteValorizado,    
Credito.SaldoCapitalVencido*tipocambio.Valor AS SaldoVencidoValorizado,    
Credito.InteresVencido*tipocambio.Valor AS InteresVencidoValorizado,    
tipocambio.Valor as TipoCambioValor,    
ISNULL(Credito.SaldoCapitalVigente*tipocambio.Valor, 0) +    
ISNULL(Credito.InteresVigente*tipocambio.Valor, 0) +    
ISNULL(Credito.SaldoCapitalVencido*tipocambio.Valor, 0) +    
ISNULL(Credito.InteresVencido*tipocambio.Valor, 0) AS MontoValorizado  ,  
ISNULL(cr.PorReservaFinal,0) AS ReservaFinal  
--SELECT *  
FROM Historico.SICCMX_Credito Credito    
INNER JOIN Historico.SICCMX_Persona Persona ON Persona.Codigo=Credito.Persona AND Credito.IdPeriodoHistorico=Persona.IdPeriodoHistorico    
--INNER JOIN Historico.SICC_TipoCambio  
INNER JOIN (SELECT Moneda.Codigo[Moneda], historico.IdPeriodoHistorico, tipocambio.Valor FROM SICC_TipoCambio tipocambio   
INNER JOIN dbo.SICC_Moneda moneda ON tipocambio.IdMoneda=moneda.IdMoneda  
INNER JOIN dbo.SICC_Periodo periodo ON tipocambio.IdPeriodo=periodo.IdPeriodo  
INNER JOIN  [dbo].[SICC_PeriodoHistorico] historico ON periodo.Fecha=historico.Fecha)  
 tipocambio ON tipocambio.Moneda=Credito.Moneda AND Credito.IdPeriodoHistorico=tipocambio.IdPeriodoHistorico    
LEFT OUTER JOIN dbo.SICCMX_Metodologia metodologia ON metodologia.Codigo=Credito.Metodologia    
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables cr ON Credito.IdPeriodoHistorico = cr.IdPeriodoHistorico AND Credito.Codigo = cr.Credito  
  
GO
