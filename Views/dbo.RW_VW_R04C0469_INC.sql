SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0469_INC]

AS

SELECT

	IdReporteLog,

	Formulario,

	CodigoCreditoCNBV,

	NULL AS CodigoCredito,

	NULL AS CodigoPersona,

	NULL AS RFC,

	NULL AS NombrePersona,

	ClasContable,

	ConcursoMercantil,

	NULL AS CategoriaCredito,

	FechaDisposicion,

	FechaVencDisposicion,

	Moneda,

	NumeroDisposicion,

	NombreFactorado,

	RFC_Factorado,

	SaldoInicial,

	TasaInteres,

	TasaInteresDisp,

	DifTasaRef,

	OperacionDifTasaRef,

	FrecuenciaRevisionTasa,

	MontoDispuesto,

	MontoPagoExigible,

	MontoCapitalPagado,

	MontoInteresPagado,

	MontoComisionPagado,

	MontoInteresMoratorio,

	MontoTotalPagado,

	MontoCondonacion,

	MontoQuitas,

	MontoBonificado,

	MontoDescuentos,

	MontoAumentosDec,

	SaldoFinal,

	SaldoCalculoInteres,

	DiasCalculoInteres,

	MontoInteresAplicar,

	SaldoInsoluto,

	SituacionCredito,

	DiasAtraso,

	FechaUltimoPago,

	NULL AS ProyectoInversion,

	MontoBancaDesarrollo,

	InstitucionFondeo,

	'' AS ReservaTotal,

	'' AS ReservaCubierta,

	'' AS ReservaExpuesta,

	'' AS SPTotal,

	'' AS SPCubierta,

	'' AS SPExpuesta,

	'' AS EITotal,

	'' AS EICubierta,

	'' AS EIExpuesta,

	'' AS PITotal,

	'' AS PICubierta,

	'' AS PIExpuesta,

	'' AS GradoRiesgo,

	'' AS ReservaTotalInterna,

	'' AS SPInterna,

	'' AS EIInterna,

	'' AS PIInterna

FROM dbo.RW_R04C0469_2016

INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2016'



UNION ALL

SELECT

	IdReporteLog,

	Formulario,

	CodigoCreditoCNBV,

	CodigoCredito,

	CodigoPersona,

	RFC,

	NombrePersona,

	NULL AS ClasContable,

	NULL AS ConcursoMercantil,

	CategoriaCredito,

	FechaDisposicion,

	FechaVencDisposicion,

	Moneda,

	NumeroDisposicion,

	NULL AS NombreFactorado,

	NULL AS RFC_Factorado,

	SaldoInicial,

	TasaInteres,

	NULL AS TasaInteresDisp,

	NULL AS DifTasaRef,

	NULL AS OperacionDifTasaRef,

	NULL AS FrecuenciaRevisionTasa,

	MontoDispuesto,

	MontoPagoExigible,

	MontoCapitalPagado,

	MontoInteresPagado,

	MontoComisionPagado,

	MontoInteresMoratorio,

	MontoTotalPagado,

	NULL AS MontoCondonacion,

	NULL AS MontoQuitas,

	MontoBonificado,

	NULL AS MontoDescuentos,

	NULL AS MontoAumentosDec,

	SaldoFinal,

	SaldoCalculoInteres,

	DiasCalculoInteres,

	MontoInteresAplicar,

	ResponsabilidadFinal,

	SituacionCredito,

	DiasVencidos,

	FechaUltimoPago,

	ProyectoInversion,

	MontoBancaDesarrollo,

	InstitucionFondeo,

	ReservaTotal,

	ReservaCubierta,

	ReservaExpuesta,

	SPTotal,

	SPCubierta,

	SPExpuesta,

	EITotal,

	EICubierta,

	EIExpuesta,

	PITotal,

	PICubierta,

	PIExpuesta,

	GradoRiesgo,

	ReservaTotalInterna,

	SPInterna,

	EIInterna,

	PIInterna

FROM dbo.RW_R04C0469

INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2015';
GO
