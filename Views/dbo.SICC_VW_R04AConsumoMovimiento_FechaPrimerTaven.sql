SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04AConsumoMovimiento_FechaPrimerTaven]
AS
-- Por recomendacion de SOSA, debe de tomarse el ultimo movto taven (si hay varios tave)
SELECT
	mov.IdConsumo,
	MAX(mov.Fecha) AS Fecha
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
WHERE tm.Codigo='TAVEN'
GROUP BY mov.IdConsumo;
GO
