SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04AHipotecarioCastigados]
AS
SELECT c.Codigo, m.Monto
FROM dbo.SICCMX_Hipotecario c
INNER JOIN dbo.SICCMX_HipotecarioReservas r ON r.IdHipotecario = c.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos m ON m.IdHipotecario = c.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = m.IdTipoMovimiento
WHERE r.ReservaCubierto + r.ReservaExpuesto = 0 AND tm.Codigo = 'CAST';
GO
