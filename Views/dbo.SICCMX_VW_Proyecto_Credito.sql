SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Proyecto_Credito]
AS
SELECT DISTINCT
	vw.IdCredito,
	pry.VentNetTotAnuales,
	pc.IdEtapa,
	pry.FechaInicioOper,
	pc.SobreCosto,
	pc.MontoCubierto AS MontoCubiertoTerceros,
	pc.ExposicionSobrecosto,
	pc.PorcenajeSobrecosto,
	pc.VPTotal,
	pry.TasaDescuentoVP,
	pc.PorcentajeCorrida,
	pc.PorcentajeFinal,
	pc.IdCalificacionFinal,
	pc.ReservaConstruccion,
	pc.ReservaOperacion
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.SICCMX_VW_Credito_NMC vw ON pry.IdLineaCredito = vw.IdLineaCredito
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto

UNION ALL

SELECT DISTINCT
	vw.IdCredito,
	pry.VentNetTotAnuales,
	pc.IdEtapa,
	pry.FechaInicioOper,
	pc.SobreCosto,
	pc.MontoCubierto AS MontoCubiertoTerceros,
	pc.ExposicionSobrecosto,
	pc.PorcenajeSobrecosto,
	pc.VPTotal,
	pry.TasaDescuentoVP,
	pc.PorcentajeCorrida,
	pc.PorcentajeFinal,
	pc.IdCalificacionFinal,
	pc.ReservaConstruccion,
	pc.ReservaOperacion
FROM dbo.SICCMX_ProyectoLinea pl
INNER JOIN dbo.SICCMX_VW_Credito_NMC vw ON pl.IdLineaCredito = vw.IdLineaCredito
INNER JOIN dbo.SICCMX_Proyecto pry ON pl.IdProyecto = pry.IdProyecto
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto;
GO
