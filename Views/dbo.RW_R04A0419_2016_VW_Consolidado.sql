SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0419_2016_VW_Consolidado]
AS
SELECT
 Codigo,
 Concepto,
 Padre,
 SUM(ReservasConsolidado_Cargos) AS ConsolidadoCargos,
 SUM(ReservasConsolidado_Abonos) AS ConsolidadoAbonos,
 SUM(ReservasConsolidado_Saldo) AS ConsolidadoSaldo,
 SUM(ReservasMNUDIS_Cargos) AS MNCargos,
 SUM(ReservasMNUDIS_Abonos) AS MNAbonos,
 SUM(ReservasMNUDIS_Saldo) AS MNSaldo,
 SUM(ReservasME_Cargos) AS MECargos,
 SUM(ReservasME_Abonos) AS MEAbonos,
 SUM(ReservasME_Saldos) AS MESaldo,
 SUM(ReservasUDIGub_Cargos) AS UDICargos,
 SUM(ReservasUDIGub_Abonos) AS UDIAbonos,
 SUM(ReservasUDIGub_Saldos) AS UDISaldo
FROM dbo.RW_R04A0419_2016_VW
GROUP BY Codigo, Concepto, Padre;
GO
