SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
CREATE VIEW [dbo].[SICCMX_VW_Credito]  
AS  
SELECT  
Credito.IdCredito,  
Credito.Codigo,  
--Credito.PeriodoAmortizacion,  
--Credito.PeriodoInteres,  
--Credito.IdInstitucionOrigen,  
--Institucion.Nombre AS Institucion,  
Credito.PorcentajeFondeo,  
Credito.SaldoCapitalVigente,  
Credito.InteresVigente,  
Credito.SaldoCapitalVencido,  
Credito.InteresVencido,  
Credito.FechaTraspasoVencida,  
Credito.FechaProximaAmortizacion,  
Credito.InteresRefinanciado,  
Credito.FechaAutorizacionOriginal,  
Credito.MontoOriginal,  
--Credito.Morosidad,  
Credito.IdReestructura,  
Reestructura.Nombre AS Reestructura,  
--Credito.IdPeriodicidadInteres,  
--PeriodicidadInteres.Nombre AS PeriodicidadInteres,  
Credito.IdSituacionCredito,  
Situacion.Nombre AS Situacion,  
Credito.IdPersona,  
Persona.Nombre AS PersonaNombre,  
Credito.IdClasificacionContable,  
Clasificacion.Nombre AS ClasificacionContable,  
--Credito.IdTipoCredito,  
------  
Credito.IdMoneda,  
Moneda.Nombre AS Moneda,  
Credito.IdDestino,  
Destino.Nombre AS Destino,  
Credito.IdTasaReferencia,  
TasaReferencia.Nombre AS TasaReferencia,  
--Credito.IdPeriodicidadCapital,  
--PeriodicidadCapital.Nombre AS PeriodicidadCapital,  
--Credito.IdClasificacionLegal,  
--ClasificacionLegal.Nombre AS ClasificacionLegal,  
Credito.TasaBruta,  
Credito.MontoLineaCredito,  
Credito.NumeroAgrupacion,  
Credito.FechaDisposicion,  
Credito.FechaVencimiento,  
--Credito.MontoAmortizacion,  
--Credito.MontoInteresCobrar,  
Credito.FrecuenciaRevisionTasa,  
--Credito.NumeroLinea,  
--Credito.CuentaContable,  
Credito.FechaTraspasoVigente,  
--Credito.NumeroReestructuras,  
Credito.Formalizda,  
Credito.Recuperacion,  
Credito.Expediente,  
Credito.MesesIncumplimiento,  
Credito.IdMetodologia,  
--Credito.FuentePagoFederal,  
Credito.IdFondeo,  
Fondeo.Nombre AS Fondeo,  
Credito.SaldoCapitalVigente*tipocambio.Valor AS SaldoVigenteValorizado,  
Credito.InteresVigente*tipocambio.Valor AS InteresVigenteValorizado,  
Credito.SaldoCapitalVencido*tipocambio.Valor AS SaldoVencidoValorizado,  
Credito.InteresVencido*tipocambio.Valor AS InteresVencidoValorizado,  
tipocambio.Valor as TipoCambioValor,  
--entidad.IdEntidad,  
--entidad.Nombre as IdEntidadNombre,  
cInfo.CodigoCreditoReestructurado as CodigoCreditoRestructura,  
ISNULL(Credito.SaldoCapitalVigente*tipocambio.Valor, 0) +  
ISNULL(Credito.InteresVigente*tipocambio.Valor, 0) +  
ISNULL(Credito.SaldoCapitalVencido*tipocambio.Valor, 0) +  
ISNULL(Credito.InteresVencido*tipocambio.Valor, 0) AS MontoValorizado,  
Credito.IdLineaCredito AS IdLineaCredito  
FROM SICCMX_Credito Credito  
LEFT OUTER JOIN SICCMX_CreditoInfo cInfo ON Credito.IdCredito=cInfo.IdCredito  
--LEFT OUTER JOIN SICC_Institucion Institucion ON Institucion.IdInstitucion=Credito.IdInstitucionOrigen  
LEFT OUTER JOIN SICC_Reestructura Reestructura ON Reestructura.IdReestructura=Credito.IdReestructura  
LEFT OUTER JOIN SICC_SituacionCredito Situacion ON Situacion.IdSituacionCredito=Credito.IdSituacionCredito  
INNER JOIN SICCMX_Persona Persona ON Persona.IdPersona=Credito.IdPersona  
--LEFT OUTER JOIN SICC_PeriodicidadInteresAnterior PeriodicidadInteres ON PeriodicidadInteres.IdPeriodicidadInteres=Credito.IdPeriodicidadInteres  
LEFT OUTER JOIN SICC_ClasificacionContable Clasificacion ON Clasificacion.IdClasificacionContable=Credito.IdClasificacionContable  
INNER JOIN SICC_Moneda Moneda ON Moneda.IdMoneda=Credito.IdMoneda   
LEFT OUTER JOIN SICC_Destino Destino ON Destino.IdDestino=Credito.IdDestino  
LEFT OUTER JOIN SICC_TasaReferencia TasaReferencia ON TasaReferencia.IdTasaReferencia=Credito.IdTasaReferencia  
--LEFT OUTER JOIN SICC_PeriodicidadCapitalAnterior PeriodicidadCapital ON PeriodicidadCapital.IdPeriodicidadCapital=Credito.IdPeriodicidadCapital  
--LEFT OUTER JOIN SICC_ClasificacionLegal ClasificacionLegal ON ClasificacionLegal.IdClasificacionLegal=Credito.IdClasificacionLegal  
LEFT OUTER JOIN SICC_Fondeo Fondeo ON Fondeo.IdFondeo=Credito.IdFondeo  
INNER JOIN SICC_TipoCambio tipocambio ON tipocambio.IdMoneda=Moneda.IdMoneda   
INNER JOIN SICC_Periodo periodo ON periodo.IdPeriodo=tipocambio.IdPeriodo AND periodo.Activo=1  
--LEFT OUTER JOIN SICCMX_Entidad entidad ON credito.IdEntidad=entidad.IdEntidad  
GO
