SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_NM_Consumo]
AS
SELECT
 c.IdPersona,
 c.IdConsumo,
 crv.E AS MontoCredito,
 0 AS He,
 ISNULL(g.Hc, 0) AS Hc,
 CASE WHEN c.IdMoneda = g.IdMoneda THEN 0 ELSE 0.08 END AS Hfx,
 cg.MontoUsadoGarantia AS ValorGarantia,
 g.IdTipoGarantia,
 g.IdClasificacionNvaMet,
 g.Aplica
FROM dbo.SICCMX_VW_Garantia_Consumo g
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON g.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICCMX_VW_Consumo c ON c.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON c.IdConsumo = crv.IdConsumo;
GO
