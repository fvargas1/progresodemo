SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Anexo22_GP]
AS
SELECT
 anx.IdGP,
 anx.NumDiasMoraPromInstBanc,
 anx.PorPagoTiemInstBanc,
 anx.NumInstRepUlt12Meses,
 anx.PorPagoTiemInstNoBanc,
 anx.MonTotPagInfonavitUltBimestre / tca.Valor AS MonTotPagInfonavitUltBimestre,
 anx.NumDiasAtrInfonavitUltBimestre,
 anx.TasaRetLab,
 anx.IndCalEstaEcono,
 anx.IntCarCompetencia,
 anx.Provedores,
 anx.Clientes,
 anx.EdoFinAudit,
 anx.NumAgeCalf,
 anx.IndConsAdmon,
 anx.EstrucOrg,
 anx.CompAccionaria,
 anx.GastosFinancieros,
 anx.VentNetTotAnuales,
 anx.PasivoCirculante,
 anx.ActivoCirculante,
 anx.ActTotalAnual,
 anx.CapitalContableProm,
 anx.UtilidadNeta,
 anx.UAFIR,
 anx.OrgDescPartidoPolitico,
 anx.FechaInfoFinanc,
 anx.FechaInfoBuro,
 anx.CalCredAgenciaCal,
 anx.ExpNegativasPag,
 anx.RotActTotales,
 anx.RotCapTrabajo,
 anx.RendimeintosCapitalROE,
 anx.UAFIR_GastosFin,
 anx.LiquidezOper,
 anx.NumeroEmpleados,
 anx.LugarRadica,
 anx.EsGarante,
 anx.SinAtrasos
FROM dbo.SICCMX_Anexo22_GP anx
INNER JOIN dbo.SICC_TipoCambio tca ON 1=1
INNER JOIN dbo.SICC_Periodo per ON tca.IdPeriodo = per.IdPeriodo AND per.Activo=1
INNER JOIN dbo.SICC_Moneda mon ON tca.IdMoneda = mon.IdMoneda
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.Value = mon.Codigo AND cfg.CodeName='MonedaUdis';
GO
