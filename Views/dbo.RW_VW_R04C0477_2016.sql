SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0477_2016]
AS
SELECT DISTINCT
	Formulario,
	CodigoCreditoCNBV,
	TipoBaja,
	SaldoPrincipalInicio,
	SaldoInsoluto,
	MontoPagado,
	MontoCastigos,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificaciones,
	MontoDescuentos,
	MontoBienDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas,
	ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(TipoBaja,'') AS EVERYTHING
FROM dbo.RW_R04C0477_2016;
GO
