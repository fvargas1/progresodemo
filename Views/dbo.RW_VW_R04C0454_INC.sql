SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0454_INC]
AS
SELECT
	IdReporteLog,
	Formulario,
	CodigoCreditoCNBV,
	NULL AS CodigoCredito,
	NULL AS CodigoPersona,
	NULL AS RFC,
	NULL AS NombrePersona,
	NULL AS CategoriaCredito,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NombreFactorado,
	RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	NULL AS ProyectoInversion,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	NULL AS ReservaTotal,
	NULL AS ReservaCubierta,
	NULL AS ReservaExpuesta,
	NULL AS SPTotal,
	NULL AS SPCubierta,
	NULL AS SPExpuesta,
	NULL AS EITotal,
	NULL AS EICubierta,
	NULL AS EIExpuesta,
	NULL AS PITotal,
	NULL AS PICubierta,
	NULL AS PIExpuesta,
	NULL AS GradoRiesgo,
	NULL AS ReservaTotalInterna,
	NULL AS SPInterna,
	NULL AS EIInterna,
	NULL AS PIInterna
FROM dbo.RW_R04C0454_2016
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2016'

UNION ALL
SELECT
	IdReporteLog,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	CodigoPersona,
	RFC,
	NombrePersona,
	CategoriaCredito,
	NULL AS ClasContable,
	NULL AS ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NULL AS NombreFactorado,
	NULL AS RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	NULL AS TasaInteresDisp,
	NULL AS DifTasaRef,
	NULL AS OperacionDifTasaRef,
	NULL AS FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	NULL AS MontoCondonacion,
	NULL AS MontoQuitas,
	MontoBonificado,
	NULL AS MontoDescuentos,
	NULL AS MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	ResponsabilidadFinal,
	SituacionCredito,
	DiasVencidos,
	FechaUltimoPago,
	ProyectoInversion,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	ReservaTotal,
	ReservaCubierta,
	ReservaExpuesta,
	SPTotal,
	SPCubierta,
	SPExpuesta,
	EITotal,
	EICubierta,
	EIExpuesta,
	PITotal,
	PICubierta,
	PIExpuesta,
	GradoRiesgo,
	ReservaTotalInterna,
	SPInterna,
	EIInterna,
	PIInterna
FROM dbo.RW_R04C0454
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2015';
GO
