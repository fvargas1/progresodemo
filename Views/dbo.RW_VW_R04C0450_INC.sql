SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0450_INC]
AS
SELECT
	IdReporteLog,
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	PI_Garante,
	SP_Garante,
	EI_Garante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM dbo.RW_R04C0450_2016
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2016'

UNION ALL
SELECT
	IdReporteLog,
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	NULL AS PI_Garante,
	NULL AS SP_Garante,
	NULL AS EI_Garante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM dbo.RW_R04C0450
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2015';
GO
