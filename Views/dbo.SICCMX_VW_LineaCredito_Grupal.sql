SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCredito_Grupal]
AS
SELECT lin.IdLineaCredito, grp.CNBV AS GrupalCNBV
FROM dbo.SICCMX_LineaCredito lin
OUTER APPLY (
 SELECT TOP 1 CNBV
 FROM dbo.SICCMX_CreditoCNBV
 WHERE NumeroLinea = lin.EsPadre
) AS grp;
GO
