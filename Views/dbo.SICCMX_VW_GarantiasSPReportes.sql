SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_GarantiasSPReportes]
AS
SELECT DISTINCT
	can.IdCredito,
	SUM(can.PrctCobSinAju) AS Porcentaje,
	MAX(CASE WHEN tg.Codigo = 'NMC-03' THEN can.SeveridadCorresp ELSE 0 END) AS SP_DerechosCobro,
	MAX(CASE WHEN tg.Codigo = 'NMC-01' THEN can.SeveridadCorresp ELSE 0 END) AS SP_BienesInmuebles,
	MAX(CASE WHEN tg.Codigo = 'NMC-02' THEN can.SeveridadCorresp ELSE 0 END) AS SP_BienesMuebles,
	MAX(CASE WHEN tg.Codigo = 'EYM-01' THEN can.SeveridadCorresp ELSE 0 END) AS SP_FidPartFed,
	MAX(CASE WHEN tg.Codigo = 'EYM-02' THEN can.SeveridadCorresp ELSE 0 END) AS SP_FidIngProp,
	0 AS SP_OtrasGar,
	SUM(CASE WHEN tot.Total > 0 THEN (can.C * can.SeveridadCorresp) / tot.Total ELSE 0 END) AS SP_GarRealNoFin
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN (
	SELECT c.IdCredito, SUM(c.C) AS Total
	FROM dbo.SICCMX_Garantia_Canasta c
	INNER JOIN dbo.SICC_TipoGarantia t ON c.IdTipoGarantia = t.IdTipoGarantia
	WHERE t.IdClasificacionNvaMet = 2
	GROUP BY c.IdCredito
) AS tot ON can.IdCredito = tot.IdCredito
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.IdClasificacionNvaMet=2
GROUP BY can.IdCredito;
GO
