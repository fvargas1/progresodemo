SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04A_Consolidado]
AS
SELECT DISTINCT
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato,
	ISNULL(Concepto,'')+'|'+ISNULL(SubReporte,'')+'|'+ISNULL(Moneda,'') AS EVERYTHING
FROM dbo.RW_R04A_Consolidado;
GO
