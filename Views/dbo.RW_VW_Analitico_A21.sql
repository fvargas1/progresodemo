SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Analitico_A21]
AS
SELECT DISTINCT
 Codigo,
 Nombre,
 PonderadoCuantitativo,
 PonderadoCualitativo,
 FactorTotal,
 [PI],
 AntSocInfCred_V,
 AntSocInfCred_P,
 QuitasCastReest_V,
 QuitasCastReest_P,
 PrctPagoInstNoBanc_V,
 PrctPagoInstNoBanc_P,
 PrctPagosEntComer_V,
 PrctPagosEntComer_P,
 CuentasCredAbiertosInstFin_V,
 CuentasCredAbiertosInstFin_P,
 MontoMaxInstFin_V,
 MontoMaxInstFin_P,
 MesesUltCredAbierto_V,
 MesesUltCredAbierto_P,
 PrctPagoInstFinBanc_V,
 PrctPagoInstFinBanc_P,
 PrctPagoInstFin29Atr_V,
 PrctPagoInstFin29Atr_P,
 PrctPagoInstFin90Atr_V,
 PrctPagoInstFin90Atr_P,
 DiasMoraInstFinBanc_V,
 DiasMoraInstFinBanc_P,
 PagosInstFinBanc_V,
 PagosInstFinBanc_P,
 AportInfonavit_V,
 AportInfonavit_P,
 DiasAtrInfonavit_V,
 DiasAtrInfonavit_P,
 TasaRetLab_V,
 TasaRetLab_P,
 IndPerMoralFid_V,
 IndPerMoralFid_P,
 ProcOrigAdmon_V,
 ProcOrigAdmon_P,
 SinAtrasos,
 ISNULL(Codigo,'')+'|'+ISNULL(Nombre,'') AS EVERYTHING
FROM dbo.RW_Analitico_A21;
GO
