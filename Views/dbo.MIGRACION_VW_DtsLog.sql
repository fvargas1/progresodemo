SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_DtsLog]
AS
SELECT
 DtsLog.id,
 DtsLog.event,
 DtsLog.computer,
 DtsLog.operator,
 DtsLog.source,
 DtsLog.sourceid,
 DtsLog.executionid,
 DtsLog.starttime,
 DtsLog.endtime,
 DtsLog.datacode,
 DtsLog.databytes,
 DtsLog.message
FROM sysssislog AS DtsLog;
GO
