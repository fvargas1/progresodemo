SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Cedula]
AS
SELECT
 Cedula.IdCedula,
 Cedula.Nombre,
 Cedula.Codename,
 Cedula.ReportFolder,
 Cedula.Reportname,
 Cedula.Parameters,
 ISNULL(Cedula.OutputParams,'') AS OutputParams,
 Cedula.Prefix,
 Cedula.Activo
FROM dbo.RW_Cedula AS Cedula;
GO
