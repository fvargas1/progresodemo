SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_InteresesDevengados]
AS
-- COMERCIAL
SELECT
 cre.Codigo,
 ISNULL(adi.InteresDevengadoValorizado,0) AS InteresDevengado
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON cre.IdCredito = adi.IdCredito

-- HIPOTECARIO
UNION ALL
SELECT
 hip.Codigo,
 ISNULL(adi.InteresesDevengadosValorizado,0)
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional adi ON hip.IdHipotecario = adi.IdHipotecario

-- CONSUMO
UNION ALL
SELECT
 con.Codigo,
 ISNULL(adi.InteresDevengadoValorizado,0)
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional adi ON con.IdConsumo = adi.IdConsumo;
GO
