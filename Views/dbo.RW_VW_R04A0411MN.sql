SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04A0411MN]
AS
SELECT DISTINCT
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato,
	ISNULL(Concepto,'')+'|'+ISNULL(Moneda,'')+'|'+ISNULL(TipoDeCartera,'')+'|'+ISNULL(TipoDeSaldo,'') AS EVERYTHING
FROM dbo.RW_R04A0411MN;
GO
