SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0417ME_VW_Consolidado]
AS
SELECT
	Codigo,
	Concepto,
	Padre,
	SUM(SaldoBase) AS SaldoBase,
	SUM(SaldoBaseValorizado) AS SaldoBaseValorizado,
	SUM(Estimacion) AS Estimacion
FROM dbo.RW_R04A0417ME_VW
GROUP BY Codigo, Concepto, Padre;
GO
