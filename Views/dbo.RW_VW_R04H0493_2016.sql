SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04H0493_2016]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	ResponsabilidadTotal,
	MontoPagoAcreditado,
	MontoQuitas,
	MontoCondonaciones,
	MontoBonificaciones,
	MontoDescuentos,
	ValorBienAdjudicado,
	MontoDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(NumeroAvaluo,'')+'|'+ISNULL(TipoBaja,'') AS EVERYTHING
FROM dbo.RW_R04H0493_2016;
GO
