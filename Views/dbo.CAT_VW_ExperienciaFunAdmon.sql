SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CAT_VW_ExperienciaFunAdmon]
AS
SELECT
 item.IdItemCatalogo,
 item.Codigo,
 item.Nombre,
 item.Padre,
 item.Orden
FROM Bajaware.ItemCatalogo item
INNER JOIN Bajaware.Catalogos cat ON cat.IdCatalogo=item.IdCatalogo
WHERE cat.Codename='A20_EXP_FUN_ADMON';
GO
