SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ConsumoReservasVariables]
AS
SELECT
	crv.IdConsumo,
	con.Codigo AS IdConsumoNombre,
	con.Codigo,
	crv.[PI],
	crv.E,
	crv.MontoGarantia,
	crv.ReservaTotal,
	crv.PorReserva,
	crv.IdCalificacion,
	cal.Codigo AS IdCalificacionNombre,
	crv.ECubierta,
	crv.EExpuesta,
	crv.SPCubierta,
	crv.SPExpuesta,
	crv.ReservaCubierta,
	crv.ReservaExpuesta,
	crv.PorReservaCubierta,
	crv.PorReservaExpuesta,
	crv.IdCalificacionCubierta,
	calCub.Codigo AS IdCalificacionCubiertaNombre,
	crv.IdCalificacionExpuesta,
	calExp.Codigo AS IdCalificacionExpuestaNombre
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
LEFT OUTER JOIN dbo.SICCMX_Consumo con ON crv.IdConsumo = con.IdConsumo
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON crv.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON crv.IdCalificacionCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON crv.IdCalificacionExpuesta = calExp.IdCalificacion;
GO
