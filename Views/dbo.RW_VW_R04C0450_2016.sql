SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0450_2016]
AS
SELECT DISTINCT
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	PI_Garante,
	SP_Garante,
	EI_Garante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia,
	ISNULL(RFC_Garante,'')+'|'+ISNULL(NombreGarante,'')+'|'+ ISNULL(CodigoGarante,'')+'|'+ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(CodigoCredito,'')
	+'|'+ISNULL(NumeroDisposicion,'')+'|'+ISNULL(NombreAcreditado,'')	AS EVERYTHING
FROM dbo.RW_R04C0450_2016;
GO
