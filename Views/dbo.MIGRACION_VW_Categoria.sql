SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Categoria]
AS
SELECT
	Categoria.IdCategoria,
	Categoria.Nombre,
	Categoria.IdParent,
	Categoria.NombreTabla
FROM dbo.MIGRACION_Categoria AS Categoria;
GO
