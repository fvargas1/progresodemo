SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415ME_VW_Consolidado]
AS
SELECT
	Codigo,
	Concepto,
	Padre,
	SUM(InteresesPagados) AS InteresesPagados,
	SUM(Comisiones) AS Comisiones,
	SUM(SaldoPromedioDiario) AS SaldoPromedioDiario,
	SUM(InteresesPagadosValorizados) AS InteresesPagadosValorizados,
	SUM(ComisionesValorizados) AS ComisionesValorizados,
	SUM(SaldoPromedioDiarioValorizados) AS SaldoPromedioDiarioValorizados
FROM dbo.RW_R04A0415ME_VW
GROUP BY Codigo, Concepto, Padre;
GO
