SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0442]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	GrupoRiesgo,
	TipoAcreditadoRel,
	PersonalidadJuridica,
	SectorEconomico,
	ActividadEconomica,
	NumeroEmpleados,
	IngresosBrutos,
	Localidad,
	NumeroConsulta,
	TipoAltaCredito,
	SpecsDisposicionCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	DestinoCredito,
	SaldoCredito,
	DenominacionCredito,
	FechaVencimiento,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	TasaRef,
	AjusteTasaRef,
	FrecuenciaRevisionTasa,
	MontoFondeo,
	CodigoBancoFondeador,
	Comisiones,
	PorcentajeCubierto,
	PorcentajeCubiertoFondeo,
	CodigoBancoGarantia,
	PorcentajeCubiertoAval,
	TipoGarantia,
	Municipio,
	Estado,
	ISNULL(CodigoPersona,'')+'|'+ISNULL(RFC,'')+'|'+ISNULL(NombrePersona,'')+'|'+ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04C0442;
GO
