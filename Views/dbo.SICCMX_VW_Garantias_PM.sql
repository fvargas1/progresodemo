SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_PM]
AS
SELECT
	can.IdCredito,
	can.MontoCobAjust AS Monto,
	can.PrctCobAjust AS Porcentaje,
	can.[PI],
	can.SeveridadCorresp AS SP,
	can.Reserva,
	g.NombreGarante AS NomGar,
	g.Codigo AS CodigoGarantia
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON can.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia AND can.IdTipoGarantia = g.IdTipoGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo='NMC-05';
GO
