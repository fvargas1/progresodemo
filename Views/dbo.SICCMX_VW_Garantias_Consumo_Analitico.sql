SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_Consumo_Analitico]
AS
SELECT
	cg.IdConsumo,
	SUM(gar.ValorGarantiaValorizado) AS MontoGarantia,
	SUM(cg.MontoUsadoGarantia) AS MontoGarantiaUsado
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
GROUP BY cg.IdConsumo;
GO
