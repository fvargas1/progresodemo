SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04H0492]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	DenominacionCredito,
	SaldoPrincipalInicio,
	TasaInteres,
	ComisionesCobradasTasa,
	ComisionesCobradasMonto,
	MontoPagoExigible,
	MontoPagoRealizado,
	MontoBonificacion,
	SaldoPrincipalFinal,
	ResponsabilidadTotal,
	FechaUltimoPago,
	SituacionCredito,
	[PI],
	SP,
	DiasAtraso,
	MAXATR,
	PorVPAGO,
	SUBCV,
	ConvenioJudicial,
	PorCobPaMed,
	PorCobPP,
	EntidadCobertura,
	ReservasSaldoFinal,
	PerdidaEsperada,
	ReservaPreventiva,
	PIInterna,
	SPInterna,
	EInterna,
	PerdidaEsperadaInterna,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04H0492;
GO
