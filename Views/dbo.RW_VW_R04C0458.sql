SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0458]
AS
SELECT DISTINCT
	Formulario,
	CodigoPersona,
	RFC,
	NombreCNBV,
	TipoCartera,
	IdActividadEconomica,
	GrupoRiesgo,
	EntFinAcreOtorgantesCre,
	IdLocalidad,
	Municipio,
	Estado,
	Nacionalidad,
	NumInfoCrediticia,
	LEI,
	IdTipoAlta,
	TipoOperacion,
	IdDestino,
	CodigoCredito,
	CodigoCnbv,
	GrupalCnbv,
	MontoLineaCredito,
	FecMaxDis,
	FecVenLinea,
	IdMoneda,
	IdDisposicion,
	TipoLinea,
	Posicion,
	RegGarantiaMob,
	IdDeudorRelacionado,
	IdInstitucionOrigen,
	IdTasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecuenciaRevisionTasa,
	IdPeriodicidadCapital,
	IdPeriodicidadInteres,
	MesesGraciaCap,
	MesesGraciaIntereses,
	GastosOrigTasa,
	GastosOriginacion,
	ComisionTasaDis,
	ComisionMontoDis,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCnbv,'')+'|'+ ISNULL(CodigoPersona,'')+'|'+ISNULL(NombreCNBV,'')+'|'
	+ISNULL(RFC,'') AS EVERYTHING
FROM dbo.RW_R04C0458
GO
