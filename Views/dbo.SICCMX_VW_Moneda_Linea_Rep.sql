SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Moneda_Linea_Rep]
AS
SELECT
	lin.IdLineaCredito,
	lin.Codigo,
	lin.IdMoneda,
	lin.EsMultimoneda,
	mon.Codigo AS CodigoMoneda,
	mon.Nombre AS NombreMoneda,
	CASE WHEN lin.EsMultimoneda = 1 THEN monMul.CodigoCNBV ELSE mon.CodigoCNBV END AS CodigoCNBVMoneda,
	mon.CodigoISO AS CodigoISOMoneda
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICC_Moneda mon ON lin.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Moneda monMul ON monMul.Codigo = '999';
GO
