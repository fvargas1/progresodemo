SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Reservas_Personales]
AS
SELECT can.IdCredito, CAST(SUM(Reserva) AS DECIMAL(23,2)) AS Reserva
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo IN ('NMC-04','NMC-05','GP')
GROUP BY can.IdCredito;
GO
