SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Archivo]
AS
SELECT
 Archivo.IdArchivo,
 Archivo.Nombre,
 Archivo.Codename,
 Archivo.JobCodename,
 Archivo.ErrorCodename,
 Archivo.LogCodename,
 Archivo.ClearLogCodename,
 Archivo.Position,
 Archivo.Activo,
 Archivo.ViewName,
 Archivo.ValidationStatus,
 Archivo.LastValidationExecuted,
 Archivo.GrupoArchivo,
 Archivo.InitCodename
FROM dbo.MIGRACION_Archivo AS Archivo
GO
