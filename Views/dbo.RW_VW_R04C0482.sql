SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0482]
AS
SELECT DISTINCT
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NombrePersona,
	TipoBaja,
	SaldoInicial,
	ResponsabilidadInicial,
	MontoPagado,
	MontoQuitCastReest,
	MontoBonificaciones,
	ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(CodigoCredito,'')+'|'+ISNULL(NombrePersona,'') AS EVERYTHING
FROM dbo.RW_R04C0482;
GO
