SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0469_2016]
AS
SELECT DISTINCT
	Formulario,
	CodigoCreditoCNBV,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NombreFactorado,
	RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(NumeroDisposicion,'')+'|'+ISNULL(NombreFactorado,'')+'|'+ISNULL(RFC_Factorado,'') AS EVERYTHING
FROM dbo.RW_R04C0469_2016;
GO
