SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Consumo_GarPer]
AS
SELECT ca.IdConsumo, MIN(a.Codigo) AS Codigo, tg.IdTipoGarantia, tg.Codigo AS Tipo
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval AND a.Aplica = 1
INNER JOIN dbo.SICC_TipoGarantia tg ON tg.Codigo = 'GP'
WHERE ca.Aplica = 1
GROUP BY ca.IdConsumo, tg.IdTipoGarantia, tg.Codigo

UNION ALL
SELECT cg.IdConsumo, MIN(g.Codigo), tg.IdTipoGarantia, tg.Codigo
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo IN ('NMC-04','NMC-04-C','NMC-05')
GROUP BY cg.IdConsumo, tg.IdTipoGarantia, tg.Codigo;
GO
