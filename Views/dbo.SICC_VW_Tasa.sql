SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_Tasa]
AS
SELECT
	Tasa.IdTasa,
	Tasa.Codigo,
	Tasa.Descripcion,
	Tasa.IdMoneda,
	moneda.Nombre AS MonedaNombre
FROM dbo.SICC_Tasa AS Tasa
LEFT OUTER JOIN dbo.SICC_Moneda moneda ON Tasa.IdMoneda = moneda.IdMoneda;
GO
