SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  VIEW [dbo].[SICCMX_VW_TotalCreditoHipotecarioRC]        
AS        
SELECT per.Codigo AS CodigoCliente,hip.Codigo AS CodigoCredito,hipInfo.FechaVencimiento, (CONVERT(MONEY,SaldoCapitalVencido)  
+CONVERT(MONEY,SaldoCapitalVigente)+CONVERT(MONEY,InteresVencido)+CONVERT(MONEY,InteresVigente) )  AS Saldo,        
(SELECT Fecha from SICC_Periodo WHERE Activo =1 ) AS Fecha, DATEDIFF(DAY,(select Fecha from SICC_Periodo WHERE Activo =1 ),FechaVencimiento) AS Plazo    
, mon.CodigoInterno AS Moneda      
FROM dbo.SICCMX_Hipotecario hip   
INNER JOIN dbo.SICCMX_HipotecarioInfo hipInfo ON hip.IdHipotecario = hipInfo.IdHipotecario  
INNER JOIN dbo.SICCMX_Persona per ON hip.IdPersona = per.IdPersona   
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = hipInfo.IdMoneda  
GO
