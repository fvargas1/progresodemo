SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_Inconsistencia]
AS
SELECT
	Inconsistencia.IdInconsistencia,
	Inconsistencia.Nombre,
	Inconsistencia.Descripcion,
	Inconsistencia.CodenameCount,
	Inconsistencia.Codename,
	Inconsistencia.IdCategoria,
	idcategoria.Nombre AS CategoriaNombre,
	Inconsistencia.Activo,
	Inconsistencia.Regulatorio,
	Inconsistencia.FechaCreacion,
	Inconsistencia.FechaActualizacion,
	Inconsistencia.Username
FROM dbo.MIGRACION_Inconsistencia AS Inconsistencia
LEFT OUTER JOIN dbo.MIGRACION_Categoria idcategoria ON Inconsistencia.IdCategoria = idcategoria.IdCategoria;
GO
