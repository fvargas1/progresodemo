SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04H0493]
AS
SELECT DISTINCT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	MontoTotalLiquidacion,
	MontoPagoLiquidacion,
	MontoBonificaciones,
	ValorBienAdjudicado,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04H0493;
GO
