SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Moneda_Credito_Rep]

AS

SELECT

	cre.IdCredito,

	cre.Codigo,

	cre.IdMoneda,

	cre.EsMultimoneda,

	mon.Codigo AS CodigoMoneda,

	mon.Nombre AS NombreMoneda,

	CASE WHEN ISNULL(cre.EsMultimoneda,0) = 0 THEN mon.CodigoCNBV ELSE monMul.CodigoCNBV END AS CodigoCNBVMoneda,

	mon.CodigoISO AS CodigoISOMoneda

FROM dbo.SICCMX_Credito cre

INNER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda

INNER JOIN dbo.SICC_Moneda monMul ON monMul.Codigo = '999';
GO
