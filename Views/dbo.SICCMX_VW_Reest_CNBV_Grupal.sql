SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Reest_CNBV_Grupal]
AS
SELECT lin.IdLineaCredito, rst.CNBV
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta AND ta.Codigo = '140'
CROSS APPLY (
 SELECT TOP 5 c.CNBV
 FROM dbo.SICCMX_LineaCredito l
 INNER JOIN dbo.SICCMX_CreditoCNBV c ON l.Codigo = c.NumeroLinea
 WHERE l.CodigoCreditoReestructurado = lin.Codigo
 ORDER BY l.MontoLinea DESC, l.Codigo
) AS rst;
GO
