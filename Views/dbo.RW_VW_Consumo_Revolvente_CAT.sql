SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Consumo_Revolvente_CAT]
AS
SELECT DISTINCT
	Fecha,
	FolioCredito,
	CatContrato,
	LimCredito,
	Producto,
	ISNULL(FolioCredito,'')+'|'+ISNULL(Producto,'') AS EVERYTHING
FROM dbo.RW_Consumo_Revolvente_CAT;
GO
