SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0473]
AS
SELECT DISTINCT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActividadEconomica,
	GrupoRiesgo,
	LocalidadDeudor,
	DomicilioMunicipio,
	DomicilioEstado,
	NumInfoCrediticia,
	LEI,
	TipoAlta,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	GrupalCNBV,
	MontoLineaAutorizado,
	FechaMaxDisponer,
	FechaVencLinea,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	PrelacionPago,
	NoRUGM,
	PrctPartFed,
	InstAgenciaExt,
	TasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecRevTasa,
	PeriodicidadCapital,
	PeriodicidadInteres,
	MesesGraciaCapital,
	MesesGraciaInteres,
	ComAperturaTasa,
	ComAperturaMonto,
	ComDispTasa,
	ComDispMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito,
	ISNULL(CodigoPersona,'')+'|'+ISNULL(RFC,'')+'|'+ISNULL(NombrePersona,'')+'|'+ISNULL(CodigoCredito,'')+'|'
	+ISNULL(CodigoCreditoCNBV,'') AS EVERYTHING
FROM dbo.RW_R04C0473;
GO
