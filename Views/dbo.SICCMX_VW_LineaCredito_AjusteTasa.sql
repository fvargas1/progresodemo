SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCredito_AjusteTasa]
AS
SELECT
 IdLineaCredito,
 CASE WHEN EsMultimoneda = 1 THEN '0' ELSE
 CASE WHEN SUBSTRING(AjusteTasaReferencia,1,1) IN ('+','-','*') THEN SUBSTRING(AjusteTasaReferencia,1,1) ELSE '' END +
 ISNULL(CAST(CAST(REPLACE(REPLACE(REPLACE(NULLIF(AjusteTasaReferencia,''),'-',''),'*',''),'+','') AS DECIMAL(18,6)) AS VARCHAR(20)),'0') END AS AjusteTasa,
 CASE
 WHEN AjusteTasaReferencia LIKE '%+%' OR SUBSTRING(AjusteTasaReferencia,1,1) NOT IN ('+','-','*') OR EsMultimoneda = 1 THEN '110'
 WHEN AjusteTasaReferencia LIKE '%*%' THEN '111'
 ELSE '112'
 END AS OperacionDiferencial,
 CASE WHEN EsMultimoneda = 1 THEN '0' ELSE FrecuenciaRevisionTasa END AS FrecuenciaRevisionTasa
FROM dbo.SICCMX_LineaCredito;
GO
