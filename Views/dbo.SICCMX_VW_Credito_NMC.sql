SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_NMC]
AS
SELECT
 cre.IdCredito,
 cre.Codigo AS CodigoCredito,
 cre.PorcentajeFondeo,
 cre.SaldoCapitalVigente,
 cre.InteresVigente,
 cre.SaldoCapitalVencido,
 cre.InteresVencido,
 cre.InteresRefinanciado,
 CAST(cre.InteresRefinanciado * tc.Valor AS DECIMAL(23,2)) AS InteresRefinanciadoValorizado,
 cre.FechaTraspasoVigente,
 cre.FechaTraspasoVencida,
 cre.FechaProximaAmortizacion,
 cre.FechaAutorizacionOriginal,
 cre.MontoOriginal,
 CAST(cre.MontoOriginal * tc.Valor AS DECIMAL(23,2)) AS MontoOriginalValorizado,
 cre.IdReestructura,
 reest.Nombre AS Reestructura,
 cInfo.IdPeriodicidadCapital,
 perCap.Nombre AS PeriodicidadCapital,
 cInfo.IdPeriodicidadInteres,
 perInt.Nombre AS PeriodicidadInteres,
 cre.IdSituacionCredito,
 sitCre.Nombre AS Situacion,
 cre.IdPersona,
 per.Nombre AS PersonaNombre,
 cre.IdClasificacionContable,
 cre.IdMoneda,
 mon.Nombre AS Moneda,
 cre.IdDestino,
 cre.IdTasaReferencia,
 cre.TasaBruta,
 cre.MontoLineaCredito,
 CAST(cre.MontoLineaCredito * tc.Valor AS DECIMAL(23,2)) AS MontoLineaValorizado,
 cre.NumeroAgrupacion,
 cre.FechaDisposicion,
 cre.FechaVencimiento,
 cre.FrecuenciaRevisionTasa,
 cre.IdLineaCredito,
 cre.NumeroReestructura,
 cre.Formalizda,
 cre.Recuperacion,
 cre.Expediente,
 cre.MesesIncumplimiento,
 cre.IdMetodologia,
 cre.IdFondeo,
 cre.InteresCarteraVencida,
 CAST(cre.InteresCarteraVencida * tc.Valor AS DECIMAL(23,2)) AS InteresCarteraVencidaValorizado,
 pos.Codigo AS Posicion,
 CAST(cre.SaldoCapitalVigente * tc.Valor AS DECIMAL(23,2)) AS SaldoVigenteValorizado,
 CAST(cre.InteresVigente * tc.Valor AS DECIMAL(23,2)) AS InteresVigenteValorizado,
 CAST(cre.SaldoCapitalVencido * tc.Valor AS DECIMAL(23,2)) AS SaldoVencidoValorizado,
 CAST(cre.InteresVencido * tc.Valor AS DECIMAL(23,2)) AS InteresVencidoValorizado,
 CAST((cre.SaldoCapitalVigente * tc.Valor)+(cre.InteresVigente * tc.Valor)+(cre.SaldoCapitalVencido * tc.Valor)+(cre.InteresVencido * tc.Valor) AS DECIMAL(23,2)) AS MontoValorizado,
 CAST(cre.SaldoCapitalVigente * tc.Valor AS DECIMAL)+CAST(cre.InteresVigente * tc.Valor AS DECIMAL)+CAST(cre.SaldoCapitalVencido * tc.Valor AS DECIMAL)+CAST(cre.InteresVencido * tc.Valor AS DECIMAL) AS MontoValorizadoRound,
 linea.Codigo AS NumeroLinea,
 tli.Codigo AS TipoLinea,
 crv.EI_Total,
 cre.IdDestinoCreditoMA,
 cInfo.IdPeriodicidadCapitalMA,
 cInfo.IdPeriodicidadInteresMA,
 linea.MontoLinea,
 ISNULL(cInfo.InteresDelMes,0) AS InteresDelMes,
 ISNULL(cInfo.InteresDelMes,0) * tc.Valor AS InteresDelMesValorizado
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo periodo ON periodo.IdPeriodo = tc.IdPeriodo AND periodo.Activo=1
LEFT OUTER JOIN dbo.SICCMX_LineaCredito linea ON cre.IdLineaCredito = linea.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TipoLinea tli ON cInfo.TipoLinea = tli.IdTipoLinea
LEFT OUTER JOIN dbo.SICC_Posicion pos ON cInfo.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICC_Reestructura reest ON reest.IdReestructura = cre.IdReestructura
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON cInfo.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON cInfo.IdPeriodicidadInteres = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON cre.IdSituacionCredito = sitCre.IdSituacionCredito
WHERE met.Codigo IN ('4','18','20','21','22');
GO
