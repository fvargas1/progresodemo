SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_0442]
AS
SELECT DISTINCT
	cre.IdLineaCredito,
	CASE WHEN SUM(ISNULL(crv.EI_Total,0)) > 0 THEN dbo.MIN2VAR(SUM(ISNULL(crv.EI_Total,0)), SUM(ISNULL(canCub.MontoCubierto,0))) / SUM(ISNULL(crv.EI_Total,0)) ELSE 0 END AS PorcentajeCubierto,
	CASE WHEN SUM(ISNULL(crv.EI_Total,0)) > 0 THEN dbo.MIN2VAR(SUM(ISNULL(crv.EI_Total,0)), SUM(ISNULL(fondo.MontoCubierto,0))) / SUM(ISNULL(crv.EI_Total,0)) ELSE 0 END AS PorcentajeFondo,
	MAX(fondo.Institucion) AS InstitucionFondo,
	CASE WHEN SUM(ISNULL(crv.EI_Total,0)) > 0 THEN dbo.MIN2VAR(SUM(ISNULL(crv.EI_Total,0)), SUM(ISNULL(aval.MontoCubierto,0))) / SUM(ISNULL(crv.EI_Total,0)) ELSE 0 END AS PorcentajeAval,
	CASE WHEN SUM(reales.Cantidad) > 1 THEN '10' ELSE MAX(reales.Codigo) END AS TipoGarReal
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN (
	SELECT can.IdCredito, SUM(ISNULL(can.MontoCobAjust,0)) AS MontoCubierto
	FROM dbo.SICCMX_Garantia_Canasta can
	WHERE ISNULL(can.EsDescubierto,0) = 0
	GROUP BY can.IdCredito
) AS canCub ON cre.IdCredito = canCub.IdCredito
LEFT OUTER JOIN (
	SELECT cg.IdCredito, SUM(ISNULL(can.MontoCobAjust,0)+ISNULL(cgPP.ReservaCubierta,0)) AS MontoCubierto, MAX(inst.Codigo) AS Institucion
	FROM dbo.SICCMX_CreditoGarantia cg
	INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
	INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
	LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta can ON cg.IdCredito = can.IdCredito AND tg.IdTipoGarantia = can.IdTipoGarantia
	LEFT OUTER JOIN dbo.SICCMX_CreditoGarantia_PP cgPP ON cg.IdCredito = cgPP.IdCredito
	LEFT OUTER JOIN dbo.SICC_Institucion inst ON g.IdBancoGarantia = inst.IdInstitucion
	WHERE g.Aplica = 1 AND tg.Codigo IN ('NMC-04','NMC-05')
	GROUP BY cg.IdCredito
) AS fondo ON cre.IdCredito = fondo.IdCredito
LEFT OUTER JOIN (
	SELECT ca.IdCredito, ISNULL(ca.MontoCobAjust,0) AS MontoCubierto
	FROM dbo.SICCMX_Garantia_Canasta ca
	INNER JOIN dbo.SICC_TipoGarantia tg ON ca.IdTipoGarantia = tg.IdTipoGarantia
	WHERE tg.Codigo='GP'
) AS aval ON cre.IdCredito = aval.IdCredito
LEFT OUTER JOIN (
	SELECT cg.IdCredito, tgMA.Codigo, COUNT(cg.IdCredito) OVER (PARTITION BY cg.IdCredito) AS Cantidad
	FROM dbo.SICCMX_CreditoGarantia cg
	INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
	LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
	LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA tgMA ON g.IdTipoGarantiaMA = tgMA.IdTipoGarantia
	WHERE g.Aplica = 1 AND tg.IdClasificacionNvaMet IN (1,2)
) AS reales ON cre.IdCredito = reales.IdCredito
GROUP BY cre.IdLineaCredito;
GO
