SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Datos_0442]


AS


SELECT


 0 AS IdPeriodoHistorico,


 'A' AS TipoRegistro,


 CodigoCredito,


 CodigoCreditoCNBV,


 SaldoCredito AS MontoLineaAutorizado,


 NULL AS SaldoFinal,


 PeriodicidadPagosCapital,
 
 '0' AS NumeroDisposicion


FROM dbo.RW_R04C0442





UNION ALL





SELECT


 r42.IdPeriodoHistorico,


 'H',


 r42.CodigoCredito,


 r42.CodigoCreditoCNBV,


 r42.SaldoCredito AS MontoLineaAutorizado,


 r43.SaldoFinal,


 r42.PeriodicidadPagosCapital,
 
 r43.NumeroDisposicion


FROM Historico.RW_R04C0442 r42


INNER JOIN Historico.RW_R04C0443 r43 ON r42.IdPeriodoHistorico = r43.IdPeriodoHistorico AND r42.CodigoCreditoCNBV = r43.CodigoCreditoCNBV


INNER JOIN dbo.SICC_PeriodoHistorico ph ON r42.IdPeriodoHistorico = ph.IdPeriodoHistorico


WHERE ph.Activo = 1;
GO
