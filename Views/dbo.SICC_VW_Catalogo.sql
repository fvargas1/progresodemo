SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_Catalogo]
AS
SELECT
	cat.IdCatalogo,
	cat.Nombre,
	cat.Descripcion,
	cat.SPGet,
	cat.Activo
FROM dbo.SICC_Catalogo cat;
GO
