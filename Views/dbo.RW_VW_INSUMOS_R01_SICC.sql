SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_INSUMOS_R01_SICC]
AS
SELECT DISTINCT
	FechaPeriodo,
	Concepto,
	MonedaISO,
	SaldoValorizado,
	ISNULL(FechaPeriodo,'')+'|'+ISNULL(Concepto,'')+'|'+ISNULL(MonedaISO,'')+'|'+ISNULL(SaldoValorizado,'') AS EVERYTHING
FROM dbo.RW_INSUMOS_R01_SICC;
GO
