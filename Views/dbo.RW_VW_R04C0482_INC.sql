SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0482_INC]
AS
SELECT
	IdReporteLog,
	Formulario,
	CodigoCreditoCNBV,
	NULL AS CodigoCredito,
	NULL AS NombrePersona,
	TipoBaja,
	SaldoPrincipalInicio,
	SaldoInsoluto,
	MontoPagado,
	MontoCastigos,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificaciones,
	MontoDescuentos,
	MontoBienDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM dbo.RW_R04C0482_2016
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2016'

UNION ALL
SELECT
	IdReporteLog,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NombrePersona,
	TipoBaja,
	SaldoInicial,
	ResponsabilidadInicial,
	MontoPagado,
	MontoQuitCastReest,
	NULL AS MontoCondonacion,
	NULL AS MontoQuitas,
	MontoBonificaciones,
	NULL AS MontoDescuentos,
	NULL AS MontoBienDacion,
	NULL AS ReservasCalifCanceladas,
	NULL AS ReservasAdicCanceladas
FROM dbo.RW_R04C0482
INNER JOIN dbo.BAJAWARE_Config cfg ON cfg.CodeName = 'ANIO_REP' AND cfg.[Value] = '2015';
GO
