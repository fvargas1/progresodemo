SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_InsumosRC15]
AS
SELECT
 cre.Codigo AS CodigoCredito,
 per.Codigo AS CodigoPersona,
 per.RFC,
 ISNULL(crevar.ReservaFinal,0) AS MontoReservas,
 ISNULL(GarReales.MontoGarantiasReales,0) [MontoGarantiasReales] ,
 ISNULL(aval.MontoCubierto,0) AS MontoAvales,
 cre.FechaDisposicion[FechaDisposicion],
 cre.IdSituacionCredito [IdSituacionCredito],
 --crevar.EI_Total
 0 as  [Exposicion]
--crevar.Exposicion
FROM 
dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Persona per ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crevar ON crevar.IdCredito = cre.IdCredito
LEFT OUTER JOIN SICCMX_VW_Cobertura_Personales aval ON aval.IdCredito = cre.IdCredito
LEFT OUTER JOIN (
	SELECT
	IdCredito,
	CAST(SUM(ValorGarantia) AS DECIMAL(23,2)) AS MontoGarantiasReales
	FROM dbo.SICCMX_VW_Garantias_NM
	WHERE IdClasificacionNvaMet IN (1,2)
	GROUP BY IdCredito, MontoCredito ) GarReales ON GarReales.IdCredito = cre.IdCredito

GO
