SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0411MN_2016_VW_Consolidado]
AS
SELECT
	Codigo,
	Concepto,
	Padre,
	SUM(TotalVigente_TotalVigente) AS TotalVigente_TotalVigente,
	SUM(CarteraVigente_TotalVigente) AS CarteraVigente_TotalVigente,
	SUM(CarteraVigente_Principal) AS CarteraVigente_Principal,
	SUM(CarteraVigente_InteresDevengado) AS CarteraVigente_InteresDevengado,
	SUM(SinPagos_TotalVigente) AS SinPagos_TotalVigente,
	SUM(SinPagos_Principal) AS SinPagos_Principal,
	SUM(SinPagos_InteresDevengado) AS SinPagos_InteresDevengado,
	SUM(ConPagosVencidos_TotalVigente) AS ConPagosVencidos_TotalVigente,
	SUM(ConPagosVencidos_Principal) AS ConPagosVencidos_Principal,
	SUM(ConPagosVencidos_InteresDevengado) AS ConPagosVencidos_InteresDevengado,
	SUM(CarteraVencida_TotalVigente) AS CarteraVencida_TotalVigente,
	SUM(CarteraVencida_Principal) AS CarteraVencida_Principal,
	SUM(CarteraVencida_InteresDevengado) AS CarteraVencida_InteresDevengado
FROM dbo.RW_R04A0411MN_2016_VW
GROUP BY Codigo, Concepto, Padre, IdReporteLog;
GO
