SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Consumo_GRUPAL_Baja]
AS
SELECT DISTINCT
	FolioCredito,
	TipoCredito,
	FechaBajaCredito,
	TipoBajaCredito,
	QuitasCondonacionesBonificacionesDescuentos,
	ISNULL(FolioCredito,'')+'|'+ISNULL(TipoCredito,'')+'|'+ISNULL(TipoBajaCredito,'') AS EVERYTHING
FROM dbo.RW_Consumo_GRUPAL_Baja;
GO
