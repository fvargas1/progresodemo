SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_LineaCredito]


AS


SELECT DISTINCT


 lin.IdLineaCredito,


 lin.Codigo,


 lin.IdPersona,


 lin.FechaDisposicion,


 lin.IdMoneda,


 lin.IdDestino,


 lin.IdTasaReferencia,


 lin.MontoLinea,


 lin.MontoLinea AS MontoLineaValorizado,


 CASE WHEN lin.EsMultimoneda = 1 THEN CAST(lin.MontoLinea AS DECIMAL(23,2)) ELSE CAST(lin.MontoLinea AS DECIMAL(23,2)) END AS MontoLinea_Alta,


 lin.FrecuenciaRevisionTasa,


 lin.IdTipoAlta,


 lin.IdDisposicion,


 lin.AjusteTasaReferencia,


 lin.IdInstitucionFondea,


 lin.ComisionesCobradas,


 CAST(lin.ComisionesCobradas * tc.Valor AS DECIMAL(23,2)) AS ComisionesCobradasValorizado,


 lin.IdTipoBaja,


 lin.ImporteQuebranto,


 CAST(lin.ImporteQuebranto * tc.Valor AS DECIMAL(23,2)) AS ImporteQuebrantoValorizado,


 lin.SaldoInicial,


 CAST(lin.SaldoInicial * tc.Valor AS DECIMAL(23,2)) AS SaldoInicialValorizado,


 lin.IdPeriodicidadCapital,


 lin.IdPeriodicidadInteres,


 lin.CodigoCreditoReestructurado,


 lin.ProductoComercial,


 lin.Posicion,


 lin.TipoLinea,


 lin.TipoOperacion,


 lin.FecMaxDis,


 lin.PorcPartFederal,


 lin.MontoPagEfeCap,


 CAST(lin.MontoPagEfeCap * tc.Valor AS DECIMAL(23,2)) AS MontoPagEfeCapValorizado,


 lin.MontoPagEfeInt,


 CAST(lin.MontoPagEfeInt * tc.Valor AS DECIMAL(23,2)) AS MontoPagEfeIntValorizado,


 lin.MontoPagEfeCom,


 CAST(lin.MontoPagEfeCom * tc.Valor AS DECIMAL(23,2)) AS MontoPagEfeComValorizado,


 lin.MontoPagEfeIntMor,


 CAST(lin.MontoPagEfeIntMor * tc.Valor AS DECIMAL(23,2)) AS MontoPagEfeIntMorValorizado,


 lin.ComDispTasa,


 lin.ComDispMonto,


 CAST(lin.ComDispMonto * tc.Valor AS DECIMAL(23,2)) AS ComDispMontoValorizado,


 lin.GastosOrigTasa,


 lin.ResTotalInicioPer,


 CAST(lin.ResTotalInicioPer * tc.Valor AS DECIMAL(23,2)) AS ResTotalInicioPerValorizado,


 lin.MontoQuitasCastQue,


 CAST(lin.MontoQuitasCastQue * tc.Valor AS DECIMAL(23,2)) AS MontoQuitasCastQueValorizado,


 lin.MontoBonificacionDesc,


 CAST(lin.MontoBonificacionDesc * tc.Valor AS DECIMAL(23,2)) AS MontoBonificacionDescValorizado,


 lin.MesesGraciaCap,


 lin.MesesGraciaIntereses,


 lin.NumEmpLoc,


 lin.NumEmpLocFedSHCP,


 lin.FecVenLinea,


 lin.CAT,


 lin.MontoLineaSinA,


 CAST(lin.MontoLineaSinA * tc.Valor AS DECIMAL(23,2)) AS MontoLineaSinAValorizado,


 lin.MontoPrimasAnuales,


 CAST(lin.MontoPrimasAnuales * tc.Valor AS DECIMAL(23,2)) AS MontoPrimasAnualesValorizado,


 lin.EsPadre,


 lin.IdMunicipioDestino,


 lin.IdActividadEconomicaDestino,


 lin.FolioConsultaBuro,


 lin.CodigoCNBV,


 lin.Fuente,


 lin.EsMultimoneda,


 lin.IdDestinoCreditoMA,


 lin.IdTipoAltaMA,


 lin.IdDisposicionMA,


 lin.IdPeriodicidadCapitalMA,


 lin.IdPeriodicidadInteresMA,


 lin.IdTipoBajaMA,


 lin.NumeroAgrupacion,


 lin.MontoBancaDesarrollo,


 CAST(lin.MontoBancaDesarrollo * tc.Valor AS DECIMAL(23,2)) AS MontoBancaDesarrolloValorizado,


 lin.ComisionDelMes,


 CAST(lin.ComisionDelMes * tc.Valor AS DECIMAL(23,2)) AS ComisionDelMesValorizado,


 lin.MontoCastigos,


 CAST(lin.MontoCastigos * tc.Valor AS DECIMAL(23,2)) AS MontoCastigosValorizado,


 lin.MontoQuebrantos,


 CAST(lin.MontoQuebrantos * tc.Valor AS DECIMAL(23,2)) AS MontoQuebrantosValorizado,


 lin.MontoDescuentos,


 CAST(lin.MontoDescuentos * tc.Valor AS DECIMAL(23,2)) AS MontoDescuentosValorizado,


 lin.MontoDacion,


 CAST(lin.MontoDacion * tc.Valor AS DECIMAL(23,2)) AS MontoDacionValorizado,


 lin.LocalidadDestino,


 lin.EstadoDestino


FROM dbo.SICCMX_LineaCredito lin


INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = lin.IdMoneda


INNER JOIN dbo.SICC_Periodo prd ON prd.IdPeriodo = tc.IdPeriodo AND prd.Activo = 1


INNER JOIN dbo.SICC_TipoCambio tcUSD ON prd.IdPeriodo = tcUSD.IdPeriodo


INNER JOIN dbo.SICC_Moneda monUSD ON tcUSD.IdMoneda = monUSD.IdMoneda AND monUSD.Codigo = '1';
GO
