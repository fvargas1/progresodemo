SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MIGRACION_VW_InconsistenciaResumen]
AS
SELECT
	InconsistenciaResumen.IdInconsistenciaResumen,
	InconsistenciaResumen.IdInconsistencia,
	idinconsistencia.Nombre AS InconsistenciaNombre,
	InconsistenciaResumen.Numero,
	InconsistenciaResumen.Fecha,
	idinconsistencia.IdCategoria,
	c.Nombre AS CategoriaNombre
FROM dbo.MIGRACION_InconsistenciaResumen AS InconsistenciaResumen
INNER JOIN dbo.MIGRACION_Inconsistencia idinconsistencia ON InconsistenciaResumen.IdInconsistencia = idinconsistencia.IdInconsistencia
LEFT OUTER JOIN dbo.MIGRACION_Categoria c ON idinconsistencia.IdCategoria = c.IdCategoria;
GO
