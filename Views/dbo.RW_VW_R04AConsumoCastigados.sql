SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04AConsumoCastigados]
AS
SELECT c.Codigo, m.Monto
FROM dbo.SICCMX_Consumo c
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables r ON r.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Movimientos m ON m.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = m.IdTipoMovimiento
WHERE r.ReservaCubierta + r.ReservaExpuesta = 0 AND tm.Codigo = 'CAST';
GO
