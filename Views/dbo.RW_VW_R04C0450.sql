SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_R04C0450]
AS
SELECT DISTINCT
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia,
	ISNULL(RFC_Garante,'')+'|'+ISNULL(NombreGarante,'')+'|'+ISNULL(CodigoGarante,'')+'|'+ISNULL(CodigoCreditoCNBV,'')+'|'+ISNULL(CodigoCredito,'')
	+'|'+ISNULL(NumeroDisposicion,'')+'|'+ISNULL(NombreAcreditado,'')+'|'+ISNULL(TipoGarantia,'')+'|'+ISNULL(CodigoGarantia,'') AS EVERYTHING
FROM dbo.RW_R04C0450;
GO
