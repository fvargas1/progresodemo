SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_TasaRef_Credito_Rep]
AS
SELECT
	cre.IdCredito,
	cre.Codigo,
	cre.IdMoneda,
	cre.EsMultimoneda,
	tr.Codigo AS CodigoTasaRef,
	tr.Codename AS CodenameTasaRef,
	CASE WHEN cre.EsMultimoneda = 1 THEN tr.Codigo ELSE trMul.Codigo END AS CodigoCNBVTasaRef
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICC_TasaReferencia tr ON cre.IdTasaReferencia = tr.IdTasaReferencia
INNER JOIN dbo.SICC_TasaReferencia trMul ON trMul.Codigo = '999';
GO
