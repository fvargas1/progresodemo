SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Consumo_Var_Historico]
AS
SELECT
 con.IdConsumo,
 pre.PorcentajePagoRealizado,
 pre.Impago,
 info.MontoExigible AS SaldoPagar,
 info.PagoRealizado,
 1 AS RN
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON info.IdConsumo = pre.IdConsumo

UNION ALL

SELECT
 conAct.IdConsumo,
 pre.PorcentajePagoRealizado,
 pre.Impago,
 info.MontoExigible,
 info.PagoRealizado,
 (ROW_NUMBER() OVER(PARTITION BY pre.Consumo ORDER BY pre.IdPeriodoHistorico DESC)) + 1 AS RN
FROM Historico.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN Historico.SICCMX_Consumo_Valorizado con ON pre.Consumo = con.Consumo AND pre.IdPeriodoHistorico = con.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_ConsumoInfo info ON con.IdPeriodoHistorico = info.IdPeriodoHistorico AND con.Consumo = info.Consumo
INNER JOIN dbo.SICCMX_Consumo conAct ON pre.Consumo = conAct.Codigo
INNER JOIN dbo.SICC_PeriodoHistorico hst ON pre.IdPeriodoHistorico = hst.IdPeriodoHistorico AND hst.Activo = 1
INNER JOIN dbo.SICC_Periodo per ON hst.Fecha < per.Fecha AND per.Activo = 1;
GO
