SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0420_2016_VW]
AS
SELECT
 con.Codigo,
 con.Nombre AS Concepto,
 con.Padre,
 SUM(CASE WHEN rep.Moneda = '15' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS Total,
 SUM(CASE WHEN rep.Moneda = '14' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoMN,
 SUM(CASE WHEN rep.Moneda = '4' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoUSD,
 SUM(CASE WHEN rep.Moneda = '9' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoUDIS
FROM dbo.ReportWare_VW_0420Concepto_2016 con
LEFT OUTER JOIN dbo.RW_R04A0420_2016 rep ON rep.Concepto = con.Codigo
GROUP BY con.Codigo, con.Nombre, con.Padre;
GO
