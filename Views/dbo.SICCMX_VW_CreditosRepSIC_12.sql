SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_CreditosRepSIC_12]
AS
SELECT credito.IdPersona, credito.CodigoCredito, MAX(credito.IdPeriodo) AS IdPeriodo
FROM dbo.SICCMX_Creditos_Castigados_SIC credito
GROUP BY credito.IdPersona, credito.CodigoCredito
GO
