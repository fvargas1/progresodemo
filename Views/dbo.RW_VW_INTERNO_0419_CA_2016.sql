SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_INTERNO_0419_CA_2016]
AS
SELECT DISTINCT
	Codigo,
	Producto,
	ReservaInicial,
	Cargos,
	Abonos,
	ReservaActual,
	ReservaActualCalif,
	Diferencia,
	ISNULL(Codigo,'')+'|'+ISNULL(Producto,'') AS EVERYTHING
FROM dbo.RW_INTERNO_0419_CA_2016;
GO
