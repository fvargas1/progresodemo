SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Consumo_AMB]
AS
SELECT
	con.IdConsumo,
	CASE WHEN info.AntiguedadAcreditado <= 42 AND pre.LimiteCredito <= 15000 THEN 1 ELSE 0 END AS Alto,
	CASE
	WHEN info.AntiguedadAcreditado <= 42 AND pre.LimiteCredito > 40000 THEN 1
	WHEN info.AntiguedadAcreditado > 42 AND pre.LimiteCredito <= 15000 THEN 1
	WHEN pre.LimiteCredito > 15000 AND pre.LimiteCredito <= 40000 THEN 1
	ELSE 0
	END AS Medio,
	CASE WHEN info.AntiguedadAcreditado > 42 AND pre.LimiteCredito > 40000 THEN 1 ELSE 0 END AS Bajo
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo;
GO
