SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[ReportWare_VW_0415Concepto]
AS
SELECT item.IdItemCatalogo, item.Codigo, item.Nombre, item.Padre
FROM Bajaware.ItemCatalogo item
INNER JOIN Bajaware.Catalogos cat ON cat.IdCatalogo = item.IdCatalogo
WHERE cat.Codename = 'RW_0415Concepto';
GO
