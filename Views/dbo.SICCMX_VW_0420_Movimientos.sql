SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_0420_Movimientos]
AS
-- MOVIMIENTOS COMERCIAL
SELECT
 cre.Codigo AS Codigo,
 tm.Codigo AS TipoMovimiento,
 SUM(mov.Monto) AS Monto
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Credito_Movimientos mov ON cre.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN (SELECT DISTINCT TipoMovimiento FROM R04.[0420Configuracion_2016]) conf ON tm.Codigo = conf.TipoMovimiento
GROUP BY cre.Codigo, tm.Codigo

UNION ALL
-- MOVIMIENTOS CONSUMO
SELECT
 con.Codigo,
 tm.Codigo,
 SUM(mov.Monto)
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_Consumo_Movimientos mov ON con.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN (SELECT DISTINCT TipoMovimiento FROM R04.[0420Configuracion_2016]) conf ON tm.Codigo = conf.TipoMovimiento
GROUP BY con.Codigo, tm.Codigo

UNION ALL
-- MOVIMIENTOS HIPOTECARIO
SELECT
 hip.Codigo,
 tm.Codigo,
 SUM(mov.Monto)
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos mov ON hip.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON mov.IdTipoMovimiento = tm.IdTipoMovimiento
INNER JOIN (SELECT DISTINCT TipoMovimiento FROM R04.[0420Configuracion_2016]) conf ON tm.Codigo = conf.TipoMovimiento
GROUP BY hip.Codigo, tm.Codigo
GO
