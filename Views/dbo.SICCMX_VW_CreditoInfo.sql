SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_CreditoInfo]
AS
SELECT
	cInfo.IdCredito,
	cInfo.IdTipoAlta,
	TipoAlta.Nombre AS IdTipoAltaNombre,
	cInfo.IdDisposicion,
	Disposicion.Nombre AS IdDisposicionNombre,
	cInfo.AjusteTasaReferencia,
	cInfo.MontoBancaDesarrollo,
	cInfo.IdInstitucionFondeo,
	InsFondeo.Nombre AS IdInstitucionFondeoNombre,
	cInfo.GastosOriginacion,
	cInfo.NumeroDisposicion,
	cInfo.MontoDispuesto,
	cInfo.MontoPagoExigible,
	cInfo.MontoPagosRealizados,
	cInfo.MontoInteresPagado,
	NULL AS MontoComisionDevengada,
	cInfo.DiasVencidos AS DiasVencido,
	cInfo.IdTipoBaja,
	TipoBaja.Nombre AS IdTipoBajaNombre,
	cInfo.MontoBonificacion,
	'' AS CodigoCnbv,
	'' AS NumeroConsulta,
	cInfo.SaldoInicial,
	cInfo.SaldoFinal,
	'' AS SpecsDisposicion,
	'' AS CodigoBancoGarantia
FROM dbo.SICCMX_CreditoInfo cInfo
INNER JOIN dbo.SICCMX_Credito cre ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoAlta TipoAlta ON TipoAlta.IdTipoAlta = cInfo.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoBaja TipoBaja ON TipoBaja.IdTipoBaja = cInfo.IdTipoBaja
LEFT OUTER JOIN dbo.SICC_DisposicionCredito Disposicion ON Disposicion.IdDisposicion = cInfo.IdDisposicion
LEFT OUTER JOIN dbo.SICC_Institucion InsFondeo ON InsFondeo.IdInstitucion = cInfo.IdInstitucionFondeo;
GO
