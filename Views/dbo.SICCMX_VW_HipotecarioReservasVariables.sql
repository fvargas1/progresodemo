SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_HipotecarioReservasVariables]
AS
SELECT
	res.IdHipotecario,
	res.[PI],
	res.SP,
	res.E,
	res.Reserva,
	res.PorReserva,
	res.IdCalificacion,
	cal.Codigo AS IdCalificacionNombre
FROM dbo.SICCMX_Hipotecario_Reservas_Variables res
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON res.IdCalificacion = cal.IdCalificacion;
GO
