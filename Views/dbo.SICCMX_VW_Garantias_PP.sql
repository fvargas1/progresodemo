SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_PP]
AS
SELECT
	cg.IdCredito,
	g.MontoCubierto AS Monto,
	g.PrctCubierto AS Porcentaje,
	gar.NombreGarante AS NomGar,
	cg.ReservaCubierta AS ReservaCubierto,
	cg.ReservaExpuesta AS ReservaExpuesto,
	gar.Codigo AS CodigoGarantia
FROM dbo.SICCMX_CreditoGarantia_PP cg
INNER JOIN dbo.SICCMX_Garantia_PP g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.SICCMX_Garantia gar ON g.IdGarantia = gar.IdGarantia;
GO
