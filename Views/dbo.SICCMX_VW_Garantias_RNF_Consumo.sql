SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_RNF_Consumo]
AS
SELECT
	vw.IdConsumo,
	CAST(SUM(vw.ValorGarantia) AS DECIMAL(23,2)) AS ValorGarantia,
	vw.MontoCredito,
	sp.NivelMinimo,
	sp.NivelMaximo,
	sp.SP_Consumo AS SeveridadPerdida,
	vw.IdTipoGarantia
FROM dbo.SICCMX_VW_Garantias_NM_Consumo vw
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON vw.IdTipoGarantia = sp.IdTipoGarantia
WHERE vw.IdClasificacionNvaMet=2 AND vw.Aplica=1
GROUP BY vw.IdConsumo, vw.MontoCredito, sp.NivelMinimo, sp.NivelMaximo, sp.SP_Consumo, vw.IdTipoGarantia
GO
