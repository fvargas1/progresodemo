SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Consumo_SP]
AS
SELECT
 con.IdConsumo,
 CASE WHEN 1 - (CAST(SUM(ISNULL(can.MontoRecuperacion, 0)) AS DECIMAL(25,4)) / con.E) < 0 THEN 0
 ELSE CAST(1 - (CAST(SUM(ISNULL(can.MontoRecuperacion, 0)) AS DECIMAL(25,4)) / con.E) AS DECIMAL(18,12)) END AS SP
FROM dbo.SICCMX_Consumo_Reservas_Variables con
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo can ON con.IdConsumo = can.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE ISNULL(con.E, 0) > 0
GROUP BY con.IdConsumo, con.E;
GO
