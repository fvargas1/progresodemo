SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Consumo_GRUPAL_Reestructura]
AS
SELECT DISTINCT
	FolioCredito,
	FechaReestructuraCredito,
	Reestructura,
	QuitasCondonacionesBonificacionesDescuentos,
	Folio2Credito,
	ISNULL(FolioCredito,'')+'|'+ISNULL(Reestructura,'')+'|'+ISNULL(Folio2Credito,'') AS EVERYTHING
FROM dbo.RW_Consumo_GRUPAL_Reestructura;
GO
