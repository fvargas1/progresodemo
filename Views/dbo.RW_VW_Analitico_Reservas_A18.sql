SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Analitico_Reservas_A18]
AS
SELECT DISTINCT
	Fecha,
	CodigoPersona,
	Nombre,
	CodigoCredito,
	MontoCredito,
	FechaVencimiento,
	[PI],
	Moneda,
	ActividadEconomica,
	MontoGarantia,
	Prct_Reserva,
	SP,
	MontoReserva,
	PI_Aval,
	SP_Aval,
	Calificacion,
	MontoGarantiaAjustado,
	ISNULL(CodigoPersona,'')+'|'+ISNULL(Nombre,'')+'|'+ISNULL(CodigoCredito,'')+'|'+ISNULL(Calificacion,'') AS EVERYTHING
FROM dbo.RW_Analitico_Reservas_A18;
GO
