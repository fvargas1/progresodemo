SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_ItemCatalogos]
AS
SELECT cat.IdCatalogo, item.Codigo, item.Nombre
FROM Bajaware.Catalogos cat
INNER JOIN Bajaware.ItemCatalogo item ON cat.IdCatalogo = item.IdCatalogo
GO
