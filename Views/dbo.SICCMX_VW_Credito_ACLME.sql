SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Credito_ACLME]    
AS       
SELECT    
Credito.IdCredito,    
Credito.Codigo,     
Credito.SaldoCapitalVigente,    
Credito.InteresVigente,    
Credito.SaldoCapitalVencido,    
Credito.InteresVencido,    
Credito.FechaTraspasoVencida,    
Credito.FechaProximaAmortizacion,    
Credito.InteresRefinanciado,    
Credito.FechaAutorizacionOriginal,    
Credito.MontoOriginal,    
--Credito.Morosidad,    
Credito.IdReestructura,    
Reestructura.Nombre AS Reestructura,    
--Credito.IdPeriodicidadInteres,    
--PeriodicidadInteres.Nombre AS PeriodicidadInteres,    
Credito.IdSituacionCredito,    
Situacion.Nombre AS Situacion,    
Credito.IdPersona,    
Persona.Nombre AS PersonaNombre,    
tp.Codigo AS IdTipoPersona,  
tp.Nombre AS TipoPersona,  
Credito.IdClasificacionContable,    
Clasificacion.Nombre AS ClasificacionContable,    
--Credito.IdTipoCredito,    
Credito.IdMoneda,    
Moneda.Nombre AS Moneda,    
Moneda.CodigoCNBV AS monedaCNBV,    
Credito.IdDestino,    
Destino.Nombre AS Destino,    
Credito.IdTasaReferencia,    
TasaReferencia.Nombre AS TasaReferencia,    
--Credito.IdPeriodicidadCapital,    
--PeriodicidadCapital.Nombre AS PeriodicidadCapital,    
--Credito.IdClasificacionLegal,    
--ClasificacionLegal.Nombre AS ClasificacionLegal,    
Credito.TasaBruta,    
Credito.MontoLineaCredito,    
Credito.NumeroAgrupacion,    
Credito.FechaDisposicion,    
Credito.FechaVencimiento,    
--Credito.MontoAmortizacion,    
--Credito.MontoInteresCobrar,    
Credito.FrecuenciaRevisionTasa,    
--Credito.NumeroLinea,    
--Credito.CuentaContable,    
Credito.FechaTraspasoVigente,    
--Credito.NumeroReestructuras,    
Credito.Formalizda,    
Credito.Recuperacion,    
Credito.Expediente,    
Credito.MesesIncumplimiento,    
Credito.IdMetodologia,    
--Credito.FuentePagoFederal,    
Credito.IdFondeo,    
Fondeo.Nombre AS Fondeo,    
Credito.SaldoCapitalVigente*tipocambio.Valor AS SaldoVigenteValorizado,    
Credito.InteresVigente*tipocambio.Valor AS InteresVigenteValorizado,    
Credito.SaldoCapitalVencido*tipocambio.Valor AS SaldoVencidoValorizado,    
Credito.InteresVencido*tipocambio.Valor AS InteresVencidoValorizado,    
tipocambio.Valor AS TipoCambioValor,    
--entidad.IdEntidad,    
--entidad.Nombre AS IdEntidadNombre,    
cInfo.CodigoCreditoReestructurado AS CodigoCreditoRestructura,    
ISNULL(Credito.SaldoCapitalVigente*tipocambio.Valor, 0) +    
ISNULL(Credito.InteresVigente*tipocambio.Valor, 0) +    
ISNULL(Credito.SaldoCapitalVencido*tipocambio.Valor, 0) +    
ISNULL(Credito.InteresVencido*tipocambio.Valor, 0) AS MontoValorizado,    
Credito.IdLineaCredito AS IdLineaCredito,    
--resVar.SPExpuesta AS SPExpuesta,    
--resVar.SPCubierta AS SPCubierta,    
--resVar.SeveridadPerdida AS SeveridadPerdida,    
--resVar.EExpuesta AS ExpExpuesta,    
--resVar.ECubierta AS ExpCubierta,    
--resVar.Exposicion AS Exposicion,    
resVar.ReservaExpuesta AS ReservaExpuesta,    
resVar.ReservaCubierta AS ReservaCubierta,    
resVar.ReservaFinal AS ReservaFinal,    
--resVar.Posicion AS Posicion,    
--resVar.TipoLinea AS TipoLinea,    
resVar.PorExpuesta AS PorExpuesto,    
resVar.PorCubierto AS PorCubierto,    
resVar.PorReservaFinal AS PorReservaFinal,    
resVar.CalifFinal AS IdCalifFinal,  
calMet.Codigo AS CalifFinal  
FROM dbo.SICCMX_Credito Credito    
INNER JOIN dbo.SICCMX_Persona Persona ON Persona.IdPersona = Credito.IdPersona    
INNER JOIN dbo.SICCMX_PersonaInfo pinfo ON Persona.IdPersona = pinfo.IdPersona  
INNER JOIN dbo.SICC_Moneda Moneda ON Moneda.IdMoneda = Credito.IdMoneda    
INNER JOIN dbo.SICC_TipoCambio tipocambio ON tipocambio.IdMoneda = Moneda.IdMoneda    
INNER JOIN dbo.SICC_Periodo periodo ON periodo.IdPeriodo = tipocambio.IdPeriodo AND periodo.Activo=1    
LEFT OUTER JOIN dbo.SICCMX_CreditoInfo cInfo ON Credito.IdCredito = cInfo.IdCredito   
LEFT OUTER JOIN dbo.SICC_TipoPersona tp ON tp.IdTipoPersona = pinfo.IdTipoPersona   
--LEFT OUTER JOIN dbo.SICC_Institucion Institucion ON Institucion.IdInstitucion = Credito.IdInstitucionOrigen    
LEFT OUTER JOIN dbo.SICC_Reestructura Reestructura ON Reestructura.IdReestructura = Credito.IdReestructura    
LEFT OUTER JOIN dbo.SICC_SituacionCredito Situacion ON Situacion.IdSituacionCredito = Credito.IdSituacionCredito    
--LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresAnterior PeriodicidadInteres ON PeriodicidadInteres.IdPeriodicidadInteres = Credito.IdPeriodicidadInteres    
LEFT OUTER JOIN dbo.SICC_ClasificacionContable Clasificacion ON Clasificacion.IdClasificacionContable = Credito.IdClasificacionContable    
LEFT OUTER JOIN dbo.SICC_Destino Destino ON Destino.IdDestino = Credito.IdDestino    
LEFT OUTER JOIN dbo.SICC_TasaReferencia TasaReferencia ON TasaReferencia.IdTasaReferencia = Credito.IdTasaReferencia    
--LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalAnterior PeriodicidadCapital ON PeriodicidadCapital.IdPeriodicidadCapital = Credito.IdPeriodicidadCapital    
--LEFT OUTER JOIN dbo.SICC_ClasificacionLegal ClasificacionLegal ON ClasificacionLegal.IdClasificacionLegal = Credito.IdClasificacionLegal    
LEFT OUTER JOIN dbo.SICC_Fondeo Fondeo ON Fondeo.IdFondeo = Credito.IdFondeo    
--LEFT OUTER JOIN dbo.SICCMX_Entidad entidad ON credito.IdEntidad = entidad.IdEntidad    
LEFT OUTER JOIN dbo.SICCMX_Credito_Reservas_Variables resVar ON Credito.IdCredito = resVar.IdCredito    
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calMet ON calMet.IdCalificacion = resVar.CalifFinal  
WHERE Credito.IdMetodologia IN (20,21,22)  

GO
