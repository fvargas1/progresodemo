SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_Periodo]
AS
SELECT
	Periodo.IdPeriodo,
	Periodo.Fecha,
	Periodo.Trimestral,
	Periodo.Activo
FROM dbo.SICC_Periodo AS Periodo
GO
