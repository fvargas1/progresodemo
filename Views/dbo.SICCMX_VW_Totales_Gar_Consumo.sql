SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Totales_Gar_Consumo]
AS
SELECT
	vwNM.IdConsumo,
	vwNM.He,
	SUM(CASE WHEN vwTot.MontoTotal > 0 THEN ((vwNM.ValorGarantia / vwTot.MontoTotal) * vwNM.Hc) ELSE 0 END) AS Hc,
	SUM(CASE WHEN vwTot.MontoTotal > 0 THEN ((vwNM.ValorGarantia / vwTot.MontoTotal) * vwNM.Hfx) ELSE 0 END) AS Hfx,
	vwTot.MontoTotal,
	vwNM.MontoCredito
FROM dbo.SICCMX_VW_Garantias_NM_Consumo vwNM
INNER JOIN dbo.SICCMX_VW_Garantia_TotGRF_Consumo vwTot ON vwNM.IdConsumo = vwTot.IdConsumo
WHERE vwNM.IdClasificacionNvaMet = 1 AND vwNM.Aplica=1
GROUP BY vwNM.IdConsumo, vwNM.He, vwNM.MontoCredito, vwTot.MontoTotal;
GO
