SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Analitico_CNR_Quincenal]
AS
SELECT DISTINCT
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorINDATR,
	INDATR,
	FactorProPago,
	ProPago,
	FactorProPR,
	ProPR,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion,
	ISNULL(CodigoCredito,'')+'|'+ISNULL(CodigoDeudor,'')+'|'+ISNULL(Nombre,'')+'|'+ISNULL(TipoCredito,'') AS EVERYTHING
FROM dbo.RW_Analitico_CNR_Quincenal;
GO
