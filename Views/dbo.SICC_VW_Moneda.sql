SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_Moneda]
AS
SELECT
 Moneda.IdMoneda,
 Moneda.Nombre,
 Moneda.Codigo,
 Moneda.MonedaOficial,
 Moneda.AplicaTC
FROM dbo.SICC_Moneda AS Moneda;
GO
