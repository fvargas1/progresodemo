SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0415MN_2016_VW]
AS
SELECT
 concepto.Codigo,
 concepto.Nombre AS Concepto,
 concepto.Padre,
 CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '4' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS InteresesPagados,
 CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '5' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS Comisiones,
 CASE WHEN rep.TipoDeCartera = '0' AND rep.TipoDeSaldo = '6' THEN CONVERT(MONEY,rep.Dato) ELSE 0 END AS SaldoPromedioDiario
FROM dbo.ReportWare_VW_0415Concepto_2016 concepto
LEFT OUTER JOIN dbo.RW_R04A0415MN_2016 rep ON rep.Concepto = concepto.Codigo;
GO
