SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0411MN_2016_VW]
AS
SELECT
	rep.IdReporteLog,
	concepto.Codigo,
	concepto.Nombre AS Concepto,
	concepto.Padre,
	CASE WHEN rep.TipoDeCartera = '1' AND rep.TipoDeSaldo = '1' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS TotalVigente_TotalVigente,
	CASE WHEN rep.TipoDeCartera = '2' AND rep.TipoDeSaldo = '1' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVigente_TotalVigente,
	CASE WHEN rep.TipoDeCartera = '2' AND rep.TipoDeSaldo = '2' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVigente_Principal,
	CASE WHEN rep.TipoDeCartera = '2' AND rep.TipoDeSaldo = '3' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVigente_InteresDevengado,
	CASE WHEN rep.TipoDeCartera = '3' AND rep.TipoDeSaldo = '1' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS SinPagos_TotalVigente,
	CASE WHEN rep.TipoDeCartera = '3' AND rep.TipoDeSaldo = '2' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS SinPagos_Principal,
	CASE WHEN rep.TipoDeCartera = '3' AND rep.TipoDeSaldo = '3' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS SinPagos_InteresDevengado,
	CASE WHEN rep.TipoDeCartera = '4' AND rep.TipoDeSaldo = '1' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS ConPagosVencidos_TotalVigente,
	CASE WHEN rep.TipoDeCartera = '4' AND rep.TipoDeSaldo = '2' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS ConPagosVencidos_Principal,
	CASE WHEN rep.TipoDeCartera = '4' AND rep.TipoDeSaldo = '3' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS ConPagosVencidos_InteresDevengado,
	CASE WHEN rep.TipoDeCartera = '5' AND rep.TipoDeSaldo = '1' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVencida_TotalVigente,
	CASE WHEN rep.TipoDeCartera = '5' AND rep.TipoDeSaldo = '2' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVencida_Principal,
	CASE WHEN rep.TipoDeCartera = '5' AND rep.TipoDeSaldo = '3' THEN CONVERT(MONEY,rep.Dato) ELSE 0.00 END AS CarteraVencida_InteresDevengado 
FROM dbo.ReportWare_VW_0411Concepto_2016 concepto
LEFT OUTER JOIN dbo.RW_R04A0411MN_2016 rep ON rep.Concepto = concepto.Codigo;
GO
