SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_Garantias_NM]
AS
SELECT
 c.IdPersona,
 c.IdCredito AS IdCredito,
 c.EI_Total AS MontoCredito,
 0 AS He,
 ISNULL(g.Hc, 0) AS Hc,
 CASE WHEN c.IdMoneda = g.IdMoneda THEN 0 ELSE 0.08 END AS Hfx,
 cg.MontoUsadoGarantia AS ValorGarantia,
 g.IdTipoGarantia,
 g.IdClasificacionNvaMet,
 g.Aplica
FROM dbo.SICCMX_VW_Garantia g
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON g.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICCMX_VW_Credito_NMC c ON c.IdCredito = cg.IdCredito;
GO
