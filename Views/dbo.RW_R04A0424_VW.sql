SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0424_VW]
AS
SELECT
	con.Codigo,
	con.Nombre AS Concepto,
	con.Padre,
	SUM(CASE WHEN rep.Moneda = '15' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS Total,
	SUM(CASE WHEN rep.Moneda = '14' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoMN,
	SUM(CASE WHEN rep.Moneda = '4' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoUSD,
	SUM(CASE WHEN rep.Moneda = '8' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END) AS SaldoUDIS
FROM dbo.ReportWare_VW_0424Concepto con
LEFT OUTER JOIN dbo.RW_R04A0424 rep ON rep.Concepto = con.Codigo
GROUP BY con.Codigo, con.Nombre, con.Padre;
GO
