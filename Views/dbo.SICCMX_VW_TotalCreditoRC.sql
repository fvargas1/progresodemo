SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  VIEW [dbo].[SICCMX_VW_TotalCreditoRC]            

AS            

SELECT  per.Codigo AS CodigoCliente,                    

  cre.Codigo as CodigoCredito,        

  cre.FechaVencimiento,      

  ISNULL(cre.MontoValorizado,0) AS Saldo,                            

  ISNULL(reser.ReservaFinal,0) AS Reserva,      

  periodo.Fecha AS Fecha,      

  CASE WHEN DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) <=0   

  THEN 8   

  ELSE (CASE WHEN cre.IdTasaReferencia <> '60' THEN cre.FrecuenciaRevisionTasa ELSE 
  DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) END) END AS Plazo,      

  NULLIF(mon.CodigoCNBV_Hipo,2) AS Moneda      

FROM SICC_Periodo periodo,       

 dbo.SICCMX_Persona per                            

  INNER JOIN SICCMX_VW_Credito cre ON per.IdPersona = cre.IdPersona                     

  INNER JOIN dbo.SICCMX_Credito_Reservas_Variables reser ON reser.IdCredito = cre.IdCredito         

  INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMoneda                        

WHERE periodo.Activo = 1 


UNION ALL



SELECT  per.Codigo AS CodigoCliente,                  

  cre.Codigo as CodigoCredito,      

  cre.FechaVencimiento,    

  ISNULL(cre.SaldoTotalValorizado,0)   AS Saldo,                          

  ISNULL(reser.ReservaExpuesta,0)+ ISNULL(reser.ReservaCubierta,0) AS Reserva,    

  periodo.Fecha AS Fecha,    

  CASE WHEN DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) <=0 

  THEN 1 

  ELSE DATEDIFF(DAY,periodo.Fecha,FechaVencimiento) END AS Plazo,    

  NULLIF(mon.CodigoCNBV_Hipo,2) AS Moneda      

FROM SICC_Periodo periodo,     

 dbo.SICCMX_Persona per                          

  INNER JOIN SICCMX_VW_Consumo cre ON per.IdPersona = cre.IdPersona                   

  INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables reser ON reser.IdConsumo = cre.IdConsumo      

  INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMoneda                      

WHERE periodo.Activo = 1
GO
