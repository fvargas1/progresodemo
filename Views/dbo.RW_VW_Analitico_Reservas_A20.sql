SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Analitico_Reservas_A20]
AS
SELECT DISTINCT
 Fecha,
 CodigoPersona,
 Nombre,
 CodigoCredito,
 MontoCredito,
 FechaVencimiento,
 [PI],
 Moneda,
 ActividadEconomica,
 MontoGarantia,
 MontoGarantiaAjustado,
 Prct_Reserva,
 SP,
 MontoReserva,
 Calificacion,
 PI_Aval,
 SP_Aval,
 ReservaAdicional,
 ISNULL(CodigoPersona,'')+'|'+ISNULL(Nombre,'')+'|'+ISNULL(CodigoCredito,'') AS EVERYTHING
FROM dbo.RW_Analitico_Reservas_A20;
GO
