SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_GarantiasReportes]
AS
SELECT DISTINCT
 cg.IdCredito,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 1 ELSE 0 END) AS NumeroGarRealFin,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 1 THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorGarRealFin,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN 1 ELSE 0 END) AS NumeroGarRealNoFin,
 SUM(CASE WHEN tg.Codigo = 'NMC-03' THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorDerechosCobro,
 SUM(CASE WHEN tg.Codigo = 'NMC-01' THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorBienesInmuebles,
 SUM(CASE WHEN tg.Codigo = 'NMC-02' THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorBienesMuebles,
 SUM(CASE WHEN tg.Codigo = 'EYM-01' THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorFidPartFed,
 SUM(CASE WHEN tg.Codigo = 'EYM-02' THEN cg.MontoCubiertoCredito ELSE 0 END) AS ValorFidIngProp,
 0 AS ValorOtrasGar
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
WHERE g.Aplica = 1 AND cg.MontoUsadoGarantia > 0 AND cg.Aplica = 1
GROUP BY cg.IdCredito;
GO
