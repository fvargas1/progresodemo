SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_RTH_Reservas]
AS
SELECT DISTINCT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	Veces,
	FactorPorPago,
	PorPago,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion,
	ISNULL(Codigo,'') AS EVERYTHING
FROM dbo.RW_RTH_Reservas;
GO
