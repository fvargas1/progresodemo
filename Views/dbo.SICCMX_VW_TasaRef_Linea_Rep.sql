SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICCMX_VW_TasaRef_Linea_Rep]
AS
SELECT
	lin.IdLineaCredito,
	lin.Codigo,
	lin.IdTasaReferencia,
	lin.EsMultimoneda,
	tr.Codigo AS CodigoTasaRef,
	tr.Codename AS CodenameTasaRef,
	CASE WHEN lin.EsMultimoneda = 1 THEN trMul.Codigo ELSE tr.Codigo END AS CodigoCNBVTasaRef
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICC_TasaReferencia tr ON lin.IdTasaReferencia = tr.IdTasaReferencia
INNER JOIN dbo.SICC_TasaReferencia trMul ON trMul.Codigo = '999';
GO
