SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SICC_VW_R04A0420HipotecarioMovimientos_AnteriorTavig_Pagos]
AS
SELECT
	cr.Codigo,
	conf.Concepto,
	mov.Monto
FROM dbo.SICCMX_Hipotecario cr
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Movimientos mov ON mov.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tm ON tm.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = cr.IdTipoCreditoR04A
INNER JOIN R04.[0420Configuracion] conf ON tp.Codigo LIKE conf.CodigoProducto AND conf.TipoMovimiento = tm.Codigo
INNER JOIN dbo.SICC_VW_R04AHipotecarioMovimiento_FechaPrimerTavig tavig ON tavig.IdHipotecario = cr.IdHipotecario
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sit ON info.IdSituacionCredito = sit.IdSituacionHipotecario
WHERE sit.Codigo = '1' AND mov.Fecha <= tavig.Fecha AND tm.Codigo NOT IN ('TAVEN','TAVIG');
GO
