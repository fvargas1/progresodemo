SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_VW_Auditoria_Avales_BANCREA]
AS
SELECT DISTINCT
	[ID],
	[ID_Linea],
	[LC],
	[S_Total],
	[IDevNC_B],
	[Estatus_B6],
	[IDC_C],
	[N_A_C],
	[ING_C],
	[PI_C],
	[IDC_G],
	[N_A_G],
	[OS_AG],
	[ING_G],
	[GP_PC],
	[PI_G],
	[TA_GTE],
	[B1],
	ISNULL(ID,'')+'|'+ISNULL(ID_Linea,'')+'|'+ISNULL(N_A_C,'')+'|'+ISNULL(N_A_G,'') AS EVERYTHING
FROM dbo.RW_Auditoria_Avales_BANCREA;
GO
