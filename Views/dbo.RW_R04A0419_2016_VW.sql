SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[RW_R04A0419_2016_VW]
AS
SELECT
 rep.IdReporteLog,
 concepto.Codigo,
 concepto.Nombre AS Concepto,
 concepto.Padre,
 CASE WHEN rep.TipoDeSaldo = '9' AND rep.Moneda='15' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasConsolidado_Cargos,
 CASE WHEN rep.TipoDeSaldo = '10' AND rep.Moneda='15' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasConsolidado_Abonos,
 CASE WHEN rep.TipoDeSaldo = '11' AND rep.Moneda='15' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasConsolidado_Saldo,
 CASE WHEN rep.TipoDeSaldo = '9' AND rep.Moneda='14' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasMNUDIS_Cargos,
 CASE WHEN rep.TipoDeSaldo = '10' AND rep.Moneda='14' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasMNUDIS_Abonos,
 CASE WHEN rep.TipoDeSaldo = '11' AND rep.Moneda='14' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasMNUDIS_Saldo,
 CASE WHEN rep.TipoDeSaldo = '9' AND rep.Moneda='4' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasME_Cargos,
 CASE WHEN rep.TipoDeSaldo = '10' AND rep.Moneda='4' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasME_Abonos,
 CASE WHEN rep.TipoDeSaldo = '11' AND rep.Moneda='4' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasME_Saldos,
 CASE WHEN rep.TipoDeSaldo = '9' AND rep.Moneda='9' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasUDIGub_Cargos,
 CASE WHEN rep.TipoDeSaldo = '10' AND rep.Moneda='9' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasUDIGub_Abonos,
 CASE WHEN rep.TipoDeSaldo = '11' AND rep.Moneda='9' THEN CAST(rep.Dato AS DECIMAL) ELSE 0.00 END AS ReservasUDIGub_Saldos
FROM dbo.ReportWare_VW_0419Concepto_2016 concepto
LEFT OUTER JOIN dbo.RW_R04A0419_2016 rep ON rep.Concepto = concepto.Codigo;
GO
