SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_038]
AS
BEGIN
-- Para créditos dispuestos en el mes, el Saldo del Principal al Inicio del Período debe de ser igual a 0.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	FechaDisposicion,
	SaldoInicial,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0484_INC
WHERE ISNULL(FechaDisposicion,'') = @FechaPeriodo AND CAST(ISNULL(NULLIF(SaldoInicial,''),'-1') AS DECIMAL) <> 0;

END
GO
