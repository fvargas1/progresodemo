SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_076]
AS

BEGIN

-- Si la Responsabilidad Total (dat_responsabilidad_total) es MAYOR o IGUAL a 0,
-- entonces las Reservas Totales (dat_reservas_totales) deben ser MENORES O IGUALES a la Responsabilidad Total del Crédito(dat_responsabilidad_total).

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaTotal,
	ResponsabilidadFinal
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL) >=0 AND CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) > CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL);

END
GO
