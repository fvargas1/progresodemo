SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_TasaValor_Insert]
	@IdTasa INT,
	@IdPeriodo INT,
	@Valor DECIMAL(18,6)
AS
INSERT INTO dbo.SICC_TasaValor (IdTasa, IdPeriodo, Valor)
VALUES (@IdTasa, @IdPeriodo, @Valor);

SELECT SCOPE_IDENTITY();
GO
