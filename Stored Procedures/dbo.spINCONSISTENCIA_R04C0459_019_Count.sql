SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_019_Count]

	@IdInconsistencia BIGINT

AS

BEGIN

-- El Monto Total Pagado Efectivamente por el Acreditado no cuadra con la suma de los montos pagados.



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0459_INC

WHERE CAST(ISNULL(NULLIF(MontoTotalPagado,''),'0') AS DECIMAL(18,2)) <> (

	CAST(ISNULL(NULLIF(MontoCapitalPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoInteresPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoComisionPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoInteresMoratorio,''),'0') AS DECIMAL(18,2)));



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
