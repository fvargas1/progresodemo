SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0457_009]
AS
BEGIN
-- Validar que el Monto Reconocido por Quitas, Castigos y Quebrantos en el Periodo (dat_monto_reconocido_quebranto) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	MontoQuitas
FROM dbo.RW_VW_R04C0457_INC
WHERE CAST(ISNULL(NULLIF(MontoQuitas,''),'-1') AS DECIMAL) < 0;

END
GO
