SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ValidaInfoAnx20_GP]
AS

UPDATE piDet
SET PuntosActual = 24
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo20_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='20IA_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Se actualiza puntaje de ''' + variab.Nombre + ''' a ''24'', ya que la "Utilidad Neta Trimestral" y el "Capital Contable Promedio" son menores a 0'
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo20_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante
INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND anx.EsGarante = piDet.EsGarante
INNER JOIN dbo.SICCMX_PI_Variables variab ON piDet.IdVariable = variab.Id AND variab.Codigo='20IA_REND_CAP_ROE'
WHERE ISNULL(anx.UtilNetaTrimestre,0) < 0 AND ISNULL(anx.CapitalContableProm,0) < 0;
GO
