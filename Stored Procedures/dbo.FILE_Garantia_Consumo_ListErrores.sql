SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Garantia_Consumo_ListErrores]
AS
SELECT
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 CodigoCliente,
 IndPerMorales,
 Fuente
FROM dbo.FILE_Garantia_Consumo
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
