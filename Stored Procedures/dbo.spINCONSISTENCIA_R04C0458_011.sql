SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_011]
AS
BEGIN
-- La Fecha de Vencimiento de la Línea de Crédito (dat_fecha_vencimiento) debe ser MAYOR O IGUAL al periodo que se reporta (cve_periodo).

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	FecVenLin,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(FecVenLin,'') < @FechaPeriodo AND ISNULL(TipoAltaCredito,'') IN ('132','133','134','139','700','701','702','731','732','733','741','742','743');

END
GO
