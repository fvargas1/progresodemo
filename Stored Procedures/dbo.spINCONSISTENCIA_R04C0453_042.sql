SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_042]
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Actividad Económica (cve_actividad_economica).

SELECT
	CodigoCredito,
	ActEconomica,
	RFC
FROM dbo.RW_VW_R04C0453_INC
WHERE ActEconomica IN (
	SELECT rep.ActEconomica
	FROM dbo.RW_VW_R04C0453_INC rep
	INNER JOIN dbo.RW_VW_R04C0453_INC rep2 ON rep.RFC = rep2.RFC AND rep.ActEconomica <> rep2.ActEconomica
	GROUP BY rep.ActEconomica, rep.RFC
);

END
GO
