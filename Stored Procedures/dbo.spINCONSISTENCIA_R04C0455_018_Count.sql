SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_018_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de la Deuda a Corto Plazo (dat_deuda_cp) es >= 10 y < 60 entonces el Puntaje Deuda Corto Plazo a Deuda Total (cve_puntaje_deuda_cp) debe ser = 89

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoDeudaCortoPlazo,''),'0') AS DECIMAL(18,6)) BETWEEN 10 AND 59.999999 AND ISNULL(P_DeudaCortoPlazoTotal,'') <> '89';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
