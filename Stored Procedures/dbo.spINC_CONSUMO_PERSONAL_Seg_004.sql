SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_PERSONAL_Seg_004]
AS

BEGIN

-- El campo "Tipo de Garantía" se debe reportar:
-- 10 en caso de que el crédito no tenga garantía;
-- 20 en caso de que el crédito tenga garantía con dinero en efectivo;
-- 30 en caso de que el crédito tenga garantía de medios de pago con liquidez inmediata;
-- 40 en caso de que el crédito tenga garantía mobiliaria constituida con apego a lo establecido en el Registro Único de Garantías Mobiliarias (RUGM),
-- y 90 en caso de que el crédito tenga una garantía distinta a las señaladas.

SELECT DISTINCT
	rep.FolioCredito,
	rep.TipoGarantia
FROM dbo.RW_Consumo_PERSONAL rep
LEFT OUTER JOIN dbo.SICC_TipoGarantiaConsumo cat ON rep.TipoGarantia = cat.CodigoBanxico
WHERE cat.IdTipoGarantiaConsumo IS NULL;

END
GO
