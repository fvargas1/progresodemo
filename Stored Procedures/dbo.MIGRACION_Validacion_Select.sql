SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_Select]
	@IdValidacion INT
AS
SELECT
	vw.IdValidacion,
	vw.Description,
	vw.Codename,
	vw.Position,
	vw.CategoryName,
	vw.FieldName,
	vw.Activo,
	vw.AplicaUpd,
	vw.ReqCalificacion,
	vw.ReqReportes
FROM dbo.MIGRACION_VW_Validacion vw
WHERE vw.IdValidacion = @IdValidacion;
GO
