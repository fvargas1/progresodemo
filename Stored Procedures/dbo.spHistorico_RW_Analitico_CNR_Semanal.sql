SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_CNR_Semanal]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_CNR_Semanal);

DELETE FROM Historico.RW_Analitico_CNR_Semanal WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_CNR_Semanal (
	IdPeriodoHistorico, CodigoCredito, CodigoDeudor, Nombre, Periodicidad_Facturacion, TipoCredito, Metodologia, Constante, FactorATR, ATR,
	FactorMAXATR, MAXATR, FactorProPago, ProPago, FactorSDOIMP, SDOIMP, FactorTipoCredito, [PI], SP_Cubierta, SP_Expuesta, ECubierta, EExpuesta,
	ETotal, MontoGarantiaTotal, MontoGarantiaUsado, ReservaCubierta, ReservaExpuesta, ReservaTotal, PorcentajeReservaTotal, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorMAXATR,
	MAXATR,
	FactorProPago,
	ProPago,
	FactorSDOIMP,
	SDOIMP,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion
FROM dbo.RW_Analitico_CNR_Semanal
WHERE IdReporteLog = @IdReporteLog;
GO
