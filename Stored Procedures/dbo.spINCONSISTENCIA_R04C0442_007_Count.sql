SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_007_Count]


	@IdInconsistencia BIGINT


AS


BEGIN





-- El Nombre del Acreditado sólo se aceptan letras en mayúsculas y números, sin caracteres distintos a estos, sin puntos ni comas.





DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;





DECLARE @count INT;





SELECT @count = COUNT(CodigoCredito)


FROM dbo.RW_R04C0442


WHERE NombrePersona LIKE '%[^A-Z0-9 ]%' AND CodigoCreditoCNBV NOT LIKE '%&%' OR BINARY_CHECKSUM(NombrePersona) <> BINARY_CHECKSUM(UPPER(NombrePersona));





INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());





SELECT @count;





END
GO
