SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_Reservas_A21_Get]
 @IdReporteLog BIGINT
AS
SELECT
 Fecha,
 CodigoPersona,
 Nombre,
 CodigoCredito,
 MontoCredito,
 FechaVencimiento,
 [PI],
 Moneda,
 ActividadEconomica,
 MontoGarantia,
 MontoGarantiaAjustado,
 Prct_Reserva,
 SP,
 MontoReserva,
 Calificacion,
 PI_Aval,
 SP_Aval,
 ReservaAdicional
FROM dbo.RW_VW_Analitico_Reservas_A21
ORDER BY Nombre;
GO
