SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoAval_CodigoCredito_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_ConsumoAval_Credito_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_ConsumoAval f
LEFT OUTER JOIN dbo.SICCMX_Consumo c ON LTRIM(f.CodigoCredito) = c.Codigo
WHERE c.Codigo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_ConsumoAval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCredito,
	'CodigoCredito',
	f.CodigoCredito,
	2,
	@Detalle
FROM dbo.FILE_ConsumoAval f
LEFT OUTER JOIN dbo.SICCMX_Consumo c ON LTRIM(f.CodigoCredito) = c.Codigo
WHERE c.Codigo IS NULL;
GO
