SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_059]
AS

BEGIN

-- El "AJUSTE EN LA TASA REFERENCIA" deberá iniciar con alguno de los siguientes caracteres: "*", "+", "–".

SELECT CodigoCredito, CodigoCreditoCNBV, AjusteTasaRef
FROM dbo.RW_R04H0491
WHERE AjusteTasaRef <> '0' AND SUBSTRING(AjusteTasaRef,1,1) NOT IN ('*','+','–');

END
GO
