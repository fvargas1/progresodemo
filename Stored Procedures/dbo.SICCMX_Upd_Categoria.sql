SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_Categoria]
AS
-- ACTUALIZA LA CATEGORIA DEL CREDITO PARA CREDITOS SIN CATEGORIA
-- ASIGNA LA DEL ULTIMO HISTORICO CON INFORMACION
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

UPDATE inf
SET IdCategoria = hst.IdCategoria
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_Credito cre ON inf.IdCredito = cre.IdCredito
CROSS APPLY (
	SELECT TOP 1 hInf.Credito, cat.IdCategoria
	FROM Historico.SICCMX_CreditoInfo hInf
	INNER JOIN dbo.SICC_PeriodoHistorico ph ON hInf.IdPeriodoHistorico = ph.IdPeriodoHistorico AND ph.Fecha < @FechaPeriodo AND ph.Activo = 1
	LEFT OUTER JOIN dbo.SICC_CategoriaCredito cat ON hInf.Categoria = cat.Codigo
	WHERE hInf.Credito = cre.Codigo AND hInf.Categoria IS NOT NULL
	ORDER BY ph.Fecha DESC
) AS hst
WHERE inf.IdCategoria IS NULL;
GO
