SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_003_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- El factor ajuste hfx debe encontrarse en formato de porcentaje y no en decimal

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0461_INC
WHERE ISNULL(Hfx,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
