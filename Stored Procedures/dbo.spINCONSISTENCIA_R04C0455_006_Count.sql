SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_006_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de las Participaciones Elegibles (dat_saldo_particip_elegibles) es = 0
-- entonces el Puntaje Deuda Total a Participaciones Elegibles (cve_puntaje_deuda_particip) debe ser = 159 ó = 62

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoPartEleg,''),'0') AS DECIMAL(18,6)) = 0 AND ISNULL(P_DeudaTotalPartEleg,'') NOT IN ('159','62');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
