SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_046]
AS

BEGIN

-- La "FECHA DE VENCIMIENTO DEL CREDITO REESTRUCTURADO" deberá ser mayor a la fecha de firma de la reestructuración del crédito (columna 25).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaVencimientoReestructura, FechaFirmaReestructura
FROM dbo.RW_R04H0491
WHERE FechaVencimientoReestructura <= FechaFirmaReestructura;

END
GO
