SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_TipoIngreso_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_TipoIngreso_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoIngreso ti ON LTRIM(f.ComprobanteIngresos) = ti.Codigo
WHERE ti.Codigo IS NULL AND LEN(f.ComprobanteIngresos) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCliente,
	'ComprobanteIngresos',
	f.ComprobanteIngresos,
	2,
	@Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_TipoIngreso ti ON LTRIM(f.ComprobanteIngresos) = ti.Codigo
WHERE ti.Codigo IS NULL AND LEN(f.ComprobanteIngresos) > 0;
GO
