SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Periodo_Ced_Individual]
	@IdPersona BIGINT,
	@IdMetodologia INT
AS
DECLARE @Institucion VARCHAR(150);
SELECT @Institucion = ins.Nombre
FROM dbo.SICC_Institucion ins
INNER JOIN dbo.BAJAWARE_Config cfg ON ins.Codigo = cfg.Value AND cfg.CodeName='CodigoInstitucion';

SELECT DISTINCT
	per.IdPersona,
	per.Codigo AS Codigo,
	per.Nombre AS Nombre,
	met.Nombre + ' - ' + met.Descripcion AS Metodologia,
	ced.Reportname,
	ced.ReportFolder,
	ced.Prefix,
	cedH.Reportname AS ReportnameHist,
	cedH.ReportFolder AS ReportFolderHist,
	cedH.Prefix AS PrefixHist,
	@Institucion AS Institucion
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.RW_Cedula ced ON ced.Codename = 'RW_Cedula_Anexo' + met.Codigo
LEFT OUTER JOIN dbo.RW_CedulaHistorico cedH ON cedH.Codename = 'RW_CedulaDatos_Anexo' + met.Codigo + '_Historico'
WHERE per.IdPersona = @IdPersona AND cre.IdMetodologia = @IdMetodologia;


SELECT prd.IdPeriodo, CONVERT(CHAR(10), prd.Fecha,126) + ' - Periodo Actual' AS Fecha
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.RW_CedulaNMC_Info inf ON per.IdPersona = inf.IdPersona
INNER JOIN dbo.SICC_Periodo prd ON prd.Activo = 1
WHERE per.IdPersona=@IdPersona
UNION
SELECT prd.IdPeriodoHistorico, CONVERT(CHAR(10), prd.Fecha,126) + ' - ' + prd.Version
FROM dbo.SICCMX_Persona per
INNER JOIN Historico.RW_CedulaNMC_Info inf ON per.Codigo = inf.Codigo
INNER JOIN dbo.SICC_PeriodoHistorico prd ON inf.IdPeriodoHistorico = prd.IdPeriodoHistorico
WHERE per.IdPersona=@IdPersona
ORDER BY Fecha DESC, IdPeriodo DESC;
GO
