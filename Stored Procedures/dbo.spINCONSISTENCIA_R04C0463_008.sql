SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_008]
AS
BEGIN
-- El Monto de la línea de Crédito Autorizado (dat_monto_credito_linea_aut) debe ser  MAYOR a 0.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	MonLineaCred
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(MonLineaCred AS DECIMAL) <= 0 AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','137','138','139','700','701','702','731','732','733','741','742','743','744','751');

END

GO
