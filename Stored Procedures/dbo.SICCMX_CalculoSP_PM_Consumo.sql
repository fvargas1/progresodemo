SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_PM_Consumo]
AS
-- CALCULAMOS SP PARA GARANTIAS DE PASO Y MEDIDA
UPDATE can
SET SeveridadCorresp = .45
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_VW_Consumo con ON can.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON con.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia AND can.IdTipoGarantia = gar.IdTipoGarantia
WHERE gar.TipoGarantia = 'NMC-05';
GO
