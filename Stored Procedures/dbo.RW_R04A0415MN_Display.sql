SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0415MN_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (Codigo, Concepto, NoAplicaInteresesPagados, NoAplicaComisiones, NoAplicaSaldoPromedioDiario, LEVEL)
AS (
	SELECT
	vw.Codigo,
	vw.Concepto,
	vw.NoAplicaInteresesPagados,
	vw.NoAplicaComisiones,
	vw.NoAplicaSaldoPromedioDiario,
	0 AS LEVEL
	FROM dbo.RW_R04A0415MN_VW_Consolidado vw
	WHERE vw.Padre = ''
	UNION ALL
	SELECT
	vw.Codigo,
	vw.Concepto,
	vw.NoAplicaInteresesPagados,
	vw.NoAplicaComisiones,
	vw.NoAplicaSaldoPromedioDiario,
	LEVEL + 1
	FROM dbo.RW_R04A0415MN_VW_Consolidado vw
	INNER JOIN Reporte r ON r.Codigo = vw.Padre
)
SELECT
	Codigo,
	REPLICATE(' ', LEVEL*5)+ Concepto AS Concepto,
	NoAplicaInteresesPagados,
	NoAplicaComisiones,
	NoAplicaSaldoPromedioDiario,
	LEVEL
FROM Reporte
ORDER BY Codigo;
GO
