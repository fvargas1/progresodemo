SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_Homologacion]

AS

DECLARE @Campo VARCHAR(50);

DECLARE @Tipo INT;

DECLARE @Fuente VARCHAR(50);



-- AGREGAMOS "_" A RFC CUANDO ESTE TIENE SOLO 12 POSICIONES (PERSONAS MORALES)

UPDATE dbo.FILE_Aval SET RFC = '_' + LTRIM(RTRIM(RFC)) WHERE LEN(LTRIM(RTRIM(RFC))) = 12;



-- AGREGAMOS "0" AL CODIGO POSTAL CUANDO ESTE TIENE SOLO 4 POSICIONES

UPDATE dbo.FILE_Aval SET CodigoPostal = '0' + LTRIM(RTRIM(CodigoPostal)) WHERE LEN(LTRIM(RTRIM(CodigoPostal))) = 4;



-- ACTUALIZAMOS LA LOCALIDAD CUANDO RECIBIMOS EL CP

UPDATE f

SET Localidad = ISNULL(NULLIF(f.Localidad,''), mun.CodigoCNBV)

FROM dbo.FILE_Aval f

INNER JOIN dbo.SICC_Municipio mun ON LTRIM(f.MunAval) = mun.CodigoMunicipio;


-- ACTUALIZAMOS EL ESTADO Y EL MUNICIPIO CUANDO RECIBIMOS SOLO LA LOCALIDAD

UPDATE f

SET Estado = ISNULL(NULLIF(f.Estado,''), loc.CodigoEstado),

 MunAval = ISNULL(NULLIF(f.MunAval,''), loc.CodigoMunicipio)

FROM dbo.FILE_Aval f

INNER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.Localidad) = loc.CodigoCNBV;



DECLARE crs CURSOR FOR

SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente

FROM dbo.FILE_Aval_Homologacion

WHERE Activo=1

ORDER BY Tipo;



OPEN crs;



FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;



WHILE @@FETCH_STATUS = 0

BEGIN

EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Aval', 'FILE_Aval_Homologacion', @Campo, @Tipo, @Fuente;



FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

END



CLOSE crs;

DEALLOCATE crs;



UPDATE dbo.FILE_Aval SET Homologado = 1;
GO
