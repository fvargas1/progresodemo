SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0493_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	MontoTotalLiquidacion,
	MontoPagoLiquidacion,
	MontoBonificaciones,
	ValorBienAdjudicado
FROM dbo.RW_VW_R04H0493
ORDER BY NumeroSecuencia;
GO
