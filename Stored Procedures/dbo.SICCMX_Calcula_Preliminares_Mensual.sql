SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Preliminares_Mensual]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo = '3';

-- INICIALIZAMOS LAS VARIABLES CON LOS VALORES RECIBIDOS EN LA MIGRACION
UPDATE pre
SET ATR = info.ATR,
	VECES = info.VECES,
	PorPago = info.PorPago
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;

-- CALCULAMOS VARIABLES SIN INFORMACION
UPDATE pre
SET ATR = ISNULL(pre.ATR, CEILING(info.DiasAtraso / 30.4)),
	VECES = ISNULL(pre.VECES, CASE WHEN info.ValorOriginalBien > 0 THEN info.ExigibleTeorico / info.ValorOriginalBien ELSE 0 END),
	PorPago = ISNULL(pre.PorPago, vw.PorPagoM)
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_VW_Historico_Consumo vw ON pre.IdConsumo = vw.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;
GO
