SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_indicadorcat_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_indicadorcat_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Consumo
SET errorCatalogo = 1
WHERE LEN(indicadorcat)>0 AND LTRIM(indicadorcat) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCredito,
 'indicadorcat',
 indicadorcat,
 2,
 @Detalle
FROM dbo.FILE_Consumo
WHERE LEN(indicadorcat)>0 AND LTRIM(indicadorcat) NOT IN ('0','1');
GO
