SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0470_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0470_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0470_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0470_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, Clasificacion, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
 Alfa, HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, LugarRadica, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_NumInstRep,
 P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_RotActTot, P_RotCapTrabajo, P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep,
 PorcPagoInstNoBanc, PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual, VentasNetasTotales,
 IngresosBrutosAnuales, AnioIngresosBrutosAnuales, RotActTot, ActivoCirculante, RotCapTrabajo, RendCapROE, P_EstEco, P_IntCarComp, P_Proveedores, P_Clientes, P_EstFinAudit,
 P_NumAgenCalif, P_IndepConsejoAdmon, P_EstOrg, P_CompAcc, P_LiqOper, P_UafirGastosFin
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '470',
 per.Codigo AS CodigoPersona,
 CASE ppi.IdClasificacion WHEN 1 THEN 111 WHEN 2 THEN 112 WHEN 3 THEN 113 ELSE NULL END AS Clasificacion,
 CASE WHEN anx.SinAtrasos = 1 THEN '1' ELSE '2' END AS SinAtrasos,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 crs.CredRepSICCNBV AS CreditoReportadoSIC,
 dlt.FactorCuantitativo AS Alfa,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 ppi.MesesPI100 AS MesesPI100,
 pi100.Codigo AS ID_PI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 anx.LugarRadica AS LugarRadica,
 ptNMC.[22_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[22_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[22_NUM_INST_REP] AS P_NumInstRep,
 ptNMC.[22_PORC_PAGO_INST_NOBANC] AS P_PorcPagoInstNoBanc,
 ptNMC.[22_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ptNMC.[22_DIAS_ATR_INFONAVIT] AS P_DiasAtrInfonavit,
 ptNMC.[22_TASA_RET_LAB] AS P_TasaRetLab,
 ptNMC.[22_ROT_ACT_TOT] AS P_RotActTot,
 ptNMC.[22_ROT_CAP_TRABAJO] AS P_RotCapTrabajo,
 ptNMC.[22_REND_CAP_ROE] AS P_RendCapROE,
 vlNMC.[22_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[22_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[22_NUM_INST_REP] AS NumInstRep,
 vlNMC.[22_PORC_PAGO_INST_NOBANC] AS PorcPagoInstNoBanc,
 vlNMC.[22_PAGOS_INFONAVIT] AS PagosInfonavit,
 vlNMC.[22_DIAS_ATR_INFONAVIT] AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 vlNMC.[22_TASA_RET_LAB] AS TasaRetLab,
 anx.PasivoCirculante AS PasivoCirculante,
 anx.UtilidadNeta AS UtilidadNeta,
 anx.CapitalContableProm AS CapitalContable,
 anx.ActTotalAnual AS ActivoTotalAnual,
 anx.VentNetTotAnuales AS VentasNetasTotales,
 pInfo.IngresosBrutos AS IngresosBrutosAnuales,
 YEAR(anx.FechaInfoFinanc) AS AnioIngresosBrutosAnuales,
 vlNMC.[22_ROT_ACT_TOT] AS RotActTot,
 anx.ActivoCirculante AS ActivoCirculante,
 vlNMC.[22_ROT_CAP_TRABAJO] AS RotCapTrabajo,
 vlNMC.[22_REND_CAP_ROE] AS RendCapROE,
 ptNMC.[22_EST_ECO] AS P_EstEco,
 ptNMC.[22_INT_CAR_COMP] AS P_IntCarComp,
 ptNMC.[22_PROVEEDORES] AS P_Proveedores,
 ptNMC.[22_CLIENTES] AS P_Clientes,
 ptNMC.[22_EST_FIN_AUDIT] AS P_EstFinAudit,
 ptNMC.[22_NUM_AGEN_CALIF] AS P_NumAgenCalif,
 ptNMC.[22_INDEP_CONSEJO_ADMON] AS P_IndepConsejoAdmon,
 ptNMC.[22_EST_ORG] AS P_EstOrg,
 ptNMC.[22_COMP_ACC] AS P_CompAcc,
 ptNMC.[22_LIQ_OPER] AS P_LiqOper,
 ptNMC.[22_UAFIR_GASTOS_FIN] AS P_UafirGastosFin
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo22 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_ValoresDelta dlt ON ppi.IdMetodologia = dlt.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(dlt.IdClasificacion,0)
INNER JOIN dbo.SICCMX_VW_PersonasValor_A22 vlNMC ON ppi.IdPersona = vlNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A22 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Persona_CredRepSIC crs ON per.IdPersona = crs.IdPersona
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON per.PI100 = pi100.IdPI100
WHERE ISNULL(anx.OrgDescPartidoPolitico,0) = 0;

EXEC dbo.RW_R04C0470_2016_Fact_Generate @IdReporteLog, @IdPeriodo, @Entidad; -- FACTORADO
EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0470_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
