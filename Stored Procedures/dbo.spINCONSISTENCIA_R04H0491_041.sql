SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_041]
AS

BEGIN

-- La "LOCALIDAD EN LA QUE SE ENCUENTRA LA VIVIENDA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.Localidad
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Municipio cat ON ISNULL(r.Localidad,'') = cat.CodigoCNBV
WHERE cat.IdMunicipio IS NULL;

END
GO
