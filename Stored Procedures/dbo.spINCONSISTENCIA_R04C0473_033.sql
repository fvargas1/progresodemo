SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_033]
AS

BEGIN

-- Las comisiones por disposición deben ser mayores o iguales a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionDisposicionMonto
FROM dbo.RW_VW_R04C0473_INC
WHERE CAST(ISNULL(NULLIF(ComisionDisposicionMonto,''),'-1') AS DECIMAL) < 0;

END


GO
