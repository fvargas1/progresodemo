SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_111]
AS

BEGIN

-- Validar que la Probabilidad de Incumplimiento Total (dat_probabilidad_incump) del reporte de Seguimiento,
-- sea igual a la Probabilidad Incumplimiento (dat_proba_incump) del reporte de PI. Se excluyen acreditados que tengan créditos calificados con Anexo 19.

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.CodigoPersona,
	rep.ProyectoInversion,
	rep.PITotal AS PI_Seguimiento,
	rpi.[PI] AS PI_ReportePI
FROM dbo.RW_R04C0474 rep
INNER JOIN dbo.RW_R04C0475 rpi ON rep.CodigoPersona = rpi.CodigoPersona
WHERE ISNULL(rep.ProyectoInversion,'') = '2' AND ISNULL(rep.PITotal,'') <> ISNULL(rpi.[PI],'');

END
GO
