SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_105]
AS

BEGIN

-- Se identifican acreditados (dat_id_acreditado_institucion) en el formulario de Probabilidad que cuentan
-- con mas de un RFC del Acreditado (dat_rfc). (No aplica cuando se trata de una sustitución de deudor).

SELECT
	REPLACE(CodigoPersona, ',', '') AS CodigoPersona,
	RFC
FROM dbo.RW_R04C0475
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_R04C0475 rep
	INNER JOIN dbo.RW_R04C0475 rep2 ON rep.CodigoPersona = rep2.CodigoPersona AND rep.RFC <> rep2.RFC
	GROUP BY rep.RFC, rep.CodigoPersona
);

END
GO
