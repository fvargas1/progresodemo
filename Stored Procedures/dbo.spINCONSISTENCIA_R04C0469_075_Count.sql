SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_075_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que si la columna de Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo
-- (cve_instituciones) es DIFERENTE de cero, entonces el Monto Fondeado por Banco de Desarrollo o Fondo
-- de Fomento (dat_monto_fondea_b_desarrollo) debe ser DIFERENTE de cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0469_INC
WHERE InstitucionFondeo <> '0' AND CAST(MontoBancaDesarrollo AS DECIMAL) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
