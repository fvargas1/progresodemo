SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Anexo31Consumo2011]
	@IdPeriodoHistorico INT
AS
DELETE Historico.RW_Anexo31Consumo2011 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Anexo31Consumo2011 (
	IdPeriodoHistorico,
	Calificacion,
	Importe,
	Reserva,
	PorcentajeReservas,
	Metodologia,
	ImporteSemanal,
	ImporteQuincenal,
	ReservaSemanal,
	ReservaQuincenal,
	TipoCredito
)
SELECT
	@IdPeriodoHistorico,
	Calificacion,
	Importe,
	Reserva,
	PorcentajeReservas,
	Metodologia,
	ImporteSemanal,
	ImporteQuincenal,
	ReservaSemanal,
	ReservaQuincenal,
	TipoCredito
FROM dbo.RW_Anexo31Consumo2011;
GO
