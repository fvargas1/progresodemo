SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0483_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0483_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0483_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0483_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActEconomica, GrupoRiesgo, Localidad, Municipio, Estado,
 Nacionalidad, IdBuroCredito, LEI, TipoAltaCredito, TipoOperacion, DestinoCredito, CodigoCredito, CodigoCreditoCNBV, CodigoGlobalCNBV, MonLineaCredValorizado,
 MonLineaCred, FecMaxDis, FecVenLin, Moneda, FormaDisposicion, RegGarantiaMob, IdDeudorRelacionado, IdInstitucionOrigen, TasaInteres, DifTasaRef,
 OperacionDifTasaRef, FrecuenciaRevisionTasa, PeriodicidadPagosCapital, PeriodicidadPagosInteres, NumMesesAmortCap, NumMesesPagoInt, ComisionAperturaTasa,
 ComisionAperturaMonto, ComisionDisposicionTasa, ComisionDisposicionMonto, LocalidadDestinoCredito, MunicipioDestinoCredito, EstadoDestinoCredito,
 ActividadDestinoCredito
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '483',
 per.Codigo AS CodigoPersona,
 per.RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 tpoPer.CodigoCNBV AS TipoCartera,
 actEco.CodigoCNBV AS ActEconomica,
 CASE WHEN LEN(ISNULL(pInfo.GrupoRiesgo,'')) = 0 OR pInfo.GrupoRiesgo LIKE '%SIN GRUPO%' THEN REPLACE(pInfo.NombreCNBV, ',', '') ELSE pInfo.GrupoRiesgo END AS GrupoRiesgo,
 mun.CodigoCNBV AS Localidad,
 pInfo.Municipio AS Municipio,
 pInfo.Estado AS Estado,
 pais.CodigoCNBV AS Nacionalidad,
 lin.FolioConsultaBuro AS IdBuroCredito,
 pInfo.LEI AS LEI,
 tpoAlta.CodigoCNBV AS TipoAltaCredito,
 tpoOper.CodigoCNBV AS TipoOperacion,
 dest.CNBV_2016_EYM AS DestinoCredito,
 lin.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 grp.GrupalCNBV AS GrupalCNBV,
 lin.MontoLineaValorizado AS MonLineaCredValorizado,
 lin.MontoLinea_Alta AS MonLineaCred,
 CASE WHEN lin.FecMaxDis IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecMaxDis,102),'.',''),1,6) END AS FecMaxDis,
 CASE WHEN lin.FecVenLinea IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecVenLinea,102),'.',''),1,6) END AS FecVenLin,
 mon.CodigoCNBVMoneda AS Moneda,
 disp.CodigoCNBV AS FormaDisposicion,
 rgm.RegGarantiaMob AS RegGarantiaMob,
 deudRel.CodigoCNBV AS IdDeudorRelacionado,
 inst.Codigo AS InstitucionOrigen,
 tasaRef.CodigoCNBVTasaRef AS TasaInteres,
 tasa.AjusteTasa AS DifTasaRef,
 tasa.OperacionDiferencial AS OperacionDifTasaRef,
 tasa.FrecuenciaRevisionTasa AS FrecuenciaRevisionTasa,
 perCap.CodigoCNBV AS PeriodicidadPagosCapital,
 perInt.CodigoCNBV AS PeriodicidadPagosInteres,
 lin.MesesGraciaCap AS NumMesesAmortCap,
 lin.MesesGraciaIntereses AS NumMesesPagoInt,
 lin.GastosOrigTasa AS ComisionAperturaTasa,
 lin.ComisionesCobradas AS ComisionAperturaMonto,
 lin.ComDispTasa AS ComisionDisposicionTasa,
 lin.ComDispMonto AS ComisionDisposicionMonto,
 munDst.CodigoCNBV AS LocalidadDestinoCredito,
 munDst.CodigoMunicipio AS MunicipioDestinoCredito,
 munDst.CodigoEstado AS EstadoDestinoCredito,
 actDst.CodigoCNBV AS ActividadDestinoCredito
FROM dbo.SICCMX_VW_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN (
 SELECT IdLineaCredito FROM dbo.SICCMX_Proyecto
 UNION ALL
 SELECT IdLineaCredito FROM dbo.SICCMX_ProyectoLinea
) AS pry ON lin.IdLineaCredito = pry.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON per.IdActividadEconomica = actEco.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON per.IdLocalidad = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
LEFT OUTER JOIN dbo.SICC_TipoAlta tpoAlta ON lin.IdTipoAlta = tpoAlta.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON lin.ProductoComercial = tcc.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON lin.TipoOperacion = tpoOper.IdTipoOperacion
LEFT OUTER JOIN dbo.SICC_Destino dest ON lin.IdDestino = dest.IdDestino
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCreditosCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_Grupal_2016 grp ON lin.IdLineaCredito = grp.IdLineaCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Moneda_Linea_Rep mon ON lin.IdLineaCredito = mon.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON lin.IdDisposicion = disp.IdDisposicion
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON lin.TipoLinea = tln.IdTipoLinea
LEFT OUTER JOIN dbo.SICC_Posicion pos ON lin.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_RegGarMob rgm ON lin.IdLineaCredito = rgm.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado deudRel ON per.IdDeudorRelacionado = deudRel.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_Institucion inst ON lin.IdInstitucionFondea = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICCMX_VW_TasaRef_Linea_Rep tasaRef ON tasaRef.IdLineaCredito = lin.IdLineaCredito
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_AjusteTasa tasa ON lin.IdLineaCredito = tasa.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON lin.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON lin.IdPeriodicidadInteres = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_Localidad2015 munDst ON lin.IdMunicipioDestino = munDst.IdLocalidad
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actDst ON lin.IdActividadEconomicaDestino = actDst.IdActividadEconomica
WHERE lin.IdTipoAlta IS NOT NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0483_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
