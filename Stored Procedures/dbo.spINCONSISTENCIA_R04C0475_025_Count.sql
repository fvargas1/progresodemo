SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_025_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (cve_ptaje_dias_mora_bcos) es = 49,
-- entonces los Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (dat_num_dias_mora_prom_bco) deben ser = -999 (Sin Informacion)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_DiasMoraInstBanc,'') = '49' AND CAST(DiasMoraInstBanc AS DECIMAL(10,6)) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
