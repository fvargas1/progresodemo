SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_012]
AS

BEGIN

-- Se validará que ID CREDITO MET CNBV esté vigente (dado de alta) y no se haya reportado como baja efectiva en el formulario de altas.

SELECT DISTINCT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICCMX_VW_Datos_0442 cat ON ISNULL(r.CodigoCreditoCNBV,'') = cat.CodigoCreditoCNBV
WHERE cat.CodigoCreditoCNBV IS NULL;

END
GO
