SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ACM_SICC_Generate]      
AS      
DECLARE @IdPeriodo BIGINT;      
DECLARE @FechaPeriodo DATETIME;      
DECLARE @IdReporte AS BIGINT;      
DECLARE @IdReporteLog AS BIGINT;      
DECLARE @TotalRegistros AS INT;      
DECLARE @TotalSaldos AS DECIMAL;      
DECLARE @TotalIntereses AS DECIMAL;      
DECLARE @FechaMigracion DATETIME;      
      
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;      
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'ACM_SICC';      
      
INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)      
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());      
      
SET @IdReporteLog = SCOPE_IDENTITY();      
      
      
TRUNCATE TABLE dbo.RW_ACM_SICC;      
      
/*US: Cuando sea posible identificar una operacion restringida, se debe agregar este dato al reporte.      
Actualmente, todo se esta reportando (a nivel vista) en "0 - No Restringido"*/      
      
INSERT INTO dbo.RW_ACM_SICC (      
 IdReporteLog,      
 Periodo,      
 Periodo_Actual,    
 Num_Credito,      
 Tipo_Cred_R04A,      
 Fecha_Ven,      
 Sector_Lab,      
 Situacion_Cred,      
 Interes_Vig,      
 Interes_Ven,      
 Saldo_Total,      
 Moneda,      
 Dias_Vencidos      
)      
SELECT      
 @IdReporteLog,      
 @IdPeriodo,      
 CONVERT(CHAR(10), @FechaPeriodo, 126),     
 NUM_CREDITO,     
 TIPO_CRED_R04A,      
 CONVERT(CHAR(10), FECHA_VEN, 126),      
 SECTOR_LAB,      
 SITUACION_CRED,      
 SUM(ISNULL (INTERESES_VIG,0)) AS INTERESES_VIG,      
 SUM(ISNULL (INTERESES_VEN,0)) AS INTERESES_VEN,      
 SUM(ISNULL (SALDO_TOTAL,0)) AS SALDO_TOTAL,      
 MONEDA,      
 DIAS_VENCIDOS      
FROM (      
 SELECT  cre.Codigo as NUM_CREDITO,     
 cr04a.Codigo AS TIPO_CRED_R04A,      
 CASE WHEN ISNULL(sitcre.Codigo,'1')='2' THEN cre.FechaTraspasoVencida ELSE cre.FechaVencimiento END AS FECHA_VEN,      
 secEco.Codigo AS SECTOR_LAB,      
 sitcre.Codigo AS SITUACION_CRED,      
 cre.InteresVigenteValorizado AS INTERESES_VIG,      
 cre.InteresVencidoValorizado AS INTERESES_VEN,      
 MontoValorizado AS SALDO_TOTAL,      
 mon.CodigoISO AS MONEDA,      
 ISNULL(DiasVencidos,0) AS DIAS_VENCIDOS      
 FROM dbo.SICCMX_VW_Credito cre      
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = cre.IdPersona      
 LEFT OUTER JOIN dbo.SICCMX_CreditoInfo cinf ON cinf.IdCredito = cre.IdCredito      
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 cr04a ON cr04a.IdTipoProducto = cinf.IdTipoProductoSerie4_2016      
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona      
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON secEco.IdSectorEconomico = pinf.IdSectorEconomico      
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = cre.IdSituacionCredito      
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cre.IdMoneda      
      
 UNION ALL      
      
      
 SELECT     
 con.Codigo,      
 cr04a.Codigo AS TIPO_CRED_R04A,      
 cinf.FechaVencimiento AS FECHA_VEN,      
 secEco.Codigo AS SECTOR_LAB,      
 sitcre.Codigo AS SITUACION_CRED,      
 con.InteresVigenteValorizado AS INTERESES_VIG,      
 con.InteresVencidoValorizado AS INTERESES_VEN,      
 con.SaldoTotalValorizado AS SALDO_TOTAL,      
 mon.CodigoISO AS MONEDA,      
 ISNULL(DiasAtraso,0)      
 FROM dbo.SICCMX_VW_Consumo con      
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = con.IdPersona      
 LEFT OUTER JOIN dbo.SICCMX_ConsumoInfo cinf ON cinf.IdConsumo = con.IdConsumo      
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 cr04a ON cr04a.IdTipoProducto = cinf.IdTipoProductoSerie4_2016     
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona      
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON secEco.IdSectorEconomico = pinf.IdSectorEconomico      
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = con.IdSituacionCredito      
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cinf.IdMoneda      
      
 UNION ALL      
      
 SELECT      
 hip.Codigo,     
 cr04a.Codigo AS TIPO_CRED_R04A,      
 hinf.FechaVencimiento AS FECHA_VEN,      
 secEco.Codigo AS SECTOR_LAB,      
 sitcre.Codigo AS SITUACION_CRED,      
 hip.InteresVigenteValorizado AS INTERESES_VIG,      
 hip.InteresVencidoValorizado AS INTERESES_VEN,      
 hip.SaldoTotalValorizado AS SALDO_TOTAL,      
 mon.CodigoISO AS MONEDA,      
 0      
 FROM dbo.SICCMX_VW_Hipotecario hip      
 LEFT OUTER JOIN dbo.SICCMX_Persona per ON per.IdPersona = hip.IdPersona      
 LEFT OUTER JOIN dbo.SICCMX_HipotecarioInfo hinf ON hinf.IdHipotecario = hip.IdHipotecario      
 LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 cr04a ON cr04a.IdTipoProducto = hip.IdTipoCreditoR04A_2016      
 LEFT OUTER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona      
 LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor secEco ON secEco.IdSectorEconomico = pinf.IdSectorEconomico      
 LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = hinf.IdSituacionCredito      
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = hinf.IdMoneda      
) AS ACM_SICC      
GROUP BY  NUM_CREDITO,TIPO_CRED_R04A, FECHA_VEN, SECTOR_LAB, SITUACION_CRED, MONEDA,DIAS_VENCIDOS;      
      
UPDATE RW_ACM_SICC SET Fecha_Ven=null WHERE Fecha_Ven='9999-12-31 00:00:00.000'      
      
SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_ACM_SICC WHERE IdReporteLog = @IdReporteLog;      
SELECT @TotalSaldos = 0;      
SELECT @TotalIntereses = 0;      
      
SET @FechaMigracion = ( SELECT MAX( Fecha ) FROM MIGRACION_ProcesoLog );      
      
UPDATE dbo.RW_ReporteLog      
SET      
 TotalRegistros = @TotalRegistros,      
 TotalSaldos = @TotalSaldos,      
 TotalIntereses = @TotalIntereses,      
 FechaCalculoProcesos = GETDATE(),      
 FechaImportacionDatos = @FechaMigracion,      
 IdFuenteDatos = 1      
WHERE IdReporteLog = @IdReporteLog;      
GO
