SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_018]
AS

BEGIN

-- El número de meses de gracia de pago de intereses debe ser mayor o igual a 0

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	NumMesesPagoInt
FROM dbo.RW_VW_R04C0473_INC
WHERE CAST(ISNULL(NULLIF(NumMesesPagoInt,''),'-1') AS DECIMAL) < 0;

END


GO
