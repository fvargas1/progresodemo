SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_026]
AS
BEGIN

-- El Saldo del Principal al Inicio del Período debe de ser igual a 0.
DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInicial
FROM dbo.RW_VW_R04C0469_INC
WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),8,6) = @FechaPeriodo AND CAST(ISNULL(NULLIF(SaldoInicial,''),'-1') AS DECIMAL) <> 0;

END


GO
