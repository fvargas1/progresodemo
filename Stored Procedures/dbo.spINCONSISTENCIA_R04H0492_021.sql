SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_021]
AS

BEGIN

-- La "SITUACIÓN DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.SituacionCredito
FROM dbo.RW_R04H0492 r
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario cat ON ISNULL(r.SituacionCredito,'') = cat.CodigoCNBV
WHERE cat.IdSituacionHipotecario IS NULL;

END
GO
