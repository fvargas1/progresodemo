SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0476_024]
AS

BEGIN

-- La SP Ajustada Por Garantías Reales Financieras debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_GarRealFin
FROM dbo.RW_VW_R04C0476_INC
WHERE CAST(ISNULL(NULLIF(SP_GarRealFin,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
