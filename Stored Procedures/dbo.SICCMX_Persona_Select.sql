SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Select](
 @IdPersona bigint
)
AS
SELECT 
 vw.IdPersona,
 vw.Codigo,
 vw.Nombre,
 vw.RFC,
 vw.Domicilio,
 0 AS IdGrupoEconomico,
 vw.IdGrupoEconomicoNombre,
 vw.Telefono,
 vw.Correo,
 vw.IdEjecutivo,
 vw.IdEjecutivoNombre,
 vw.IdInstitucion,
 vw.IdInstitucionNombre,
 vw.IdActividadEconomica,
 vw.IdActividadEconomicaNombre,
 vw.IdLocalidad,
 vw.IdLocalidadNombre,
 vw.IdDeudorRelacionado,
 vw.IdDeudorRelacionadoNombre,
 0 AS IdTamanoDeudor,
 '' AS IdTamanoDeudorNombre,
 vw.IdSucursal,
 vw.IdSucursalNombre,
 0 AS IdUsuario,
 '' AS IdUsuarioNombre,
 0 AS DiasIncumplimiento,
 0 AS IdIndustria,
 '' AS IdIndustriaNombre,
 0 AS Estado,
 0 AS IdEntidad,
 '' AS IdEntidadNombre,
 vw.FechaCreacion,
 vw.FechaActualizacion,
 vw.Username,
 '' AS SectorCodigo,
 '' AS SectorNombre
FROM dbo.SICCMX_VW_Persona vw
WHERE vw.IdPersona = @IdPersona
GO
