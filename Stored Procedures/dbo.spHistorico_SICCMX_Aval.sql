SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Aval]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Aval WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Aval (
 IdPeriodoHistorico,
 Codigo,
 Nombre,
 RFC,
 Domicilio,
 PersonalidadJuridica,
 TipoAval,
 MunAval,
 PIAval,
 Moneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 ActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 Persona,
 Aplica,
 Localidad,
 Estado,
 CodigoPostal
)
SELECT
 @IdPeriodoHistorico,
 a.Codigo,
 a.Nombre,
 a.RFC,
 a.Domicilio,
 tpoPer.Codigo, -- IdPersonalidadJuridica
 tipoAv.Codigo, -- IdTipoAval
 mun.CodigoMunicipio, -- IdMunAval
 a.PIAval,
 mon.Codigo, -- IdMoneda
 a.Calif_Fitch,
 a.Calif_Moodys,
 a.Calif_SP,
 a.Calif_HRRATINGS,
 a.Calif_Otras,
 act.Codigo, -- IdActividadEconomica
 a.LEI,
 cob.Codigo, -- TipoCobertura
 figGar.Codigo, -- FiguraGarantiza
 per.Codigo, -- IdPersona
 a.Aplica,
 a.Localidad,
 a.Estado,
 a.CodigoPostal
FROM dbo.SICCMX_Aval a
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON a.IdPersonalidadJuridica = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_TipoGarante tipoAv ON a.IdTipoAval = tipoAv.IdTipoGarante
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON a.IdMunAval = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICC_Moneda mon ON a.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON a.IdActividadEconomica = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_TipoCobertura cob ON a.TipoCobertura = cob.IdTipoCobertura
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza figGar ON a.FiguraGarantiza = figGar.IdFigura
LEFT OUTER JOIN dbo.SICCMX_Persona per ON a.IdPersona = per.IdPersona;
GO
