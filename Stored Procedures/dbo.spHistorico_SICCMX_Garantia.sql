SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Garantia WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia (
 IdPeriodoHistorico,
 Codigo,
 TipoGarantiaMA,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 ActividaEcoGarante,
 LocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 TipoGarante,
 LEI,
 IndPerMorales,
 Persona,
 CodigoPostalGarante
)
SELECT
 @IdPeriodoHistorico,
 g.Codigo,
 tipoGarMA.Codigo, -- TipoGarantiaMA
 tipoGar.Codigo, -- TipoGarantia
 g.ValorGarantia,
 mon.Codigo, -- Moneda
 g.FechaValuacion,
 g.Descripcion,
 ins.Codigo, -- BancoGarantia
 g.ValorGarantiaProyectado,
 g.RegGarantiaMob,
 g.Hc,
 g.VencimientoRestante,
 g.IdGradoRiesgo,
 agen.Codigo, -- AgenciaCalificadora
 g.Calificacion, -- Calificacion
 emi.Codigo, -- Emisor
 esc.Codigo, -- Escala
 g.EsIPC,
 g.PIGarante,
 g.NumRPPC,
 g.RFCGarante,
 g.NombreGarante,
 g.IdGarante,
 actEc.Codigo, -- ActividaEcoGarante
 loc.CodigoCNBV, -- LocalidadGarante
 g.MunicipioGarante,
 g.EstadoGarante,
 tpoGte.Codigo, -- TipoGarante
 g.LEI,
 g.IndPerMorales,
 per.Codigo,
 g.CodigoPostalGarante
FROM dbo.SICCMX_Garantia g
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA tipoGarMA ON g.IdTipoGarantiaMA = tipoGarMA.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_TipoGarantia tipoGar ON g.IdTipoGarantia = tipoGar.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON g.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Institucion ins ON g.IdBancoGarantia = ins.IdInstitucion
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON g.IdAgenciaCalificadora = agen.IdAgencia
LEFT OUTER JOIN dbo.SICC_Emisor emi ON g.IdEmisor = emi.IdEmisor
LEFT OUTER JOIN dbo.SICC_Escala esc ON g.IdEscala = esc.IdEscala
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEc ON g.IdActividaEcoGarante = actEc.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON g.IdLocalidadGarante = loc.IdLocalidad
LEFT OUTER JOIN dbo.SICC_TipoGarante tpoGte ON g.IdTipoGarante = tpoGte.IdTipoGarante
LEFT OUTER JOIN dbo.SICCMX_Persona per ON g.IdPersona = per.IdPersona;
GO
