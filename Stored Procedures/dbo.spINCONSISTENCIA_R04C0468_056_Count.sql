SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_056_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Comisión por Disposición del Crédito en Tasa (dat_com_disposicion_tasa) sea MAYOR O IGUAL a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(ISNULL(NULLIF(ComisionDisposicionTasa,''),'-1') AS DECIMAL(10,6)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
