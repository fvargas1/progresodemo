SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Accionista_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Accionista_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Accionista_Hst (
 IdPeriodoHistorico,
 Deudor,
 NombreAccionista,
 Porcentaje,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 Deudor,
 NombreAccionista,
 Porcentaje,
 Fuente
FROM dbo.FILE_Accionista_Hst;
GO
