SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_BK12_NUM_CRED_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_BK12_NUM_CRED_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Califica
SET errorFormato = 1
WHERE LEN(BK12_NUM_CRED)>0 AND (ISNUMERIC(ISNULL(BK12_NUM_CRED,'0')) = 0 OR BK12_NUM_CRED LIKE '%[^0-9 ]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoDeudor,
 'BK12_NUM_CRED',
 BK12_NUM_CRED,
 1,
 @Detalle
FROM dbo.FILE_Califica
WHERE LEN(BK12_NUM_CRED)>0 AND (ISNUMERIC(ISNULL(BK12_NUM_CRED,'0')) = 0 OR BK12_NUM_CRED LIKE '%[^0-9 ]%');
GO
