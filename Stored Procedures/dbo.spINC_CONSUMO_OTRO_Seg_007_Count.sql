SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_OTRO_Seg_007_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se debe reportar la clave (folio) de consulta a la Sociedad de Información Crediticia, que se realizó como parte del proceso de otorgamiento del crédito

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_OTRO rep
WHERE LEN(ISNULL(rep.ClaveConsultaSociedadInformacionCrediticia,'')) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
