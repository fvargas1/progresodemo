SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_044_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Monto del Pago Total Exigible al Acreditado en el Periodo (dat_monto_pago_exigible_acred) sea MAYOR O IGUAL a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(MontoPagoExigible,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
