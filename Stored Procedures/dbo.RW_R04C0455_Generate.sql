SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0455_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0455';

INSERT INTO dbo.RW_ReporteLog(IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES(@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0455;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0455 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
 HITSIC, FechaConsultaSIC, FechaInfoFinanc, MesesPI100, GarantiaLeyFederal, CumpleCritContGral, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_PorcPagoInstNoBanc,
 P_NumInstCalif, P_DeudaTotalPartEleg, P_ServDeudaIngAjust, P_DeudaCortoPlazoTotal, P_IngTotGastoCorriente, P_InvIngTotales, P_IngPropIngTotales, SdoDeudaTotal,
 SdoPartEleg, SdoIngTotalesAjust, SdoDeudaCortoPlazo, SdoIngTotales, SdoGastosCorrientes, SdoInversion, SdoIngPropios, P_TasaDesempLocal, P_ServFinEntReg,
 P_ObligConting, P_BalanceOpPIB, P_NivEficRecauda, P_SolFlexEjecPres, P_SolFlexImpLoc, P_TranspFinPublicas, P_EmisionDeuda
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0455',
 per.Codigo AS CodigoPersona,
 per.RFC AS RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.PonderadoCuantitativo AS PuntajeCuantitativo,
 ppi.PonderadoCualitativo AS PuntajeCualitativo,
 crs.CodigoCNBV AS CreditoReportadoSIC,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 CASE WHEN ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) > 12 THEN 0 ELSE ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) END AS MesesPI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 ccc.CodigoCNBV AS CumpleCritContGral,
 ptNMC.[EM_DIAS_MORA] AS P_DiasMoraInstBanc,
 ptNMC.[EM_POR_PAGOS_BANCARIOS] AS P_PorcPagoInstBanc,
 ptNMC.[EM_POR_PAGOS_NOBANCARIOS] AS P_PorcPagoInstNoBanc,
 ptNMC.[EM_NUM_INSTITUCIONES] AS P_NumInstCalif,
 ptNMC.[EM_DEUDA_TOTAL] AS P_DeudaTotalPartEleg,
 ptNMC.[EM_SERVICIO_DEUDA] AS P_ServDeudaIngAjust,
 ptNMC.[EM_DEUDA_CORTOPLAZO] AS P_DeudaCortoPlazoTotal,
 ptNMC.[EM_INGRESOS_TOTALES] AS P_IngTotGastoCorriente,
 ptNMC.[EM_INVERSION] AS P_InvIngTotales,
 ptNMC.[EM_INGRESOS_AUTONOMOS] AS P_IngPropIngTotales,
 anx.SaldoDeudaTotal AS SdoDeudaTotal,
 anx.SaldoPartEle AS SdoPartEleg,
 anx.SaldoIngTotAju AS SdoIngTotalesAjust,
 anx.SaldoDeuCortPlzo AS SdoDeudaCortoPlazo,
 anx.SaldoIngTot AS SdoIngTotales,
 anx.SaldoGastoCorr AS SdoGastosCorrientes,
 anx.SaldoInversion AS SdoInversion,
 anx.SaldoIngPropios AS SdoIngPropios,
 ptNMC.[EM_TASA_DESEMPLEO] AS P_TasaDesempLocal,
 ptNMC.[EM_PRESENCIA_SERVICIOS] AS P_ServFinEntReg,
 ptNMC.[EM_OBLIGACIONES_CON] AS P_ObligConting,
 ptNMC.[EM_BALANCE_OPERATIVO] AS P_BalanceOpPIB,
 ptNMC.[EM_NIVEL_RECAUDACION] AS P_NivEficRecauda,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_EJE] AS P_SolFlexEjecPres,
 ptNMC.[EM_SOLIDEZ_FLEXIBILIDAD_IMP] AS P_SolFlexImpLoc,
 ptNMC.[EM_TRANSPARENCIA_FINANZAS] AS P_TranspFinPublicas,
 ptNMC.[EM_EMISION_DEUDA] AS P_EmisionDeuda
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo18 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A18 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_CredRepSIC crs ON cInfo.CredRepSIC = crs.IdCredRepSIC
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN (
 SELECT IdPersona, MAX(FechaIngreso) AS FechaIngreso
 FROM dbo.SICCMX_Creditos_Castigados_SIC
 GROUP BY IdPersona
) AS castigados ON per.IdPersona = castigados.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral ccc ON cInfo.CumpleCritContGral = ccc.IdCumpleCritContGral
WHERE cre.MontoValorizado > 0;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0455 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
