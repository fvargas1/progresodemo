SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Auditoria_24_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @TC_UDI DECIMAL(10,6);
DECLARE @SI VARCHAR(15);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'AUDITORIA' AND Nombre = '_SP';

SELECT @TC_UDI=tipoC.Valor
FROM dbo.SICC_Periodo periodo
INNER JOIN dbo.SICC_TipoCambio tipoC ON periodo.IdPeriodo = tipoC.IdPeriodo
INNER JOIN dbo.SICC_Moneda moneda ON tipoC.IdMoneda = moneda.IdMoneda
WHERE periodo.Activo=1 AND moneda.Codigo=(SELECT VALUE FROM dbo.BAJAWARE_Config WHERE Codename='MonedaUdis');

SET @SI = '-999';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Auditoria_24;

INSERT INTO dbo.RW_Auditoria_24 (
 IdReporteLog, ID, IDC, N_A, FO, TC, LC, S_Total, IDevNC_B, IDevNC_CO, GC, SGC, ATR_SIC, ATR_BC, INF_SIC, Cre_Litigio, Mto_Ob, Estatus, ICV,
 Recla, Enpro, CM, HPI, UDI, ING, PIC, PIE, SPC, SPE, EIC, EIE, RVA_G, RVA_PYM, RVA_CPP, RVA_C, RVA_E, RVA_T, PI_CT, PI_PC, PI_CC, PI_A, PI_fac,
 PI_CG, PI_PCG, SPO_ATRA, SPO_IF_PC, SPO_GTIA_R, Cre_con_GF, GF_Cuantos, GF_EIO, GF_He, GF_C, GF_Hc, GF_Hfx, GF_EIAJ, GF_SPC, Cre_con_GNF,
 GNF_NC, GNF_CiGR, GNF_CDF, GNF_CIN, GNF_CMU, GNF_CF1, GNF_CF2, GNF_CF3, GNF_Cadi, GNF_EIE, Cre_con_P, GP_PIAVAL100, GP_PIG, GP_PC, GP_Ga,
 GP_Pa, Cre_con_PYM, GC_Cob_PYM, GC_PI_GA, GC_SP_GA, Cre_con_PP, GC_PCPP, GC_SSi, GC_RVAS_Portafolio, GC_Mto_Cob_pp, GC_PRVAS_Portafolio,
 GC_DIFPP, EI_S, SOFOM, B1, B2, B3, B4, B5
)
SELECT
 @IdReporteLog,
 credito.CodigoCredito AS ID, -- 01
 persona.Codigo AS IDC, -- 02
 REPLACE(pInfo.NombreCNBV,',','') AS N_A, -- 03
 SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(credito.FechaAutorizacionOriginal,0),102),'.',''),1,6) AS FO, -- 04
 ISNULL(tcc.Codigo,'9999') AS TC, -- 05
 CAST(credito.MontoLineaCredito AS DECIMAL(23,2)) AS LC, -- 06
 CAST (credito.MontoValorizado AS DECIMAL(23,2)) AS S_Total, -- 07
 '' AS IDevNC_B, -- 08
 '' AS IDevNC_CO, -- 09
 met.Nombre AS GC, -- 10
 CASE WHEN met.Codigo = '20' THEN CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre,0) = 1 THEN 'Otorgante de Crédito' ELSE 'No Otorgante de Crédito' END
 WHEN met.Codigo = '21' THEN CASE WHEN ISNULL(anx.SinAtrasos,0) = 1 THEN 'Sin Atrasos' ELSE 'Con Atrasos' END
 WHEN met.Codigo = '22' THEN CASE ppi.IdClasificacion WHEN 1 THEN 'Pequeño Corporativo' WHEN 2 THEN 'Corporativo' WHEN 3 THEN 'Grande Corporativo' WHEN 4 THEN 'Organismo Descentralizado' END
 END AS SGC, -- 11
 '' AS ATR_SIC, -- 12
 lit.Morosidad AS ATR_BC, -- 13
 hts.Codigo AS INF_SIC, -- 14
 CASE WHEN ISNULL(lit.Litigio,0) = 1 THEN 'Si' ELSE 'No' END AS Cre_Litigio, -- 15
 '' AS Mto_Ob, -- 16
 sitCre.Nombre AS Estatus, -- 17
 CASE WHEN ((credito.SaldoVencidoValorizado + credito.InteresVencidoValorizado) / prct.MontoTotal * 100) < 5 THEN 1 ELSE 0 END AS ICV, -- 18
 '' AS Recla, -- 19
 ISNULL(cInfo.Emproblemado,0) AS Enpro, -- 20
 '' AS CM, -- 21
 CASE WHEN castigados.IdPersona IS NULL THEN 0 ELSE 1 END AS HPI, -- 22
 @TC_UDI AS UDI, -- 23
 CAST(ISNULL(anx.VentNetTotAnuales,0) AS DECIMAL(23,2)) AS ING, -- 24
 CAST(ISNULL(crv.PI_Cubierta,0) AS DECIMAL(10,6)) AS PIC, -- 25
 CAST(ISNULL(crv.PI_Expuesta,0) AS DECIMAL(10,6)) AS PIE, -- 26
 CAST(ISNULL(crv.SP_Cubierta,0) AS DECIMAL(10,6)) AS SPC, -- 27
 CAST(ISNULL(crv.SP_Expuesta,0) AS DECIMAL(10,6)) AS SPE, -- 28
 CAST(ISNULL(crv.EI_Cubierta_GarPer,0) AS DECIMAL(23,2)) AS EIC, -- 29
 CAST(ISNULL(crv.EI_Expuesta_GarPer,0) AS DECIMAL(23,2)) AS EIE, -- 30
 CAST(ISNULL(gRa.ReservaGP,0) AS DECIMAL(23,2)) AS RVA_G, -- 31
 CAST(ISNULL(gpPM.Reserva,0) AS DECIMAL(23,2)) AS RVA_PYM, -- 32
 CAST(ISNULL(gpPP.ReservaCubierto,0) AS DECIMAL(23,2)) AS RVA_CPP, -- 33
 CAST(ISNULL(crv.ReservaCubierta_GarPer,0) AS DECIMAL(23,2)) AS RVA_C, -- 34
 CAST(ISNULL(crv.ReservaExpuesta_GarPer,0) AS DECIMAL(23,2)) AS RVA_E, -- 35
 CAST(ISNULL(crv.ReservaFinal,0) AS DECIMAL(23,2)) AS RVA_T, -- 36
 ISNULL(ppi.FactorTotal,0) AS PI_CT, -- 37
 ISNULL(ppi.FactorCuantitativo,0) AS PI_PC, -- 38
 ISNULL(ppi.FactorCualitativo,0) AS PI_CC, -- 39
 CAST(ISNULL(delta.FactorCuantitativo / 100,0) AS DECIMAL(10,6)) AS PI_A, -- 40
 CASE WHEN fond.Nombre LIKE 'Art. 112%' THEN 1 ELSE 0 END AS PI_fac, -- 41
 CASE WHEN gFondo.IdCredito IS NULL THEN 0 ELSE 1 END AS PI_CG, -- 42
 CAST(ISNULL(gFondo.Porcentaje,0) AS DECIMAL(10,6)) AS PI_PCG, -- 43
 ISNULL(credito.MesesIncumplimiento,0) AS SPO_ATRA, -- 44
 CASE WHEN pos.Codigo = '2' THEN 1 ELSE 0 END AS SPO_IF_PC, -- 45
 CASE WHEN gRA.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS SPO_GTIA_R, -- 46
 CASE WHEN ISNULL(gRA.CantidadGF,0) > 0 THEN 1 ELSE 0 END AS Cre_con_GF, -- 47
 ISNULL(gCnt.CntGF,0) AS GF_Cuantos, -- 48
 '' AS GF_EIO, -- 49
 CAST(ISNULL(gRA.He,0) AS DECIMAL(10,6)) AS GF_He, -- 50
 CAST(ISNULL(gRA.MontoGF,0) AS DECIMAL(23,2)) AS GF_C, -- 51
 CAST(ISNULL(gRA.Hc,0) AS DECIMAL(10,6)) AS GF_Hc, -- 52
 CAST(ISNULL(gRA.Hfx,0) AS DECIMAL(10,6)) AS GF_Hfx, -- 53
 CAST(dbo.MAX2VAR(0, crv.EI_Total - ISNULL(gRA.MontoGNF,0)) AS DECIMAL(23,2)) AS GF_EIAJ, -- 54
 CAST(ISNULL(spRep.SP_GarRealNoFin,0) AS DECIMAL(10,6)) AS GF_SPC, -- 55
 CASE WHEN ISNULL(gRA.CantidadGNF,0) > 0 THEN 1 ELSE 0 END AS Cre_con_GNF, -- 56
 ISNULL(gCnt.CntGNF,0) AS GNF_NC, -- 57
 CAST(ISNULL(spRep.Porcentaje,0) * 100 AS DECIMAL(18,6)) AS GNF_CiGR, -- 58
 CAST(ISNULL(gRA.ValorDC,0) AS DECIMAL(23,2)) AS GNF_CDF, -- 59
 CAST(ISNULL(gRA.ValorBI,0) AS DECIMAL(23,2)) AS GNF_CIN, -- 60
 CAST(ISNULL(gRA.ValorBM,0) AS DECIMAL(23,2)) AS GNF_CMU, -- 61
 CAST(ISNULL(gRA.ValorFAF,0) AS DECIMAL(23,2)) AS GNF_CF1, -- 62
 CAST(ISNULL(gRA.ValorFIP,0) AS DECIMAL(23,2)) AS GNF_CF2, -- 63
 CAST(ISNULL(gRA.ValorAMB,0) AS DECIMAL(23,2)) AS GNF_CF3, -- 64
 '' AS GNF_Cadi, -- 65
 '0' AS GNF_EIE, -- 66
 CASE WHEN gRA.CantidadPer > 0 THEN 1 ELSE 0 END AS Cre_con_P, -- 67
 CASE WHEN av100.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS GP_PIAVAL100, -- 68
 CAST(ISNULL(av100.PIAval,0) AS DECIMAL(10,6)) AS GP_PIG, -- 69
 0 AS GP_PC, -- 70
 0 AS GP_Ga, -- 71
 0 AS GP_Pa, -- 72
 CASE WHEN gpPM.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS Cre_con_PYM, -- 73
 CAST(ISNULL(gpPM.Porcentaje,0) AS DECIMAL(10,6)) AS GC_Cob_PYM, -- 74
 CAST(ISNULL(gpPM.[PI],0) AS DECIMAL(10,6)) AS GC_PI_GA, -- 75
 CAST(ISNULL(gpPM.SP,0) AS DECIMAL(10,6)) AS GC_SP_GA, -- 76
 CASE WHEN gpPP.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS Cre_con_PP, -- 77
 CAST(ISNULL(gpPP.Porcentaje,0) AS DECIMAL(10,6)) AS GC_PCPP, -- 78
 '' AS GC_SSi, -- 79
 '' AS GC_RVAS_Portafolio, -- 80
 '' AS GC_Mto_Cob_pp, -- 81
 '' AS GC_PRVAS_Portafolio, -- 82
 '' AS GC_DIFPP, -- 83
 '' AS EI_S, -- 84
 '' AS SOFOM, -- 85
 '' AS B1, -- 86
 '' AS B2, -- 87
 '' AS B3, -- 88
 '' AS B4, -- 89
 '' AS B5 -- 90
FROM dbo.SICCMX_Persona persona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON persona.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON persona.IdPersona = credito.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON credito.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON credito.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON credito.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON persona.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_ValoresDelta delta ON ppi.IdMetodologia = delta.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(delta.IdClasificacion,0)
INNER JOIN (
 SELECT crLit.IdPersona, MAX(CAST(ciLit.ConcursoMercantil AS INT)) AS Litigio, MAX(crLit.IdSituacionCredito) AS IdSituacionCredito, MAX(ciLit.DiasMorosidad) AS Morosidad
 FROM dbo.SICCMX_Credito crLit
 INNER JOIN dbo.SICCMX_CreditoInfo ciLit ON crLit.IdCredito = ciLit.IdCredito
 GROUP BY crLit.IdPersona
) AS lit ON persona.IdPersona = lit.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON cInfo.ProductoComercial = tcc.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_HitSic hts ON pInfo.HITSIC = hts.IdHitSic
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON credito.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasPersReportes garPer ON credito.IdCredito = garPer.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PM gpPM ON credito.IdCredito = gpPM.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PP gpPP ON credito.IdCredito = gpPP.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fond ON persona.EsFondo = fond.IdTipoReserva
LEFT OUTER JOIN (
 SELECT IdPersona, VentNetTotAnuales, 0 AS EntFinAcreOtorgantesCre, SinAtrasos FROM dbo.SICCMX_Anexo21
 UNION
 SELECT IdPersona, VentNetTotAnuales, 0, 0 FROM dbo.SICCMX_Anexo22
 UNION
 SELECT IdPersona, ActivoTotal, EntFinAcreOtorgantesCre, 0 FROM dbo.SICCMX_Anexo20
) AS anx ON persona.IdPersona = anx.IdPersona
LEFT OUTER JOIN (
 SELECT cub.IdCredito, SUM(cub.Porcentaje) AS Porcentaje
 FROM (
 SELECT IdCredito, Porcentaje FROM dbo.SICCMX_VW_Garantias_PM
 UNION ALL
 SELECT IdCredito, PrctAval FROM dbo.SICCMX_VW_GarantiasPersReportes WHERE TipoAval='7'
 ) AS cub
 GROUP BY cub.IdCredito
) AS gFondo ON credito.IdCredito = gFondo.IdCredito
LEFT OUTER JOIN dbo.SICC_Posicion pos ON cInfo.Posicion = pos.IdPosicion
LEFT OUTER JOIN (
 SELECT
 c.IdCredito,
 MAX(c.He) AS He,
 MAX(c.Hc) AS Hc,
 MAX(c.Hfx) AS Hfx,
 COUNT(c.IdCredito) AS Cantidad,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 1 ELSE 0 END) AS CantidadGF,
 SUM(CASE WHEN c.IdTipoGarantia IS NULL THEN c.C ELSE 0 END) AS MontoGF,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN c.C ELSE 0 END) AS MontoGNF,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN 1 ELSE 0 END) AS CantidadGNF,
 SUM(CASE WHEN tg.Codigo='NMC-03' THEN c.C ELSE 0 END) AS ValorDC,
 SUM(CASE WHEN tg.Codigo='NMC-01' THEN c.C ELSE 0 END) AS ValorBI,
 SUM(CASE WHEN tg.Codigo='NMC-02' THEN c.C ELSE 0 END) AS ValorBM,
 SUM(CASE WHEN tg.Codigo='EYM-01' THEN c.C ELSE 0 END) AS ValorFAF,
 SUM(CASE WHEN tg.Codigo='EYM-02' THEN c.C ELSE 0 END) AS ValorFIP,
 SUM(CASE WHEN tg.Codigo='EYM-03' THEN c.C ELSE 0 END) AS ValorAMB,
 SUM(CASE WHEN tg.Codigo='GP' THEN c.Reserva ELSE 0 END) AS ReservaGP,
 SUM(CASE WHEN tg.Codigo='GP' THEN 1 ELSE 0 END) AS CantidadPer
 FROM dbo.SICCMX_Garantia_Canasta c
 LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON c.IdTipoGarantia = tg.IdTipoGarantia
 WHERE c.EsDescubierto IS NULL
 GROUP BY c.IdCredito
) AS gRA ON credito.IdCredito = gRA.IdCredito
LEFT OUTER JOIN (
 SELECT cg.IdCredito, SUM(CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 1 ELSE 0 END) AS CntGF, SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN 1 ELSE 0 END) AS CntGNF
 FROM dbo.SICCMX_CreditoGarantia cg
 INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
 INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
 INNER JOIN dbo.SICCMX_Garantia_Canasta c ON cg.IdCredito = c.IdCredito AND CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 0 ELSE tg.IdTipoGarantia END = ISNULL(c.IdTipoGarantia,0) AND c.EsDescubierto IS NULL
 GROUP BY cg.IdCredito
) AS gCnt ON credito.IdCredito = gCnt.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasSPReportes spRep ON credito.IdCredito = spRep.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Creditos_Castigados_SIC castigados ON persona.IdPersona = castigados.IdPersona AND credito.CodigoCredito = castigados.CodigoCredito
LEFT OUTER JOIN (
 SELECT
 IdPersona,
 SUM(SaldoVigenteValorizado+InteresVigenteValorizado) AS MontoVigente,
 SUM(SaldoVencidoValorizado+InteresVencidoValorizado) AS MontoVencido,
 SUM(MontoValorizado) AS MontoTotal,
 SUM(SaldoVigenteValorizado+InteresVigenteValorizado) / SUM(MontoValorizado) * 100 AS PrctVigente,
 SUM(SaldoVencidoValorizado+InteresVencidoValorizado) / SUM(MontoValorizado) * 100 AS PrctVencido
 FROM dbo.SICCMX_VW_Credito_NMC
 WHERE MontoValorizado>0
 GROUP BY IdPersona
) AS prct ON credito.IdPersona = prct.IdPersona
LEFT OUTER JOIN (
 SELECT ca.IdCredito, a.PIAval
 FROM dbo.SICCMX_CreditoAval ca
 INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
 INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
 INNER JOIN dbo.SICCMX_Garantia_Canasta c ON ca.IdCredito = c.IdCredito
 INNER JOIN dbo.SICC_TipoGarantia t ON c.IdTipoGarantia = t.IdTipoGarantia AND t.Codigo='GP'
 WHERE tg.Codigo='7' AND c.PrctCobAjust >= 1 AND ca.Aplica = 1
) av100 ON credito.IdCredito = av100.IdCredito;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Auditoria_24 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
