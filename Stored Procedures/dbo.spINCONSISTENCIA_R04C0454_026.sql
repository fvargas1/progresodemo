SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_026]
AS
BEGIN
-- Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo debe ser diferente de 0.

SELECT
	CodigoCredito,
	InstitucionFondeo
FROM dbo.RW_VW_R04C0454_INC
WHERE ISNULL(NULLIF(InstitucionFondeo,''),'0') = '0';

END
GO
