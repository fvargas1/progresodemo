SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_002]
AS

BEGIN

-- El campo EXPOSI INCUMPLI METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	EIInterna AS EI_MetodologiaInterna
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(EIInterna,''),'-1') AS DECIMAL) < 0;

END

GO
