SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_009_Count]


 @IdInconsistencia BIGINT


AS


BEGIN


-- Validar que el Municipio del Garante corresponda a Catalogo de CNBV.





DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;





DECLARE @count INT;





SELECT @count = COUNT(DISTINCT r50.CodigoCredito)


FROM dbo.RW_VW_R04C0450_INC r50


LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON r50.Municipio = mun.CodigoMunicipio


WHERE mun.IdLocalidad IS NULL;





INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());





SELECT @count;





END
GO
