SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0467_003]
AS

BEGIN

-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 133, validar que el mismo ID Metodología CNBV (dat_id_credito_met_cnbv)
-- se encuentre en el subreporte 463 con Tipo de Alta (cve_tipo_alta_credito) = 133.

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
	rep.TipoBaja,
	hist.TipoAlta
FROM dbo.RW_VW_R04C0467_INC rep
LEFT OUTER JOIN Historico.RW_R04C0463 hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE rep.TipoBaja = '133' AND hist.TipoAlta <> '133';

END

GO
