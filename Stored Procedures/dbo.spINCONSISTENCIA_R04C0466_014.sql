SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_014]

AS



BEGIN



-- Validar que los Nombres no contengan caracteres especiales



SELECT

	CodigoCredito,

	NumeroDisposicion,

	REPLACE(NombrePersona, ',', '') AS NombreDeudor

FROM dbo.RW_R04C0466

WHERE NombrePersona LIKE '%[^a-z,.0-9 ]%' AND NombrePersona NOT LIKE '%&%' OR NombrePersona LIKE '%[áéíóú]%';



END
GO
