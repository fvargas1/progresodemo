SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_104]
AS

BEGIN

-- El RFC de los acreditados debe ser de 13 posiciones.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC
FROM dbo.RW_VW_R04C0468_INC
WHERE LEN(ISNULL(RFC,'')) <> 13;

END


GO
