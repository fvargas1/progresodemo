SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_PP]
AS
-- ACTUALIZAMOS RESERVAS ORIGINALES
UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET
 ReservaCubiertaOriginal = ReservaCubierta_GarPer,
 ReservaExpuestaOriginal = ReservaExpuesta_GarPer,
 ReservaFinalOriginal = ReservaFinal;


TRUNCATE TABLE dbo.SICCMX_Garantia_PP;
TRUNCATE TABLE dbo.SICCMX_CreditoGarantia_PP;


DECLARE @IdTipoGarantia BIGINT;
SELECT @IdTipoGarantia = IdTipoGarantia FROM dbo.SICC_TipoGarantia WHERE Codigo='NMC-04';

-- SE INSERTAN CREDITOS CON GARANTIAS DE PRIMERAS PERDIDAS
INSERT INTO dbo.SICCMX_CreditoGarantia_PP (IdCredito, IdGarantia)
SELECT cg.IdCredito, cg.IdGarantia
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cg.IdCredito = crv.IdCredito
WHERE g.IdTipoGarantia = @IdTipoGarantia AND crv.EI_Total > 0;

-- SE CALCULA EL PORCENTAJE CORRESPONDIENTE A CADA CREDITO
UPDATE cg
SET PrctReserva = dbo.NoN(rv.ReservaFinal, tcg.Reserva)
FROM dbo.SICCMX_CreditoGarantia_PP cg
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables rv ON cg.IdCredito = rv.IdCredito
INNER JOIN (
 SELECT cgPP.IdGarantia, SUM(resVar.ReservaFinal) AS Reserva
 FROM dbo.SICCMX_CreditoGarantia_PP cgPP
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables resVar ON cgPP.IdCredito = resVar.IdCredito
 GROUP BY cgPP.IdGarantia
) AS tcg ON cg.IdGarantia = tcg.IdGarantia;


-- SE INSERTAN TOTALES DE GARANTIAS DE PRIMERAS PERDIDAS
INSERT INTO dbo.SICCMX_Garantia_PP (IdGarantia, MontoCubierto, MontoExpuesto, PIGarante, SPGarante)
SELECT
 gar.IdGarantia,
 gar.ValorGarantia,
 dbo.MAX2VAR(0, tcg.Reserva - gar.ValorGtiaValorizado),
 gar.PIGarante,
 .45
FROM dbo.SICCMX_VW_Garantia gar
INNER JOIN (
 SELECT cgPP.IdGarantia, SUM(crv.ReservaFinal) AS Reserva
 FROM dbo.SICCMX_CreditoGarantia_PP cgPP
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cgPP.IdCredito = crv.IdCredito
 GROUP BY cgPP.IdGarantia
) AS tcg ON gar.IdGarantia = tcg.IdGarantia
WHERE gar.IdTipoGarantia = @IdTipoGarantia;


UPDATE cgPP
SET ReservaExpuesta = crv.PI_Total * crv.SP_Total * (crv.EI_Total * (1 - dbo.MIN2VAR(1, (gPP.MontoCubierto / (gPP.MontoCubierto + gPP.MontoExpuesto)))))
FROM dbo.SICCMX_CreditoGarantia_PP cgPP
INNER JOIN dbo.SICCMX_Garantia_PP gPP ON cgPP.IdGarantia = gPP.IdGarantia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cgPP.IdCredito = crv.IdCredito;

UPDATE cgPP
SET ReservaCubierta = gPP.PIGarante * gPP.SPGarante * (crv.ReservaFinalOriginal - cgPP.ReservaExpuesta)
FROM dbo.SICCMX_CreditoGarantia_PP cgPP
INNER JOIN dbo.SICCMX_Garantia_PP gPP ON cgPP.IdGarantia = gPP.IdGarantia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cgPP.IdCredito = crv.IdCredito;

UPDATE dbo.SICCMX_CreditoGarantia_PP SET Reserva = ReservaCubierta + ReservaExpuesta;


UPDATE gPP
SET ReservaCubierto = cgPP.ReservaCubierta,
 ReservaExpuesto = cgPP.ReservaExpuesta,
 ReservaFinal = cgPP.Reserva
FROM dbo.SICCMX_Garantia_PP gPP
INNER JOIN (
 SELECT IdGarantia, SUM(ReservaExpuesta) AS ReservaExpuesta, SUM(ReservaCubierta) AS ReservaCubierta, SUM(Reserva) AS Reserva
 FROM dbo.SICCMX_CreditoGarantia_PP
 GROUP BY IdGarantia
) AS cgPP ON gPP.IdGarantia = cgPP.IdGarantia;


-- ACTUALIZA EL PORCENTAJE TOTAL CUBIERTO
UPDATE g
SET PrctCubierto = g.MontoCubierto / pg.Monto
FROM dbo.SICCMX_Garantia_PP g
INNER JOIN (
 SELECT cg.IdGarantia, CAST(SUM(c.MontoValorizado) AS DECIMAL(23,2)) AS Monto
 FROM dbo.SICCMX_VW_Credito_NMC c
 INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg ON c.IdCredito = cg.IdCredito
 GROUP BY cg.IdGarantia
) AS pg ON g.IdGarantia = pg.IdGarantia;



-- SE ACTUALIZA EN RESERVAS CALCULADAS POR CREDITO
UPDATE rv
SET ReservaCubierta = cgPP.Reserva * (1 - can.PrctCobAjust),
 ReservaExpuesta = cgPP.Reserva * can.PrctCobAjust,
 ReservaFinal = (cgPP.Reserva * (1 - can.PrctCobAjust)) + (cgPP.Reserva * can.PrctCobAjust),
 PorReservaFinal = ((cgPP.Reserva * (1 - can.PrctCobAjust)) + (cgPP.Reserva * can.PrctCobAjust)) / rv.EI_Total,
 ReservaCubPP = cgPP.Reserva,
 PorCubiertoPP = cgPP.PrctReserva
FROM dbo.SICCMX_Credito_Reservas_Variables rv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cgPP ON rv.IdCredito = cgPP.IdCredito
INNER JOIN dbo.SICCMX_Garantia_Canasta can ON rv.IdCredito = can.IdCredito
WHERE can.EsDescubierto = 1;


--Monto Cubierto Primeras Perdidas
UPDATE rv
SET ECubiertaPP = PorCubiertoPP * garPP.MontoCubierto
FROM dbo.SICCMX_Credito_Reservas_Variables rv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cgPP ON rv.IdCredito = cgPP.IdCredito
INNER JOIN dbo.SICCMX_Garantia_PP garPP ON cgPP.IdGarantia = garPP.IdGarantia;


UPDATE rv
SET CalifFinal = (SELECT IdCalificacion FROM dbo.SICC_CalificacionNvaMet WHERE PorReservaFinal BETWEEN RangoMenor AND RangoMayor)
FROM dbo.SICCMX_Credito_Reservas_Variables rv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cgPP ON rv.IdCredito = cgPP.IdCredito;


-- SE INSERTAN LOGS DE CALCULO
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 rv.IdCredito,
 GETDATE(),
 '',
 'Se actualiza la Reserva Cubierta a ' + CONVERT(VARCHAR, CAST(rv.ReservaCubierta AS MONEY), 1)
 + ' y la Reserva Expuesta a ' + CONVERT(VARCHAR, CAST(rv.ReservaExpuesta AS MONEY), 1)
 + ' dando como Reserva Final ' + CONVERT(VARCHAR, CAST(rv.ReservaFinal AS MONEY), 1)
 + ' por concepto de Garantía de Primeras Perdidas.'
FROM dbo.SICCMX_Credito_Reservas_Variables rv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cgPP ON rv.IdCredito = cgPP.IdCredito;


UPDATE crv
SET ReservaCubierta_GarPer = cg.ReservaCubierta,
 ReservaExpuesta_GarPer = cg.ReservaExpuesta
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg ON crv.IdCredito = cg.IdCredito;


UPDATE crv
SET EI_Expuesta_GarPer = crv.EI_Total * (1 - dbo.MIN2VAR(1, dbo.NoN(g.MontoCubierto, gar.Reserva))),
 EI_Cubierta_GarPer = crv.ReservaFinalOriginal - cg.ReservaExpuesta,
 PI_Cubierta = g.PIGarante,
 SP_Cubierta = g.SPGarante
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg ON crv.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_Garantia_PP g ON cg.IdGarantia = g.IdGarantia
INNER JOIN (
 SELECT cg2.IdGarantia, SUM (crv2.ReservaFinalOriginal) AS Reserva
 FROM dbo.SICCMX_Credito_Reservas_Variables crv2
 INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg2 ON crv2.IdCredito = cg2.IdCredito
 GROUP BY cg2.IdGarantia
) AS gar ON g.IdGarantia = gar.IdGarantia;



UPDATE crv
SET SP_Total =
 CASE
 WHEN ((crv.ReservaFinalOriginal - cg.ReservaExpuesta)+(crv.EI_Total * (1 - dbo.MIN2VAR(1, (g.MontoCubierto / (g.MontoCubierto + g.MontoExpuesto)))))) = 0
 THEN 0
 ELSE (CAST(crv.SP_Total * (crv.EI_Total * (1 - dbo.MIN2VAR(1, (g.MontoCubierto / (g.MontoCubierto + g.MontoExpuesto))))) AS DECIMAL(22,10))
 / ((crv.ReservaFinalOriginal - cg.ReservaExpuesta)+(crv.EI_Total * (1 - dbo.MIN2VAR(1, (g.MontoCubierto / (g.MontoCubierto + g.MontoExpuesto)))))))
 + (CAST(g.SPGarante * (crv.ReservaFinalOriginal - cg.ReservaExpuesta) AS DECIMAL(22,10))
 / ((crv.ReservaFinalOriginal - cg.ReservaExpuesta)+(crv.EI_Total * (1 - dbo.MIN2VAR(1, (g.MontoCubierto / (g.MontoCubierto + g.MontoExpuesto)))))))
 END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg ON crv.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_Garantia_PP g ON cg.IdGarantia = g.IdGarantia;

UPDATE crv
SET PI_Total = CASE WHEN (crv.EI_Total * crv.SP_Total) = 0 THEN 0 ELSE crv.ReservaFinal / CAST(crv.EI_Total * crv.SP_Total AS DECIMAL(25,4)) END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_CreditoGarantia_PP cg ON crv.IdCredito = cg.IdCredito;
GO
