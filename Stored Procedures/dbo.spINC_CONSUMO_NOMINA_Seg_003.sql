SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_NOMINA_Seg_003]
AS

BEGIN

-- El campo "Mecanismo de Pago" se debe reportar, respecto del mecanismo de pago del crédito de que se trate:
-- 10 = cuando se tenga pactado el pago mediante "Domiciliación en una cuenta de depósito bancario";
-- 20 = cuando se tenga pactado el pago periódico mediante "Cargos Recurrentes en una tarjeta de crédito";
-- 30 = cuando se tenga pactado un "Convenio" con la empresa donde trabaje el acreditado, por medio del cual dicha
-- empresa se compromete a descontar por nómina el importe del Monto Exigible de cada Período de Facturación,
-- y 80 = cuando no se tenga previsto algún mecanismo de pago automático.

SELECT DISTINCT
	rep.FolioCredito,
	rep.MecanismoPago
FROM dbo.RW_Consumo_NOMINA rep
LEFT OUTER JOIN dbo.SICC_FormaPagoConsumo cat ON rep.MecanismoPago = cat.CodigoBanxico
WHERE cat.IdFormaPagoConsumo IS NULL;

END
GO
