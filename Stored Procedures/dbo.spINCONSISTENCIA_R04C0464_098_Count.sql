SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_098_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Número de Día Utilizados para el Cálculo del Intereses en el periodo reportado sea MAYOR O IGUAL a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(DiasCalculoInteres,''),'-1') AS DECIMAL) < 0

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
