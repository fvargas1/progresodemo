SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Preliminares_Anexo18]
AS
DECLARE @TIIE DECIMAL(10,6);
DECLARE @PBASE DECIMAL(10,6);

SELECT @TIIE = tv.Valor
FROM dbo.SICC_TasaValor tv
INNER JOIN dbo.SICC_Tasa t ON tv.IdTasa = t.IdTasa AND t.Codigo='TIIE'
INNER JOIN dbo.SICC_Periodo p ON tv.IdPeriodo = p.IdPeriodo AND p.Activo=1;

SELECT @PBASE = tv.Valor
FROM dbo.SICC_TasaValor tv
INNER JOIN dbo.SICC_Tasa t ON tv.IdTasa = t.IdTasa AND t.Codigo='PBASE'
INNER JOIN dbo.SICC_Periodo p ON tv.IdPeriodo = p.IdPeriodo AND p.Activo=1;


TRUNCATE TABLE dbo.SICCMX_Anexo18_Preliminares;

INSERT INTO dbo.SICCMX_Anexo18_Preliminares (IdPersona)
SELECT DISTINCT IdPersona
FROM dbo.SICCMX_Anexo18;

UPDATE pre
SET ServicioDeuda = ISNULL(anx.ServicioDeuda, (ISNULL(anx.SaldoDeudaTotal,0) + ISNULL(anx.EmisionesBursatiles,0)) * (@TIIE + @PBASE))
FROM dbo.SICCMX_Anexo18_Preliminares pre
INNER JOIN dbo.SICCMX_VW_Anexo18 anx ON pre.IdPersona = anx.IdPersona;

UPDATE pre
SET
	DeudaTotalPartEleg = ISNULL(anx.DeudaTotalPartEleg, CASE WHEN anx.SaldoPartEle > 0 THEN ((ISNULL(anx.SaldoDeudaTotal,0) + ISNULL(anx.EmisionesBursatiles,0)) / anx.SaldoPartEle) * 100.00 ELSE NULL END),
	ServicioDeudaIngresosTotales = ISNULL(anx.ServicioDeudaIngresosTotales, CASE WHEN anx.SaldoIngTotAju > 0 THEN (ISNULL(pre.ServicioDeuda,0) / anx.SaldoIngTotAju) * 100.00 ELSE NULL END),
	DeudaCortoPlazoDeudaTotal = ISNULL(anx.DeudaCortoPlazoDeudaTotal, CASE WHEN anx.SaldoDeudaTotal > 0 THEN (ISNULL(anx.SaldoDeuCortPlzo,0) / (anx.SaldoDeudaTotal + ISNULL(anx.EmisionesBursatiles,0))) * 100.00 ELSE NULL END),
	IngresosTotalesGastoCorr = ISNULL(anx.IngresosTotalesGastoCorr, CASE WHEN anx.SaldoGastoCorr > 0 THEN (ISNULL(anx.SaldoIngTot,0) / anx.SaldoGastoCorr) * 100.00 ELSE NULL END),
	InvIngresosTotales = ISNULL(anx.InvIngresosTotales, CASE WHEN anx.SaldoIngTot > 0 THEN (ISNULL(anx.SaldoInversion,0) / anx.SaldoIngTot) * 100.00 ELSE NULL END),
	IngPropiosIngTotales = ISNULL(anx.IngPropiosIngTotales, CASE WHEN anx.SaldoIngTot > 0 THEN (ISNULL(anx.SaldoIngPropios,0) / anx.SaldoIngTot) * 100.00 ELSE NULL END),
	ObligContDerivadasBenef = ISNULL(anx.ObligContDerivadasBenef, CASE WHEN anx.SaldoIngTotAju > 0 THEN (ISNULL(anx.SaldoObliCont,0) / anx.SaldoIngTotAju) * 100.00 ELSE NULL END),
	BalanceOperativoPIB = ISNULL(anx.BalanceOperativoPIB, CASE WHEN anx.PIBLocal > 0 THEN ((ISNULL(anx.SaldoIngCorr,0) - ISNULL(anx.SaldoGastoCorr,0)) / anx.PIBLocal) * 100.00 ELSE NULL END),
	NivelEficienciaRec = ISNULL(anx.NivelEficienciaRec, CASE WHEN anx.SaldoGastoCorr > 0 THEN (ISNULL(anx.SaldoIngPropios,0) / anx.SaldoGastoCorr) * 100.00 ELSE NULL END)
FROM dbo.SICCMX_Anexo18_Preliminares pre
INNER JOIN dbo.SICCMX_VW_Anexo18 anx ON pre.IdPersona = anx.IdPersona;
GO
