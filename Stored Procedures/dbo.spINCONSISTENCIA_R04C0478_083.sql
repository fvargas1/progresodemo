SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_083]
AS

BEGIN

-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Tipo de Cartera (cve_tipo_cartera).

SELECT
	CodigoCredito,
	TipoCartera,
	RFC
FROM dbo.RW_VW_R04C0478_INC
WHERE TipoCartera IN (
	SELECT rep.TipoCartera
	FROM dbo.RW_VW_R04C0478_INC rep
	INNER JOIN dbo.RW_VW_R04C0478_INC rep2 ON rep.RFC = rep2.RFC AND rep.TipoCartera <> rep2.TipoCartera
	GROUP BY rep.TipoCartera, rep.RFC
);

END


GO
