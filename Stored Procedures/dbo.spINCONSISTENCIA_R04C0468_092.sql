SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_092]
AS

BEGIN

-- Si el campo no es nulo, entonces el CURP (dat_curp) debe ser un dato de 18 posiciones, donde las primeras 10 posiciones deberán ser igual a las primeras 10 posiciones del RFC (dat_rfc).

SELECT
	CodigoCredito,
	CURP,
	RFC
FROM dbo.RW_VW_R04C0468_INC
WHERE LEN(ISNULL(CURP,'')) > 0 AND (LEN(ISNULL(CURP,'')) <> 18 OR SUBSTRING(ISNULL(CURP,''),1,10) <> SUBSTRING(ISNULL(RFC,''),1,10))

END


GO
