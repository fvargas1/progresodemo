SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_Migracion]
AS

UPDATE an21
SET
 IdPersona = persona.IdPersona,
 AntSocInfCredCredito = NULLIF(f.AntSocInfCredCredito,''),
 QuitasCastRest = NULLIF(f.QuitasCastRest,''),
 PorPagoTiemInstNoBanc = CAST(NULLIF(f.PorPagoTiemInstNoBanc,'') AS DECIMAL(18,10)) * 100.00,
 PorPagEntCom60oMasAtraso = CAST(NULLIF(f.PorPagEntCom60oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 CuenCredAbierInstBancUlt12mes = NULLIF(f.CuenCredAbierInstBancUlt12mes,''),
 MonMaxCredOtorInstBanExpUDIS = NULLIF(f.MonMaxCredOtorInstBanExpUDIS,''),
 NumMesUltCredAbiUlt12Meses = NULLIF(f.NumMesUltCredAbiUlt12Meses,''),
 PorPagIntBan60oMasAtraso = CAST(NULLIF(f.PorPagIntBan60oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 PorPagIntBan1a29Atraso = CAST(NULLIF(f.PorPagIntBan1a29Atraso,'') AS DECIMAL(18,10)) * 100.00,
 PorPagIntBan90oMasAtraso = CAST(NULLIF(f.PorPagIntBan90oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 NumDiasMoraPromInstBanc = NULLIF(f.NumDiasMoraPromInstBanc,''),
 NumPagTiemIntBanUlt12Meses = NULLIF(f.NumPagTiemIntBanUlt12Meses,''),
 MonTotPagInfonavitUltBimestre = NULLIF(f.MonTotPagInfonavitUltBimestre,''),
 NumDiasAtrInfonavitUltBimestre = NULLIF(f.NumDiasAtrInfonavitUltBimestre,''),
 TasaRetLab = CAST(NULLIF(f.TasaRetLab,'') AS DECIMAL(18,10)) * 100.00,
 ProcOriAdmonCred = NULLIF(f.ProcOriAdmonCred,''),
 SinAtrasos = NULLIF(f.SinAtrasos,''),
 VentNetTotAnuales = NULLIF(f.VentNetTotAnuales,''),
 OrgDescPartidoPolitico = NULLIF(f.OrgDescPartidoPolitico,''),
 FechaInfoFinanc = NULLIF(f.FechaInfoFinanc,''),
 FechaInfoBuro = NULLIF(f.FechaInfoBuro,''),
 CalCredAgenciaCal = NULLIF(f.CalCredAgenciaCal,''),
 ExpNegativasPag = NULLIF(f.ExpNegativasPag,''),
 IndPerMorales = CASE WHEN tpoPer.Codigo IN ('320','420') THEN 0 ELSE 1 END,
 NumeroEmpleados = NULLIF(pInfo.NumeroEmpleados,''),
 LugarRadica = CASE WHEN ISNUMERIC(pInfo.Estado) = 1 AND CAST(pInfo.Estado AS INT) BETWEEN 1 AND 32 THEN 1 ELSE 2 END,
 F_AntSocInfCredCredito = NULLIF(f.F_AntSocInfCredCredito,''),
 F_QuitasCastRest = NULLIF(f.F_QuitasCastRest,''),
 F_PorPagoTiemInstNoBanc = NULLIF(f.F_PorPagoTiemInstNoBanc,''),
 F_PorPagEntCom60oMasAtraso = NULLIF(f.F_PorPagEntCom60oMasAtraso,''),
 F_CuenCredAbierInstBancUlt12mes = NULLIF(f.F_CuenCredAbierInstBancUlt12mes,''),
 F_MonMaxCredOtorInstBanExpUDIS = NULLIF(f.F_MonMaxCredOtorInstBanExpUDIS,''),
 F_NumMesUltCredAbiUlt12Meses = NULLIF(f.F_NumMesUltCredAbiUlt12Meses,''),
 F_PorPagIntBan60oMasAtraso = NULLIF(f.F_PorPagIntBan60oMasAtraso,''),
 F_PorPagIntBan1a29Atraso = NULLIF(f.F_PorPagIntBan1a29Atraso,''),
 F_PorPagIntBan90oMasAtraso = NULLIF(f.F_PorPagIntBan90oMasAtraso,''),
 F_NumDiasMoraPromInstBanc = NULLIF(f.F_NumDiasMoraPromInstBanc,''),
 F_NumPagTiemIntBanUlt12Meses = NULLIF(f.F_NumPagTiemIntBanUlt12Meses,''),
 F_MonTotPagInfonavitUltBimestre = NULLIF(f.F_MonTotPagInfonavitUltBimestre,''),
 F_NumDiasAtrInfonavitUltBimestre = NULLIF(f.F_NumDiasAtrInfonavitUltBimestre,''),
 F_TasaRetLab = NULLIF(f.F_TasaRetLab,'')
FROM dbo.SICCMX_Anexo21 an21
INNER JOIN dbo.SICCMX_Persona persona ON an21.IdPersona = persona.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON persona.IdPersona = pInfo.IdPersona
INNER JOIN dbo.FILE_Anexo21 f ON LTRIM(f.CodigoCliente) = persona.Codigo
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL AND LEN(ISNULL(f.EsGarante,'')) = 0;


INSERT INTO dbo.SICCMX_Anexo21 (
 IdPersona,
 AntSocInfCredCredito,
 QuitasCastRest,
 PorPagoTiemInstNoBanc,
 PorPagEntCom60oMasAtraso,
 CuenCredAbierInstBancUlt12mes,
 MonMaxCredOtorInstBanExpUDIS,
 NumMesUltCredAbiUlt12Meses,
 PorPagIntBan60oMasAtraso,
 PorPagIntBan1a29Atraso,
 PorPagIntBan90oMasAtraso,
 NumDiasMoraPromInstBanc,
 NumPagTiemIntBanUlt12Meses,
 MonTotPagInfonavitUltBimestre,
 NumDiasAtrInfonavitUltBimestre,
 TasaRetLab,
 ProcOriAdmonCred,
 SinAtrasos,
 VentNetTotAnuales,
 OrgDescPartidoPolitico,
 FechaInfoFinanc,
 FechaInfoBuro,
 CalCredAgenciaCal,
 ExpNegativasPag,
 IndPerMorales,
 NumeroEmpleados,
 LugarRadica,
 F_AntSocInfCredCredito,
 F_QuitasCastRest,
 F_PorPagoTiemInstNoBanc,
 F_PorPagEntCom60oMasAtraso,
 F_CuenCredAbierInstBancUlt12mes,
 F_MonMaxCredOtorInstBanExpUDIS,
 F_NumMesUltCredAbiUlt12Meses,
 F_PorPagIntBan60oMasAtraso,
 F_PorPagIntBan1a29Atraso,
 F_PorPagIntBan90oMasAtraso,
 F_NumDiasMoraPromInstBanc,
 F_NumPagTiemIntBanUlt12Meses,
 F_MonTotPagInfonavitUltBimestre,
 F_NumDiasAtrInfonavitUltBimestre,
 F_TasaRetLab
)
SELECT
 persona.IdPersona,
 NULLIF(f.AntSocInfCredCredito,''),
 NULLIF(f.QuitasCastRest,''),
 CAST(NULLIF(f.PorPagoTiemInstNoBanc,'') AS DECIMAL(18,10)) * 100.00,
 CAST(NULLIF(f.PorPagEntCom60oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.CuenCredAbierInstBancUlt12mes,''),
 NULLIF(f.MonMaxCredOtorInstBanExpUDIS,''),
 NULLIF(f.NumMesUltCredAbiUlt12Meses,''),
 CAST(NULLIF(f.PorPagIntBan60oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 CAST(NULLIF(f.PorPagIntBan1a29Atraso,'') AS DECIMAL(18,10)) * 100.00,
 CAST(NULLIF(f.PorPagIntBan90oMasAtraso,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.NumDiasMoraPromInstBanc,''),
 NULLIF(f.NumPagTiemIntBanUlt12Meses,''),
 NULLIF(f.MonTotPagInfonavitUltBimestre,''),
 NULLIF(f.NumDiasAtrInfonavitUltBimestre,''),
 CAST(NULLIF(f.TasaRetLab,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.ProcOriAdmonCred,''),
 NULLIF(f.SinAtrasos,''),
 NULLIF(f.VentNetTotAnuales,''),
 NULLIF(f.OrgDescPartidoPolitico,''),
 NULLIF(f.FechaInfoFinanc,''),
 NULLIF(f.FechaInfoBuro,''),
 NULLIF(f.CalCredAgenciaCal,''),
 NULLIF(f.ExpNegativasPag,''),
 CASE WHEN tpoPer.Codigo IN ('320','420') THEN 0 ELSE 1 END,
 NULLIF(pInfo.NumeroEmpleados,''),
 CASE WHEN ISNUMERIC(pInfo.Estado) = 1 AND CAST(pInfo.Estado AS INT) BETWEEN 1 AND 32 THEN 1 ELSE 2 END,
 NULLIF(f.F_AntSocInfCredCredito,''),
 NULLIF(f.F_QuitasCastRest,''),
 NULLIF(f.F_PorPagoTiemInstNoBanc,''),
 NULLIF(f.F_PorPagEntCom60oMasAtraso,''),
 NULLIF(f.F_CuenCredAbierInstBancUlt12mes,''),
 NULLIF(f.F_MonMaxCredOtorInstBanExpUDIS,''),
 NULLIF(f.F_NumMesUltCredAbiUlt12Meses,''),
 NULLIF(f.F_PorPagIntBan60oMasAtraso,''),
 NULLIF(f.F_PorPagIntBan1a29Atraso,''),
 NULLIF(f.F_PorPagIntBan90oMasAtraso,''),
 NULLIF(f.F_NumDiasMoraPromInstBanc,''),
 NULLIF(f.F_NumPagTiemIntBanUlt12Meses,''),
 NULLIF(f.F_MonTotPagInfonavitUltBimestre,''),
 NULLIF(f.F_NumDiasAtrInfonavitUltBimestre,''),
 NULLIF(f.F_TasaRetLab,'')
FROM dbo.FILE_Anexo21 f
INNER JOIN dbo.SICCMX_Persona persona ON LTRIM(f.CodigoCliente) = persona.Codigo
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON persona.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICCMX_Anexo21 an21 ON persona.IdPersona = an21.IdPersona
WHERE an21.IdPersona IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL AND LEN(ISNULL(f.EsGarante,'')) = 0;
GO
