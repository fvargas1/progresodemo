SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Ejecutivo]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Ejecutivo WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Ejecutivo (
	IdPeriodoHistorico,
	Codigo,
	Nombre,
	Telefono,
	Correo
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	eje.Codigo,
	eje.Nombre,
	eje.Telefono,
	eje.Correo
FROM dbo.SICCMX_Ejecutivo eje;
GO
