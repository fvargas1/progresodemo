SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Reporte_ListByGrupo]
	@Grupo VARCHAR(50)
AS
SELECT
	MAX( l.IdReporteLog ) AS IdReporteLog,
	vw.IdReporte,
	vw.GrupoReporte,
	vw.Nombre
FROM dbo.RW_VW_Reporte vw
INNER JOIN dbo.RW_VW_ReporteLog l ON vw.IdReporte = l.IdReporte
WHERE ( vw.GrupoReporte = @Grupo OR @Grupo IS NULL ) AND Activo=1
GROUP BY vw.IdReporte, vw.GrupoReporte, vw.Nombre
ORDER BY vw.Nombre ASC;
GO
