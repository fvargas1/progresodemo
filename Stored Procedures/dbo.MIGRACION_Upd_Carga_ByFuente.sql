SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Upd_Carga_ByFuente]
 @Codename VARCHAR(50),
 @Fuente VARCHAR(50)
AS
DECLARE @QRY VARCHAR(MAX);
DECLARE @Campos VARCHAR(3000);
DECLARE @Cond VARCHAR(2000);
DECLARE @Tabla VARCHAR(50);

SELECT @Tabla = ViewName FROM dbo.MIGRACION_Archivo WHERE Codename = @Codename;

SET @QRY = 'UPDATE car '
 + 'SET NumRegistrosBW = (SELECT COUNT(*) FROM dbo.' + @Tabla + ' WHERE Fuente = ''' + @Fuente + ''') '
 + 'FROM dbo.MIGRACION_Archivo arc '
 + 'INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo '
 + 'WHERE arc.Codename = ''' + @Codename + ''' AND car.Fuente =  ''' + @Fuente + '''';

EXEC (@QRY);


SELECT @Campos =
	'CAST(ISNULL(NULLIF('
	+ REPLACE(REPLACE(car.ValidacionMonto, '+', ',''''),''0'') AS DECIMAL(23,2)) + CAST(ISNULL(NULLIF('), '-', ',''''),''0'') AS DECIMAL(23,2)) - CAST(ISNULL(NULLIF(')
	+ ',''''),''0'') AS DECIMAL(23,2))'
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo
WHERE arc.Codename = @Codename AND car.Fuente = @Fuente AND LEN(car.ValidacionMonto) > 0;

SELECT @Cond =
	'ISNUMERIC('
	+ REPLACE(REPLACE(car.ValidacionMonto, '+', ') = 1 AND ISNUMERIC('), '-', ') = 1 AND ISNUMERIC(')
	+ ') = 1'
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo
WHERE arc.Codename = @Codename AND car.Fuente = @Fuente AND LEN(car.ValidacionMonto) > 0;

SET @QRY = 'UPDATE car '
 + 'SET MontoCCBW = (SELECT SUM(' + @Campos + ') FROM dbo.' + @Tabla + ' WHERE Fuente = ''' + @Fuente + ''' AND ' + @Cond + ') '
 + 'FROM dbo.MIGRACION_Archivo arc '
 + 'INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo '
 + 'WHERE arc.Codename = ''' + @Codename + ''' AND car.Fuente =  ''' + @Fuente + '''';

EXEC (@QRY);


INSERT INTO dbo.MIGRACION_Carga_Errores (Codename, TipoError, Descripcion)
SELECT
	arc.Codename,
	'CC - ' + car.FileCarga,
	'Registros Banco: ' + CAST(car.NumRegistros AS VARCHAR)
	+ ' | Registros BW: ' + CAST(car.NumRegistrosBW AS VARCHAR)
	+ ' | Monto CC Banco: ' + CONVERT(VARCHAR,CAST(car.MontoCC AS MONEY),-1)
	+ ' | Monto CC BW: ' + CONVERT(VARCHAR,CAST(car.MontoCCBW AS MONEY),-1)
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Archivos_Carga car ON arc.IdArchivo = car.IdArchivo
WHERE arc.Codename = @Codename
	AND car.Fuente = @Fuente
	AND ISNULL(car.NumRegistros,0) > 0
	AND ISNULL(car.MontoCC,0) > 0
	AND (ISNULL(car.NumRegistros,0) <> ISNULL(car.NumRegistrosBW,0) OR ISNULL(car.MontoCC,0) <> ISNULL(car.MontoCCBW,0));
GO
