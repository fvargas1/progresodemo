SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_092_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el campo no es nulo, entonces el CURP (dat_curp) debe ser un dato de 18 posiciones, donde las primeras 10 posiciones deberán ser igual a las primeras 10 posiciones del RFC (dat_rfc).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE LEN(ISNULL(CURP,'')) > 0 AND (LEN(ISNULL(CURP,'')) <> 18 OR SUBSTRING(ISNULL(CURP,''),1,10) <> SUBSTRING(ISNULL(RFC,''),1,10))

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
