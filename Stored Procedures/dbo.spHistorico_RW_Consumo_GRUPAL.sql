SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_GRUPAL]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_GRUPAL);

DELETE FROM Historico.RW_Consumo_GRUPAL WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_GRUPAL (
	IdPeriodoHistorico, IdProducto, FolioCredito, FolioCliente, Reestructura, FechaInicioCredito, FechaTeoricaVencimientoCredito, PeriodosFacturacionCredito,
	PlazoTotal, ImporteOriginalCredito, TasaInteresAnualCredito, MecanismoPago, CodigoPostal, FechaCorte, PlazoRemanente, SaldoCredito, MontoExigible,
	PagoRealizado, PromedioPorcentajePagosRealizados, DiasAtraso, NumeroAtrasos, IdGrupo, NumeroIntegrantesGrupo, PromedioCiclosGrupo, TipoGarantia,
	ImporteGarantia, RegistroUnicoGarantiasMobiliarias, ProbabilidadIncumplimiento, SeveridadPerdidaParteNoCubierta, SeveridadPerdidaParteCubierta,
	ExposicionIncumplimientoParteNoCubierta, ExposicionIncumplimientoParteCubierta, MontoReservasConstituirCredito, ClasificacionCredito,
	QuitasCondonacionesBonificacionesDescuentos, RelacionAcreditadoInstitucion, ClaveConsultaSociedadInformacionCrediticia,
	MetodologiaUtilizadaCalculoReservas, ProbabilidadIncumplimientoInterna, SeveridadPerdidaInterna, ExposicionIncumplimientoInterna,
	MontoReservasConstituirCreditoInterno, CAT
)
SELECT
	@IdPeriodoHistorico,
	IdProducto,
	FolioCredito,
	FolioCliente,
	Reestructura,
	FechaInicioCredito,
	FechaTeoricaVencimientoCredito,
	PeriodosFacturacionCredito,
	PlazoTotal,
	ImporteOriginalCredito,
	TasaInteresAnualCredito,
	MecanismoPago,
	CodigoPostal,
	FechaCorte,
	PlazoRemanente,
	SaldoCredito,
	MontoExigible,
	PagoRealizado,
	PromedioPorcentajePagosRealizados,
	DiasAtraso,
	NumeroAtrasos,
	IdGrupo,
	NumeroIntegrantesGrupo,
	PromedioCiclosGrupo,
	TipoGarantia,
	ImporteGarantia,
	RegistroUnicoGarantiasMobiliarias,
	ProbabilidadIncumplimiento,
	SeveridadPerdidaParteNoCubierta,
	SeveridadPerdidaParteCubierta,
	ExposicionIncumplimientoParteNoCubierta,
	ExposicionIncumplimientoParteCubierta,
	MontoReservasConstituirCredito,
	ClasificacionCredito,
	QuitasCondonacionesBonificacionesDescuentos,
	RelacionAcreditadoInstitucion,
	ClaveConsultaSociedadInformacionCrediticia,
	MetodologiaUtilizadaCalculoReservas,
	ProbabilidadIncumplimientoInterna,
	SeveridadPerdidaInterna,
	ExposicionIncumplimientoInterna,
	MontoReservasConstituirCreditoInterno,
	CAT
FROM dbo.RW_Consumo_GRUPAL
WHERE IdReporteLog = @IdReporteLog;
GO
