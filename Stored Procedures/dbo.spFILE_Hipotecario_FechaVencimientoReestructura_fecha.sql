SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_FechaVencimientoReestructura_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_FechaVencimientoReestructura_fecha';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorFormato = 1
WHERE LEN(FechaVencimientoReestructura)>0 AND ISDATE(FechaVencimientoReestructura) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'FechaVencimientoReestructura',
 FechaVencimientoReestructura,
 1,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE LEN(FechaVencimientoReestructura)>0 AND ISDATE(FechaVencimientoReestructura) = 0;
GO
