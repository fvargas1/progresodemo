SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_016]
AS

BEGIN

-- El campo SEVERIDAD PERDIDA METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPInterna AS SP_MetodologiaInterna
FROM dbo.RW_R04C0474
WHERE CAST(ISNULL(NULLIF(SPInterna,''),'-1') AS DECIMAL(10,6)) < 0;

END
GO
