SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_List]
AS
SELECT
 vw.IdValidacion,
 vw.Description,
 vw.Codename,
 vw.Position,
 vw.CategoryName,
 vw.FieldName,
 vw.Activo,
 CASE WHEN vw.Activo = 1 THEN 'on' ELSE 'blank' END AS ActivoImg
FROM dbo.MIGRACION_VW_Validacion vw
ORDER BY vw.CategoryName, vw.Position;
GO
