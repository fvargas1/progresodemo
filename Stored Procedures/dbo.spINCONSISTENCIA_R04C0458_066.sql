SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_066]
AS
BEGIN
-- Si la Tasa de Interés (cve_tasa) es IGUAL a 600 validar que el Ajuste de la tasa (dat_ajuste_tasa_referencia) sea IGUAL 0.

SELECT
 CodigoCredito,
 REPLACE(NombrePersona, ',', '') AS NombreDeudor,
 TasaInteres,
 DifTasaRef
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TasaInteres,'') = '600' AND CAST(ISNULL(NULLIF(REPLACE(REPLACE(REPLACE(DifTasaRef,'*',''),'-',''),'+',''),''),'-1') AS DECIMAL(14,6)) <> 0
 AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139');

END
GO
