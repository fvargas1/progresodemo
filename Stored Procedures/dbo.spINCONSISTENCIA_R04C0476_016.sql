SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0476_016]
AS

BEGIN

-- El porcentaje No cubierto debe encontrarse entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PrctExpuesto
FROM dbo.RW_VW_R04C0476_INC
WHERE CAST(ISNULL(NULLIF(PrctExpuesto,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
