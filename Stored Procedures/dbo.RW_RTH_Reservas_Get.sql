SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_RTH_Reservas_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	Veces,
	FactorPorPago,
	PorPago,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_VW_RTH_Reservas
ORDER BY Codigo;
GO
