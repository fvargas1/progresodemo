SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Periodo_SetActivo]
	@IdPeriodo INT
AS
DECLARE @next_fecha DATETIME;

SELECT @next_fecha = dbo.fnPeriodo_NextDate(YEAR(p.Fecha),MONTH(p.Fecha))
FROM dbo.SICC_Periodo p
WHERE p.IdPeriodo = @IdPeriodo;

DECLARE @trimestral BIT;
SELECT @trimestral = (CASE WHEN (MONTH(@next_fecha) % 3) = 0 THEN 1 ELSE 0 END);


IF NOT EXISTS (SELECT * FROM dbo.SICC_Periodo WHERE Fecha = @next_fecha)
BEGIN
 INSERT INTO dbo.SICC_Periodo (Fecha, Trimestral, Activo)
 VALUES (@next_fecha, @trimestral, 0)
END

UPDATE dbo.SICC_Periodo SET Activo = 0 WHERE Activo = 1;
UPDATE dbo.SICC_Periodo SET Activo = 1 WHERE IdPeriodo = @IdPeriodo;


--EXEC dbo.FILE_Archivos_ClearData;
GO
