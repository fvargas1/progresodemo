SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_A22_GP_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	Nombre,
	TipoGarantia,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	DiasMoraInstFinBanc_V,
	DiasMoraInstFinBanc_P,
	PrctPagoInstFinBanc_V,
	PrctPagoInstFinBanc_P,
	NumInstRep_V,
	NumInstRep_P,
	PrctPagoInstFinNoBanc_V,
	PrctPagoInstFinNoBanc_P,
	TotalPagosInfonavit_V,
	TotalPagosInfonavit_P,
	DiasAtrInfonavit_V,
	DiasAtrInfonavit_P,
	TasaRetLab_V,
	TasaRetLab_P,
	RotActTotal_V,
	RotActTotal_P,
	RotCapTrabajo_V,
	RotCapTrabajo_P,
	ROE_V,
	ROE_P,
	EstEconomica_V,
	EstEconomica_P,
	IntCarComp_V,
	IntCarComp_P,
	Proveedores_V,
	Proveedores_P,
	Clientes_V,
	Clientes_P,
	EstFinAudit_V,
	EstFinAudit_P,
	NumAgenCalif_V,
	NumAgenCalif_P,
	IndepConAdmon_V,
	IndepConAdmon_P,
	EstructOrg_V,
	EstructOrg_P,
	CompAccionaria_V,
	CompAccionaria_P,
	LiquidezOper_V,
	LiquidezOper_P,
	UAFIRGastosFin_V,
	UAFIRGastosFin_P
FROM Historico.RW_Analitico_A22_GP
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
