SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_Select]
	@IdInconsistencia INT
AS
SELECT
	vw.IdInconsistencia,
	vw.Nombre,
	vw.Descripcion,
	vw.CodenameCount,
	vw.Codename,
	vw.IdCategoria,
	vw.CategoriaNombre + ISNULL(' -  ' + rep.Descripcion,'') AS CategoriaNombre,
	vw.Activo,
	vw.Regulatorio,
	vw.FechaCreacion,
	vw.FechaActualizacion,
	vw.Username
FROM dbo.MIGRACION_VW_Inconsistencia vw
INNER JOIN dbo.RW_Reporte rep ON vw.CategoriaNombre = ISNULL(rep.GrupoReporte,'') + ISNULL(rep.Nombre,'')
WHERE vw.IdInconsistencia = @IdInconsistencia;
GO
