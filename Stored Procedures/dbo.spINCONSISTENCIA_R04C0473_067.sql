SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_067]
AS

BEGIN

-- Si la tasa es Fija o Tasa Cero entonces la Frec Rev Tasa debe ser igual a 0

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TasaInteres,
	FrecuenciaRevisionTasa
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TasaInteres,'') = '600' AND ISNULL(FrecuenciaRevisionTasa,'') <> '0';

END


GO
