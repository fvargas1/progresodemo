SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_Group_Group_ListChildren]
	@IdGroupParent BAJAWARE_utID
AS
SELECT g.IdGroup, g.Active, g.Description, g.IdGroupType
FROM dbo.BAJAWARE_Rel_Group_Group rel
LEFT OUTER JOIN dbo.BAJAWARE_Group g ON rel.IdGroupChild = g.IdGroup
WHERE IdGroupParent = @IdGroupParent;
GO
