SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo18_BalanceOperativoPIB_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo18_BalanceOperativoPIB_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo18
SET errorFormato = 1
WHERE LEN(ISNULL(BalanceOperativoPIB,'')) > 0 AND (ISNUMERIC(BalanceOperativoPIB) = 0 OR BalanceOperativoPIB LIKE '%[^0-9 .-]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'BalanceOperativoPIB',
 BalanceOperativoPIB,
 1,
 @Detalle
FROM dbo.FILE_Anexo18
WHERE LEN(ISNULL(BalanceOperativoPIB,'')) > 0 AND (ISNUMERIC(BalanceOperativoPIB) = 0 OR BalanceOperativoPIB LIKE '%[^0-9 .-]%');
GO
