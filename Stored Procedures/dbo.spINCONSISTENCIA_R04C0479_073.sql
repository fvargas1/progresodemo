SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_073]
AS

BEGIN

-- Validar que los Intereses Resultantes del Aplicar la Tasa al Saldo Base (dat_monto_interes) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoInteresAplicar
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(MontoInteresAplicar,''),'-1') AS DECIMAL) < 0;

END


GO
