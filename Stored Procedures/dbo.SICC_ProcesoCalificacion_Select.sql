SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_Select]
	@IdProcesoCalificacion int
AS
SELECT
	vw.IdProcesoCalificacion,
	vw.Nombre,
	vw.Descripcion,
	vw.Codename,
	vw.IdMetodologia,
	vw.MetodologiaNombre,
	vw.Position,
	vw.Activo,
	vw.IdGrupoProceso,
	vw.GrupoProcesoNombre,
	vw.Params
FROM dbo.SICC_VW_ProcesoCalificacion vw
WHERE vw.IdProcesoCalificacion = @IdProcesoCalificacion;
GO
