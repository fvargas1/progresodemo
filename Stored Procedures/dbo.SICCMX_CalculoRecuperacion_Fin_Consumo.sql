SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoRecuperacion_Fin_Consumo]
AS
-- EL MONTO DE RECUPERACION DE LAS GARANTIAS FINANCIERAS ES IGUAL AL MONTO DE COBERTURA
UPDATE dbo.SICCMX_Garantia_Canasta_Consumo
SET MontoRecuperacion = ISNULL(MontoCobAjust,0)
WHERE IdTipoGarantia IS NULL AND EsDescubierto IS NULL;
GO
