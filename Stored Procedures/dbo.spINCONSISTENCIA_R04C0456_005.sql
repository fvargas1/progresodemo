SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_005]
AS
BEGIN
-- El porcentaje cubierto debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	PrctGarPersonales
FROM dbo.RW_VW_R04C0456_INC
WHERE ISNULL(PrctGarPersonales,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END
GO
