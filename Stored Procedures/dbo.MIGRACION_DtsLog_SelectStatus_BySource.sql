SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_DtsLog_SelectStatus_BySource]    
 @source NVARCHAR(250)    
AS    
DECLARE @executionid VARCHAR(250);    
DECLARE @starttime DATETIME;    
DECLARE @endtime DATETIME;    
DECLARE @message VARCHAR(250);    
DECLARE @rutaCarga VARCHAR(300);    
DECLARE @existFile INT;    
DECLARE @rutaWinTemp VARCHAR(300);    
DECLARE @sqlSt NVARCHAR(300);    
DECLARE @CountErrors INT;    
DECLARE @NumRegistros INT;    
    
    
SELECT @rutaWinTemp = Value + @source + '_err.txt' FROM dbo.BAJAWARE_Config WHERE Codename='WinTemp';    
    
EXEC master.dbo.xp_fileexist @rutaWinTemp, @existFile OUTPUT;    
    
IF @existFile = 1 AND NOT EXISTS(SELECT * FROM dbo.MIGRACION_VW_DtsLog WHERE source=@source AND event='PackageEnd' AND LEN(message)=0)    
BEGIN    
SELECT TOP 1 @executionid = vw.executionid FROM dbo.MIGRACION_VW_DtsLog vw WHERE vw.event = 'PackageStart' AND vw.source = @source AND LEN(vw.message)=0 ORDER BY vw.starttime DESC;    
    
INSERT INTO dbo.SysSSISlog (event, computer, operator, source, sourceid, executionid, starttime, endtime, datacode , databytes, message)    
VALUES ('OnError', 'SERVER', 'BAJAWARE', @source, @executionid, @executionid, GETDATE(), GETDATE(), 0, 0x, 'Error en ' + @source + '_err.txt');    
INSERT INTO dbo.SysSSISlog (event, computer, operator, source, sourceid, executionid, starttime, endtime, datacode , databytes, message)    
VALUES ('PackageEnd', 'SERVER', 'BAJAWARE', @source, @executionid, @executionid, GETDATE(), GETDATE(), 0, 0x, '');    
END    
    
    
SELECT TOP 1    
 @executionid = vw.executionid,    
 @starttime = vw.starttime     
FROM dbo.MIGRACION_VW_DtsLog vw    
WHERE vw.event = 'PackageStart' AND vw.source = @source    
ORDER BY vw.starttime DESC;    
    
    
SELECT TOP 1    
 @endtime = vw.endtime    
FROM dbo.MIGRACION_VW_DtsLog vw    
WHERE vw.executionid = @executionid AND vw.event = 'PackageEnd'    
ORDER BY vw.executionid, vw.starttime, vw.endtime, vw.id;    
    
    
SELECT TOP 1 @message = 'Errores de Ejecucion. Revisar log de Job y/o DTS. '    
FROM dbo.MIGRACION_VW_DtsLog vw    
WHERE vw.executionid = @executionid AND vw.event = 'OnError'    
ORDER BY vw.executionid, vw.starttime, vw.endtime, vw.id;    
    
    
SELECT @CountErrors=COUNT(Codename) FROM dbo.MIGRACION_Carga_Errores WHERE Codename=@source;    
    
    
SELECT @NumRegistros = ISNULL(NumRegistros,0)    
FROM dbo.MIGRACION_Archivo    
WHERE JobCodename=@source; --JG/ER 03/JUL/15 
    
    
SELECT    
 @source AS job,    
 @starttime AS starttime,    
 @endtime AS endtime,    
 CASE WHEN @message IS NULL AND @CountErrors=0 THEN 0 ELSE 1 END AS error,    
 CASE    
 WHEN @message IS NULL AND @starttime IS NOT NULL AND @endtime IS NULL    
 THEN 'Procesando.'    
 WHEN @message IS NULL AND @starttime IS NOT NULL AND @endtime IS NOT NULL    
 THEN 'Se Ejecuto con Exito.'    
 WHEN @message IS NULL AND @starttime IS NULL AND @endtime IS NULL    
 THEN 'Proceso sin ejecutar.'    
 ELSE @message    
 END AS mensaje,    
 ISNULL(@NumRegistros,0) AS records --JG/ER 03/JUL/15

GO
