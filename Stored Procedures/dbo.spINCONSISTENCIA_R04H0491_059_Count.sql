SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_059_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "AJUSTE EN LA TASA REFERENCIA" deberá iniciar con alguno de los siguientes caracteres: "*", "+", "–".

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE AjusteTasaRef <> '0' AND SUBSTRING(AjusteTasaRef,1,1) NOT IN ('*','+','–');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
