SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_025]
AS
BEGIN
-- Id línea crédito de metodología debe de ser de 29 posiciones.

SELECT DISTINCT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0459_INC
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 29;

END


GO
