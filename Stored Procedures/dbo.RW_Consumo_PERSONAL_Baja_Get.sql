SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_PERSONAL_Baja_Get]
	@IdReporteLog BIGINT
AS
SELECT
	FolioCredito,
	TipoCredito,
	FechaBajaCredito,
	TipoBajaCredito,
	QuitasCondonacionesBonificacionesDescuentos
FROM dbo.RW_VW_Consumo_PERSONAL_Baja
ORDER BY FolioCredito;
GO
