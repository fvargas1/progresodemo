SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CC_MIG_Migracion]
AS
SELECT
	arc.Codename,
	arc.Nombre,
	ISNULL(sts.Total,0) AS Total,
	ISNULL(sts.Actualizados,0) AS Actualizados,
	ISNULL(sts.Nuevos,0) AS Nuevos,
	ISNULL(sts.Bajas,0) AS Bajas
FROM dbo.MIGRACION_Archivo arc
LEFT OUTER JOIN dbo.MIGRACION_Archivos_Stats sts ON arc.IdArchivo = sts.IdArchivo
WHERE arc.Activo=1
ORDER BY arc.Position;
GO
