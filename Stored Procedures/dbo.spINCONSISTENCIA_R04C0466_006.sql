SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_006]
AS

BEGIN

-- El porcentaje cubierto ecpm debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctECPM
FROM dbo.RW_R04C0466
WHERE ISNULL(PrctECPM,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END

GO
