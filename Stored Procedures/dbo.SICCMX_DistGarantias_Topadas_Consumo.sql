SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Topadas_Consumo]
AS

UPDATE cg
SET PorUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN con.E / cgTotal.Total ELSE 0 END,
 MontoUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN gar.ValorGarantiaValorizado * (con.E / cgTotal.Total) ELSE 0 END
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables con ON cg.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
INNER JOIN (
 SELECT cg2.IdGarantia, CAST(SUM(ISNULL(vwc.E, 0)) AS DECIMAL(23,2)) AS Total
 FROM dbo.SICCMX_ConsumoGarantia cg2
 INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables vwc ON cg2.IdConsumo = vwc.IdConsumo
 GROUP BY cg2.IdGarantia
) AS cgTotal ON gar.IdGarantia = cgTotal.IdGarantia;


CREATE TABLE #TempFin (
 IdCredito BIGINT NULL,
 IdGarantia BIGINT NULL,
 MontoGarantia DECIMAL(25,4) NULL,
 MontoGarantiaAjust DECIMAL(25,4) NULL,
 Hc DECIMAL(18,10) NULL,
 Hfx DECIMAL(18,10) NULL
);


CREATE TABLE #Temp (
 IdCredito BIGINT NULL,
 IdTipoGarantia INT NULL,
 MontoCredito DECIMAL(25,4) NULL,
 MontoGarantia DECIMAL(25,4) NULL,
 MontoGarantiaAjust DECIMAL(25,4) NULL,
 PrctGarantia DECIMAL(18,10) NULL,
 Orden INT NULL
);


INSERT INTO #TempFin (
 IdCredito,
 IdGarantia,
 MontoGarantia,
 MontoGarantiaAjust,
 Hc,
 Hfx
)
SELECT
 cg.IdConsumo,
 cg.IdGarantia,
 gar.ValorGarantiaValorizado,
 cg.MontoUsadoGarantia,
 gar.Hc,
 CASE WHEN con.IdMoneda <> gar.IdMoneda THEN 0.08 ELSE 0 END
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON con.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
WHERE gar.Aplica=1 AND gar.IdClasificacionNvaMet=1;


-- AJUSTAMOS EL MONTO DE LAS GARANTIAS FINANCIERAS
UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust * (1 - Hc - Hfx)
FROM #TempFin t;

-- INSERTAMOS GARANTIAS FINANCIERAS
INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 t.IdCredito,
 NULL,
 con.E,
 SUM(t.MontoGarantia),
 SUM(t.MontoGarantiaAjust),
 1,
 1
FROM #TempFin t
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables con ON con.IdConsumo = t.IdCredito
GROUP BY t.IdCredito, con.E;


INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 cg.IdConsumo,
 gar.IdTipoGarantia,
 con.E,
 SUM(cg.MontoUsadoGarantia),
 SUM(cg.MontoUsadoGarantia),
 1,
 gar.SortOrder
FROM dbo.SICCMX_Consumo_Reservas_Variables con
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON con.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
WHERE gar.Aplica=1 AND ISNULL(gar.IdClasificacionNvaMet,0) <> 1
GROUP BY cg.IdConsumo, gar.IdTipoGarantia, con.E, gar.SortOrder;


-- INSERTAMOS INFORMACION DE AVALES
INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 ca.IdConsumo,
 tg.IdTipoGarantia,
 con.E,
 SUM(ca.Monto),
 SUM(ca.Monto),
 1,
 tg.SortOrder
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN dbo.SICC_TipoGarantia tg ON 1=1 AND tg.Codigo='GP'
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables con ON ca.IdConsumo = con.IdConsumo
WHERE ca.Aplica = 1
GROUP BY ca.IdConsumo, tg.IdTipoGarantia, con.E, tg.SortOrder;



-- AJUSTAMOS EL MONTO DE LAS GARANTIAS NO FINANCIERAS
UPDATE t
SET MontoGarantiaAjust = MontoGarantia / (sp.NivelMaximo/100)
FROM #Temp t
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia;

UPDATE t
SET
 MontoGarantiaAjust = 0,
 PrctGarantia = 0
FROM #Temp t
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia
WHERE (t.MontoGarantia / t.MontoCredito) * 100.00 < sp.NivelMinimo
AND ISNULL(t.MontoCredito,0)>0;


-- CALCULAMOS EL MONTO AJUSTADO DE LA GARANTIA
UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust + (t.MontoCredito - (SELECT SUM(t2.MontoGarantiaAjust) FROM #Temp t2 WHERE t.IdCredito = t2.IdCredito AND t2.Orden <= t.Orden))
FROM #Temp t
WHERE t.MontoCredito - (SELECT SUM(t2.MontoGarantiaAjust) FROM #Temp t2 WHERE t.IdCredito = t2.IdCredito AND t2.Orden <= t.Orden) < 0;

UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust * (sp.NivelMaximo/100)
FROM #Temp t INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia;

UPDATE #Temp SET MontoGarantiaAjust = 0 WHERE MontoGarantiaAjust < 0;


UPDATE #Temp SET PrctGarantia = MontoGarantiaAjust / MontoGarantia WHERE ISNULL(MontoGarantia,0)>0;


-- ACTUALIZAMOS EL PORCENTAJE A UTILIZAR DE LA GARANTIA
UPDATE cg
SET PorUsadoGarantia = PorUsadoGarantia * t.PrctGarantia
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia AND g.Aplica=1
INNER JOIN #Temp t ON cg.IdConsumo = t.IdCredito AND g.IdTipoGarantia = t.IdTipoGarantia;

UPDATE cg
SET MontoUsadoGarantia = g.ValorGarantiaValorizado * cg.PorUsadoGarantia
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables c ON cg.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia
WHERE c.E > 0;


-- ACTUALIZAMOS EL PORCENTAJE Y MONTO A UTILIZAR DEL AVAL
UPDATE ca
SET Porcentaje = Porcentaje * t.PrctGarantia
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN #Temp t ON ca.IdConsumo = t.IdCredito
INNER JOIN dbo.SICC_TipoGarantia tg ON t.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo='GP'
WHERE ca.Aplica = 1;


DROP TABLE #TempFin;
DROP TABLE #Temp;
GO
