SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_022]
AS
BEGIN
-- Si el Saldo de Gastos Corrientes (dat_saldo_gasto_corriente) es > 109 y <= 120.5,
-- entonces el Puntaje Ingresos Totales a Gasto Corriente (cve_puntaje_ingreso_gasto) debe ser = 91

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoIngTotales,
	P_IngTotGastoCorriente AS Puntos_SdoIngTotales
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngTotales,''),'0') AS DECIMAL(18,6)) BETWEEN 109.000001 AND 120.5 AND ISNULL(P_IngTotGastoCorriente,'') <> '91';

END
GO
