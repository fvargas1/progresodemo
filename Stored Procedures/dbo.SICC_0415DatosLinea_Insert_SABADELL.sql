SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0415DatosLinea_Insert_SABADELL]
AS
-- COMERCIAL
INSERT R04.[0415Datos] (
 Codigo,
 TipoProducto,
 Moneda,
 SituacionCredito,
 SaldoPromedio,
 SaldoPromedioUSD,
 InteresMes,
 InteresMesUSD,
 ComisionMes,
 ComisionMesUSD
)
SELECT
	lin.Codigo,
	cg.TipoProducto,
	cg.Moneda,
	cg.Situacion,
	0,
	0,
	0,
	0,
	ISNULL(lad.ComisionValorizado,0),
	ISNULL(lad.ComisionValorizadoUSD,0)
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_VW_LineaCreditoAdicional lad ON lin.IdLineaCredito = lad.IdLineaCredito
CROSS APPLY (
	SELECT TOP 1 lin.IdLineaCredito, sic.Codigo AS Situacion, tp.Codigo AS TipoProducto, mon.ClasSerieA AS Moneda
	FROM dbo.SICCMX_VW_Credito_NMC cre
	INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
	INNER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
	LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = inf.IdTipoProductoSerie4
	LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = cre.IdSituacionCredito
	WHERE cre.IdLineaCredito = lin.IdLineaCredito
	ORDER BY cre.MontoValorizado DESC
) AS cg
WHERE lin.ComisionDelMes > 0;
GO
