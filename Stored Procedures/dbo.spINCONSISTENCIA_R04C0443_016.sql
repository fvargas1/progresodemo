SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_016]
AS

BEGIN

-- Las calificaciones válidas son: A1, A2, B1, B2, B3, C1, C2, D, E, EX ó 0.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.CalificacionCreditoCubiertaCNBV
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cat ON ISNULL(r.CalificacionCreditoCubiertaCNBV,'') = cat.Codigo
WHERE cat.IdCalificacion IS NULL;

END
GO
