SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Reserva_PP_Credito]  
AS  
-- CALCULANDO RESERVA PRIMERAS PERDIDAS POR CREDITO  
UPDATE hrv  
SET ReservaPP_Credito = hg.MontoUsadoGarantia * 0.45 * ISNULL(gar.PIGarante, 0.01111111)  
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv  
INNER JOIN dbo.SICCMX_HipotecarioGarantia hg ON hrv.IdHipotecario = hg.IdHipotecario  
INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario gar ON hg.IdGarantia = gar.IdGarantia  
WHERE gar.IdTipoGarantiaCodigo = 'NMC-04-C';

-- ACTUALIZAMOS RESERVAS ORIGINALES
UPDATE dbo.SICCMX_Hipotecario_Reservas_Variables SET ReservaOriginal = Reserva;
UPDATE dbo.SICCMX_Hipotecario_Reservas_Variables SET Reserva = Reserva + ISNULL(ReservaPP_Credito,0);
GO
