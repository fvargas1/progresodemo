SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_005]
AS
BEGIN
-- El ID de Crédito Metodología CNBV debe de ser de 29 Posiciones

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0453_INC
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 29;

END
GO
