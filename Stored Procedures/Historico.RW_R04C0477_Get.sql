SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0477_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NombrePersona,
	TipoBaja,
	SaldoInicial,
	ResponsabilidadInicial,
	MontoPagado,
	MontoQuitCastReest,
	MontoBonificaciones
FROM Historico.RW_R04C0477
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoCredito
GO
