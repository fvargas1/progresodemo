SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0492_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'H-0492_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04H0492_2016;

INSERT INTO dbo.RW_R04H0492_2016 (
	IdReporteLog, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, DenominacionCredito, SaldoPrincipalInicio,
	TasaInteres, ComisionesCobradasTasa, ComisionesCobradasMonto, MontoPagoExigible, MontoPagoRealizado, MontoQuitas, MontoCondonacion, MontoBonificacion,
	MontoDescuentos, SaldoPrincipalFinal, ResponsabilidadTotal, FechaUltimoPago, SituacionCredito, InteresesDevengados, SaldoCalculoInteres, DiasCalculoInteres,
	TipoRegimen, [PI], SP, DiasAtraso, ATR, PorCLTV, ValorViviendaAct, FactorActVivienda, TipoActualizacion, NumeroReAvaluo, MAXATR, PorVPAGO, PorRET, SUBCV,
	ConvenioJudicial, PorCobPaMed, PorCobPP, EntidadCobertura, ReservasSaldoFinal, ReservasConstDesc, PerdidaEsperada, ReservaPreventiva, PIInterna, SPInterna,
	EInterna, PerdidaEsperadaInterna, ReservasInterna, ReservasAdicionales, ReservasAdicConstDesc, GrupoRiesgoME, PonderadorRiesgoME, ExposicionNetaRes, ReqCapME,
	SP_ReqCap_MI, PI_ReqCap_MI, EI_ReqCap_MI, Ponderador_ReqCap_MI, ReqCap_MI
)
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'492',
	ROW_NUMBER() OVER ( ORDER BY hip.Codigo ASC ) AS NumeroSecuencia,
	hip.Codigo AS CodigoCredito,
	cnbv.CNBV AS CodigoCreditoCNBV,
	REPLACE(info.NumeroAvaluo, '-', '') AS NumeroAvaluo,
	mon.CodigoCNBV_Hipo AS DenominacionCredito,
	info.SaldoInicialCapital AS SaldoPrincipalInicio,
	info.TasaInteres AS TasaInteres,
	info.TasaComisiones AS ComisionesCobradasTasa,
	info.ComisionesCobradas AS ComisionesCobradasMonto,
	info.MontoExigible AS MontoPagoExigible,
	info.MontoPagadoCliente AS MontoPagoRealizado,
	info.MontoQuitas AS MontoQuitas,
	info.MontoCondonaciones AS MontoCondonacion,
	info.MontoBonificacion AS MontoBonificacion,
	info.MontoDescuentos AS MontoDescuentos,
	info.SaldoFinalCapital AS SaldoPrincipalFinal,
	info.ResponsabilidadTotal AS ResponsabilidadTotal,
	CASE WHEN info.FechaUltimoPagoCliente IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaUltimoPagoCliente,102),'.',''),1,6) END AS FechaUltimoPago,
	sit.CodigoCNBV AS SituacionCredito,
	hAdi.InteresesDevengados AS InteresesDevengados,
	info.SaldoBaseCalculoInteres AS SaldoCalculoInteres,
	info.NumeroDiasCalculoIntereses AS DiasCalculoInteres,
	reg.CodigoCNBV AS TipoRegimen,
	hrv.[PI] * 100 AS [PI],
	hrv.SP * 100 AS SP,
	pre.DiasAtraso AS DiasAtraso,
	pre.ATR AS ATR,
	pre.PorCLTVi * 100 AS PorCLTV,
	pre.ValorVivienda AS ValorViviendaAct,
	info.FactorDeVivienda AS FactorActVivienda,
	act.CodigoCNBV AS TipoActualizacion,
	info.NumeroReAvaluoInmueble AS NumeroReAvaluo,
	pre.MAXATR AS MAXATR,
	pre.PorPago AS PorVPAGO,
	info.PromedioDeRet * 100 AS PorRET,
	pre.SUBCVi AS SUBCV,
	conv.CodigoCNBV AS ConvenioJudicial,
	pre.PorCOBPAMED * 100 AS PorCobPaMed,
	pre.PorCOBPP * 100 AS PorCobPP,
	ent.CodigoCNBV AS EntidadCobertura,
	info.ReservasSaldoFinal AS ReservasSaldoFinal,
	res.ReservaCalConsDesc AS ReservasConstDesc,
	hrv.PerdidaEsperada * 100 AS PerdidaEsperada,
	hrv.Reserva AS ReservaPreventiva,
	'' AS PIInterna,
	'' AS SPInterna,
	'' AS EInterna,
	'' AS PerdidaEsperadaInterna,
	'' AS ReservasInterna,
	info.ReservasAdicionales AS ReservasAdicionales,
	res.ReservaAdicConsDesc AS ReservasAdicConstDesc,
	grh.CodigoCNBV AS GrupoRiesgoME,
	info.PonderadorRiesgoEstandar * 100 AS PonderadorRiesgoME,
	info.ExposicionNetaReservas AS ExposicionNetaRes,
	info.RequerimientoCapital AS ReqCapME,
	'' AS SP_ReqCap_MI,
	'' AS PI_ReqCap_MI,
	'' AS EI_ReqCap_MI,
	'' AS Ponderador_ReqCap_MI,
	'' AS ReqCap_MI
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON info.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICCMX_Reservas_Variacion_Hipotecario res ON pre.IdHipotecario = res.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_HipotecarioAdicional hAdi ON pre.IdHipotecario = hAdi.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_VW_HipotecarioCNBV cnbv ON hip.IdHipotecario = cnbv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sit ON info.IdSituacionCredito = sit.IdSituacionHipotecario
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial conv ON pre.IdConvenio = conv.IdConvenioJudicial
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON hip.IdEntidadCobertura = ent.IdEntidadCobertura
LEFT OUTER JOIN dbo.SICC_TipoRegimen reg ON info.IdTipoRegimenCredito = reg.IdTipoRegimen
LEFT OUTER JOIN dbo.SICC_TipoActualizacion act ON info.IdTipoActualizacion = act.IdTipoActualizacion
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgoHip grh ON info.IdGrupoRiesgoEstandar = grh.IdRCGrupoRiesgoHip;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04H0492_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
