SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_055]
AS
BEGIN
-- Validar que el Saldo del Principal al Inicio del Periodo (dat_saldo_princ_inicio) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	SaldoInicial
FROM dbo.RW_VW_R04C0484_INC
WHERE CAST(ISNULL(NULLIF(SaldoInicial,''),'-1') AS DECIMAL) < 0;

END
GO
