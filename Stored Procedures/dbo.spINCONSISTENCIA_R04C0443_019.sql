SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_019]
AS

BEGIN

-- El monto del saldo del principal al inicio del periodo deberá ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, SaldoInicial
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(SaldoInicial,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
