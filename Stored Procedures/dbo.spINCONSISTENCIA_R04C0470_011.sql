SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_011]
AS

BEGIN

-- Si el Porcentaje de Pagos en Tiempo con Instituciones Financiera Bancarias en los Últimos 12 meses
-- (dat_porcent_pgo_bcos) es = 0, entonces el Puntaje Asignado por Porcentaje de Pagos en Tiempo con
-- Instituciones Financiera Bancarias en los Últimos 12 meses (cve_ptaje_porcent_pago_bcos) debe ser = 17 ó = 54

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstBanc,
	P_PorcPagoInstBanc AS Puntos_PorcPagoInstBanc
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(PorcPagoInstBanc AS DECIMAL(10,6)) = 0 AND ISNULL(P_PorcPagoInstBanc,'') NOT IN ('17','54');

END


GO
