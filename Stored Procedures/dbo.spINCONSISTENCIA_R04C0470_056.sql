SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_056]
AS

BEGIN

-- Validar que un mismo RFC Acreditado no tenga más de un ID Acred. Asignado por la Inst. diferente.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RFC
FROM dbo.RW_R04C0470
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_R04C0470 rep
	INNER JOIN dbo.RW_R04C0470 rep2 ON rep.RFC = rep2.RFC
	AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.RFC
);

END
GO
