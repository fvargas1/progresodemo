SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_008_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- El porcentaje descubierto debe ser 100%, si el crédito no cuenta con ninguna garantía asociada. dat_porcent_no_cubierto = 100
-- si dat_hum_gtias_reales_financ=0 y dat_num_gtias_reales_no_fin=0 y dat_pctje_cubto_oblig_aval=0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0461_INC
WHERE CAST(PrctGarRealFin AS DECIMAL(10,6)) + CAST(PrctGarRealNoFin AS DECIMAL(10,6)) + CAST(PrctAval AS DECIMAL(10,6)) + CAST(PrctECPM AS DECIMAL(10,6)) = 0 AND CAST(PrctExpuesto AS DECIMAL(10,6)) <> 100;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
