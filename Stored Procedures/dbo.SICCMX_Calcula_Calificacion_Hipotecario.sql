SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Calificacion_Hipotecario]
AS
UPDATE hrv
SET IdCalificacion = cal.IdCalificacion
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON vp.IdMetodologia = cal.IdMetodologia AND hrv.PorReserva BETWEEN cal.RangoMenor AND cal.RangoMayor;
GO
