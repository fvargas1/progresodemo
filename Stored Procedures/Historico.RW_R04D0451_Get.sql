SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04D0451_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Concepto,
	Subreporte,
	Moneda,
	MetodoCalificacion,
	TipoCalificacion,
	TipoSaldo,
	Dato
FROM Historico.RW_R04D0451
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Concepto;
GO
