SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Hipotecario_Moneda_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Hipotecario_Moneda_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
WHERE mon.Codigo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'Moneda',
 Moneda,
 2,
 @Detalle
FROM dbo.FILE_Garantia_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
WHERE mon.Codigo IS NULL;
GO
