SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_024]
AS

BEGIN

-- El monto del pago exigible no puede ser mayor a la Responsabilidad Total, a menos que sea amortización única al vencimiento.

SELECT DISTINCT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.MontoExigible, r.ResponsabilidadTotal, vw.PeriodicidadPagosCapital
FROM dbo.RW_R04C0443 r
INNER JOIN dbo.SICCMX_VW_Datos_0442 vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV
WHERE CAST(ISNULL(NULLIF(MontoExigible,''),'0') AS DECIMAL(23,2)) > CAST(ISNULL(NULLIF(ResponsabilidadTotal,''),'0') AS DECIMAL(23,2)) AND vw.PeriodicidadPagosCapital <> '1';

END
GO
