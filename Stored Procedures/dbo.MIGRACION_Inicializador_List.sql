SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inicializador_List]
AS
SELECT
	vw.IdInicializador,
	vw.Description,
	vw.Codename,
	vw.Position
FROM dbo.MIGRACION_VW_Inicializador vw
ORDER BY vw.POSITION;
GO
