SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_010]
AS

BEGIN

-- El RFC dentro del Identificador Met. CNBV debe corresponder con el acreditado

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0478_INC
WHERE CHARINDEX(ISNULL(RFC,''), ISNULL(CodigoCreditoCNBV,'')) = 0;

END


GO
