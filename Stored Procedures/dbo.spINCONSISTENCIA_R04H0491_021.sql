SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_021]
AS

BEGIN

-- La "DENOMINACION DEL CRÉDITO ORIGINAL" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.DenominacionCredito
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Moneda cat ON ISNULL(r.DenominacionCredito,'') = cat.CodigoCNBV_Hipo
WHERE cat.IdMoneda IS NULL;

END
GO
