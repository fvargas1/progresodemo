SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_010_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Para créditos bursatilizados (en columna 9, "Tipo de Alta del Crédito" = 10) se validará que el crédito se haya reportado
-- en el formulario H-0493 Bajas de créditos a la vivienda por concepto de baja por bursatilización. Este tipo de créditos
-- deben tener el "Tipo de Baja" igual a 2 (columna 8 = 2) en el formulario H-0493.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r91.IdReporteLog)
FROM dbo.RW_R04H0491 r91
LEFT OUTER JOIN dbo.RW_R04H0493 r93 ON r91.CodigoCreditoCNBV = r93.CodigoCreditoCNBV AND r93.TipoBaja = '2'
WHERE r91.TipoAlta='10' AND r93.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
