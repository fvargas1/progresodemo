SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_007]
AS

BEGIN

-- El monto del saldo del principal al inicio del periodo deberá ser mayor o igual a cero.

SELECT CodigoCredito, CodigoCreditoCNBV, SaldoPrincipalInicial
FROM dbo.RW_R04H0493
WHERE CAST(ISNULL(NULLIF(SaldoPrincipalInicial,''),'-1') AS DECIMAL) < 0;

END
GO
