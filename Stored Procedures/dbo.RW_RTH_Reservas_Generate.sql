SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_RTH_Reservas_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = '_RTH_Reservas';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_RTH_Reservas;

INSERT INTO dbo.RW_RTH_Reservas (
	IdReporteLog, Codigo, SaldoCapitalVigente, InteresVigente, SaldoCapitalVencido, InteresVencido, Constante, FactorATR,
	ATR, FactorVECES, Veces, FactorPorPago, PorPago, [PI], SP, E, Reserva, PorReserva, Calificacion
)
SELECT DISTINCT
	@IdReporteLog,
	con.Codigo,
	CAST(const.Constante AS DECIMAL(10,4)),
	CAST(con.SaldoCapitalVigenteValorizado AS DECIMAL(23,2)),
	CAST(con.InteresVigenteValorizado AS DECIMAL(23,2)),
	CAST(con.SaldoCapitalVencidoValorizado AS DECIMAL(23,2)),
	CAST(con.InteresVencidoValorizado AS DECIMAL(23,2)),
	CAST(const.ATR AS DECIMAL(10,4)) AS factorATR,
	CAST(vp.ATR AS DECIMAL(16,8)),
	CAST(const.VECES AS DECIMAL(10,4)) AS factorVECES,
	CAST(vp.Veces AS DECIMAL(16,8)),
	CAST(const.PorVPAGO AS DECIMAL(10,4)) AS factorProPago,
	CAST(vp.PPagoIM AS DECIMAL(16,8)),
	CAST(v.[PI] AS DECIMAL(16,8)),
	CAST(v.SP AS DECIMAL(16,8)),
	v.E,
	v.Reserva,
	CAST(v.PorReserva AS DECIMAL(16,8)),
	calif.Codigo AS Calificacion
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables v ON v.IdHipotecario = vp.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Hipotecario con ON con.IdHipotecario = vp.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON met.IdMetodologiaHipotecario = vp.IdMetodologia
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calif ON calif.IdCalificacion = v.IdCalificacion
WHERE met.Codigo = '2';


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_RTH_Reservas WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
