SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_100_Cat]
AS
-- SE ACTUALIZA PI AL 100% PARA PERSONAS CON PI100
UPDATE ppi
SET [PI] = 1
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON ppi.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
WHERE per.PI100 IS NOT NULL AND cre.MontoValorizado > 0;

-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT cre.IdPersona, GETDATE(), '', 'Se actualizó la PI al 100% con base en opción "' + CAST(per.PI100 AS VARCHAR(2)) + '" del Catalogo de PI100'
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
WHERE per.PI100 IS NOT NULL AND cre.MontoValorizado > 0;
GO
