SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_TipoCambio_Fecha_Catalog]
AS
SET DATEFORMAT YMD;
DECLARE @Periodo DATETIME;
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_TipoCambio_Fecha_Datetime';
SELECT @Periodo = Fecha FROM SICC_Periodo WHERE Activo = 1;

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_TipoCambio
SET errorCatalogo = 1
WHERE (MONTH(CAST(FechaTipoCambio AS DATETIME))<>MONTH(@Periodo) OR YEAR(CAST(FechaTipoCambio AS DATETIME))<>YEAR(@Periodo));

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_TipoCambio_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoMonedaISO,
	'FechaTipoCambio',
	FechaTipoCambio,
	1,
	@Detalle
FROM dbo.FILE_TipoCambio
WHERE (MONTH(CAST(FechaTipoCambio AS DATETIME))<>MONTH(@Periodo) OR YEAR(CAST(FechaTipoCambio AS DATETIME))<>YEAR(@Periodo));
GO
