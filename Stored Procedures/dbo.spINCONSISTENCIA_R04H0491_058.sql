SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_058]
AS

BEGIN

-- Si la tasa de referencia indica que es TFIJA (600), entonces el Ajuste en la Tasa de Referencia es 0.

SELECT CodigoCredito, CodigoCreditoCNBV, TasaRef, AjusteTasaRef
FROM dbo.RW_R04H0491
WHERE TasaRef = '600' AND AjusteTasaRef <> '0';

END
GO
