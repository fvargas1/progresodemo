SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_AUTO_Seg_002]
AS

BEGIN

-- El campo "Escala de los Períodos de Facturación del Crédito" se debe reportar, según el Período de Facturación:
-- 10 = Semanal; 20 = Decenal; 30 = Catorcenal; 40 = Quincenal; 50 = Mensual, y 60 = Un solo pago al vencimiento tanto del capital como de los intereses.

SELECT DISTINCT
	rep.FolioCredito,
	rep.PeriodosFacturacionCredito
FROM dbo.RW_Consumo_AUTO rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo cat ON rep.PeriodosFacturacionCredito = cat.CodigoBanxico
WHERE cat.IdPeriodicidadPagoConsumo IS NULL;

END
GO
