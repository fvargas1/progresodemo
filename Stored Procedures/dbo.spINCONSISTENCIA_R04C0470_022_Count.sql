SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_022_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Rendimiento Sobre Capital ROE (dat_roe) es > 0 y < 0.10, entonces el Puntaje Asignado por Rendimiento Sobre Capital ROE (cve_ptaje_roe) debe ser = 64

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(RendCapROE AS DECIMAL(10,6)) > 0 AND CAST(RendCapROE AS DECIMAL(10,6)) < 0.10 AND ISNULL(P_RendCapROE,'') <> '64';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
