SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_GeneroAcreditado_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_GeneroAcreditado_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Genero cat ON LTRIM(f.GeneroAcreditado) = cat.Codigo
WHERE cat.IdGenero IS NULL AND LEN(f.GeneroAcreditado) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'GeneroAcreditado',
 f.GeneroAcreditado,
 2,
 @Detalle
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Genero cat ON LTRIM(f.GeneroAcreditado) = cat.Codigo
WHERE cat.IdGenero IS NULL AND LEN(f.GeneroAcreditado) > 0;
GO
