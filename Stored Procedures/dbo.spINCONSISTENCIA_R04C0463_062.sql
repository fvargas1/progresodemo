SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_062]
AS
BEGIN
-- Si el Tipo de Operación es 254, el Destino de Crédito deberá ser 330, 332, 333, 338, 430, 432, 433 ó 438

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoOperacion,
	DestinoCredito
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(TipoOperacion,'') = '254' AND ISNULL(DestinoCredito,'') NOT IN ('330', '332', '333', '338', '430', '433', '438');

END

GO
