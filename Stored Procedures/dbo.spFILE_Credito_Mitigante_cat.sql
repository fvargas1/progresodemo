SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_Mitigante_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_Mitigante_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_Mitigante cat ON LTRIM(f.Mitigante) = cat.Codigo
WHERE cat.IdMitigante IS NULL AND LEN(f.Mitigante) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'Mitigante',
 f.Mitigante,
 2,
 @Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_Mitigante cat ON LTRIM(f.Mitigante) = cat.Codigo
WHERE cat.IdMitigante IS NULL AND LEN(f.Mitigante) > 0;
GO
