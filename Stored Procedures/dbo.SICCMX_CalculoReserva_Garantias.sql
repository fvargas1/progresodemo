SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoReserva_Garantias]
AS
-- LAS GARANTIAS FINANCIERAS NO GENERAN RESERVAS
UPDATE dbo.SICCMX_Garantia_Canasta
SET Reserva = 0
WHERE IdTipoGarantia IS NULL AND EsDescubierto IS NULL;

-- CALCULAMOS LAS RESERVAS
UPDATE can
SET Reserva = (can.PrctCobAjust * can.SeveridadCorresp) * can.[PI] * cre.EI_Total
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
WHERE can.IdTipoGarantia IS NOT NULL OR can.EsDescubierto = 1;

-- CALCULAMOS LAS RESERVAS PARA GARANTIAS DE PASO Y MEDIDA
UPDATE can
SET Reserva = can.MontoRecuperacion * can.SeveridadCorresp * can.[PI]
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE tg.Codigo='NMC-05';
GO
