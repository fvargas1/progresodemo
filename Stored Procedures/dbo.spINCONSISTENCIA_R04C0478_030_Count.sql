SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_030_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Alta del Crédito es mayor o igual a 700, el Tipo de Operación debe ser 181 ó 256

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(TipoAltaCredito,'0') AS INT) >= 700 AND ISNULL(TipoOperacion,'') NOT IN ('181','256');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
