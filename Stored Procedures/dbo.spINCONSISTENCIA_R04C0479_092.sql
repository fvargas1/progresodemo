SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_092]
AS

BEGIN

-- Validar que el Monto de Intereses Moratorios y Otros Accesorios Pagados Efectivamente por el Acreditado en el Periodo (dat_mto_inter_mora_otros_acc) sea MAYOR O IGUAL a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	MontoInteresMoratorio
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(MontoInteresMoratorio,''),'-1') AS DECIMAL) < 0

END


GO
