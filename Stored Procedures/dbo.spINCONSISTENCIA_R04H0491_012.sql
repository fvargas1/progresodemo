SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_012]
AS

BEGIN

-- La "CATEGORIA CREDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.CategoriaCredito
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_CategoriaCreditoHipotecario cat ON ISNULL(r.CategoriaCredito,'') = cat.CodigoCNBV
WHERE cat.IdCategoriaHipotecario IS NULL;

END
GO
