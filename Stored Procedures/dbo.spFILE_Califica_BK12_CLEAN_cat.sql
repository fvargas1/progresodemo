SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_BK12_CLEAN_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_BK12_CLEAN_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Califica
SET errorCatalogo = 1
WHERE LTRIM(ISNULL(BK12_CLEAN,'')) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoDeudor,
 'BK12_CLEAN',
 BK12_CLEAN,
 2,
 @Detalle
FROM dbo.FILE_Califica
WHERE LTRIM(ISNULL(BK12_CLEAN,'')) NOT IN ('0','1');
GO
