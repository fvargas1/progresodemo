SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_TipoCambio_errores_ClearLog]
AS
UPDATE dbo.FILE_TipoCambio
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_TipoCambio_errores;
GO
