SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_PM_Consumo]
AS
-- GARANTIAS PASO Y MEDIDA
UPDATE can
SET
 C = gar.ValorGarantiaValorizado,
 PrctCobSinAju = CASE WHEN con.E > 0 THEN CAST(gar.ValorGarantiaValorizado / con.E AS DECIMAL(18,8)) ELSE 0 END,
 PrctCobAjust = CASE WHEN con.E > 0 THEN CAST(gar.ValorGarantiaValorizado / con.E AS DECIMAL(18,8)) ELSE 0 END,
 MontoCobAjust = gar.ValorGarantiaValorizado,
 [PI] = ISNULL(gar.PIGarante, 0.01111111)
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables con ON can.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoGarantia cg ON con.IdConsumo = cg.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia AND can.IdTipoGarantia = gar.IdTipoGarantia
WHERE gar.TipoGarantia = 'NMC-05';
GO
