SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_053_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_29_dias_atra) es = 15,
-- entonces el Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (dat_porcen_pgo_bco_29_dias) debe ser > 0 y < 50

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoInstBanc29,'') = '15' AND (CAST(PorcPagoInstBanc29 AS DECIMAL(10,6)) <= 0 OR CAST(PorcPagoInstBanc29 AS DECIMAL(10,6)) >= 50);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
