SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_082_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Moneda corresponda a Catalogo de CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC rep
LEFT OUTER JOIN dbo.SICC_Moneda moneda ON ISNULL(rep.Moneda,'') = moneda.CodigoCNBV
WHERE moneda.IdMoneda IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
