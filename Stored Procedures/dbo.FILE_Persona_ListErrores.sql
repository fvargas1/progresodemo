SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Persona_ListErrores]
AS
SELECT
 NombreCliente,
 CodigoCliente,
 RFC,
 ActividadEconomica,
 TipoAcreditadoRelacionadoMA,
 TipoAcreditadoRelacionado,
 Domicilio,
 Ejecutivo,
 Telefono,
 Correo,
 Sucursal,
 NombreGrupoEconomico,
 PersonalidadJuridicaMA,
 PersonalidadJuridica,
 NumeroEmpleados,
 IngresosBrutos,
 ComprobanteIngresos,
 SectorLaboral,
 Localidad,
 NombreCNBV,
 Estado,
 Municipio,
 Nacionalidad,
 CURP,
 LEI,
 HITSIC,
 EsFondo,
 PadreGrupoEconomico,
 PI100,
 SectorEconomico,
 RCGrupoRiesgo,
 RCCalificacion,
 CalificacionLargoPlazo,
 AgenciaLargoPlazo,
 CalificacionCortoPlazo,
 AgenciaCortoPlazo,
 CodigoPostal,
 Fuente
FROM dbo.FILE_Persona
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
