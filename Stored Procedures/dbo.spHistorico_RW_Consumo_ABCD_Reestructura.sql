SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_ABCD_Reestructura]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_ABCD_Reestructura);

DELETE FROM Historico.RW_Consumo_ABCD_Reestructura WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_ABCD_Reestructura (
	IdPeriodoHistorico, FolioCredito, FechaReestructuraCredito, Reestructura, QuitasCondonacionesBonificacionesDescuentos, Folio2Credito
)
SELECT
	@IdPeriodoHistorico,
	FolioCredito,
	FechaReestructuraCredito,
	Reestructura,
	QuitasCondonacionesBonificacionesDescuentos,
	Folio2Credito
FROM dbo.RW_Consumo_ABCD_Reestructura
WHERE IdReporteLog = @IdReporteLog;
GO
