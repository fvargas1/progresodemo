SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_Provedores_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_Provedores_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo22 f
LEFT OUTER JOIN dbo.CAT_VW_Proveedores cat ON LTRIM(f.Provedores) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCliente,
	'Provedores',
	f.Provedores,
	2,
	@Detalle
FROM dbo.FILE_Anexo22 f
LEFT OUTER JOIN dbo.CAT_VW_Proveedores cat ON LTRIM(f.Provedores) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL;
GO
