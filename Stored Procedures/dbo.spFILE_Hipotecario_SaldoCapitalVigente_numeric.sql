SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_SaldoCapitalVigente_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_SaldoCapitalVigente_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorFormato = 1
WHERE ISNUMERIC(ISNULL(SaldoCapitalVigente,'0')) = 0 OR SaldoCapitalVigente LIKE '%[^0-9 .]%';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'SaldoCapitalVigente',
 SaldoCapitalVigente,
 1,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE ISNUMERIC(ISNULL(SaldoCapitalVigente,'0')) = 0 OR SaldoCapitalVigente LIKE '%[^0-9 .]%';
GO
