SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_UpdReestructuras_Lineas]
AS
DECLARE @IdTipoAlta INT;
DECLARE @IdTipoBaja INT;
DECLARE @IdTipoBajaReest INT;

SELECT @IdTipoAlta = IdTipoAlta FROM dbo.SICC_TipoAlta WHERE Codigo = '140';
SELECT @IdTipoBaja = IdTipoBaja FROM dbo.SICC_TipoBaja WHERE Codigo = '132';
SELECT @IdTipoBajaReest = IdTipoBaja FROM dbo.SICC_TipoBaja WHERE Codigo = '142';

-- ACTUALIZAMOS EL TIPO DE ALTA PARA CREDITOS NUEVOS REESTRUCTURADOS
UPDATE lin
SET IdTipoAlta = @IdTipoAlta
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_LineaCredito linReest ON lin.Codigo = linReest.CodigoCreditoReestructurado;

-- ACTUALIZAMOS EL TIPO DE BAJA PARA CREDITOS VIEJOS REESTRUCTURADOS
UPDATE lin
SET IdTipoBaja = @IdTipoBaja
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_LineaCredito linReest ON lin.CodigoCreditoReestructurado = linReest.Codigo;

-- ACTUALIZAMOS EL TIPO DE BAJA PARA EL CREDITO REESTRUCTURADO MAS ANTIGUO
UPDATE reest
SET IdTipoBaja = @IdTipoBajaReest
FROM dbo.SICCMX_LineaCredito lin
CROSS APPLY (
 SELECT TOP 1 l.Codigo, l.IdTipoBaja
 FROM dbo.SICCMX_LineaCredito l
 INNER JOIN dbo.SICCMX_LineaCredito lr ON l.CodigoCreditoReestructurado = lr.Codigo
 WHERE l.CodigoCreditoReestructurado = lin.Codigo
 ORDER BY l.FechaDisposicion
) AS reest;
GO
