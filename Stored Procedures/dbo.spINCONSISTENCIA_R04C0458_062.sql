SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_062]
AS
BEGIN
-- Si el Tipo de Cartera (cve_tipo_cartera) es IGUAL a 299, 410 ó 420 la Nacionalidad del Acreditado (cve_pais) sea diferente de 484.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	Nacionalidad
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoCartera,'') IN ('299','410','420') AND ISNULL(Nacionalidad,'') = '484';

END
GO
