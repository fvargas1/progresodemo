SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_032]
AS
BEGIN
-- La Responsabilidad Total debe ser mayor o igual al Saldo del Principal al Final del Periodo

SELECT
	CodigoCredito,
	SaldoInsoluto,
	SaldoFinal
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoFinal,''),'0') AS DECIMAL);

END
GO
