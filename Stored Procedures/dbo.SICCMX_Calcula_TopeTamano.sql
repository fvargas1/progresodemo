SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_TopeTamano]
AS
-- CALCULAMOS EL TOPE MAXIMO COMBINADO
UPDATE inf
SET TopeTamano = CAST((inf.NumeroEmpleados * 0.1) + ((inf.IngresosBrutos / 1000000) * 0.9) AS DECIMAL(9,1))
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo inf ON per.IdPersona = inf.IdPersona;

-- ACTUALIZAMOS EL TAMANO DEL DEUDOR DE ACUERDO A SU TOPE MAXIMO
UPDATE inf
SET IdTamanoDeudor = tam.IdTamanoDeudor
FROM dbo.SICCMX_PersonaInfo inf
INNER JOIN dbo.SICCMX_Persona per ON inf.IdPersona = per.IdPersona
INNER JOIN dbo.SICC_ActividadEconomica act ON per.IdActividadEconomica = act.IdActividadEconomica
INNER JOIN dbo.SICC_Rel_Tamano_Sector rel ON act.IdSector = rel.IdSector AND inf.TopeTamano BETWEEN rel.RangoMenor AND rel.RangoMayor
INNER JOIN dbo.SICC_TamanoDeudor tam ON rel.IdTamano = tam.IdTamanoDeudor;
GO
