SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_034]
AS

BEGIN

-- Si el Puntaje Asignado por Indicador de Persona Moral o Fideicomiso (cve_ptaje_indicad_pm_fideico) es = 56,
-- entonces el Indicador de Persona Moral o Fideicomiso (dat_indicador_pm_fideico) debe ser = 1

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	IndPersFid,
	P_IndPersFid AS Puntos_IndPersFid
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_IndPersFid,'') = '56' AND ISNULL(IndPersFid,'') <> '1';

END


GO
