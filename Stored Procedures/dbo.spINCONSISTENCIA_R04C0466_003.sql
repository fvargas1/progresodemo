SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_003]
AS

BEGIN

-- El factor ajuste hfx debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	Hfx
FROM dbo.RW_R04C0466
WHERE ISNULL(Hfx,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END

GO
