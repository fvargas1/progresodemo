SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_065_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- El RENDIMIENTO SOBRE CAPITAL (ROE) debe tener formato Numérico (10,6)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0470_INC

WHERE LEN(RendCapROE) > 0 AND RendCapROE <> '-999'

	AND (CHARINDEX('.',RendCapROE) = 0 OR CHARINDEX('.',RendCapROE) > 5 OR LEN(LTRIM(SUBSTRING(RendCapROE, CHARINDEX('.', RendCapROE) + 1, LEN(RendCapROE)))) <> 2);



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
