SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_108_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la SP No Cubierta por Garantias Personales esté entre 0 y 100.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(SPExpuesta,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
