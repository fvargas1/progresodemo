SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_Reservas_A22_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_Analitico_Reservas_A22';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Analitico_Reservas_A22;

-- Informacion para reporte
INSERT INTO dbo.RW_Analitico_Reservas_A22 (
 IdReporteLog, Fecha, CodigoPersona, Nombre, CodigoCredito, MontoCredito, FechaVencimiento, [PI], Moneda, ActividadEconomica,
 MontoGarantia, MontoGarantiaAjustado, Prct_Reserva, SP, MontoReserva, PI_Aval, SP_Aval, Calificacion, ReservaAdicional
)
SELECT DISTINCT
 @IdReporteLog,
 CONVERT(CHAR(10), GETDATE(),126) AS Fecha,
 per.Codigo AS CodigoPersona,
 REPLACE(per.Nombre, ',', '') AS NombreCliente,
 cre.CodigoCredito AS CodigoCredito,
 CAST(cre.MontoValorizado AS DECIMAL(21,2)) AS MontoCredito,
 CONVERT(CHAR(10), cre.FechaVencimiento,126) AS FechaVencimiento,
 CAST(ppi.[PI]*100 AS DECIMAL(14,6)) AS [PI],
 cre.Moneda AS Moneda,
 act.CodigoCNBV AS ActividadEconomica,
 CAST(ISNULL(creGar.Monto,0) AS DECIMAL(21,2)) AS MontoGarantia,
 CAST(ISNULL(can.Monto,0) AS DECIMAL(21,2)) AS MontoGarantiaAjustado,
 CAST(crv.PorReservaFinal*100 AS DECIMAL(14,6)) AS Prct_Reserva,
 CAST(crv.SP_Total*100 AS DECIMAL(14,6)) AS SP,
 CAST(crv.ReservaFinal AS DECIMAL(21,2)) AS MontoReserva,
 CAST(av.[PI]*100 AS DECIMAL(14,6)) AS PI_Aval,
 CAST(av.SP*100 AS DECIMAL(14,6)) AS SP_Aval,
 cal.Codigo AS Calificacion,
 adic.ReservasAdicTotal AS ReservaAdicional
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Credito_ReservasAdicionales adic ON cre.IdCredito = adic.IdCredito
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON per.IdActividadEconomica = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion
LEFT OUTER JOIN (
 SELECT cg.IdCredito, SUM(ISNULL(NULLIF(cg.MontoBanco,0), ISNULL(cg.MontoCubiertoCredito,0))) AS Monto
 FROM dbo.SICCMX_CreditoGarantia cg
 GROUP BY cg.IdCredito
) AS creGar ON cre.IdCredito = creGar.IdCredito
LEFT OUTER JOIN (
 SELECT can.IdCredito, can.SeveridadCorresp AS SP, can.[PI]
 FROM dbo.SICCMX_Garantia_Canasta can
 INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo = 'GP'
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables rv ON can.IdCredito = rv.IdCredito
 WHERE rv.PIAval = 1
) AS av ON cre.IdCredito = av.IdCredito
LEFT OUTER JOIN (
 SELECT cn.IdCredito, SUM(ISNULL(cn.MontoCobAjust,0)) AS Monto
 FROM dbo.SICCMX_Garantia_Canasta cn
 WHERE cn.EsDescubierto IS NULL
 GROUP BY cn.IdCredito
) AS can ON cre.IdCredito = can.IdCredito
WHERE met.Codigo = '22' AND cre.MontoValorizado > 0;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_Reservas_A22 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
