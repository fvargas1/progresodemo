SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_041]
AS

BEGIN

-- Si el Puntaje Asignado por la Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) es = 53,
-- entonces la Tasa de Retención Laboral (dat_tasa_retencion_laboral) debe ser = -999 (Sin Informacion)

SELECT
 CodigoPersona AS CodigoDeudor,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 TasaRetLab,
 P_TasaRetLab AS Puntos_TasaRetLab
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_TasaRetLab,'') = '53' AND CAST(TasaRetLab AS DECIMAL(18,6)) <> -999;

END


GO
