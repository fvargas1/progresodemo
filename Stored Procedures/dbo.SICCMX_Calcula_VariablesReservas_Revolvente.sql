SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_VariablesReservas_Revolvente]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo = '5';

-- Zi
UPDATE vp
SET Zi =
 (ct.Constante 
 + (ct.ATR * vp.NumeroImpagosConsecutivos)
 + (ct.MAXATR * vp.NumeroImpagosHistoricos)
 + (ct.ProPR * vp.PorcentajeUso)
 + (ct.ProPago * vp.PorPago)
 + (ct.Alto * vp.Alto)
 + (ct.Medio * vp.Medio)
 + (ct.Bajo * vp.Bajo)
 + (ct.GVeces1 * info.GVeces1)
 + (ct.GVeces2 * info.GVeces2)
 + (ct.GVeces3 * info.GVeces3)
 + (ct.INDATR * info.Meses_desde_ult_atr_bk)
 )
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON vp.IdMetodologia = ct.IdMetodologia
INNER JOIN dbo.SICCMX_ConsumoInfo info ON vp.IdConsumo = info.IdConsumo
WHERE vp.IdMetodologia = @IdMetodologia;

-- PI
UPDATE crv
SET [PI] =
 CASE
 WHEN vp.NumeroImpagosConsecutivos >= ct.ATRPI THEN 1.00
 ELSE 1 / ( 1 + EXP( -1 * vp.Zi))
 END
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares vp ON crv.IdConsumo = vp.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON vp.IdMetodologia = ct.IdMetodologia
INNER JOIN dbo.SICCMX_ConsumoInfo info ON crv.IdConsumo = info.IdConsumo
WHERE vp.IdMetodologia = @IdMetodologia;


-- SP
UPDATE crv
SET SPExpuesta =
 CASE
 WHEN vp.NumeroImpagosConsecutivos <= 4 THEN .75
 WHEN vp.NumeroImpagosConsecutivos > 4 AND vp.NumeroImpagosConsecutivos <= 5 THEN .77
 WHEN vp.NumeroImpagosConsecutivos > 5 AND vp.NumeroImpagosConsecutivos <= 6 THEN .80
 WHEN vp.NumeroImpagosConsecutivos > 6 AND vp.NumeroImpagosConsecutivos <= 7 THEN .82
 WHEN vp.NumeroImpagosConsecutivos > 7 AND vp.NumeroImpagosConsecutivos <= 8 THEN .86
 WHEN vp.NumeroImpagosConsecutivos > 8 AND vp.NumeroImpagosConsecutivos <= 9 THEN .90
 WHEN vp.NumeroImpagosConsecutivos > 9 AND vp.NumeroImpagosConsecutivos <= 10 THEN .92
 WHEN vp.NumeroImpagosConsecutivos > 10 AND vp.NumeroImpagosConsecutivos <= 11 THEN .96
 ELSE 1.00
 END
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares vp ON crv.IdConsumo = vp.IdConsumo
WHERE vp.IdMetodologia = @IdMetodologia;


-- EI
UPDATE crv
SET E =
 CASE
 WHEN con.SaldoTotalValorizado < vp.LimiteCredito THEN
 dbo.MAX2VAR(
 CASE
 WHEN con.SaldoTotalValorizado = 0 OR vp.LimiteCredito = 0 THEN 0
 ELSE con.SaldoTotalValorizado * CASE WHEN 1.026 * POWER(con.SaldoTotalValorizado / vp.LimiteCredito, -.5434) > 1 THEN 1.026 * POWER(con.SaldoTotalValorizado / vp.LimiteCredito, -.5434) ELSE 1 END
 END,
 con.SaldoTotalValorizado + (0.1379 * (vp.LimiteCredito - con.SaldoTotalValorizado)))
 ELSE con.SaldoTotalValorizado
 END
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares vp ON crv.IdConsumo = vp.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON vp.IdMetodologia = ct.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Consumo con ON vp.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
WHERE vp.IdMetodologia = @IdMetodologia;
GO
