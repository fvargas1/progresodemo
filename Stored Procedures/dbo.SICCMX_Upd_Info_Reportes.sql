SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_Info_Reportes]
AS
-- REDONDEAMOS LOS MONTOS A 2 DECIMALES PARA EVITAR DESCUADRES EN REPORTES
UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET
ExpProyectada = ROUND(ExpProyectada,2),
EI_Total = ROUND(EI_Total,2),
EI_Expuesta = ROUND(EI_Expuesta,2),
EI_Cubierta = ROUND(EI_Cubierta,2),
ReservaFinal = ROUND(ReservaFinal,2),
ReservaExpuesta = ROUND(ReservaExpuesta,2),
ReservaCubierta = ROUND(ReservaCubierta,2),
ReservaFinalOriginal = ROUND(ReservaFinalOriginal,2),
ReservaExpuestaOriginal = ROUND(ReservaExpuestaOriginal,2),
ReservaCubiertaOriginal = ROUND(ReservaCubiertaOriginal,2),
EI_Expuesta_GarPer = ROUND(EI_Expuesta_GarPer,2),
EI_Cubierta_GarPer = ROUND(EI_Cubierta_GarPer,2),
ReservaExpuesta_GarPer = ROUND(ReservaExpuesta_GarPer,2),
ReservaCubierta_GarPer = ROUND(ReservaCubierta_GarPer,2);

UPDATE dbo.SICCMX_Garantia_Canasta
SET
EI_Ajust = ROUND(EI_Ajust,2),
MontoRecuperacion = ROUND(MontoRecuperacion,2),
Reserva = ROUND(Reserva,2);
GO
