SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_HipotecarioReservas]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_HipotecarioReservas WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_HipotecarioReservas (
	IdPeriodoHistorico,
	Hipotecario,
	FechaCalificacion,
	MontoExpuesto,
	SPExpuesto,
	ReservaExpuesto,
	PorcentajeExpuesto,
	CalificacionExpuesto,
	MontoCubierto,
	SPCubierto,
	ReservaCubierto,
	PorcentajeCubierto,
	CalificacionCubierto,
	MontoAdicional,
	ReservaAdicional,
	PorcentajeAdicional,
	CalificacionAdicional
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	hip.Codigo,
	res.FechaCalificacion,
	res.MontoExpuesto,
	res.SPExpuesto,
	res.ReservaExpuesto,
	res.PorcentajeExpuesto,
	calExp.Codigo,
	res.MontoCubierto,
	res.SPCubierto,
	res.ReservaCubierto,
	res.PorcentajeCubierto,
	calCub.Codigo,
	res.MontoAdicional,
	res.ReservaAdicional,
	res.PorcentajeAdicional,
	calAdi.Codigo
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON hip.IdHipotecario = res.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calExp ON res.IdCalificacionExpuesto = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calCub ON res.IdCalificacionCubierto = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calAdi ON res.IdCalificacionAdicional = calAdi.IdCalificacion;
GO
