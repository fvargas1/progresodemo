SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_023]
AS
BEGIN
-- La clave de la institución dentro del ID es incorrecta

DECLARE @IdInstitucion INT;
SELECT @IdInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	CodigoCreditoCNBV,
	@IdInstitucion AS Institucion
FROM dbo.RW_VW_R04C0458_INC
WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),2,6) <> @IdInstitucion;

END
GO
