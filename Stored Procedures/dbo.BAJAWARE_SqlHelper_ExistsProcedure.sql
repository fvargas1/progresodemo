SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_SqlHelper_ExistsProcedure]
	@ProcedureName NVARCHAR(150)
AS
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@ProcedureName) AND type in (N'P', N'PC'))
SELECT 1
ELSE
SELECT 0
GO
