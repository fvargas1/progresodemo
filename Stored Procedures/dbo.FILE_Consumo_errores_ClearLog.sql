SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Consumo_errores_ClearLog]
AS
UPDATE dbo.FILE_Consumo
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Consumo_errores;
GO
