SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_038_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Si la Tasa de Retención Laboral (dat_tasa_retencion_laboral) es >=66.6,
-- entonces el Puntaje Asignado por Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) debe ser = 67

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(TasaRetLab AS DECIMAL(18,6)) >= 66.6 AND ISNULL(P_TasaRetLab,'') <> '67';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
