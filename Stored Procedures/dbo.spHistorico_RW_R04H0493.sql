SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0493]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0493);

DELETE FROM Historico.RW_R04H0493 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0493 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, TipoBaja, SaldoPrincipalInicial,
	MontoTotalLiquidacion, MontoPagoLiquidacion, MontoBonificaciones, ValorBienAdjudicado
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	MontoTotalLiquidacion,
	MontoPagoLiquidacion,
	MontoBonificaciones,
	ValorBienAdjudicado
FROM dbo.RW_R04H0493
WHERE IdReporteLog = @IdReporteLog;
GO
