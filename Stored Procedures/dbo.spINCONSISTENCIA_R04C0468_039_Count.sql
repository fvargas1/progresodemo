SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_039_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Tasa de Interés (cve_tasa) es IGUAL a 300, 305, 310, 320, 330, 340, 350, 360, 370, 410 ó 480, la moneda (cve_moneda) debe ser DIFERENTE de 0 (pesos).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TasaInteres,'') IN ('300','305','310','320','330','340','350','360','370','410','480') AND ISNULL(Moneda,'') = '0'
	AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139','700','701');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
