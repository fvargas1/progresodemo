SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_026_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Nombre del Acreditado (dat_nombre) acepte valores alfanuméricos y los siguientes caracteres especiales: "_", "/" o ".".

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC
WHERE NombrePersona LIKE '%[^a-z,.0-9 _/]%' OR NombrePersona LIKE '%[áéíóú]%';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
