SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_PersonaDatosCedula]
	@IdPersona BIGINT,
	@IdProyecto BIGINT
AS
DECLARE @Periodo DATETIME;
SELECT @Periodo=Fecha FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	i.Nombre AS Institucion,
	@Periodo AS Fecha_Evaluacion,
	p.Nombre AS Nombre_Deudor,
	p.Codigo AS Expediente,
	REPLACE(p.RFC,'_','') AS RFC,
	ISNULL(p.Domicilio,'') AS Dir_Deudor,
	ISNULL(p.Telefono,'NA') AS Telefono,
	ISNULL(p.Correo,'NA') AS Email,
	'' AS Region_Economica,
	ISNULL(sec.Nombre,'') AS Sector_Economico,
	ISNULL(ae.Nombre,'NA') AS Actividad_Especifica,
	acc.Nombre AS Socio_Principal,
	CASE WHEN dr.Codigo='8' OR dr.Codigo IS NULL THEN 'NO' ELSE 'SI' END AS Acreditado_Relacionado,
	'' AS Grupo_Economico,
	ISNULL(dr.Descripcion,'') AS Fraccion_Art_73,
	pc.SaldoInsoluto AS Total_Responsabilidades,
	eje.Nombre AS Ejecutivo_Cuenta
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON p.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Proyecto pry ON pInfo.IdPersona = pry.IdPersona
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
LEFT OUTER JOIN dbo.SICC_Institucion i ON i.IdInstitucion = p.IdInstitucion
LEFT OUTER JOIN dbo.SICC_ActividadEconomica ae ON ae.IdActividadEconomica = p.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sec ON pInfo.IdSectorEconomico = sec.IdSectorEconomico
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado dr ON dr.IdDeudorRelacionado = p.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICCMX_Accionista acc ON p.IdPersona = acc.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo eje ON p.IdEjecutivo = eje.IdEjecutivo
WHERE p.IdPersona = @IdPersona AND pry.IdProyecto = @IdProyecto;
GO
