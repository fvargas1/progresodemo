SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoGarantia_Garantia_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_CreditoGarantia_Garantia_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_CreditoGarantia f
LEFT OUTER JOIN dbo.SICCMX_Garantia cat ON LTRIM(f.CodigoGarantia) = cat.Codigo
WHERE cat.IdGarantia IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_CreditoGarantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoGarantia,
 'CodigoGarantia',
 f.CodigoGarantia,
 2,
 @Detalle
FROM dbo.FILE_CreditoGarantia f
LEFT OUTER JOIN dbo.SICCMX_Garantia cat ON LTRIM(f.CodigoGarantia) = cat.Codigo
WHERE cat.IdGarantia IS NULL;
GO
