SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_056]
AS

BEGIN

-- Validar que la Comisión por Disposición del Crédito en Tasa (dat_com_disposicion_tasa) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionDisposicionTasa
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(ISNULL(NULLIF(ComisionDisposicionTasa,''),'-1') AS DECIMAL(10,6)) < 0;

END


GO
