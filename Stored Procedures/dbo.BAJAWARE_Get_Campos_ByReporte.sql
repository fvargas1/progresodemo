SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Get_Campos_ByReporte]
 @IdGroup INT,
 @IdReporte INT
AS
SELECT GrupoReporte + Nombre + ' - ' + Descripcion AS Reporte
FROM dbo.RW_Reporte rep
WHERE rep.IdReporte = @IdReporte;

SELECT DISTINCT
 cam.IdReporteCampos,
 cam.Descripcion,
 cam.Orden,
 cam.Afectable,
 CASE WHEN rel.IdRel IS NULL THEN cam.TipoDato ELSE rel.TipoDato END AS TipoDato,
 CASE WHEN rel.IdRel IS NULL THEN cam.Longitud ELSE rel.Longitud END AS Longitud,
 CASE WHEN rel.IdRel IS NULL THEN cam.Decimales ELSE rel.Decimales END AS Decimales,
 CASE WHEN rel.IdRel IS NULL THEN cam.Predeterminado ELSE rel.Predeterminado END AS Predeterminado,
 ISNULL(CASE WHEN rel.IdRel IS NULL THEN cam.AplicaFormatoVP ELSE rel.AplicaFormatoVP END, 0) AS AplicaFormatoVP
FROM dbo.RW_ReporteCampos cam
LEFT OUTER JOIN dbo.RW_Rel_Grupo_ReporteCampos rel ON cam.IdReporteCampos = rel.IdReporteCampos AND rel.IdGroup = @IdGroup
WHERE cam.IdReporte = @IdReporte
ORDER BY cam.Orden;
GO
