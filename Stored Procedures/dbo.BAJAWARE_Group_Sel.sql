SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Group_Sel]
	@IdGroup BAJAWARE_utID
AS
SELECT IdGroup, Active, Description, IdGroupType, Codename
FROM dbo.BAJAWARE_Group
WHERE IdGroup = @IdGroup;
GO
