SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_116_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- El PORCENTAJE DE PAGOS A INSTITUCIONES BANCARIAS CON 60 O MÁS DÍAS DE ATRASO EN LOS ÚLTIMOS 24 MESES debe tener formato Numérico (10,6)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE LEN(PorcPagoInstBanc) > 0 AND PorcPagoInstBanc <> '-99'

	AND (CHARINDEX('.',PorcPagoInstBanc) = 0 OR CHARINDEX('.',PorcPagoInstBanc) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc, CHARINDEX('.', PorcPagoInstBanc) + 1, LEN(PorcPagoInstBanc)))) <> 6);



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
