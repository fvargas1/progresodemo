SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_024]
AS

BEGIN

-- El Saldo del Principal al Final del Período debe ser igual a la suma del Saldo del Principal al Inicio del periodo, monto del crédito dispuesto y monto pagado al capital.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInicial,
	MontoDispuesto,
	MontoCapitalPagado,
	SaldoFinal AS 'SaldoFinal [SaldoInicial+MontoDispuesto-MontoCapitalPagado]'
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(SaldoFinal,0) AS MONEY) < (
	CAST(ISNULL(SaldoInicial,0) AS MONEY) +
	CAST(ISNULL(MontoDispuesto,0) AS MONEY) -
	CAST(ISNULL(MontoCapitalPagado,0) AS MONEY))

END

GO
