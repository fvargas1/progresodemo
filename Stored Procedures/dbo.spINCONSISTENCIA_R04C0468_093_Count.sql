SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_093_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el acreditado no es persona física (cve_tipo_cartera <> 320 y 420), entonces la CURP (dat_curp) debe venir en nulo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('320','420') AND LEN(ISNULL(CURP,'')) > 0

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
