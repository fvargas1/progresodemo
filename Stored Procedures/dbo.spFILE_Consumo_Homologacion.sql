SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_Homologacion]
AS
DECLARE @Campo VARCHAR(50);
DECLARE @Tipo INT;
DECLARE @Fuente VARCHAR(50);

-- AGREGAMOS "0" A TIPO DE CREDITO R04A CUANDO ESTE TIENE SOLO 4 POSICIONES
UPDATE dbo.FILE_Consumo SET TipoCreditoR04A = '0' + LTRIM(RTRIM(TipoCreditoR04A)) WHERE LEN(TipoCreditoR04A) < 6;
UPDATE dbo.FILE_Consumo SET TipoCreditoR04A_2016 = '0' + LTRIM(RTRIM(TipoCreditoR04A_2016)) WHERE LEN(TipoCreditoR04A_2016) < 6;

DECLARE crs CURSOR FOR
SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente
FROM dbo.FILE_Consumo_Homologacion
WHERE Activo=1
ORDER BY Tipo;

OPEN crs;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

WHILE @@FETCH_STATUS = 0
BEGIN
EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Consumo', 'FILE_Consumo_Homologacion', @Campo, @Tipo, @Fuente;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;
END

CLOSE crs;
DEALLOCATE crs;

UPDATE dbo.FILE_Consumo SET Homologado = 1;
GO
