SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_059]
AS
BEGIN
-- Si la Tasa de Interés (cve_tasa) es IGUAL a 300, 305, 310, 320, 330, 340, 350, 360, 370, 410 ó 480, la moneda (cve_moneda) debe ser DIFERENTE de 0 (pesos).

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TasaInteres,
	Moneda
FROM dbo.RW_VW_R04C0483_INC
WHERE ISNULL(TasaInteres,'') IN ('300','305','310','320','330','340','350','360','370','410','480') AND ISNULL(Moneda,'') = '0'
	AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139','700','701');

END
GO
