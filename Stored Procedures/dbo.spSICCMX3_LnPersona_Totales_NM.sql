SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX3_LnPersona_Totales_NM]
	@IdPersona BIGINT,
	@IdMetodologia INT
AS
SELECT
	per.IdPersona,
	per.Codigo,
	per.Nombre,
	SUM(ISNULL(crv.EI_Total, 0)) AS TotalResponsabilidad,
	SUM(ISNULL(crv.ReservaFinal, 0)) AS TotalReserva,
	SUM(ISNULL(cub.Monto,0)) AS TotalGarantia,
	SUM(ISNULL(crv.EI_Expuesta, 0)) AS MontoExpuesto,
	SUM(ISNULL(crv.EI_Cubierta, 0)) AS MontoCubierto,
	SUM(ISNULL(crv.ReservaExpuesta_GarPer, 0)) AS ReservaExpuesto,
	SUM(ISNULL(crv.ReservaCubierta_GarPer, 0)) AS ReservaCubierto,
	ISNULL(ppi.[PI], 0) AS [PI]
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON per.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN (
	SELECT can.IdCredito, SUM(ISNULL(can.MontoCobAjust,0)) AS Monto
	FROM dbo.SICCMX_Garantia_Canasta can
	WHERE ISNULL(can.EsDescubierto,0) = 0
	GROUP BY can.IdCredito
) AS cub ON cre.IdCredito = cub.IdCredito
WHERE per.IdPersona=@IdPersona AND met.Codigo IN ('18','20','21','22')
GROUP BY per.IdPersona, per.Codigo, per.Nombre, ppi.[PI];
GO
