SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_035]
AS

BEGIN

-- La probabilidad de incumplimiento del garante debe encontrarse entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PI_Garante
FROM dbo.RW_R04C0466
WHERE CAST(ISNULL(NULLIF(PI_Garante,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
