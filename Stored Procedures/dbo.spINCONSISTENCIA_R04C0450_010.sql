SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_010]


AS


BEGIN


-- Validar que el Estado del Garante corresponda a Catalogo de CNBV.


SELECT DISTINCT


 r50.CodigoCredito,


 REPLACE(r50.NombreAcreditado, ',', '') AS NombreDeudor,


 r50.CodigoGarante,


 r50.NombreGarante,


 r50.Estado


FROM dbo.RW_VW_R04C0450_INC r50


LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON r50.Estado = mun.CodigoEstado


WHERE mun.IdLocalidad IS NULL;



END
GO
