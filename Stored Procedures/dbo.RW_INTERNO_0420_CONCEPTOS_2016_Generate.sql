SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INTERNO_0420_CONCEPTOS_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INTERNO' AND Nombre = '-0420-Conceptos_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_INTERNO_0420_CONCEPTOS_2016;

INSERT INTO dbo.RW_INTERNO_0420_CONCEPTOS_2016 (
	IdReporteLog,
	Codigo,
	CodigoProducto,
	Moneda,
	Concepto,
	Monto
)
SELECT
	@IdReporteLog,
	dat.Codigo,
	dat.CodigoProducto,
	dat.Moneda,
	con.Concepto,
	con.Monto
FROM R04.[0420Datos_2016] dat
INNER JOIN R04.[0420Conceptos_2016] con ON dat.Codigo = con.Codigo
WHERE con.Monto > 0;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_INTERNO_0420_CONCEPTOS_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
