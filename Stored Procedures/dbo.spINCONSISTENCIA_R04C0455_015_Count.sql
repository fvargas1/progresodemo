SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_015_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de los Ingresos Totales Ajustados (dat_saldo_total_ajustado) es > 4
-- entonces el Puntaje Servicio de Deuda a Ingresos Totales Ajustados (cve_puntaje_serv_deuda_ing) debe ser = 24

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngTotales,''),'0') AS DECIMAL(18,6)) > 4 AND ISNULL(P_ServDeudaIngAjust,'') <> '24';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
