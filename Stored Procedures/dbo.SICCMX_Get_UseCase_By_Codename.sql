SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Get_UseCase_By_Codename]
	@Codename VARCHAR(100)
AS
SELECT
	IdUseCase,
	Active,
	Description,
	Name,
	CodeName,
	Visible,
	PageLink,
	IdUseCaseGroup,
	SortOrder,
	StringKey
FROM dbo.BAJAWARE_UseCase
WHERE CodeName = @Codename;
GO
