SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_033]
AS

BEGIN

-- Si el Puntaje Asignado por Indicador de Persona Moral o Fideicomiso (cve_ptaje_indicad_pm_fideico) es = 38,
-- entonces el Indicador de Persona Moral o Fideicomiso (dat_indicador_pm_fideico) debe ser = 0

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	IndPersFid,
	P_IndPersFid AS Puntos_IndPersFid
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_IndPersFid,'') = '38' AND ISNULL(IndPersFid,'') <> '0';

END

GO
