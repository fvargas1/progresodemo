SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_A18_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	Nombre,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	DiasMoraPromedio_V,
	DiasMoraPromedio_P,
	PorPagosInstBancarias_V,
	PorPagosInstBancarias_P,
	PorPagosInstNoBancarias_V,
	PorPagosInstNoBancarias_P,
	NumInstCalif_V,
	NumInstCalif_P,
	DeudaTotalPartEleg_V,
	DeudaTotalPartEleg_P,
	ServicioDeudaIngresosTotales_V,
	ServicioDeudaIngresosTotales_P,
	DeudaCortoPlazoDeudaTotal_V,
	DeudaCortoPlazoDeudaTotal_P,
	IngresosTotalesGastoCorr_V,
	IngresosTotalesGastoCorr_P,
	InvIngresosTotales_V,
	InvIngresosTotales_P,
	IngPropiosIngTotales_V,
	IngPropiosIngTotales_P,
	TasaDesempleoLocal_V,
	TasaDesempleoLocal_P,
	PresSerFinEntidadesReg_V,
	PresSerFinEntidadesReg_P,
	ObligContDerivadasBenef_V,
	ObligContDerivadasBenef_P,
	BalanceOperativoPIB_V,
	BalanceOperativoPIB_P,
	NivelEficienciaRec_V,
	NivelEficienciaRec_P,
	SolidezFlexEjecPresupuesto_V,
	SolidezFlexEjecPresupuesto_P,
	SolidezFlexImpLocales_V,
	SolidezFlexImpLocales_P,
	TranspFinanzasPublicas_V,
	TranspFinanzasPublicas_P,
	EmisionesBursatiles_V,
	EmisionesBursatiles_P
FROM Historico.RW_Analitico_A18
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
