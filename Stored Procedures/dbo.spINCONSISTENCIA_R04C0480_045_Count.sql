SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_045_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Porcentaje de Pagos en Tiempo con Instituciones Financiera Bancarias en los Últimos 12 meses (dat_porcent_pgo_bcos) sea >= 0 pero <= 100

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PorcPagoInstBanc AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100 AND CAST(PorcPagoInstBanc AS DECIMAL) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
