SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_035]
AS

BEGIN

-- Si la Rotación de Capital de Trabajo (dat_rotac_capital_trabajo) es > 8.81 y < 15.56,
-- entonces el Puntaje Asignado por Rotación de Capital de Trabajo (cve_ptaje_rotac_capit_trabajo) debe ser = 79

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RotCapTrabajo,
	P_RotCapTrabajo AS Puntos_RRotCapTrabajo
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(RotCapTrabajo AS DECIMAL(10,6)) > 8.81 AND CAST(RotCapTrabajo AS DECIMAL(10,6)) < 15.56 AND ISNULL(P_RotCapTrabajo,'') <> '79';

END


GO
