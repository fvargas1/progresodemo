SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_HipotecarioGarantia_errores_ListErrorLog]
AS
SELECT
	f.identificador,
	f.nombreCampo,
	f.valor,
	f.tipoError,
	f.description
FROM dbo.FILE_HipotecarioGarantia_errores f;
GO
