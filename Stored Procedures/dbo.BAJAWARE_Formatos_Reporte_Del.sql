SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Formatos_Reporte_Del]
	@IdGroup INT,
	@xml VARCHAR(MAX)
AS
DECLARE @DocHandle AS INT;

-- Create an internal representation
EXEC sys.sp_xml_preparedocument @DocHandle OUTPUT, @xml;

DELETE rel
FROM dbo.RW_Rel_Grupo_ReporteCampos rel
INNER JOIN OPENXML (@DocHandle, 'Campos/Campo',11) WITH (idRC INT) AS tmp ON rel.IdReporteCampos = tmp.idRC
WHERE rel.IdGroup = @IdGroup;

-- Remove the DOM
EXEC sys.sp_xml_removedocument @DocHandle;
GO
