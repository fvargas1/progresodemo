SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_List_ByPeriodo]
	@IdPeriodo BIGINT
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE IdPeriodo = @IdPeriodo;

SELECT
	vw.IdPeriodoHistorico,
	CONVERT( VARCHAR( 50 ), vw.Fecha, 105 ) AS Fecha,
	vw.Version,
	vw.FechaCreacion,
	vw.Username
FROM dbo.SICC_VW_PeriodoHistorico vw
WHERE vw.Fecha = @FechaPeriodo
ORDER BY vw.Fecha DESC;
GO
