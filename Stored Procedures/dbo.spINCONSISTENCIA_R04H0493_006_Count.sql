SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_006_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Los créditos que se den de baja por reestructura o bursatilización, deberán darse de Alta por el mismo motivo que origina la baja.
-- (Revisar catálogo de tipo de alta de crédito del formulario H-0491).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r91.IdReporteLog)
FROM dbo.RW_R04H0493 r93
LEFT OUTER JOIN dbo.RW_R04H0491 r91 ON r93.CodigoCreditoCNBV = r91.CodigoCreditoCNBV AND r91.TipoAlta='10'
WHERE r93.TipoBaja = '2' AND r91.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
