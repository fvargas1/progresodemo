SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0420_2016_Display]
 @IdReporteLog BIGINT
AS

WITH Reporte (Codigo, Concepto, Padre, Total, SaldoMN, SaldoUSD, SaldoUDIS, LEVEL)
AS (
 SELECT Codigo, Concepto, Padre, Total, SaldoMN, SaldoUSD, SaldoUDIS, 0 AS LEVEL
 FROM dbo.RW_R04A0420_2016_VW con
 WHERE Padre = ''
 UNION ALL
 SELECT con.Codigo, con.Concepto, con.Padre, con.Total, con.SaldoMN, con.SaldoUSD, con.SaldoUDIS, LEVEL + 1
 FROM dbo.RW_R04A0420_2016_VW con
 INNER JOIN Reporte rep ON rep.Codigo = con.Padre
)
SELECT
 rep.Codigo,
 REPLICATE(' ',LEVEL*5) + rep.Concepto Concepto,
 rep.Padre,
 rep.Total,
 rep.SaldoMN,
 rep.SaldoUSD,
 rep.SaldoUDIS,
 rep.LEVEL
FROM Reporte rep
INNER JOIN dbo.ReportWare_VW_0420Concepto_2016 vw ON vw.Codigo = rep.Codigo  
ORDER BY vw.Orden;
GO
