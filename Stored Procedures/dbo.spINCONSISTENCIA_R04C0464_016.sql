SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_016]
AS

BEGIN

-- El campo SEVERIDAD PERDIDA METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPInterna AS SP_MetodologiaInterna
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(SPInterna,''),'-1') AS DECIMAL(10,6)) < 0;

END

GO
