SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_039]
AS
BEGIN
-- Para personas morales El CURP no aplica, dejar vacío el campo.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	CURP
FROM dbo.RW_VW_R04C0463_INC
WHERE TipoCartera IN ('310','410') AND LEN(ISNULL(CURP,'')) > 0;

END

GO
