SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_088]
AS

BEGIN

-- Las claves de Actividad Económica (cve_actividad_economica) válidas para créditos otorgados al Gobierno Federal (tipo de cartera = 510) son:
-- 93111, 93121, 93131, 93141, 93151, 93161 ó 93181.

SELECT
	CodigoCredito,
	TipoCartera,
	ActEconomica
FROM dbo.RW_VW_R04C0478_INC
WHERE TipoCartera='510' AND ActEconomica NOT IN ('93111','93121','93131','93141','93151','93161','93181')

END


GO
