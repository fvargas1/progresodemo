SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_068]
AS
BEGIN
-- Un mismo RFC Acreditado (dat_rfc) no debe tener más de un ID Acreditado Asignado por la Institución (dat_id_credito_institucion) diferentes.

SELECT
	CodigoPersona,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC
FROM dbo.RW_VW_R04C0463_INC
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_VW_R04C0463_INC rep
	INNER JOIN dbo.RW_VW_R04C0463_INC rep2 ON rep.RFC = rep2.RFC AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.RFC
);

END

GO
