SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_035_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Las Reservas Totales (dat_reservas_totales) deben ser IGUALES (diferencia absoluta de $10.-)
-- a la suma de Reservas por Garantías Personales (dat_reservas_gtias_pers) y reservas por parte del acreditado (dat_reservas_acreditado).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0479
WHERE ABS(CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) - (
	CAST(ISNULL(NULLIF(ReservaCubierta,''),'0') AS DECIMAL) +
	CAST(ISNULL(NULLIF(ReservaExpuesta,''),'0') AS DECIMAL))) > 10;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
