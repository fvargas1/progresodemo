SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_097]
AS

BEGIN

-- Validar que ID Acreditado Asignado por la Institución (dat_id_acreditado_instit) se haya reportado previamente en el reporte de altas R04-C 473

SELECT
 rep.CodigoPersona AS CodigoDeudor,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor
FROM dbo.RW_VW_R04C0475_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0473_INC r73 ON rep.CodigoPersona = r73.CodigoPersona
LEFT OUTER JOIN Historico.RW_VW_R04C0473_INC hist ON rep.CodigoPersona = hist.CodigoPersona
WHERE hist.IdPeriodoHistorico IS NULL AND r73.CodigoPersona IS NULL;

END


GO
