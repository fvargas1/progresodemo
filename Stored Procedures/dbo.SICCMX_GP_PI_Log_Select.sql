SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_GP_PI_Log_Select]
	@IdAval BIGINT,
	@EsGarante INT
AS
SELECT
	IdGP,
	EsGarante,
	FechaCalculo,
	Usuario,
	Descripcion
FROM dbo.SICCMX_Persona_PI_Log_GP
WHERE IdGP = @IdAval AND EsGarante = @EsGarante
ORDER BY FechaCalculo ASC;
GO
