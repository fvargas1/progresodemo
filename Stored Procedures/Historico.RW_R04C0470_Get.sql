SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0470_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	Clasificacion,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	GarantiaLeyFederal,
	LugarRadica,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_NumInstRep,
	P_PorcPagoInstNoBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_RotActTot,
	P_RotCapTrabajo,
	P_RendCapROE,
	DiasMoraInstBanc,
	PorcPagoInstBanc,
	NumInstRep,
	PorcPagoInstNoBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	PasivoCirculante,
	UtilidadNeta,
	CapitalContable,
	ActivoTotalAnual,
	VentasNetasTotales,
	IngresosBrutosAnuales,
	RotActTot,
	ActivoCirculante,
	RotCapTrabajo,
	RendCapROE,
	P_EstEco,
	P_IntCarComp,
	P_Proveedores,
	P_Clientes,
	P_EstFinAudit,
	P_NumAgenCalif,
	P_IndepConsejoAdmon,
	P_EstOrg,
	P_CompAcc,
	P_LiqOper,
	P_UafirGastosFin
FROM Historico.RW_R04C0470
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NombrePersona;
GO
