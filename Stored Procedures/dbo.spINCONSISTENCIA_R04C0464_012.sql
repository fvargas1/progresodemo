SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_012]
AS

BEGIN

-- El campo RESERV TOTAL METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaTotalInterna
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(ReservaTotalInterna,''),'-1') AS DECIMAL) < 0;

END

GO
