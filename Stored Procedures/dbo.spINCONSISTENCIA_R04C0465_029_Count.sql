SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_029_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- Si el Puntaje Asignado por el Monto Máximo de Crédito Ototgado por Inst Bcarias en los últimos 12 meses (cve_ptaje_mont_max_cred_otorg) es = 52,

-- entonces el Monto Máximo de Crédito Otorgado por Inst Bcarias en los últimos 12 meses (dat_monto_max_cred_bco_udis) debe ser > 0 y < 1000000



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE ISNULL(P_MonMaxCred,'') = '52' AND (CAST(MonMaxCred AS DECIMAL) < 0 OR CAST(MonMaxCred AS DECIMAL) >= 1000000);



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
