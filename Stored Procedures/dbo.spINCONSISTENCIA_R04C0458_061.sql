SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_061]
AS
BEGIN
-- Si la Tasa de Referencia (cve_tasa) es igual a 560 y 600, entonces la Frecuencia de Revisión de la Tasa (dat_frecuencia_revision_tasa) debe ser igual a 0.

SELECT
	CodigoCredito,
	TipoAltaCredito,
	TasaInteres AS TasaReferencia,
	FrecuenciaRevisionTasa
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139','700','701') AND ISNULL(TasaInteres,'') IN ('560','600') AND ISNULL(FrecuenciaRevisionTasa,'') <> '0'

END
GO
