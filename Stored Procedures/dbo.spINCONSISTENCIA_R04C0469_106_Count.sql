SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_106_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Probabilidad de Incumplimiento Total (dat_probabilidad_incump) del reporte de Seguimiento,
-- sea igual a la Probabilidad Incumplimiento (dat_proba_incump) del reporte de PI. Se excluyen acreditados que tengan créditos calificados con Anexo 19.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_R04C0469 rep
INNER JOIN dbo.RW_R04C0470 rpi ON rep.CodigoPersona = rpi.CodigoPersona
WHERE ISNULL(rep.ProyectoInversion,'') = '2' AND ISNULL(rep.PITotal,'') <> ISNULL(rpi.[PI],'');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
