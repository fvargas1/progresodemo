SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_015_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "MONTO DEL PAGO EXIGIBLE AL ACREDITADO" no deberá ser mayor a lo registrado en el saldo del principal al inicio del periodo (col. 9).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(MontoPagoExigible,''),'0') AS DECIMAL) > CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
