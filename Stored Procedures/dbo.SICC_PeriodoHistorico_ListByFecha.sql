SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_ListByFecha]
	@IdPeriodo BIGINT
AS
SELECT
	vw.IdPeriodoHistorico,
	CONVERT(VARCHAR(50), vw.Fecha, 105) AS Fecha,
	vw.Version,
	vw.FechaCreacion,
	vw.Username,
	vw.Activo,
	vw.Reporte
FROM dbo.SICC_VW_PeriodoHistorico vw
WHERE vw.Fecha IN (Select Fecha FROM SICC_Periodo where IdPeriodo=@IdPeriodo)
ORDER BY vw.Fecha DESC, vw.FechaCreacion DESC;
GO
