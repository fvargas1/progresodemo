SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_CodigoRepetidoNombreAval]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_CodigoRepetidoNombreAval';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Aval f
WHERE f.NombreAval IN (
 SELECT f.NombreAval
 FROM dbo.FILE_Aval f
 GROUP BY f.NombreAval
 HAVING COUNT(f.NombreAval)>1
) AND LEN(f.NombreAval) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoAval,
 'NombreAval',
 f.NombreAval,
 1,
 @Detalle
FROM dbo.FILE_Aval f
WHERE f.NombreAval IN (
 SELECT f.NombreAval
 FROM dbo.FILE_Aval f
 GROUP BY f.NombreAval
 HAVING COUNT(f.NombreAval)>1
) AND LEN(f.NombreAval) > 0;
GO
