SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_LineaCredito_Garantia_Log]
 @IdGarantia BIGINT
AS
SELECT
 IdGarantia,
 FechaCalculo,
 Usuario,
 Descripcion
FROM dbo.SICCMX_Garantia_Log
WHERE IdGarantia = @IdGarantia
ORDER BY FechaCalculo
GO
