SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_044]
AS

BEGIN

-- Si no tiene grupo Riesgo, deberá anotarse SIN GRUPO DE RIESGO COMUN

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	GrupoRiesgo
FROM dbo.RW_VW_R04C0478_INC
WHERE LEN(ISNULL(GrupoRiesgo,'')) = 0;

END


GO
