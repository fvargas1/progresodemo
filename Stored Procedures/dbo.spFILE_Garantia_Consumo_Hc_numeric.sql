SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_Hc_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Consumo_Hc_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia_Consumo
SET errorFormato = 1
WHERE LEN(Hc)>0 AND (ISNUMERIC(ISNULL(Hc,'0')) = 0 OR Hc LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoGarantia,
	'Hc',
	Hc,
	1,
	@Detalle
FROM dbo.FILE_Garantia_Consumo
WHERE LEN(Hc)>0 AND (ISNUMERIC(ISNULL(Hc,'0')) = 0 OR Hc LIKE '%[^0-9 .]%');
GO
