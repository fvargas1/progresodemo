SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_091]
AS

BEGIN

-- Si la Moneda de la disposición (cve_moneda <> 0) en el formulario de SEGUIMIENTO es DIFERENTE a 0,
-- entonces el registro de la Moneda (cve_moneda) en el formulario de ALTAS para el mismo
-- CRÉDITO (dat_id_credito_met_cnbv) debe ser DIFERENTE a 0. (Cruce entre el reporte de seguimiento y el reporte de altas).

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.Moneda AS MonedaSeguimiento,
	vw.Moneda AS MonedaAltas
FROM dbo.RW_VW_R04C0474_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(rep.Moneda,'') <> '0' AND ISNULL(vw.Moneda,'') = '0';

END


GO
