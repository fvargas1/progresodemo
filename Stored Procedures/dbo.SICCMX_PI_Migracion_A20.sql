SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_Migracion_A20]
AS
DECLARE @IdVar INT;
DECLARE @Campo VARCHAR(50);
DECLARE @SENT VARCHAR(500);
DECLARE @IdMetodologia INT;

SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=20;

DELETE piDet
FROM dbo.SICCMX_Persona_PI_Detalles piDet
INNER JOIN dbo.SICCMX_Anexo20 anx ON piDet.IdPersona = anx.IdPersona;


-- PARA ENTIDADES FINANCIERAS ACREDITADAS, QUE SEAN A SU VEZ OTORGANTES DE CRÉDITO
DECLARE curAnexo CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=@IdMetodologia AND Campo IS NOT NULL AND (Codigo LIKE '20IA[_]%' OR Codigo LIKE '20[_]%');

OPEN curAnexo;

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles (IdPersona, IdVariable, ValorActual) ' +
'SELECT anx.IdPersona, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo20 anx ' +
'WHERE anx.EntFinAcreOtorgantesCre = 1';
EXEC(@SENT);

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

END

CLOSE curAnexo;
DEALLOCATE curAnexo;


-- PARA ENTIDADES FINANCIERAS ACREDITADAS DISTINTAS A LAS OTORGANTES DE CRÉDITO
DECLARE curAnexo2 CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=20 AND Campo IS NOT NULL AND (Codigo LIKE '20IB[_]%' OR Codigo LIKE '20[_]%');

OPEN curAnexo2;

FETCH NEXT FROM curAnexo2 INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles (IdPersona, IdVariable, ValorActual) ' +
'SELECT anx.IdPersona, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo20 anx ' +
'WHERE ISNULL(anx.EntFinAcreOtorgantesCre, 0) <> 1';
EXEC(@SENT);

FETCH NEXT FROM curAnexo2 INTO @IdVar, @Campo;

END

CLOSE curAnexo2;
DEALLOCATE curAnexo2;
GO
