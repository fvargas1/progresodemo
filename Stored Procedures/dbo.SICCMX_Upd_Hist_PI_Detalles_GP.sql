SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_Hist_PI_Detalles_GP]
AS
DECLARE @IdPeriodoActivo INT;
DECLARE @IdPeriodoHist INT;
DECLARE @FechaH1 DATETIME;
DECLARE @FechaH2 DATETIME;
DECLARE @FechaH3 DATETIME;
DECLARE @FechaH4 DATETIME;

SELECT @IdPeriodoActivo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
 @FechaH1 = CASE WHEN tb.RN = 1 THEN Fecha ELSE @FechaH1 END,
 @FechaH2 = CASE WHEN tb.RN = 2 THEN Fecha ELSE @FechaH2 END,
 @FechaH3 = CASE WHEN tb.RN = 3 THEN Fecha ELSE @FechaH3 END,
 @FechaH4 = CASE WHEN tb.RN = 4 THEN Fecha ELSE @FechaH4 END
FROM (
 SELECT TOP 4 ROW_NUMBER() OVER(ORDER BY Fecha DESC) AS RN, Fecha
 FROM dbo.SICC_Periodo
 WHERE IdPeriodo<@IdPeriodoActivo AND Trimestral=1
) AS tb;


-- ACTUALIZAMOS H1
SELECT @IdPeriodoHist=MAX(IdPeriodoHistorico) FROM dbo.SICC_PeriodoHistorico WHERE Fecha=@FechaH1 AND Activo=1;

UPDATE det
SET PuntosH1 = hst.PuntosActual,
 ValorH1 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Aval av ON det.IdGP = av.IdAval
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON av.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;

UPDATE det
SET PuntosH1 = hst.PuntosActual,
 ValorH1 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Garantia gar ON det.IdGP = gar.IdGarantia
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON gar.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;


-- ACTUALIZAMOS H2
SELECT @IdPeriodoHist=MAX(IdPeriodoHistorico) FROM dbo.SICC_PeriodoHistorico WHERE Fecha=@FechaH2 AND Activo=1;

UPDATE det
SET PuntosH2 = hst.PuntosActual,
 ValorH2 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Aval av ON det.IdGP = av.IdAval
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON av.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;

UPDATE det
SET PuntosH2 = hst.PuntosActual,
 ValorH2 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Garantia gar ON det.IdGP = gar.IdGarantia
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON gar.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;


-- ACTUALIZAMOS H3
SELECT @IdPeriodoHist=MAX(IdPeriodoHistorico) FROM dbo.SICC_PeriodoHistorico WHERE Fecha=@FechaH3 AND Activo=1;

UPDATE det
SET PuntosH3 = hst.PuntosActual,
 ValorH3 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Aval av ON det.IdGP = av.IdAval
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON av.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;

UPDATE det
SET PuntosH3 = hst.PuntosActual,
 ValorH3 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Garantia gar ON det.IdGP = gar.IdGarantia
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON gar.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;


-- ACTUALIZAMOS H4
SELECT @IdPeriodoHist=MAX(IdPeriodoHistorico) FROM dbo.SICC_PeriodoHistorico WHERE Fecha=@FechaH4 AND Activo=1;

UPDATE det
SET PuntosH4 = hst.PuntosActual,
 ValorH4 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Aval av ON det.IdGP = av.IdAval
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON av.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;

UPDATE det
SET PuntosH4 = hst.PuntosActual,
 ValorH4 = hst.ValorActual
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_Garantia gar ON det.IdGP = gar.IdGarantia
INNER JOIN dbo.SICC_EsGarante esg ON det.EsGarante = esg.IdEsGarante
INNER JOIN dbo.SICCMX_PI_Variables piv ON det.IdVariable = piv.Id
INNER JOIN Historico.SICCMX_Persona_PI_Detalles_GP hst ON gar.Codigo=hst.GP AND esg.Codigo=hst.EsGarante AND piv.Codigo=hst.Variable AND hst.IdPeriodoHistorico=@IdPeriodoHist;
GO
