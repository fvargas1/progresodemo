SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Archivos_ClearData]
AS
DECLARE @IdArchivo BIGINT;
DECLARE @Tabla VARCHAR(200);

DECLARE CursorArchivos CURSOR FOR
SELECT IdArchivo, Codename
FROM dbo.MIGRACION_Archivo
WHERE Activo=1
ORDER BY IdArchivo ASC;

OPEN CursorArchivos;

FETCH NEXT FROM CursorArchivos INTO @IdArchivo, @Tabla;

WHILE @@FETCH_STATUS = 0
BEGIN

EXEC ('TRUNCATE TABLE '+ @Tabla);

FETCH NEXT FROM CursorArchivos INTO @IdArchivo, @Tabla;

END
CLOSE CursorArchivos;
DEALLOCATE CursorArchivos;


EXEC dbo.SICCMX_Consumo_DeleteAll;
EXEC dbo.SICCMX_Hipotecario_DeleteAll;
EXEC dbo.SICCMX_Credito_DeleteAll;
EXEC dbo.SICCMX_Adjudicado_DeleteAll;
GO
