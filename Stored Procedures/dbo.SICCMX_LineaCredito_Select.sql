SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_LineaCredito_Select]
	@IdPersona BIGINT
AS
SELECT DISTINCT
 0 AS IdLineaCredito,
 '' AS NumeroLinea,
 0 AS PIAval,
 0 AS Exposicion,
 0 AS ExpCubierta,
 0 AS ExpExpuesta,
 0 AS SPExpuesta,
 0 AS SPCubierta,
 0 AS SeveridadPerdida,
 0 AS Reserva,
 0 AS PorReserva,
 '' AS Calificacion
FROM dbo.BAJAWARE_Config
WHERE 1=2
/*
SELECT DISTINCT
 ln.IdLineaCredito,
 ln.Codigo AS NumeroLinea,
 ISNULL(lnv.PIAval,0) AS PIAval,
 ISNULL(lnv.Exposicion,0) AS Exposicion,
 ISNULL(lnv.ExpCubierta,0) AS ExpCubierta,
 ISNULL(lnv.ExpExpuesta,0) AS ExpExpuesta,
 ISNULL(lnv.SPExpuesta,0) AS SPExpuesta,
 ISNULL(lnv.SPCubierta,0) AS SPCubierta,
 ISNULL(lnv.SeveridadPerdida,0) AS SeveridadPerdida,
 ISNULL(lnv.ReservaFinal, 0) AS Reserva,
 ISNULL(lnv.PorReservaFinal,0) AS PorReserva,
 calif.Codigo AS Calificacion
FROM dbo.SICCMX_LineaCredito ln
INNER JOIN dbo.SICCMX_Persona_PI ppi ON ln.IdPersona = ppi.IdPersona
LEFT OUTER JOIN dbo.SICCMX_LineaCredito_Reservas_Variables lnv ON lnv.IdLineaCredito = ln.IdLineaCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Credito_NMC cr ON ln.IdLineaCredito = cr.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calif ON calif.IdCalificacion = lnv.CalifFinal
WHERE ln.IdPersona = @IdPersona AND lnv.Exposicion > 0
ORDER BY ln.Codigo;
*/
GO
