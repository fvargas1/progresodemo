SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_054_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el ID Metodología CNBV (dat_id_credito_met_cnbv) no tenga más de un Nombre Acreditado (dat_nombre).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_VW_R04C0458_INC rep
	INNER JOIN dbo.RW_VW_R04C0458_INC rep2 ON rep.CodigoCreditoCNBV = rep2.CodigoCreditoCNBV AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.CodigoCreditoCNBV
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
