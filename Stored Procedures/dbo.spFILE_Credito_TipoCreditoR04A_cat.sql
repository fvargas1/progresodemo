SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_TipoCreditoR04A_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_TipoCreditoR04A_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 rst ON LTRIM(f.TipoCreditoR04A) = rst.Codigo
WHERE rst.IdTipoProducto IS NULL AND LEN(f.TipoCreditoR04A) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'TipoCreditoR04A',
 f.TipoCreditoR04A,
 2,
 @Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 rst ON LTRIM(f.TipoCreditoR04A) = rst.Codigo
WHERE rst.IdTipoProducto IS NULL AND LEN(f.TipoCreditoR04A) > 0;
GO
