SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_118]
AS

BEGIN

-- Validar que el ID Metodología CNBV (dat_id_credito_met_cnbv) no tenga más de un Nombre Acreditado (dat_nombre).

SELECT
	CodigoCredito,
	NombrePersona,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0463_INC
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_VW_R04C0463_INC rep
	INNER JOIN dbo.RW_VW_R04C0463_INC rep2 ON rep.CodigoCreditoCNBV = rep2.CodigoCreditoCNBV AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.CodigoCreditoCNBV
);

END

GO
