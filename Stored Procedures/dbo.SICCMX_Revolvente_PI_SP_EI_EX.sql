SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Revolvente_PI_SP_EI_EX]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo = '5';

UPDATE crv
SET Tipo_EX = 0
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON crv.IdConsumo = pre.IdConsumo AND pre.IdMetodologia = @IdMetodologia;

-- EXCEPCION 3.i
UPDATE crv
SET [PI] = 0.0418,
 SPExpuesta = 0.67,
 E = con.SaldoTotalValorizado + (1.00 * (pre.LimiteCredito - con.SaldoTotalValorizado)),
 Tipo_EX = 3.1
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Consumo con ON crv.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
INNER JOIN (
 SELECT vw.IdConsumo
 FROM dbo.SICCMX_VW_Consumo_Var_Historico vw
 WHERE vw.RN <= 4
 GROUP BY vw.IdConsumo
 HAVING SUM(ISNULL(vw.SaldoPagar,0)) <= 0 AND SUM(ISNULL(vw.PagoRealizado,0)) <= 0
) ex ON pre.IdConsumo = ex.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;


-- EXCEPCION 3.ii
UPDATE crv
SET [PI] =
 CASE
 WHEN pre.Alto = 1 THEN 0.0466
 WHEN pre.Medio = 1 THEN 0.0344
 WHEN pre.Bajo = 1 THEN 0.0218
 END,
 SPExpuesta = 0.7,
 E = con.SaldoTotalValorizado + (0.4267 * (pre.LimiteCredito - con.SaldoTotalValorizado)),
 Tipo_EX = 3.2
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Consumo con ON crv.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
INNER JOIN (
 SELECT vw.IdConsumo
 FROM dbo.SICCMX_VW_Consumo_Var_Historico vw
 WHERE vw.RN <= 4
 GROUP BY vw.IdConsumo
 HAVING SUM(ISNULL(vw.SaldoPagar,0)) <= 0 AND SUM(ISNULL(vw.PagoRealizado,0)) > 0
) ex ON pre.IdConsumo = ex.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;


-- EXCEPCION 4
UPDATE crv
SET [PI] =
 CASE
 WHEN pre.Alto = 1 THEN 0.0870
 WHEN pre.Medio = 1 THEN 0.0579
 WHEN pre.Bajo = 1 THEN 0.0312
 END,
 SPExpuesta = 0.7,
 E = con.SaldoTotalValorizado + (0.5181 * (pre.LimiteCredito - con.SaldoTotalValorizado)),
 Tipo_EX = 4
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Consumo con ON crv.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
INNER JOIN (
 SELECT vw.IdConsumo
 FROM dbo.SICCMX_VW_Consumo_Var_Historico vw
 WHERE vw.RN BETWEEN 2 AND 4
 GROUP BY vw.IdConsumo
 HAVING SUM(ISNULL(vw.SaldoPagar,0)) > 0
) ex ON pre.IdConsumo = ex.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia AND ISNULL(info.MontoExigible,0) <= 0;


-- SE ACTUALIZA LA EXPOSICION A "0" PARA BAJAS DEL MES
UPDATE crv
SET E = 0
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON crv.IdConsumo = inf.IdConsumo
WHERE inf.IdTipoBaja IS NOT NULL;
GO
