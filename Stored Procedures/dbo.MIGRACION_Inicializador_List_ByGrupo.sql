SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inicializador_List_ByGrupo]
	@Grupo VARCHAR(100)
AS

SET @Grupo = NULLIF(@Grupo,'');

SELECT
	vw.IdInicializador,
	vw.Description,
	vw.Codename,
	vw.Position
FROM dbo.MIGRACION_VW_Inicializador vw
WHERE (@Grupo IS NULL OR vw.GrupoArchivo = @Grupo) AND vw.Activo = 1
ORDER BY vw.Position;
GO
