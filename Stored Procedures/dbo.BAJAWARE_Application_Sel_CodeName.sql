SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Application_Sel_CodeName]
	@CodeName BAJAWARE_utDescripcion100
AS
SELECT
IdApplication, Active, Description, [Name], CodeName, Folder, HomePage, SortOrder, StringKey, Version
FROM dbo.BAJAWARE_Application
WHERE CodeName = @CodeName;
GO
