SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_030]
AS
BEGIN
-- La Fecha de la Disposición del Crédito deberá ser menor que Fecha de Vencimiento de la disposición del crédito.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion,
	FechaVencDisposicion
FROM dbo.FechaVencDisposicion
WHERE FechaDisposicion >= FechaVencDisposicion;

END
GO
