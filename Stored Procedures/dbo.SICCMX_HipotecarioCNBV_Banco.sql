SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioCNBV_Banco]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

UPDATE dbo.SICCMX_HipotecarioCNBV
SET CNBV = info.CodigoCNBV
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_HipotecarioCNBV cnbv ON hip.Codigo = cnbv.Codigo
WHERE LEN(info.CodigoCNBV) > 0;
GO
