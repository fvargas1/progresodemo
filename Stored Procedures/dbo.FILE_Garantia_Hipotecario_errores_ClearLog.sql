SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Garantia_Hipotecario_errores_ClearLog]
AS
UPDATE dbo.FILE_Garantia_Hipotecario
SET
errorFormato = NULL,
errorCatalogo = NULL
WHERE errorFormato IS NOT NULL OR errorCatalogo IS NOT NULL;

TRUNCATE TABLE dbo.FILE_Garantia_Hipotecario_errores;
GO
