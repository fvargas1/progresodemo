SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_074_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Cuentas o Créditos Abiertos con Inst Financ Bcarias en los últimos 12 meses (cve_ptaje_cuentas_con_bcos) es = 16,
-- entonces las Cuentas o Créditos Abiertos con Inst Financ Bcarias en los últimos 12 meses (dat_ctas_abiertas_bancos) debe ser >= 8

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_CredAbInstBanc,'') = '16' AND CAST(CredAbInstBanc AS DECIMAL) < 8;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
