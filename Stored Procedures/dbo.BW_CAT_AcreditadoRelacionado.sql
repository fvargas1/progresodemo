SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_AcreditadoRelacionado]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	CAST(cat.Codigo AS INT) AS Codigo,
	cat.Nombre,
	cat.CodigoCNBV,
	cat.CodigoBanxico,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_DeudorRelacionado cat
LEFT OUTER JOIN dbo.FILE_Persona_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'TipoAcreditadoRelacionado' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'')+'|'+ISNULL(cat.CodigoCNBV,'')+'|'+ISNULL(cat.CodigoBanxico,'') LIKE '%' + @filtro + '%'
ORDER BY CAST(cat.Codigo AS INT);
GO
