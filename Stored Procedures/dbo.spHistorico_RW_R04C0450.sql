SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0450]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0450);

DELETE FROM Historico.RW_R04C0450 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0450 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, RFC_Garante, NombreGarante, CodigoGarante, ActEconomica, Localidad, Municipio, Estado,
	LEI, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, NombreAcreditado, TipoGarantia, CodigoGarantia, MonedaGarantia,
	MontoGarantia, PrctGarantia
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM dbo.RW_R04C0450
WHERE IdReporteLog = @IdReporteLog;
GO
