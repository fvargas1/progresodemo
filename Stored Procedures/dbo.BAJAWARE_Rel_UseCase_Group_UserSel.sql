SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_UseCase_Group_UserSel]
	@IdUser BAJAWARE_utID,
	@CodeName BAJAWARE_utDescripcion100
AS
SELECT rel.IdRel
FROM dbo.BAJAWARE_Rel_UseCase_Group rel
INNER JOIN dbo.BAJAWARE_UseCase uc ON rel.IdUseCase = uc.IdUseCase
INNER JOIN dbo.BAJAWARE_Group g ON rel.IdGroup = g.IdGroup
LEFT OUTER JOIN dbo.BAJAWARE_Rel_Group_User usr ON g.IdGroup = usr.IdGroup
WHERE uc.CodeName = @CodeName AND usr.IdUser = @IdUser;
GO
