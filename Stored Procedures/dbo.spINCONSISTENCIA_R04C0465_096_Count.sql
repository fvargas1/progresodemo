SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_096_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Puntaje Crediticio Cuantitativo (dat_ptaje_credit_cuantit) sea >= 204 pero <=913

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCuantitativo,''),'-1') AS DECIMAL) NOT BETWEEN 204 AND 913;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
