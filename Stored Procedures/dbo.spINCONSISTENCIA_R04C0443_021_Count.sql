SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_021_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La tasa de interés bruta del periodo debe ser > 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(TasaInteres,''),'0') AS DECIMAL(10,2)) <= 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
