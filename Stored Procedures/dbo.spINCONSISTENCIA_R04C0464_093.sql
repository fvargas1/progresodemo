SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_093]
AS

BEGIN

-- Si la Moneda de la disposición es diferente a Pesos (cve_moneda <> 0), validar que para el crédito (dat_id_credito_met_cnbv)
-- que se reporta el Saldo del Principal al Inicio del Periodo (dat_saldo_princ_iniciot) actual no tenga una diferencia absoluta
-- mayor al 10% respecto al Saldo del Principal al Final del Periodo (dat_saldo_princ_finalt-1)del mes anterior.

DECLARE @IdPeriodo INT;

SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Fecha = (SELECT DATEADD(MONTH,-1,Fecha) FROM dbo.SICC_Periodo WHERE Activo=1);

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.Moneda,
	rep.SaldoInicial,
	hst.SaldoFinal
FROM dbo.RW_VW_R04C0464_INC rep
INNER JOIN Historico.RW_VW_R04C0464_INC hst ON rep.CodigoCreditoCNBV = hst.CodigoCreditoCNBV AND hst.Periodo = @IdPeriodo
WHERE ISNULL(rep.Moneda,'') <> '0'
	AND ABS(CAST(ISNULL(NULLIF(rep.SaldoInicial,''),'0') AS DECIMAL) - CAST(ISNULL(NULLIF(hst.SaldoFinal,''),'0') AS DECIMAL)) > CAST(ISNULL(NULLIF(hst.SaldoFinal,''),'0') AS DECIMAL) * 0.1

END

GO
