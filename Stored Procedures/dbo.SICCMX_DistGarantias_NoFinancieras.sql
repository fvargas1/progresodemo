SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_NoFinancieras]
AS
-- GARANTIAS REALES NO FINANCIERAS (CREDITO)
UPDATE can
SET
 C = vw.ValorGarantia,
 PrctCobSinAju = CASE WHEN cre.EI_Total > 0 THEN CAST(vw.ValorGarantia / cre.EI_Total AS DECIMAL(18,8)) ELSE 0 END,
 PrctCobAjust = CASE WHEN cre.EI_Total > 0 THEN dbo.MIN2VAR(1, (CAST(vw.ValorGarantia / cre.EI_Total AS DECIMAL(18,10)) / (vw.NivelMaximo / 100))) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(cre.EI_Total, (vw.ValorGarantia / (vw.NivelMaximo / 100))),
 [PI] = ppi.[PI]
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cre.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_Garantias_RNF vw ON can.IdCredito = vw.IdCredito AND vw.IdTipoGarantia=can.IdTipoGarantia;
GO
