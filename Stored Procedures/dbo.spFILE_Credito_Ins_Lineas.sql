SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_Ins_Lineas]
AS
SET NOCOUNT ON;
INSERT INTO dbo.SICCMX_LineaCredito (Codigo, Fuente)
SELECT DISTINCT LTRIM(RTRIM(f.NumeroLinea)), 'CR'
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
WHERE lin.IdLineaCredito IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;

SET NOCOUNT OFF;
UPDATE lin
SET
 IdPersona = fl.IdPersona,
 FechaDisposicion = fl.FechaDisposicion,
 IdMoneda = fl.IdMoneda,
 IdDestino = fl.IdDestino,
 IdTasaReferencia = fl.IdTasaReferencia,
 MontoLinea = fl.MontoLinea,
 FrecuenciaRevisionTasa = fl.FrecuenciaRevisionTasa,
 IdTipoAlta = fl.IdTipoAlta,
 IdDisposicion = fl.IdDisposicion,
 AjusteTasaReferencia = fl.AjusteTasa,
 IdInstitucionFondea = fl.IdInstitucion,
 ComisionesCobradas = fl.ComisionesCobradas,
 IdTipoBaja = fl.IdTipoBaja,
 ImporteQuebranto = fl.ImporteQuebranto,
 SaldoInicial = gpLinea.SaldoInicial,
 IdPeriodicidadCapital = fl.IdPeriodicidadCapital,
 IdPeriodicidadInteres = fl.IdPeriodicidadInteres,
 CodigoCreditoReestructurado = fl.CodigoCreditoReestructurado,
 ProductoComercial = fl.IdTipoCredito,
 Posicion = fl.IdPosicion,
 TipoLinea = fl.IdTipoLinea,
 TipoOperacion = fl.IdTipoOperacion,
 FecMaxDis = fl.FecMaxDis,
 PorcPartFederal = fl.PorcPartFederal,
 MontoPagEfeCap = gpLinea.MontoPagEfeCap,
 MontoPagEfeInt = gpLinea.MontoPagEfeInt,
 MontoPagEfeCom = gpLinea.MontoPagEfeCom,
 MontoPagEfeIntMor = gpLinea.MontoPagEfeIntMor,
 ComDispTasa = fl.ComDispTasa,
 ComDispMonto = fl.ComDispMonto,
 GastosOrigTasa = fl.GastosOrigTasa,
 ResTotalInicioPer = gpLinea.ResTotalInicioPer,
 MontoQuitasCastQue = gpLinea.MontoQuitasCastQue,
 MontoBonificacionDesc = gpLinea.MontoBonificacionDesc,
 MesesGraciaCap = fl.MesesGraciaCap,
 MesesGraciaIntereses = fl.MesesGraciaIntereses,
 NumEmpLoc = fl.NumEmpLoc,
 NumEmpLocFedSHCP = fl.NumEmpLocFedSHCP,
 FecVenLinea = fl.FecVenLinea,
 CAT = fl.CAT,
 MontoLineaSinA = fl.MontoLineaSinA,
 MontoPrimasAnuales = fl.MontoPrimasAnuales,
 EsPadre = fl.EsPadre,
 IdMunicipioDestino = fl.IdMunicipio,
 IdActividadEconomicaDestino = fl.IdActividadEconomica,
 FolioConsultaBuro = fl.FolioConsultaBuro,
 EsMultimoneda = fl.EsMultimoneda,
 IdDestinoCreditoMA = fl.IdDestinoCreditoMA,
 IdTipoAltaMA = fl.IdTipoAltaMA,
 IdDisposicionMA = fl.IdDisposicionMA,
 IdPeriodicidadCapitalMA = fl.IdPeriodicidadCapitalMA,
 IdPeriodicidadInteresMA = fl.IdPeriodicidadInteresMA,
 IdTipoBajaMA = fl.IdTipoBajaMA,
 NumeroAgrupacion = fl.NumeroAgrupacion,
 MontoBancaDesarrollo = fl.ImporteApoyoBancaDesarrollo
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN (
 SELECT LTRIM(fc.NumeroLinea) AS NumeroLinea,
 SUM(CAST(ISNULL(NULLIF(fc.SaldoInicial,''),'0') AS DECIMAL(23,2))) AS SaldoInicial,
 SUM(CAST(ISNULL(NULLIF(fc.ResTotalInicioPer,''),'0') AS DECIMAL(23,2))) AS ResTotalInicioPer,
 SUM(CAST(ISNULL(NULLIF(fc.MontoPagEfeCap,''),'0') AS DECIMAL(23,2))) AS MontoPagEfeCap,
 SUM(CAST(ISNULL(NULLIF(fc.MontoPagEfeInt,''),'0') AS DECIMAL(23,2))) AS MontoPagEfeInt,
 SUM(CAST(ISNULL(NULLIF(fc.MontoPagEfeCom,''),'0') AS DECIMAL(23,2))) AS MontoPagEfeCom,
 SUM(CAST(ISNULL(NULLIF(fc.MontoPagEfeIntMor,''),'0') AS DECIMAL(23,2))) AS MontoPagEfeIntMor,
 SUM(CAST(ISNULL(NULLIF(fc.MontoQuitasCastQue,''),'0') AS DECIMAL(23,2))) AS MontoQuitasCastQue,
 SUM(CAST(ISNULL(NULLIF(fc.MontoBonificacionDesc,''),'0') AS DECIMAL(23,2))) AS MontoBonificacionDesc
 FROM dbo.FILE_Credito fc
 GROUP BY fc.NumeroLinea
) AS gpLinea ON lin.Codigo = gpLinea.NumeroLinea
CROSS APPLY (
 SELECT TOP 1
 per.IdPersona AS IdPersona,
 NULLIF(f.FechaDisposicion,'') AS FechaDisposicion,
 mon.IdMoneda AS IdMoneda,
 dest.IdDestino AS IdDestino,
 tasaRef.IdTasaReferencia AS IdTasaReferencia,
 NULLIF(f.MontoLinea, '') AS MontoLinea,
 NULLIF(f.FrecuenciaRevisionTasa, '') AS FrecuenciaRevisionTasa,
 tipoAl.IdTipoAlta AS IdTipoAlta,
 dispCre.IdDisposicion AS IdDisposicion,
 LTRIM(RTRIM(f.AjusteTasa)) AS AjusteTasa,
 inst.IdInstitucion AS IdInstitucion,
 NULLIF(f.ComisionesCobradas, '') AS ComisionesCobradas,
 tipoBa.IdTipoBaja AS IdTipoBaja,
 NULLIF(f.ImporteQuebranto, '') AS ImporteQuebranto,
 perCap.IdPeriodicidadCapital AS IdPeriodicidadCapital,
 perInt.IdPeriodicidadInteres AS IdPeriodicidadInteres,
 LTRIM(RTRIM(f.CodigoCreditoReestructurado)) AS CodigoCreditoReestructurado,
 tcc.IdTipoCredito AS IdTipoCredito,
 pos.IdPosicion AS IdPosicion,
 tln.IdTipoLinea AS IdTipoLinea,
 tpoOper.IdTipoOperacion AS IdTipoOperacion,
 NULLIF(f.FecMaxDis, '') AS FecMaxDis,
 NULLIF(f.PorcPartFederal, '') AS PorcPartFederal,
 NULLIF(f.ComDispTasa, '') AS ComDispTasa,
 NULLIF(f.ComDispMonto, '') AS ComDispMonto,
 NULLIF(f.GastosOrigTasa, '') AS GastosOrigTasa,
 CAST(NULLIF(f.MesesGraciaCap, '') AS DECIMAL) AS MesesGraciaCap,
 CAST(NULLIF(f.MesesGraciaIntereses, '') AS DECIMAL) AS MesesGraciaIntereses,
 LTRIM(RTRIM(f.NumEmpLoc)) AS NumEmpLoc,
 LTRIM(RTRIM(f.NumEmpLocFedSHCP)) AS NumEmpLocFedSHCP,
 NULLIF(f.FecVenLinea, '') AS FecVenLinea,
 NULLIF(f.CAT, '') AS CAT,
 NULLIF(f.MontoLineaSinA, '') AS MontoLineaSinA,
 NULLIF(f.MontoPrimasAnuales, '') AS MontoPrimasAnuales,
 LTRIM(RTRIM(f.EsPadre)) AS EsPadre,
 (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.MunicipioDestino)) AS IdMunicipio,
 actEco.IdActividadEconomica AS IdActividadEconomica,
 LTRIM(RTRIM(f.FolioConsultaBuro)) AS FolioConsultaBuro,
 LTRIM(RTRIM(f.EsMultimoneda)) AS EsMultimoneda,
 destMA.IdDestinoCredito AS IdDestinoCreditoMA,
 tipoAlMA.IdTipoAlta AS IdTipoAltaMA,
 dispCreMA.IdDisposicion AS IdDisposicionMA,
 perCapMA.IdPeriodicidadCapital AS IdPeriodicidadCapitalMA,
 perIntMA.IdPeriodicidadInteres AS IdPeriodicidadInteresMA,
 tipoBaMA.IdTipoBaja AS IdTipoBajaMA,
 LTRIM(RTRIM(f.NumeroAgrupacion)) AS NumeroAgrupacion,
 NULLIF(f.ImporteApoyoBancaDesarrollo, '') AS ImporteApoyoBancaDesarrollo
 FROM dbo.FILE_Credito f
 INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
 LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
 LEFT OUTER JOIN dbo.SICC_Destino dest ON LTRIM(f.DestinoCredito) = dest.Codigo
 LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON LTRIM(f.TasaReferencia) = tasaRef.Codigo
 LEFT OUTER JOIN dbo.SICC_TipoAlta tipoAl ON LTRIM(f.TipoAlta) = tipoAl.Codigo
 LEFT OUTER JOIN dbo.SICC_DisposicionCredito dispCre ON LTRIM(f.DisposicionCredito) = dispCre.Codigo
 LEFT OUTER JOIN dbo.SICC_Institucion inst ON LTRIM(f.InstitutoFondea) = inst.Codigo
 LEFT OUTER JOIN dbo.SICC_TipoBaja tipoBa ON LTRIM(f.TipoBaja) = tipoBa.Codigo
 LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON LTRIM(f.PeriodicidadCapital) = perCap.Codigo
 LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON LTRIM(f.PeriodicidadInteres) = perInt.Codigo
 LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON LTRIM(f.ProductoComercial) = tcc.Codigo
 LEFT OUTER JOIN dbo.SICC_Posicion pos ON LTRIM(f.Posicion) = pos.Codigo
 LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON LTRIM(f.TipoLinea) = tln.Codigo
 LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON LTRIM(f.TipoOperacion) = tpoOper.Codigo
 LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON LTRIM(f.ActividadEconomicaDestino) = actEco.Codigo
 LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON destMA.Codigo = LTRIM(f.DestinoCreditoMA)
 LEFT OUTER JOIN dbo.SICC_TipoAltaMA tipoAlMA ON tipoAlMA.Codigo = LTRIM(f.TipoAltaMA)
 LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dispCreMA ON dispCreMA.Codigo = LTRIM(f.DisposicionCreditoMA)
 LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCapMA ON perCapMA.Codigo = LTRIM(f.PeriodicidadCapitalMA)
 LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perIntMA ON perIntMA.Codigo = LTRIM(f.PeriodicidadInteresMA)
 LEFT OUTER JOIN dbo.SICC_TipoBajaMA tipoBaMA ON tipoBaMA.Codigo = LTRIM(f.TipoBajaMA)
 WHERE lin.Codigo = LTRIM(f.NumeroLinea) AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL
 ORDER BY CAST(NULLIF(f.NumeroDisposicion,'') AS INT)
) AS fl
WHERE lin.Fuente = 'CR';
GO
