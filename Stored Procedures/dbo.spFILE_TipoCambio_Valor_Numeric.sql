SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_TipoCambio_Valor_Numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_TipoCambio_Valor_Numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_TipoCambio
SET errorFormato = 1
WHERE ISNUMERIC(ISNULL(ValorTipoCambio,'0')) = 0 OR ValorTipoCambio LIKE '%[^0-9 .]%';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_TipoCambio_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoMonedaISO,
 'ValorTipoCambio',
 ValorTipoCambio,
 1,
 @Detalle
FROM dbo.FILE_TipoCambio
WHERE ISNUMERIC(ISNULL(ValorTipoCambio,'0')) = 0 OR ValorTipoCambio LIKE '%[^0-9 .]%';
GO
