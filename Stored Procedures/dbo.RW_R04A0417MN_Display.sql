SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0417MN_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (Codigo, Concepto, Padre, SaldoBase, Estimacion, LEVEL)
AS (
	SELECT
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.SaldoBase,
	con.Estimacion,
	0 AS LEVEL
	FROM dbo.RW_R04A0417MN_VW_Consolidado con
	WHERE Padre = ''
	UNION ALL
	SELECT
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.SaldoBase,
	con.Estimacion,
	LEVEL + 1
	FROM dbo.RW_R04A0417MN_VW_Consolidado con
	INNER JOIN Reporte rep ON con.Padre = rep.Codigo
)
SELECT
	rep.Codigo,
	REPLICATE(' ', LEVEL*5)+ Concepto AS Concepto,
	rep.Padre,
	SaldoBase,
	Estimacion,
	LEVEL
FROM Reporte rep
INNER JOIN dbo.ReportWare_VW_0417Concepto vw ON vw.Codigo = rep.Codigo
ORDER BY vw.Orden;
GO
