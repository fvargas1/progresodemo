SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_INTERNO_0420_CONCEPTOS_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_INTERNO_0420_CONCEPTOS_2016);

DELETE FROM Historico.RW_INTERNO_0420_CONCEPTOS_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_INTERNO_0420_CONCEPTOS_2016 (
	IdPeriodoHistorico,
	Codigo,
	CodigoProducto,
	Moneda,
	Concepto,
	Monto
)
SELECT
	@IdPeriodoHistorico,
	Codigo,
	CodigoProducto,
	Moneda,
	Concepto,
	Monto
FROM dbo.RW_INTERNO_0420_CONCEPTOS_2016
WHERE IdReporteLog = @IdReporteLog;
GO
