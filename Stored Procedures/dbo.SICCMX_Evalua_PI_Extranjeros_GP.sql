SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Evalua_PI_Extranjeros_GP]
AS

UPDATE ppi
SET [PI] = 0.005
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON aval.IdAval = ppi.IdGP
INNER JOIN dbo.SICC_Localidad2015 mun ON aval.IdMunAval = mun.IdLocalidad
INNER JOIN (
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22_GP
) AS cal ON ppi.IdGP = cal.IdGp AND ppi.EsGarante = cal.EsGarante
WHERE mun.CodigoPais <> '484' AND cal.CalCredAgenciaCal = 1;

UPDATE ppi
SET [PI] = 0.005
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON gar.IdGarantia = ppi.IdGP
INNER JOIN dbo.SICC_Localidad2015 mun ON gar.IdLocalidadGarante = mun.IdLocalidad
INNER JOIN (
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22_GP
) AS cal ON ppi.IdGP = cal.IdGp AND ppi.EsGarante = cal.EsGarante
WHERE mun.CodigoPais <> '484' AND cal.CalCredAgenciaCal = 1;


-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Se actualizó la PI al 0.5% ya que es extranjero y cuenta con una calificación de crédito emitida por una agencia calificadora según el Anexo 1-B de la CUB'
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON aval.IdAval = ppi.IdGP
INNER JOIN dbo.SICC_Localidad2015 mun ON aval.IdMunAval = mun.IdLocalidad
INNER JOIN (
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22_GP
) AS cal ON ppi.IdGP = cal.IdGp AND ppi.EsGarante = cal.EsGarante
WHERE mun.CodigoPais <> '484' AND cal.CalCredAgenciaCal = 1;

INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Se actualizó la PI al 0.5% ya que es extranjero y cuenta con una calificación de crédito emitida por una agencia calificadora según el Anexo 1-B de la CUB'
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON gar.IdGarantia = ppi.IdGP
INNER JOIN dbo.SICC_Localidad2015 mun ON gar.IdLocalidadGarante = mun.IdLocalidad
INNER JOIN (
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21_GP
 UNION ALL
 SELECT IdGP, EsGarante, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22_GP
) AS cal ON ppi.IdGP = cal.IdGp AND ppi.EsGarante = cal.EsGarante
WHERE mun.CodigoPais <> '484' AND cal.CalCredAgenciaCal = 1;
GO
