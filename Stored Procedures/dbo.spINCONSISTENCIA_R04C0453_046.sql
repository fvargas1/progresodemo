SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_046]
AS
BEGIN
-- Las claves de Actividad Económica (cve_actividad_economica) válidas para los formularios de créditos otorgados a
-- Estados y Municipios son: 93122, 93123, 93132, 93133, 93142, 93143, 93152, 93153, 93162, 93163.

SELECT
	CodigoCredito,
	ActEconomica
FROM dbo.RW_VW_R04C0453_INC
WHERE ActEconomica NOT IN ('93122','93123','93132','93133','93142','93143','93152','93153','93162','93163')

END
GO
