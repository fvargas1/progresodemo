SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_Migracion]
AS

UPDATE cre
SET PorcentajeFondeo = NULLIF(f.PorcentajeGtiaFondos, ''),
 SaldoCapitalVigente = NULLIF(f.SaldoCapitalVigente, ''),
 InteresVigente = NULLIF(f.SaldoInteresVigente, ''),
 SaldoCapitalVencido = NULLIF(f.SaldoCapitalVencido, ''),
 InteresVencido = NULLIF(f.SaldoInteresVencido, ''),
 InteresRefinanciado = NULLIF(f.InteresRefinanciado, ''),
 FechaTraspasoVigente = NULLIF(f.FechaTraspasoVigente, ''),
 FechaTraspasoVencida = NULLIF(f.FechaTraspasoVencido, ''),
 FechaProximaAmortizacion = NULLIF(f.FechaProximaAmortizacion, ''),
 FechaAutorizacionOriginal = NULLIF(f.FechaAutorizacionOriginal, ''),
 MontoOriginal = NULLIF(f.MontoOriginal, ''),
 IdReestructura = rees.Clasificacion,
 IdSituacionCredito = sitCre.IdSituacionCredito,
 IdPersona = per.IdPersona,
 IdClasificacionContable = clasCon.IdClasificacionContable,
 IdMoneda = mon.IdMoneda,
 IdDestino = dest.IdDestino,
 IdTasaReferencia = tasaRef.IdTasaReferencia,
 TasaBruta = NULLIF(f.TasaInteresBruta, ''),
 MontoLineaCredito = NULLIF(f.MontoLinea, ''),
 NumeroAgrupacion = f.NumeroAgrupacion,
 FechaDisposicion = NULLIF(f.FechaDisposicion, ''),
 FechaVencimiento = NULLIF(f.FechaVencimiento, ''),
 FrecuenciaRevisionTasa = NULLIF(f.FrecuenciaRevisionTasa, ''),
 IdLineaCredito = lin.IdLineaCredito,
 NumeroReestructura = f.NumeroReestructura,
 Formalizda = NULLIF(f.OperacionFormalizada, ''),
 Recuperacion = NULLIF(f.InformacionRecuperacion, ''),
 Expediente = NULLIF(f.ExpedienteCompleto, ''),
 MesesIncumplimiento = NULLIF(f.MesesVencidos, ''),
 IdMetodologia = met.Codigo,
 IdFondeo = fond.IdFondeo,
 InteresCarteraVencida = NULLIF(f.InteresCarteraVencida, ''),
 EsMultimoneda = NULLIF(f.EsMultimoneda,''),
 IdDestinoCreditoMA = destMA.IdDestinoCredito
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.FILE_Credito f ON cre.Codigo = LTRIM(f.CodigoCredito)
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
INNER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAlta rees ON rees.Codigo = LTRIM(f.TipoAlta)
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON sitCre.Codigo = LTRIM(f.SituacionCredito)
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clasCon ON clasCon.Codigo = LTRIM(f.ClasificacionContable)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
LEFT OUTER JOIN dbo.SICC_Destino dest ON dest.Codigo = LTRIM(f.DestinoCredito)
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON LTRIM(f.TasaReferencia) = tasaRef.Codigo
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON met.Codigo = LTRIM(f.Metodologia)
LEFT OUTER JOIN dbo.SICC_Fondeo fond ON fond.Codigo = LTRIM(f.FuenteFondeo)
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON destMA.Codigo = LTRIM(f.DestinoCreditoMA)
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


INSERT INTO dbo.SICCMX_Credito (
 Codigo,
 PorcentajeFondeo,
 SaldoCapitalVigente,
 InteresVigente,
 SaldoCapitalVencido,
 InteresVencido,
 InteresRefinanciado,
 FechaTraspasoVigente,
 FechaTraspasoVencida,
 FechaProximaAmortizacion,
 FechaAutorizacionOriginal,
 MontoOriginal,
 IdReestructura,
 IdSituacionCredito,
 IdPersona,
 IdClasificacionContable,
 IdMoneda,
 IdDestino,
 IdTasaReferencia,
 TasaBruta,
 MontoLineaCredito,
 NumeroAgrupacion,
 FechaDisposicion,
 FechaVencimiento,
 FrecuenciaRevisionTasa,
 IdLineaCredito,
 NumeroReestructura,
 Formalizda,
 Recuperacion,
 Expediente,
 MesesIncumplimiento,
 IdMetodologia,
 IdFondeo,
 InteresCarteraVencida,
 EsMultimoneda,
 IdDestinoCreditoMA
)
SELECT
 f.CodigoCredito,
 NULLIF(f.PorcentajeGtiaFondos, ''),
 NULLIF(f.SaldoCapitalVigente, ''),
 NULLIF(f.SaldoInteresVigente, ''),
 NULLIF(f.SaldoCapitalVencido, ''),
 NULLIF(f.SaldoInteresVencido, ''),
 NULLIF(f.InteresRefinanciado, ''),
 NULLIF(f.FechaTraspasoVigente, ''),
 NULLIF(f.FechaTraspasoVencido, ''),
 NULLIF(f.FechaProximaAmortizacion, ''), NULLIF(f.FechaAutorizacionOriginal, ''),
 NULLIF(f.MontoOriginal, ''),
 rees.Clasificacion,
 sitCre.IdSituacionCredito,
 per.IdPersona,
 clasCon.IdClasificacionContable,
 mon.IdMoneda,
 dest.IdDestino,
 tasaRef.IdTasaReferencia,
 NULLIF(f.TasaInteresBruta, ''),
 NULLIF(f.MontoLinea, ''),
 f.NumeroAgrupacion,
 NULLIF(f.FechaDisposicion, ''),
 NULLIF(f.FechaVencimiento, ''),
 NULLIF(f.FrecuenciaRevisionTasa, ''),
 lin.IdLineaCredito,
 f.NumeroReestructura,
 NULLIF(f.OperacionFormalizada, ''),
 NULLIF(f.InformacionRecuperacion, ''),
 NULLIF(f.ExpedienteCompleto, ''),
 NULLIF(f.MesesVencidos, ''),
 met.Codigo,
 fond.IdFondeo,
 NULLIF(f.InteresCarteraVencida, ''),
 NULLIF(f.EsMultimoneda,''),
 destMA.IdDestinoCredito
FROM dbo.FILE_Credito f
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
INNER JOIN dbo.SICCMX_LineaCredito lin ON LTRIM(f.NumeroLinea) = lin.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAlta rees ON rees.Codigo = LTRIM(f.TipoAlta)
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON sitCre.Codigo = LTRIM(f.SituacionCredito)
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clasCon ON clasCon.Codigo = LTRIM(f.ClasificacionContable)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
LEFT OUTER JOIN dbo.SICC_Destino dest ON dest.Codigo = LTRIM(f.DestinoCredito)
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON LTRIM(f.TasaReferencia) = tasaRef.Codigo
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON met.Codigo = LTRIM(f.Metodologia)
LEFT OUTER JOIN dbo.SICC_Fondeo fond ON fond.Codigo = LTRIM(f.FuenteFondeo)
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON destMA.Codigo = LTRIM(f.DestinoCreditoMA)
LEFT OUTER JOIN dbo.SICCMX_Credito cre ON cre.Codigo = LTRIM(f.CodigoCredito)
WHERE cre.Codigo IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


SET NOCOUNT ON;
-- CREDITOS INFO: Siempre se agregan todos aunque sea vacio para que la info salga en los joins
INSERT INTO dbo.SICCMX_CreditoInfo (IdCredito)
SELECT DISTINCT c.IdCredito
FROM dbo.SICCMX_Credito c
LEFT OUTER JOIN dbo.SICCMX_CreditoInfo ci ON c.IdCredito = ci.IdCredito
WHERE ci.IdCredito IS NULL;

-- CREDITO ADICIONAL
INSERT INTO dbo.SICCMX_CreditoAdicional (IdCredito)
SELECT DISTINCT c.IdCredito
FROM dbo.SICCMX_Credito c
LEFT OUTER JOIN dbo.SICCMX_CreditoAdicional ca ON c.IdCredito = ca.IdCredito
WHERE ca.IdCredito IS NULL;


UPDATE cInfo
SET 
 IdTipoAlta = tipoAl.IdTipoAlta,
 IdDisposicion = dispCre.IdDisposicion,
 AjusteTasaReferencia = LTRIM(RTRIM(f.AjusteTasa)),
 MontoBancaDesarrollo = NULLIF(f.ImporteApoyoBancaDesarrollo, ''),
 IdInstitucionFondeo = inst.IdInstitucion,
 GastosOriginacion = NULLIF(f.ComisionesCobradas, ''),
 NumeroDisposicion = NULLIF(f.NumeroDisposicion, ''),
 MontoDispuesto = NULLIF(f.ImporteCreditoDispuesto, ''),
 MontoPagoExigible = NULLIF(f.PagoExigible, ''),
 MontoPagosRealizados = ISNULL(NULLIF(f.PagoCliente,''),
 CAST(ISNULL(NULLIF(f.MontoPagEfeCap,''),'0') AS DECIMAL(23,2))
 + CAST(ISNULL(NULLIF(f.MontoPagEfeInt,''),'0') AS DECIMAL(23,2))
 + CAST(ISNULL(NULLIF(f.MontoPagEfeCom,''),'0') AS DECIMAL(23,2))
 + CAST(ISNULL(NULLIF(f.MontoPagEfeIntMor,''),'0') AS DECIMAL(23,2))),
 MontoInteresPagado = NULLIF(f.InteresPagado, ''),
 DiasVencidos = NULLIF(f.DiasVencidos, ''),
 IdTipoBaja = tipoBa.IdTipoBaja,
 MontoBonificacion = NULLIF(f.ImporteQuebranto, ''),
 SaldoInicial = NULLIF(f.SaldoInicial, ''),
 SaldoFinal = NULLIF(f.SaldoPrincipalFinal, ''),
 IdPeriodicidadCapital = perCap.IdPeriodicidadCapital,
 IdPeriodicidadInteres = perInt.IdPeriodicidadInteres,
 CodigoCreditoReestructurado = LTRIM(RTRIM(f.CodigoCreditoReestructurado)),
 IdTipoProductoSerie4 = tipoPro4.IdTipoProducto,
 ProductoComercial = tcc.IdTipoCredito,
 SaldoPromedio = NULLIF(f.SaldoPromedio, ''),
 InteresDelMes = NULLIF(f.InteresDevengado, ''),
 ComisionDelMes = NULLIF(f.Comision, ''),
 Posicion = pos.IdPosicion,
 TipoLinea = tln.IdTipoLinea,
 ExposicionIncumplimiento = NULLIF(f.ExposicionIncumplimiento, ''),
 TipoOperacion = tpoOper.IdTipoOperacion,
 FecMaxDis = NULLIF(f.FecMaxDis, ''),
 PorcPartFederal = NULLIF(f.PorcPartFederal, ''),
 MontoPagEfeCap = NULLIF(f.MontoPagEfeCap, ''),
 MontoPagEfeInt = NULLIF(f.MontoPagEfeInt, ''),
 MontoPagEfeCom = NULLIF(f.MontoPagEfeCom, ''),
 MontoPagEfeIntMor = NULLIF(f.MontoPagEfeIntMor, ''),
 CredRepSIC = crs.IdCredRepSIC,
 ValMercDerivCred = NULLIF(f.ValMercDerivCred, ''),
 ConcursoMercantil = NULLIF(f.ConcursoMercantil, ''),
 ComDispTasa = NULLIF(f.ComDispTasa, ''),
 ComDispMonto = NULLIF(f.ComDispMonto, ''),
 GastosOrigTasa = NULLIF(f.GastosOrigTasa, ''),
 ResTotalInicioPer = NULLIF(f.ResTotalInicioPer, ''),
 MontoQuitasCastQue = NULLIF(f.MontoQuitasCastQue, ''),
 MontoBonificacionDesc = NULLIF(f.MontoBonificacionDesc, ''),
 NumEmpLoc = f.NumEmpLoc,
 NumEmpLocFedSHCP = f.NumEmpLocFedSHCP,
 MesesGraciaCap = CAST(NULLIF(f.MesesGraciaCap, '') AS DECIMAL),
 MesesGraciaIntereses = CAST(NULLIF(f.MesesGraciaIntereses, '') AS DECIMAL),
 SaldoParaCalculoInteres = NULLIF(f.SaldoParaCalculoInteres, ''),
 MontoIntereses = NULLIF(f.MontoIntereses, ''),
 FecPagExigibleRealizado = NULLIF(f.FecPagExigibleRealizado, ''),
 FecVenLinea = NULLIF(f.FecVenLinea, ''),
 CAT = NULLIF(f.CAT, ''),
 MontoLineaSinA = NULLIF(f.MontoLineaSinA, ''),
 MontoPrimasAnuales = NULLIF(f.MontoPrimasAnuales, ''),
 Emproblemado = NULLIF(f.Emproblemado, ''),
 CumpleCritContGral = ccc.IdCumpleCritContGral,
 EsPadre = NULLIF(f.EsPadre, ''),
 IdMunicipioDestino = (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadDestino)),
 IdActividadEconomicaDestino = actEco.IdActividadEconomica,
 DiasCalculoInteres = CAST(NULLIF(f.DiasCalculoInteres, '') AS DECIMAL),
 IdCategoria = cat.IdCategoria,
 FolioConsultaBuro = LTRIM(RTRIM(f.FolioConsultaBuro)),
 IdTipoAltaMA = tipoAlMA.IdTipoAlta,
 IdDisposicionMA = dispCreMA.IdDisposicion,
 IdPeriodicidadCapitalMA = perCapMA.IdPeriodicidadCapital,
 IdPeriodicidadInteresMA = perIntMA.IdPeriodicidadInteres,
 IdTipoBajaMA = tipoBaMA.IdTipoBaja,
 DiasMorosidad = NULLIF(f.DiasMorosidad,''),
 IdTipoProductoSerie4_2016 = tipoPro4_16.IdTipoProducto
FROM dbo.FILE_Credito f
INNER JOIN dbo.SICCMX_Credito cre ON LTRIM(f.CodigoCredito) = cre.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoAlta tipoAl ON tipoAl.Codigo = LTRIM(f.TipoAlta)
LEFT OUTER JOIN dbo.SICC_DisposicionCredito dispCre ON dispCre.Codigo = LTRIM(f.DisposicionCredito)
LEFT OUTER JOIN dbo.SICC_Institucion inst ON inst.Codigo = LTRIM(f.InstitutoFondea)
LEFT OUTER JOIN dbo.SICC_TipoBaja tipoBa ON tipoBa.Codigo = LTRIM(f.TipoBaja)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON perCap.Codigo = LTRIM(f.PeriodicidadCapital)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON perInt.Codigo = LTRIM(f.PeriodicidadInteres)
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tipoPro4 ON tipoPro4.Codigo = LTRIM(f.TipoCreditoR04A)
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON tcc.Codigo = LTRIM(f.ProductoComercial)
LEFT OUTER JOIN dbo.SICC_Posicion pos ON pos.Codigo = LTRIM(f.Posicion)
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON tln.Codigo = LTRIM(f.TipoLinea)
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON tpoOper.Codigo = LTRIM(f.TipoOperacion)
LEFT OUTER JOIN dbo.SICC_CredRepSIC crs ON crs.Codigo = LTRIM(f.CredRepSIC)
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral ccc ON LTRIM(f.CumpleCritContGral) = ccc.Codigo
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON LTRIM(f.ActividadEconomicaDestino) = actEco.Codigo
LEFT OUTER JOIN dbo.SICC_CategoriaCredito cat ON cat.Codigo = LTRIM(f.CategoriaCredito)
LEFT OUTER JOIN dbo.SICC_TipoAltaMA tipoAlMA ON tipoAlMA.Codigo = LTRIM(f.TipoAltaMA)
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dispCreMA ON dispCreMA.Codigo = LTRIM(f.DisposicionCreditoMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCapMA ON perCapMA.Codigo = LTRIM(f.PeriodicidadCapitalMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perIntMA ON perIntMA.Codigo = LTRIM(f.PeriodicidadInteresMA)
LEFT OUTER JOIN dbo.SICC_TipoBajaMA tipoBaMA ON tipoBaMA.Codigo = LTRIM(f.TipoBajaMA)
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tipoPro4_16 ON tipoPro4_16.Codigo = LTRIM(f.TipoCreditoR04A_2016)
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


-- UPDATE SICCMX_CreditoAdicional
UPDATE ca
SET
 SaldoPromedio = NULLIF(f.SaldoPromedio,''),
 InteresDevengado = NULLIF(f.InteresDevengado,''),
 Comision = NULLIF(f.Comision,'')
FROM dbo.SICCMX_CreditoAdicional ca
INNER JOIN dbo.SICCMX_Credito cre ON ca.IdCredito = cre.IdCredito
INNER JOIN dbo.FILE_Credito f ON cre.Codigo = LTRIM(f.CodigoCredito)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;

-- CAMPOS CAMBIO REGULATORIO 2016
UPDATE cInfo
SET DeudorFactoraje = LTRIM(RTRIM(f.DeudorFactoraje)),
 DeudorFactorajeRFC = LTRIM(RTRIM(f.DeudorFactorajeRFC)),
 MontoOtrosPrincipal = NULLIF(f.MontoOtrosPrincipal,''),
 MontoCondonacion = NULLIF(f.MontoCondonacion,''),
 MontoCastigos = NULLIF(f.MontoCastigos,''),
 MontoQuebrantos = NULLIF(f.MontoQuebrantos,''),
 MontoDescuentos = NULLIF(f.MontoDescuentos,''),
 MontoDacion = NULLIF(f.MontoDacion,''),
 IdMitigante = mit.IdMitigante,
 FactorConversion = NULLIF(f.FactorConversion,''),
 ExposicionAjuMit = NULLIF(f.ExposicionAjuMit,''),
 ExposicionNeta = NULLIF(f.ExposicionNeta,''),
 PonderadorRiesgo = NULLIF(f.PonderadorRiesgo,''),
 ReqCapital = NULLIF(f.ReqCapital,''),
 ReservasAdicionales = NULLIF(f.ReservasAdicionales,''),
 LocalidadDestino = LTRIM(RTRIM(f.LocalidadDestino)),
 EstadoDestino = LTRIM(RTRIM(f.EstadoDestino))
FROM dbo.FILE_Credito f
INNER JOIN dbo.SICCMX_Credito cre ON LTRIM(f.CodigoCredito) = cre.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_Mitigante mit ON LTRIM(f.Mitigante) = mit.Codigo
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO
