SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_ExistsRFCGarante]


AS


DECLARE @Detalle VARCHAR(1000);


DECLARE @Requerido BIT;


SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_ExistsRFCGarante';





IF @Requerido = 1


BEGIN


UPDATE dbo.FILE_Garantia


SET errorFormato = 1


WHERE LEN(ISNULL(RFCGarante, '')) = 0;





SET NOCOUNT ON;


END





INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)


SELECT


 CodigoGarantia,


 'RFC_Garante',


 NombreGarante,


 1,


 @Detalle


FROM dbo.FILE_Garantia


WHERE LEN(ISNULL(RFCGarante, '')) = 0;
GO
