SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04D0451]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04D0451);

DELETE FROM Historico.RW_R04D0451 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04D0451 (
	IdPeriodoHistorico, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato
)
SELECT
	@IdPeriodoHistorico,
	Concepto,
	Subreporte,
	Moneda,
	MetodoCalificacion,
	TipoCalificacion,
	TipoSaldo,
	Dato
FROM dbo.RW_R04D0451
WHERE IdReporteLog = @IdReporteLog;
GO
