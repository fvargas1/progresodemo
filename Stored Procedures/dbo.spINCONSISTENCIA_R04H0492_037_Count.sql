SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_037_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si existe dato diferente de cero en la columna 27 o 28, en este campo se debe reportar una clave diferente de cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE (CAST(ISNULL(NULLIF(PorCobPaMed,''),'0') AS DECIMAL) > 0 OR CAST(ISNULL(NULLIF(PorCobPP,''),'0') AS DECIMAL) > 0) AND EntidadCobertura= '0';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
