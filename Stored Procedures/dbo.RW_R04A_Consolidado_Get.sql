SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A_Consolidado_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato
FROM dbo.RW_VW_R04A_Consolidado
ORDER BY SubReporte, Moneda, Concepto;
GO
