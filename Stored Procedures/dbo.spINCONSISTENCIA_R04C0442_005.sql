SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_005]
AS

BEGIN

-- Si el acreditado es persona física para el RFC, se validará que:
-- - Las primeras 4 posiciones sean letras.
-- - Las siguientes 6 posiciones deben ser números (año, mes y día válidos).
-- - Las últimas 3 posiciones son alfanuméricos.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PersonalidadJuridica, RFC
FROM dbo.RW_R04C0442
WHERE PersonalidadJuridica = '1' AND (SUBSTRING(RFC,1,4) LIKE '%[^A-Z]%' OR SUBSTRING(RFC,5,6) LIKE '%[^0-9]%' OR SUBSTRING(RFC,11,3) LIKE '%[^A-Z0-9]%');

END
GO
