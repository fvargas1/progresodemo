SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_010_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- La Fecha Máxima para Disponer de los Recursos (dat_fecha_max_disposicion) debe ser MAYOR O IGUAL al periodo que se reporta (cve_periodo).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0453_INC
WHERE ISNULL(FecMaxDis,'') < @FechaPeriodo AND ISNULL(TipoAltaCredito,'') IN ('132','133','134','139','700','701','702','731','732','733','741','742','743');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
