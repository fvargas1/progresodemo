SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_027]
AS

BEGIN

-- Sí es un crédito sin fuente de pago propia (cve_proy_pago_anexo_19 = 2), entonces la Exposición al Incumplimiento
-- (dat_exposicion_incumplimiento) debe ser MAYOR O IGUAL a la Responsabilidad Total (dat_responsabilidad_total).

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	EITotal AS EI,
	ResponsabilidadFinal AS ResponsabilidadTotal
FROM dbo.RW_R04C0474
WHERE ISNULL(ProyectoInversion,'') = '2' AND CAST(ISNULL(NULLIF(EITotal,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL);

END
GO
