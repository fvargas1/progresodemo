SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_022]
AS

BEGIN

-- El Saldo Base para el Cálculo de Intereses a la Fecha de Corte del Crédito debe ser >= a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoCalculoInteres
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(SaldoCalculoInteres,''),'-1') AS DECIMAL) < 0;

END


GO
