SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Aval_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Aval_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Aval_Hst (
 IdPeriodoHistorico,
 CodigoAval,
 NombreAval,
 RFC,
 Domicilio,
 PersonalidadJuridica,
 TipoAval,
 MunAval,
 PIAval,
 Moneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 ActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 CodigoCliente,
 Localidad,
 Estado,
 CodigoPostal,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoAval,
 NombreAval,
 RFC,
 Domicilio,
 PersonalidadJuridica,
 TipoAval,
 MunAval,
 PIAval,
 Moneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 ActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 CodigoCliente,
 Localidad,
 Estado,
 CodigoPostal,
 Fuente
FROM dbo.FILE_Aval_Hst;
GO
