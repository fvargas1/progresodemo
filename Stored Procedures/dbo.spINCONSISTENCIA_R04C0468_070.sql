SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_070]
AS

BEGIN

-- Validar que el Acreditado Relacionado corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.IdDeudorRelacionado
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado deud ON ISNULL(rep.IdDeudorRelacionado,'') = deud.CodigoCNBV
WHERE deud.IdDeudorRelacionado IS NULL;

END


GO
