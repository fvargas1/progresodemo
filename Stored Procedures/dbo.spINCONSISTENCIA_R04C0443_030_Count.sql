SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_030_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se verificará que el número de días vencidos, no sea mayor a 90 días para seguirlo considerando como vigente.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0443
WHERE SituacionCredito ='1' AND CAST(ISNULL(NULLIF(DiasVencido,''),'0') AS DECIMAL) > 90;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
