SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_081]
AS
BEGIN
-- Validar que si la Situación del Crédito (cve_situacion_credito) es IGUAL a 2, el dato reportado en la columna de Número de días Vencidos (dat_num_vencidos) sea DIFERENTE de cero.

SELECT
 rep.CodigoCredito,
 rep.NumeroDisposicion,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
 rep.SituacionCredito,
 rep.DiasAtraso
FROM dbo.RW_VW_R04C0474_INC rep
OUTER APPLY (
	SELECT TOP 1 vw.Codigo, vw.TipoAlta
	FROM dbo.SICCMX_VW_Credito_TiposAlta vw
	WHERE vw.Codigo = rep.NumeroDisposicion
	ORDER BY vw.Fecha DESC
) AS oa
WHERE rep.SituacionCredito = '2' AND CAST(ISNULL(NULLIF(rep.DiasAtraso,''),'0') AS INT) = 0 AND ISNULL(oa.TipoAlta,'') NOT IN ('133','731','732','733');

END


GO
