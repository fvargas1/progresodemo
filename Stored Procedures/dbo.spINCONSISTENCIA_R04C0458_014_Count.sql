SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_014_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Un mismo RFC Acreditado (dat_rfc) no debe tener más de un ID Acreditado Asignado por la Institución (dat_id_credito_institucion) diferentes.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_VW_R04C0458_INC rep
	INNER JOIN dbo.RW_VW_R04C0458_INC rep2 ON rep.RFC = rep2.RFC AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
