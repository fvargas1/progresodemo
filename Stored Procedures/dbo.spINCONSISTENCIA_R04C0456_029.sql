SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_029]
AS
BEGIN
-- La SP Ajustada Por Fideicomisos De Garantía Y De Admón. Con Part. Fed. Y Aport. Federales Como Fuente De Pago debe estar entre 1 y 100.

SELECT
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_FidPartFed
FROM dbo.RW_VW_R04C0456_INC
WHERE CAST(ISNULL(NULLIF(SP_FidPartFed,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
