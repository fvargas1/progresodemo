SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_004]

AS



BEGIN



-- El RFC solo debe contener letras en mayúsculas y números, sin caracteres distintos a estos y sin espacios.



SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, RFC

FROM dbo.RW_R04C0443

WHERE RFC LIKE '%[^A-Z0-9_]%' AND RFC NOT LIKE '%&%'  OR BINARY_CHECKSUM(RFC) <> BINARY_CHECKSUM(UPPER(RFC));



END
GO
