SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_011]
AS

BEGIN

-- La "COMISIONES COBRADAS AL ACREDITADO (DENOMINADAS EN TASA)" debe ser mayor a cero si la comisión cobrada al acreditado denominada en monto (col. 12) es mayor a cero.

SELECT CodigoCredito, CodigoCreditoCNBV, ComisionesCobradasTasa, ComisionesCobradasMonto
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(ComisionesCobradasMonto,''),'0') AS DECIMAL) > 0 AND CAST(ISNULL(NULLIF(ComisionesCobradasTasa,''),'-1') AS DECIMAL(8,2)) = 0;

END
GO
