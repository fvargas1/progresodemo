SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Financieras]
AS
-- GARANTIAS REALES FINANCIERAS (CREDITO)
UPDATE can
SET
 He = vw.He,
 Hc = vw.Hc,
 Hfx = vw.Hfx,
 C = vw.MontoTotal,
 PrctCobSinAju = CASE WHEN cre.EI_Total > 0 THEN vw.MontoTotal / cre.EI_Total ELSE 0 END,
 PrctCobAjust = CASE WHEN cre.EI_Total > 0 THEN dbo.MIN2VAR(1, (CAST(vw.MontoTotal * (1 - vw.Hc - vw.Hfx) AS DECIMAL(25,4)) / cre.EI_Total)) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(cre.EI_Total, (vw.MontoTotal * (1 - vw.Hc - vw.Hfx))),
 [PI] = ppi.[PI],
 EI_Ajust = CASE WHEN vw.MontoTotal > 0 THEN dbo.MAX2VAR(0, (vw.MontoCredito * (1 + vw.He)) - (vw.MontoTotal * (1 - vw.Hc - vw.Hfx))) ELSE 0 END
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cre.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_Totales_Gar vw ON can.IdCredito = vw.IdCredito;
GO
