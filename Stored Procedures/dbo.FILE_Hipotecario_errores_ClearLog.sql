SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Hipotecario_errores_ClearLog]
AS
UPDATE dbo.FILE_Hipotecario
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Hipotecario_errores;
GO
