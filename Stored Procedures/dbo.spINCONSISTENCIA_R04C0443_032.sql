SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_032]
AS

BEGIN

-- La responsabilidad total deberá ser mayor o igual al saldo del principal al final del periodo.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, ResponsabilidadTotal, SaldoFinal
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(ResponsabilidadTotal,''),'-1') AS DECIMAL(23,2)) < CAST(ISNULL(NULLIF(SaldoFinal,''),'-1') AS DECIMAL(23,2));

END
GO
