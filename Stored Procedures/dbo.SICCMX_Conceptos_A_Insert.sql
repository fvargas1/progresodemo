SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Conceptos_A_Insert]
	@IdConcepto INT,
	@IdPeriodo INT,
	@Monto DECIMAL(23,2)
AS
INSERT INTO dbo.SICC_Conceptos_Periodo (IdConcepto, IdPeriodo, Monto) VALUES (@IdConcepto, @IdPeriodo, @Monto)
GO
