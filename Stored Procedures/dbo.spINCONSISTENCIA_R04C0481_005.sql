SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_005]
AS

BEGIN

-- El porcentaje cubierto debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctGarPersonales
FROM dbo.RW_VW_R04C0481_INC
WHERE ISNULL(PrctGarPersonales,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END


GO
