SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_031]
AS
BEGIN
-- La Fecha del Último Pago Exigible debe ser menor o igual al Periodo que se reporta.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	FechaUltimoPago,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0484_INC
WHERE FechaUltimoPago > @FechaPeriodo;

END
GO
