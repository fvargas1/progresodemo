SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCaseGroup_Ins]
	@Active BAJAWARE_utActivo,
	@Name BAJAWARE_utDescripcion100,
	@IdApplication BAJAWARE_utID,
	@SortOrder BAJAWARE_utInt,
	@StringKey BAJAWARE_utDescripcion100,
	@Description BAJAWARE_utDescripcion100,
	@CodeName BAJAWARE_utDescripcion50
AS
INSERT INTO dbo.BAJAWARE_UseCaseGroup (Active, Name, IdApplication, SortOrder, StringKey, Description, CodeName)
VALUES (@Active, @Name, @IdApplication, @SortOrder, @StringKey, @Description, @CodeName);

SELECT IdUseCaseGroup, Active, Name, IdApplication, SortOrder, StringKey, Description, CodeName
FROM dbo.BAJAWARE_UseCaseGroup
WHERE IdUseCaseGroup=SCOPE_IDENTITY();
GO
