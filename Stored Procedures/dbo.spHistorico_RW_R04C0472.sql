SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0472]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0472);
 
DELETE FROM Historico.RW_R04C0472 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0472 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, NombrePersona, TipoBaja, SaldoInicial,
	ResponsabilidadInicial, MontoPagado, MontoQuitCastReest, MontoBonificaciones
)
SELECT 
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NombrePersona,
	TipoBaja,
	SaldoInicial,
	ResponsabilidadInicial,
	MontoPagado,
	MontoQuitCastReest,
	MontoBonificaciones
FROM dbo.RW_R04C0472
WHERE IdReporteLog = @IdReporteLog;
GO
