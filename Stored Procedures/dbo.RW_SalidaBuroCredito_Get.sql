SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_SalidaBuroCredito_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Periodo,
	BP,
	Cuenta,
	Calificacion
FROM dbo.RW_SalidaBuroCredito;
GO
