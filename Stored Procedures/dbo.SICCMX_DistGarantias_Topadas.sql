SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Topadas]
AS

CREATE TABLE #TempFin (
 IdCredito BIGINT NULL,
 IdGarantia BIGINT NULL,
 MontoGarantia DECIMAL(25,4) NULL,
 MontoGarantiaAjust DECIMAL(25,4) NULL,
 Hc DECIMAL(18,10) NULL,
 Hfx DECIMAL(18,10) NULL
);


CREATE TABLE #Temp (
 IdCredito BIGINT NULL,
 IdTipoGarantia INT NULL,
 MontoCredito DECIMAL(25,4) NULL,
 MontoGarantia DECIMAL(25,4) NULL,
 MontoGarantiaAjust DECIMAL(25,4) NULL,
 PrctGarantia DECIMAL(18,10) NULL,
 Orden INT NULL
);


INSERT INTO #TempFin (
 IdCredito,
 IdGarantia,
 MontoGarantia,
 MontoGarantiaAjust,
 Hc,
 Hfx
)
SELECT
 cg.IdCredito,
 cg.IdGarantia,
 garantia.ValorGtiaValorizado,
 cg.MontoUsadoGarantia,
 garantia.Hc,
 CASE WHEN credito.IdMoneda <> garantia.IdMoneda THEN 0.08 ELSE 0 END
FROM dbo.SICCMX_VW_Credito_NMC credito
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON credito.IdCredito = cg.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia garantia ON cg.IdGarantia = garantia.IdGarantia
WHERE garantia.Aplica = 1 AND garantia.IdClasificacionNvaMet = 1 AND cg.Aplica = 1;


-- AJUSTAMOS EL MONTO DE LAS GARANTIAS FINANCIERAS
UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust * (1 - Hc - Hfx)
FROM #TempFin t;

-- INSERTAMOS GARANTIAS FINANCIERAS
INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 t.IdCredito,
 NULL,
 credito.EI_Total,
 SUM(t.MontoGarantia),
 SUM(t.MontoGarantiaAjust),
 1,
 1
FROM #TempFin t
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON credito.IdCredito = t.IdCredito
GROUP BY t.IdCredito, credito.EI_Total;


INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 cg.IdCredito,
 garantia.IdTipoGarantia,
 credito.EI_Total,
 SUM(cg.MontoUsadoGarantia),
 SUM(cg.MontoUsadoGarantia),
 1,
 garantia.SortOrder
FROM dbo.SICCMX_VW_Credito_NMC credito
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON credito.IdCredito = cg.IdCredito AND cg.Aplica = 1
INNER JOIN dbo.SICCMX_VW_Garantia garantia ON cg.IdGarantia = garantia.IdGarantia
WHERE garantia.Aplica=1 AND ISNULL(garantia.IdClasificacionNvaMet,0)<>1
GROUP BY cg.IdCredito, garantia.IdTipoGarantia, credito.EI_Total, garantia.SortOrder;


-- INSERTAMOS INFORMACION DE AVALES
INSERT INTO #Temp (
 IdCredito,
 IdTipoGarantia,
 MontoCredito,
 MontoGarantia,
 MontoGarantiaAjust,
 PrctGarantia,
 Orden
)
SELECT
 ca.IdCredito,
 tg.IdTipoGarantia,
 cre.EI_Total,
 SUM(ca.Monto),
 SUM(ca.Monto),
 1,
 tg.SortOrder
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICC_TipoGarantia tg ON 1=1 AND tg.Codigo='GP'
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON ca.IdCredito = cre.IdCredito
WHERE ca.Aplica = 1
GROUP BY ca.IdCredito, tg.IdTipoGarantia, cre.EI_Total, tg.SortOrder;



-- AJUSTAMOS EL MONTO DE LAS GARANTIAS NO FINANCIERAS
UPDATE t
SET MontoGarantiaAjust = MontoGarantia / (sp.NivelMaximo/100)
FROM #Temp t
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia;

UPDATE t
SET
 MontoGarantiaAjust = 0,
 PrctGarantia = 0
FROM #Temp t
INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia
WHERE (t.MontoGarantia / t.MontoCredito) * 100.00 < sp.NivelMinimo
AND ISNULL(t.MontoCredito,0)>0;


-- CALCULAMOS EL MONTO AJUSTADO DE LA GARANTIA
UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust + (t.MontoCredito - (SELECT SUM(t2.MontoGarantiaAjust) FROM #Temp t2 WHERE t.IdCredito = t2.IdCredito AND t2.Orden <= t.Orden))
FROM #Temp t
WHERE t.MontoCredito - (SELECT SUM(t2.MontoGarantiaAjust) FROM #Temp t2 WHERE t.IdCredito = t2.IdCredito AND t2.Orden <= t.Orden) < 0;

UPDATE t
SET MontoGarantiaAjust = MontoGarantiaAjust * (sp.NivelMaximo/100)
FROM #Temp t INNER JOIN dbo.SICCMX_SPGarantiasReales sp ON t.IdTipoGarantia = sp.IdTipoGarantia;

UPDATE #Temp SET MontoGarantiaAjust = 0 WHERE MontoGarantiaAjust < 0;


UPDATE #Temp SET PrctGarantia = MontoGarantiaAjust / MontoGarantia WHERE ISNULL(MontoGarantia,0)>0;


-- ACTUALIZAMOS EL PORCENTAJE A UTILIZAR DE LA GARANTIA
UPDATE cg
SET PorcUsadoGarantia = PorcUsadoGarantia * t.PrctGarantia
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia AND g.Aplica=1
INNER JOIN #Temp t ON cg.IdCredito = t.IdCredito AND g.IdTipoGarantia = t.IdTipoGarantia;

UPDATE cg
SET MontoUsadoGarantia = g.ValorGtiaValorizado * cg.PorcUsadoGarantia,
 MontoCubiertoCredito = g.ValorGtiaValorizado * cg.PorcUsadoGarantia,
 PorcCubiertoCredito = CAST(g.ValorGtiaValorizado * cg.PorcUsadoGarantia AS DECIMAL(23,2)) / c.MontoValorizado
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC c ON cg.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia
WHERE c.MontoValorizado > 0 AND cg.Aplica = 1;


-- ACTUALIZAMOS EL PORCENTAJE Y MONTO A UTILIZAR DEL AVAL
UPDATE ca
SET Porcentaje = Porcentaje * t.PrctGarantia
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN #Temp t ON ca.IdCredito = t.IdCredito
INNER JOIN dbo.SICC_TipoGarantia tg ON t.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo='GP'
WHERE ca.Aplica = 1;


DROP TABLE #TempFin;
DROP TABLE #Temp;
GO
