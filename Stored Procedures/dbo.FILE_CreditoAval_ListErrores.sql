SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_CreditoAval_ListErrores]
AS
SELECT
 CodigoAval,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_CreditoAval
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
