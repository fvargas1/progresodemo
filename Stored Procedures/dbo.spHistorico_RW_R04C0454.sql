SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0454]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0454);

DELETE FROM Historico.RW_R04C0454 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0454 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, CodigoPersona, RFC, NombrePersona, CategoriaCredito,
	FechaDisposicion, FechaVencDisposicion, Moneda, NumeroDisposicion, SaldoInicial, TasaInteres, MontoDispuesto, MontoPagoExigible,
	MontoCapitalPagado, MontoInteresPagado, MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado, MontoBonificado, SaldoFinal,
	SaldoCalculoInteres, DiasCalculoInteres, MontoInteresAplicar, ResponsabilidadFinal, SituacionCredito, DiasVencidos, FechaUltimoPago,
	ProyectoInversion, MontoBancaDesarrollo, InstitucionFondeo, ReservaTotal, ReservaCubierta, ReservaExpuesta, SPTotal, SPCubierta,
	SPExpuesta, EITotal, EICubierta, EIExpuesta, PITotal, PICubierta, PIExpuesta, GradoRiesgo, ReservaTotalInterna, SPInterna, EIInterna,
	PIInterna
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	CodigoPersona,
	RFC,
	NombrePersona,
	CategoriaCredito,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	SaldoInicial,
	TasaInteres,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoBonificado,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	ResponsabilidadFinal,
	SituacionCredito,
	DiasVencidos,
	FechaUltimoPago,
	ProyectoInversion,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	ReservaTotal,
	ReservaCubierta,
	ReservaExpuesta,
	SPTotal,
	SPCubierta,
	SPExpuesta,
	EITotal,
	EICubierta,
	EIExpuesta,
	PITotal,
	PICubierta,
	PIExpuesta,
	GradoRiesgo,
	ReservaTotalInterna,
	SPInterna,
	EIInterna,
	PIInterna
FROM dbo.RW_R04C0454
WHERE IdReporteLog = @IdReporteLog;
GO
