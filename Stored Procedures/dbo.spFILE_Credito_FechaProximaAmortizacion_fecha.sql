SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_FechaProximaAmortizacion_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_FechaProximaAmortizacion_fecha';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Credito
SET errorFormato = 1
WHERE LEN(FechaProximaAmortizacion)>0 AND ISDATE(FechaProximaAmortizacion) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCredito,
	'FechaProximaAmortizacion',
	FechaProximaAmortizacion,
	1,
	@Detalle
FROM dbo.FILE_Credito
WHERE LEN(FechaProximaAmortizacion)>0 AND ISDATE(FechaProximaAmortizacion) = 0;
GO
