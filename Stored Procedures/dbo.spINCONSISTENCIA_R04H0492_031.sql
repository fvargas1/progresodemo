SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_031]
AS

BEGIN

-- El dato "CONVENIO JUDICIAL O FIDEICOMISO DE GARANTÍA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.ConvenioJudicial
FROM dbo.RW_R04H0492 r
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial cat ON ISNULL(r.ConvenioJudicial,'') = cat.CodigoCNBV
WHERE cat.IdConvenioJudicial IS NULL;

END
GO
