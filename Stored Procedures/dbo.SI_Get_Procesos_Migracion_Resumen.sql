SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Get_Procesos_Migracion_Resumen]
AS
SELECT
 IdProceso,
 Nombre,
 Nivel,
 Codigo,
 CONVERT(VARCHAR(20), FechaInicio, 120) AS FechaInicio,
 CONVERT(VARCHAR(20), FechaFin, 120) AS FechaFin,
 Estatus,
 MaxNivel,
 Progreso,
 Position
FROM dbo.SICCMX_VW_ProcesosMigracion
ORDER BY Position

SELECT
 CONVERT(VARCHAR(20), MIN(FechaInicio), 120) AS FechaInicio,
 CONVERT(VARCHAR(20), MAX(FechaFin), 120) AS FechaFin,
 MAX(Estatus) AS Estatus,
 CAST(
 (SELECT CAST(COUNT(pr.IdInicializador) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Inicializador pr WHERE pr.FechaFin IS NOT NULL) /
 (SELECT CAST(COUNT(pr.IdInicializador) AS DECIMAL(5,2)) FROM dbo.MIGRACION_Inicializador pr)
 * 100 AS INT) AS Progreso
FROM dbo.MIGRACION_Inicializador
GO
