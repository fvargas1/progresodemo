SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_018_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Porcentaje de Pagos en Tiempo con Instituciones Financieras NO Bancarias en los Últimos 12 meses
-- (dat_porcent_pgo_no_bcos) es >= 0 y < 37, entonces el Puntaje Asignado por Porcentaje de Pagos en Tiempo
-- con Instituciones Financieras NO Bancarias en los Últimos 12 meses (cve_ptaje_porc_pgo_no_banco) debe ser = 22

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) >= 0 AND CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) < 37 AND ISNULL(P_PorcPagoInstNoBanc,'') <> '22';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
