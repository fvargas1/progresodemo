SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_076_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Responsabilidad Total (dat_responsabilidad_total) es MAYOR o IGUAL a 0,
-- entonces las Reservas Totales (dat_reservas_totales) deben ser MENORES O IGUALES a la Responsabilidad Total del Crédito(dat_responsabilidad_total).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0474
WHERE CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL) >= 0 AND CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) > CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
