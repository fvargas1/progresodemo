SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_008]
AS
BEGIN
-- Validar que la Clave de Estado (cve_estado) se encuentre dentro los valores de 1 a 32.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	Estado
FROM dbo.RW_VW_R04C0453_INC
WHERE CAST(ISNULL(Estado,'0') AS INT) NOT BETWEEN 1 AND 32;

END
GO
