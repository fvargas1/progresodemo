SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_InsumoRC15_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado
FROM Historico.RW_InsumoRC15
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoCredito;
GO
