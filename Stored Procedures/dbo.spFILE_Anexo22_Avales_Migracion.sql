SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_Avales_Migracion]
AS

UPDATE an22
SET
 IdGP = av.IdAval,
 NumDiasMoraPromInstBanc = NULLIF(f.NumDiasMoraPromInstBanc,''),
 PorPagoTiemInstBanc = CAST(NULLIF(f.PorPagoTiemInstBanc,'') AS DECIMAL(18,10)) * 100.00,
 NumInstRepUlt12Meses = NULLIF(f.NumInstRepUlt12Meses,''),
 PorPagoTiemInstNoBanc = CAST(NULLIF(f.PorPagoTiemInstNoBanc,'') AS DECIMAL(18,10)) * 100.00,
 MonTotPagInfonavitUltBimestre = NULLIF(f.MonTotPagInfonavitUltBimestre,''),
 NumDiasAtrInfonavitUltBimestre = CASE WHEN LTRIM(f.NumDiasAtrInfonavitUltBimestre) = 'SP' THEN 9999.00 ELSE NULLIF(f.NumDiasAtrInfonavitUltBimestre,'') END,
 TasaRetLab = CAST(NULLIF(f.TasaRetLab,'') AS DECIMAL(18,10)) * 100.00,
 IndCalEstaEcono = NULLIF(f.IndCalEstaEcono,''),
 IntCarCompetencia = NULLIF(f.IntCarCompetencia,''),
 Provedores = NULLIF(f.Provedores,''),
 Clientes = NULLIF(f.Clientes,''),
 EdoFinAudit = NULLIF(f.EdoFinAudit,''),
 NumAgeCalf = NULLIF(f.NumAgeCalf,''),
 IndConsAdmon = NULLIF(f.IndConsAdmon,''),
 EstrucOrg = NULLIF(f.EstrucOrg,''),
 CompAccionaria = NULLIF(f.CompAccionaria,''),
 GastosFinancieros = NULLIF(f.GastosFinancieros,''),
 VentNetTotAnuales = NULLIF(f.VentNetTotAnuales,''),
 PasivoCirculante = NULLIF(f.PasivoCirculante,''),
 ActivoCirculante = NULLIF(f.ActivoCirculante,''),
 ActTotalAnual = NULLIF(f.ActTotalAnual,''),
 CapitalContableProm = NULLIF(f.CapitalContableProm,''),
 UtilidadNeta = NULLIF(f.UtilidadNeta,''),
 UAFIR = NULLIF(f.UAFIR,''),
 OrgDescPartidoPolitico = NULLIF(f.OrgDescPartidoPolitico,''),
 FechaInfoFinanc = NULLIF(f.FechaInfoFinanc,''),
 FechaInfoBuro = NULLIF(f.FechaInfoBuro,''),
 CalCredAgenciaCal = LTRIM(NULLIF(f.CalCredAgenciaCal,'')),
 ExpNegativasPag = LTRIM(NULLIF(f.ExpNegativasPag,'')),
 NumeroEmpleados = 0,
 LugarRadica = CASE WHEN mun.CodigoPais = '484' AND ISNUMERIC(mun.CodigoEstado) = 1 AND CAST(mun.CodigoEstado AS INT) BETWEEN 1 AND 32 THEN 1 ELSE 2 END,
 EsGarante = esg.IdEsGarante,
 F_NumDiasMoraPromInstBanc = NULLIF(f.F_NumDiasMoraPromInstBanc,''),
 F_PorPagoTiemInstBanc = NULLIF(f.F_PorPagoTiemInstBanc,''),
 F_NumInstRepUlt12Meses = NULLIF(f.F_NumInstRepUlt12Meses,''),
 F_PorPagoTiemInstNoBanc = NULLIF(f.F_PorPagoTiemInstNoBanc,''),
 F_MonTotPagInfonavitUltBimestre = NULLIF(f.F_MonTotPagInfonavitUltBimestre,''),
 F_NumDiasAtrInfonavitUltBimestre = NULLIF(f.F_NumDiasAtrInfonavitUltBimestre,''),
 F_TasaRetLab = NULLIF(f.F_TasaRetLab,''),
 F_RotActTotales = NULLIF(f.F_RotActTotales,''),
 F_RotCapTrabajo = NULLIF(f.F_RotCapTrabajo,''),
 F_RendimeintosCapitalROE = NULLIF(f.F_RendimeintosCapitalROE,''),
 F_IndCalEstaEcono = NULLIF(f.F_IndCalEstaEcono,''),
 F_IntCarCompetencia = NULLIF(f.F_IntCarCompetencia,''),
 F_Provedores = NULLIF(f.F_Provedores,''),
 F_Clientes = NULLIF(f.F_Clientes,''),
 F_EdoFinAudit = NULLIF(f.F_EdoFinAudit,''),
 F_NumAgeCalf = NULLIF(f.F_NumAgeCalf,''),
 F_IndConsAdmon = NULLIF(f.F_IndConsAdmon,''),
 F_EstrucOrg = NULLIF(f.F_EstrucOrg,''),
 F_CompAccionaria = NULLIF(f.F_CompAccionaria,''),
 F_LiquidezOper = NULLIF(f.F_LiquidezOper,''),
 F_UAFIR_GastosFin = NULLIF(f.F_UAFIR_GastosFin,''),
 SinAtrasos = NULLIF(f.SinAtrasos,'')
FROM dbo.SICCMX_Anexo22_GP an22
INNER JOIN dbo.SICCMX_Aval av ON an22.IdGP = av.IdAval
INNER JOIN dbo.FILE_Anexo22 f ON LTRIM(f.CodigoCliente) = av.Codigo
INNER JOIN dbo.SICC_EsGarante esg ON LTRIM(f.EsGarante) = esg.Codigo
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON av.IdMunAval = mun.IdLocalidad
WHERE esg.Layout = 'AVAL' AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


INSERT INTO dbo.SICCMX_Anexo22_GP (
 IdGP,
 NumDiasMoraPromInstBanc,
 PorPagoTiemInstBanc,
 NumInstRepUlt12Meses,
 PorPagoTiemInstNoBanc,
 MonTotPagInfonavitUltBimestre,
 NumDiasAtrInfonavitUltBimestre,
 TasaRetLab,
 IndCalEstaEcono,
 IntCarCompetencia,
 Provedores,
 Clientes,
 EdoFinAudit,
 NumAgeCalf,
 IndConsAdmon,
 EstrucOrg,
 CompAccionaria,
 GastosFinancieros,
 VentNetTotAnuales,
 PasivoCirculante,
 ActivoCirculante,
 ActTotalAnual,
 CapitalContableProm,
 UtilidadNeta,
 UAFIR,
 OrgDescPartidoPolitico,
 FechaInfoFinanc,
 FechaInfoBuro,
 CalCredAgenciaCal,
 ExpNegativasPag,
 NumeroEmpleados,
 LugarRadica,
 EsGarante,
 F_NumDiasMoraPromInstBanc,
 F_PorPagoTiemInstBanc,
 F_NumInstRepUlt12Meses,
 F_PorPagoTiemInstNoBanc,
 F_MonTotPagInfonavitUltBimestre,
 F_NumDiasAtrInfonavitUltBimestre,
 F_TasaRetLab,
 F_RotActTotales,
 F_RotCapTrabajo,
 F_RendimeintosCapitalROE,
 F_IndCalEstaEcono,
 F_IntCarCompetencia,
 F_Provedores,
 F_Clientes,
 F_EdoFinAudit,
 F_NumAgeCalf,
 F_IndConsAdmon,
 F_EstrucOrg,
 F_CompAccionaria,
 F_LiquidezOper,
 F_UAFIR_GastosFin,
 SinAtrasos
)
SELECT
 av.IdAval,
 NULLIF(f.NumDiasMoraPromInstBanc,''),
 CAST(NULLIF(f.PorPagoTiemInstBanc,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.NumInstRepUlt12Meses,''),
 CAST(NULLIF(f.PorPagoTiemInstNoBanc,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.MonTotPagInfonavitUltBimestre,''),
 CASE WHEN LTRIM(f.NumDiasAtrInfonavitUltBimestre) = 'SP' THEN 9999.00 ELSE NULLIF(f.NumDiasAtrInfonavitUltBimestre,'') END,
 CAST(NULLIF(f.TasaRetLab,'') AS DECIMAL(18,10)) * 100.00,
 NULLIF(f.IndCalEstaEcono,''),
 NULLIF(f.IntCarCompetencia,''),
 NULLIF(f.Provedores,''),
 NULLIF(f.Clientes,''),
 NULLIF(f.EdoFinAudit,''),
 NULLIF(f.NumAgeCalf,''),
 NULLIF(f.IndConsAdmon,''),
 NULLIF(f.EstrucOrg,''),
 NULLIF(f.CompAccionaria,''),
 NULLIF(f.GastosFinancieros,''),
 NULLIF(f.VentNetTotAnuales,''),
 NULLIF(f.PasivoCirculante,''),
 NULLIF(f.ActivoCirculante,''),
 NULLIF(f.ActTotalAnual,''),
 NULLIF(f.CapitalContableProm,''),
 NULLIF(f.UtilidadNeta,''),
 NULLIF(f.UAFIR,''),
 NULLIF(f.OrgDescPartidoPolitico,''),
 NULLIF(f.FechaInfoFinanc,''),
 NULLIF(f.FechaInfoBuro,''),
 LTRIM(NULLIF(f.CalCredAgenciaCal,'')),
 LTRIM(NULLIF(f.ExpNegativasPag,'')),
 0,
 CASE WHEN mun.CodigoPais = '484' AND ISNUMERIC(mun.CodigoEstado) = 1 AND CAST(mun.CodigoEstado AS INT) BETWEEN 1 AND 32 THEN 1 ELSE 2 END,
 esg.IdEsGarante,
 NULLIF(f.F_NumDiasMoraPromInstBanc,''),
 NULLIF(f.F_PorPagoTiemInstBanc,''),
 NULLIF(f.F_NumInstRepUlt12Meses,''),
 NULLIF(f.F_PorPagoTiemInstNoBanc,''),
 NULLIF(f.F_MonTotPagInfonavitUltBimestre,''),
 NULLIF(f.F_NumDiasAtrInfonavitUltBimestre,''),
 NULLIF(f.F_TasaRetLab,''),
 NULLIF(f.F_RotActTotales,''),
 NULLIF(f.F_RotCapTrabajo,''),
 NULLIF(f.F_RendimeintosCapitalROE,''),
 NULLIF(f.F_IndCalEstaEcono,''),
 NULLIF(f.F_IntCarCompetencia,''),
 NULLIF(f.F_Provedores,''),
 NULLIF(f.F_Clientes,''),
 NULLIF(f.F_EdoFinAudit,''),
 NULLIF(f.F_NumAgeCalf,''),
 NULLIF(f.F_IndConsAdmon,''),
 NULLIF(f.F_EstrucOrg,''),
 NULLIF(f.F_CompAccionaria,''),
 NULLIF(f.F_LiquidezOper,''),
 NULLIF(f.F_UAFIR_GastosFin,''),
 NULLIF(f.SinAtrasos,'')
FROM dbo.FILE_Anexo22 f
INNER JOIN dbo.SICCMX_Aval av ON f.CodigoCliente = av.Codigo
INNER JOIN dbo.SICC_EsGarante esg ON LTRIM(f.EsGarante) = esg.Codigo
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON av.IdMunAval = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICCMX_Anexo22_GP an22 ON av.IdAval = an22.IdGP
WHERE esg.Layout = 'AVAL' AND an22.IdGP IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


SET NOCOUNT ON;
UPDATE dbo.SICCMX_Anexo22_GP
SET
 RotActTotales = CASE WHEN ActTotalAnual = 0 THEN NULL ELSE CAST(VentNetTotAnuales AS DECIMAL(22,8)) / ActTotalAnual END,
 RotCapTrabajo = CASE WHEN (ActivoCirculante - PasivoCirculante) = 0 THEN NULL ELSE CAST(VentNetTotAnuales AS DECIMAL(22,8)) / (ActivoCirculante - PasivoCirculante) END,
 RendimeintosCapitalROE = CASE WHEN CapitalContableProm = 0 THEN NULL ELSE UtilidadNeta / CapitalContableProm END,
 UAFIR_GastosFin = CASE WHEN GastosFinancieros = 0 THEN NULL ELSE UAFIR / GastosFinancieros END,
 LiquidezOper = CASE WHEN PasivoCirculante = 0 THEN NULL ELSE ActivoCirculante / PasivoCirculante END;


-- CUANDO LOS VALORES PARA EL CALCULO DEL RENDIMIENTOS CAPITAL (ROE) SEAN NEGATIVOS, EL RESULTADO SE MANTIENE NEGATIVO
UPDATE dbo.SICCMX_Anexo22_GP
SET RendimeintosCapitalROE = RendimeintosCapitalROE * -1
WHERE UtilidadNeta < 0 AND CapitalContableProm < 0;
GO
