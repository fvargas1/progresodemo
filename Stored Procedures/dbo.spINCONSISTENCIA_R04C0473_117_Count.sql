SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_117_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Tasa de Referencia (cve_tasa) es igual a 100, 110, 120, 140, 410, 610, 620 ó 625, entonce la Moneda (cve_moneda) debe ser igual a pesos (0).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139','700','701')
	AND ISNULL(TasaInteres,'') IN ('100','110','120','140','410','610','620','625') AND ISNULL(Moneda,'') <> '0'

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
