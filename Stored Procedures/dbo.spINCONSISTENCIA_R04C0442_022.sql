SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_022]
AS

BEGIN

-- El ID met CNBV deberá ser único e irrepetible para cada crédito de la entidad.

SELECT CodigoCreditoCNBV, COUNT(CodigoCreditoCNBV) AS Repetidos
FROM dbo.RW_R04C0442
GROUP BY CodigoCreditoCNBV
HAVING COUNT(CodigoCreditoCNBV) > 1;

END
GO
