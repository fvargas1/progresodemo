SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_045]
AS

BEGIN

-- Un mismo RFC Acreditado (dat_rfc) no debe tener más de un ID Acreditado Asignado por la Institución (dat_id_credito_institucion) diferentes.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	CodigoPersona,
	RFC
FROM dbo.RW_VW_R04C0468_INC
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_VW_R04C0468_INC rep
	INNER JOIN dbo.RW_VW_R04C0468_INC rep2 ON rep.RFC = rep2.RFC
	AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.RFC
);

END


GO
