SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0450_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM dbo.RW_VW_R04C0450
ORDER BY NombreGarante, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion;
GO
