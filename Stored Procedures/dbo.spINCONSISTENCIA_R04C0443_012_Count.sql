SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_012_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se validará que ID CREDITO MET CNBV esté vigente (dado de alta) y no se haya reportado como baja efectiva en el formulario de altas.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT r.CodigoCredito)
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICCMX_VW_Datos_0442 cat ON ISNULL(r.CodigoCreditoCNBV,'') = cat.CodigoCreditoCNBV
WHERE cat.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
