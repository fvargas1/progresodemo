SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0443_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	NumeroDisposicion,
	FechaDisposicion,
	CalificacionPersona,
	CalificacionCreditoCubierta,
	CalificacionCreditoExpuesta,
	CalificacionPersonaCNBV,
	CalificacionCreditoCubiertaCNBV,
	CalificacionCreditoExpuestaCNBV,
	SituacionCredito,
	SaldoInicial,
	TasaInteres,
	MontoDispuesto,
	MontoExigible,
	MontoPagado,
	MontoInteresPagado,
	MontoComisionDevengada,
	DiasVencido,
	SaldoFinal,
	ResponsabilidadTotal,
	TipoBaja,
	MontoReconocido,
	ProductoComercial
FROM Historico.RW_R04C0443
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NombrePersona;
GO
