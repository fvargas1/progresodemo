SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Portafolio_VPM]
AS
-- Se excluye garantia Euler y se considera en Aval
UPDATE dbo.FILE_Garantia
SET errorCatalogo = 1
WHERE LTRIM(TipoGarantia) = '2110';

-- Se excluye garantia Fonaga y se considera en Portafolios
UPDATE dbo.FILE_Garantia
SET errorCatalogo = 1
WHERE LTRIM(TipoGarantia) IN ('1851', '1852', '1853');


INSERT INTO dbo.SICCMX_CreditoGarantia_Portafolio (
	CodigoPortafolio,
	CodigoCredito,
	CodigoGarantia
)
SELECT
	LTRIM(RTRIM(gp.CodigoGarantia)),
	LTRIM(RTRIM(fcg.CodigoCredito)),
	LTRIM(RTRIM(fcg.CodigoGarantia))
FROM dbo.FILE_CreditoGarantia fcg
INNER JOIN dbo.FILE_Garantia fg ON LTRIM(fcg.CodigoGarantia) = LTRIM(fg.CodigoGarantia)
INNER JOIN (
	SELECT cg.CodigoCredito, cg.CodigoGarantia
	FROM dbo.FILE_CreditoGarantia cg
	INNER JOIN dbo.FILE_Garantia g ON LTRIM(cg.CodigoGarantia) = LTRIM(g.CodigoGarantia)
	WHERE LTRIM(g.TipoGarantia) = 'NMC-04'
) AS gp ON fcg.CodigoCredito = gp.CodigoCredito
WHERE LTRIM(fg.TipoGarantia) IN ('1851', '1852', '1853');


UPDATE fg
SET RFCGarante = LTRIM(RTRIM(gp.RFCGarante)),
	IdGarante = LTRIM(RTRIM(gp.IdGarante)),
	ActividaEcoGarante = LTRIM(RTRIM(gp.ActividaEcoGarante)),
	MunicipioGarante = LTRIM(RTRIM(gp.MunicipioGarante)),
	LEI = LTRIM(RTRIM(gp.LEI)),
	NombreGarante = LTRIM(RTRIM(gp.NombreGarante)),
	Moneda = LTRIM(RTRIM(gp.Moneda))
FROM dbo.FILE_CreditoGarantia fcg
INNER JOIN dbo.FILE_Garantia fg ON LTRIM(fcg.CodigoGarantia) = LTRIM(fg.CodigoGarantia)
CROSS APPLY (
	SELECT TOP 1 g.RFCGarante, g.IdGarante, g.ActividaEcoGarante, g.MunicipioGarante, g.LEI, g.NombreGarante, g.Moneda
	FROM dbo.FILE_CreditoGarantia cg
	INNER JOIN dbo.FILE_Garantia g ON LTRIM(cg.CodigoGarantia) = LTRIM(g.CodigoGarantia)
	WHERE cg.CodigoCredito = fcg.CodigoCredito AND LTRIM(g.TipoGarantia) IN ('1851', '1852', '1853')
) AS gp
WHERE LTRIM(fg.TipoGarantia) = 'NMC-04';
GO
