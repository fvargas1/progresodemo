SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Financieras_Consumo]
AS
-- GARANTIAS REALES FINANCIERAS (CREDITO)
UPDATE can
SET
 He = vw.He,
 Hc = vw.Hc,
 Hfx = vw.Hfx,
 C = vw.MontoTotal,
 PrctCobSinAju = CASE WHEN crv.E > 0 THEN vw.MontoTotal / crv.E ELSE 0 END,
 PrctCobAjust = CASE WHEN crv.E > 0 THEN dbo.MIN2VAR(1, (CAST(vw.MontoTotal * (1 - vw.Hc - vw.Hfx) AS DECIMAL(25,4)) / crv.E)) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(crv.E, (vw.MontoTotal * (1 - vw.Hc - vw.Hfx))),
 [PI] = crv.[PI],
 EI_Ajust = CASE WHEN vw.MontoTotal > 0 THEN dbo.MAX2VAR(0, (vw.MontoCredito * (1 + vw.He)) - (vw.MontoTotal * (1 - vw.Hc - vw.Hfx))) ELSE 0 END
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON can.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICCMX_VW_Totales_Gar_Consumo vw ON can.IdConsumo = vw.IdConsumo;
GO
