SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_011_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si se reporta algún número de no garantías financieras, entonces el valor de éstas debe ser mayor a cero.
-- Si dat_num_gtias_reales_no_fin>0, entonces  dat_valor_gtia_derechos_cobro > 0 o dat_valor_gtia_inmuebles >0 o dat_valor_gtia_muebles > 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0471_INC
WHERE CAST(NumeroGarRealNoFin AS DECIMAL) > 0 AND (CAST(ValorDerechosCobro AS DECIMAL) + CAST(ValorBienesInmuebles AS DECIMAL) + CAST(ValorBienesMuebles AS DECIMAL)) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
