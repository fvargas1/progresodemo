SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0476_036]
AS

BEGIN

-- El porcentaje cubierto por esquemas de paso y medida debe encontrarse entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PrctECPM
FROM dbo.RW_VW_R04C0476_INC
WHERE CAST(ISNULL(NULLIF(PrctECPM,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
