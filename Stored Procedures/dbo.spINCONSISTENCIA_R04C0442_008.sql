SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_008]
AS

BEGIN

-- Para el caso de personas morales el Nombre del Acreditado no debe contener lo siguiente:
-- - A.C.
-- - S.A.
-- - SA DE CV
-- - S.A. DE C.V.
-- - S.C. DE R.L. DE C.V.
-- - SC DE RL DE CV

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PersonalidadJuridica, NombrePersona
FROM dbo.RW_R04C0442
WHERE PersonalidadJuridica = '2' AND (NombrePersona LIKE '%A.C.%'
	OR NombrePersona LIKE '%S.A.%'
	OR NombrePersona LIKE '%SA DE CV%'
	OR NombrePersona LIKE '%S.A. DE C.V.%'
	OR NombrePersona LIKE '%S.C. DE R.L. DE C.V.%'
	OR NombrePersona LIKE '%SC DE RL DE CV%');

END
GO
