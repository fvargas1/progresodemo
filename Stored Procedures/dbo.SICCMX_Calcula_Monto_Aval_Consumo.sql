SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Monto_Aval_Consumo]
AS
UPDATE dbo.SICCMX_ConsumoAval SET Porcentaje = 1 WHERE Porcentaje > 1;

UPDATE ca
SET Monto = CASE WHEN cob.Codigo = '1' THEN crv.E ELSE CAST(con.SaldoCapitalVigenteValorizado + con.SaldoCapitalVencidoValorizado AS DECIMAL(23,2)) END * ca.Porcentaje
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN dbo.SICCMX_VW_Consumo con ON ca.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICC_TipoCobertura cob ON av.TipoCobertura = cob.IdTipoCobertura
WHERE ca.Aplica = 1;
GO
