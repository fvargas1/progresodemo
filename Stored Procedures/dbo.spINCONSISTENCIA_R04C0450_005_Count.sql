SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_005_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Se debe reportar el Monto de la Garantía o el Porcentaje Cubierto del Crédito

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0450_INC
WHERE CAST(ISNULL(MontoGarantia,'0') AS MONEY) = 0 AND CAST(ISNULL(PrctGarantia,'0') AS MONEY) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
