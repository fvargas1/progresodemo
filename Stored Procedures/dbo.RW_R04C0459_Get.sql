SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0459_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	CodigoPersona,
	RFC,
	NombrePersona,
	CategoriaCredito,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	SaldoInicial,
	TasaInteres,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoBonificado,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	ResponsabilidadFinal,
	SituacionCredito,
	DiasVencidos,
	FechaUltimoPago,
	ProyectoInversion,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	ReservaTotal,
	ReservaCubierta,
	ReservaExpuesta,
	SPTotal,
	SPCubierta,
	SPExpuesta,
	EITotal,
	EICubierta,
	EIExpuesta,
	PITotal,
	PICubierta,
	PIExpuesta,
	GradoRiesgo,
	ReservaTotalInterna,
	SPInterna,
	EIInterna,
	PIInterna
FROM dbo.RW_VW_R04C0459
ORDER BY CodigoCredito
GO
