SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_Delete]
	@IdValidacion int
AS
DELETE FROM dbo.MIGRACION_Validacion WHERE IdValidacion = @IdValidacion;
GO
