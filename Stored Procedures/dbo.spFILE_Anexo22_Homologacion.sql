SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_Homologacion]
AS
DECLARE @Campo VARCHAR(50);
DECLARE @Tipo INT;
DECLARE @Fuente VARCHAR(50);

DECLARE crs CURSOR FOR
SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente
FROM dbo.FILE_Anexo22_Homologacion
WHERE Activo=1
ORDER BY Tipo;

OPEN crs;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

WHILE @@FETCH_STATUS = 0
BEGIN
EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Anexo22', 'FILE_Anexo22_Homologacion', @Campo, @Tipo, @Fuente;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;
END

CLOSE crs;
DEALLOCATE crs;

UPDATE dbo.FILE_Anexo22 SET Homologado = 1;
GO
