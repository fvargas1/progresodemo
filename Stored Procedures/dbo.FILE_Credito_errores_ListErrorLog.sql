SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Credito_errores_ListErrorLog]
AS
SELECT
f.identificador,
f.nombreCampo,
f.valor,
f.tipoError,
f.description
FROM dbo.FILE_Credito_errores f
GO
