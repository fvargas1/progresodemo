SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_028]
AS

BEGIN

-- Si la Rotación de Activos Totales (dat_rotac_activos_tot) es <0.75, entonces el Puntaje Asignado por Rotación de Activos Totales (cve_ptaje_rotac_activo_tot) debe ser = 50

SELECT
 CodigoPersona AS CodigoDeudor,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 RotActTot,
 P_RotActTot AS Puntos_RotActTot
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(RotActTot AS DECIMAL(18,6)) < 0.75 AND ISNULL(P_RotActTot,'') <> '50';

END


GO
