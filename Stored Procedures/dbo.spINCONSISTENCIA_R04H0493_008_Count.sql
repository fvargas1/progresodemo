SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_008_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "RESPONSABILIDAD TOTAL AL MOMENTO DE LA BAJA" debe ser mayor o igual a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0493
WHERE CAST(ISNULL(NULLIF(MontoTotalLiquidacion,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
