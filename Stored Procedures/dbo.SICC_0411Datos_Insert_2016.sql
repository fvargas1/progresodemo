SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0411Datos_Insert_2016]
AS

TRUNCATE TABLE R04.[0411Datos_2016];

-- COMERCIAL
INSERT INTO R04.[0411Datos_2016] (
 Codigo,
 CodigoProducto,
 SituacionCredito,
 Morosidad,
 CapitalVigente,
 CapitalVencido,
 InteresVigente,
 InteresVencido,
 Moneda
)
SELECT
 cre.CodigoCredito,
 tp.Codigo,
 sic.Codigo,
 ISNULL(info.DiasMorosidad,0),
 cre.SaldoVigenteValorizado,
 cre.SaldoVencidoValorizado,
 cre.InteresVigenteValorizado,
 cre.InteresVencidoValorizado,
 mon.ClasSerieA
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito 
INNER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = cre.IdSituacionCredito
WHERE cre.Posicion <> '181' AND cre.TipoLinea <> '181'; -- No se consideran Cartas de Crédito


-- CONSUMO
INSERT INTO R04.[0411Datos_2016] (
 Codigo,
 CodigoProducto,
 SituacionCredito,
 Morosidad,
 CapitalVigente,
 CapitalVencido,
 InteresVigente,
 InteresVencido,
 Moneda
)
SELECT
 cre.Codigo,
 tp.Codigo,
 sic.Codigo,
 ISNULL(info.DiasAtraso,0),
 cre.SaldoCapitalVigenteValorizado,
 cre.SaldoCapitalVencidoValorizado,
 cre.InteresVigenteValorizado,
 cre.InteresVencidoValorizado,
 mon.ClasSerieA
FROM dbo.SICCMX_VW_Consumo cre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON cre.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = cre.IdSituacionCredito;


-- HIPOTECARIO
INSERT INTO R04.[0411Datos_2016] (
 Codigo,
 CodigoProducto,
 SituacionCredito,
 Morosidad,
 CapitalVigente,
 CapitalVencido,
 InteresVigente,
 InteresVencido,
 Moneda
)
SELECT
 cre.Codigo,
 tp.Codigo,
 sic.Codigo,
 pre.DiasAtraso,
 cre.SaldoCapitalVigenteValorizado,
 cre.SaldoCapitalVencidoValorizado,
 cre.InteresVigenteValorizado,
 cre.InteresVencidoValorizado,
 mon.ClasSerieA
FROM dbo.SICCMX_VW_Hipotecario cre
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON cre.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON info.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = cre.IdTipoCreditoR04A_2016
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = info.IdSituacionCredito;
GO
