SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_020]

AS



BEGIN



-- El Monto Total Pagado Efectivamente por el Acreditado no cuadra con la suma de los montos pagados.



SELECT

	CodigoCredito,

	NumeroDisposicion,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	MontoTotalPagado,

	MontoCapitalPagado,

	MontoInteresPagado,

	MontoComisionPagado,

	MontoInteresMoratorio

FROM dbo.RW_VW_R04C0469_INC

WHERE CAST(ISNULL(NULLIF(MontoTotalPagado,''),'0') AS DECIMAL(18,2)) <> (

	CAST(ISNULL(NULLIF(MontoCapitalPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoInteresPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoComisionPagado,''),'0') AS DECIMAL(18,2)) +

	CAST(ISNULL(NULLIF(MontoInteresMoratorio,''),'0') AS DECIMAL(18,2)));



END
GO
