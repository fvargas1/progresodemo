SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_030]
AS

BEGIN

-- La Fecha Máxima para Disponer de los Recursos (dat_fecha_max_disposicion) debe ser MENOR o IGUAL a la Fecha Vencimiento de Línea de Crédito (dat_fecha_vencimiento).

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	FecMaxDis,
	FecVenLin
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(FecMaxDis,'') > ISNULL(FecVenLin,'') AND ISNULL(TipoAltaCredito,'') IN ('132','133','134','139','700','701','702','731','732','733','741','742','743');

END


GO
