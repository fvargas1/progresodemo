SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_027_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Alta (cve_tipo_alta_credito) es igual a 132, 136, 700, 701, 702, validar que el RFC contenido en el ID
-- metodología CNBV (dat_id_credito_met_cnbv) en las posiciones 14 a la 26, sea IGUAL al RFC que se reporta (dat_rfc).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('132','136','700','701','702') AND CHARINDEX(ISNULL(RFC,''), ISNULL(CodigoCreditoCNBV,'')) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
