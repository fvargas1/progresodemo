SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_ListActivos]
AS
SELECT
 vw.IdArchivo,
 vw.Nombre,
 vw.Codename,
 vw.JobCodename,
 vw.ErrorCodename,
 vw.LogCodename,
 vw.ClearLogCodename,
 vw.Position,
 vw.Activo,
 vw.ViewName,
 vw.ValidationStatus,
 vw.LastValidationExecuted,
 vw.InitCodename
FROM dbo.MIGRACION_VW_Archivo vw
WHERE vw.Activo = 1
ORDER BY vw.POSITION
GO
