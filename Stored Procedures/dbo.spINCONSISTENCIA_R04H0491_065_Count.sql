SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_065_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "PORCENTAJE QUE CUBRE EL SEGURO DE CRÉDITO A LA VIVIENDA " debe ser en base 100 y a dos decimales.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE LEN(SUBSTRING(PorcentajeCubiertoSeguro,CHARINDEX('.',PorcentajeCubiertoSeguro)+1,LEN(PorcentajeCubiertoSeguro))) <> 2;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
