SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_103]
AS

BEGIN

-- Si las Reservas por Garantías Personales (dat_reser_cubier_gtia_pers) son MAYORES o IGUALES a cero,
-- validar que el valor absoluto de la siguiente operación sea menor a 1000, cuando se trate de un crédito
-- sin fuente de pago propia (cve_proy_pago_anexo_19 = 2) :
-- ABS(dat_reser_cubier_gtia_pers - dat_sever_cubier_persona/100 * dat_exp_cubier_persona * dat_p_i_cubier_persona/100) <1000

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	ReservaCubierta,
	PICubierta,
	SPCubierta,
	EICubierta
FROM dbo.RW_R04C0459
WHERE CAST(ReservaCubierta AS DECIMAL) >= 0 AND ISNULL(ProyectoInversion,'') = '2'
	AND ABS(CAST(ReservaCubierta AS DECIMAL) -
	CAST((CAST(PICubierta AS DECIMAL(12,8))/100.00) * (CAST(SPCubierta AS DECIMAL(12,8))/100.00) * CAST(EICubierta AS DECIMAL) AS DECIMAL)) >= 1000;

END
GO
