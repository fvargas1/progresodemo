SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0482_002]
AS

BEGIN

-- La Resp. Total al Inicio del Periodo debe ser >= Saldo del Principal al Inicio del Periodo.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInsoluto,
	SaldoPrincipalInicio
FROM dbo.RW_VW_R04C0482_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'0') AS DECIMAL);

END


GO
