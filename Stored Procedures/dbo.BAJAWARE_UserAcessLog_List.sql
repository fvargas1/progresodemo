SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UserAcessLog_List]
AS
SELECT
	IdUserLog,
	Username,
	FechaAcceso,
	CASE [Status]
	WHEN 0 THEN 'Inició Sesión'
	WHEN 1 THEN 'Denegado'
	WHEN 2 THEN 'Bloqueado'
	END AS [Status],
	IPAddress
FROM dbo.BAJAWARE_UserAccesLog
ORDER BY FechaAcceso DESC;
GO
