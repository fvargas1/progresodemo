SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_UseCase_Group_MatrixList]
AS
SELECT IdUseCase as idRow, IdGroup as idColumn
FROM dbo.BAJAWARE_Rel_UseCase_Group
GO
