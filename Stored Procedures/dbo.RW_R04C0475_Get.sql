SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0475_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	GarantiaLeyFederal,
	CumpleCritContGub,
	P_AntSocCred,
	P_QuitCastReest,
	P_PorcPagoInstNoBanc,
	P_PorcPagoEntComer,
	P_CredAbInstBanc,
	P_MonMaxCred,
	P_MesesUltCredAb,
	P_PorcPagoInstBanc,
	P_PorcPagoInstBanc29,
	P_PorcPagoInstBanc90,
	P_DiasMoraInstBanc,
	P_PagosInstBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_IndPersFid,
	P_ProcOrigAdmon,
	AntSocCred,
	PorcPagoInstNoBanc,
	PorcPagoEntComer,
	CredAbInstBanc,
	MonMaxCred,
	MesesUltCredAb,
	PorcPagoInstBanc,
	PorcPagoInstBanc29,
	PorcPagoInstBanc90,
	DiasMoraInstBanc,
	PagosInstBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	IndPersFid,
	VentasNetasTotales
FROM dbo.RW_VW_R04C0475
ORDER BY NombrePersona;
GO
