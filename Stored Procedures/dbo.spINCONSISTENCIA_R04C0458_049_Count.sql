SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_049_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Las claves de Actividad Económica (cve_actividad_economica) válidas para los formularios de créditos a Entidades Financieras son:
-- 49311, 52111, 52211, 52221, 52222, 52231, 52232, 52239, 52241, 52242, 52243, 52244, 52245, 52249, 52251, 52311, 52312, 52321,
-- 52391, 52392, 52399, 52411, 52412, 52413, 52414, 52421, 52422, 53111, 53121, 53131, 53211 ó 53212.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE ActEconomica NOT IN (
	'49311', '52111', '52211', '52221', '52222', '52231', '52232', '52239', '52241', '52242', '52243', '52244', '52245', '52249', '52251',
	'52311', '52312', '52321', '52391', '52392', '52399', '52411', '52412', '52413', '52414', '52421', '52422', '53111', '53121', '53131',
	'53211', '53212'
)

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
