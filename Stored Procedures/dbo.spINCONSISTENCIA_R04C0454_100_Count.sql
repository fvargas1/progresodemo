SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_100_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- La clave de la Institución de Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo (cve_inst_bca_des_fondo) debe ser diferente de la Clave de la Institución (cve_institución).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @CodigoInstitucion VARCHAR(10);

SELECT @CodigoInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion'

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0454_INC
WHERE ISNULL(InstitucionFondeo,'') = @CodigoInstitucion

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
