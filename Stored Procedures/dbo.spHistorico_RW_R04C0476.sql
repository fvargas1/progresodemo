SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0476]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0476);

DELETE FROM Historico.RW_R04C0476 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0476 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, CodigoPersona, RFC, NombrePersona,
	PrctExpuesto, SP, EI_SinGarantia, [PI], NumeroGarRealFin, PrctGarRealFin, He, Hfx, Hc, ValorGarRealFin, SP_GarRealFin, EI_GarRealFin,
	NumeroGarRealNoFin, PrctGarRealNoFin, ValorDerechosCobro, ValorBienesInmuebles, ValorBienesMuebles, ValorFidPartFed, ValorFidIngProp,
	ValorOtrasGar, SP_DerechosCobro, SP_BienesInmuebles, SP_BienesMuebles, SP_FidPartFed, SP_FidIngProp, SP_OtrasGar, SP_GarRealNoFin,
	NumeroGarPersonales, PrctGarPersonales, NombreAval, PrctAval, TipoAval, RFCAval, TipoGarante, PI_Garante, ValuacionDerCredito,
	MonedaGarPersonal, NombreGaranteECPM, NombreGarantePP, PrctECPM, PrctPP, MontoPP
)
SELECT 
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	RFC,
	NombrePersona,
	PrctExpuesto,
	SP,
	EI_SinGarantia,
	[PI],
	NumeroGarRealFin,
	PrctGarRealFin,
	He,
	Hfx,
	Hc,
	ValorGarRealFin,
	SP_GarRealFin,
	EI_GarRealFin,
	NumeroGarRealNoFin,
	PrctGarRealNoFin,
	ValorDerechosCobro,
	ValorBienesInmuebles,
	ValorBienesMuebles,
	ValorFidPartFed,
	ValorFidIngProp,
	ValorOtrasGar,
	SP_DerechosCobro,
	SP_BienesInmuebles,
	SP_BienesMuebles,
	SP_FidPartFed,
	SP_FidIngProp,
	SP_OtrasGar,
	SP_GarRealNoFin,
	NumeroGarPersonales,
	PrctGarPersonales,
	NombreAval,
	PrctAval,
	TipoAval,
	RFCAval,
	TipoGarante,
	PI_Garante,
	ValuacionDerCredito,
	MonedaGarPersonal,
	NombreGaranteECPM,
	NombreGarantePP,
	PrctECPM,
	PrctPP,
	MontoPP
FROM dbo.RW_R04C0476
WHERE IdReporteLog = @IdReporteLog
GO
