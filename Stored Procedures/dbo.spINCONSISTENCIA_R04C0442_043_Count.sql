SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_043_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Tipo de garantía real debe existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA cat ON ISNULL(r.TipoGarantia,'') = cat.Codigo
WHERE cat.IdTipoGarantia IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
