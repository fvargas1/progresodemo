SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_TipoReserva5Prct]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	CAST(cat.Codigo AS INT) AS Codigo,
	cat.Nombre,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_TipoReserva5Prct cat
LEFT OUTER JOIN dbo.FILE_Persona_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'EsFondo' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'') LIKE '%' + @filtro + '%'
ORDER BY CAST(cat.Codigo AS INT);
GO
