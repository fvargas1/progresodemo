SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0453_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0453';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0453;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0453 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActEconomica, GrupoRiesgo, Localidad, Municipio,
 Estado, IdBuroCredito, LEI, TipoAltaCredito, TipoOperacion, DestinoCredito, CodigoCredito, CodigoCreditoCNBV, CodigoGlobalCNBV, MonLineaCred,
 FecMaxDis, FecVenLin, Moneda, FormaDisposicion, TipoLinea, Posicion, NumEmpLoc, NumEmpFedSHCP, NumRPPC, RegGarantiaMob, PorcPartFederal,
 TasaInteres, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa, PeriodicidadPagosCapital, PeriodicidadPagosInteres, NumMesesAmortCap,
 NumMesesPagoInt, ComisionAperturaTasa, ComisionAperturaMonto, ComisionDisposicionTasa, ComisionDisposicionMonto, LocalidadDestinoCredito,
 MunicipioDestinoCredito, EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0453',
 per.Codigo AS CodigoPersona,
 per.RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombreCNBV,
 tpoPer.CodigoCNBV AS TipoCartera,
 actEco.CodigoCNBV AS IdActividadEconomica,
 CASE WHEN LEN(ISNULL(pInfo.GrupoRiesgo,'')) = 0 OR pInfo.GrupoRiesgo LIKE '%SIN GRUPO%' THEN REPLACE(pInfo.NombreCNBV, ',', '') ELSE pInfo.GrupoRiesgo END AS GrupoRiesgo,
 mun.CodigoCNBV AS IdLocalidad,
 pInfo.Municipio AS Municipio,
 pInfo.Estado AS Estado,
 lin.FolioConsultaBuro AS IdBuroCredito,
 pInfo.LEI AS LEI,
 tpoAlta.CodigoCNBV AS IdTipoAlta,
 tpoOper.CodigoCNBV AS TipoOperacion,
 dest.CodigoCNBVEYM AS IdDestino,
 lin.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 grp.GrupalCNBV AS GrupalCnbv,
 lin.MontoLinea AS MonLineaCred,
 CASE WHEN lin.FecMaxDis IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecMaxDis,102),'.',''),1,6) END AS FecMaxDis,
 CASE WHEN lin.FecVenLinea IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecVenLinea,102),'.',''),1,6) END AS FecVenLinea,
 mon.CodigoCNBV AS IdMoneda,
 disp.CodigoCNBV AS IdDisposicion,
 tln.Codigo AS TipoLinea,
 pos.Codigo AS Posicion,
 lin.NumEmpLoc AS NumEmpLoc,
 lin.NumEmpLocFedSHCP AS NumEmpLocFedSHCP,
 rgm.NumRPPC AS NumRPPC,
 rgm.RegGarantiaMob AS RegGarantiaMob,
 lin.PorcPartFederal AS PorcPartFederal,
 tasaRef.Codigo AS IdTasaReferencia,
 tasa.AjusteTasa AS AjusteTasaReferencia,
 tasa.OperacionDiferencial AS OperacionTasaReferencia,
 lin.FrecuenciaRevisionTasa AS FrecuenciaRevisionTasa,
 perCap.CodigoCNBV AS IdPeriodicidadCapital,
 perInt.CodigoCNBV AS IdPeriodicidadInteres,
 lin.MesesGraciaCap AS MesesGraciaCap,
 lin.MesesGraciaIntereses AS MesesGraciaIntereses,
 lin.GastosOrigTasa AS ComisionAperturaTasa,
 lin.ComisionesCobradas AS ComisionAperturaMonto,
 lin.ComDispTasa AS ComisionDisposicionTasa,
 lin.ComDispMonto AS ComisionDisposicionMonto,
 munDst.CodigoCNBV AS LocalidadDestinoCredito,
 munDst.CodigoMunicipio AS MunicipioDestinoCredito,
 munDst.CodigoEstado AS EstadoDestinoCredito,
 actDst.CodigoCNBV AS ActividadDestinoCredito
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Anexo18 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON per.IdActividadEconomica = actEco.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Municipio mun ON per.IdLocalidad = mun.IdMunicipio
LEFT OUTER JOIN dbo.SICC_TipoAlta tpoAlta ON lin.IdTipoAlta = tpoAlta.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON lin.TipoOperacion = tpoOper.IdTipoOperacion
LEFT OUTER JOIN dbo.SICC_Destino dest ON lin.IdDestino = dest.IdDestino
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_Grupal grp ON lin.IdLineaCredito = grp.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON lin.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON lin.IdDisposicion = disp.IdDisposicion
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON lin.TipoLinea = tln.IdTipoLinea
LEFT OUTER JOIN dbo.SICC_Posicion pos ON lin.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_RegGarMob rgm ON lin.IdLineaCredito = rgm.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON lin.IdTasaReferencia = tasaRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_AjusteTasa tasa ON lin.IdLineaCredito = tasa.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON lin.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON lin.IdPeriodicidadInteres = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_Municipio munDst ON lin.IdMunicipioDestino = munDst.IdMunicipio
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actDst ON lin.IdActividadEconomicaDestino = actDst.IdActividadEconomica
WHERE met.Codigo IN ('4','18') AND lin.IdTipoAlta IS NOT NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0453 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
