SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_Reservas_A22]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_Reservas_A22);

DELETE FROM Historico.RW_Analitico_Reservas_A22 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_Reservas_A22 (
 IdPeriodoHistorico, Fecha, CodigoPersona, Nombre, CodigoCredito, MontoCredito, FechaVencimiento, [PI], Moneda, ActividadEconomica,
 MontoGarantia, MontoGarantiaAjustado, Prct_Reserva, SP, MontoReserva, PI_Aval, SP_Aval, Calificacion, ReservaAdicional
)
SELECT
 @IdPeriodoHistorico,
 Fecha,
 CodigoPersona,
 Nombre,
 CodigoCredito,
 MontoCredito,
 FechaVencimiento,
 [PI],
 Moneda,
 ActividadEconomica,
 MontoGarantia,
 MontoGarantiaAjustado,
 Prct_Reserva,
 SP,
 MontoReserva,
 PI_Aval,
 SP_Aval,
 Calificacion,
 ReservaAdicional
FROM dbo.RW_Analitico_Reservas_A22
WHERE IdReporteLog = @IdReporteLog;
GO
