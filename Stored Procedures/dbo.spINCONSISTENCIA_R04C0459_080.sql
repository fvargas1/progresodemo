SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_080]
AS
BEGIN
-- Validar que si la columna de Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo
-- (cve_instituciones) es DIFERENTE de cero, entonces el Monto Fondeado por Banco de Desarrollo o Fondo
-- de Fomento (dat_monto_fondea_b_desarrollo) debe ser DIFERENTE de cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoBancaDesarrollo,
	InstitucionFondeo
FROM dbo.RW_VW_R04C0459_INC
WHERE InstitucionFondeo <> '0' AND CAST(MontoBancaDesarrollo AS DECIMAL) = 0;

END


GO
