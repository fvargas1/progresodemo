SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_CalculandoConceptos_2016]
AS

TRUNCATE TABLE R04.[0419Conceptos_2016];

-- Calculamos los conceptos historicos.
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 Codigo,
 '139200000000',
 CAST(ReservaHistorico AS DECIMAL) + CAST(IntCarVencHistorico AS DECIMAL) + CAST(ReservasAdicHistorico AS DECIMAL)
FROM R04.[0419Datos_2016]
WHERE (ReservaHistorico + IntCarVencHistorico + ReservasAdicHistorico) <> 0 AND CodigoProducto IS NOT NULL;

-- Calculamos los conceptos totales actuales.
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 Codigo,
 '139000000000',
 CAST(ReservaActual AS DECIMAL) + CAST(IntCarVencActual AS DECIMAL) + CAST(ReservasAdicActual AS DECIMAL)
FROM R04.[0419Datos_2016]
WHERE (ReservaActual + IntCarVencActual + ReservasAdicActual) <> 0 AND CodigoProducto IS NOT NULL AND SaldoActual > 0;


-- ALTAS DEL MES
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(dat.ReservaActual AS DECIMAL),
 'A'
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CREADI'
WHERE dat.TipoAlta = 'COMPRA';


-- BAJAS DEL MES
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(dat.ReservaHistorico + dat.IntCarVencHistorico + dat.ReservasAdicHistorico AS DECIMAL),
 'C'
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = dat.TipoBaja
WHERE dat.TipoAlta IS NULL;


-- MOVIMIENTOS CARGOS
/*INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(SUM(mov.Monto * dat.PrctReservasHistorico) AS DECIMAL),
 'C'
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_0419_Movimientos mov ON dat.Codigo = mov.Codigo
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND mov.TipoMovimiento = conf.TipoMovimiento
WHERE dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL
GROUP BY dat.Codigo, conf.Concepto;*/


-- AJUSTE CALIFICACION
/*INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(ABS(dat.AjusteCalificacion) AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND CASE WHEN dat.AjusteCalificacion > 0 THEN 'CANCELACION' ELSE 'CRECAL' END = conf.TipoMovimiento
WHERE dat.AjusteCalificacion <> 0 AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL;*/


-- AJUSTE CAMBIARIO
/*INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 ABS(dat.AjusteCambiario),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND CASE WHEN dat.AjusteCambiario > 0 THEN 'AJCAMBCARGO' ELSE 'AJCAMBABONO' END = conf.TipoMovimiento
WHERE dat.AjusteCambiario <> 0 AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL;*/


-- INTERESES CARTERA VENCIDA
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST((dat.IntCarVencActual - dat.IntCarVencHistorico) + (dat.ReservasAdicActual - dat.ReservasAdicHistorico) AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'ADICIONALES'
WHERE dat.IntCarVencActual > dat.IntCarVencHistorico;


-- AUMENTO POR CALIFICACION CUANDO NO HAY MOVIMIENTOS
/*INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(((dat.SaldoActual - dat.SaldoHistorico) - ISNULL(mov.Monto,0)) * dat.PrctReservasActual AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CRECAL'
LEFT OUTER JOIN (
	SELECT Codigo, CAST(SUM(Monto) AS DECIMAL(23,2)) AS Monto
	FROM SICCMX_VW_0419_Movimientos
	GROUP BY Codigo
) AS mov ON mov.Codigo = dat.Codigo
WHERE ((dat.SaldoActual - dat.SaldoHistorico) - ISNULL(mov.Monto,0)) > 0 AND dat.SaldoHistorico > 0;*/


-- CARGOS
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(dat.ReservaHistorico AS DECIMAL) - CAST(dat.ReservaActual AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.ReservaHistorico > dat.ReservaActual AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL;

INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST((dat.IntCarVencHistorico - dat.IntCarVencActual) + (dat.ReservasAdicHistorico - dat.ReservasAdicActual) AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CANCELACION'
WHERE dat.IntCarVencHistorico > dat.IntCarVencActual;


-- ABONOS
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto, CargoAbono)
SELECT
 dat.Codigo,
 conf.Concepto,
 CAST(dat.ReservaActual AS DECIMAL) - CAST(dat.ReservaHistorico AS DECIMAL),
 conf.CargoAbono
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Configuracion_2016] conf ON dat.CodigoProducto = conf.CodigoProducto AND conf.TipoMovimiento = 'CRECAL'
WHERE dat.ReservaActual > dat.ReservaHistorico AND dat.TipoAlta IS NULL AND dat.TipoBaja IS NULL;
GO
