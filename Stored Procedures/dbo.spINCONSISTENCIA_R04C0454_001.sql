SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_001]
AS
BEGIN
-- Debe anotarse el número de disposición del crédito

SELECT
	CodigoCredito,
	NumeroDisposicion
FROM dbo.RW_VW_R04C0454_INC
WHERE LEN(ISNULL(NumeroDisposicion,'')) = 0;

END
GO
