SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_077_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Tipo de Cartera corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC rep
LEFT OUTER JOIN dbo.SICC_TipoPersona tpo ON ISNULL(rep.TipoCartera,'') = tpo.CodigoCNBV AND tpo.A22OD = 1
WHERE tpo.IdTipoPersona IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
