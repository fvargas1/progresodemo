SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ComercialInfo_Select]
	@IdCredito BIGINT
AS
SELECT
	vw.IdCredito,
	vw.IdTipoAlta,
	vw.IdTipoAltaNombre,
	vw.IdDisposicion,
	vw.IdDisposicionNombre,
	vw.AjusteTasaReferencia,
	vw.MontoBancaDesarrollo,
	vw.IdInstitucionFondeo,
	vw.IdInstitucionFondeoNombre,
	vw.GastosOriginacion,
	vw.NumeroDisposicion,
	vw.MontoDispuesto,
	vw.MontoPagoExigible,
	vw.MontoPagosRealizados,
	vw.MontoInteresPagado,
	vw.MontoComisionDevengada,
	vw.DiasVencido,
	vw.IdTipoBaja,
	vw.IdTipoBajaNombre,
	vw.MontoBonificacion,
	vw.CodigoCnbv,
	vw.NumeroConsulta,
	vw.SaldoInicial,
	vw.SaldoFinal,
	vw.SpecsDisposicion,
	vw.CodigoBancoGarantia
FROM dbo.SICCMX_VW_CreditoInfo vw
WHERE vw.IdCredito = @IdCredito;
GO
