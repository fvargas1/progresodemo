SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_RCCalificacion]
AS
-- ACTUALIZAMOS LA CALIFICACION PARA LARGO PLAZO
UPDATE inf
SET IdRCCalificacion = cal.IdRCCalificacion
FROM dbo.SICCMX_PersonaInfo inf
INNER JOIN dbo.SICC_RCCalificacion cal ON inf.IdAgenciaLargoPlazo = cal.IdRCAgenciaCal AND inf.CalificacionLargoPlazo = cal.Calificacion
WHERE inf.IdRCCalificacion IS NULL;

-- ACTUALIZAMOS LA CALIFICACION PARA CORTO PLAZO
UPDATE inf
SET IdRCCalificacionCortoPlazo = cal.IdRCCalificacion
FROM dbo.SICCMX_PersonaInfo inf
INNER JOIN dbo.SICC_RCCalificacion cal ON inf.IdAgenciaCortoPlazo = cal.IdRCAgenciaCal AND inf.CalificacionCortoPlazo = cal.Calificacion
WHERE inf.IdRCCalificacionCortoPlazo IS NULL;

-- ASIGNAMOS LA MISMA CALIFICACION DE LARGO PLAZO CUANDO NO HAYA CALIFICACION DE CORTO PLAZO
UPDATE dbo.SICCMX_PersonaInfo SET IdRCCalificacionCortoPlazo = IdRCCalificacion WHERE IdRCCalificacionCortoPlazo IS NULL;
GO
