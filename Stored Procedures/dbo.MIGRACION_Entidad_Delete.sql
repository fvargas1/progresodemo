SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Entidad_Delete]
	@IdEntidad INT
AS
DELETE FROM dbo.MIGRACION_Entidad
WHERE IdEntidad = @IdEntidad;
GO
