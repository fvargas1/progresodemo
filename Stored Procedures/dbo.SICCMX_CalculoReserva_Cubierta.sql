SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoReserva_Cubierta]
AS
UPDATE crv
SET ReservaCubierta = ISNULL(cub.Reserva,0)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
LEFT OUTER JOIN (
 SELECT can.IdCredito, SUM(ISNULL(can.Reserva,0)) AS Reserva
 FROM dbo.SICCMX_Garantia_Canasta can
 WHERE can.EsDescubierto IS NULL
 GROUP BY can.IdCredito
) AS cub ON crv.IdCredito = cub.IdCredito;

UPDATE dbo.SICCMX_Credito_Reservas_Variables SET ReservaCubierta_GarPer = EI_Cubierta_GarPer * PI_Cubierta * SP_Cubierta;
GO
