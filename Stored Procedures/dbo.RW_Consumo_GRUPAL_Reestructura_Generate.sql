SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_GRUPAL_Reestructura_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodoHistorico BIGINT;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'CONSUMO' AND Nombre = '-GRUPAL_Reestructuras';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Consumo_GRUPAL_Reestructura;

INSERT INTO dbo.RW_Consumo_GRUPAL_Reestructura (
 IdReporteLog, FolioCredito, FechaReestructuraCredito, Reestructura, QuitasCondonacionesBonificacionesDescuentos, Folio2Credito
)
SELECT DISTINCT
 @IdReporteLog,
 info.CodigoCreditoReestructurado AS FolioCredito,
 CASE WHEN info.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaDisposicion,102),'.','/'),1,10) END AS FechaReestructuraCredito,
 rst.CodigoBanxico AS Reestructura,
 ISNULL(info.QuitasCondonacionesValorizado,0) + ISNULL(info.BonificacionesDescuentosValorizado,0) AS QuitasCondonacionesBonificacionesDescuentos,
 info.CreditoCodificado AS Folio2Credito
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_VW_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON info.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICC_TipoCreditoConsumo tcc ON con.IdTipoCredito = tcc.IdTipoCreditoConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo rst ON con.IdReestructura = rst.IdReestructuraConsumo
WHERE met.Codigo = '4' AND LEN(info.CodigoCreditoReestructurado) > 0;



-- SE REPORTAN LAS REESTRUCTURAS GENERADAS EL MES INMEDIATO ANTERIOR
-- PARA LOS MESES DE FEBRERO, ABRIL, JUNIO, AGOSTO, OCTUBRE Y DICIEMBRE
IF MONTH(@FechaPeriodo) IN (2, 4, 6, 8, 10, 12)
BEGIN

SELECT @IdPeriodoHistorico = IdPeriodoHistorico
FROM dbo.SICC_PeriodoHistorico
WHERE SUBSTRING(REPLACE(CONVERT(VARCHAR,Fecha,102),'.',''),1,6) = SUBSTRING(REPLACE(CONVERT(VARCHAR,DATEADD(MONTH,-1,@FechaPeriodo),102),'.',''),1,6) AND Activo = 1;

INSERT INTO dbo.RW_Consumo_GRUPAL_Reestructura (
 IdReporteLog, FolioCredito, FechaReestructuraCredito, Reestructura, QuitasCondonacionesBonificacionesDescuentos, Folio2Credito
)
SELECT DISTINCT
 @IdReporteLog,
 FolioCredito,
 FechaReestructuraCredito,
 Reestructura,
 QuitasCondonacionesBonificacionesDescuentos,
 Folio2Credito
FROM Historico.RW_Consumo_GRUPAL_Reestructura
WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

END

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Consumo_GRUPAL_Reestructura WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
