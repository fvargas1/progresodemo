SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Garantia_Consumo_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Garantia_Consumo_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Garantia_Consumo_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 CodigoCliente,
 IndPerMorales,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 FechaValuacion,
 Descripcion,
 BancoGarantia,
 ValorGarantiaProyectado,
 Hc,
 VencimientoRestante,
 GradoRiesgo,
 AgenciaCalificadora,
 Calificacion,
 Emisor,
 Escala,
 EsIPC,
 PIGarante,
 CodigoCliente,
 IndPerMorales,
 Fuente
FROM dbo.FILE_Garantia_Consumo_Hst;
GO
