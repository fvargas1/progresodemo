SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_Back_Hst]
AS

INSERT INTO dbo.FILE_Proyecto_Hst (
	CodigoCliente,
	CodigoProyecto,
	NombreDelProyecto,
	DescripciondelProyecto,
	NumeroLinea,
	Sobrecosto,
	MontoCubiertoPorTerceros,
	MesesContemplados,
	MesesAdicionales,
	VPTotal,
	UtilidadPerdidaAcumulada,
	Etapa,
	FechaInicioOper,
	TasaDescuentoVP,
	VentNetTotAnuales
)
SELECT
	pry.CodigoCliente,
	pry.CodigoProyecto,
	pry.NombreDelProyecto,
	pry.DescripciondelProyecto,
	pry.NumeroLinea,
	pry.Sobrecosto,
	pry.MontoCubiertoPorTerceros,
	pry.MesesContemplados,
	pry.MesesAdicionales,
	pry.VPTotal,
	pry.UtilidadPerdidaAcumulada,
	pry.Etapa,
	pry.FechaInicioOper,
	pry.TasaDescuentoVP,
	pry.VentNetTotAnuales
FROM dbo.FILE_Proyecto pry
LEFT OUTER JOIN dbo.FILE_Proyecto_Hst hst ON LTRIM(pry.CodigoProyecto) = LTRIM(hst.CodigoProyecto)
WHERE hst.CodigoProyecto IS NULL;
GO
