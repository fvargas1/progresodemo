SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_011]
AS

BEGIN

-- El tipo de personalidad jurídica debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.PersonalidadJuridica
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TipoPersonaMA cat ON ISNULL(r.PersonalidadJuridica,'') = cat.CodigoCNBV
WHERE cat.IdTipoPersona IS NULL;

END
GO
