SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_106]

AS



BEGIN



-- Se identifican acreditados (dat_rfc) en el formulario de Probabilidad que no fueron identificados en el reporte de Seguimiento.



SELECT

	rPI.CodigoPersona,

	rPI.NombrePersona,

	rPI.RFC

FROM dbo.RW_R04C0465 rPI

LEFT OUTER JOIN dbo.RW_R04C0464 rSE ON rPI.RFC = rSE.RFC

WHERE rSE.RFC IS NULL



END
GO
