SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Proyecto_errores_ClearLog]
AS
UPDATE dbo.FILE_Proyecto
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Proyecto_errores;
GO
