SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_036]
AS

BEGIN

-- El dato "ENTIDAD QUE OTORGA ESQUEMA DE COBERTURA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.EntidadCobertura
FROM dbo.RW_R04H0492 r
LEFT OUTER JOIN dbo.SICC_EntidadCobertura cat ON ISNULL(r.EntidadCobertura,'') = cat.CodigoCNBV
WHERE cat.IdEntidadCobertura IS NULL;

END
GO
