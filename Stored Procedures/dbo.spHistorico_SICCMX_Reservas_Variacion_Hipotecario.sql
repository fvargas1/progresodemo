SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Reservas_Variacion_Hipotecario]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Reservas_Variacion_Hipotecario WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Reservas_Variacion_Hipotecario (
	IdPeriodoHistorico,
	Hipotecario,
	ReservaCalConsDesc,
	ReservaAdicConsDesc,
	ReservaPeriodoAnt,
	ReservaAdicPeriodoAnt
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	hip.Codigo,
	res.ReservaCalConsDesc,
	res.ReservaAdicConsDesc,
	res.ReservaPeriodoAnt,
	res.ReservaAdicPeriodoAnt
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Reservas_Variacion_Hipotecario res ON hip.IdHipotecario = res.IdHipotecario;
GO
