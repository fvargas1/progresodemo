SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_CumpleCritContGral_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_CumpleCritContGral_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET errorCatalogo = 1
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral cc ON LTRIM(f.CumpleCritContGral) = cc.Codigo
WHERE LEN(f.CumpleCritContGral) > 0 AND cc.IdCumpleCritContGral IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'CumpleCritContGral',
 f.CumpleCritContGral,
 2,
 @Detalle
FROM dbo.FILE_Credito f
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral cc ON LTRIM(f.CumpleCritContGral) = cc.Codigo
WHERE LEN(f.CumpleCritContGral) > 0 AND cc.IdCumpleCritContGral IS NULL;
GO
