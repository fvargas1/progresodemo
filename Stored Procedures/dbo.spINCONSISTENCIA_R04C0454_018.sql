SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_018]
AS
BEGIN
-- Valdar que el Monto Total Pagado Efectivamente por el Acreditado (dat_monto_pagado_total) en el periodo debe ser MAYOR o IGUAL a cero.

SELECT
	CodigoCredito,
	MontoTotalPagado
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(MontoTotalPagado,''),'0') AS DECIMAL) < 0;

END
GO
