SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_NumeroEmpleados]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_NumeroEmpleados';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Persona
SET errorFormato = 1
WHERE LEN(NumeroEmpleados) > 0 AND (ISNUMERIC(ISNULL(NumeroEmpleados,'0')) = 0 OR NumeroEmpleados LIKE '%[^0-9 ]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoCliente,
	'NumeroEmpleados',
	NumeroEmpleados,
	1,
	'No es un numero'
FROM dbo.FILE_Persona
WHERE LEN(NumeroEmpleados) > 0 AND (ISNUMERIC(ISNULL(NumeroEmpleados,'0')) = 0 OR NumeroEmpleados LIKE '%[^0-9 ]%');
GO
