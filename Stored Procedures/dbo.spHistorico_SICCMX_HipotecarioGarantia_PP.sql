SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_HipotecarioGarantia_PP]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_HipotecarioGarantia_PP WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_HipotecarioGarantia_PP (
 IdPeriodoHistorico,
 Codigo,
 Garantia,
 PrctReserva,
 Reserva,
 ReservaCubierta,
 ReservaExpuesta
)
SELECT
 @IdPeriodoHistorico,
 cr.Codigo,
 ga.Codigo,
 pp.PrctReserva,
 pp.Reserva,
 pp.ReservaCubierta,
 pp.ReservaExpuesta
FROM dbo.SICCMX_HipotecarioGarantia_PP pp
INNER JOIN dbo.SICCMX_Hipotecario cr ON pp.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICCMX_Garantia_Hipotecario ga ON pp.IdGarantia = ga.IdGarantia;
GO
