SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_051_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Monto de la Comisión por la Disposición del Crédito (dat_com_disposicion_monto) sea MAYOR O IGUAL a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(ISNULL(NULLIF(ComisionDisposicionMonto,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
