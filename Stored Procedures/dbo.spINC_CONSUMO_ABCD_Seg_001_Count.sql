SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_ABCD_Seg_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Tipo de Reestructura de ser 50 en el caso de créditos originales (que no provienen de una reestructura);
-- y 100 en el caso de créditos ABCD que ya fueron objeto de una reestructura y continúan siendo ABCD.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_ABCD rep
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo cat ON ISNULL(rep.Reestructura,'') = cat.CodigoBanxico AND ISNULL(cat.ClasificacionReporte,'ABCD') = 'ABCD'
WHERE cat.IdReestructuraConsumo IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
