SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Totales_Consumo_Gen]
AS
DECLARE @IdPeriodo INT;
DECLARE @FechaPeriodo DATETIME;
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

DELETE FROM dbo.RW_Totales_Por_Periodo_Consumo WHERE IdPeriodo = @IdPeriodo;

INSERT INTO dbo.RW_Totales_Por_Periodo_Consumo (IdPeriodo, FechaPeriodo, IdPersona, [PI], EI, EI_Cubierta, EI_Expuesta, Reserva, Reserva_Cubierta, Reserva_Expuesta)
SELECT
	@IdPeriodo,
	@FechaPeriodo,
	per.IdPersona,
	crv.[PI],
	SUM(crv.E),
	SUM(crv.ECubierta),
	SUM(crv.EExpuesta),
	SUM(crv.ReservaTotal),
	SUM(crv.ReservaCubierta),
	SUM(crv.ReservaExpuesta)
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Consumo con ON per.IdPersona = con.IdPersona
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo
GROUP BY per.IdPersona, crv.[PI];
GO
