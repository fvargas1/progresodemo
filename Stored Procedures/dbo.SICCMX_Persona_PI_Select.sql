SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_PI_Select]
 @IdPersona int
AS
SELECT
 vw.IdPersona,
 vw.FactorCuantitativo,
 vw.PonderadoCuantitativo,
 vw.FactorCualitativo,
 vw.PonderadoCualitativo,
 vw.FactorTotal,
 vw.PI,
 vw.IdMetodologia,
 vw.IdMetodologiaNombre,
 vw.PrctCuantitativo,
 vw.PrctCualitativo
FROM dbo.SICCMX_VW_Persona_PI vw
WHERE vw.IdPersona = @IdPersona;
GO
