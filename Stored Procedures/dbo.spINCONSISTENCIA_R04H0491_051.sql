SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_051]
AS

BEGIN

-- El "SECTOR LABORAL DEL ACREDITADO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.SectorLaboral
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_SectorLaboral cat ON ISNULL(r.SectorLaboral,'') = cat.Codigo
WHERE cat.IdSectorLaboral IS NULL;

END
GO
