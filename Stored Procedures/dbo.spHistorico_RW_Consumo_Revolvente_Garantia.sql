SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_Revolvente_Garantia]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_Revolvente_Garantia);

DELETE FROM Historico.RW_Consumo_Revolvente_Garantia WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_Revolvente_Garantia (
	IdPeriodoHistorico, FolioCredito, Garantia, GarantiaReal, ImporteGarantia, PI_Garante, SP, SP_SG, Hc, Hfx, PrctCobertura, Exposicion, EI_Ajustada,
	Reservas, ClaveGarante, RUGM, IdPortafolio
)
SELECT
	@IdPeriodoHistorico,
	FolioCredito,
	Garantia,
	GarantiaReal,
	ImporteGarantia,
	PI_Garante,
	SP,
	SP_SG,
	Hc,
	Hfx,
	PrctCobertura,
	Exposicion,
	EI_Ajustada,
	Reservas,
	ClaveGarante,
	RUGM,
	IdPortafolio
FROM dbo.RW_Consumo_Revolvente_Garantia
WHERE IdReporteLog = @IdReporteLog;
GO
