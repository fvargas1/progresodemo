SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_053]
AS

BEGIN

-- Validar que el Número de Meses de Gracia para Amortizar el Capital (dat_num_meses_gracia_capital) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	NumMesesAmortCap
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(NULLIF(NumMesesAmortCap,''),'-1') AS DECIMAL) < 0;

END


GO
