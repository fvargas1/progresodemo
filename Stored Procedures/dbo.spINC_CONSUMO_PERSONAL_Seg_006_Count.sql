SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_PERSONAL_Seg_006_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el "Tipo de Garantía" es "40" se debe reportar el "Registro Único de Garantías Mobiliarias"

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_PERSONAL rep
WHERE rep.TipoGarantia = '40' AND LEN(ISNULL(rep.RegistroUnicoGarantiasMobiliarias,'')) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
