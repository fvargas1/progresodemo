SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Hipotecario_Reservas]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Hipotecario_Reservas);

DELETE FROM Historico.RW_Hipotecario_Reservas WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Hipotecario_Reservas (
	IdPeriodoHistorico, Codigo, SaldoCapitalVigente, InteresVigente, SaldoCapitalVencido, InteresVencido, Constante, FactorATR, ATR, FactorMAXATR,
	MAXATR, FactorProPago, PorPago, FactorPorCLTV, PorCLTVi, FactorINTEXP, INTEXP, FactorMON, MON, [PI], SP, E, Reserva, PorReserva, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorMAXATR,
	MAXATR,
	FactorProPago,
	PorPago,
	FactorPorCLTV,
	PorCLTVi,
	FactorINTEXP,
	INTEXP,
	FactorMON,
	MON,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM dbo.RW_Hipotecario_Reservas
WHERE IdReporteLog = @IdReporteLog;
GO
