SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_024]
AS

BEGIN

-- El ID met CNBV se validará de la siguiente manera:
-- - La posición 1 debe ser = 2
-- - De la posición 2 a la posición 7 deberá ser igual al dato de la Col.2.
-- - De la posición 8 a la posición 13 corresponderá a la fecha de otorgamiento.
-- - De la posición 14 a la 26 corresponderá al dato que se anotó como RFC para el acreditado.
-- - De la posición 27 a 29 Corresponde a un número consecutivo que la Entidad asignará a los créditos otorgados para diferenciar créditos otorgados a un mismo acreditado.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, Entidad, @FechaPeriodo AS FechaAlta, RFC
FROM dbo.RW_R04C0442
WHERE TipoAltaCredito = '2' AND (SUBSTRING(CodigoCreditoCNBV,1,1) <> '2'
 OR SUBSTRING(CodigoCreditoCNBV,2,6) <> Entidad
 OR SUBSTRING(CodigoCreditoCNBV,8,6) <> @FechaPeriodo
 OR SUBSTRING(CodigoCreditoCNBV,14,13) <> RFC
 OR SUBSTRING(CodigoCreditoCNBV,27,3) LIKE '%[^0-9]%');

END
GO
