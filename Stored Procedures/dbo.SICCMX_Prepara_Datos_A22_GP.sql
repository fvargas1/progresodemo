SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Prepara_Datos_A22_GP]
AS
DECLARE @IdMetodologia INT;
DECLARE @IdPeriodo BIGINT;
DECLARE @IdMoneda BIGINT;
DECLARE @udis_menor1 MONEY;
DECLARE @udis_menor2 MONEY;
DECLARE @udis_mayor MONEY;
DECLARE @monto_udis_menor1 MONEY;
DECLARE @monto_udis_menor2 MONEY;
DECLARE @monto_udis_mayor MONEY;
DECLARE @TipoCambioUdis MONEY;

SET @udis_menor1 = NULL;
SET @udis_menor2 = NULL;
SET @udis_mayor = NULL;

SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo=1;
SELECT @IdMoneda = IdMoneda FROM dbo.SICC_Moneda WHERE Codigo = (SELECT VALUE FROM dbo.BAJAWARE_Config WHERE Codename='MonedaUdis');

--SE TOMA EL RANGO MENOR DE UDIS PARA PEQUEÑOS CORPORATIVOS DE BAJAWARE_Config CodeName='A22_UDIS_MENOR1'
SELECT @udis_menor1 = CONVERT(MONEY,Value) FROM dbo.BAJAWARE_Config where CodeName='A22_UDIS_MENOR1';
IF @udis_menor1 IS NULL SET @udis_menor1 = 14000000;

--SE TOMA EL RANGO MAYOR DE UDIS PARA PEQUEÑOS CORPORATIVOS DE BAJAWARE_Config CodeName='A22_UDIS_MENOR2'
SELECT @udis_menor2 = CONVERT(MONEY,Value) FROM dbo.BAJAWARE_Config where CodeName='A22_UDIS_MENOR2';
IF @udis_menor2 IS NULL SET @udis_menor2 = 54000000;

--SE TOMA EL RANGO DE UDIS PARA GRANDES CORPORATIVOS DE BAJAWARE_Config CodeName='A22_UDIS_MAYOR'
SELECT @udis_mayor = CONVERT(MONEY,Value) FROM dbo.BAJAWARE_Config where CodeName='A22_UDIS_MAYOR';
IF @udis_mayor IS NULL SET @udis_mayor = 216000000;


SELECT @TipoCambioUdis = Valor FROM dbo.SICC_TipoCambio WHERE IdPeriodo = @IdPeriodo AND IdMoneda = @IdMoneda;
SET @monto_udis_menor1 = @udis_menor1 * @TipoCambioUdis;
SET @monto_udis_menor2 = @udis_menor2 * @TipoCambioUdis;
SET @monto_udis_mayor = @udis_mayor * @TipoCambioUdis;


SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=22;

INSERT INTO dbo.SICCMX_Persona_PI_GP (IdGP, EsGarante, IdMetodologia, IdClasificacion)
SELECT DISTINCT IdGP, EsGarante, @IdMetodologia, CASE
 WHEN OrgDescPartidoPolitico = 1 THEN 4
 WHEN VentNetTotAnuales >= @monto_udis_menor1 AND VentNetTotAnuales < @monto_udis_menor2 THEN 1
 WHEN VentNetTotAnuales >= @monto_udis_mayor THEN 3
 ELSE 2 END
FROM dbo.SICCMX_Anexo22_GP;
GO
