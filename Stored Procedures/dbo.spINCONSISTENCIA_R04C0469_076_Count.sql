SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_076_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Validar que si la Situación del Crédito (cve_situacion_credito) es IGUAL a 2, el dato reportado en la columna de Número de días Vencidos (dat_num_vencidos) sea DIFERENTE de cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0469_INC rep
OUTER APPLY (
	SELECT TOP 1 vw.Codigo, vw.TipoAlta
	FROM dbo.SICCMX_VW_Credito_TiposAlta vw
	WHERE vw.Codigo = rep.NumeroDisposicion
	ORDER BY vw.Fecha DESC
) AS oa
WHERE rep.SituacionCredito = '2' AND CAST(ISNULL(NULLIF(rep.DiasAtraso,''),'0') AS INT) = 0 AND ISNULL(oa.TipoAlta,'') NOT IN ('133','731','732','733');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
