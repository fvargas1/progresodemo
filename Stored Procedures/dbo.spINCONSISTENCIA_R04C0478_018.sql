SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_018]
AS

BEGIN

-- Las instituciones NO deben presentar las siguientes claves de Actividad Económica (cve_actividad_economica):
-- 52111, 52211, 52221, 52222, 52231, 52232, 52239, 52241, 52242, 52243, 52244, 52245, 52249, 52251, 52311, 52312,
-- 52321, 52391, 52392, 52399, 52411, 52412, 52413, 52414, 52421, 52422, 53111, 53121, 53131, 53211, 53212, 93122,
-- 93123, 93132, 93133, 93142, 93143, 93152, 93153, 93162 y 93163

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ActEconomica
FROM dbo.RW_VW_R04C0478_INC
WHERE ISNULL(ActEconomica,'') IN ('',
	'52111', '52211', '52221', '52222', '52231', '52232', '52239', '52241', '52242', '52243', '52244', '52245', '52249',
	'52251', '52311', '52312', '52321', '52391', '52392', '52399', '52411', '52412', '52413', '52414', '52421', '52422',
	'53111', '53121', '53131', '53211', '53212', '93122', '93123', '93132', '93133', '93142', '93143', '93152', '93153',
	'93162', '93163'
);

END


GO
