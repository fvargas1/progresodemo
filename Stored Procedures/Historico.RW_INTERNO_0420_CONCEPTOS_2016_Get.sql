SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_INTERNO_0420_CONCEPTOS_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	CodigoProducto,
	Moneda,
	Concepto,
	Monto
FROM Historico.RW_INTERNO_0420_CONCEPTOS_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Codigo, Concepto;
GO
