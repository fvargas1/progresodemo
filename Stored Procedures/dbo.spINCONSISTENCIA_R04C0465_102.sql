SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_102]
AS

BEGIN

-- Validar que un mismo RFC Acreditado no tenga más de un Nombre del Acreditado diferente.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RFC
FROM dbo.RW_R04C0465
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_R04C0465 rep
	INNER JOIN dbo.RW_R04C0465 rep2 ON rep.RFC = rep2.RFC
	AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.RFC
);

END
GO
