SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_016_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Las calificaciones válidas son: A1, A2, B1, B2, B3, C1, C2, D, E, EX ó 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cat ON ISNULL(r.CalificacionCreditoCubiertaCNBV,'') = cat.Codigo
WHERE cat.IdCalificacion IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
