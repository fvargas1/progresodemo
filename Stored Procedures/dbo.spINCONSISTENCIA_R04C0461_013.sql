SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_013]
AS

BEGIN

-- Validar que un mismo ID Crédito Metodología CNBV no tenga más de un Nombre de Acreditado.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	CodigoCreditoCNBV
FROM dbo.RW_R04C0461 rep
WHERE CodigoCreditoCNBV IN (
	SELECT rep.CodigoCreditoCNBV
	FROM dbo.RW_R04C0461 rep
	INNER JOIN dbo.RW_R04C0461 rep2 ON rep.CodigoCreditoCNBV = rep2.CodigoCreditoCNBV AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.CodigoCreditoCNBV
);

END
GO
