SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0415Datos_CalculandoConceptos_2016]
AS
UPDATE dat
SET dat.Concepto = conf.Concepto
FROM R04.[0415Datos_2016] dat
INNER JOIN R04.[0415Configuracion_2016] conf ON conf.TipoProducto = dat.TipoProducto AND conf.SituacionCredito = dat.SituacionCredito;
GO
