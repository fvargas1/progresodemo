SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_UpdPendingAll]
	@sessionID VARCHAR(100)
AS
UPDATE dbo.MIGRACION_ActualizacionMasiva_Temp
SET seActualiza = 1
WHERE sessionID = @sessionID;
GO
