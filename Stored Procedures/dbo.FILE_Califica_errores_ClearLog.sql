SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Califica_errores_ClearLog]
AS
UPDATE dbo.FILE_Califica
SET
errorFormato = NULL,
errorCatalogo = NULL;
TRUNCATE TABLE dbo.FILE_Califica_errores;
GO
