SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_026]
AS

BEGIN

-- Se deberá anotar el monto de los intereses pagados. En caso de que no haya monto de intereses pagados, se anotará 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, MontoInteresPagado
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoInteresPagado,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
