SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_024]
AS

BEGIN

-- Si el Puntaje Asignado por Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (cve_ptaje_dias_mora_bcos) es = 34,
-- entonces los Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (dat_num_dias_mora_prom_bco) deben ser >= 10.12 y < 36.36

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasMoraInstBanc,
	P_DiasMoraInstBanc AS Puntos_DiasMoraInstBanc
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_DiasMoraInstBanc,'') = '34' AND (CAST(DiasMoraInstBanc AS DECIMAL(10,6)) < 10.12 OR CAST(DiasMoraInstBanc AS DECIMAL(10,6)) >= 36.36);

END

GO
