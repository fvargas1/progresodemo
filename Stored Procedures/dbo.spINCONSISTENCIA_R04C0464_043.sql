SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_043]

AS



BEGIN



-- Si la Fecha de Otorgamiento contenida en el ID metodología CNBV (dat_id_credito_met_cnbv) posiciones 8 a la 13

-- es igual al periodo que se reporta (cve_periodo), validar que el Saldo del Principal al Inicio del Periodo

-- (dat_saldo_princ_inicio) sea igual a cero.



DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;



SELECT

	CodigoCredito,

	NumeroDisposicion,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	CodigoCreditoCNBV,

	@FechaPeriodo AS FechaPeriodo,

	SaldoInicial

FROM dbo.RW_VW_R04C0464_INC

WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),8,6) = @FechaPeriodo AND ISNULL(NULLIF(SaldoInicial,''),'0') <> '0.00';



END
GO
