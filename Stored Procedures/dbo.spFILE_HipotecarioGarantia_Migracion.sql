SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_HipotecarioGarantia_Migracion]
AS
UPDATE hg
SET PorUsadoGarantia = 0,
	MontoUsadoGarantia = 0
FROM dbo.SICCMX_HipotecarioGarantia hg
INNER JOIN dbo.SICCMX_Hipotecario h ON hg.IdHipotecario = h.IdHipotecario
INNER JOIN dbo.SICCMX_Garantia_Hipotecario g ON hg.IdGarantia = g.IdGarantia
INNER JOIN dbo.FILE_HipotecarioGarantia f ON h.Codigo = LTRIM (f.CodigoCredito) AND g.Codigo = LTRIM(f.CodigoGarantia)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_HipotecarioGarantia (
	IdHipotecario,
	IdGarantia
)
SELECT
	(SELECT IdHipotecario FROM dbo.SICCMX_Hipotecario WHERE Codigo = LTRIM(f.CodigoCredito)),
	(SELECT IdGarantia FROM dbo.SICCMX_Garantia_Hipotecario WHERE Codigo = LTRIM(f.CodigoGarantia))
FROM dbo.FILE_HipotecarioGarantia f
WHERE f.CodigoGarantia+'-'+f.CodigoCredito
NOT IN (
	SELECT g.Codigo+'-'+hip.Codigo
	FROM dbo.SICCMX_HipotecarioGarantia hg
	INNER JOIN dbo.SICCMX_Garantia_Hipotecario g ON hg.IdGarantia = g.IdGarantia
	INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.IdHipotecario = hg.IdHipotecario
) AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
