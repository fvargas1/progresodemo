SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_102]
AS

BEGIN

-- Validar que el ID Acreditado Asigando (dat_id_acreditado_institucion) no tenga más de un Nombre Acreditado (dat_nombre).

SELECT
	CodigoCredito,
	REPLACE(CodigoPersona, ',', '') AS CodigoPersona,
	NombrePersona
FROM dbo.RW_VW_R04C0473_INC
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_VW_R04C0473_INC rep
	INNER JOIN dbo.RW_VW_R04C0473_INC rep2 ON rep.CodigoPersona = rep2.CodigoPersona AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.CodigoPersona
);

END


GO
