SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Datos_Credito]
AS
TRUNCATE TABLE dbo.SICCMX_Credito_Reservas_Variables;

INSERT INTO dbo.SICCMX_Credito_Reservas_Variables (IdCredito)
SELECT IdCredito FROM dbo.SICCMX_VW_Credito_NMC;
GO
