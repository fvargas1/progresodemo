SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_DeleteActualizados]
	@sessionID VARCHAR(100)
AS
DELETE FROM dbo.MIGRACION_ActualizacionMasiva_Temp
WHERE seActualiza = 1 AND sessionID = @sessionID;
GO
