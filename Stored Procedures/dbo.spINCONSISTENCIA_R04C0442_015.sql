SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_015]
AS

BEGIN

-- Los ingresos brutos del acreditado deben ser mayores o iguales a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, IngresosBrutos
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(IngresosBrutos,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
