SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_084_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Actividad Económica (cve_actividad_economica).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE ActEconomica IN (
	SELECT rep.ActEconomica
	FROM dbo.RW_VW_R04C0478_INC rep
	INNER JOIN dbo.RW_VW_R04C0478_INC rep2 ON rep.RFC = rep2.RFC AND rep.ActEconomica <> rep2.ActEconomica
	GROUP BY rep.ActEconomica, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
