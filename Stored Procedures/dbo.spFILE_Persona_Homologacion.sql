SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_Homologacion]

AS

DECLARE @Campo VARCHAR(50);

DECLARE @Tipo INT;

DECLARE @Fuente VARCHAR(50);



-- AGREGAMOS "_" A RFC CUANDO ESTE TIENE SOLO 12 POSICIONES (PERSONAS MORALES)

UPDATE dbo.FILE_Persona SET RFC = '_' + LTRIM(RTRIM(RFC)) WHERE LEN(LTRIM(RTRIM(RFC))) = 12;

UPDATE dbo.FILE_Persona SET CodigoPostal = Municipio

-- AGREGAMOS "0" AL CODIGO POSTAL CUANDO ESTE TIENE SOLO 4 POSICIONES

UPDATE dbo.FILE_Persona SET CodigoPostal = '0' + LTRIM(RTRIM(CodigoPostal)) WHERE LEN(LTRIM(RTRIM(CodigoPostal))) = 4;



-- ACTUALIZAMOS LA LOCALIDAD CUANDO RECIBIMOS EL CP

UPDATE f

SET Localidad = ISNULL(NULLIF(f.Localidad,''), mun.CodigoCNBV)

FROM dbo.FILE_Persona f

INNER JOIN dbo.SICC_Municipio mun ON LTRIM(f.CodigoPostal) = mun.CodigoMunicipio;

UPDATE dbo.FILE_Persona SET Municipio = ''

-- ACTUALIZAMOS EL ESTADO Y EL MUNICIPIO CUANDO RECIBIMOS SOLO LA LOCALIDAD

UPDATE f

SET Estado = ISNULL(NULLIF(f.Estado,''), loc.CodigoEstado),

 Municipio = ISNULL(NULLIF(f.Municipio,''), loc.CodigoMunicipio),

 Nacionalidad = ISNULL(NULLIF(f.Nacionalidad,''), loc.CodigoPais)

FROM dbo.FILE_Persona f

INNER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.Localidad) = loc.CodigoCNBV;



-- EJECUTAMOS VALIDACIONES CONFIGURADAS

DECLARE crs CURSOR FOR

SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente

FROM dbo.FILE_Persona_Homologacion

WHERE Activo=1

ORDER BY Tipo;



OPEN crs;



FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;



WHILE @@FETCH_STATUS = 0

BEGIN

EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Persona', 'FILE_Persona_Homologacion', @Campo, @Tipo, @Fuente;



FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

END



CLOSE crs;

DEALLOCATE crs;



UPDATE dbo.FILE_Persona SET Homologado = 1;
GO
