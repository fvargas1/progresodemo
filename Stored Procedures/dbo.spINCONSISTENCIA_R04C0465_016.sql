SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_016]
AS

BEGIN

-- Si el Puntaje Asignado por Días atrasados Infonavit en el último bimestre (cve_ptaje_dias_atra_infonavit) es = 49,
-- entonces los Días atrasados Infonavit en el último bimestre (dat_dias_mora_infonavit) debe ser = -999 (Sin Informacion)

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasAtrInfonavit,
	P_DiasAtrInfonavit AS Puntos_DiasAtrInfonavit
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_DiasAtrInfonavit,'') = '49' AND CAST(DiasAtrInfonavit AS DECIMAL(10,6)) <> -999;

END

GO
