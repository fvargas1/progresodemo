SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_007]


AS





BEGIN





-- El Nombre del Acreditado sólo se aceptan letras en mayúsculas y números, sin caracteres distintos a estos, sin puntos ni comas.





SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NombrePersona


FROM dbo.RW_R04C0442


WHERE NombrePersona LIKE '%[^A-Z0-9 ]%' AND CodigoCreditoCNBV NOT LIKE '%&%'  OR BINARY_CHECKSUM(NombrePersona) <> BINARY_CHECKSUM(UPPER(NombrePersona));





END
GO
