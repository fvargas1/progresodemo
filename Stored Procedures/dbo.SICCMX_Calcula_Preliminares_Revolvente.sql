SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Preliminares_Revolvente]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo = '5';

-- INICIALIZAMOS LAS VARIABLES CON LOS VALORES RECIBIDOS EN LA MIGRACION
UPDATE pre
SET Impago = info.Impago,
 NumeroImpagosConsecutivos = info.NumeroImpagosConsecutivos,
 NumeroImpagosHistoricos = info.NumeroImpagosHistoricos,
 PorcentajeUso = info.PorcentajeUso,
 PorPago = info.PorPago,
 Alto = info.Alto,
 Medio = info.Medio,
 Bajo = info.Bajo
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;

-- CALCULAMOS VARIABLES SIN INFORMACION
UPDATE pre
SET Impago = ISNULL(pre.Impago, CASE WHEN ISNULL(info.PagoRealizado,0) < ISNULL(info.MontoExigible,0) THEN 1 ELSE 0 END),
 PorcentajeUso = ISNULL(pre.PorcentajeUso, CASE WHEN pre.LimiteCredito > 0 THEN ISNULL(info.MontoExigible,0) / pre.LimiteCredito ELSE 0 END),
 PorPago = ISNULL(pre.PorPago, CASE WHEN info.MontoExigible > 0 THEN info.PagoRealizado / info.MontoExigible ELSE 0 END),
 Alto = ISNULL(pre.Alto, amb.Alto),
 Medio = ISNULL(pre.Medio, amb.Medio),
 Bajo = ISNULL(pre.Bajo, amb.Bajo)
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_VW_Consumo_AMB amb ON info.IdConsumo = amb.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;

UPDATE pre
SET NumeroImpagosConsecutivos = ISNULL(pre.NumeroImpagosConsecutivos, vw.ImpagoConsecutivos),
 NumeroImpagosHistoricos = ISNULL(pre.NumeroImpagosHistoricos, vw.ImpagosHistoricos)
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_ConsumoInfo info ON pre.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_VW_Historico_Consumo vw ON pre.IdConsumo = vw.IdConsumo
WHERE pre.IdMetodologia = @IdMetodologia;
GO
