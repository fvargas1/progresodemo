SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0491_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'H-0491';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04H0491;

INSERT INTO dbo.RW_R04H0491 (
	IdReporteLog, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, ProductoHipotecario, CategoriaCredito, TipoAlta,
	DestinoCredito, FechaOtorgamiento, FechaVencimiento, DenominacionCredito, MontoOriginal, Comisiones, MontoSubsidioFederal, EntidadCoFin, MontoSubCuenta,
	MontoOtorgadoCoFin, Apoyo, ValorOriginalVivienda, ValorAvaluo, NumeroAvaluo, Localidad, FechaFirmaReestructura, FechaVencimientoReestructura, MontoReestructura,
	DenominacionCreditoReestructura, IngresosMensuales, TipoComprobacionIngresos, SectorLaboral, NumeroConsultaSIC, PeriodicidadAmortizaciones, TipoTasaInteres,
	TasaRef, AjusteTasaRef, SeguroAcreditado, TipoSeguro, EntidadSeguro, PorcentajeCubiertoSeguro, MontoSubCuentaGarantia, INTEXP, SDES, Municipio, Estado
)
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'491',
	ROW_NUMBER() OVER ( ORDER BY hip.Codigo ASC ),
	hip.Codigo AS CodigoCredito,
	cnbv.CNBV AS CodigoCreditoCNBV,
	ISNULL(prod.CodigoCNBV,0) AS ProductoHipotecario,
	cat.CodigoCNBV AS CategoriaCredito,
	tpoAlta.CodigoCNBV AS TipoAlta,
	ISNULL(dest.CodigoCNBV, '0') AS DestinoCredito,
	CASE WHEN info.FechaOtorgamiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaOtorgamiento,102),'.',''),1,6) END AS FechaOtorgamiento,
	CASE WHEN info.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaVencimiento,102),'.',''),1,6) END AS FechaVencimiento,
	ISNULL(mon.CodigoCNBV_Hipo, '0') AS DenominacionCredito,
	CAST(ISNULL(info.MontoOriginal,0) AS DECIMAL) AS MontoOriginal,
	CAST(ISNULL(info.GastosOriginacion, 0) AS DECIMAL) AS Comisiones,
	CAST(ISNULL(info.MontoSubsidioFederal, 0) AS DECIMAL) AS MontoSubsidioFederal,
	ISNULL(cof.CodigoCNBV, '0') AS EntidadCoFin,
	CAST(ISNULL(info.MontoSubcuenta,0) AS DECIMAL) AS MontoSubCuenta,
	CAST(ISNULL(info.MontoCofinanciador, 0) AS DECIMAL) AS MontoOtorgadoCoFin,
	CAST(ISNULL(info.MontoApoyoBancaDesarrollo, 0) AS DECIMAL) AS Apoyo,
	CAST(ISNULL(info.ValorOriginal, 0) AS DECIMAL) AS ValorOriginalVivienda,
	CAST(ISNULL(info.ValorAvaluo,0) AS DECIMAL) AS ValorAvaluo,
	REPLACE(info.NumeroAvaluo, '-', '') AS NumeroAvaluo,
	ISNULL(loc.CodigoCNBV, '0') AS Localidad,
	CASE WHEN info.FechaReestructura IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaReestructura,102),'.',''),1,6) END AS FechaFirmaReestructura,
	CASE WHEN info.FechaVencimientoReestructura IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaVencimientoReestructura,102),'.',''),1,6) END AS FechaVencimientoReestructura,
	CAST(ISNULL(info.MontoReestructura, 0) AS DECIMAL) AS MontoReestructura,
	ISNULL(monRees.CodigoCNBV_Hipo, '0') AS DenominacionCreditoReestructura,
	CAST(ISNULL(info.IngresosMensuales, 0) AS DECIMAL) AS IngresosMensuales,
	ISNULL(ing.Codigo,'0') AS TipoComprobacionIngresos,
	ISNULL(sect.Codigo, '0') AS SectorLaboral,
	ISNULL(NULLIF(info.FolioBuroCredito,''), '0') AS NumeroConsultaSIC,
	ISNULL(perCap.CodigoCNBV, '0') AS PeriodicidadAmortizaciones,
	ISNULL(tasa.CodigoCNBV, '0') AS TipoTasaInteres,
	ISNULL(tasaRef.Codigo, '0') AS TasaRef,
	ISNULL(NULLIF(info.AjusteTasaReferencia,''), '0') AS AjusteTasaRef,
	ISNULL(seguro.CodigoCNBV, '0') AS SeguroAcreditado,
	ISNULL(tpoSeg.CodigoCNBV, '0') AS TipoSeguro,
	ISNULL(entAsg.CodigoCNBV, '0') AS EntidadSeguro,
	CAST(ISNULL(info.PorcentajeAsegurado, 0) AS DECIMAL(10,2)) AS PorcentajeCubiertoSeguro,
	CAST(ISNULL(pre.SUBCVi,0) AS DECIMAL) AS MontoSubCuentaGarantia,
	ISNULL(pre.INTEXP, 0) AS INTEXP,
	CAST(ISNULL(pre.SDESi,0) AS DECIMAL) AS SDES,
	ISNULL(NULLIF(pre.Municipio,''), '0') AS Municipio,
	ISNULL(NULLIF(pre.Estado,''), '0') AS Estado
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON hip.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_VW_HipotecarioCNBV cnbv ON hip.IdHipotecario = cnbv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_ProductoHipotecario prod ON info.IdProductoHipotecario = prod.IdProductoHipotecario
LEFT OUTER JOIN dbo.SICC_CategoriaCreditoHipotecario cat ON info.IdCategoria = cat.IdCategoriaHipotecario
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario tpoAlta ON info.IdTipoAlta = tpoAlta.IdTipoAltaHipotecario
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario dest ON info.IdDestinoHipotecario = dest.IdDestinoHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Cofinanciamiento cof ON info.IdEntidadCofinanciadora = cof.IdCofinanciamiento
LEFT OUTER JOIN dbo.SICC_Municipio loc ON info.IdLocalidad = loc.IdMunicipio
LEFT OUTER JOIN dbo.SICC_Moneda monRees ON info.IdMonedaReestructurado = monRees.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoIngreso ing ON pInfo.IdTipoIngreso = ing.IdTipoIngreso
LEFT OUTER JOIN dbo.SICC_SectorLaboral sect ON pInfo.IdSectorLaboral = sect.IdSectorLaboral
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalHipotecario perCap ON info.IdPeriodicidadCapital = perCap.IdPeriodicidadCapitalHipotecario
LEFT OUTER JOIN dbo.SICC_TasaInteresHipotecario tasa ON info.IdTipoTasaInteres = tasa.IdTasaInteresHipotecario
LEFT OUTER JOIN dbo.SICC_TasaReferenciaHipotecario tasaRef ON info.IdTasaReferencia = tasaRef.IdTasaReferenciaHipotecario
LEFT OUTER JOIN dbo.SICC_SeguroAcreditado seguro ON info.IdSeguroCargoCliente = seguro.IdSeguroAcreditado
LEFT OUTER JOIN dbo.SICC_TipoSeguro tpoSeg ON info.IdTipoSeguro = tpoSeg.IdTipoSeguro
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora entAsg ON info.IdEntidadAseguradora = entAsg.IdEntidadAseguradora
WHERE info.IdTipoAlta IS NOT NULL;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04H0491 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
