SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0442_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	GrupoRiesgo,
	TipoAcreditadoRel,
	PersonalidadJuridica,
	SectorEconomico,
	ActividadEconomica,
	NumeroEmpleados,
	IngresosBrutos,
	Localidad,
	NumeroConsulta,
	TipoAltaCredito,
	SpecsDisposicionCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	DestinoCredito,
	SaldoCredito,
	DenominacionCredito,
	FechaVencimiento,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	TasaRef,
	AjusteTasaRef,
	FrecuenciaRevisionTasa,
	MontoFondeo,
	CodigoBancoFondeador,
	Comisiones,
	PorcentajeCubierto,
	PorcentajeCubiertoFondeo,
	CodigoBancoGarantia,
	PorcentajeCubiertoAval,
	TipoGarantia,
	Municipio,
	Estado
FROM Historico.RW_R04C0442
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NombrePersona;
GO
