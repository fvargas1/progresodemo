SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_AUTO_Seg_002_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El campo "Escala de los Períodos de Facturación del Crédito" se debe reportar, según el Período de Facturación:
-- 10 = Semanal; 20 = Decenal; 30 = Catorcenal; 40 = Quincenal; 50 = Mensual, y 60 = Un solo pago al vencimiento tanto del capital como de los intereses.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_AUTO rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo cat ON rep.PeriodosFacturacionCredito = cat.CodigoBanxico
WHERE cat.IdPeriodicidadPagoConsumo IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
