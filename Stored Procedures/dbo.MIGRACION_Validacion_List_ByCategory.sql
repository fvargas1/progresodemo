SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Validacion_List_ByCategory]
	@Categoria VARCHAR(50)
AS
SELECT
	vw.IdValidacion,
	vw.Description,
	vw.Codename,
	vw.Position,
	vw.CategoryName,
	vw.FieldName,
	vw.Activo,
	vw.AplicaUpd,
	ISNULL(vw.ReqCalificacion,0) AS ReqCalificacion,
	ISNULL(vw.ReqReportes,0) AS ReqReportes
FROM dbo.MIGRACION_VW_Validacion vw
WHERE vw.CategoryName = @Categoria AND vw.Activo = 1
ORDER BY Position;
GO
