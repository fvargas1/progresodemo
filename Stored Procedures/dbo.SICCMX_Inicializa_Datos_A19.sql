SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Datos_A19]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo='4';

UPDATE dbo.SICCMX_Credito SET IdMetodologia = NULL WHERE IdMetodologia = @IdMetodologia;
GO
