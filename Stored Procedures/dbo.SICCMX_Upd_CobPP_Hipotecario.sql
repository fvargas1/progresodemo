SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_CobPP_Hipotecario]
AS
-- SE CALCULA EL PORCENTAJE Y EL MONTO QUE SE USARA DEL TOTAL DE LA GARANTIA PARA CADA UNO DE LOS CREDITOS CUBIERTOS
UPDATE hg
SET PorUsadoGarantia = CASE WHEN hgTotal.Total > 0 THEN dbo.NoN(hip.SaldoTotalValorizado, hgTotal.Total) ELSE 0 END,
 MontoUsadoGarantia = CASE WHEN hgTotal.Total > 0 THEN gar.ValorGtiaValorizado * (dbo.NoN(hip.SaldoTotalValorizado, hgTotal.Total)) ELSE 0 END
FROM dbo.SICCMX_HipotecarioGarantia AS hg
INNER JOIN dbo.SICCMX_VW_Hipotecario AS hip ON hg.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario gar ON hg.IdGarantia = gar.IdGarantia
INNER JOIN (
 SELECT hg2.IdGarantia, SUM(ISNULL(vwc.SaldoTotalValorizado, 0)) AS Total
 FROM dbo.SICCMX_HipotecarioGarantia hg2
 INNER JOIN dbo.SICCMX_VW_Hipotecario vwc ON hg2.IdHipotecario = vwc.IdHipotecario
 GROUP BY hg2.IdGarantia
) AS hgTotal ON gar.IdGarantia = hgTotal.IdGarantia
WHERE gar.IdTipoGarantiaCodigo='NMC-04-C';


-- SE CALCULA EL PORCENTAJE CUBIERTO DEL CREDITO POR PP
UPDATE pre
SET PorCOBPP = CASE WHEN hip.SaldoTotalValorizado > 0 THEN dbo.MIN2VAR(1, hg.MontoUsadoGarantia / hip.SaldoTotalValorizado) ELSE 0 END
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioGarantia hg ON pre.IdHipotecario = hg.IdHipotecario
INNER JOIN dbo.SICCMX_VW_Garantia_Hipotecario gar ON hg.IdGarantia = gar.IdGarantia
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON pre.IdHipotecario = hip.IdHipotecario
WHERE gar.IdTipoGarantiaCodigo = 'NMC-04-C';
GO
