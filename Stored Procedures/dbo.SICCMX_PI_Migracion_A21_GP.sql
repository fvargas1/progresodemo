SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_Migracion_A21_GP]
AS
DECLARE @IdVar INT;
DECLARE @Campo VARCHAR(50);
DECLARE @SENT VARCHAR(500);
DECLARE @IdMetodologia INT;

SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=21;

DELETE piDet
FROM dbo.SICCMX_Persona_PI_Detalles_GP piDet
INNER JOIN dbo.SICCMX_Anexo21_GP anx ON piDet.IdGP = anx.IdGP AND piDet.EsGarante = anx.EsGarante;


-- SIN ATRASO
DECLARE curAnexo CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=@IdMetodologia AND Campo IS NOT NULL AND Codigo LIKE '21SA[_]%';

OPEN curAnexo;

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles_GP (IdGP, EsGarante, IdVariable, ValorActual) ' +
'SELECT anx.IdGP, anx.EsGarante, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo21_GP anx ' +
'WHERE ISNULL(anx.SinAtrasos, 0) = 1';
EXEC(@SENT);

FETCH NEXT FROM curAnexo INTO @IdVar, @Campo;

END

CLOSE curAnexo;
DEALLOCATE curAnexo;


-- CON ATRASO
DECLARE curAnexo2 CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=@IdMetodologia AND Campo IS NOT NULL AND Codigo LIKE '21CA[_]%';

OPEN curAnexo2;

FETCH NEXT FROM curAnexo2 INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles_GP (IdGP, EsGarante, IdVariable, ValorActual) ' +
'SELECT anx.IdGP, anx.EsGarante, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo21_GP anx ' +
'WHERE ISNULL(anx.SinAtrasos, 0) <> 1';
EXEC(@SENT);

FETCH NEXT FROM curAnexo2 INTO @IdVar, @Campo;

END

CLOSE curAnexo2;
DEALLOCATE curAnexo2;
GO
