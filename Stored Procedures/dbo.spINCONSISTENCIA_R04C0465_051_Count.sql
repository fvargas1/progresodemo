SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_051_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- Si el Puntaje Asignado por Porcentaje de Pagos a Inst Financ Bcarias con 60 o más días de atraso en los últimos 24 meses (cve_ptaje_pgo_bco_60_dias_atra) es = 53,

-- entonces el Porcentaje de Pagos a Inst Financ Bcarias con 60 o más días de atraso en los últimos 24 meses (dat_porcen_pgo_bcos_60_dias) debe ser = -999 (Sin Informacion)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE ISNULL(P_PorcPagoInstBanc,'') = '53' AND CAST(PorcPagoInstBanc AS DECIMAL(10,6)) <> -99;



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
