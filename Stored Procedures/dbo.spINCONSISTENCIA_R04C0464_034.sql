SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_034]
AS

BEGIN

-- Las Reservas Totales (dat_reservas_totales) deben ser IGUALES (diferencia absoluta de $10.-)
-- a la suma de Reservas por Garantías Personales (dat_reservas_gtias_pers) y reservas por parte del acreditado (dat_reservas_acreditado).

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	CAST(ISNULL(NULLIF(ReservaCubierta,''),'0') AS DECIMAL) + CAST(ISNULL(NULLIF(ReservaExpuesta,''),'0') AS DECIMAL) AS [ReservaCubierta + ReservaExpuesta]
FROM dbo.RW_VW_R04C0464_INC
WHERE ABS(CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) - (
	CAST(ISNULL(NULLIF(ReservaCubierta,''),'0') AS DECIMAL) +
	CAST(ISNULL(NULLIF(ReservaExpuesta,''),'0') AS DECIMAL))) > 10;

END

GO
