SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_110_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Nacionalidad (cve_pais).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE Nacionalidad IN (
	SELECT rep.Nacionalidad
	FROM dbo.RW_VW_R04C0463_INC rep
	INNER JOIN dbo.RW_VW_R04C0463_INC rep2 ON rep.RFC = rep2.RFC AND rep.Nacionalidad <> rep2.Nacionalidad
	GROUP BY rep.Nacionalidad, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
