SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_002]
AS
BEGIN
-- El RFC de los acreditados debe ser de 13 posiciones.

SELECT
 CodigoCredito,
 REPLACE(NombreAcreditado, ',', '') AS NombreDeudor,
 RFC_Garante
FROM dbo.RW_VW_R04C0450_INC
WHERE LEN(ISNULL(RFC_Garante,'')) <> 13;

END
GO
