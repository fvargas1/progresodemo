SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_047]

AS



BEGIN



-- Validar que el Puntaje Crediticio Cualitativo (dat_ptaje_credit_cualit) sea >= 0 pero <= 263



SELECT

 CodigoPersona AS CodigoDeudor,

 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

 PuntajeCualitativo

FROM dbo.RW_VW_R04C0470_INC

WHERE CAST(PuntajeCualitativo AS DECIMAL) NOT BETWEEN 0 AND 752;



END
GO
