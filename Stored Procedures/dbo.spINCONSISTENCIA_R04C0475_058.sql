SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_058]
AS

BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_29_dias_atra) es = 87,
-- entonces el Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (dat_porcen_pgo_bco_29_dias) debe ser >= 95

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstBanc29,
	P_PorcPagoInstBanc29 AS Puntos_PorcPagoInstBanc29
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PorcPagoInstBanc29,'') = '87' AND CAST(PorcPagoInstBanc29 AS DECIMAL(10,6)) < 95;

END


GO
