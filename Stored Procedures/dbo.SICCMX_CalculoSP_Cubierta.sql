SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Cubierta]
AS
UPDATE crv
SET SP_Cubierta = ISNULL(per.SP,0)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
LEFT OUTER JOIN dbo.SICCMX_VW_Cobertura_Personales per ON crv.IdCredito = per.IdCredito;
GO
