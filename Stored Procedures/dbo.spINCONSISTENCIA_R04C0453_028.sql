SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_028]
AS
BEGIN
-- Validar que la Localidad corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.Localidad
FROM dbo.RW_VW_R04C0453_INC rep
LEFT OUTER JOIN dbo.SICC_Localidad loc ON ISNULL(rep.Localidad,'') = loc.CodigoCNBV
WHERE loc.IdLocalidad IS NULL;

END
GO
