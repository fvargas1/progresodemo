SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_OTRO_Seg_005]
AS

BEGIN

-- Se debe reportar el Código Postal del domicilio del acreditado, según el correspondiente registro de la Institución.

SELECT DISTINCT
	rep.FolioCredito,
	rep.CodigoPostal
FROM dbo.RW_Consumo_OTRO rep
LEFT OUTER JOIN dbo.SICC_Municipio cat ON rep.CodigoPostal = cat.CodigoMunicipio
WHERE cat.IdMunicipio IS NULL;

END
GO
