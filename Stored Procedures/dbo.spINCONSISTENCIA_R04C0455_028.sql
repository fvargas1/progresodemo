SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_028]
AS
BEGIN
-- Si el Saldo de Ingresos Propios (dat_saldo_ingresos_propios) es = 0,
-- entonces el Puntaje Ingresos Propios a Ingresos Totales (cve_puntaje_ingreso_propio) debe ser = 29 ó = 42

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoIngPropios,
	P_IngPropIngTotales AS Puntos_SdoIngPropios
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngPropios,''),'0') AS DECIMAL(18,6)) = 0 AND ISNULL(P_IngPropIngTotales,'') NOT IN ('29','42');

END
GO
