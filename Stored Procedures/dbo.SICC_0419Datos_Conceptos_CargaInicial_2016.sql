SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_Conceptos_CargaInicial_2016]
AS
-- ESTE PROCESO SOLO DEBE ESTAR ACTIVO PARA EL PRIMER PERIODO DE CALIFICACION
-- Y SOLO SI LA INSTITUCION CAPTURA LOS CONCEPTOS INICIALES EN LA PANTALLA DE CAMBIO DE PERIODO
DECLARE @Init0419 VARCHAR(10);
DECLARE @IdPeriodo INT;
SELECT @Init0419 = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CARGA_INIT_0419';
SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

IF @Init0419 <> '1'
BEGIN

DELETE FROM dbo.SICC_Conceptos_Periodo WHERE IdPeriodo = @IdPeriodo;

END
ELSE
BEGIN

DELETE FROM R04.[0419Conceptos_2016] WHERE Concepto = '139200000000';

INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoHistorico,
 ReservaHistorico,
 IntCarVencHistorico,
 ReservasAdicHistorico,
 PrctReservasHistorico,
 SaldoActual,
 ReservaActual,
 IntCarVencActual,
 ReservasAdicActual,
 PrctReservasActual,
 SaldoActualOrigen,
 AjusteCambiario,
 AjusteCalificacion,
 TipoBaja,
 TipoAlta
)
SELECT 'BWCI_' + ca.Moneda , -- Codigo - varchar(50)
 '010101' , -- CodigoProducto - varchar(10)
 CASE ca.Moneda WHEN '14' THEN '0' WHEN '4' THEN '1' ELSE '200' END, -- Moneda - varchar(50)
 0 , -- SaldoHistorico - decimal
 c.Monto , -- ReservaHistorico - decimal
 0 , -- IntCarVencHistorico - decimal
 0 , -- ReservasAdicHistorico - decimal
 0 , -- PrctReservasHistorico - decimal
 0 , -- SaldoActual - decimal
 0 , -- ReservaActual - decimal
 0 , -- IntCarVencActual - decimal
 0 , -- ReservasAdicActual - decimal
 0 , -- PrctReservasActual - decimal
 0 , -- SaldoActualOrigen - decimal
 0 , -- AjusteCambiario - decimal
 0 , -- AjusteCalificacion - decimal
 NULL , -- TipoBaja - varchar(50)
 NULL -- TipoAlta - varchar(50)
FROM dbo.SICC_Conceptos_A ca
INNER JOIN dbo.SICC_Conceptos_Periodo c ON ca.IdConcepto = c.IdConcepto AND c.IdPeriodo = @IdPeriodo
WHERE ca.SubReporte = '419' AND ca.Moneda <> '15';

-- CONCEPTOS HISTORICOS
INSERT INTO R04.[0419Conceptos_2016] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 '139200000000',
 CAST(ReservaHistorico + IntCarVencHistorico + ReservasAdicHistorico AS DECIMAL)
FROM R04.[0419Datos_2016] dat
WHERE (ReservaHistorico + IntCarVencHistorico + ReservasAdicHistorico) <> 0 AND dat.Codigo LIKE 'BWCI%';


CREATE TABLE #TEMP (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 Reserva DECIMAL(23,2) NULL,
 Moneda VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 Cargos DECIMAL(23,2) NULL,
 Abonos DECIMAL(23,2) NULL,
 ReservasDiff DECIMAL(23,2) NULL,
 PrctAjuste DECIMAL(16,10) NULL,
 ReservaAjustada DECIMAL(23,2) NULL
)

INSERT INTO #TEMP (Codigo, Reserva, Moneda)
SELECT
 dat.Codigo,
 dat.ReservaActual + dat.IntCarVencActual + dat.ReservasAdicActual,
 CASE
 WHEN dat.Moneda = '0' THEN '14'
 WHEN dat.Moneda IN ('20','200','202') THEN '9'
 ELSE '4' END
FROM R04.[0419Datos_2016] dat
WHERE dat.Codigo NOT LIKE 'BWCI%' AND (dat.ReservaActual + dat.IntCarVencActual + dat.ReservasAdicActual) > 0


UPDATE tmp
SET Cargos = mov.Cargos,
 Abonos = mov.Abonos,
 ReservasDiff = -(CAST(rt.Reserva AS DECIMAL) - CAST(init.SaldoReservaExpHistorico AS DECIMAL))
FROM #TEMP tmp
INNER JOIN (
 SELECT Codigo, SUM(CASE WHEN CargoAbono = 'C' THEN CAST(Monto AS DECIMAL) ELSE 0 END) AS Cargos, SUM(CASE WHEN CargoAbono = 'A' THEN CAST(Monto AS DECIMAL) ELSE 0 END) AS Abonos
 FROM R04.[0419Conceptos_2016]
 WHERE CargoAbono IS NOT NULL
 GROUP BY Codigo
) AS mov ON tmp.Codigo = mov.Codigo
INNER JOIN (
 SELECT Moneda, SUM(Reserva) AS Reserva
 FROM #TEMP
 GROUP BY Moneda
) AS rt ON tmp.Moneda = rt.Moneda
INNER JOIN R04.[0419Datos_2016] init ON tmp.Moneda = CASE init.Moneda WHEN '0' THEN '14' WHEN '200' THEN '9' ELSE '4' END AND init.Codigo LIKE 'BWCI%'


UPDATE tmp
SET PrctAjuste = tmp.ReservasDiff / (mov.Cargos - mov.Abonos)
FROM #TEMP tmp
INNER JOIN (
 SELECT Moneda, SUM(Cargos) AS Cargos, SUM(Abonos) AS Abonos
 FROM #TEMP
 GROUP BY Moneda
) AS mov ON tmp.Moneda = mov.Moneda


UPDATE #TEMP SET ReservaAjustada = Reserva * PrctAjuste;
--SELECT * FROM #TEMP
UPDATE con
SET Monto = ABS(con.Monto * tmp.PrctAjuste)
FROM R04.[0419Conceptos_2016] con
INNER JOIN #TEMP tmp ON con.Codigo = tmp.Codigo
WHERE con.CargoAbono IS NOT NULL

DROP TABLE #TEMP;

END
GO
