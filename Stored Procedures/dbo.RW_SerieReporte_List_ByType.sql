SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_SerieReporte_List_ByType]
 @ReportType VARCHAR(100) = NULL
AS
SELECT
 GrupoReporte,
 CASE WHEN GrupoReporte = 'R04' THEN 'Reportes Regulatorios'
 WHEN GrupoReporte = 'INTERNO' THEN 'Reportes Internos'
 WHEN GrupoReporte = 'AUDITORIA' THEN 'Reportes de Auditoría'
 WHEN GrupoReporte = 'RETORNO' THEN 'Retorno de SICC'
 ELSE 'Reportes Regulatorios' END AS Descripcion,
 COUNT( DISTINCT reporte.IdReporte ) AS TotalReportes
FROM dbo.RW_ReporteLog serie
INNER JOIN dbo.RW_Reporte reporte ON serie.IdReporte = reporte.IdReporte
WHERE ( ReglamentoOficial = @ReportType OR @ReportType IS NULL OR LTRIM( RTRIM( @ReportType ) ) = '' ) AND reporte.Activo=1
GROUP BY reporte.GrupoReporte;
GO
