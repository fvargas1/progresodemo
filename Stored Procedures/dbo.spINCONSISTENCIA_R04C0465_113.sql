SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_113]
AS

BEGIN

-- Si el Puntaje por Meses desde el último crédito abierto bancarias en los últimos 12 meses (cve_ptaje_meses_ultim_cred) es = 58,
-- entonces el Meses desde el último crédito abierto bancarias en los últimos 12 meses es >= 6

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MesesUltCredAb,
	P_MesesUltCredAb AS Puntos_MesesUltCredAb
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_MesesUltCredAb,'') = '58' AND CAST(MesesUltCredAb AS DECIMAL) < 6 AND CAST(MesesUltCredAb AS DECIMAL) <> -999;

END

GO
