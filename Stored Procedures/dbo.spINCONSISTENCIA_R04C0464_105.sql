SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_105]
AS
BEGIN
-- Si el crédito es calificado como Proyecto De Inversión Con Fuente De Pago Propia (cve_fuente_pago = 1),
-- entonces la Severidad de la Pérdida (dat_severidad_perdida), la Probabilidad de Incumplimiento y la Exposición al Incumplimiento deben ser igual a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	SPTotal,
	PITotal,
	EITotal
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(ProyectoInversion,'') = '1'
	AND (ISNULL(CAST(SPTotal AS VARCHAR),'') <> '0.000000' OR ISNULL(CAST(PITotal AS VARCHAR),'') <> '0.000000' OR ISNULL(CAST(EITotal AS VARCHAR),'') <> '0.00');

END
GO
