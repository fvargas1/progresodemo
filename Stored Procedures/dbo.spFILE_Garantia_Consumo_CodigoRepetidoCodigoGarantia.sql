SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_CodigoRepetidoCodigoGarantia]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Consumo_CodigoRepetidoCodigoGarantia';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia_Consumo f
WHERE f.CodigoGarantia IN (
	SELECT f.CodigoGarantia
	FROM dbo.FILE_Garantia_Consumo f
	GROUP BY f.CodigoGarantia
	HAVING COUNT(f.CodigoGarantia)>1
) AND LEN(f.CodigoGarantia) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoGarantia,
	'CodigoGarantia',
	f.CodigoGarantia,
	1,
	@Detalle
FROM dbo.FILE_Garantia_Consumo f
WHERE f.CodigoGarantia IN (
	SELECT f.CodigoGarantia
	FROM dbo.FILE_Garantia_Consumo f
	GROUP BY f.CodigoGarantia
	HAVING COUNT(f.CodigoGarantia)>1
) AND LEN(f.CodigoGarantia) > 0;
GO
