SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_CreditoAdicional]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_CreditoAdicional WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_CreditoAdicional (
	IdPeriodoHistorico,
	Credito,
	SaldoPromedio,
	InteresDevengado,
	Comision
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	cre.Codigo,
	cad.SaldoPromedio,
	cad.InteresDevengado,
	cad.Comision
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoAdicional cad ON cre.IdCredito = cad.IdCredito;
GO
