SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_033]
AS

BEGIN

-- El porcentaje cubierto con garantías personales debe estar entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PrctGarPersonales
FROM dbo.RW_VW_R04C0481_INC
WHERE CAST(ISNULL(NULLIF(PrctGarPersonales,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
