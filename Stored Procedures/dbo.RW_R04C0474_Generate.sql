SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0474_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0474';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());


SET @IdReporteLog = SCOPE_IDENTITY();

--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0474;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0474 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, CodigoPersona, RFC, NombrePersona, CategoriaCredito, FechaDisposicion,
 FechaVencDisposicion, Moneda, NumeroDisposicion, SaldoInicial, TasaInteres, MontoDispuesto, MontoPagoExigible, MontoCapitalPagado, MontoInteresPagado,
 MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado, MontoBonificado, SaldoFinal, DiasCalculoInteres, SaldoCalculoInteres, MontoInteresAplicar,
 ResponsabilidadFinal, SituacionCredito, DiasVencidos, FechaUltimoPago, ProyectoInversion, MontoBancaDesarrollo, InstitucionFondeo, ReservaTotal,
 ReservaCubierta, ReservaExpuesta, SPTotal, SPCubierta, SPExpuesta, EITotal, EICubierta, EIExpuesta, PITotal, PICubierta, PIExpuesta, GradoRiesgo,
 ReservaTotalInterna, SPInterna, EIInterna, PIInterna
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0474',
 cnbv.CNBV AS CodigoCreditoCNBV,
 cre.NumeroLinea AS CodigoCredito,
 per.Codigo AS CodigoPersona,
 per.RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 cat.CodigoCNBV AS CategoriaCredito,
 CASE WHEN cre.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaDisposicion,102),'.',''),1,6) END AS FechaDisposicion,
 CASE WHEN cre.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaVencimiento,102),'.',''),1,6) END AS FechaVencDisposicion,
 mon.CodigoCNBV AS Moneda,
 cre.CodigoCredito AS NumeroDisposicion,
 cInfo.SaldoInicial AS SaldoInicial,
 cre.TasaBruta AS TasaInteres,
 cInfo.MontoDispuesto AS MontoDispuesto,
 cinfo.MontoPagoExigible AS MontoPagoExigible,
 cInfo.MontoPagEfeCap AS MontoCapitalPagado,
 cInfo.MontoPagEfeInt AS MontoInteresPagado,
 cInfo.MontoPagEfeCom AS MontoComisionPagado,
 cInfo.MontoPagEfeIntMor AS MontoInteresMoratorio,
 cInfo.MontoPagosRealizados AS MontoTotalPagado,
 cInfo.MontoBonificacion AS MontoBonificado,
 cInfo.SaldoFinal AS SaldoFinal,
 cInfo.SaldoParaCalculoInteres AS SaldoCalculoInteres,
 cInfo.DiasCalculoInteres AS DiasCalculoInteres,
 cInfo.InteresDelMes AS MontoInteresAplicar,
 cre.MontoValorizado AS ResponsabilidadFinal,
 sitCre.Codigo AS SituacionCredito,
 cInfo.DiasMorosidad AS DiasVencidos,
 SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cInfo.FecPagExigibleRealizado,@FechaPeriodo),102),'.',''),1,6) AS FechaUltimoPago,
 CASE WHEN met.Codigo='4' THEN '1' ELSE '2' END AS ProyectoInversion,
 cInfo.MontoBancaDesarrollo AS MontoBancaDesarrollo,
 inst.Codigo AS InstitucionFondeo,
 crv.ReservaFinal AS ReservaTotal,
 crv.ReservaCubierta_GarPer AS ReservaCubierta,
 crv.ReservaExpuesta_GarPer AS ReservaExpuesta,
 crv.SP_Total * 100 AS SPTotal,
 crv.SP_Cubierta * 100 AS SPCubierta,
 crv.SP_Expuesta * 100 AS SPExpuesta,
 crv.EI_Total AS EITotal,
 crv.EI_Cubierta_GarPer AS EICubierta,
 crv.EI_Expuesta_GarPer AS EIExpuesta,
 crv.PI_Total * 100 AS PITotal,
 crv.PI_Cubierta * 100 AS PICubierta,
 crv.PI_Expuesta * 100 AS PIExpuesta,
 calNM.Codigo AS GradoRiesgo,
 '' AS ReservaTotalInterna,
 '' AS SPInterna,
 '' AS EIInterna,
 '' AS PIInterna
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pInfo.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Anexo21 anx ON cre.IdPersona = anx.IdPersona
LEFT OUTER JOIN dbo.SICMCX_VW_Clientes_A19_Rep anx19 ON per.IdPersona = anx19.IdPersona AND anx19.Metodologia = '21OD'
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICC_CategoriaCredito cat ON cInfo.IdCategoria = cat.IdCategoria
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON cre.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Institucion inst ON cInfo.IdInstitucionFondeo = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calNM ON crv.CalifFinal = calNM.IdCalificacion
WHERE ((met.Codigo='21' AND ISNULL(anx.OrgDescPartidoPolitico,0) = 1) OR (met.Codigo='4' AND anx19.IdPersona IS NOT NULL))
 AND (cre.MontoValorizado > 0 OR cInfo.IdTipoBaja IS NOT NULL OR cInfo.SaldoInicial > 0);

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0474 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
