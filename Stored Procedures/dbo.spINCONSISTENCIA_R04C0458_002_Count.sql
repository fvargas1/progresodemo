SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_002_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el RFC (dat_rfc) inicia con CNBX, validar que  sí corresponda a personas extranjeras y haya sido registrado en CNBV.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(RFC,'') LIKE 'CNBX%' AND Nacionalidad = '484';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
