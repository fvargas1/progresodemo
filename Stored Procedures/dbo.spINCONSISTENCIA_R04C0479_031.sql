SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_031]
AS

BEGIN

-- La Fecha de la Disposición del Crédito deberá ser menor que Fecha de Vencimiento de la disposición del crédito.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion,
	FechaVencDisposicion
FROM dbo.RW_VW_R04C0479_INC
WHERE FechaDisposicion >= FechaVencDisposicion;

END


GO
