SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04H0492_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	DenominacionCredito,
	SaldoPrincipalInicio,
	TasaInteres,
	ComisionesCobradasTasa,
	ComisionesCobradasMonto,
	MontoPagoExigible,
	MontoPagoRealizado,
	MontoBonificacion,
	SaldoPrincipalFinal,
	ResponsabilidadTotal,
	FechaUltimoPago,
	SituacionCredito,
	[PI],
	SP,
	DiasAtraso,
	MAXATR,
	PorVPAGO,
	SUBCV,
	ConvenioJudicial,
	PorCobPaMed,
	PorCobPP,
	EntidadCobertura,
	ReservasSaldoFinal,
	PerdidaEsperada,
	ReservaPreventiva,
	PIInterna,
	SPInterna,
	EInterna,
	PerdidaEsperadaInterna
FROM Historico.RW_R04H0492
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NumeroSecuencia;
GO
