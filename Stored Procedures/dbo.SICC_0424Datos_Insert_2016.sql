SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0424Datos_Insert_2016]
AS
DECLARE @IdPeriodoHistorico BIGINT;
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT @IdPeriodoHistorico = IdPeriodoHistorico
FROM dbo.SICC_PeriodoHistorico
WHERE YEAR(Fecha) = YEAR(DATEADD(MONTH, -1, @FechaPeriodo)) AND MONTH(Fecha) = MONTH(DATEADD(MONTH, -1, @FechaPeriodo)) AND Activo = 1;


TRUNCATE TABLE R04.[0424Datos_2016];

/* COMERCIAL */
-- Insertamos la Informacion Historica
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionHistorica,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresVencidoHistorico,
 IntCarVencHistorico,
 IntDevengadoHistorico
)
SELECT
 cr.Codigo,
 info.TipoProductoSerie4_2016,
 cr.Moneda,
 cr.SituacionCredito,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0) END,
 ISNULL(cr.InteresVigente,0),
 ISNULL(cr.InteresVencido,0),
 ISNULL(cr.InteresCarteraVencida,0),
 ISNULL(adi.InteresDevengado,0)
FROM Historico.SICCMX_Credito cr
INNER JOIN Historico.SICCMX_CreditoInfo info ON info.IdPeriodoHistorico = cr.IdPeriodoHistorico AND info.Credito = cr.Codigo
INNER JOIN Historico.SICCMX_CreditoAdicional adi ON cr.IdPeriodoHistorico = adi.IdPeriodoHistorico AND info.Credito = adi.Credito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.Moneda = mon.Codigo
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico
 AND (info.Posicion <> '181' AND info.TipoLinea <> '181') -- no se consideran cartas de credito


-- Actualizar info actual de los creditos historicos
UPDATE datos
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NOT NULL THEN tp.Codigo ELSE datos.CodigoProducto END,
 Moneda = mon.Codigo,
 SituacionActual = sc.Codigo,
 SaldoActual =
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL)
 ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0)
 END,
 InteresVigente = ISNULL(cr.InteresVigente,0),
 InteresVencido = ISNULL(cr.InteresVencido,0),
 IntCarVenc = ISNULL(cr.InteresCarteraVencida,0),
 IntDevengado = ISNULL(adi.InteresDevengadoValorizado,0),
 TipoAlta = CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '135' THEN 'COMPRA' ELSE 'OTORG' END END,
 TipoBaja = CASE tb.Codigo WHEN '133' THEN 'REEST' WHEN '136' THEN 'VENCAR' ELSE NULL END
FROM R04.[0424Datos_2016] datos 
INNER JOIN dbo.SICCMX_VW_Credito_NMC cr ON cr.CodigoCredito = datos.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo inf ON inf.IdCredito = cr.IdCredito
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON cr.IdCredito = adi.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = inf.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoAlta ta ON inf.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoBaja tb ON inf.IdTipoBaja = tb.IdTipoBaja
WHERE cr.Posicion <> '181' AND cr.TipoLinea <> '181'; -- no se consideran cartas de credito


-- Insertar creditos que no tienen historia
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionActual,
 SaldoActual,
 InteresVigente,
 InteresVencido,
 IntCarVenc,
 IntDevengado,
 TipoAlta,
 TipoBaja
)
SELECT
 cr.CodigoCredito,
 tp.Codigo,
 mon.Codigo,
 sc.Codigo,
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(cr.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(cr.SaldoCapitalVencido,0) AS DECIMAL)
 ELSE ISNULL(cr.SaldoCapitalVigente,0) + ISNULL(cr.SaldoCapitalVencido,0)
 END,
 ISNULL(cr.InteresVigente,0),
 ISNULL(cr.InteresVencido,0),
 ISNULL(cr.InteresCarteraVencida,0),
 ISNULL(adi.InteresDevengadoValorizado,0),
 CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '135' THEN 'COMPRA' ELSE 'OTORG' END END,
 CASE tb.Codigo WHEN '133' THEN 'REEST' WHEN '136' THEN 'VENCAR' ELSE NULL END
FROM dbo.SICCMX_VW_Credito_NMC cr
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cr.IdCredito = inf.IdCredito
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON cr.IdCredito = adi.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = inf.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoAlta ta ON inf.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoBaja tb ON inf.IdTipoBaja = tb.IdTipoBaja
LEFT OUTER JOIN R04.[0424Datos_2016] dat ON cr.CodigoCredito = dat.Codigo
WHERE (cr.Posicion <> '181' AND cr.TipoLinea <> '181') -- no se consideran cartas de credito
 AND dat.Id IS NULL;


-- CONSUMOS
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionHistorica,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresVencidoHistorico,
 IntCarVencHistorico,
 IntDevengadoHistorico
)
SELECT
 con.Codigo,
 info.TipoProductoSerie4_2016,
 info.Moneda,
 con.SituacionCredito,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0) END,
 ISNULL(con.InteresVigente,0),
 ISNULL(con.InteresVencido,0),
 ISNULL(con.InteresCarteraVencida,0),
 ISNULL(adi.InteresDevengado,0)
FROM Historico.SICCMX_Consumo con
INNER JOIN Historico.SICCMX_ConsumoInfo info ON info.IdPeriodoHistorico = con.IdPeriodoHistorico AND info.Consumo = con.Codigo
INNER JOIN Historico.SICCMX_ConsumoAdicional adi ON con.IdPeriodoHistorico = adi.IdPeriodoHistorico AND info.Consumo = adi.Consumo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE con.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar info actual de consumo historicos
UPDATE datos
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NOT NULL THEN tp.Codigo ELSE datos.CodigoProducto END,
 Moneda = mon.Codigo,
 SituacionActual = sc.Codigo,
 SaldoActual =
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL)
 ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0)
 END,
 InteresVigente = ISNULL(con.InteresVigente,0),
 InteresVencido = ISNULL(con.InteresVencido,0),
 IntCarVenc = ISNULL(con.InteresCarteraVencida,0),
 IntDevengado = ISNULL(adi.InteresDevengadoValorizado,0),
 TipoAlta = NULL,
 TipoBaja = CASE tb.Codigo WHEN '50' THEN 'VENCAR' ELSE NULL END
FROM R04.[0424Datos_2016] datos
INNER JOIN dbo.SICCMX_Consumo con ON con.Codigo = datos.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON info.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional adi ON con.IdConsumo = adi.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sc ON sc.IdSituacionConsumo = con.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoBajaConsumo tb ON info.IdTipoBaja = tb.IdTipoBajaConsumo;


-- Insertar consumos que estan en la tabla SICC_Consumo pero que no estan en Historico.SICCMX_Consumo
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionActual,
 SaldoActual,
 InteresVigente,
 InteresVencido,
 IntCarVenc,
 IntDevengado,
 TipoAlta,
 TipoBaja
)
SELECT
 con.Codigo,
 tp.Codigo,
 mon.Codigo,
 sc.Codigo,
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(con.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(con.SaldoCapitalVencido,0) AS DECIMAL)
 ELSE ISNULL(con.SaldoCapitalVigente,0) + ISNULL(con.SaldoCapitalVencido,0)
 END,
 ISNULL(con.InteresVigente,0),
 ISNULL(con.InteresVencido,0),
 ISNULL(con.InteresCarteraVencida,0),
 ISNULL(adi.InteresDevengadoValorizado,0),
 NULL,
 CASE tb.Codigo WHEN '50' THEN 'VENCAR' ELSE NULL END
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional adi ON info.IdConsumo = adi.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sc ON sc.IdSituacionConsumo = con.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoBajaConsumo tb ON info.IdTipoBaja = tb.IdTipoBajaConsumo
LEFT OUTER JOIN R04.[0424Datos_2016] dat ON con.Codigo = dat.Codigo
WHERE dat.Id IS NULL;


-- HIPOTECARIO
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionHistorica,
 SaldoHistorico,
 InteresVigenteHistorico,
 InteresVencidoHistorico,
 IntCarVencHistorico,
 IntDevengadoHistorico
)
SELECT
 hip.Hipotecario,
 hip.TipoCreditoR04A_2016,
 info.Moneda,
 info.SituacionCredito,
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL) ELSE ISNULL(hip.SaldoCapitalVigente,0) + ISNULL(hip.SaldoCapitalVencido,0) END,
 ISNULL(hip.InteresVigente,0),
 ISNULL(hip.InteresVencido,0),
 ISNULL(hip.InteresCarteraVencida,0),
 ISNULL(adi.InteresesDevengados,0)
FROM Historico.SICCMX_Hipotecario hip
INNER JOIN Historico.SICCMX_HipotecarioInfo info ON info.IdPeriodoHistorico = hip.IdPeriodoHistorico AND info.Hipotecario = hip.Hipotecario
INNER JOIN Historico.SICCMX_HipotecarioAdicional adi ON hip.IdPeriodoHistorico = adi.IdPeriodoHistorico AND hip.Hipotecario = adi.Hipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.Moneda = mon.Codigo
WHERE hip.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar info actual de hipotecario historicos
UPDATE datos
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NOT NULL THEN tp.Codigo ELSE datos.CodigoProducto END,
 Moneda = mon.Codigo,
 SituacionActual = sc.Codigo,
 SaldoActual =
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVigente,0) AS DECIMAL) + CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL)
 ELSE ISNULL(hip.SaldoCapitalVigente,0) + ISNULL(hip.SaldoCapitalVencido,0)
 END,
 InteresVigente = ISNULL(hip.InteresVigente,0),
 InteresVencido = ISNULL(hip.InteresVencido,0),
 IntCarVenc = ISNULL(hip.InteresCarteraVencida,0),
 IntDevengado = ISNULL(adi.InteresesDevengadosValorizado,0),
 TipoAlta = CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '5' THEN 'COMPRA' ELSE 'OTORG' END END,
 TipoBaja = CASE tb.Codigo WHEN '3' THEN 'REEST' WHEN '5' THEN 'VENCAR' ELSE NULL END
FROM R04.[0424Datos_2016] datos
INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.Codigo = datos.Codigo
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional adi ON hip.IdHipotecario = adi.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A_2016
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sc ON info.IdSituacionCredito = sc.IdSituacionHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario ta ON info.IdTipoAlta = ta.IdTipoAltaHipotecario
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario tb ON info.IdTipoBaja = tb.IdTipoBajaHipotecario;


-- Insertar hipotecarios que estan en la tabla SICC_Hipotecario pero que no estab en Historico.SICCMX_Hipotecario
INSERT INTO R04.[0424Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SituacionActual,
 SaldoActual,
 InteresVigente,
 InteresVencido,
 IntCarVenc,
 IntDevengado,
 TipoAlta,
 TipoBaja
)
SELECT
 hip.Codigo,
 tp.Codigo,
 mon.Codigo,
 sic.Codigo,
 CASE
 WHEN mon.CodigoISO = 'MXN' THEN CAST(ISNULL(hip.SaldoCapitalVencido,0) AS DECIMAL) + CAST(hip.SaldoCapitalVigente AS DECIMAL)
 ELSE ISNULL(hip.SaldoCapitalVencido,0) + hip.SaldoCapitalVigente
 END,
 ISNULL(hip.InteresVigente,0),
 ISNULL(hip.InteresVencido,0),
 ISNULL(hip.InteresCarteraVencida,0),
 ISNULL(adi.InteresesDevengadosValorizado,0),
 CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '5' THEN 'COMPRA' ELSE 'OTORG' END END,
 CASE tb.Codigo WHEN '3' THEN 'REEST' WHEN '5' THEN 'VENCAR' ELSE NULL END
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional adi ON info.IdHipotecario = adi.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A_2016
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sic ON sic.IdSituacionHipotecario = info.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN R04.[0424Datos_2016] dat ON hip.Codigo = dat.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario ta ON info.IdTipoAlta = ta.IdTipoAltaHipotecario
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario tb ON info.IdTipoBaja = tb.IdTipoBajaHipotecario
WHERE dat.Id IS NULL;
GO
