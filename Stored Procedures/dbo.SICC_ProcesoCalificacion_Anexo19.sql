SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_Anexo19]
AS
-- CALCULAMOS EL VALOR DE LA GARANTIA POR PROYECTO
UPDATE pc
SET ValorGarantia = ISNULL(gar.ValorGarantia,0)
FROM dbo.SICCMX_ProyectoCalificacion pc
LEFT OUTER JOIN dbo.SICCMX_VW_Proyecto_Garantia gar ON pc.IdProyecto = gar.IdProyecto;


-- RESTAMOS EL VALOR DE LA GARANTIA AL SOBRECOSTO
UPDATE dbo.SICCMX_ProyectoCalificacion
SET SobrecostoGarantia = CASE WHEN (ISNULL(SobreCosto,0) - ValorGarantia) < 0 THEN 0 ELSE (ISNULL(SobreCosto,0) - ValorGarantia) END;


-- RESTAMOS EL VALOR DEL MONTO CUBIERTO POR TERCEROS AL SOBRECOSTO RESTANTE PARA DETERMINAR LA EXPOSICION
UPDATE dbo.SICCMX_ProyectoCalificacion
SET ExposicionSobrecosto = CASE WHEN (SobrecostoGarantia - ISNULL(MontoCubierto,0)) < 0 THEN 0 ELSE (SobrecostoGarantia - ISNULL(MontoCubierto,0)) END;


-- CALCULAMOS EL PORCENTAJE DE EXPOSICION
UPDATE pc
SET PorcenajeSobrecosto =
 CASE WHEN CASE WHEN ln.MontoLinea > 0 THEN (pc.ExposicionSobrecosto / ln.MontoLinea) ELSE 0 END > 0.2 THEN CASE WHEN ln.SaldoInsoluto > 0 THEN pc.ExposicionSobrecosto / ln.SaldoInsoluto ELSE 0 END ELSE 0.005 END
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICCMX_VW_Proyecto_DatosLinea ln ON pc.IdProyecto = ln.IdProyecto;

-- EL PORCENTAJE NO PUEDE SER MAYOR A 1 (100%)
UPDATE dbo.SICCMX_ProyectoCalificacion SET PorcenajeSobrecosto = 1 WHERE PorcenajeSobrecosto > 1;


-- CALCULAMOS LA CALIFICACION DEL SOBRECOSTO
UPDATE pc
SET IdCalificacionSobrecosto = cal.IdCalificacion
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.PorcenajeSobrecosto BETWEEN cal.RangoMenor AND cal.RangoMayor;


-- CALCULAMOS EL SALDO INSOLUTO POR PROYECTO
UPDATE pc
SET SaldoInsoluto = vw.SaldoInsoluto
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICCMX_VW_Proyecto_DatosLinea vw ON pc.IdProyecto = vw.IdProyecto;


-- CALCULAMOS EL PORCENTAJE DE LA CORRIDA
UPDATE dbo.SICCMX_ProyectoCalificacion
SET PorcentajeCorrida = CASE WHEN (VPTotal - SaldoInsoluto) >= 0 THEN 0.005 ELSE CASE WHEN SaldoInsoluto > 0 THEN -((VPTotal - SaldoInsoluto) / SaldoInsoluto) END END;

-- EL PORCENTAJE NO PUEDE SER MAYOR A 1 (100%) NI MENOR A 0.005 (0.5%)
UPDATE dbo.SICCMX_ProyectoCalificacion SET PorcenajeSobrecosto = 1 WHERE PorcenajeSobrecosto > 1;
UPDATE dbo.SICCMX_ProyectoCalificacion SET PorcenajeSobrecosto = 0.005 WHERE PorcenajeSobrecosto < 0.005;


-- CALCULAMOS LA CALIFICACION DE LA CORRIDA
UPDATE pc
SET IdCalificacionCorrida = cal.IdCalificacion
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.PorcentajeCorrida BETWEEN cal.RangoMenor AND cal.RangoMayor;


-- CALCULAMOS LA CALIFICACION FINAL
UPDATE pc
SET PorcentajeFinal =
 CASE WHEN etp.Codigo='1' THEN CASE WHEN pc.PorcenajeSobrecosto > pc.PorcentajeCorrida THEN pc.PorcenajeSobrecosto ELSE pc.PorcentajeCorrida END
 ELSE pc.PorcentajeCorrida END,
 IdCalificacionFinal =
 CASE WHEN etp.Codigo='1' THEN CASE WHEN pc.IdCalificacionSobrecosto > pc.IdCalificacionCorrida THEN pc.IdCalificacionSobrecosto ELSE pc.IdCalificacionCorrida END
 ELSE pc.IdCalificacionCorrida END
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICC_Etapa etp ON pc.IdEtapa = etp.IdEtapa;


-- CALCULAMOS LAS RESERVAS
UPDATE pc
SET ReservaConstruccion = CASE WHEN etp.Codigo='1' THEN vw.SaldoInsoluto * pc.PorcentajeFinal ELSE 0 END,
 ReservaOperacion = CASE WHEN etp.Codigo='2' THEN vw.SaldoInsoluto * pc.PorcentajeFinal ELSE 0 END
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICCMX_VW_Proyecto_DatosLinea vw ON pc.IdProyecto = vw.IdProyecto
INNER JOIN dbo.SICC_Etapa etp ON pc.IdEtapa = etp.IdEtapa;
GO
