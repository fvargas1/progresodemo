SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_029_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- La fecha de disposición debe ser una fecha válida

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0454_INC
WHERE LEN(ISNULL(FechaDisposicion,'')) <> 6 OR ISDATE(ISNULL(FechaDisposicion,'') + '15') = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
