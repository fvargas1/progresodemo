SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Get_Metodologias_Activas]
AS
SELECT DISTINCT
	pc.IdMetodologiaGeneral,
	pc.IdMetodologia,
	mg.Nombre AS MetodologiaGeneral,
	CASE
	WHEN mg.Codigo = '1' THEN com.Codigo
	WHEN mg.Codigo = '2' THEN hip.Codigo
	WHEN mg.Codigo = '3' THEN con.Codigo
	ELSE ''
	END AS CodigoMetodologia,
	CASE
	WHEN mg.Codigo = '1' THEN com.Nombre
	WHEN mg.Codigo = '2' THEN hip.Descripcion
	WHEN mg.Codigo = '3' THEN con.Nombre
	ELSE ''
	END AS Metodologia
FROM dbo.SICC_ProcesoCalificacion pc
INNER JOIN dbo.SICCMX_Metodologia_General mg ON pc.IdMetodologiaGeneral = mg.IdMetodologia
LEFT OUTER JOIN dbo.SICCMX_Metodologia com ON pc.IdMetodologia = com.IdMetodologia AND mg.Codigo = '1'
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Metodologia hip ON pc.IdMetodologia = hip.IdMetodologiaHipotecario AND mg.Codigo = '2'
LEFT OUTER JOIN dbo.SICCMX_Consumo_Metodologia con ON pc.IdMetodologia = con.IdMetodologiaConsumo AND mg.Codigo = '3'
WHERE pc.Activo = 1 AND pc.IdMetodologia IS NOT NULL
ORDER BY pc.IdMetodologiaGeneral, pc.IdMetodologia;
GO
