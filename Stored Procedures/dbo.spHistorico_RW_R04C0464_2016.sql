SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0464_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0464_2016);

DELETE FROM Historico.RW_R04C0464_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0464_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoCreditoCNBV, ClasContable, ConcursoMercantil, FechaDisposicion, FechaVencDisposicion, Moneda,
	NumeroDisposicion, NombreFactorado, RFC_Factorado, SaldoInicial, TasaInteres, TasaInteresDisp, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa,
	MontoDispuesto, MontoPagoExigible, MontoCapitalPagado, MontoInteresPagado, MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado,
	MontoCondonacion, MontoQuitas, MontoBonificado, MontoDescuentos, MontoAumentosDec, SaldoFinal, SaldoCalculoInteres, DiasCalculoInteres,
	MontoInteresAplicar, SaldoInsoluto, SituacionCredito, DiasAtraso, FechaUltimoPago, MontoBancaDesarrollo, InstitucionFondeo
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoCreditoCNBV,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NombreFactorado,
	RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	MontoBancaDesarrollo,
	InstitucionFondeo
FROM dbo.RW_R04C0464_2016
WHERE IdReporteLog = @IdReporteLog;
GO
