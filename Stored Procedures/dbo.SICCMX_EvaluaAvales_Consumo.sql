SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_EvaluaAvales_Consumo]
AS

UPDATE dbo.SICCMX_ConsumoAval SET Aplica = 0;

-- NO APLICAN AVALES CON PI MAYOR A LA DEL ACREDITADO
UPDATE ca
SET Aplica = 1
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON ca.IdConsumo = crv.IdConsumo
WHERE av.PIAval < crv.[PI];

-- SI CUENTA CON MAS DE UN AVAL, SOLO APLICA EL DE MEJOR PI
UPDATE ca
SET Aplica = 0
FROM dbo.SICCMX_ConsumoAval ca
CROSS APPLY (
	SELECT TOP 1 caTmp.IdConsumo, caTmp.IdAval
	FROM dbo.SICCMX_ConsumoAval caTmp
	INNER JOIN dbo.SICCMX_Aval aTmp ON caTmp.IdAval = aTmp.IdAval
	WHERE caTmp.IdConsumo = ca.IdConsumo AND caTmp.Aplica = 1
	ORDER BY aTmp.PIAval
) AS val
WHERE ca.IdAval <> val.IdAval;
GO
