SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_063]

AS



BEGIN



-- El PORCENTAJE DE PAGOS EN TIEMPO CON INSTITUCIONES BANCARIAS EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoInstBanc

FROM dbo.RW_VW_R04C0470_INC

WHERE LEN(PorcPagoInstBanc) > 0 AND PorcPagoInstBanc <> '-99'

	AND (CHARINDEX('.',PorcPagoInstBanc) = 0 OR CHARINDEX('.',PorcPagoInstBanc) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc, CHARINDEX('.', PorcPagoInstBanc) + 1, LEN(PorcPagoInstBanc)))) <> 6);



END
GO
