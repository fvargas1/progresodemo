SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_071]
AS

BEGIN

-- El Estado debe ser un código valido del catalogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.Estado
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_Municipio cat ON ISNULL(r.Estado,'') = CAST(cat.CodigoEstado AS VARCHAR(5))
WHERE cat.IdMunicipio IS NULL;

END
GO
