SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0491_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'H-0491_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04H0491_2016;

INSERT INTO dbo.RW_R04H0491_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, GeneroAcred, EdadAcred, EdoCivilAcred, NombreAcred,
 RFC_Acred, CURP_Acred, CveUnicaAcred, MunicipioAcred, EstadoAcred, AportaSubcuenta, GeneroCoAcred, EdadCoAcred, EdoCivilCoAcred, NombreCoAcred, RFC_CoAcred,
 CURP_CoAcred, MunicipioCoAcred, EstadoCoAcred, ProductoHipotecario, CategoriaCredito, TipoAlta, DestinoCredito, FechaOtorgamiento, FechaVencimiento,
 DenominacionCredito, MontoOriginal, Comisiones, MontoSubsidioFederal, EntidadCoFin, MontoSubCuenta, MontoOtorgadoCoFin, Apoyo, ValorOriginalVivienda,
 ValorAvaluo, NumeroAvaluo, Localidad, Municipio, Estado, CveRegUnico, SegmentoVivienda, FechaFirmaReestructura, FechaVencimientoReestructura,
 MontoReestructura, DenominacionCreditoReestructura, IngresosMensuales, TipoComprobacionIngresos, SectorLaboral, NumeroConsultaSIC, IngresosMensualesCoAcred,
 TipoComprobacionIngresosCoAcred, SectorLaboralCoAcred, NumeroConsultaSICCoAcred, PeriodicidadAmortizaciones, TipoTasaInteres, TasaRef, AjusteTasaRef,
 SeguroAcreditado, TipoSeguro, EntidadSeguro, PorcentajeCubiertoSeguro, MontoSubCuentaGarantia, ValorGarantias, INTEXP, SDES, PorLTV
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '491',
 ROW_NUMBER() OVER ( ORDER BY hip.Codigo ASC ) AS NumeroSecuencia,
 hip.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 genAc.CodigoCNBV AS GeneroAcred,
 info.EdadAcreditado AS EdadAcred,
 ecAc.CodigoCNBV AS EdoCivilAcred,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombreAcred,
 per.RFC AS RFC_Acred,
 pInfo.CURP AS CURP_Acred,
 per.Codigo AS CveUnicaAcred,
 info.MunicipioLaboraAcreditado AS MunicipioAcred,
 info.EstadoLaboraAcreditado AS EstadoAcred,
 aport.CodigoCNBV AS AportaSubcuenta,
 genCo.CodigoCNBV AS GeneroCoAcred,
 info.EdadCoacreditado AS EdadCoAcred,
 ecCo.CodigoCNBV AS EdoCivilCoAcred,
 REPLACE(info.NombreCoacreditado, ',', '') AS NombreCoAcred,
 info.RfcCoacreditado AS RFC_CoAcred,
 info.CurpCoacreditado AS CURP_CoAcred,
 info.MunicipioLaboraCoacreditado AS MunicipioCoAcred,
 info.EstadoLaboraCoacreditado AS EstadoCoAcred, 
 prod.CodigoCNBV AS ProductoHipotecario,
 cat.CodigoCNBV AS CategoriaCredito,
 tpoAlta.CodigoCNBV AS TipoAlta,
 dest.CodigoCNBV AS DestinoCredito,
 CASE WHEN info.FechaOtorgamiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaOtorgamiento,102),'.',''),1,6) END AS FechaOtorgamiento,
 CASE WHEN info.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaVencimiento,102),'.',''),1,6) END AS FechaVencimiento,
 mon.CodigoCNBV_Hipo AS DenominacionCredito,
 info.MontoOriginal AS MontoOriginal,
 info.GastosOriginacion AS Comisiones,
 info.MontoSubsidioFederal AS MontoSubsidioFederal,
 cof.CodigoCNBV AS EntidadCoFin,
 info.MontoSubcuenta AS MontoSubCuenta,
 info.MontoCofinanciador AS MontoOtorgadoCoFin,
 info.MontoApoyoBancaDesarrollo AS Apoyo,
 info.ValorOriginal AS ValorOriginalVivienda,
 info.ValorAvaluo AS ValorAvaluo,
 REPLACE(info.NumeroAvaluo, '-', '') AS NumeroAvaluo,
 loc.CodigoCNBV AS Localidad,
 pre.Municipio AS Municipio,
 pre.Estado AS Estado,
 info.ClaveDeCuv AS CveRegUnico,
 seg.CodigoCNBV AS SegmentoVivienda,
 CASE WHEN info.FechaReestructura IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaReestructura,102),'.',''),1,6) END AS FechaFirmaReestructura,
 CASE WHEN info.FechaVencimientoReestructura IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaVencimientoReestructura,102),'.',''),1,6) END AS FechaVencimientoReestructura,
 info.MontoReestructura AS MontoReestructura,
 monRees.CodigoCNBV_Hipo AS DenominacionCreditoReestructura,
 info.IngresosMensuales AS IngresosMensuales,
 ing.Codigo AS TipoComprobacionIngresos,
 sect.Codigo AS SectorLaboral,
 info.FolioBuroCredito AS NumeroConsultaSIC,
 info.IngresosBrutosCoacreditado AS IngresosMensualesCoAcred,
 ingCo.Codigo AS TipoComprobacionIngresosCoAcred,
 sectCo.Codigo AS SectorLaboralCoAcred,
 info.NumeroCrediticiaCoacreditado AS NumeroConsultaSICCoAcred, 
 perCap.CodigoCNBV AS PeriodicidadAmortizaciones,
 tasa.CodigoCNBV AS TipoTasaInteres,
 tasaRef.Codigo AS TasaRef,
 info.AjusteTasaReferencia AS AjusteTasaRef,
 seguro.CodigoCNBV AS SeguroAcreditado,
 tpoSeg.CodigoCNBV AS TipoSeguro,
 entAsg.CodigoCNBV AS EntidadSeguro,
 info.PorcentajeAsegurado AS PorcentajeCubiertoSeguro,
 pre.SUBCVi AS MontoSubCuentaGarantia,
 hip.MontoGarantia AS ValorGarantias,
 pre.INTEXP AS INTEXP,
 pre.SDESi AS SDES,
 info.PrctLTV * 100 AS PorLTV
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Persona per ON hip.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_VW_HipotecarioCNBV cnbv ON hip.IdHipotecario = cnbv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_ProductoHipotecario prod ON info.IdProductoHipotecario = prod.IdProductoHipotecario
LEFT OUTER JOIN dbo.SICC_CategoriaCreditoHipotecario cat ON info.IdCategoria = cat.IdCategoriaHipotecario
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario tpoAlta ON info.IdTipoAlta = tpoAlta.IdTipoAltaHipotecario
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario dest ON info.IdDestinoHipotecario = dest.IdDestinoHipotecario
LEFT OUTER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Cofinanciamiento cof ON info.IdEntidadCofinanciadora = cof.IdCofinanciamiento
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON info.IdLocalidad = loc.IdLocalidad
LEFT OUTER JOIN dbo.SICC_Moneda monRees ON info.IdMonedaReestructurado = monRees.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoIngreso ing ON pInfo.IdTipoIngreso = ing.IdTipoIngreso
LEFT OUTER JOIN dbo.SICC_SectorLaboral sect ON pInfo.IdSectorLaboral = sect.IdSectorLaboral
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalHipotecario perCap ON info.IdPeriodicidadCapital = perCap.IdPeriodicidadCapitalHipotecario
LEFT OUTER JOIN dbo.SICC_TasaInteresHipotecario tasa ON info.IdTipoTasaInteres = tasa.IdTasaInteresHipotecario
LEFT OUTER JOIN dbo.SICC_TasaReferenciaHipotecario tasaRef ON info.IdTasaReferencia = tasaRef.IdTasaReferenciaHipotecario
LEFT OUTER JOIN dbo.SICC_SeguroAcreditado seguro ON info.IdSeguroCargoCliente = seguro.IdSeguroAcreditado
LEFT OUTER JOIN dbo.SICC_TipoSeguro tpoSeg ON info.IdTipoSeguro = tpoSeg.IdTipoSeguro
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora entAsg ON info.IdEntidadAseguradora = entAsg.IdEntidadAseguradora
LEFT OUTER JOIN dbo.SICC_Genero genAc ON info.IdGeneroAcreditado = genAc.IdGenero
LEFT OUTER JOIN dbo.SICC_EstadoCivil ecAc ON info.IdEstadoCivilAcreditado = ecAc.IdEstadoCivil
LEFT OUTER JOIN dbo.SICC_AportaSubcuenta aport ON info.IdAportaSubcuenta = aport.IdAportaSubcuenta
LEFT OUTER JOIN dbo.SICC_Genero genCo ON info.IdGeneroCoacreditado = genCo.IdGenero
LEFT OUTER JOIN dbo.SICC_EstadoCivil ecCo ON info.IdEstadoCivilCoacreditado = ecCo.IdEstadoCivil
LEFT OUTER JOIN dbo.SICC_ClasificacionContableHip seg ON info.IdSegmentoDeVivienda = seg.IdClasificacionContableHip
LEFT OUTER JOIN dbo.SICC_TipoIngreso ingCo ON info.IdComprobacionIngresosCoacreditado = ingCo.IdTipoIngreso
LEFT OUTER JOIN dbo.SICC_SectorLaboral sectCo ON info.IdSectorLaboralCoacreditado = sectCo.IdSectorLaboral
WHERE info.IdTipoAlta IS NOT NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04H0491_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
