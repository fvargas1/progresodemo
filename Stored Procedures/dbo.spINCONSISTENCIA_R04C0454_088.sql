SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_088]
AS
BEGIN
-- Si la Categoría del Crédito (cve_categoria_credito) en el formulario de SEGUIMIENTO es IGUAL a 2,
-- entonces el registro del Tipo de Alta del Crédito (cve_tipo_alta_credito) en el formulariode ALTAS para el mismo
-- CRÉDITO (dat_id_credito_met_cnbv) debe estar entre 700 y 751. (Cruce entre el reporte de seguimiento y el reporte de altas).

SELECT
	CodigoCredito,
	rep.CategoriaCredito,
	vw.CNBV AS CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0454_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(rep.CategoriaCredito,'') = '2' AND CAST(ISNULL(NULLIF(vw.TipoAlta,''),'-1') AS INT) NOT BETWEEN 700 AND 751;

END
GO
