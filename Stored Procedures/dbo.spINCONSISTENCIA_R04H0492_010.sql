SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_010]
AS

BEGIN

-- La "COMISIONES COBRADAS AL ACREDITADO (DENOMINADAS EN TASA)" debe ser en base 100 y a dos decimales.

SELECT CodigoCredito, CodigoCreditoCNBV, ComisionesCobradasTasa
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(ComisionesCobradasTasa,CHARINDEX('.',ComisionesCobradasTasa)+1,LEN(ComisionesCobradasTasa))) <> 2;

END
GO
