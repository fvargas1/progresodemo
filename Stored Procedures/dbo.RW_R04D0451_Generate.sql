SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaMigracion DATETIME;
DECLARE @IdPeriodoHistorico AS BIGINT;
DECLARE @FechaInicialNMC DATETIME;
DECLARE @FechaHistorica DATETIME;
DECLARE @FechaPeriodo DATETIME;

SELECT @IdPeriodoHistorico = MAX(IdPeriodoHistorico)
FROM dbo.SICC_PeriodoHistorico hist
INNER JOIN dbo.SICC_Periodo per ON DATEPART(YEAR, DATEADD(MONTH, -3, per.Fecha)) = DATEPART(YEAR, hist.Fecha)
AND DATEPART(MONTH, DATEADD(MONTH, -3, per.Fecha)) = DATEPART(MONTH, hist.Fecha)
AND per.Activo = 1;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo periodo WHERE periodo.Activo = 1;
SELECT @FechaHistorica = Fecha FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'D-0451' AND GrupoReporte = 'R04';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Riesgo crediticio y reservas de la cartera comercial', GETDATE(), 'Bajaware');


SET @IdReporteLog = SCOPE_IDENTITY();

TRUNCATE TABLE dbo.RW_R04D0451;


CREATE TABLE #TempCalificaciones (
 Codigo VARCHAR(20),
 Monto DECIMAL
)

/*
CALIFICACIONES POR DEUDOR PERIODO ACTUAL
*/
INSERT INTO #TempCalificaciones (Codigo, Monto)
SELECT
 con.Codigo,
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICC_CalificacionNvaMet calif ON calif.IdCalificacion = crv.CalifFinal
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = calif.Codigo
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cre.IdMetodologia
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cre.IdClasificacionContable AND (cc.Codigo IS NULL OR cc.Codigo NOT IN ('816000','139151010400','139151010500')) -- Para descartar los fideicomisos
WHERE met.Codigo IN ('20','21','22')
GROUP BY con.Codigo;

INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo calificacion
 '1', -- Calificaciones de riesgo del deudor
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(Monto AS DECIMAL))
FROM #TempCalificaciones
GROUP BY Codigo;


TRUNCATE TABLE #TempCalificaciones;

/*
CALIFICACIONES POR DEUDOR PERIODO HISTORICO
*/
INSERT INTO #TempCalificaciones (Codigo, Monto)
SELECT
 con.Codigo,
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito cre
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables crv ON cre.Codigo = crv.Credito AND cre.IdPeriodoHistorico = crv.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = crv.CalifFinal
WHERE cre.Metodologia IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (cre.ClasificacionContable IS NULL OR cre.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND cre.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;

INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo calificacion
 '1', -- Calificaciones de riesgo del deudor
 '19', -- Saldo total al cierre historico
 SUM(CAST(Monto AS DECIMAL))
FROM #TempCalificaciones
GROUP BY Codigo;


TRUNCATE TABLE #TempCalificaciones;

/*
POR DEUDOR MIGRACIONES DE MEJORA
*/
INSERT INTO #TempCalificaciones (Codigo, Monto)
SELECT
 con.Codigo,
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cre.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON cal.IdCalificacion = crv.CalifFinal
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables crvHist ON crvHist.Credito = cre.Codigo
INNER JOIN Historico.SICCMX_Credito creHist ON creHist.Codigo = cre.Codigo AND creHist.Metodologia = met.Codigo AND creHist.IdPeriodoHistorico = crvHist.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = cal.Codigo
WHERE met.Codigo IN ('20','21','22') -- lo que no es parametrico es individualizado
AND crvHist.IdPeriodoHistorico IS NOT NULL --para asegurar que no sea nuevo credito
AND (creHist.ClasificacionContable IS NULL OR creHist.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500' )) -- para descartar los fideicomisos
AND cal.Codigo < crvHist.CalifFinal
AND crvHist.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;

INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo calificacion
 '1', -- Calificaciones de riesgo del deudor
 '116', -- Migraciones Mejoras
 SUM(CAST(Monto AS DECIMAL))
FROM #TempCalificaciones con
GROUP BY con.Codigo;


TRUNCATE TABLE #TempCalificaciones;

/*
POR DEUDOR DISMINUCIONES
*/
INSERT INTO #TempCalificaciones (Codigo, Monto)
SELECT
 con.Codigo,
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON cal.IdCalificacion = crv.CalifFinal
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cre.IdMetodologia
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables crvHist ON crvHist.Credito = cre.Codigo
INNER JOIN Historico.SICCMX_Credito creHist ON creHist.Codigo = cre.Codigo AND creHist.Metodologia = met.Codigo AND creHist.IdPeriodoHistorico = crvHist.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = cal.Codigo
WHERE met.Codigo IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (creHist.ClasificacionContable IS NULL OR creHist.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500' )) -- para descartar los fideicomisos
AND cal.Codigo > crvHist.CalifFinal
AND crvHist.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;


INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo calificacion
 '1', -- Calificaciones de riesgo del deudor
 '117', -- Migraciones Descensos
 SUM(CAST(Monto AS DECIMAL))
FROM #TempCalificaciones
GROUP BY Codigo;


TRUNCATE TABLE #TempCalificaciones;

INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRNuevosPorPersona @IdPeriodoHistorico, @IdReporteLog;


TRUNCATE TABLE #TempCalificaciones;

INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRLiquidadosPorPersona @IdPeriodoHistorico, @IdReporteLog;


/*
INSERTAMOS SALDOS CALIF OPERACION METODO INDIVIDUALIZADO PERIODO ACTUAL
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '2', -- Calificaciones de riesgo del deudor
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cr.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet calActual ON calActual.IdCalificacion = crv.CalifFinal
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = calActual.Codigo
WHERE met.Codigo IN ('20','21','22') -- lo que no es parametrico es individualizado
GROUP BY con.Codigo;

/*
INSERTAMOS CALIF OPERACION MET INDIVIDUALIZADO HISTORICO
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '2', -- Calificaciones de riesgo del deudor
 '19', -- Saldo total al cierre periodo anterior
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = crv.Credito AND crv.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = crv.CalifFinal
WHERE cr.Metodologia IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND crv.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;


/*
MIGRACIONES DE MEJORA POR CREDITO MET NO IND
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '2', -- Calificaciones de riesgo del deudor
 '116', -- Mejoras de calificacion por operacion
 SUM(CAST(crv.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = crv.Credito AND crv.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = crv.CalifFinal
INNER JOIN dbo.SICCMX_Credito actual ON cr.Codigo = actual.Codigo
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables reservas ON actual.IdCredito = reservas.IdCredito
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON reservas.CalifFinal = cal.IdCalificacion
WHERE cr.Metodologia IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND crv.IdPeriodoHistorico = @IdPeriodoHistorico
AND crv.CalifFinal > cal.Codigo
GROUP BY con.Codigo;


/*
MIGRACIONES DESCENSOS DE CALIFICACION MET NO IND
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '2', -- Calificaciones de riesgo del deudor
 '117', -- Descensos de calificacion por operacion
 SUM(CAST(reservas.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = vw.CalifFinal
INNER JOIN dbo.SICCMX_Credito actual ON cr.Codigo = actual.Codigo
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables reservas ON actual.IdCredito = reservas.IdCredito
INNER JOIN dbo.SICC_CalificacionNvaMet cal ON reservas.CalifFinal = cal.IdCalificacion
WHERE cr.Metodologia IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico
AND vw.CalifFinal < cal.Codigo
GROUP BY con.Codigo;


/*
NUEVAS CALIFICACIONES DE RIESGOS CREDITOS NUEVOS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRNuevosPorProducto @IdPeriodoHistorico, @IdReporteLog;

/*
CREDITOS LIQUIDADOS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRLiquidadosPorProducto @IdPeriodoHistorico, @IdReporteLog;


/*
MIGRACIONES A E DEL TRIMESTRE ACTUAL
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo calificacion
 '2', -- Calificaciones de riesgo por operacion
 '119', -- Migraciones llevadas a E
 SUM(CAST(reser.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables reser
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = reser.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cr.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet califAct ON reser.CalifFinal = califAct.IdCalificacion
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables crHistorico ON crHistorico.Credito = cr.Codigo
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = crHistorico.CalifFinal
WHERE (cc.Codigo IS NULL OR cc.Codigo NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND met.Codigo IN ('20','21','22') -- lo que no es parametrico es individualizado
AND califAct.Codigo = 'E'
AND crHistorico.CalifFinal NOT IN ('E')
AND crHistorico.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;


/*
CALIFICACIONES CREDITOS MET IND
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '2', -- Metodo no individualizado
 '2', -- Calificaciones de riesgo del deudor
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vars.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vars
INNER JOIN dbo.SICCMX_Credito c ON vars.IdCredito = c.IdCredito
INNER JOIN dbo.SICC_CalificacionNvaMet calif ON vars.CalifFinal = calif.IdCalificacion
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = calif.Codigo
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = c.IdMetodologia
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = c.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
WHERE met.Codigo NOT IN ('20','21','22') -- lo que no es parametrico es individualizado
GROUP BY con.Codigo;

/*
CALIFICACIONES DE CREDITOS MET IND HISTORICO
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '2', -- Metodo no individualizado
 '2', -- Calificaciones de riesgo del deudor
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vars.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vars
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vars.Credito AND vars.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = vars.CalifFinal
WHERE cr.Metodologia NOT IN ('20','21','22') -- lo que no es parametrico es individualizado
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
GROUP BY con.Codigo;

/*
NUEVAS CALIFICACIONES DE RIESGOS CREDITOS NUEVOS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRNuevosPorProductoMETIND @IdPeriodoHistorico, @IdReporteLog;

/*
CREDITOS LIQUIDADOS MET IND
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
EXEC dbo.RW_R04D0451_CRLiquidadosPorProductoMETIND @IdPeriodoHistorico, @IdReporteLog;


/*
RESERVAS MET IND PERIODO ACTUAL
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '3', -- Composicion reservas
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(ReservaFinal AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vars
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vars.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cr.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet calif ON vars.CalifFinal = calif.IdCalificacion
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = calif.Codigo
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND (cc.Codigo IS NULL OR cc.Codigo NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
WHERE met.Codigo IN ('20','21','22') -- lo que no es parametrico es individualizado
GROUP BY con.Codigo;

/*
RESERVAS MET IND HISTORICO
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '1', -- Metodo individualizado
 '3', -- Composicion reservas
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(ReservaFinal AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vars
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vars.Credito AND cr.IdPeriodoHistorico = vars.IdPeriodoHistorico
 AND ( cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = vars.CalifFinal
WHERE cr.Metodologia IN ('20','21','22') -- lo que no es parametrico es individualizado
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico IS NOT NULL
GROUP BY con.Codigo;

/*
 RESERVA MET NO INVIDUALIZADO
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '2', -- Metodo individualizado
 '3', -- Composicion reservas
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(ReservaFinal AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND (cc.Codigo IS NULL OR cc.Codigo NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = cr.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet calif ON vw.CalifFinal = calif.IdCalificacion
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = calif.Codigo
WHERE met.Codigo NOT IN ('20','21','22') -- lo que no es parametrico es individualizado
GROUP BY con.Codigo;

/*
 RESERVA MET NO IND HISTORICO
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Codigo,
 '0451',
 '1', -- Moneda
 '2', -- Metodo NO individualizado
 '3', -- Composicion reservas
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(ReservaFinal AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vars
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vars.Credito AND cr.IdPeriodoHistorico = vars.IdPeriodoHistorico
 AND ( cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')) -- para descartar los fideicomisos
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = vars.CalifFinal
WHERE cr.Metodologia NOT IN ('20','21','22') -- lo que no es parametrico es individualizado
AND vars.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico IS NOT NULL
GROUP BY con.Codigo;

/*
 RESERVAS TOTALES POR METODOLOGIA
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 IdReporteLog,
 CASE WHEN MetodoCalificacion = '1' THEN '845000000000' ELSE '846000000000' END, -- totales de reservas
 Subreporte,
 Moneda,
 MetodoCalificacion,
 TipoCalificacion,
 TipoSaldo,
 SUM(CAST(Dato AS DECIMAL))
FROM dbo.RW_R04D0451 rep
WHERE TipoCalificacion = '3'
GROUP BY IdReporteLog, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo;


/*
 RESERVAS TOTALES
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842000000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable
AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500'); -- para descartar los fideicomisos

/*
 RESERVAS TOTALES HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842000000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico;


/*
 RESERVAS VIGENTES
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842001000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('1'); --vigente

/*
 RESERVAS VIGENTES HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842001000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('1')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico IS NOT NULL


/*
 RESERVAS VENCIDAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842002000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA

/*
 RESERVAS VENCIDAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '842002000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.ReservaFinal AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico is not NULL

/*
 SALDOS VIGENTES
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843100000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable
AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('1') --vigente

/*
 SALDOS VIGENTES HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843100000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('1')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico IS NOT NULL


/*
 SALDOS VENCIDAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843200000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA


/*
 SALDOS VENCIDOS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843200000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico AND @IdPeriodoHistorico IS NOT NULL


/*
 VARIABLES PARA CALCULOS DE PROPORCION
*/
DECLARE @CarteraComercialTotal DECIMAL;
DECLARE @CarteraComercialTotalHistorico DECIMAL;

SELECT @CarteraComercialTotal = SUM(CAST(vw.EI_Total AS DECIMAL))
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito;


SELECT @CarteraComercialTotalHistorico = SUM(CAST(vw.EI_Total AS DECIMAL))
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND cr.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico;


/*
 VENCIDA 0 - 29 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843201000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos BETWEEN 0 AND 29
HAVING SUM(CAST(ISNULL(vw.EI_Total,0) AS DECIMAL)) > 0;

/*
 VENCIDA 0 - 29 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843201000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos BETWEEN 0 AND 29

/*
 VENCIDA 30 - 59 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843202000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos BETWEEN 30 AND 59
HAVING SUM(CAST(ISNULL(vw.EI_Total,0) AS DECIMAL)) > 0;

/*
 VENCIDA 30 - 59 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843202000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos BETWEEN 30 AND 59

/*
 VENCIDA 60 - 89 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843203000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos BETWEEN 60 AND 89
HAVING SUM(CAST(ISNULL(vw.EI_Total,0) AS DECIMAL)) > 0;

/*
 VENCIDA 60 - 89 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843203000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND vw.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos BETWEEN 60 AND 89;

/*
 VENCIDA 90 - 179 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843204000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos BETWEEN 90 AND 179
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VENCIDA 90 - 179 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843204000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos BETWEEN 90 AND 179
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VENCIDA 180 - 365 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843205000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos BETWEEN 180 AND 365
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VENCIDA 180 - 365 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843205000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos BETWEEN 180 AND 365
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VENCIDA MAS 365 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843206000000',
 '0451',
 '1', -- Moneda 
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('2') --VENCIDA
AND info.DiasVencidos > 365
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VENCIDA MAS 365 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '843206000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('2')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasVencidos > 365
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0

/*
 VIGENTE 0 - 29 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844001000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('1') --VIGENTE
AND info.DiasMorosidad BETWEEN 1 AND 29
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VIGENTE 0 - 29 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844001000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('1')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasMorosidad BETWEEN 1 AND 29;

/*
 VIGENTE 30 - 59 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844002000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('1') --VIGENTE
AND info.DiasMorosidad BETWEEN 30 AND 59
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VIGENTE 30 - 59 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844002000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('1')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasMorosidad BETWEEN 30 AND 59;

/*
 VIGENTE 60 - 89 DIAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844003000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotal
FROM dbo.SICCMX_Credito_Reservas_Variables vw
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = vw.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_ClasificacionContable cc ON cc.IdClasificacionContable = cr.IdClasificacionContable AND cc.Codigo NOT IN ('816000', '139151010400', '139151010500') -- para descartar los fideicomisos
INNER JOIN dbo.SICC_SituacionCredito sc ON sc.IdSituacionCredito = cr.IdSituacionCredito
WHERE sc.Codigo IN ('1') --VIGENTE
AND info.DiasMorosidad BETWEEN 60 AND 89
HAVING SUM(CAST(vw.EI_Total AS DECIMAL)) > 0;

/*
 VIGENTE 60 - 89 DIAS HISTORICAS
*/
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844003000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CAST(vw.EI_Total AS DECIMAL)) AS Dato --/ @CarteraComercialTotalHistorico
FROM Historico.SICCMX_Credito_Reservas_Variables vw
INNER JOIN Historico.SICCMX_Credito cr ON cr.Codigo = vw.Credito AND vw.IdPeriodoHistorico = cr.IdPeriodoHistorico
INNER JOIN Historico.SICCMX_CreditoInfo info ON cr.Codigo = info.Credito AND info.IdPeriodoHistorico = vw.IdPeriodoHistorico
WHERE cr.SituacionCredito IN ('1')
AND (cr.ClasificacionContable IS NULL OR cr.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND info.DiasMorosidad BETWEEN 60 AND 89;


INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844000000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '1', -- Saldo total al cierre periodo actual
 SUM(CONVERT(BIGINT, Dato)) AS Dato --/ @CarteraComercialTotalHistorico
FROM dbo.RW_R04D0451
WHERE Concepto IN('844001000000','844002000000','844003000000')
AND TipoSaldo='1' AND CAST(NULLIF(Dato,'') AS DECIMAL) > 0;


INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 @IdReporteLog,
 '844000000000',
 '0451',
 '1', -- Moneda
 '3', -- Montos claves de desemplenio de calidad
 '4', -- Montos claves de desemplenio de calidad
 '19', -- Saldo total al cierre periodo actual
 SUM(CONVERT(BIGINT, Dato)) AS Dato --/ @CarteraComercialTotalHistorico
FROM dbo.RW_R04D0451
WHERE Concepto IN('844001000000','844002000000','844003000000')
AND TipoSaldo='19' AND CAST(NULLIF(Dato,'') AS DECIMAL) > 0;


-- Calculamos la variacion
INSERT INTO dbo.RW_R04D0451 (IdReporteLog, Concepto, Subreporte, Moneda, MetodoCalificacion, TipoCalificacion, TipoSaldo, Dato)
SELECT
 IdReporteLog,
 Concepto,
 Subreporte,
 Moneda,
 MetodoCalificacion,
 TipoCalificacion,
 '115', --variacion
 SUM( CAST(Dato AS DECIMAL) * CASE WHEN TipoSaldo = '19' THEN -1 ELSE 1 END)
FROM dbo.RW_R04D0451
WHERE TipoSaldo IN ('1', '19')
GROUP BY IdReporteLog , Concepto , Subreporte , Moneda , MetodoCalificacion , TipoCalificacion


DROP TABLE #TempCalificaciones;

/*Asegurar que los siguientes registros no se reporten*/
DELETE dbo.RW_R04D0451
WHERE (Concepto IN('841800000000') AND TipoSaldo IN ('116')) /*representan celdas invalidadas para las cuales no aplica la información solicitada, ver Doc Intrucciones de llenado */
OR (Concepto IN('841100000000') AND TipoSaldo IN ('117')) /*representan celdas invalidadas para las cuales no aplica la información solicitada, ver Doc Intrucciones de llenado */


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04D0451 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
