SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_ACM_SICC]  
 @IdPeriodoHistorico INT  
AS  
  
DELETE FROM Historico.RW_ACM_SICC WHERE IdPeriodoHistorico = @IdPeriodoHistorico;  
  
INSERT INTO Historico.RW_ACM_SICC (  
 IdPeriodoHistorico,  
 Periodo_Actual,
 Num_Credito,  
 Tipo_Cred_R04A,  
 Fecha_Ven,  
 Sector_Lab,  
 Situacion_Cred,  
 Interes_Vig,  
 Interes_Ven,  
 Saldo_Total,  
 Moneda  
)  
SELECT  
 @IdPeriodoHistorico,  
 Periodo_Actual, 
 Num_Credito, 
 Tipo_Cred_R04A,  
 Fecha_Ven,  
 Sector_Lab,  
 Situacion_Cred,  
 Interes_Vig,  
 Interes_Ven,  
 Saldo_Total,  
 Moneda  
FROM dbo.RW_ACM_SICC; 

GO
