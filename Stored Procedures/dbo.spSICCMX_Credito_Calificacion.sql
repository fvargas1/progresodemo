SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Credito_Calificacion]
	@IdCredito BIGINT
AS
SELECT
	crv.IdCredito,
	cre.CodigoCredito AS Codigo,
	GETDATE() AS FechaCalificacion,
	ISNULL(calExp.Codigo,'SC') AS CalExpuesta,
	ISNULL(crv.EI_Expuesta,0) AS MontoExpuesto,
	ISNULL(crv.ReservaExpuesta,0) AS ReservaExpuesto,
	ISNULL(crv.PorExpuesta,0) * 100 AS PorcentajeExpuesto,
	per.Codigo AS ClaveDeudor,
	per.Nombre AS NombreDeudor,
	cd.Codigo AS CalificacionDeudor,
	ISNULL(cInfo.DiasVencidos,0) AS Morosidad,
	per.IdPersona,
	ISNULL(crv.EI_Total,0) AS Saldo,
	cre.IdMetodologia
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON crv.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaCalificacion pc ON per.IdPersona = pc.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.CalifExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cd ON pc.IdCalificacion = cd.IdCalificacion
WHERE cre.IdCredito = @IdCredito;
GO
