SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0450_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM Historico.RW_R04C0450
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NombreGarante, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion;
GO
