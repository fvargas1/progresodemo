SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_052]
AS

BEGIN

-- Validar que ID Acreditado Asignado por la Institución (dat_id_acreditado_instit) se haya reportado previamente en el reporte de altas R04-C 468 (Documento SITI 1855).

SELECT
 rep.CodigoPersona AS CodigoDeudor,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor
FROM dbo.RW_VW_R04C0470_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0468_INC r68 ON rep.CodigoPersona = r68.CodigoPersona
LEFT OUTER JOIN Historico.RW_VW_R04C0468_INC hist ON rep.CodigoPersona = hist.CodigoPersona
WHERE hist.IdPeriodoHistorico IS NULL AND r68.CodigoPersona IS NULL;

END


GO
