SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_TipoGarantiaMA_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_TipoGarantiaMA_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA cat ON LTRIM(f.TipoGarantiaMA) = cat.Codigo
WHERE cat.IdTipoGarantia IS NULL AND LEN(f.TipoGarantiaMA) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'TipoGarantiaMA',
 f.TipoGarantiaMA,
 2,
 @Detalle
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA cat ON LTRIM(f.TipoGarantiaMA) = cat.Codigo
WHERE cat.IdTipoGarantia IS NULL AND LEN(f.TipoGarantiaMA) > 0;
GO
