SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_100_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el crédito es calificado como Proyecto De Inversión Con Fuente De Pago Propia (cve_fuente_pago = 1),
-- entonces la Severidad de la Pérdida (dat_severidad_perdida), la Probabilidad de Incumplimiento y la Exposición al Incumplimiento deben ser igual a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0469
WHERE ISNULL(ProyectoInversion,'') = '1'
	AND (ISNULL(SPTotal,'') <> '0.000000' OR ISNULL(PITotal,'') <> '0.000000' OR ISNULL(EITotal,'') <> '0');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
