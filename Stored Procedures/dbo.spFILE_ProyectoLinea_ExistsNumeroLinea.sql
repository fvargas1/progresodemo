SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ProyectoLinea_ExistsNumeroLinea]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_ProyectoLinea_ExistsNumeroLinea';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_ProyectoLinea
SET errorFormato = 1
WHERE LEN(ISNULL(NumeroLinea, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_ProyectoLinea_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoProyecto,
 'NumeroLinea',
 NumeroLinea,
 1,
 @Detalle
FROM dbo.FILE_ProyectoLinea
WHERE LEN(ISNULL(NumeroLinea, '')) = 0;
GO
