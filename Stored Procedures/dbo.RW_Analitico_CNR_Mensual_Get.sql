SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_CNR_Mensual_Get]
	@IdReporteLog BIGINT
AS
SELECT
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	VECES,
	FactorProPago,
	ProPago,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion
FROM dbo.RW_VW_Analitico_CNR_Mensual
ORDER BY Nombre;
GO
