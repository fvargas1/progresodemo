SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_011_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "COMISIONES COBRADAS AL ACREDITADO (DENOMINADAS EN TASA)" debe ser mayor a cero si la comisión cobrada al acreditado denominada en monto (col. 12) es mayor a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(ComisionesCobradasMonto,''),'0') AS DECIMAL) > 0 AND CAST(ISNULL(NULLIF(ComisionesCobradasTasa,''),'-1') AS DECIMAL(8,2)) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
