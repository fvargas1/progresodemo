SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_004]
AS
BEGIN
-- El ID metodología CNBV (dat_id_credito_met_cnbv) debe empezar con 2.

SELECT
 CodigoCredito,
 REPLACE(NombreAcreditado, ',', '') AS NombreDeudor,
 CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0450_INC
WHERE SUBSTRING(CodigoCreditoCNBV,1,1) <> '2';

END
GO
