SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_087]
AS

BEGIN

-- Si el Puntaje Asignado por el Porcentaje de Pagos en Tiempo con Entidades Financ No Bcarias en los últimos 12 meses (cve_ptaje_porc_pagos_no_bcos) es = 47,
-- entonces el Porcentaje de Pago en Tiempo con Entidades Financieras No Bcarias en los últimos 12 meses (dat_porcent_pgos_no_bcos) debe ser >= 75 y < 87

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstNoBanc,
	P_PorcPagoInstNoBanc AS Puntos_PorcPagoInstNoBanc
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PorcPagoInstNoBanc,'') = '47' AND (CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) < 75 OR CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) >= 87);

END


GO
