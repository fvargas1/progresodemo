SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Auditoria_24_BANCREA_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @TC_UDI DECIMAL(10,6);
DECLARE @FechaPeriodo DATETIME;
DECLARE @SI VARCHAR(15);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='AUDITORIA' AND Nombre = '_SP';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT @TC_UDI=tipoC.Valor
FROM dbo.SICC_Periodo periodo
INNER JOIN dbo.SICC_TipoCambio tipoC ON periodo.IdPeriodo = tipoC.IdPeriodo
INNER JOIN dbo.SICC_Moneda moneda ON tipoC.IdMoneda = moneda.IdMoneda
WHERE periodo.Activo=1 AND moneda.Codigo=(SELECT VALUE FROM dbo.BAJAWARE_Config WHERE Codename='MonedaUdis');

SET @SI = '-999';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Auditoria_24_BANCREA;

INSERT INTO dbo.RW_Auditoria_24_BANCREA (
 IdReporteLog, [ID], [IDC], [N_A], [FO], [FV], [PL], [TC], [Tasa], [T_Tasa], [Revolvente], [Destino], [ING], [ID_Linea], [LC], [S_Total], [IDevNC_B],
 [IDevNC_CO], [Grupo_y_Sub], [ATR_SIC], [ATR_BC], [Estatus_B6], [ICV], [Recla], [Enpro], [CM], [HPI], [UDI], [PIC], [PIE], [SPC], [SPE], [EIC], [EIE],
 [RVA_G], [RVA_PYM], [RVA_CPP], [RVA_C], [RVA_E], [RVA_T], [PI_fac], [PI_CG], [GDO_RGO], [PI_PCG], [SPO_ATRA], [DIAS_ATR], [SPO_IF_PC], [SPO_GTIA_R],
 [Cre_con_GF], [GF_Cuantos], [GF_EIO], [GF_He], [GF_C], [GF_Hc], [GF_Hfx], [GF_EIAJ], [GF_SPC], [Cre_con_GNF], [GNF_NC], [GNF_CiGR], [GNF_CDF],
 [GNF_CIN], [GNF_CMU], [GNF_CF1], [GNF_CF2], [GNF_CF3], [GNF_Cadi], [GNF_EIE], [GNF_EIEA], [Cre_con_P], [GP_PIAVAL100], [GP_PIG], [GP_PC], [GP_Ga],
 [GP_Pa], [Cre_con_PYM], [GC_Cob_PYM], [GC_PI_GA], [GC_SP_GA], [Cre_con_PP], [GC_PCPP], [GC_SSi], [GC_RVAS_Portafolio], [GC_Mto_Cob_pp],
 [GC_PRVAS_Portafolio], [GC_DIFPP], [EI_S], [SOFOM], [Sin_Ingre], [PI_>5], [PI_<5], [REEST], [F_REEST], [PER_PAGO], [R_CTAS_ORD], [F_CALIF], [B1]
)
SELECT
 @IdReporteLog,
 credito.CodigoCredito AS ID, -- 01
 persona.Codigo AS IDC, -- 02
 REPLACE(pInfo.NombreCNBV,',','') AS N_A, -- 03
 CASE WHEN credito.FechaAutorizacionOriginal IS NULL THEN @SI ELSE CONVERT(VARCHAR,credito.FechaAutorizacionOriginal,103) END AS FO, -- 04
 CASE WHEN credito.FechaVencimiento IS NULL THEN @SI ELSE CONVERT(VARCHAR,credito.FechaVencimiento,103) END AS FV, -- 05
 ISNULL(DATEDIFF(MONTH, credito.FechaAutorizacionOriginal, credito.FechaVencimiento), 0) AS PL, -- 06
 ISNULL(tcc.Codigo,@SI) AS TC, -- 07
 CAST(credito.TasaBruta AS DECIMAL(10,6)) AS Tasa, -- 08
 CASE WHEN tRef.Codigo = '600' THEN 1 ELSE 2 END AS T_Tasa, -- 09
 CASE WHEN disp.Codigo = '103' THEN 1 ELSE 0 END AS Revolvente, -- 10
 ISNULL(dest.Codigo, '0') AS Destino, -- 11
 CAST(ISNULL(anx.VentNetTotAnuales,0) AS DECIMAL(23,2)) AS ING, -- 12
 credito.NumeroLinea AS ID_Linea, -- 13
 CAST(credito.MontoLineaCredito AS DECIMAL(23,2)) AS LC, -- 14
 CAST (credito.MontoValorizado AS DECIMAL(23,2)) AS S_Total, -- 15
 '0.00' AS IDevNC_B, -- 16
 CAST(credito.InteresRefinanciado AS DECIMAL(23,2)) AS IDevNC_CO, -- 17
 CASE
 WHEN met.Codigo = '18' THEN 'A18_G'
 WHEN met.Codigo = '20' THEN 'A20'
 WHEN met.Codigo = '21' THEN CASE WHEN ISNULL(anx.SinAtrasos,0) = 1 THEN 'A21_1' ELSE 'A21_2' END
 WHEN met.Codigo = '22' THEN CASE ppi.IdClasificacion WHEN 1 THEN 'A22_PC' WHEN 2 THEN 'A22_C' WHEN 3 THEN 'A22_GC' END
 END AS Grupo_y_Sub, -- 18
 CASE
 WHEN ppi.IdMetodologia=21 AND anx.SinAtrasos=0 THEN ISNULL(CAST(CAST(vwV21.[21CA_DIAS_MORA_INST_BANC] AS DECIMAL) AS VARCHAR), @SI)
 WHEN ppi.IdMetodologia=22 THEN ISNULL(CAST(CAST(vwV22.[22_DIAS_MORA_INST_BANC] AS DECIMAL) AS VARCHAR), @SI)
 WHEN ppi.IdMetodologia=20 THEN ISNULL(CAST(CAST(ISNULL(vwV20.[20IA_DIAS_MORA_INST_BANC], vwV20.[20IB_DIAS_MORA_INST_BANC]) AS DECIMAL) AS VARCHAR), @SI)
 ELSE @SI END AS ATR_SIC, -- 19
 lit.Morosidad AS ATR_BC, -- 20
 sitCre.Codigo AS Estatus_B6, -- 21
 CASE WHEN sitCre.Codigo = '1' THEN @SI ELSE CASE WHEN ((credito.SaldoVencidoValorizado + credito.InteresVencidoValorizado) / prct.MontoTotal * 100) < 5 THEN 1 ELSE 0 END END AS ICV, -- 22
 '2' AS Recla, -- 23
 ISNULL(cInfo.Emproblemado,0) AS Enpro, -- 24
 ISNULL(cInfo.ConcursoMercantil,0) AS CM, -- 25
 CASE WHEN castigados.IdPersona IS NULL THEN 0 ELSE 1 END AS HPI, -- 26
 @TC_UDI AS UDI, -- 27
 CAST(ISNULL(crv.PI_Cubierta,0) AS DECIMAL(10,6)) AS PIC, -- 28
 CAST(ISNULL(crv.PI_Expuesta,0) AS DECIMAL(10,6)) AS PIE, -- 29
 CAST(ISNULL(crv.SP_Cubierta,0) AS DECIMAL(10,4)) AS SPC, -- 30
 CAST(ISNULL(crv.SP_Expuesta,0) AS DECIMAL(10,4)) AS SPE, -- 31
 CAST(ISNULL(crv.EI_Cubierta_GarPer,0) AS DECIMAL(23,2)) AS EIC, -- 32
 CAST(ISNULL(crv.EI_Expuesta_GarPer,0) AS DECIMAL(23,2)) AS EIE, -- 33
 CAST(ISNULL(gRa.ReservaGP,0) AS DECIMAL(23,2)) AS RVA_G, -- 34
 CAST(ISNULL(gpPM.Reserva,0) AS DECIMAL(23,2)) AS RVA_PYM, -- 35
 CAST(ISNULL(gpPP.ReservaCubierto,0) AS DECIMAL(23,2)) AS RVA_CPP, -- 36
 CAST(ISNULL(crv.ReservaCubierta_GarPer,0) AS DECIMAL(23,2)) AS RVA_C, -- 37
 CAST(ISNULL(crv.ReservaExpuesta_GarPer,0) AS DECIMAL(23,2)) AS RVA_E, -- 38
 CAST(ISNULL(crv.ReservaFinal,0) AS DECIMAL(23,2)) AS RVA_T, -- 39
 CASE WHEN fond.Nombre LIKE 'Art. 112%' THEN 1 ELSE 0 END AS PI_fac, -- 40
 CASE WHEN gFondo.IdCredito IS NULL THEN 0 ELSE 1 END AS PI_CG, -- 41
 cal.Codigo AS GDO_RGO, -- 42
 CAST(ISNULL(gFondo.Porcentaje,0) AS DECIMAL(10,6)) AS PI_PCG, -- 43
 CAST(ISNULL(credito.MesesIncumplimiento,0) AS DECIMAL(10,2)) AS SPO_ATRA, -- 44
 ISNULL(cInfo.DiasMorosidad,0) AS DIAS_ATR, -- 45
 CASE WHEN pos.Codigo = '2' THEN 1 ELSE 0 END AS SPO_IF_PC, -- 46
 CASE WHEN gRA.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS SPO_GTIA_R, -- 47
 CASE WHEN ISNULL(gRA.CantidadGF,0) > 0 THEN 1 ELSE 0 END AS Cre_con_GF, -- 48
 ISNULL(gCnt.CntGF,0) AS GF_Cuantos, -- 49
 CAST (credito.MontoValorizado AS DECIMAL(23,2)) AS GF_EIO, -- 50
 CAST(ISNULL(gRA.He,0) AS DECIMAL(10,4)) AS GF_He, -- 51
 CAST(ISNULL(vcg.MontoGF,0) AS DECIMAL(23,2)) AS GF_C, -- 52
 CAST(ISNULL(gRA.Hc,0) AS DECIMAL(10,4)) AS GF_Hc, -- 53
 CAST(ISNULL(gRA.Hfx,0) AS DECIMAL(10,6)) AS GF_Hfx, -- 54
 CAST(dbo.MAX2VAR(0, crv.EI_Total - ISNULL(gRA.MontoGNF,0)) AS DECIMAL(23,2)) AS GF_EIAJ, -- 55
 CAST(ISNULL(spRep.SP_GarRealNoFin,0) AS DECIMAL(10,4)) AS GF_SPC, -- 56
 CASE WHEN ISNULL(gRA.CantidadGNF,0) > 0 THEN 1 ELSE 0 END AS Cre_con_GNF, -- 57
 ISNULL(gCnt.CntGNF,0) AS GNF_NC, -- 58
 CAST(ISNULL(spRep.Porcentaje,0) * 100 AS DECIMAL(10,6)) AS GNF_CiGR, -- 59
 CAST(ISNULL(vcg.ValorDC,0) AS DECIMAL(23,2)) AS GNF_CDF, -- 60
 CAST(ISNULL(vcg.ValorBI,0) AS DECIMAL(23,2)) AS GNF_CIN, -- 61
 CAST(ISNULL(vcg.ValorBM,0) AS DECIMAL(23,2)) AS GNF_CMU, -- 62
 CAST(ISNULL(vcg.ValorFAF,0) AS DECIMAL(23,2)) AS GNF_CF1, -- 63
 CAST(ISNULL(vcg.ValorFIP,0) AS DECIMAL(23,2)) AS GNF_CF2, -- 64
 CAST(ISNULL(vcg.ValorAMB,0) AS DECIMAL(23,2)) AS GNF_CF3, -- 65
 '0.00' AS GNF_Cadi, -- 66
 '0.00' AS GNF_EIE, -- 67
 '0.00' AS GNF_EIA, -- 68
 CASE WHEN gRA.CantidadPer > 0 THEN 1 ELSE 0 END AS Cre_con_P, -- 69
 CASE WHEN av100.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS GP_PIAVAL100, -- 70
 CAST(ISNULL(av100.PIAval,0) AS DECIMAL(10,6)) AS GP_PIG, -- 71
 '0.00' AS GP_PC, -- 72
 '0.00' AS GP_Ga, -- 73
 '0.00' AS GP_Pa, -- 74
 CASE WHEN gpPM.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS Cre_con_PYM, -- 75
 CAST(ISNULL(gpPM.Porcentaje,0) AS DECIMAL(10,6)) AS GC_Cob_PYM, -- 76
 CAST(ISNULL(gpPM.[PI],0) AS DECIMAL(10,6)) AS GC_PI_GA, -- 77
 CAST(ISNULL(gpPM.SP,0) AS DECIMAL(10,4)) AS GC_SP_GA, -- 78
 CASE WHEN gpPP.IdCredito IS NOT NULL THEN 1 ELSE 0 END AS Cre_con_PP, -- 79
 CAST(ISNULL(gpPP.Porcentaje,0) AS DECIMAL(10,6)) AS GC_PCPP, -- 80
 '0.00' AS GC_SSi, -- 81
 '0.00' AS GC_RVAS_Portafolio, -- 82
 '0.00' AS GC_Mto_Cob_pp, -- 83
 '0.000000' AS GC_PRVAS_Portafolio, -- 84
 '0.0000' AS GC_DIFPP, -- 85
 '0' AS EI_S, -- 86
 '0' AS SOFOM, -- 87
 '0' AS Sin_Ingre, -- 88
 '0' AS [PI_>5], -- 89
 '0' AS [PI_<5], -- 90
 '0' AS REEST, -- 91
 '0' AS F_REEST, -- 92
 '3' AS PER_PAGO, -- 93
 '0' AS R_CTAS_ORD, -- 94
 CONVERT(VARCHAR,@FechaPeriodo,103) AS F_CALIF, -- 95
 '0' AS B1 -- 96
FROM dbo.SICCMX_Persona persona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON persona.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON persona.IdPersona = credito.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON credito.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON credito.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON credito.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON persona.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_ValoresDelta delta ON ppi.IdMetodologia = delta.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(delta.IdClasificacion,0)
INNER JOIN (
 SELECT crLit.IdPersona, MAX(CAST(ciLit.ConcursoMercantil AS INT)) AS Litigio, MAX(crLit.IdSituacionCredito) AS IdSituacionCredito, MAX(ciLit.DiasMorosidad) AS Morosidad
 FROM dbo.SICCMX_Credito crLit
 INNER JOIN dbo.SICCMX_CreditoInfo ciLit ON crLit.IdCredito = ciLit.IdCredito
 GROUP BY crLit.IdPersona
) AS lit ON persona.IdPersona = lit.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON cInfo.ProductoComercial = tcc.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON credito.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_TasaReferencia tRef ON credito.IdTasaReferencia = tRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON cInfo.IdDisposicion = disp.IdDisposicion
LEFT OUTER JOIN dbo.SICC_Destino dest ON credito.IdDestino = dest.IdDestino
LEFT OUTER JOIN dbo.SICCMX_VW_PersonasValor_A20 vwV20 ON persona.IdPersona = vwV20.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_PersonasValor_A21 vwV21 ON persona.IdPersona = vwV21.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_PersonasValor_A22 vwV22 ON persona.IdPersona = vwV22.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasPersReportes garPer ON credito.IdCredito = garPer.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PM gpPM ON credito.IdCredito = gpPM.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PP gpPP ON credito.IdCredito = gpPP.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON cInfo.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_TipoReserva5Prct fond ON persona.EsFondo = fond.IdTipoReserva
LEFT OUTER JOIN (
 SELECT IdPersona, VentNetTotAnuales, 0 AS EntFinAcreOtorgantesCre, SinAtrasos FROM dbo.SICCMX_Anexo21
 UNION
 SELECT IdPersona, VentNetTotAnuales, 0, 0 FROM dbo.SICCMX_Anexo22
 UNION
 SELECT IdPersona, ActivoTotal, EntFinAcreOtorgantesCre, 0 FROM dbo.SICCMX_Anexo20
) AS anx ON persona.IdPersona = anx.IdPersona
LEFT OUTER JOIN (
 SELECT cub.IdCredito, SUM(cub.Porcentaje) AS Porcentaje
 FROM (
 SELECT IdCredito, Porcentaje FROM dbo.SICCMX_VW_Garantias_PM
 UNION ALL
 SELECT IdCredito, PrctAval FROM dbo.SICCMX_VW_GarantiasPersReportes WHERE TipoAval='7'
 ) AS cub
 GROUP BY cub.IdCredito
) AS gFondo ON credito.IdCredito = gFondo.IdCredito
LEFT OUTER JOIN dbo.SICC_Posicion pos ON cInfo.Posicion = pos.IdPosicion
LEFT OUTER JOIN (
 SELECT
 c.IdCredito,
 MAX(c.He) AS He,
 MAX(c.Hc) AS Hc,
 MAX(c.Hfx) AS Hfx,
 COUNT(c.IdCredito) AS Cantidad,
 SUM(CASE WHEN c.IdTipoGarantia IS NULL THEN 1 ELSE 0 END) AS CantidadGF,
 SUM(CASE WHEN c.IdTipoGarantia IS NULL THEN c.C ELSE 0 END) AS MontoGF,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN c.C ELSE 0 END) AS MontoGNF,
 SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN 1 ELSE 0 END) AS CantidadGNF,
 SUM(CASE WHEN tg.Codigo='NMC-03' THEN c.C ELSE 0 END) AS ValorDC,
 SUM(CASE WHEN tg.Codigo='NMC-01' THEN c.C ELSE 0 END) AS ValorBI,
 SUM(CASE WHEN tg.Codigo='NMC-02' THEN c.C ELSE 0 END) AS ValorBM,
 SUM(CASE WHEN tg.Codigo='EYM-01' THEN c.C ELSE 0 END) AS ValorFAF,
 SUM(CASE WHEN tg.Codigo='EYM-02' THEN c.C ELSE 0 END) AS ValorFIP,
 SUM(CASE WHEN tg.Codigo='EYM-03' THEN c.C ELSE 0 END) AS ValorAMB,
 SUM(CASE WHEN tg.Codigo='GP' THEN c.Reserva ELSE 0 END) AS ReservaGP,
 SUM(CASE WHEN tg.Codigo='GP' THEN 1 ELSE 0 END) AS CantidadPer
 FROM dbo.SICCMX_Garantia_Canasta c
 LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON c.IdTipoGarantia = tg.IdTipoGarantia
 WHERE c.EsDescubierto IS NULL
 GROUP BY c.IdCredito
) AS gRA ON credito.IdCredito = gRA.IdCredito
LEFT OUTER JOIN (
 SELECT cg.IdCredito, SUM(CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 1 ELSE 0 END) AS CntGF, SUM(CASE WHEN tg.IdClasificacionNvaMet = 2 THEN 1 ELSE 0 END) AS CntGNF
 FROM dbo.SICCMX_CreditoGarantia cg
 INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia
 INNER JOIN dbo.SICC_TipoGarantia tg ON g.IdTipoGarantia = tg.IdTipoGarantia
 INNER JOIN dbo.SICCMX_Garantia_Canasta c ON cg.IdCredito = c.IdCredito AND CASE WHEN tg.IdClasificacionNvaMet = 1 THEN 0 ELSE tg.IdTipoGarantia END = ISNULL(c.IdTipoGarantia,0) AND c.EsDescubierto IS NULL
 GROUP BY cg.IdCredito
) AS gCnt ON credito.IdCredito = gCnt.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasSPReportes spRep ON credito.IdCredito = spRep.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Creditos_Castigados_SIC castigados ON persona.IdPersona = castigados.IdPersona AND credito.CodigoCredito = castigados.CodigoCredito
LEFT OUTER JOIN (
 SELECT
 IdPersona,
 SUM(SaldoVigenteValorizado+InteresVigenteValorizado) AS MontoVigente,
 SUM(SaldoVencidoValorizado+InteresVencidoValorizado) AS MontoVencido,
 SUM(MontoValorizado) AS MontoTotal,
 SUM(SaldoVigenteValorizado+InteresVigenteValorizado) / SUM(MontoValorizado) * 100 AS PrctVigente,
 SUM(SaldoVencidoValorizado+InteresVencidoValorizado) / SUM(MontoValorizado) * 100 AS PrctVencido
 FROM dbo.SICCMX_VW_Credito_NMC
 WHERE MontoValorizado>0
 GROUP BY IdPersona
) AS prct ON credito.IdPersona = prct.IdPersona
LEFT OUTER JOIN (
 SELECT ca.IdCredito, a.PIAval
 FROM dbo.SICCMX_CreditoAval ca
 INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
 INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
 INNER JOIN dbo.SICCMX_Garantia_Canasta c ON ca.IdCredito = c.IdCredito
 INNER JOIN dbo.SICC_TipoGarantia t ON c.IdTipoGarantia = t.IdTipoGarantia AND t.Codigo='GP'
 WHERE tg.Codigo='7' AND c.PrctCobAjust >= 1
) av100 ON credito.IdCredito = av100.IdCredito
LEFT OUTER JOIN (
 SELECT
 cg.IdCredito,
 SUM(CASE WHEN g.IdClasificacionNvaMet = 1 THEN cg.MontoBanco ELSE 0 END) AS MontoGF,
 SUM(CASE WHEN g.IdClasificacionNvaMet = 2 THEN cg.MontoBanco ELSE 0 END) AS MontoGNF,
 SUM(CASE WHEN g.CodigoTipoGarantia='NMC-03' THEN cg.MontoBanco ELSE 0 END) AS ValorDC,
 SUM(CASE WHEN g.CodigoTipoGarantia='NMC-01' THEN cg.MontoBanco ELSE 0 END) AS ValorBI,
 SUM(CASE WHEN g.CodigoTipoGarantia='NMC-02' THEN cg.MontoBanco ELSE 0 END) AS ValorBM,
 SUM(CASE WHEN g.CodigoTipoGarantia='EYM-01' THEN cg.MontoBanco ELSE 0 END) AS ValorFAF,
 SUM(CASE WHEN g.CodigoTipoGarantia='EYM-02' THEN cg.MontoBanco ELSE 0 END) AS ValorFIP,
 SUM(CASE WHEN g.CodigoTipoGarantia='EYM-03' THEN cg.MontoBanco ELSE 0 END) AS ValorAMB
 FROM dbo.SICCMX_CreditoGarantia cg
 INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia
 WHERE g.Aplica = 1
 GROUP BY cg.IdCredito
) AS vcg ON credito.IdCredito = vcg.IdCredito;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Auditoria_24_BANCREA WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses, FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
