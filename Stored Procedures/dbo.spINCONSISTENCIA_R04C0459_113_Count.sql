SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_113_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Número de Días Vencidos (dat_num_dias_vencidos) es MAYOR a 90, entonces la Situación del Crédito (cve_situacion_credito) es IGUAL a 2.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) > 90 AND ISNULL(SituacionCredito,'') <> '2';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
