SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_ExperienciaFunAdmon]
	@filtro VARCHAR(50)
AS
SELECT Codigo, Nombre
FROM dbo.CAT_VW_ExperienciaFunAdmon
WHERE ISNULL(Codigo,'')+'|'+ISNULL(Nombre,'') LIKE '%' + @filtro + '%';
GO
