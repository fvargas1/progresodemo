SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_ConsumoGarantia_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_ConsumoGarantia_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_ConsumoGarantia_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_ConsumoGarantia_Hst;
GO
