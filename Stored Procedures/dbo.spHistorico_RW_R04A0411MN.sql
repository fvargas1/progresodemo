SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04A0411MN]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04A0411MN);

DELETE FROM Historico.RW_R04A0411MN WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04A0411MN (
	IdPeriodoHistorico, Periodo, Entidad, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato
FROM dbo.RW_R04A0411MN
WHERE IdReporteLog = @IdReporteLog;
GO
