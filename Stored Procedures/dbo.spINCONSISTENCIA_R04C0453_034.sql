SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_034]
AS
BEGIN
-- Vaidar que la Periodicidad Pagos Interes corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.PeriodicidadPagosInteres
FROM dbo.RW_VW_R04C0453_INC rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON ISNULL(rep.PeriodicidadPagosInteres,'') = perInt.CodigoCNBV
WHERE perInt.IdPeriodicidadInteres IS NULL;

END
GO
