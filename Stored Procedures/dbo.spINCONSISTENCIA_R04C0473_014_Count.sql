SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_014_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Nombre del Acreditado (dat_nombre) no debe incluir las terminaciones SA DE CV, entre otras.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0473_INC
WHERE UPPER(NombrePersona) LIKE '%SC DE RL DE CV%' OR
UPPER(NombrePersona) LIKE '%S.C. DE R.L. DE C.V.%' OR
UPPER(NombrePersona) LIKE '%S.A. DE C.V.%' OR
UPPER(NombrePersona) LIKE '%SA DE CV%' OR
UPPER(NombrePersona) LIKE '%A.C.%' OR
UPPER(NombrePersona) LIKE '%S.A.%';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
