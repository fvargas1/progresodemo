SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0411ME_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (
	Codigo, Concepto, Padre, TotalVigente_TotalVigente, CarteraVigente_TotalVigente, CarteraVigente_Principal, CarteraVigente_InteresDevengado,
	SinPagos_TotalVigente, SinPagos_Principal, SinPagos_InteresDevengado, ConPagosVencidos_TotalVigente, ConPagosVencidos_Principal,
	ConPagosVencidos_InteresDevengado, CarteraVencida_TotalVigente, CarteraVencida_Principal, CarteraVencida_InteresDevengado, LEVEL
) AS (
	SELECT
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.TotalVigente_TotalVigente,
	con.CarteraVigente_TotalVigente,
	con.CarteraVigente_Principal,
	con.CarteraVigente_InteresDevengado,
	con.SinPagos_TotalVigente,
	con.SinPagos_Principal,
	con.SinPagos_InteresDevengado,
	con.ConPagosVencidos_TotalVigente,
	con.ConPagosVencidos_Principal,
	con.ConPagosVencidos_InteresDevengado,
	con.CarteraVencida_TotalVigente,
	con.CarteraVencida_Principal,
	con.CarteraVencida_InteresDevengado,
	0 AS LEVEL
	FROM dbo.RW_R04A0411ME_VW_Consolidado con
	WHERE Padre = ''
	UNION ALL
	SELECT
	con.Codigo,
	con.Concepto,
	con.Padre,
	con.TotalVigente_TotalVigente,
	con.CarteraVigente_TotalVigente,
	con.CarteraVigente_Principal,
	con.CarteraVigente_InteresDevengado,
	con.SinPagos_TotalVigente,
	con.SinPagos_Principal,
	con.SinPagos_InteresDevengado,
	con.ConPagosVencidos_TotalVigente,
	con.ConPagosVencidos_Principal,
	con.ConPagosVencidos_InteresDevengado,
	con.CarteraVencida_TotalVigente,
	con.CarteraVencida_Principal,
	con.CarteraVencida_InteresDevengado,
	LEVEL + 1
	FROM dbo.RW_R04A0411ME_VW_Consolidado con
	INNER JOIN Reporte rep ON con.Padre = rep.Codigo
)
SELECT
	Codigo,
	REPLICATE(' ',LEVEL*5)+Concepto AS Concepto,
	TotalVigente_TotalVigente,
	CarteraVigente_TotalVigente,
	CarteraVigente_Principal,
	CarteraVigente_InteresDevengado,
	SinPagos_TotalVigente,
	SinPagos_Principal,
	SinPagos_InteresDevengado,
	ConPagosVencidos_TotalVigente,
	ConPagosVencidos_Principal,
	ConPagosVencidos_InteresDevengado,
	CarteraVencida_TotalVigente,
	CarteraVencida_Principal,
	CarteraVencida_InteresDevengado,
	LEVEL
FROM Reporte
ORDER BY Codigo;
GO
