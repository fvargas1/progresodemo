SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_PIAval_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_PIAval_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Aval
SET errorFormato = 1
WHERE LEN(PIAval)>0 AND (ISNUMERIC(ISNULL(PIAval,'0')) = 0 OR PIAval LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoAval,
 'PIAval',
 PIAval,
 1,
 @Detalle
FROM dbo.FILE_Aval
WHERE LEN(PIAval)>0 AND (ISNUMERIC(ISNULL(PIAval,'0')) = 0 OR PIAval LIKE '%[^0-9 .]%');
GO
