SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0415ME_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (
	Codigo,
	Concepto,
	Padre,
	InteresesPagados,
	Comisiones,
	SaldoPromedioDiario,
	InteresesPagadosValorizados,
	ComisionesValorizados,
	SaldoPromedioDiarioValorizados,
	LEVEL
) AS (
	SELECT
	vw.Codigo,
	vw.Concepto,
	vw.Padre,
	vw.InteresesPagados,
	vw.Comisiones,
	vw.SaldoPromedioDiario,
	vw.InteresesPagadosValorizados,
	vw.ComisionesValorizados,
	vw.SaldoPromedioDiarioValorizados,
	0 AS LEVEL
	FROM dbo.RW_R04A0415ME_VW_Consolidado vw
	WHERE vw.Padre = ''
	UNION ALL
	SELECT
	vw.Codigo,
	vw.Concepto,
	vw.Padre,
	vw.InteresesPagados,
	vw.Comisiones,
	vw.SaldoPromedioDiario,
	vw.InteresesPagadosValorizados,
	vw.ComisionesValorizados,
	vw.SaldoPromedioDiarioValorizados,
	LEVEL + 1
	FROM dbo.RW_R04A0415ME_VW_Consolidado vw
	INNER JOIN Reporte r ON r.Codigo = vw.Padre
)
SELECT
	Codigo,
	REPLICATE(' ', LEVEL*5)+ Concepto AS Concepto,
	vw.InteresesPagados,
	vw.Comisiones,
	vw.SaldoPromedioDiario,
	vw.InteresesPagadosValorizados,
	vw.ComisionesValorizados,
	vw.SaldoPromedioDiarioValorizados,
	LEVEL
FROM Reporte vw
ORDER BY Codigo;
GO
