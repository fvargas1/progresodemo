SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_CNR_Mensual_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorVECES,
	VECES,
	FactorProPago,
	ProPago,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion
FROM Historico.RW_Analitico_CNR_Mensual
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
