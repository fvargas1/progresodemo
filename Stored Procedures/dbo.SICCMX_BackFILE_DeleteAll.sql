SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_BackFILE_DeleteAll]
AS
DECLARE @tbl VARCHAR(100);
DECLARE @qry VARCHAR(150);
DECLARE crs CURSOR FOR
SELECT tab.name AS Tabla
FROM sys.tables tab
INNER JOIN sys.schemas sch ON tab.schema_id = sch.schema_id
WHERE sch.name = 'dbo' AND tab.name LIKE '%[_]Hst';

OPEN crs;

FETCH NEXT FROM crs INTO @tbl;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @qry = 'TRUNCATE TABLE dbo.' + @tbl;
EXEC(@qry);

FETCH NEXT FROM crs INTO @tbl;

END

CLOSE crs;
DEALLOCATE crs;
GO
