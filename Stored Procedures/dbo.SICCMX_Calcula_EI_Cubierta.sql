SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_EI_Cubierta]
AS
UPDATE crv
SET EI_Cubierta = CASE WHEN ISNULL(cub.Cobertura,0) > crv.EI_Total THEN crv.EI_Total ELSE ISNULL(cub.Cobertura,0) END,
 EI_Cubierta_GarPer = CASE WHEN ISNULL(per.MontoCubierto,0) > crv.EI_Total THEN crv.EI_Total ELSE ISNULL(per.MontoCubierto,0) END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Cub cub ON crv.IdCredito = cub.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Cobertura_Personales per ON crv.IdCredito = per.IdCredito;
GO
