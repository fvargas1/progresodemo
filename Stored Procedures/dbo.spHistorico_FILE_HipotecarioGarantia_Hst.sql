SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_HipotecarioGarantia_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_HipotecarioGarantia_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_HipotecarioGarantia_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Fuente
FROM dbo.FILE_HipotecarioGarantia_Hst;
GO
