SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_Insert]
	@Nombre VARCHAR(150),
	@Descripcion VARCHAR(2000),
	@CodenameCount VARCHAR(150),
	@Codename VARCHAR(150),
	@IdCategoria INT,
	@Activo BIT,
	@Regulatorio BIT,
	@FechaCreacion DATETIME,
	@FechaActualizacion DATETIME = NULL,
	@Username VARCHAR(50)
AS
INSERT INTO dbo.MIGRACION_Inconsistencia (
	Nombre,
	Descripcion,
	CodenameCount,
	Codename,
	IdCategoria,
	Activo,
	Regulatorio,
	FechaCreacion,
	FechaActualizacion,
	Username
) VALUES (
	@Nombre,
	@Descripcion,
	@CodenameCount,
	@Codename,
	@IdCategoria,
	@Activo,
	@Regulatorio,
	@FechaCreacion,
	@FechaActualizacion,
	@Username
);

SELECT SCOPE_IDENTITY();
GO
