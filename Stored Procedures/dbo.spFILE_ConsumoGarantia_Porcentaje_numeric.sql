SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoGarantia_Porcentaje_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_ConsumoGarantia_Porcentaje_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_ConsumoGarantia
SET errorFormato = 1
WHERE LEN(Porcentaje)>0 AND (ISNUMERIC(ISNULL(Porcentaje,'0')) = 0 OR Porcentaje LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_ConsumoGarantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoGarantia,
	'Porcentaje',
	Porcentaje,
	1,
	@Detalle
FROM dbo.FILE_ConsumoGarantia
WHERE LEN(Porcentaje)>0 AND (ISNUMERIC(ISNULL(Porcentaje,'0')) = 0 OR Porcentaje LIKE '%[^0-9 .]%');
GO
