SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spGetConfigReports]
	@IdMetodologia VARCHAR(5)
AS
SELECT
	ReportFolder,
	Reportname
FROM dbo.RW_Cedula
WHERE Codename = CASE @IdMetodologia
 WHEN 7 THEN 'RW_Cedula_Anexo18'
 WHEN 20 THEN 'RW_Cedula_Anexo20'
 WHEN 21 THEN 'RW_Cedula_Anexo21'
 WHEN 22 THEN 'RW_Cedula_Anexo22'
 ELSE ''
 END
GO
