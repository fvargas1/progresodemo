SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_037]
AS

BEGIN

-- Si el Tipo de Cartera es igual a 310, el RFC Acreditado debe iniciar con guion bajo.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	RFC
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoCartera,'') = '310' AND SUBSTRING(ISNULL(RFC,''),1,1) <> '_';

END


GO
