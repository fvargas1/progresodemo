SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Ejecutivo_ListErrores]
AS
SELECT
 CodigoEjecutivo,
 Nombre,
 Telefono,
 Correo,
 Fuente
FROM dbo.FILE_Ejecutivo
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
