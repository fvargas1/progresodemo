SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_089]
AS

BEGIN

-- Validar que el Nombre Grupo de Riesgo (dat_nombre_grupo_riesgo) venga en mayúsculas.

SELECT
	CodigoCredito,
	REPLACE(GrupoRiesgo, ',', '') AS GrupoRiesgo
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(GrupoRiesgo AS VARBINARY(250)) != CAST(UPPER(GrupoRiesgo) AS VARBINARY(120));

END


GO
