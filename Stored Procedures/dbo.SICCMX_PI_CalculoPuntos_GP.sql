SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_CalculoPuntos_GP]
AS

--Primero se calcula el puntaje para aquellas variables donde no hay informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable
WHERE det.ValorActual IS NULL AND mat.RangoMenor IS NULL;


--Se calculan los valores cuando existe informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual >= mat.RangoMenor AND ValorActual < mat.RangoMayor
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor <> mat.RangoMayor;


--Se calculan las variables cuando no existen diferencias entre el menor y mayor
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles_GP det
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual = mat.RangoMenor
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor = mat.RangoMayor;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT IdGP, EsGarante, GETDATE(), '', '********** SE CALCULAN LOS PUNTOS POR VARIABLE **********'
FROM dbo.SICCMX_Persona_PI_GP;


-- Actualiza el puntaje a 24 a los clientes con Utilidad Neta Trimestral y Capital Contable Promedio menor a 0
EXEC SICCMX_ValidaInfoAnx20_GP;

-- Actualiza el puntaje a 57 a los clientes con Ventas Totales Anuales mayor a 216 millones de UDIS y con mas de 12 instituciones reportadas
EXEC SICCMX_ValidaInfoAnx22_GP;


-- LOG PARA ANEXO 20
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Activo Total : $ ' + CONVERT(VARCHAR, CAST(an20.ActivoTotal AS MONEY), 1)
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_VW_Anexo20_GP an20 ON ppi.IdGP = an20.IdGP AND ppi.EsGarante = an20.EsGarante
UNION ALL
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Entidad Financiera ' + CASE WHEN ISNULL(an20.EntFinAcreOtorgantesCre, 0) = 0 THEN 'NO ' ELSE '' END + 'Otorgante de Crédito'
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_VW_Anexo20_GP an20 ON ppi.IdGP = an20.IdGP AND ppi.EsGarante = an20.EsGarante
UNION ALL
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Tipo de Entidad Financiera : ' + vwCat.Nombre
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_VW_Anexo20_GP an20 ON ppi.IdGP = an20.IdGP AND ppi.EsGarante = an20.EsGarante
INNER JOIN dbo.CAT_VW_EntFin_Sujeta vwCat ON CAST(an20.EntFinSujRegBan AS VARCHAR(5)) = vwCat.Codigo;


-- LOG PARA ANEXO 21
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Cliente ' + CASE WHEN ISNULL(an21.SinAtrasos, 0) = 1 THEN 'sin atrasos' ELSE 'con atrasos' END
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_VW_Anexo21_GP an21 ON ppi.IdGP = an21.IdGP AND ppi.EsGarante = an21.EsGarante;


-- LOG PARA ANEXO 22
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT ppi.IdGP, ppi.EsGarante, GETDATE(), '', 'Ventas Netas Anuales : $ ' + CONVERT(VARCHAR, CONVERT(MONEY, an22.VentNetTotAnuales), 1)
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_VW_Anexo22_GP an22 ON ppi.IdGP = an22.IdGP AND ppi.EsGarante = an22.EsGarante;
GO
