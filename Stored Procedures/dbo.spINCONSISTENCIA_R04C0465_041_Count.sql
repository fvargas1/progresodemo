SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_041_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- Si el Puntaje Asignado por la Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) es = 53,

-- entonces la Tasa de Retención Laboral (dat_tasa_retencion_laboral) debe ser = -999 (Sin Informacion)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE ISNULL(P_TasaRetLab,'') = '53' AND CAST(TasaRetLab AS DECIMAL(10,6)) <> -99;



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
