SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_048]
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Nacionalidad (cve_pais).

SELECT
	CodigoCredito,
	Nacionalidad,
	RFC
FROM dbo.RW_VW_R04C0458_INC
WHERE Nacionalidad IN (
	SELECT rep.Nacionalidad
	FROM dbo.RW_VW_R04C0458_INC rep
	INNER JOIN dbo.RW_VW_R04C0458_INC rep2 ON rep.RFC = rep2.RFC AND rep.Nacionalidad <> rep2.Nacionalidad
	GROUP BY rep.Nacionalidad, rep.RFC
);

END
GO
