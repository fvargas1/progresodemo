SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Reservas_InteresVencido]
AS
UPDATE crv
SET ReservaFinal = crv.ReservaFinal + cre.InteresRefinanciado
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
WHERE cre.InteresRefinanciado > 0;
GO
