SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_072_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Periodicidad Pagos Capital corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON ISNULL(rep.PeriodicidadPagosCapital,'') = perCap.CodigoCNBV
WHERE perCap.IdPeriodicidadCapital IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
