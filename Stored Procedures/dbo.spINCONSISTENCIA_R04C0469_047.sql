SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_047]
AS

BEGIN

-- Validar que el Número de días vencidos (dat_num_vencidos) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasAtraso
FROM dbo.RW_VW_R04C0469_INC
WHERE CAST(ISNULL(NULLIF(DiasAtraso,''),'-1') AS DECIMAL) < 0;

END


GO
