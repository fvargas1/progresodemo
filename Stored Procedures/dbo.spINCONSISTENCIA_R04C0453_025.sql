SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_025]
AS
BEGIN
-- Validar que la Comisión por Apertura del Crédito en Tasa (dat_com_apertura_tasa) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionAperturaTasa
FROM dbo.RW_VW_R04C0453_INC
WHERE CAST(ISNULL(NULLIF(ComisionAperturaTasa,''),'-1') AS DECIMAL(10,6)) < 0;

END
GO
