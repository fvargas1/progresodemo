SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_Select]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	vw.IdPeriodoHistorico,
	vw.Fecha,
	vw.Version,
	vw.FechaCreacion,
	vw.Username,
	ISNULL(vw.CarteraComercial,0) AS CarteraComercial,
	ISNULL(vw.CastigosNetos,0) AS CastigosNetos,
	ISNULL(vw.Activo,0) AS Activo,
	ISNULL(vw.Reporte,0) AS Reporte
FROM dbo.SICC_VW_PeriodoHistorico vw
WHERE vw.IdPeriodoHistorico = @IdPeriodoHistorico;
GO
