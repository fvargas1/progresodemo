SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_076]
AS

BEGIN

-- Vaidar que la Institucion corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.IdInstitucionOrigen
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_Institucion inst ON ISNULL(rep.IdInstitucionOrigen,'') = inst.Codigo
WHERE inst.IdInstitucion IS NULL;

END


GO
