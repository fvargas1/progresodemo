SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_SP_CFNVT]
AS
UPDATE hrv
SET SP = CASE WHEN vp.ATR >= const.ATRSP THEN 1
	ELSE
	CASE WHEN ((1 - vp.TRi) * (0.8 * ISNULL(tr.FactorAjuste,1))) > 0.10 THEN ((1 - vp.TRi) * (0.8 * ISNULL(tr.FactorAjuste,1))) ELSE 0.10 END
	END
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON vp.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo IN ('3','4')
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON hrv.IdHipotecario = inf.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoRegimen tr ON inf.IdTipoRegimenCredito = tr.IdTipoRegimen;
GO
