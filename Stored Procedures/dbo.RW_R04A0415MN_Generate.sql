SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0415MN_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @SaldoFinalTotalConsumo DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'A-0415MN' AND GrupoReporte = 'R04';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Saldos Promedio, Intereses y Comisiones por Cartera de Crédito', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();

TRUNCATE TABLE dbo.RW_R04A0415MN;

-- Calculamos el interes pagado
INSERT INTO dbo.RW_R04A0415MN (
 IdReporteLog,
 Concepto,
 SubReporte,
 Moneda,
 TipoDeCartera,
 TipoDeSaldo,
 Dato
)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0415',
 '14', -- Todo va a moneda nacional
 0,
 '4', -- Interes pagado
 SUM(CAST(dat.InteresMes AS DECIMAL))
FROM R04.[0415Datos] dat
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'MN'
GROUP BY dat.Concepto;


-- Calculamos comisiones del mes
INSERT INTO dbo.RW_R04A0415MN (
 IdReporteLog,
 Concepto,
 SubReporte,
 Moneda,
 TipoDeCartera,
 TipoDeSaldo,
 Dato
)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0415',
 '14', -- Todo va a moneda nacional
 0,
 '5', -- Comisiones
 SUM(CAST(dat.ComisionMes AS DECIMAL))
FROM R04.[0415Datos] dat
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'MN'
GROUP BY dat.Concepto;


-- Calculamos el saldo promedio del mes
INSERT INTO dbo.RW_R04A0415MN (
 IdReporteLog,
 Concepto,
 SubReporte,
 Moneda,
 TipoDeCartera,
 TipoDeSaldo,
 Dato
)
SELECT
 @IdReporteLog,
 dat.Concepto,
 '0415',
 '14', -- Todo va a moneda nacional
 0,
 '6', -- Saldo Promedio
 SUM(CAST(dat.SaldoPromedio AS DECIMAL))
FROM R04.[0415Datos] dat
WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'MN'
GROUP BY dat.Concepto;


-- CALCULAMOS LAS CUENTAS PADRES
DECLARE @maxLevel INT;
CREATE TABLE #tree (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
 nivel INT
);

WITH Conceptos (Codigo, LEVEL)
AS (
 SELECT Codigo, 0 AS LEVEL
 FROM dbo.ReportWare_VW_0415Concepto
 WHERE Padre = ''
 UNION ALL
 SELECT vw.Codigo, LEVEL + 1
 FROM dbo.ReportWare_VW_0415Concepto vw
 INNER JOIN Conceptos con ON vw.Padre = con.Codigo
)

INSERT INTO #tree (Codigo, nivel)
SELECT Codigo, [LEVEL] FROM Conceptos;

SELECT @maxLevel = MAX (nivel) FROM #tree;

WHILE @maxLevel >= 0
BEGIN

 INSERT INTO dbo.RW_R04A0415MN (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
 SELECT rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo,
 SUM(CAST(ISNULL(rep.Dato,0) AS DECIMAL))
 FROM dbo.ReportWare_VW_0415Concepto conceptos
 INNER JOIN dbo.RW_R04A0415MN rep ON rep.Concepto = conceptos.Codigo
 INNER JOIN #tree con ON rep.Concepto = con.Codigo
 WHERE con.nivel = @maxLevel AND
 conceptos.Padre NOT IN (SELECT Concepto FROM dbo.RW_R04A0415MN)
 AND conceptos.Padre <> ''
 GROUP BY rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeSaldo, rep.TipoDeCartera;

 SET @maxLevel = @maxLevel - 1;
END 

DROP TABLE #tree;


INSERT INTO dbo.RW_R04A0415MN (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
	@IdReporteLog,
	sal.Concepto,
	sal.SubReporte,
	sal.Moneda,
	sal.TipoCartera,
	sal.TipoSaldo,
	'0'
FROM dbo.BAJAWARE_R04A_Salida sal
LEFT OUTER JOIN dbo.RW_R04A0415MN r04 ON sal.Concepto = r04.Concepto AND sal.Moneda = r04.Moneda AND sal.TipoCartera = r04.TipoDeCartera AND sal.TipoSaldo = r04.TipoDeSaldo
WHERE sal.Subreporte = '0415' AND sal.Moneda = '14' AND r04.Concepto IS NULL;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0415MN WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
