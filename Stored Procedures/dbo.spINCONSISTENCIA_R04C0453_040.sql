SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_040]
AS
BEGIN
-- Si el RFC (dat_rfc) inicia con CNBV, CNBX o CNBF, validar que el Tipo de Cartera (cve_tipo_cartera) sea igual a 140, 141, 142, 270, 299, 330, 340, 410, 420, 540, 550 ó 560.

SELECT
	CodigoCredito,
	RFC,
	TipoCartera
FROM dbo.RW_VW_R04C0453_INC
WHERE SUBSTRING(ISNULL(RFC,''),1,4) IN ('CNBV','CNBX','CNBF') AND ISNULL(TipoCartera,'') NOT IN ('140','141','142','270','299','330','340','410','420','540','550','560')

END
GO
