SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_048]
AS

BEGIN

-- La "PÉRDIDA ESPERADA (Metodología Interna) debe estar expresado a seis decimales.

SELECT CodigoCredito, CodigoCreditoCNBV, PerdidaEsperadaInterna
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(PerdidaEsperadaInterna,CHARINDEX('.',PerdidaEsperadaInterna)+1,LEN(PerdidaEsperadaInterna))) <> 6;

END
GO
