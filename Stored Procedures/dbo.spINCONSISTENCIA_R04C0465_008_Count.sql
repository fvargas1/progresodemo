SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_008_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Aportaciones al Infonavit en el último bimestre (cve_ptaje_aport_infonavit) es = 63,
-- entonces las Aportaciones al Infonavit en el último bimestre (dat_aportaciones_infonavit) deben ser >= 153 y < 741

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PagosInfonavit,'') = '63' AND (CAST(PagosInfonavit AS DECIMAL) < 153 OR CAST(PagosInfonavit AS DECIMAL) >= 741);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
