SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_006_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Un mismo RFC (col. 4) no puede tener más de un Nombre de Garante (Col. 5) diferente.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r50.IdReporteLog)
FROM dbo.RW_VW_R04C0450_INC r50
INNER JOIN (
 SELECT rep.NombreGarante
 FROM dbo.RW_VW_R04C0450_INC rep
 INNER JOIN dbo.RW_VW_R04C0450_INC rep2 ON rep.RFC_Garante = rep2.RFC_Garante AND rep.NombreGarante <> rep2.NombreGarante
 GROUP BY rep.NombreGarante, rep.RFC_Garante
) AS repetidos ON r50.NombreGarante = repetidos.NombreGarante AND LEN(r50.NombreGarante) > 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
