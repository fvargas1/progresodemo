SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_032_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El monto del crédito otorgado por el Cofinanciador es cero (0) si "Entidad que otorgó el Cofinanciamiento"
-- es igual a cero (0) (columna 17), o si el "Tipo de Alta del Crédito" es igual 3 (columna 9).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE MontoOtorgadoCoFin = '0' AND (EntidadCoFin <> '0' AND TipoAlta <> '3');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
