SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_013]
AS

BEGIN

-- El campo SEVERIDAD CUBIER GTIA PERSONAL debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPCubierta AS SP_Cubierta
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(SPCubierta,''),'-1') AS DECIMAL(10,6)) < 0;

END
GO
