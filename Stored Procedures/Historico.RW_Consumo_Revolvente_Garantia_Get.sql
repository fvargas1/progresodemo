SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Consumo_Revolvente_Garantia_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	FolioCredito,
	Garantia,
	GarantiaReal,
	ImporteGarantia,
	PI_Garante,
	SP,
	SP_SG,
	Hc,
	Hfx,
	PrctCobertura,
	Exposicion,
	EI_Ajustada,
	Reservas,
	ClaveGarante,
	RUGM,
	IdPortafolio
FROM Historico.RW_Consumo_Revolvente_Garantia
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO
