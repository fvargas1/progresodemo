SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_ConsumoGarantia]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_ConsumoGarantia WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_ConsumoGarantia (
	IdPeriodoHistorico,
	Codigo,
	Garantia,
	PorUsadoGarantia,
	MontoUsadoGarantia,
	Aplica
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	c.Codigo,
	g.Codigo,
	cg.PorUsadoGarantia,
	cg.MontoUsadoGarantia,
	cg.Aplica
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_Consumo c ON cg.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Garantia_Consumo g ON cg.IdGarantia = g.IdGarantia;
GO
