SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_008]
AS

BEGIN

-- Si el Puntaje Asignado por Aportaciones al Infonavit en el último bimestre (cve_ptaje_aport_infonavit) es = 63,
-- entonces las Aportaciones al Infonavit en el último bimestre (dat_aportaciones_infonavit) deben ser >= 153 y < 741

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PagosInfonavit,
	P_PagosInfonavit AS Puntos_PagosInfonavit
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PagosInfonavit,'') = '63' AND (CAST(PagosInfonavit AS DECIMAL) < 153 OR CAST(PagosInfonavit AS DECIMAL) >= 741);

END

GO
