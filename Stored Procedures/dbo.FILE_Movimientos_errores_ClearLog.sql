SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Movimientos_errores_ClearLog]
AS
UPDATE dbo.FILE_Movimientos
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_Movimientos_errores;
GO
