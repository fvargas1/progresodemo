SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_104_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se identifican acreditados (dat_id_acreditado_institucion) en el formulario de Probabilidad que no fueron identificados en el reporte de Seguimiento.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rPI.IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC rPI
LEFT OUTER JOIN dbo.RW_R04C0464 rSE ON rPI.CodigoPersona = rSE.CodigoPersona
WHERE rSE.CodigoPersona IS NULL

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
