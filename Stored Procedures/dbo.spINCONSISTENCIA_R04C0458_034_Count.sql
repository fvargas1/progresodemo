SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_034_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Tipo de Operacion corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC rep
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpo ON ISNULL(rep.TipoOperacion,'') = tpo.CodigoCNBV
WHERE tpo.IdTipoOperacion IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
