SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_CodigoRepetidoCodigoCliente]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_CodigoRepetidoCodigoCliente';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo21 f
WHERE f.CodigoCliente IN (
 SELECT f.CodigoCliente
 FROM dbo.FILE_Anexo21 f
 GROUP BY f.CodigoCliente, f.EsGarante
 HAVING COUNT(f.CodigoCliente)>1
) AND LEN(f.CodigoCliente) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoCliente,
 'CodigoCliente',
 f.CodigoCliente,
 1,
 @Detalle
FROM dbo.FILE_Anexo21 f
WHERE f.CodigoCliente IN (
 SELECT f.CodigoCliente
 FROM dbo.FILE_Anexo21 f
 GROUP BY f.CodigoCliente, f.EsGarante
 HAVING COUNT(f.CodigoCliente)>1
) AND LEN(f.CodigoCliente) > 0;
GO
