SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_046_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Porcentaje de Pagos en Tiempo con Instituciones Financieras NO Bancarias en los Últimos 12 meses (dat_porcent_pgo_no_bcos) sea >= 0 pero <= 100

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PorcPagoInstNoBanc AS DECIMAL(18,6)) NOT BETWEEN 0 AND 100 AND CAST(PorcPagoInstNoBanc AS DECIMAL) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
