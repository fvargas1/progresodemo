SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0411Datos_CalculandoConceptos]
AS
UPDATE dat
SET Concepto = conf.Concepto
FROM R04.[0411Datos] dat
INNER JOIN R04.[0411Configuracion] conf ON conf.TipoProducto = dat.CodigoProducto AND conf.SituacionCredito = dat.SituacionCredito;
GO
