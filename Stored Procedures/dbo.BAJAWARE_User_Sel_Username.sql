SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_Sel_Username]
	@Username BAJAWARE_utDescripcion100
AS
SELECT
	u.IdUser,
	u.Active,
	u.Description,
	u.Blocked,
	u.Name,
	u.Email,
	u.Telefono,
	u.Mobil,
	u.NumeroControl,
	u.Username,
	u.Password,
	u.UsuarioWindows,
	u.IdUserType,
	u.LastLoginAccess,
	u.CreatedDate,
	u.FechaNacimiento,
	u.UltimaFechaLogin,
	u.NeverLogedIn
FROM dbo.BAJAWARE_User u
WHERE Username = @Username;
GO
