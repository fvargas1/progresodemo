SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_012]
AS
BEGIN
-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas.

SELECT
	rep.CodigoCredito,
	rep.CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0456_INC rep
LEFT OUTER JOIN Historico.RW_VW_R04C0453_INC hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE hist.IdPeriodoHistorico IS NULL;

END
GO
