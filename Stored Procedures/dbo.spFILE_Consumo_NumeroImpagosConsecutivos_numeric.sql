SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_NumeroImpagosConsecutivos_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_NumeroImpagosConsecutivos_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Consumo
SET errorFormato = 1
WHERE LEN(NumeroImpagosConsecutivos)>0 AND (ISNUMERIC(ISNULL(NumeroImpagosConsecutivos,'0')) = 0 OR NumeroImpagosConsecutivos LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'NumeroImpagosConsecutivos',
 NumeroImpagosConsecutivos,
 1,
 @Detalle
FROM dbo.FILE_Consumo
WHERE LEN(NumeroImpagosConsecutivos)>0 AND (ISNUMERIC(ISNULL(NumeroImpagosConsecutivos,'0')) = 0 OR NumeroImpagosConsecutivos LIKE '%[^0-9 .]%');
GO
