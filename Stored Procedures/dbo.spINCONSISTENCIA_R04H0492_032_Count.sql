SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_032_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "PORCENTAJE CUBIERTO POR EL ESQUEMA DE COBERTURA DE PASO Y MEDIDA (%CobPaMed)" debe ser mayor o igual a cero y menor o igual a 100.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(PorCobPaMed,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
