SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_SalidaBuroCredito_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Periodo,
	BP,
	Cuenta,
	Calificacion
FROM Historico.RW_SalidaBuroCredito
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO
