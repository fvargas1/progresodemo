SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_028]
AS

BEGIN

-- La Fecha de Vencimiento debe ser mayor o igual al periodo actual de envío.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, FechaVencimiento, @FechaPeriodo AS FechaPeriodo
FROM dbo.RW_R04C0442
WHERE FechaVencimiento < @FechaPeriodo;

END
GO
