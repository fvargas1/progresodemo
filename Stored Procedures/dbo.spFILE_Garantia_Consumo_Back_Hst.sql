SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Consumo_Back_Hst]
AS

INSERT INTO dbo.FILE_Garantia_Consumo_Hst (
	CodigoGarantia,
	TipoGarantia,
	ValorGarantia,
	Moneda,
	FechaValuacion,
	Descripcion,
	BancoGarantia,
	ValorGarantiaProyectado,
	Hc,
	VencimientoRestante,
	GradoRiesgo,
	AgenciaCalificadora,
	Calificacion,
	Emisor,
	Escala,
	EsIPC,
	PIGarante,
	CodigoCliente,
	IndPerMorales
)
SELECT
	gc.CodigoGarantia,
	gc.TipoGarantia,
	gc.ValorGarantia,
	gc.Moneda,
	gc.FechaValuacion,
	gc.Descripcion,
	gc.BancoGarantia,
	gc.ValorGarantiaProyectado,
	gc.Hc,
	gc.VencimientoRestante,
	gc.GradoRiesgo,
	gc.AgenciaCalificadora,
	gc.Calificacion,
	gc.Emisor,
	gc.Escala,
	gc.EsIPC,
	gc.PIGarante,
	gc.CodigoCliente,
	gc.IndPerMorales
FROM dbo.FILE_Garantia_Consumo gc
LEFT OUTER JOIN dbo.FILE_Garantia_Consumo_Hst hst ON LTRIM(gc.CodigoGarantia) = LTRIM(hst.CodigoGarantia)
WHERE hst.CodigoGarantia IS NULL;
GO
