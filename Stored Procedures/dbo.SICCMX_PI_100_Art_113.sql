SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_100_Art_113]
AS
-- SE ACTUALIZA PI AL 100% PARA PERSONAS EMPROBLEMADAS SEGUN ART. 113 FRACCION II DE LA CUB
UPDATE ppi
SET [PI] = 1
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON ppi.IdPersona = credito.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON credito.IdCredito = cInfo.IdCredito
WHERE cInfo.Emproblemado = 1 AND credito.MontoValorizado > 0;

-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT credito.IdPersona, GETDATE(), '', 'Se actualizó la PI al 100% ya que cuenta con Creditos Emproblemados según Art. 113 Fracción II de la CUB'
FROM dbo.SICCMX_VW_Credito_NMC credito
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON credito.IdCredito = cInfo.IdCredito
WHERE cInfo.Emproblemado = 1 AND credito.MontoValorizado > 0;

UPDATE per
SET PI100 = pi100.IdPI100
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
INNER JOIN dbo.SICC_PI100 pi100 ON pi100.Codigo = '21'
WHERE inf.Emproblemado = 1 AND cre.MontoValorizado > 0;
GO
