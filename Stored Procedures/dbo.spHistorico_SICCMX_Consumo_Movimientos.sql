SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Consumo_Movimientos]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Consumo_Movimientos WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Consumo_Movimientos (
	IdPeriodoHistorico,
	Consumo,
	TipoMovimiento,
	Monto,
	Fecha,
	CalificacionAfecto,
	Concepto0419,
	Concepto0420,
	Concepto0424
)
SELECT
	@IdPeriodoHistorico,
	con.Codigo,
	tm.Codigo,
	cm.Monto,
	cm.Fecha,
	cm.CalificacionAfecto,
	cm.Concepto0419,
	cm.Concepto0420,
	cm.Concepto0424
FROM dbo.SICCMX_Consumo_Movimientos cm
INNER JOIN dbo.SICCMX_Consumo con ON cm.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tm ON cm.IdTipoMovimiento = tm.IdTipoMovimiento;
GO
