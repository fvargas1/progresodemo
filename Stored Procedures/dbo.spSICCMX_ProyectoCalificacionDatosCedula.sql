SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoCalificacionDatosCedula]
	@IdProyecto BIGINT
AS
SELECT
	pry.IdProyecto,
	pc.VpTotal,
	pc.UpAcumulada,
	pc.ReservaConstruccion,
	pc.ReservaOperacion,
	pc.SobreCosto,
	pc.MontoCubierto,
	pc.ExposicionSobrecosto,
	cals.Codigo AS CalificacionSobrecosto,
	pc.PorcenajeSobrecosto AS PorcentajeSobrecosto,
	calc.Codigo AS CalificacionCorrida,
	pc.PorcentajeCorrida AS PorcentajeCorrida,
	calf.Codigo AS CalificacionFinal,
	pc.SaldoInsoluto AS SaldoCredito
FROM dbo.SICCMX_Proyecto pry
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cals ON pc.IdCalificacionSobrecosto = cals.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calc ON pc.IdCalificacionCorrida = calc.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calf ON pc.IdCalificacionFinal = calf.IdCalificacion
WHERE pry.IdProyecto = @IdProyecto;
GO
