SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_126]
AS

BEGIN

-- El Monto del Crédito Simple o Monto Autorizado de la Línea de Crédito sin Incluir Accesorios (dat_monto_credito_simple),
-- debe ser menor o igual al Monto de la Línea de Crédito Autorizado (dat_monto_linea_credito_autori).

SELECT
	CodigoCredito,
	MontoSinAccesorios,
	MonLineaCred
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(NULLIF(MontoSinAccesorios,''),0) > ISNULL(NULLIF(MonLineaCred,''),0)

END

GO
