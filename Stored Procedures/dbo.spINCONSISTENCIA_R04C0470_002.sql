SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_002]
AS

BEGIN

-- Si el Número de Días de Mora Promedio con Instituciones Bancarias en los Últimos meses (dat_dias_mora_prom_bcos) es = 0,
-- entonces el Puntaje Asignado por los Días de Mora Promedio con Instituciones Bancarias en los Últimos 12 meses (cve_ptaje_dias_mora_bcos) debe ser = 90 ó = 73

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasMoraInstBanc,
	P_DiasMoraInstBanc AS Puntos_DiasMoraInstBanc
FROM dbo.RW_VW_R04C0470_INC
WHERE DiasMoraInstBanc = '0' AND ISNULL(P_DiasMoraInstBanc,'') NOT IN ('90','73');

END


GO
