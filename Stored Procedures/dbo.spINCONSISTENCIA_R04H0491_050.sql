SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_050]
AS

BEGIN

-- El "TIPO DE COMPROBACIÓN DE INGRESOS DEL ACREDITADO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoComprobacionIngresos
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_TipoIngreso cat ON ISNULL(r.TipoComprobacionIngresos,'') = cat.Codigo
WHERE cat.IdTipoIngreso IS NULL;

END
GO
