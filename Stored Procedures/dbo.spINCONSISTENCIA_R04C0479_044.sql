SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_044]
AS

BEGIN

-- Validar que el Monto del Pago Total Exigible al Acreditado en el Periodo (dat_monto_pago_exigible_acred) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoPagoExigible
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(MontoPagoExigible,''),'-1') AS DECIMAL) < 0;

END


GO
