SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0467_005_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 139, validar que el mismo ID Metodología CNBV
-- (dat_id_credito_met_cnbv) se encuentre en el subreporte 463 con Tipo de Alta (cve_tipo_alta_credito) = 137.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0467_INC rep
LEFT OUTER JOIN Historico.RW_R04C0463 hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE rep.TipoBaja = '139' AND hist.TipoAlta <> '137';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
