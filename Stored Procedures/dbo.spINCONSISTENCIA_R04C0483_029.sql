SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_029]
AS
BEGIN
-- Validar que el Tipo de Alta corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.TipoAltaCredito
FROM dbo.RW_VW_R04C0483_INC rep
LEFT OUTER JOIN dbo.SICC_TipoAlta alta ON ISNULL(rep.TipoAltaCredito,'') = alta.CodigoCNBV
WHERE alta.IdTipoAlta IS NULL;

END
GO
