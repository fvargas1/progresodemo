SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_PI_CalcularValorFinal]
AS

--Se calcula el PI
UPDATE dbo.SICCMX_Persona_PI
SET PI = 1 / (1 + EXP( -1 * ( ( val.X - per.FactorTotal ) * LOG(2)/40)))
FROM dbo.SICCMX_Persona_PI per
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = per.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(per.IdClasificacion, '');


-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'El valor del PI fue de : '
 + CONVERT(VARCHAR, CONVERT(DECIMAL(18,8), ppi.PI * 100))
 + '% y calculado segun la formula 1 / (1 + EXP( -1 * ( ( '
 + CONVERT(VARCHAR, CONVERT(INT, val.X))
 + ' - '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.FactorTotal))
 + ' ) * LOG(2)/40)))'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = ppi.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(ppi.IdClasificacion, '');


INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 credito.IdCredito,
 GETDATE(),
 '',
 'La probabilidad de incumplimiento del deudor es '
 + CAST(CAST(ISNULL(ppi.[PI]*100, 0) AS DECIMAL(10,2)) AS VARCHAR) + '%'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON ppi.IdPersona = credito.IdPersona;


INSERT INTO dbo.SICCMX_LineaCredito_Reservas_Variables_Log (IdLineaCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 linea.IdLineaCredito,
 GETDATE(),
 '',
 'La probabilidad de incumplimiento del deudor es '
 + CAST(CAST(ISNULL(ppi.[PI]*100, 0) AS DECIMAL(10,2)) AS VARCHAR) + '%'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_LineaCredito linea ON ppi.IdPersona = linea.IdPersona;
GO
