SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_093]
AS

BEGIN

-- Si el acreditado no es persona física (cve_tipo_cartera <> 320 y 420), entonces la CURP (dat_curp) debe venir en nulo.

SELECT
	CodigoCredito,
	TipoCartera,
	CURP
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('320','420') AND LEN(ISNULL(CURP,'')) > 0

END


GO
