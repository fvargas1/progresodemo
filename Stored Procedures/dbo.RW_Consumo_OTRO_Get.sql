SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_OTRO_Get]
	@IdReporteLog BIGINT
AS
SELECT
	IdProducto,
	FolioCredito,
	FolioCliente,
	Reestructura,
	FechaInicioCredito,
	FechaTeoricaVencimientoCredito,
	PeriodosFacturacionCredito,
	PlazoTotal,
	ImporteOriginalCredito,
	ValorOriginalBien,
	TasaInteresAnualCredito,
	MecanismoPago,
	CodigoPostal,
	FechaCorte,
	PlazoRemanente,
	SaldoCredito,
	MontoExigible,
	PagoRealizado,
	PromedioPorcentajePagosRealizados,
	DiasAtraso,
	NumeroAtrasos,
	MaximoNumeroAtrasos,
	IndicadorAtraso,
	SumaExigiblesTeoricosCredito,
	TipoGarantia,
	ImporteGarantia,
	RegistroUnicoGarantiasMobiliarias,
	ProbabilidadIncumplimiento,
	SeveridadPerdidaParteNoCubierta,
	SeveridadPerdidaParteCubierta,
	ExposicionIncumplimientoParteNoCubierta,
	ExposicionIncumplimientoParteCubierta,
	MontoReservasConstituirCredito,
	ClasificacionCredito,
	QuitasCondonacionesBonificacionesDescuentos,
	RelacionAcreditadoInstitucion,
	ClaveConsultaSociedadInformacionCrediticia,
	MetodologiaUtilizadaCalculoReservas,
	ProbabilidadIncumplimientoInterna,
	SeveridadPerdidaInterna,
	ExposicionIncumplimientoInterna,
	MontoReservasConstituirCreditoInterno,
	CAT
FROM dbo.RW_VW_Consumo_OTRO
ORDER BY IdProducto;
GO
