SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Accionista_ListErrores]
AS
SELECT
 Deudor,
 NombreAccionista,
 Porcentaje,
 Fuente
FROM dbo.FILE_Accionista
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
