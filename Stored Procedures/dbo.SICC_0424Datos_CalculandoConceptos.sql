SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0424Datos_CalculandoConceptos]
AS
DECLARE @PeriodoAnterior INT;

SELECT @PeriodoAnterior = ant.IdPeriodo
FROM dbo.SICC_Periodo act
INNER JOIN dbo.SICC_Periodo ant ON YEAR(ant.Fecha) = YEAR(DATEADD(month, -1, act.Fecha)) AND MONTH(ant.Fecha) = MONTH(DATEADD(MONTH, -1, act.Fecha))
WHERE act.Activo = 1;


TRUNCATE TABLE R04.[0424Conceptos];

/* Creditos comerciales
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo */
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424CreditosMovimientos_AnteriorTaven_Suma taven ON taven.Codigo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424CreditosMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = cr.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto

UNION ALL
-- Pagos creditos de comerciales
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0424Datos] dat
INNER JOIN SICC_VW_R04A0424CreditosMovimientos_AnteriorTaven_Pagos pagos ON pagos.Codigo = dat.Codigo
GROUP BY dat.Codigo, pagos.Concepto

UNION ALL
-- Pagos posteriores al motimiento tavig de creditos de comerciales
SELECT
 dat.Codigo,
 tavig.Concepto,
 SUM(tavig.Monto)
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_VW_R04A0424CreditosMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = dat.Codigo
GROUP BY dat.Codigo, tavig.Concepto;


-- Insert tavencredito
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo='TAVEN'
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica = '1' -- Historico vigente

UNION ALL
-- Movimientos tavig comercial
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Credito_Movimientos mov
INNER JOIN dbo.SICCMX_Credito cr ON cr.IdCredito = mov.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento 
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


/* Creditos de consumo
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo*/
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424ConsumoMovimientos_AnteriorTaven_Suma taven ON taven.Codigo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424ConsumoMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = cr.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto;


-- Pagos creditos de consumo
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_VW_R04A0424ConsumoMovimientos_AnteriorTaven_Pagos pagos ON pagos.Codigo = dat.Codigo
GROUP BY dat.Codigo, pagos.Concepto

UNION ALL
-- Pagos posteriores al movimiento tavig de creditos de consumo
SELECT
 dat.Codigo,
 tavig.Concepto,
 SUM(tavig.Monto)
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_VW_R04A0424ConsumoMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = dat.Codigo
GROUP BY dat.Codigo, tavig.Concepto;


-- Insert tavenconsumo
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo='TAVEN'
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica = '1' -- Historico vigente

UNION ALL
-- Insert de movimientos tavig
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Consumo_Movimientos mov
INNER JOIN dbo.SICCMX_Consumo cr ON cr.IdConsumo = mov.IdConsumo
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


/* Creditos hipotecarios
En este insert se espera que solo exista un movto taven o tavig.
Los movimientos de creditos con movto taven o tavig se insertan mas abajo*/
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 SUM(mov.Monto)
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424HipotecarioMovimientos_AnteriorTaven_Suma taven ON taven.Codigo = cr.Codigo
LEFT OUTER JOIN dbo.SICC_VW_R04A0424HipotecarioMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = cr.Codigo
WHERE taven.Codigo IS NULL AND tavig.Codigo IS NULL AND conf.TipoMovimiento NOT IN ('TAVEN','TAVIG')
GROUP BY cr.Codigo, conf.Concepto;


-- Pagos Hipotecario de consumo
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 pagos.Concepto,
 SUM(pagos.Monto)
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_VW_R04A0424HipotecarioMovimientos_AnteriorTaven_Pagos pagos ON pagos.Codigo = dat.Codigo
GROUP BY dat.Codigo, pagos.Concepto

UNION ALL
-- Pagos posteriores al movimiento tavig de hipotecario
SELECT
 dat.Codigo,
 tavig.Concepto,
 SUM(tavig.Monto)
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_VW_R04A0424HipotecarioMovimientos_PosteriorTavig_Suma tavig ON tavig.Codigo = dat.Codigo
GROUP BY dat.Codigo, tavig.Concepto;


-- Insert tavenHipotecario
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON info.IdHipotecario = cr.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento AND tmov.Codigo='TAVEN'
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE datos.SituacionActual = '2' -- Credito actual vencido
 AND datos.SituacionHistorica = '1'

UNION ALL
-- Traspaso tavig
SELECT
 cr.Codigo,
 conf.Concepto,
 mov.Monto
FROM dbo.SICCMX_Hipotecario_Movimientos mov
INNER JOIN dbo.SICCMX_Hipotecario cr ON cr.IdHipotecario = mov.IdHipotecario
INNER JOIN dbo.SICC_TipoMovimiento tmov ON tmov.IdTipoMovimiento = mov.IdTipoMovimiento
INNER JOIN R04.[0424Datos] datos ON datos.Codigo = cr.Codigo
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto AND conf.TipoMovimiento = tmov.Codigo
WHERE tmov.Codigo = 'TAVIG';


/*Movimiento Salidas por Reestrucutra: Insertar los creditos que se reestructuraron y que directamente se pasan a cartera vencida*/
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 conf.Concepto,
 CAST(datos.SaldoActual AS DECIMAL) + CAST(datos.InteresVencido AS DECIMAL) + CAST(datos.InteresVigente AS DECIMAL)
FROM R04.[0424Datos] datos
INNER JOIN R04.[0424Configuracion] conf ON datos.CodigoProducto LIKE conf.CodigoProducto
WHERE conf.TipoMovimiento = 'REEST' AND datos.Reestructurado = 1;


-- Valorizamos todos los montos anteriores
UPDATE conceptos
SET Monto = conceptos.Monto * tc.Valor
FROM R04.[0424Conceptos] conceptos
INNER JOIN R04.[0424Datos] dat ON dat.Codigo = conceptos.Codigo
INNER JOIN dbo.SICC_Moneda mon ON dat.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo p ON p.IdPeriodo = tc.IdPeriodo AND p.Activo = 1
WHERE mon.Codigo <> '0' AND conceptos.Concepto NOT LIKE '856%'; -- El ajuste cambiario ya esta valorizado


-- Intereses devengados
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000'
 END Concepto,
 ISNULL(adi.InteresDevengadoValorizado,0)
FROM R04.[0424Datos] datos
INNER JOIN dbo.SICCMX_Credito cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON cr.IdCredito = adi.IdCredito
WHERE datos.SituacionActual ='1'

UNION ALL
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000'
 END Concepto,
 ISNULL(adi.InteresDevengadoValorizado,0)
FROM R04.[0424Datos] datos
INNER JOIN dbo.SICCMX_Consumo cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional adi ON cr.IdConsumo = adi.IdConsumo
WHERE datos.SituacionActual ='1'

UNION ALL
SELECT
 datos.Codigo,
 CASE
 WHEN datos.CodigoProducto LIKE '0101%' THEN '854004101100'
 WHEN datos.CodigoProducto LIKE '0102%' THEN '854004102100'
 WHEN datos.CodigoProducto LIKE '0103%' THEN '854004103100'
 WHEN datos.CodigoProducto LIKE '02%' THEN '854004200000'
 WHEN datos.CodigoProducto LIKE '03%' THEN '854004300000' END Concepto,
 ISNULL(adi.InteresesDevengadosValorizado,0)
FROM R04.[0424Datos] datos
INNER JOIN dbo.SICCMX_Hipotecario cr ON datos.Codigo = cr.Codigo
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional adi ON cr.IdHipotecario = adi.IdHipotecario
WHERE datos.SituacionActual ='1';


/*** Ajuste Cambiario DLLR/UDIS***/
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '856010110000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '856010210000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '856010310000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '856020000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '856030000000'
 END ConceptoActual,
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencida,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencida,0)) * tch.Valor) AS AjusteCambiario
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
INNER JOIN dbo.SICC_TipoCambio tch ON tch.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo perh ON perh.IdPeriodo = tch.IdPeriodo AND perh.IdPeriodo = @PeriodoAnterior
WHERE CodigoProducto IS NOT NULL
 AND ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencida,0)) * tc.Valor) -
 ((ISNULL(dat.SaldoHistorico,0)+ISNULL(dat.InteresVencidoHistorico,0)+ISNULL(dat.InteresVigenteHistorico,0)+ISNULL(dat.InteresCarteraVencida,0)) * tch.Valor) <> 0
 AND mon.Codigo <> '0'
 AND dat.SituacionActual = '1' AND dat.SituacionHistorica = '1';


-- Conceptos Saldos Iniciales
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '851011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '851021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '851031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '852000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '853000000000'
 END ConceptoHistorico,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoHistorico,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVencidoHistorico,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVigenteHistorico,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoHistorico,0) + ISNULL(dat.InteresVencidoHistorico,0) + ISNULL(dat.InteresVigenteHistorico,0)) * tc.Valor AS DECIMAL)
 END AS SaldoHistorico
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.R0424CreditosTavig_List tavig ON dat.Codigo = tavig.Codigo
WHERE tc.IdPeriodo IN (
 SELECT ant.IdPeriodo
 FROM dbo.SICC_Periodo act
 INNER JOIN dbo.SICC_Periodo ant ON YEAR(ant.Fecha) = YEAR(DATEADD(month, -1, act.Fecha)) AND MONTH(ant.Fecha) = MONTH(DATEADD(MONTH, -1, act.Fecha))
 WHERE act.Activo = 1
)
AND dat.CodigoProducto IS NOT NULL
AND tavig.Codigo IS NULL -- omitir los creditos con movimiento tavig
AND dat.SituacionHistorica='1'

UNION ALL
-- Conceptos Saldos Actuales
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 CASE WHEN mon.CodigoISO = 'MXN'
 THEN CAST((CAST(ISNULL(dat.SaldoActual,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVigente,0) AS DECIMAL) + CAST(ISNULL(dat.InteresVencido,0) AS DECIMAL)) * tc.Valor AS DECIMAL)
 ELSE CAST((ISNULL(dat.SaldoActual,0) + ISNULL(dat.InteresVigente,0) + ISNULL(dat.InteresVencido,0)) * tc.Valor AS DECIMAL)
 END AS SaldoActual
FROM R04.[0424Datos] dat
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = dat.IdMoneda
INNER JOIN dbo.SICC_TipoCambio tc ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON per.IdPeriodo = tc.IdPeriodo AND per.Activo = 1
WHERE dat.CodigoProducto IS NOT NULL AND dat.SituacionActual = '1';


-- Los creditos TAVEN deben tener saldo cero en el 424
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 0
FROM R04.[0424Datos] dat
WHERE dat.CodigoProducto IS NOT NULL AND dat.SituacionActual = '2' AND dat.SituacionHistorica = '1';


/* Insertamos almenos un registro en cero como saldo final si es que no hay ningun registro en 424Conceptos que no sea saldo historico en cero*/
INSERT INTO R04.[0424Conceptos] (Codigo, Concepto, Monto)
SELECT
 dat.Codigo,
 CASE
 WHEN dat.CodigoProducto LIKE '0101%' THEN '861011000000'
 WHEN dat.CodigoProducto LIKE '0102%' THEN '861021000000'
 WHEN dat.CodigoProducto LIKE '0103%' THEN '861031000000'
 WHEN dat.CodigoProducto LIKE '02%' THEN '862000000000'
 WHEN dat.CodigoProducto LIKE '03%' THEN '863000000000'
 END ConceptoActual,
 0
FROM R04.[0424Datos] dat
WHERE dat.CodigoProducto IS NOT NULL AND (SELECT COUNT(Codigo) FROM R04.[0424Conceptos] WHERE Concepto LIKE '86%') = 0;
GO
