SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_023_Count]


 @IdInconsistencia BIGINT


AS


BEGIN





-- El ID met CNBV deberá venir en letras mayúsculas y números, sin caracteres distintos a estos.





DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;





DECLARE @count INT;





SELECT @count = COUNT(CodigoCredito)


FROM dbo.RW_R04C0442


WHERE CodigoCreditoCNBV LIKE '%[^A-Z0-9_]%' AND CodigoCreditoCNBV NOT LIKE '%&%'  OR BINARY_CHECKSUM(CodigoCreditoCNBV) <> BINARY_CHECKSUM(UPPER(CodigoCreditoCNBV));





INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());





SELECT @count;





END
GO
