SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_A18_GP_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @query VARCHAR(8000);
DECLARE @Ids VARCHAR(2000);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_Analitico_A18_GP';
SELECT @Ids = STUFF((SELECT'],[' + LTRIM(C.Nombre) + '_V],[' + LTRIM(C.Nombre) + '_P'
FROM SICCMX_PI_Variables C
WHERE C.IdMetodologia=7 GROUP BY C.Nombre ORDER BY MIN(C.Orden) FOR XML PATH('')), 1, 2, '') + ']';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();

SET @query =
'SELECT * FROM(
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, esg.Nombre AS TipoGarantia, piv.Nombre+''_V'' AS Nombre,
CASE WHEN cat.Nombre IS NULL THEN CASE WHEN det.PuntosActual IS NULL THEN '''' ELSE ISNULL(CAST(CAST(det.ValorActual AS DECIMAL(22,4)) AS VARCHAR), ''Sin información'') END ELSE
cat.Codigo + ''. '' + REPLACE(cat.Nombre,'','','''') END AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Aval per
INNER JOIN SICCMX_Persona_PI_Detalles_GP det ON det.IdGP=per.IdAval
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP=det.IdGP AND ppi.EsGarante=det.EsGarante
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante=esg.IdEsGarante
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON det.IdVariable=rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo=cat.IdCatalogo AND SUBSTRING(REPLACE(CAST(det.ValorActual AS VARCHAR),''.'',''''),1,3)=cat.Codigo
WHERE piv.IdMetodologia=7 AND esg.Layout = ''AVAL''
UNION ALL
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, esg.Nombre AS TipoGarantia, piv.Nombre+''_P'' AS Nombre,
ISNULL(CAST(det.PuntosActual AS VARCHAR),'''') AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Aval per
INNER JOIN SICCMX_Persona_PI_Detalles_GP det ON det.IdGP=per.IdAval
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP=det.IdGP AND ppi.EsGarante=det.EsGarante
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante=esg.IdEsGarante
WHERE piv.IdMetodologia=7 AND esg.Layout = ''AVAL''
UNION ALL
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Descripcion,'','','''') AS Cte_Nombre, esg.Nombre AS TipoGarantia, piv.Nombre+''_V'' AS Nombre,
CASE WHEN cat.Nombre IS NULL THEN CASE WHEN det.PuntosActual IS NULL THEN '''' ELSE ISNULL(CAST(CAST(det.ValorActual AS DECIMAL(22,4)) AS VARCHAR), ''Sin información'') END ELSE
cat.Codigo + ''. '' + REPLACE(cat.Nombre,'','','''') END AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Garantia per
INNER JOIN SICCMX_Persona_PI_Detalles_GP det ON det.IdGP=per.IdGarantia
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP=det.IdGP AND ppi.EsGarante=det.EsGarante
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante=esg.IdEsGarante
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON det.IdVariable=rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo=cat.IdCatalogo AND SUBSTRING(REPLACE(CAST(det.ValorActual AS VARCHAR),''.'',''''),1,3)=cat.Codigo
WHERE piv.IdMetodologia=7 AND esg.Layout = ''GARANTIA''
UNION ALL
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Descripcion,'','','''') AS Cte_Nombre, esg.Nombre AS TipoGarantia, piv.Nombre+''_P'' AS Nombre,
ISNULL(CAST(det.PuntosActual AS VARCHAR),'''') AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Garantia per
INNER JOIN SICCMX_Persona_PI_Detalles_GP det ON det.IdGP=per.IdGarantia
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI_GP ppi ON ppi.IdGP=det.IdGP AND ppi.EsGarante=det.EsGarante
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante=esg.IdEsGarante
WHERE piv.IdMetodologia=7 AND esg.Layout = ''GARANTIA''
) t PIVOT (max(Dato) FOR Nombre IN ('+@Ids+')) AS pvt';


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Analitico_A18_GP;

-- Informacion para reporte
INSERT INTO dbo.RW_Analitico_A18_GP (
 IdReporteLog, Codigo, Nombre, TipoGarantia, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI], DiasMoraPromedio_V, DiasMoraPromedio_P, PorPagosInstBancarias_V,
 PorPagosInstBancarias_P, PorPagosInstNoBancarias_V, PorPagosInstNoBancarias_P, NumInstCalif_V, NumInstCalif_P, DeudaTotalPartEleg_V, DeudaTotalPartEleg_P,
 ServicioDeudaIngresosTotales_V, ServicioDeudaIngresosTotales_P, DeudaCortoPlazoDeudaTotal_V, DeudaCortoPlazoDeudaTotal_P, IngresosTotalesGastoCorr_V,
 IngresosTotalesGastoCorr_P, InvIngresosTotales_V, InvIngresosTotales_P, IngPropiosIngTotales_V, IngPropiosIngTotales_P, TasaDesempleoLocal_V, TasaDesempleoLocal_P,
 PresSerFinEntidadesReg_V, PresSerFinEntidadesReg_P, ObligContDerivadasBenef_V, ObligContDerivadasBenef_P, BalanceOperativoPIB_V, BalanceOperativoPIB_P,
 NivelEficienciaRec_V, NivelEficienciaRec_P, SolidezFlexEjecPresupuesto_V, SolidezFlexEjecPresupuesto_P, SolidezFlexImpLocales_V, SolidezFlexImpLocales_P,
 TranspFinanzasPublicas_V, TranspFinanzasPublicas_P, EmisionesBursatiles_V, EmisionesBursatiles_P
)
EXECUTE (@query);


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_A18_GP WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
