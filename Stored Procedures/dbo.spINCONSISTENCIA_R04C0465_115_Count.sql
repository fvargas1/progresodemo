SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_115_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- El PORCENTAJE DE PAGOS A ENTIDADES COMERCIALES CON 60 O MÁS DÍAS DE ATRASO EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE LEN(PorcPagoEntComer) > 0 AND PorcPagoEntComer <> '-99'

	AND (CHARINDEX('.',PorcPagoEntComer) = 0 OR CHARINDEX('.',PorcPagoEntComer) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoEntComer, CHARINDEX('.', PorcPagoEntComer) + 1, LEN(PorcPagoEntComer)))) <> 6);



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
