SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_018]
AS

BEGIN

-- El tipo de alta del crédito debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.TipoAltaCredito
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TipoAltaMA cat ON ISNULL(r.TipoAltaCredito,'') = cat.CodigoCNBV
WHERE cat.IdTipoAlta IS NULL;

END
GO
