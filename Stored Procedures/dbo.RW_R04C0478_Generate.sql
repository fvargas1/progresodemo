SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0478_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0478';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0478;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0478 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActividadEconomica, GrupoRiesgo, LocalidadDeudor,
 DomicilioMunicipio, DomicilioEstado, NumInfoCrediticia, LEI, TipoAlta, TipoProducto, TipoOperacion, DestinoCredito, CodigoCredito,
 CodigoCreditoCNBV, GrupalCNBV, MontoLineaAutorizado, FechaMaxDisponer, FechaVencLinea, Moneda, FormaDisposicion, TipoLinea, PrelacionPago,
 NoRUGM, PrctPartFed, InstAgenciaExt, TasaReferencia, AjusteTasaReferencia, OperacionTasaReferencia, FrecRevTasa, PeriodicidadCapital,
 PeriodicidadInteres, MesesGraciaCapital, MesesGraciaInteres, ComAperturaTasa, ComAperturaMonto, ComDispTasa, ComDispMonto,
 LocalidadDestinoCredito, MunicipioDestinoCredito, EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0478',
 per.Codigo AS CodigoPersona,
 per.RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 tpoPer.CodigoCNBV AS TipoCartera,
 actEco.CodigoCNBV AS ActividadEconomica,
 CASE WHEN LEN(ISNULL(pInfo.GrupoRiesgo,'')) = 0 OR pInfo.GrupoRiesgo LIKE '%SIN GRUPO%' THEN REPLACE(pInfo.NombreCNBV, ',', '') ELSE pInfo.GrupoRiesgo END AS GrupoRiesgo,
 mun.CodigoCNBV AS LocalidadDeudor,
 pInfo.Municipio AS DomicilioMunicipio,
 pInfo.Estado AS DomicilioEstado,
 lin.FolioConsultaBuro AS NumInfoCrediticia,
 pInfo.LEI AS LEI,
 tpoAlta.CodigoCNBV AS IdTipoAlta,
 tcc.Codigo AS TipoProducto,
 tpoOper.CodigoCNBV AS TipoOperacion,
 dest.CodigoCNBVOD AS DestinoCredito,
 lin.Codigo AS CodigoCredito,
 cnbv.CNBV AS CodigoCreditoCNBV,
 grp.GrupalCNBV AS GrupalCNBV,
 lin.MontoLinea AS MontoLineaAutorizado,
 CASE WHEN lin.FecMaxDis IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecMaxDis,102),'.',''),1,6) END AS FechaMaxDisponer,
 CASE WHEN lin.FecVenLinea IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,lin.FecVenLinea,102),'.',''),1,6) END AS FechaVencLinea,
 mon.CodigoCNBV AS Moneda,
 disp.CodigoCNBV AS FormaDisposicion,
 tln.Codigo AS TipoLinea,
 pos.Codigo AS PrelacionPago,
 rgm.RegGarantiaMob AS NoRUGM,
 lin.PorcPartFederal AS PrctPartFed,
 '' AS InstAgenciaExt,
 tasaRef.Codigo AS TasaReferencia,
 tasa.AjusteTasa AS AjusteTasaReferencia,
 tasa.OperacionDiferencial AS OperacionTasaReferencia,
 lin.FrecuenciaRevisionTasa AS FrecRevTasa,
 perCap.CodigoCNBV AS PeriodicidadCapital,
 perInt.CodigoCNBV_A22 AS PeriodicidadInteres,
 lin.MesesGraciaCap AS MesesGraciaCapital,
 lin.MesesGraciaIntereses AS MesesGraciaInteres,
 lin.GastosOrigTasa AS ComAperturaTasa,
 lin.ComisionesCobradas AS ComAperturaMonto,
 lin.ComDispTasa AS ComDispTasa,
 lin.ComDispMonto AS ComDispMonto,
 munDst.CodigoCNBV AS LocalidadDestinoCredito,
 munDst.CodigoMunicipio AS MunicipioDestinoCredito,
 munDst.CodigoEstado AS EstadoDestinoCredito,
 actDst.CodigoCNBV AS ActividadDestinoCredito
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Anexo22 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICMCX_VW_Clientes_A19_Rep anx19 ON per.IdPersona = anx19.IdPersona AND anx19.Metodologia = '22OD'
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON pInfo.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON per.IdActividadEconomica = actEco.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Municipio mun ON per.IdLocalidad = mun.IdMunicipio
LEFT OUTER JOIN dbo.SICC_TipoAlta tpoAlta ON lin.IdTipoAlta = tpoAlta.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON lin.ProductoComercial = tcc.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON lin.TipoOperacion = tpoOper.IdTipoOperacion
LEFT OUTER JOIN dbo.SICC_Destino dest ON lin.IdDestino = dest.IdDestino
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_Grupal grp ON lin.IdLineaCredito = grp.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_Moneda mon ON lin.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON lin.IdDisposicion = disp.IdDisposicion
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON lin.TipoLinea = tln.IdTipoLinea
LEFT OUTER JOIN dbo.SICC_Posicion pos ON lin.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_RegGarMob rgm ON lin.IdLineaCredito = rgm.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON lin.IdTasaReferencia = tasaRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICCMX_VW_LineaCredito_AjusteTasa tasa ON lin.IdLineaCredito = tasa.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON lin.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON lin.IdPeriodicidadInteres = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_Municipio munDst ON lin.IdMunicipioDestino = munDst.IdMunicipio
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actDst ON lin.IdActividadEconomicaDestino = actDst.IdActividadEconomica
WHERE ((met.Codigo='22' AND ISNULL(anx.OrgDescPartidoPolitico,0) = 1) OR (met.Codigo='4' AND anx19.IdPersona IS NOT NULL)) AND lin.IdTipoAlta IS NOT NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0478 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
