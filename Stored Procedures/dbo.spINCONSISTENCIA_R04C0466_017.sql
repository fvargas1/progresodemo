SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_017]
AS

BEGIN

-- La severidad de la pérdida asociada al porcentaje descubierto debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP AS SeveridadPerdida
FROM dbo.RW_R04C0466
WHERE CAST(ISNULL(NULLIF(SP,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
