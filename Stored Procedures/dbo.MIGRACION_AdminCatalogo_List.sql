SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_AdminCatalogo_List]
AS
SELECT
	vw.IdAdminCatalog,
	vw.Nombre,
	vw.CodeName,
	vw.NombreTabla,
	vw.NombreIdentidad,
	vw.NombreBusqueda
FROM dbo.MIGRACION_VW_AdminCatalogo vw;
GO
