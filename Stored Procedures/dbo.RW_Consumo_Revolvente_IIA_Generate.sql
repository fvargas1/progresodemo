SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_IIA_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'CONSUMO' AND Nombre = '-Revolvente_IIA';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Consumo_Revolvente_IIA;

INSERT INTO dbo.RW_Consumo_Revolvente_IIA (
	IdReporteLog, Producto, FolioCredito, LimCredito, SaldoTot, PagoNGI, SaldoRev, InteresRev, SaldoPMSI, Meses, PagoMin, TasaRev, SaldoPCI,
	InteresPCI, SaldoPagar, PagoReal, LimCreditoA, PagoExige, ImpagosC, ImpagoSum, MesApert, SaldoCont, Situacion, ProbInc, SevPer, ExpInc,
	MReserv, Relacion, ClasCont, CveCons, limcreditocalif, montopagarinst, montopagarsic, mesantig, mesesic, segmento, gveces, garantia,
	Catcuenta, indicadorcat, FolioCliente, CP, comtotal, comtardio, pagonginicio, pagoexigepsi
)
SELECT
	@IdReporteLog,
	con.IdProductoNombre AS Producto,
	info.CreditoCodificado AS FolioCredito,
	CAST(info.LimCreditoValorizado AS DECIMAL) AS LimCredito,
	CAST(con.SaldoTotalValorizado AS DECIMAL(23,2)) AS SaldoTot,
	CAST(info.PagoNGIValorizado AS DECIMAL(23,2)) AS PagoNGI,
	CAST(info.SaldoRevValorizado AS DECIMAL(23,2)) AS SaldoRev,
	CAST(con.InteresVigenteValorizado AS DECIMAL(23,2)) AS InteresRev,
	CAST(info.SaldoPMSIValorizado AS DECIMAL(23,2)) AS SaldoPMSI,
	CAST(0 AS INT) AS Meses,
	CAST(info.PagoMinValorizado AS DECIMAL(23,2)) AS PagoMin,
	CAST(con.TasaInteres AS DECIMAL(10,2)) AS TasaRev,
	CAST(info.SaldoPCIValorizado AS DECIMAL(23,2)) AS SaldoPCI,
	CAST(info.InteresPCI AS DECIMAL(23,2)) AS InteresPCI,
	CAST(con.SaldoTotalValorizado AS DECIMAL(23,2)) AS SaldoPagar,
	CAST(info.PagoRealizadoValorizado AS DECIMAL(23,2)) AS PagoReal,
	CAST(pre.LimiteCredito AS DECIMAL(23,2)) AS LimCreditoA,
	CAST(info.PagoExigibleValorizado AS DECIMAL(23,2)) AS PagoExige,
	CAST(pre.NumeroImpagosConsecutivos AS INT) AS ImpagosC,
	CAST(pre.NumeroImpagosHistoricos AS INT) AS ImpagoSum,
	CAST(pre.MesesTranscurridos AS INT) AS MesApert,
	CAST(con.SaldoTotalValorizado AS DECIMAL(23,2)) AS SaldoCont,
	vwSit.Situacion AS Situacion,
	CAST(crv.[PI] * 100 AS DECIMAL(5,2)) AS ProbInc,
	CAST(crv.SPExpuesta * 100 AS DECIMAL(5,2)) AS SevPer,
	CAST(crv.E AS DECIMAL(23,2)) AS ExpInc,
	CAST(crv.ReservaTotal AS DECIMAL(23,2)) AS MReserv,
	dr.CodigoCNBV AS Relacion,
	sit.CodigoBanxico AS ClasCont,
	info.FolioConsultaBuro AS CveCons,
	CAST(ISNULL(info.limcreditocalif,0) AS DECIMAL) AS limcreditocalif,
	CAST(ISNULL(info.MonExiIns,0) AS DECIMAL(23,2)) AS montopagarinst,
	CAST(ISNULL(info.MonExiRepSIC,0) AS DECIMAL(23,2)) AS montopagarsic,
	CAST(info.AntiguedadAcreditado AS DECIMAL) AS mesantig,
	CAST(info.Meses_desde_ult_atr_bk AS DECIMAL) AS mesesic,
	CASE WHEN pre.Alto = 1 THEN 1 WHEN pre.Medio = 1 THEN 2 WHEN pre.Bajo = 1 THEN 3 END AS segmento,
	CASE WHEN info.GVeces1 = 1 THEN 1 WHEN info.GVeces2 = 1 THEN 2 WHEN info.GVeces3 = 1 THEN 3 END AS gveces,
	ISNULL(ind.CodigoBanxico,'1') AS garantia,
	CAST(info.CAT AS DECIMAL(10,1)) AS Catcuenta,
	info.indicadorcat AS indicadorcat,
	info.ClienteCodificado AS FolioCliente,
	pInfo.CodigoPostal AS CP,
	CAST(ISNULL(info.comtotal,0) AS DECIMAL(23,2)) AS comtotal,
	CAST(ISNULL(info.comtardio,0) AS DECIMAL(23,2)) AS comtardio,
	CAST(ISNULL(info.pagonginicio,0) AS DECIMAL(23,2)) AS pagonginicio,
	CAST(ISNULL(info.pagoexigepsi,0) AS DECIMAL(23,2)) AS pagoexigepsi
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_VW_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON info.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON pre.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_ConsumoRev_Situacion vwSit ON con.IdConsumo = vwSit.IdConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON con.IdSituacionCredito = sit.IdSituacionConsumo
LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA dr ON per.IdDeudorRelacionado = dr.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_IndicadorGarantia_Consumo ind ON info.idgarantia = ind.IdIndicador
WHERE con.Revolvente = 1;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Consumo_Revolvente_IIA WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
