SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_TipoCambio_Fecha_Datetime]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_TipoCambio_Fecha_Datetime';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_TipoCambio
SET errorFormato = 1
WHERE LEN(FechaTipoCambio)>0 AND ISDATE(FechaTipoCambio) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_TipoCambio_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoMonedaISO,
 'FechaTipoCambio',
 FechaTipoCambio,
 1,
 @Detalle
FROM dbo.FILE_TipoCambio
WHERE LEN(FechaTipoCambio)>0 AND ISDATE(FechaTipoCambio) = 0;
GO
