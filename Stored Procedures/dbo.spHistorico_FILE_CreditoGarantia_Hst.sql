SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_CreditoGarantia_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_CreditoGarantia_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_CreditoGarantia_Hst (
 IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoGarantia,
 CodigoCredito,
 Porcentaje,
 Fuente
FROM dbo.FILE_CreditoGarantia_Hst;
GO
