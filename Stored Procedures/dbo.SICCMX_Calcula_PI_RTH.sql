SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_PI_RTH]
AS
DECLARE @IdMetodologia INT;
DECLARE @FechaPeriodo DATETIME;
DECLARE @bw_config CHAR(1);

SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '2';
SET @FechaPeriodo =(SELECT Fecha FROM dbo.SICC_Periodo WHERE Activo = 1);


UPDATE pre
SET ATR = 0,
	PPagoIM = 1
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON pre.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICC_TipoRegimen tr ON info.IdTipoRegimenCredito = tr.IdTipoRegimen AND tr.Codigo = '1'
WHERE pre.IdMetodologia = @IdMetodologia;


-- Calculamos la PI cuando el ATR es mayor a lo maximo para cada Metodologia
INSERT INTO dbo.SICCMX_Hipotecario_Reservas_Variables (IdHipotecario, [PI], E)
SELECT
 vp.IdHipotecario,
 1,
 hip.SaldoTotalValorizado
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR >= const.ATRPI
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON hip.IdHipotecario = vp.IdHipotecario
WHERE vp.IdMetodologia = @IdMetodologia;


-- Calculamos la PI cuando el ATR es menor a lo maximo para cada Metodologia
INSERT INTO dbo.SICCMX_Hipotecario_Reservas_Variables (IdHipotecario, [PI], E)
SELECT
 vp.IdHipotecario,
 1 /
 -- Esto es lo que se tiene que cambiar para cada Metodologia
 ( 1 + EXP( -1 * ( const.Constante + (const.ATR * vp.ATR) + (const.VECES * vp.Veces) + (const.PorVPAGO * vp.PPagoIM)))),
 hip.SaldoTotalValorizado
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR < const.ATRPI
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON hip.IdHipotecario = vp.IdHipotecario
WHERE const.IdMetodologia = @IdMetodologia;


SELECT @bw_config = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName='RTH_ATR_PAGO';

IF @bw_config = '1'
BEGIN
-- Calculamos la PI para Creditos con menos de 4 periodos de antiguedad
-- Atrasos = 0
-- Porcentaje de Pago = 100%
-- Se debe activar en BAJAWARE_Config solo si el banco lo solicita
SELECT 1 / ( 1 + EXP( -1 * ( const.Constante + (const.ATR * 0.00) + (const.VECES * vp.Veces) + (const.PorVPAGO * 1.00))))
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON hrv.IdHipotecario = vp.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON vp.IdHipotecario = info.IdHipotecario
WHERE const.IdMetodologia = @IdMetodologia AND DATEDIFF(MONTH, info.FechaOtorgamiento, @FechaPeriodo) < 4;
END
GO
