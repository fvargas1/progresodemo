SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_031_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de Ingresos Propios (dat_saldo_ingresos_propios) es > 9.3,
-- entonces el Puntaje Ingresos Propios a Ingresos Totales (cve_puntaje_ingreso_propio) debe ser = 70

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngPropios,''),'0') AS DECIMAL(18,6)) > 9.3 AND ISNULL(P_IngPropIngTotales,'') <> '70';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
