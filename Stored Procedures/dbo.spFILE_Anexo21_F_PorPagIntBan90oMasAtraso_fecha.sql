SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_F_PorPagIntBan90oMasAtraso_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_F_PorPagIntBan90oMasAtraso_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo21
SET errorFormato = 1
WHERE (LEN(F_PorPagIntBan90oMasAtraso)>0 AND ISDATE(F_PorPagIntBan90oMasAtraso) = 0) OR F_PorPagIntBan90oMasAtraso > @FechaPeriodo;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_PorPagIntBan90oMasAtraso',
 F_PorPagIntBan90oMasAtraso,
 1,
 @Detalle
FROM dbo.FILE_Anexo21
WHERE (LEN(F_PorPagIntBan90oMasAtraso)>0 AND ISDATE(F_PorPagIntBan90oMasAtraso) = 0) OR F_PorPagIntBan90oMasAtraso > @FechaPeriodo;
GO
