SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_UseCase_Group_MenuOptions]
	@IdUser BAJAWARE_utID,
	@IdApplication BAJAWARE_utID
AS
SELECT DISTINCT
	uc.IdUseCase,
	uc.Active,
	uc.[Description],
	uc.[Name],
	uc.CodeName,
	uc.Visible,
	uc.PageLink,
	uc.IdUseCaseGroup,
	uc.SortOrder,
	uc.StringKey,
	ucg.SortOrder AS SortGroup
FROM dbo.BAJAWARE_Rel_UseCase_Group rel
INNER JOIN BAJAWARE_UseCase uc ON uc.IdUseCase = rel.IdUseCase
INNER JOIN BAJAWARE_UseCaseGroup ucg ON uc.IdUseCaseGroup = ucg.IdUseCaseGroup
INNER JOIN BAJAWARE_Group g ON g.IdGroup = rel.IdGroup
INNER JOIN BAJAWARE_VirtualGroup vg ON g.IdGroup = vg.IdGroup
WHERE vg.IdUser = @IdUser AND ucg.IdApplication = @IdApplication AND uc.Visible = 1 AND uc.Active = 1
ORDER BY ucg.SortOrder, uc.SortOrder, uc.IdUseCaseGroup;
GO
