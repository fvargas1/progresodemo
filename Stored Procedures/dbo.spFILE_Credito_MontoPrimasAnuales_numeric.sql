SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_MontoPrimasAnuales_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_MontoPrimasAnuales_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Credito
SET errorFormato = 1
WHERE LEN(MontoPrimasAnuales)>0 AND (ISNUMERIC(ISNULL(MontoPrimasAnuales,'0')) = 0 OR MontoPrimasAnuales LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCredito,
	'MontoPrimasAnuales',
	MontoPrimasAnuales,
	1,
	@Detalle
FROM dbo.FILE_Credito
WHERE LEN(MontoPrimasAnuales)>0 AND (ISNUMERIC(ISNULL(MontoPrimasAnuales,'0')) = 0 OR MontoPrimasAnuales LIKE '%[^0-9 .]%');
GO
