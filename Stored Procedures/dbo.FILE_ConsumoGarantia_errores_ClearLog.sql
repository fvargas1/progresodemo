SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ConsumoGarantia_errores_ClearLog]
AS
UPDATE dbo.FILE_ConsumoGarantia
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_ConsumoGarantia_errores;
GO
