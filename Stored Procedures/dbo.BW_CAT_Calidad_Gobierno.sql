SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_Calidad_Gobierno]
	@filtro VARCHAR(50)
AS
SELECT Codigo, Nombre
FROM dbo.CAT_VW_Calidad_Gobierno
WHERE ISNULL(Codigo,'')+'|'+ISNULL(Nombre,'') LIKE '%' + @filtro + '%';
GO
