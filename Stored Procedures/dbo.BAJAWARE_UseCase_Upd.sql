SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_Upd]
	@IdUseCase BAJAWARE_utID,
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@Name BAJAWARE_utDescripcion100,
	@CodeName BAJAWARE_utDescripcion100,
	@Visible BAJAWARE_utActivo,
	@PageLink BAJAWARE_utDescripcion250,
	@IdUseCaseGroup BAJAWARE_utID,
	@SortOrder BAJAWARE_utInt,
	@StringKey BAJAWARE_utDescripcion100
AS
UPDATE dbo.BAJAWARE_UseCase
SET
	Active = @Active,
	Description = @Description,
	Name = @Name,
	CodeName = @CodeName,
	Visible = @Visible,
	PageLink = @PageLink,
	IdUseCaseGroup = @IdUseCaseGroup,
	SortOrder = @SortOrder,
	StringKey = @StringKey 
WHERE IdUseCase = @IdUseCase;
GO
