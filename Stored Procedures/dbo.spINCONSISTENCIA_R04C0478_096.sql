SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_096]
AS

BEGIN

-- Validar que el porcentaje de participaciones federales esté en base 100. Valores entre 1 y 100. Se aceptan hasta 6 decimales.

SELECT
	CodigoCredito,
	PorcPartFederal
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(NULLIF(PorcPartFederal,''),'-1') AS DECIMAL(18,6)) NOT BETWEEN 0 AND 100 OR ISNULL(NULLIF(PorcPartFederal,''),'-1') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]'

END


GO
