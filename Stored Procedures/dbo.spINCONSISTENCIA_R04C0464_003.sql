SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_003]
AS

BEGIN

-- El campo EXPOSIC CUBIER GTIA PERSONAL debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	EICubierta AS EI_Cubierta
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(EICubierta,''),'-1') AS DECIMAL) < 0;

END

GO
