SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_054]
AS
BEGIN
-- Si el Tipo de Alta del Crédito es mayor o igual a 700, el Tipo de Operación debe ser 181 ó 256

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoAltaCredito,
	TipoOperacion
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(ISNULL(TipoAltaCredito,'0') AS INT) >= 700 AND ISNULL(TipoOperacion,'') NOT IN ('181','256');

END

GO
