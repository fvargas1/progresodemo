SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_022]
AS
BEGIN
-- El RFC dentro del ID (COL.20) no corresponde con el acreditado

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0458_INC
WHERE CHARINDEX(ISNULL(RFC,''), ISNULL(CodigoCreditoCNBV,'')) = 0;

END
GO
