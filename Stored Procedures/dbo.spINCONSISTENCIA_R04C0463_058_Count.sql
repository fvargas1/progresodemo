SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_058_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Tipo de Cartera (cve_tipo_cartera) es DIFERENTE de 410 ó 420, la Clave de Estado (cve_estado) debe estar dentro 1 a 32

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('410','420') AND CAST(ISNULL(Estado,'0') AS INT) NOT BETWEEN 1 AND 32;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
