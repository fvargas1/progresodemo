SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0450_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0450_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0450_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0450_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, RFC_Garante, NombreGarante, CodigoGarante, PI_Garante, SP_Garante, EI_Garante, ActEconomica, Localidad,
 Municipio, Estado, LEI, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, NombreAcreditado, TipoGarantia, CodigoGarantia, MonedaGarantia,
 MontoGarantia, PrctGarantia
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '450',
 gar.RFCGarante AS RFC_Garante,
 gar.NombreGarante AS NombreGarante,
 gar.IdGarante AS CodigoGarante,
 gar.PIGarante * 100 AS PI_Garante,
 '' AS SP_Garante,
 gar.ValorGtiaValorizado AS EI_Garante,
 act.CodigoCNBV AS ActEconomica,
 mun.CodigoCNBV AS Localidad,
 gar.MunicipioGarante AS Municipio,
 gar.EstadoGarante AS Estado,
 gar.LEI AS LEI,
 cnbv.CNBV AS CodigoCreditoCNBV,
 lin.Codigo AS CodigoCredito,
 cre.Codigo AS NumeroDisposicion,
 pInfo.NombreCNBV AS NombreAcreditado,
 tg.CodigoCNBV AS TipoGarantia,
 CASE WHEN gar.Codigo = cgp.CodigoPortafolio THEN cgp.CodigoGarantia ELSE gar.Codigo END AS CodigoGarantia,
 mon.CodigoCNBV AS MonedaGarantia,
 gar.ValorGarantia AS MontoGarantia,
 cg.PorcCubiertoCredito * 100 AS PrctGarantia
FROM dbo.SICCMX_VW_Garantia gar
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON gar.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICCMX_Credito cre ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON cre.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON gar.IdActividaEcoGarante = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON gar.IdLocalidadGarante = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICCMX_CreditoGarantia_Portafolio cgp ON cre.Codigo = cgp.CodigoCredito

UNION ALL

SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '450',
 aval.RFC AS RFC_Garante,
 aval.Nombre AS NombreGarante,
 aval.Codigo AS CodigoGarante,
 aval.PIAval * 100 AS PI_Garante,
 '' AS SP_Garante,
 ca.Monto AS EI_Garante,
 act.CodigoCNBV AS ActEconomica,
 mun.CodigoCNBV AS Localidad,
 mun.CodigoMunicipio AS Municipio,
 mun.CodigoEstado AS Estado,
 aval.LEI AS LEI,
 cnbv.CNBV AS CodigoCreditoCNBV,
 lin.Codigo AS CodigoCredito,
 cre.CodigoCredito AS NumeroDisposicion,
 pInfo.NombreCNBV AS NombreAcreditado,
 ISNULL(fig.CodigoCNBV, '300') AS TipoGarantia,
 aval.Codigo AS CodigoGarantia,
 mon.CodigoCNBV AS MonedaGarantia,
 ca.Monto AS MontoGarantia,
 ca.Porcentaje * 100 AS PrctGarantia
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.SICCMX_CreditoAval ca ON aval.IdAval = ca.IdAval
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON ca.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON cre.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON aval.IdActividadEconomica = act.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON aval.IdMunAval = mun.IdLocalidad
LEFT OUTER JOIN dbo.SICC_Moneda mon ON aval.IdMoneda = mon.IdMoneda;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0450_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
