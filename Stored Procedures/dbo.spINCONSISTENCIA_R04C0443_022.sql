SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_022]
AS

BEGIN

-- Se debe indicar el monto de crédito dispuesto del periodo. En caso de que no haya disposiciones en el periodo que se reporta, se anotará 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, MontoDispuesto
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
