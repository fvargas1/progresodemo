SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0485_014]
AS
BEGIN
-- Cuando la Fecha de Otorgamiento contenida en el ID metodología CNBV (dat_id_credito_met_cnbv)
-- posiciones 8 a la 13 sea IGUAL al periodo que se reporta (cve_periodo), validar que los créditos
-- dentro de este supuesto NO formen parte de este reporte.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCreditoCNBV,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0485_INC
WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),8,6) = @FechaPeriodo;

END
GO
