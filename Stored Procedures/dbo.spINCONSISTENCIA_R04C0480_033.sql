SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_033]
AS

BEGIN

-- Si la Rotación de Capital de Trabajo (dat_rotac_capital_trabajo) es > 0 y <= 3.94 ,
-- entonces el Puntaje Asignado por Rotación de Capital de Trabajo (cve_ptaje_rotac_capit_trabajo) debe ser = 66

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RotCapTrabajo,
	P_RotCapTrabajo AS Puntos_RRotCapTrabajo
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(RotCapTrabajo AS DECIMAL(10,6)) > 0 AND CAST(RotCapTrabajo AS DECIMAL(10,6)) <= 3.94 AND ISNULL(P_RotCapTrabajo,'') <> '66';

END


GO
