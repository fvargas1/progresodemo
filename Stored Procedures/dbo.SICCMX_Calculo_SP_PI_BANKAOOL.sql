SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_SP_PI_BANKAOOL]
AS

CREATE TABLE #Temp(
 IdCredito INT,
 IdTipoGarantia INT,
 MontoCubierto DECIMAL(23,2),
 SP DECIMAL(10,6),
 MontoRecuperacion DECIMAL(23,2),
 EsDescubierto BIT
);


INSERT INTO #Temp (IdCredito, IdTipoGarantia, MontoCubierto, SP, MontoRecuperacion, EsDescubierto)
SELECT
 can.IdCredito,
 can.IdTipoGarantia,
 can.MontoCobAjust,
 can.SeveridadCorresp,
 can.MontoRecuperacion,
 0
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE ISNULL(tg.Codigo,'') <> 'NMC-05' AND can.EsDescubierto IS NULL;


-- SE CALCULA LA PARTE EXPUESTA CON LAS GARANTIAS SIN TOMAR EN CUENTA LOS AVALES
INSERT INTO #Temp (IdCredito, IdTipoGarantia, MontoCubierto, SP, MontoRecuperacion, EsDescubierto)
SELECT DISTINCT
 can.IdCredito,
 NULL,
 NULL,
 .45,
 NULL,
 1
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE ISNULL(tg.Codigo,'') <> 'NMC-05' AND can.EsDescubierto IS NULL;


UPDATE temp
SET MontoCubierto = dbo.MAX2VAR(0, crv.EI_Total - t.MontoCubierto),
 MontoRecuperacion = dbo.MAX2VAR(0, crv.EI_Total - t.MontoCubierto) * (1-temp.SP)
FROM #Temp temp
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON temp.IdCredito = crv.IdCredito
INNER JOIN (
 SELECT t.IdCredito, SUM(t.MontoCubierto) AS MontoCubierto
 FROM #Temp t
 GROUP BY t.IdCredito
) AS t ON crv.IdCredito = t.IdCredito
WHERE temp.EsDescubierto = 1;


-- CALCULA SEVERIDAD PONDERADA
UPDATE can
SET SeveridadCorresp = temp.SP
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN (
 SELECT t.IdCredito, CASE WHEN crv.EI_Total > 0 THEN dbo.MAX2VAR(0, 1 - (CAST(SUM(ISNULL(t.MontoRecuperacion,0)) AS DECIMAL(23,2)) / crv.EI_Total)) ELSE 0 END AS SP
 FROM #Temp t
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON t.IdCredito = crv.IdCredito
 GROUP BY t.IdCredito, crv.EI_Total
) AS temp ON can.IdCredito = temp.IdCredito
WHERE ISNULL(can.EsDescubierto, 0) = 1;


UPDATE crv
SET SP_Expuesta = temp.SP
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN (
 SELECT t.IdCredito, CASE WHEN crv2.EI_Total > 0 THEN dbo.MAX2VAR(0, 1 - (CAST(SUM(ISNULL(t.MontoRecuperacion,0)) AS DECIMAL(23,2)) / crv2.EI_Total)) ELSE 0 END AS SP
 FROM #Temp t
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv2 ON t.IdCredito = crv2.IdCredito
 GROUP BY t.IdCredito, crv2.EI_Total
) AS temp ON crv.IdCredito = temp.IdCredito;


-- INSERTAMOS LOG
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 can.IdCredito,
 GETDATE(),
 '',
 'Se actualiza SP Expuesta a ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), temp.SP * 100)) + '%'
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN (
 SELECT t.IdCredito, CASE WHEN crv.EI_Total > 0 THEN dbo.MAX2VAR(0, 1 - (CAST(SUM(ISNULL(t.MontoRecuperacion,0)) AS DECIMAL(23,2)) / crv.EI_Total)) ELSE 0 END AS SP
 FROM #Temp t
 INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON t.IdCredito = crv.IdCredito
 GROUP BY t.IdCredito, crv.EI_Total
) AS temp ON can.IdCredito = temp.IdCredito
WHERE ISNULL(can.EsDescubierto, 0) = 1;


-- ELIMINIAMOS LAS GARANTIAS YA QUE SOLO LAS USAMOS PARA EL CALCULO DE LA SEVERIDAD PONDERADA
DELETE can
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN #Temp tmp ON can.IdCredito = tmp.IdCredito 
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia
WHERE ISNULL(tg.Codigo,'') <> 'NMC-05' AND can.EsDescubierto IS NULL;


UPDATE can
SET PrctCobSinAju = 1 - ISNULL(canFondo.PrctCobSinAju, 0),
 PrctCobAjust = 1 - ISNULL(canFondo.PrctCobAjust, 0),
 Reserva = (1 - ISNULL(canFondo.PrctCobAjust, 0)) * crv.EI_Total * ppi.[PI] * crv.SP_Expuesta,
 MontoCobAjust = dbo.MAX2VAR(ISNULL(crv.EI_Total, 0) - ISNULL((canFondo.PrctCobAjust * crv.EI_Total), 0), 0),
 MontoRecuperacion = dbo.MAX2VAR(ISNULL(crv.EI_Total, 0) - ISNULL((canFondo.PrctCobAjust * crv.EI_Total), 0), 0) * (1 - can.SeveridadCorresp)
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_Garantia_Canasta canFondo ON can.IdCredito = canFondo.IdCredito AND ISNULL(canFondo.EsDescubierto,0) = 0
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON can.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Credito cre ON can.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cre.IdPersona = ppi.IdPersona
INNER JOIN #Temp temp ON can.IdCredito = temp.IdCredito
WHERE can.EsDescubierto = 1;


UPDATE crv
SET EI_Expuesta_GarPer = can.PrctCobAjust * crv.EI_Total,
 EI_Expuesta = can.PrctCobAjust * crv.EI_Total,
 EI_Cubierta_GarPer = crv.EI_Total - (can.PrctCobAjust * crv.EI_Total),
 EI_Cubierta = crv.EI_Total - (can.PrctCobAjust * crv.EI_Total)
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Garantia_Canasta can ON crv.IdCredito = can.IdCredito AND ISNULL(can.EsDescubierto,0) = 1
INNER JOIN #Temp temp ON crv.IdCredito = temp.IdCredito;


UPDATE crv
SET ReservaCubierta = canCub.Reserva,
 ReservaExpuesta = canExp.Reserva,
 ReservaFinal = canCub.Reserva + canExp.Reserva
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Garantia_Canasta canCub ON crv.IdCredito = canCub.IdCredito AND ISNULL(canCub.EsDescubierto,0)=0
INNER JOIN dbo.SICCMX_Garantia_Canasta canExp ON crv.IdCredito = canExp.IdCredito AND ISNULL(canExp.EsDescubierto,0)=1
INNER JOIN #Temp temp ON crv.IdCredito = temp.IdCredito;


UPDATE crv
SET PorReservaFinal = ReservaFinal / EI_Total,
 CalifFinal = cal.IdCalificacion
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN #Temp temp ON crv.IdCredito = temp.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON (crv.ReservaFinal / crv.EI_Total) BETWEEN cal.RangoMenor AND cal.RangoMayor
WHERE temp.EsDescubierto = 1;


UPDATE crv
SET SP_Total = CASE WHEN EI_Total = 0 THEN 0 ELSE (CAST(SP_Expuesta * EI_Expuesta_GarPer AS DECIMAL(22,10)) / EI_Total) + (CAST(SP_Cubierta * EI_Cubierta_GarPer AS DECIMAL(22,10)) / EI_Total) END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN #Temp tmp ON crv.IdCredito = tmp.IdCredito;

UPDATE crv
SET PI_Total = CASE WHEN (EI_Total * SP_Total) = 0 THEN PI_Expuesta ELSE ReservaFinal / CAST(EI_Total * SP_Total AS DECIMAL(25,4)) END
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN #Temp tmp ON crv.IdCredito = tmp.IdCredito;


DROP TABLE #Temp
GO
