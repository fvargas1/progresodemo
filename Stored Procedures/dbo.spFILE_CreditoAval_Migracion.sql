SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoAval_Migracion]
AS

UPDATE rel
SET Porcentaje = NULLIF(f.Porcentaje,''),
	PorcentajeOriginal = NULLIF(f.Porcentaje,'')
FROM dbo.SICCMX_CreditoAval rel
INNER JOIN dbo.SICCMX_Credito cre ON rel.IdCredito = cre.IdCredito 
INNER JOIN dbo.SICCMX_Aval aval ON rel.IdAval = rel.IdAval 
INNER JOIN dbo.FILE_CreditoAval f ON cre.Codigo = f.CodigoCredito AND aval.Codigo = f.CodigoAval
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


INSERT INTO dbo.SICCMX_CreditoAval(
 IdCredito,
 IdAval,
 Porcentaje,
 PorcentajeOriginal
)
SELECT
 (SELECT IdCredito FROM dbo.SICCMX_Credito WHERE Codigo = LTRIM(f.CodigoCredito)),
 (SELECT IdAval FROM dbo.SICCMX_Aval WHERE Codigo = LTRIM(f.CodigoAval)),
 NULLIF(f.Porcentaje,''),
 NULLIF(f.Porcentaje,'')
FROM dbo.FILE_CreditoAval f
WHERE f.CodigoAval+'-'+f.CodigoCredito
NOT IN (
 SELECT a.Codigo+'-'+cred.Codigo
 FROM dbo.SICCMX_CreditoAval ca
 INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
 INNER JOIN dbo.SICCMX_Credito cred ON cred.IdCredito=ca.IdCredito
) AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO
