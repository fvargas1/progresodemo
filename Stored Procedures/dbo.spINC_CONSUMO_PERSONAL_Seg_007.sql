SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_PERSONAL_Seg_007]
AS

BEGIN

-- Se debe reportar la clave (folio) de consulta a la Sociedad de Información Crediticia, que se realizó como parte del proceso de otorgamiento del crédito

SELECT DISTINCT
	rep.FolioCredito,
	rep.ClaveConsultaSociedadInformacionCrediticia
FROM dbo.RW_Consumo_PERSONAL rep
WHERE LEN(ISNULL(rep.ClaveConsultaSociedadInformacionCrediticia,'')) = 0;

END
GO
