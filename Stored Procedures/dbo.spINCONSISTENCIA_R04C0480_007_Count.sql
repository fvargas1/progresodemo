SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_007_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Número de Instituciones Reportadas en los últimos 12 meses (dat_num_instit_reportadas) es = 0,
-- entonces el Puntaje Asignado por Número de Instituciones Reportadas en los últimos 12 meses
-- (cve_ptaje_num_instit_report) debe ser = 79

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(NumInstRep AS DECIMAL(10,6)) = 0 AND ISNULL(P_NumInstRep,'') <> '79';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
