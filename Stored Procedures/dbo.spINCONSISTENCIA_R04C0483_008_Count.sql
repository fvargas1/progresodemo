SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_008_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que la Clave de Estado (cve_estado) se encuentre dentro los valores de 1 a 32.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC
WHERE CAST(ISNULL(Estado,'0') AS INT) NOT BETWEEN 1 AND 32;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
