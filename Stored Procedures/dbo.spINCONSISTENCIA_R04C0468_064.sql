SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_064]
AS

BEGIN

-- Validar que venga el Nombre del Acreditado (dat_nombre) acepte valores alfanuméricos y los siguientes caracteres especiales: "_", "/" o ".".

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor
FROM dbo.RW_VW_R04C0468_INC
WHERE NombrePersona LIKE '%[^a-z,.0-9 _/]%' OR NombrePersona LIKE '%[áéíóú]%';

END


GO
