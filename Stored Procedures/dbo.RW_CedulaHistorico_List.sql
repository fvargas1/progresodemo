SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_CedulaHistorico_List]
AS
SELECT
 vw.IdCedula,
 vw.Nombre,
 vw.Codename,
 vw.ReportFolder,
 vw.Reportname,
 vw.Parameters,
 vw.Prefix,
 vw.Activo,
 vw.OutputParams
FROM dbo.RW_VW_CedulaHistorico vw
WHERE Activo=1
ORDER BY vw.Nombre ASC;
GO
