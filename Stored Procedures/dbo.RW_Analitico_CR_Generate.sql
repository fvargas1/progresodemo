SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_CR_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INTERNO' AND Nombre = '_Analitico_CR';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Analitico_CR;

INSERT INTO dbo.RW_Analitico_CR (
 IdReporteLog, CodigoPersona, CodigoCredito, SaldoTotal, [PI], SP, EI, Reserva, PrctReserva, GradoRiesgo, B0, B1, ACT, B2, HIST, B3,
 PrctUSO, B4, PrctPago, B5, Alto, B6, Medio, B7, Bajo, B8, GVeces1, B9, GVeces2, B10, GVeces3, B11, BKATR, LimiteCredito, Tipo_EX, Zi,
 MontoGarantia
)
SELECT
 @IdReporteLog,
 per.Codigo AS CodigoPersona,
 con.Codigo AS CodigoCredito,
 con.SaldoTotalValorizado AS SaldoTotal,
 crv.[PI] AS [PI],
 crv.SPExpuesta AS SP,
 CAST(crv.E AS DECIMAL(23,2)) AS EI,
 CAST(crv.ReservaExpuesta AS DECIMAL(23,2)) AS Reserva,
 crv.PorReservaExpuesta AS PrctReserva,
 cal.Codigo AS GradoRiesgo,
 ct.Constante AS B0,
 ct.ATR AS B1,
 CAST(pre.NumeroImpagosConsecutivos AS DECIMAL(10,2)) AS ACT,
 ct.MAXATR AS B2,
 CAST(pre.NumeroImpagosHistoricos AS DECIMAL(10,2)) AS HIST,
 ct.ProPR AS B3,
 pre.PorcentajeUso AS PrctUSO,
 ct.ProPago AS B4,
 pre.PorPago AS PrctPago,
 ct.Alto AS B5,
 pre.Alto AS Alto,
 ct.Medio AS B6,
 pre.Medio AS Medio,
 ct.Bajo AS B7,
 pre.Bajo AS Bajo,
 ct.GVeces1 AS B8,
 info.GVeces1 AS GVeces1,
 ct.GVeces2 AS B9,
 info.GVeces2 AS GVeces2,
 ct.GVeces3 AS B10,
 info.GVeces3 AS GVeces3,
 ct.INDATR AS B11,
 CAST(info.Meses_desde_ult_atr_bk AS DECIMAL(10,2)) AS BKATR,
 CAST(pre.LimiteCredito AS DECIMAL(23,2)) AS LimiteCredito,
 crv.Tipo_EX,
 pre.Zi,
 CAST(ISNULL(gar.Monto,0) AS DECIMAL(23,2))
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON crv.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo AND met.Codigo = '5'
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON met.IdMetodologiaConsumo = ct.IdMetodologia
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON crv.IdCalificacionExpuesta = cal.IdCalificacion
LEFT OUTER JOIN (
	SELECT IdConsumo, SUM(MontoUsadoGarantia) AS Monto
	FROM dbo.SICCMX_ConsumoGarantia
	GROUP BY IdConsumo
) AS gar ON con.IdConsumo = gar.IdConsumo;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_CR WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
