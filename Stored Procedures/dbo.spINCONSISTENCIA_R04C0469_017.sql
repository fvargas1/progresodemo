SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_017]
AS

BEGIN

-- El Monto Dispuesto de la Línea de Crédito en el mes debe ser igual a 0.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion,
	@FechaPeriodo AS FechaPeriodo,
	MontoDispuesto
FROM dbo.RW_VW_R04C0469_INC
WHERE ISNULL(FechaDisposicion,'') < @FechaPeriodo AND CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL) <> 0;

END


GO
