SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_PERSONAL_Seg_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Tipo de Reestructura de ser 50 en el caso de créditos originales (que no provienen de una reestructura);
-- 500 = en el caso de créditos Personales que fueron objeto de una reestructura y continúan siendo Personales;
-- 510 en el caso de créditos ABCD que fueron objeto de una reestructura y pasaron a ser créditos Personales;
-- 520 en el caso de créditos de Auto que fueron objeto de una reestructura y pasaron a ser créditos Personales;
-- 530 en el caso de créditos de nómina que fueron objeto de una reestructura y pasaron a ser créditos Personales;
-- 540 en el caso de créditos Grupales que fueron objeto de una reestructura y pasaron a ser créditos Personales;
-- 560 en el caso de créditos de Tarjetas de Crédito que fueron objeto de una reestructura y pasaron a ser créditos Personales,
-- y 580 en el caso de créditos Otros que fueron objeto de una reestructura y pasaron a ser Personales.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_PERSONAL rep
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo cat ON rep.Reestructura = cat.CodigoBanxico AND ISNULL(cat.ClasificacionReporte,'PERSONAL') = 'PERSONAL'
WHERE cat.IdReestructuraConsumo IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
