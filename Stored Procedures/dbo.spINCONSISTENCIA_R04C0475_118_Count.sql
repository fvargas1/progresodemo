SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_118_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El PORCENTAJE DE PAGOS A INSTITUCIONES BANCARIAS CON 90 O MÁS DÍAS DE ATRASO EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE LEN(PorcPagoInstBanc90) > 0 AND PorcPagoInstBanc90 <> '-999'
	AND (CHARINDEX('.',PorcPagoInstBanc90) = 0 OR CHARINDEX('.',PorcPagoInstBanc90) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc90, CHARINDEX('.', PorcPagoInstBanc90) + 1, LEN(PorcPagoInstBanc90)))) <> 6);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
