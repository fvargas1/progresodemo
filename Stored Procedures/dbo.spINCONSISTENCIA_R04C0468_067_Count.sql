SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_067_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Tipo de Alta corresponda a Catalogo de CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_TipoAlta alta ON ISNULL(rep.TipoAltaCredito,'') = alta.CodigoCNBV
WHERE alta.IdTipoAlta IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
