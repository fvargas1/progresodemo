SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_024]
AS

BEGIN

-- Si el RFC (dat_rfc) inicia con CNBF, se valida que sí corresponda a un Fideicomiso y haya sido registrado en CNBV.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(RFC,'') LIKE 'CNBF%' AND ISNULL(NombrePersona,'') NOT LIKE 'FIDEICOMISO%';

END


GO
