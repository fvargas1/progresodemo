SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_126_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Monto del Crédito Simple o Monto Autorizado de la Línea de Crédito sin Incluir Accesorios (dat_monto_credito_simple),
-- debe ser menor o igual al Monto de la Línea de Crédito Autorizado (dat_monto_linea_credito_autori).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(NULLIF(MontoSinAccesorios,''),0) > ISNULL(NULLIF(MonLineaCred,''),0)

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
