SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_CodigoCredito_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Movimientos_CodigoCredito_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Movimientos f
LEFT OUTER JOIN (
	SELECT Codigo FROM dbo.SICCMX_Credito
	UNION ALL
	SELECT Codigo FROM dbo.SICCMX_Consumo
	UNION ALL
	SELECT Codigo FROM dbo.SICCMX_Hipotecario
) cat ON LTRIM(f.CodigoCredito) = cat.Codigo
WHERE cat.Codigo IS NULL AND LEN(f.CodigoCredito) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Movimientos_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoCredito,
	'CodigoCredito',
	f.CodigoCredito,
	2,
	@Detalle
FROM dbo.FILE_Movimientos f
LEFT OUTER JOIN (
	SELECT Codigo FROM dbo.SICCMX_Credito
	UNION ALL
	SELECT Codigo FROM dbo.SICCMX_Consumo
	UNION ALL
	SELECT Codigo FROM dbo.SICCMX_Hipotecario
) cat ON LTRIM(f.CodigoCredito) = cat.Codigo
WHERE cat.Codigo IS NULL AND LEN(f.CodigoCredito) > 0;
GO
