SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_072_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Monto de la Comisión por Apertura del Crédito (dat_comisiones_apertura_cred) no sea mayor al 15% del Monto Autorizado de la Línea de Crédito (dat_monto_credito_linea_aut).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0473_INC
WHERE CAST(ISNULL(ComisionAperturaMonto,'0') AS DECIMAL) > CAST(ISNULL(MonLineaCred,'0') AS DECIMAL) * 0.15;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
