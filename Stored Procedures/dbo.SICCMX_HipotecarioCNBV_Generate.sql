SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioCNBV_Generate]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

INSERT INTO dbo.SICCMX_HipotecarioCNBV (
	Codigo,
	CNBV,
	FechaCreacion,
	IdPeriodo
)
SELECT
	hip.Codigo,
	'1' + LEFT(CONVERT(VARCHAR, info.FechaOtorgamiento, 112),6) + RIGHT(info.NumeroAvaluo,17),
	GETDATE(),
	@IdPeriodo
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_HipotecarioCNBV cnbv ON hip.Codigo = cnbv.Codigo
WHERE cnbv.IdHipotecarioCNBV IS NULL;
GO
