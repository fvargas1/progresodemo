SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_038]
AS

BEGIN

-- Si el Puntaje Asignado por la Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) es = 45,
-- entonces la Tasa de Retención Laboral (dat_tasa_retencion_laboral) debe ser >= 34 y < 58.16

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	TasaRetLab,
	P_TasaRetLab AS Puntos_TasaRetLab
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_TasaRetLab,'') = '45' AND (CAST(TasaRetLab AS DECIMAL(10,6)) < 34 OR CAST(TasaRetLab AS DECIMAL(10,6)) >= 58.16);

END

GO
