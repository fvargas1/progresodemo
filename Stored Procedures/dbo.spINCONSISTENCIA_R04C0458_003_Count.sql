SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_003_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Tipo de Alta (cve_tipo_alta_credito) es <> 135, validar que la Clave de la Institución contenida
-- en el ID metodología CNBV (dat_id_credito_met_cnbv) en las posiciones 2 a la 7, sea IGUAL a la Clave de 
-- Institución que se reporta (cve_institucion).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @IdInstitucion INT;

SELECT @IdInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','136','137','139','700','701','702','731','732','733','741','742','743','751') AND SUBSTRING(CodigoCreditoCNBV,2,6) <> @IdInstitucion;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
