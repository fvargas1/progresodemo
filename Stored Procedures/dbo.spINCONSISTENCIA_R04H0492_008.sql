SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_008]
AS

BEGIN

-- La tasa de interés debe ser estrictamente mayor a cero.

SELECT CodigoCredito, CodigoCreditoCNBV, TasaInteres
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(TasaInteres,''),'0') AS DECIMAL(8,2)) <= 0;

END
GO
