SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_093]
AS

BEGIN

-- Validar que el Número de Día Utilizados para el Cálculo del Intereses en el periodo reportado sea MAYOR O IGUAL a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	DiasCalculoInteres
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(NULLIF(DiasCalculoInteres,''),'-1') AS DECIMAL) < 0

END


GO
