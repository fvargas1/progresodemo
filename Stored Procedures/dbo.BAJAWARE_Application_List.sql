SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Application_List]
AS
SELECT
IdApplication, Active, Description, Name, CodeName, Folder, HomePage, SortOrder, StringKey, Version
FROM dbo.BAJAWARE_Application
WHERE Active=1;
GO
