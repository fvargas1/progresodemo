SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0485_014_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Cuando la Fecha de Otorgamiento contenida en el ID metodología CNBV (dat_id_credito_met_cnbv)
-- posiciones 8 a la 13 sea IGUAL al periodo que se reporta (cve_periodo), validar que los créditos
-- dentro de este supuesto NO formen parte de este reporte.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0485_INC
WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),8,6) = @FechaPeriodo;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
