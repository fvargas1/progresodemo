SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Credito_Reservas_Variables_Log_Select]
	@IdCredito BIGINT
AS
SELECT
 IdCredito,
 Fecha,
 Usuario,
 Comentarios
FROM dbo.SICCMX_Credito_Reservas_Variables_Log
WHERE IdCredito = @IdCredito
ORDER BY Fecha
GO
