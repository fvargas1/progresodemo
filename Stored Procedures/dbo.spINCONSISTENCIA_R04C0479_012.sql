SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_012]
AS

BEGIN

-- El campo RESERV TOTAL METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaTotalInterna
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(ReservaTotalInterna,''),'-1') AS DECIMAL) < 0;

END
GO
