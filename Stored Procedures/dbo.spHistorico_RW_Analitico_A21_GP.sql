SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Analitico_A21_GP]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Analitico_A21_GP);

DELETE FROM Historico.RW_Analitico_A21_GP WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Analitico_A21_GP (
 IdPeriodoHistorico, Codigo, Nombre, TipoGarantia, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI], AntSocInfCred_V, AntSocInfCred_P, QuitasCastReest_V,
 QuitasCastReest_P, PrctPagoInstNoBanc_V, PrctPagoInstNoBanc_P, PrctPagosEntComer_V, PrctPagosEntComer_P, CuentasCredAbiertosInstFin_V,
 CuentasCredAbiertosInstFin_P, MontoMaxInstFin_V, MontoMaxInstFin_P, MesesUltCredAbierto_V, MesesUltCredAbierto_P, PrctPagoInstFinBanc_V,
 PrctPagoInstFinBanc_P, PrctPagoInstFin29Atr_V, PrctPagoInstFin29Atr_P, PrctPagoInstFin90Atr_V, PrctPagoInstFin90Atr_P, DiasMoraInstFinBanc_V,
 DiasMoraInstFinBanc_P, PagosInstFinBanc_V, PagosInstFinBanc_P, AportInfonavit_V, AportInfonavit_P, DiasAtrInfonavit_V, DiasAtrInfonavit_P,
 TasaRetLab_V, TasaRetLab_P, IndPerMoralFid_V, IndPerMoralFid_P, ProcOrigAdmon_V, ProcOrigAdmon_P, SinAtrasos
)
SELECT
 @IdPeriodoHistorico,
 Codigo,
 Nombre,
 TipoGarantia,
 PonderadoCuantitativo,
 PonderadoCualitativo,
 FactorTotal,
 [PI],
 AntSocInfCred_V,
 AntSocInfCred_P,
 QuitasCastReest_V,
 QuitasCastReest_P,
 PrctPagoInstNoBanc_V,
 PrctPagoInstNoBanc_P,
 PrctPagosEntComer_V,
 PrctPagosEntComer_P,
 CuentasCredAbiertosInstFin_V,
 CuentasCredAbiertosInstFin_P,
 MontoMaxInstFin_V,
 MontoMaxInstFin_P,
 MesesUltCredAbierto_V,
 MesesUltCredAbierto_P,
 PrctPagoInstFinBanc_V,
 PrctPagoInstFinBanc_P,
 PrctPagoInstFin29Atr_V,
 PrctPagoInstFin29Atr_P,
 PrctPagoInstFin90Atr_V,
 PrctPagoInstFin90Atr_P,
 DiasMoraInstFinBanc_V,
 DiasMoraInstFinBanc_P,
 PagosInstFinBanc_V,
 PagosInstFinBanc_P,
 AportInfonavit_V,
 AportInfonavit_P,
 DiasAtrInfonavit_V,
 DiasAtrInfonavit_P,
 TasaRetLab_V,
 TasaRetLab_P,
 IndPerMoralFid_V,
 IndPerMoralFid_P,
 ProcOrigAdmon_V,
 ProcOrigAdmon_P,
 SinAtrasos
FROM dbo.RW_Analitico_A21_GP
WHERE IdReporteLog = @IdReporteLog;
GO
