SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INTERNO_PI_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Codigo,
	Nombre,
	Metodologia,
	Clasificacion,
	FactorCuantitativo,
	FactorCualitativo,
	Factor_Alpha,
	Factor_1mAlpha,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI]
FROM dbo.RW_INTERNO_PI
WHERE IdReporteLog = @IdReporteLog
ORDER BY Metodologia, Nombre;
GO
