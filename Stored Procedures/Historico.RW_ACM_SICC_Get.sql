SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_ACM_SICC_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Periodo_Actual,
	Num_Credito,
	Tipo_Cred_R04A,
	Fecha_Ven,
	Sector_Lab,
	Situacion_Cred,
	Interes_Vig,
	Interes_Ven,
	Saldo_Total,
	Moneda
FROM Historico.RW_ACM_SICC
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;

GO
