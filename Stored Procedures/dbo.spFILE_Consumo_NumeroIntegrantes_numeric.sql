SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_NumeroIntegrantes_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_NumeroIntegrantes_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Consumo
SET errorFormato = 1
WHERE LEN(NumeroIntegrantes)>0 AND (ISNUMERIC(ISNULL(NumeroIntegrantes,'0')) = 0 OR NumeroIntegrantes LIKE '%[^0-9 ]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'NumeroIntegrantes',
 NumeroIntegrantes,
 1,
 @Detalle
FROM dbo.FILE_Consumo
WHERE LEN(NumeroIntegrantes)>0 AND (ISNUMERIC(ISNULL(NumeroIntegrantes,'0')) = 0 OR NumeroIntegrantes LIKE '%[^0-9 ]%');
GO
