SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_019]
AS

BEGIN

-- El número de garantías reales debe ser mayor o igual a cero

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	NumeroGarRealFin
FROM dbo.RW_VW_R04C0481_INC
WHERE CAST(ISNULL(NULLIF(NumeroGarRealFin,''),'-1') AS INT) < 0;

END


GO
