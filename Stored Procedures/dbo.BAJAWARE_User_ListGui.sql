SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_ListGui]
AS
SELECT
	IdUser,
	Active,
	Description,
	Blocked,
	[Name],
	Email,
	Telefono,
	Mobil,
	NumeroControl,
	Username,
	UsuarioWindows,
	UltimaFechaLogin,
	FechaNacimiento,
	NeverLogedIn
FROM dbo.BAJAWARE_User
WHERE Active=1;
GO
