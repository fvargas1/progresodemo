SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_028]
AS

BEGIN

-- Se deberá anotar el monto de las comisiones devengadas. En caso de que no haya monto de comisiones devengadas se anotará 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, MontoComisionDevengada
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoComisionDevengada,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
