SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_022]
AS

BEGIN

-- La clave de Denominación del Crédito Original es igual a cero (0) si y solo si "Tipo de Alta del Crédito" es igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, DenominacionCredito, TipoAlta
FROM dbo.RW_R04H0491
WHERE DenominacionCredito = '0' AND TipoAlta <> '3';

END
GO
