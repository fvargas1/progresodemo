SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_035_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que los Intereses Resultantes del Aplicar la Tasa al Saldo Base (dat_monto_interes) sea MAYOR O IGUAL a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC
WHERE CAST(ISNULL(NULLIF(MontoInteresAplicar,''),'0') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
