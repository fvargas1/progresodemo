SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_CRNuevosPorProducto]
	@IdPeriodoHistorico BIGINT,
	@IdReporteLog BIGINT
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

CREATE TABLE #TEMP_Monto (
	Calificacion VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)

CREATE TABLE #TEMP_Tempo (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Calificacion VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)

CREATE TABLE #TEMP_Anterior (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
)

CREATE TABLE #TEMP_Diff (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)


INSERT INTO #TEMP_Anterior
SELECT
	ha.Codigo
FROM Historico.SICCMX_Credito ha
WHERE ha.IdPeriodoHistorico = @IdPeriodoHistorico
AND ha.Metodologia IN ('20','21','22')
AND (ha.ClasificacionContable IS NULL OR ha.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'));


--INSERTO PARTE EXPUESTA          
INSERT INTO #TEMP_Tempo
SELECT
	c.Codigo,
	calif.Codigo,
	ISNULL(cr.EI_Total,0) AS Monto
FROM dbo.SICCMX_Credito_Reservas_Variables cr
INNER JOIN SICCMX_Credito c ON cr.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON c.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICC_CalificacionNvaMet calif ON cr.CalifFinal = calif.IdCalificacion
LEFT OUTER JOIN SICC_Clasificacioncontable clas on c.IdClasificacionContable = clas.IdClasificacionContable
WHERE met.Codigo IN ('20','21','22') AND cr.EI_Total > 0
AND (clas.Codigo IS NULL OR clas.Codigo NOT IN ('816000', '139151010400', '139151010500'));

INSERT INTO #TEMP_Monto
SELECT
	t.Calificacion,
	SUM(ISNULL(t.Monto,0)) AS Monto
FROM #TEMP_Tempo t
LEFT OUTER JOIN #TEMP_Anterior ta ON t.Credito = ta.Credito
WHERE ta.Credito IS NULL
GROUP BY Calificacion;


TRUNCATE TABLE #TEMP_Tempo;

--INSERTO LOS CREDITOS QUE EL MONTO DEL PERIODO ANTERIOR ES < AL PERIODO ACTUAL, NUEVOS PARCIAL
INSERT INTO #TEMP_Diff
SELECT
	c.Codigo,
	SUM(ISNULL(hact.EI_Total,0)) - SUM(ISNULL(hant.EI_Total,0)) AS Monto
FROM dbo.SICCMX_Credito_Reservas_Variables hact
INNER JOIN dbo.SICCMX_Credito c ON hact.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON c.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clas ON c.IdClasificacionContable = clas.IdClasificacionContable
INNER JOIN Historico.SICCMX_Credito_Reservas_Variables hant ON hant.Credito = c.Codigo
INNER JOIN Historico.SICCMX_Credito hantCredito ON hant.Credito = hantCredito.Codigo AND hant.IdPeriodoHistorico = hantCredito.IdPeriodoHistorico
WHERE met.Codigo IN ('20','21','22') AND hantCredito.Metodologia IN ('20','21','22')
AND hant.IdPeriodoHistorico = @IdPeriodoHistorico
AND (clas.Codigo IS NULL OR clas.Codigo NOT IN ('816000', '139151010400', '139151010500'))
AND (hantCredito.ClasificacionContable IS NULL OR hantCredito.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
GROUP BY c.Codigo
HAVING (SUM(ISNULL(hact.EI_Total,0))-SUM(ISNULL(hant.EI_Total,0))) > 0;

INSERT INTO #TEMP_Monto
SELECT
	(SELECT MAX(calif.Codigo)
	FROM dbo.SICCMX_Credito_Reservas_Variables vars
	INNER JOIN dbo.SICCMX_Credito c ON vars.IdCredito = c.IdCredito
	INNER JOIN dbo.SICC_CalificacionNvaMet calif ON calif.IdCalificacion = vars.CalifFinal
	WHERE c.Codigo = t.Credito),
	ISNULL(t.Monto,0)
FROM #TEMP_Diff t;


SELECT
	@IdReporteLog,
	con.Codigo AS Calificacion,
	'0451',
	'1', -- Moneda
	'1', -- Metodo calificacion
	'2', -- Calificaciones de riesgo por operacion
	'118', -- Nuevas calificaciones de riesgo
	CAST(SUM(ISNULL(tm.Monto,0)) AS DECIMAL) AS Monto
FROM dbo.SICC_CalificacionNvaMet cal
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = cal.Codigo
LEFT OUTER JOIN #TEMP_Monto tm ON cal.Codigo = tm.Calificacion
WHERE cal.Codigo <> 'NA' AND cal.Codigo <> 'SC'
GROUP BY con.Codigo;


DROP TABLE #TEMP_Monto;
DROP TABLE #TEMP_Anterior;
DROP TABLE #TEMP_Tempo;
DROP TABLE #TEMP_Diff;
GO
