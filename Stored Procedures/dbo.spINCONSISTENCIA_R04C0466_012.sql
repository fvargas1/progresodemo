SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_012]
AS

BEGIN

-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas.

SELECT
 rep.CodigoCredito,
 rep.NumeroDisposicion,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
 rep.CodigoCreditoCNBV
FROM dbo.RW_R04C0466 rep
LEFT OUTER JOIN dbo.RW_R04C0463 r63 ON rep.CodigoCreditoCNBV = r63.CodigoCreditoCNBV
LEFT OUTER JOIN Historico.RW_R04C0463 hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE hist.IdPeriodoHistorico IS NULL AND r63.CodigoCreditoCNBV IS NULL;

END

GO
