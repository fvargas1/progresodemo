SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Preliminares_Hipotecario]
AS
DECLARE @IdMoneda BIGINT;
SELECT @IdMoneda = IdMoneda FROM dbo.SICC_Moneda WHERE Codigo = '0';

-- INICIALIZAMOS LAS VARIABLES CON LOS VALORES RECIBIDOS EN LA MIGRACION
UPDATE pre
SET ATR = info.ATR,
 MaxATR = info.MaxATR,
 VPago = info.VPago,
 PorPago = info.PorPago,
 PorCLTVi = info.PorCLTVi,
 PPagoIM = info.PPagoIM,
 TRi = info.TRi,
 MON = info.MON
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON pre.IdHipotecario = info.IdHipotecario;

-- CALCULAMOS VARIABLES SIN INFORMACION
UPDATE pre
SET
 ATR = ISNULL(pre.ATR, CEILING(pre.DiasAtraso / 30.40)),
 VPago = ISNULL(pre.VPago, CASE WHEN hInfo.MontoExigible = 0 THEN 0 ELSE hInfo.MontoPagadoCliente / hInfo.MontoExigible END),
 PorCLTVi = ISNULL(pre.PorCLTVi, CASE WHEN pre.ValorVivienda = 0 THEN 0 ELSE (hip.SaldoTotalValorizado) / pre.ValorVivienda END),
 MON = ISNULL(pre.MON, CASE WHEN hInfo.IdMoneda = @IdMoneda THEN 0 ELSE 1 END)
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON pre.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioInfo hInfo ON hip.IdHipotecario = hInfo.IdHipotecario;


UPDATE pre
SET
 MaxATR = ISNULL(pre.MaxATR, CASE WHEN met.Codigo <> '4' THEN vw.MAXATR ELSE vw.MAXATR_REA_PRO END),
 PorPago = ISNULL(pre.PorPago, vw.PrctPagoEx),
 PPagoIM = ISNULL(pre.PPagoIM, vw.PrctPagoIM)
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_VW_Historico_Hipotecario vw ON pre.IdHipotecario = vw.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario;
GO
