SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_033_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- Se verificará que la responsabilidad total no sea mayor que la línea de crédito autorizada reportada en el formulario de Altas.



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(DISTINCT r.CodigoCredito)

FROM dbo.RW_R04C0443 r

INNER JOIN dbo.SICCMX_VW_Datos_0442 cat ON ISNULL(r.CodigoCreditoCNBV,'') = cat.CodigoCreditoCNBV

WHERE CAST(ISNULL(NULLIF(r.SaldoFinal,''),'-1') AS DECIMAL(23,2)) > CAST(ISNULL(NULLIF(cat.MontoLineaAutorizado,''),'-1') AS DECIMAL(23,2));



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
