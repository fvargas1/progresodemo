SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ConsumoReservasVariables_Select]
	@IdConsumo BIGINT
AS
SELECT
	vw.IdConsumo,
	vw.IdConsumoNombre,
	vw.Codigo,
	vw.[PI],
	vw.E,
	vw.MontoGarantia,
	vw.ReservaTotal,
	vw.PorReserva,
	vw.IdCalificacion,
	vw.IdCalificacionNombre,
	vw.ECubierta,
	vw.EExpuesta,
	vw.SPCubierta,
	vw.SPExpuesta,
	vw.ReservaCubierta,
	vw.ReservaExpuesta,
	vw.PorReservaCubierta,
	vw.PorReservaExpuesta,
	vw.IdCalificacionCubierta,
	vw.IdCalificacionCubiertaNombre,
	vw.IdCalificacionExpuesta,
	vw.IdCalificacionExpuestaNombre
FROM dbo.SICCMX_VW_ConsumoReservasVariables vw
WHERE vw.IdConsumo = @IdConsumo;
GO
