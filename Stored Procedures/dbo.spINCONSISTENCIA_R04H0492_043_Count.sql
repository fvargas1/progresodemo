SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_043_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "PROBABILIDAD DE INCUMPLIMIENTO (Metodología Interna)" debe estar expresado a seis decimales.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(PIInterna,CHARINDEX('.',PIInterna)+1,LEN(PIInterna))) <> 6;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
