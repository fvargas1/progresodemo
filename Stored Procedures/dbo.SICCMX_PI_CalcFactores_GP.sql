SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_CalcFactores_GP]
AS

--Actualizamos valores.
UPDATE per
SET
 per.FactorCuantitativo = fact.FAC_CUANTI,
 per.FactorCualitativo = fact.FAC_CUALI
FROM dbo.SICCMX_Persona_PI_GP per
INNER JOIN SICCMX_VW_Persona_PI_Factores_GP fact ON fact.IdGP = per.IdGP AND fact.EsGarante = per.EsGarante;

-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGP,
 EsGarante,
 GETDATE(),
 '',
 'El valor del factor cuantitativo fue de : '+ CAST(CAST(FactorCuantitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI_GP;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdGP,
 EsGarante,
 GETDATE(),
 '',
 'El valor del factor cualitativo fue de : '+ CAST(CAST(FactorCualitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI_GP;


UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 90
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo22_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdGP,
 ppi.EsGarante,
 GETDATE(),
 '',
 'Se suman 90 puntos al factor cuantitativo quedando como resultado '+ CAST(CAST(ppi.FactorCuantitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI_GP ppi
INNER JOIN dbo.SICCMX_Anexo22_GP anx ON ppi.IdGP = anx.IdGP AND ppi.EsGarante = anx.EsGarante;
GO
