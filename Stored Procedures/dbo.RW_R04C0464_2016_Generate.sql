SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0464_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0464_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0464_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0464_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, ClasContable, ConcursoMercantil, FechaDisposicion, FechaVencDisposicion, Moneda,
 NumeroDisposicion, NombreFactorado, RFC_Factorado, SaldoInicial, TasaInteres, TasaInteresDisp, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa,
 MontoDispuesto, MontoPagoExigible, MontoCapitalPagado, MontoInteresPagado, MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado,
 MontoCondonacion, MontoQuitas, MontoBonificado, MontoDescuentos, MontoAumentosDec, SaldoFinal, SaldoCalculoInteres, DiasCalculoInteres,
 MontoInteresAplicar, SaldoInsoluto, SituacionCredito, DiasAtraso, FechaUltimoPago, MontoBancaDesarrollo, InstitucionFondeo
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '464',
 cnbv.CNBV AS CodigoCreditoCNBV,
 cc.Codigo AS ClasContable,
 CASE WHEN cInfo.ConcursoMercantil = 1 THEN '1' ELSE '2' END AS ConcursoMercantil,
 CASE WHEN cre.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaDisposicion,102),'.',''),1,6) END AS FechaDisposicion,
 CASE WHEN cre.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaVencimiento,102),'.',''),1,6) END AS FechaVencDisposicion,
 mon.CodigoCNBVMoneda AS Moneda,
 cre.CodigoCredito AS NumeroDisposicion,
 cInfo.DeudorFactoraje AS NombreFactorado,
 cInfo.DeudorFactorajeRFC AS RFC_Factorado,
 cInfo.SaldoInicial AS SaldoInicial,
 cre.TasaBruta AS TasaInteres,
 tasaRef.CodigoCNBVTasaRef AS TasaInteresDisp,
 tasa.AjusteTasa AS DifTasaRef,
 tasa.OperacionDiferencial AS OperacionDifTasaRef,
 tasa.FrecuenciaRevisionTasa AS FrecuenciaRevisionTasa,
 cInfo.MontoDispuesto AS MontoDispuesto,
 cinfo.MontoPagoExigible AS MontoPagoExigible,
 cInfo.MontoPagEfeCap AS MontoCapitalPagado,
 cInfo.MontoPagEfeInt AS MontoInteresPagado,
 cInfo.MontoPagEfeCom AS MontoComisionPagado,
 cInfo.MontoPagEfeIntMor AS MontoInteresMoratorio,
 cInfo.MontoPagosRealizados AS MontoTotalPagado,
 cInfo.MontoCondonacion AS MontoCondonacion,
 cInfo.MontoQuitasCastQue AS MontoQuitas,
 cInfo.MontoBonificacionDesc AS MontoBonificado,
 cInfo.MontoDescuentos AS MontoDescuentos,
 cInfo.MontoOtrosPrincipal AS MontoAumentosDec,
 cInfo.SaldoFinal AS SaldoFinal,
 cInfo.SaldoParaCalculoInteres AS SaldoCalculoInteres,
 cInfo.DiasCalculoInteres AS DiasCalculoInteres,
 cInfo.InteresDelMes AS MontoInteresAplicar,
 cre.MontoValorizado AS SaldoInsoluto,
 sitCre.Codigo AS SituacionCredito,
 cInfo.DiasMorosidad AS DiasAtraso,
 SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cInfo.FecPagExigibleRealizado,@FechaPeriodo),102),'.',''),1,6) AS FechaUltimoPago,
 cInfo.MontoBancaDesarrollo AS MontoBancaDesarrollo,
 inst.Codigo AS InstitucionFondeo
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pInfo.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Anexo21 anx ON cre.IdPersona = anx.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Moneda_Credito_Rep mon ON cre.IdCredito = mon.IdCredito
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON cre.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Institucion inst ON cInfo.IdInstitucionFondeo = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICC_ClasificacionContable cc ON cre.IdClasificacionContable = cc.IdClasificacionContable
LEFT OUTER JOIN dbo.SICCMX_VW_TasaRef_Credito_Rep tasaRef ON cre.IdCredito = tasaRef.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Credito_AjusteTasa tasa ON cre.IdCredito = tasa.IdCredito
WHERE (met.Codigo='21' AND ISNULL(anx.OrgDescPartidoPolitico,0) = 0) AND (cre.MontoValorizado > 0 OR cInfo.IdTipoBaja IS NOT NULL OR cInfo.SaldoInicial > 0);

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0464_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
