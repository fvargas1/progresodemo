SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_012]
AS

BEGIN

-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas.

SELECT
 rep.CodigoCredito,
 rep.NumeroDisposicion,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
 rep.CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0481_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0478_INC r78 ON rep.CodigoCreditoCNBV = r78.CodigoCreditoCNBV
LEFT OUTER JOIN Historico.RW_VW_R04C0478_INC hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE hist.IdPeriodoHistorico IS NULL AND r78.CodigoCreditoCNBV IS NULL;

END


GO
