SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_099]
AS

BEGIN

-- Validar que si la Situación del Crédito (cve_situacion_credito) es IGUAL a 1, el dato reportado en la columna de Número de días Vencidos (dat_num_vencidos) sea MENOR o IGUAL a 90.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	SituacionCredito,
	DiasAtraso
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(SituacionCredito,'') = '1' AND CAST(ISNULL(NULLIF(DiasAtraso,''),'-1') AS DECIMAL) > 90

END

GO
