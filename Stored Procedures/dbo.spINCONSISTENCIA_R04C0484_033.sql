SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_033]
AS
BEGIN
-- La Tasa de Interés Bruta del Periodo (dat_tasa_bruta_periodo) sea MAYOR o IGUAL a 0.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	TasaInteres
FROM dbo.RW_VW_R04C0484_INC
WHERE CAST(ISNULL(NULLIF(TasaInteres,''),'0') AS DECIMAL(10,6)) < 0;

END
GO
