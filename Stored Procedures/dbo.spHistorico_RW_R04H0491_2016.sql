SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04H0491_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04H0491_2016);

DELETE FROM Historico.RW_R04H0491_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04H0491_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, GeneroAcred, EdadAcred, EdoCivilAcred, NombreAcred,
	RFC_Acred, CURP_Acred, CveUnicaAcred, MunicipioAcred, EstadoAcred, AportaSubcuenta, GeneroCoAcred, EdadCoAcred, EdoCivilCoAcred, NombreCoAcred, RFC_CoAcred,
	CURP_CoAcred, MunicipioCoAcred, EstadoCoAcred, ProductoHipotecario, CategoriaCredito, TipoAlta, DestinoCredito, FechaOtorgamiento, FechaVencimiento,
	DenominacionCredito, MontoOriginal, Comisiones, MontoSubsidioFederal, EntidadCoFin, MontoSubCuenta, MontoOtorgadoCoFin, Apoyo, ValorOriginalVivienda,
	ValorAvaluo, NumeroAvaluo, Localidad, Municipio, Estado, CveRegUnico, SegmentoVivienda, FechaFirmaReestructura, FechaVencimientoReestructura,
	MontoReestructura, DenominacionCreditoReestructura, IngresosMensuales, TipoComprobacionIngresos, SectorLaboral, NumeroConsultaSIC, IngresosMensualesCoAcred,
	TipoComprobacionIngresosCoAcred, SectorLaboralCoAcred, NumeroConsultaSICCoAcred, PeriodicidadAmortizaciones, TipoTasaInteres, TasaRef, AjusteTasaRef,
	SeguroAcreditado, TipoSeguro, EntidadSeguro, PorcentajeCubiertoSeguro, MontoSubCuentaGarantia, ValorGarantias, INTEXP, SDES, PorLTV
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	GeneroAcred,
	EdadAcred,
	EdoCivilAcred,
	NombreAcred,
	RFC_Acred,
	CURP_Acred,
	CveUnicaAcred,
	MunicipioAcred,
	EstadoAcred,
	AportaSubcuenta,
	GeneroCoAcred,
	EdadCoAcred,
	EdoCivilCoAcred,
	NombreCoAcred,
	RFC_CoAcred,
	CURP_CoAcred,
	MunicipioCoAcred,
	EstadoCoAcred,
	ProductoHipotecario,
	CategoriaCredito,
	TipoAlta,
	DestinoCredito,
	FechaOtorgamiento,
	FechaVencimiento,
	DenominacionCredito,
	MontoOriginal,
	Comisiones,
	MontoSubsidioFederal,
	EntidadCoFin,
	MontoSubCuenta,
	MontoOtorgadoCoFin,
	Apoyo,
	ValorOriginalVivienda,
	ValorAvaluo,
	NumeroAvaluo,
	Localidad,
	Municipio,
	Estado,
	CveRegUnico,
	SegmentoVivienda,
	FechaFirmaReestructura,
	FechaVencimientoReestructura,
	MontoReestructura,
	DenominacionCreditoReestructura,
	IngresosMensuales,
	TipoComprobacionIngresos,
	SectorLaboral,
	NumeroConsultaSIC,
	IngresosMensualesCoAcred,
	TipoComprobacionIngresosCoAcred,
	SectorLaboralCoAcred,
	NumeroConsultaSICCoAcred,
	PeriodicidadAmortizaciones,
	TipoTasaInteres,
	TasaRef,
	AjusteTasaRef,
	SeguroAcreditado,
	TipoSeguro,
	EntidadSeguro,
	PorcentajeCubiertoSeguro,
	MontoSubCuentaGarantia,
	ValorGarantias,
	INTEXP,
	SDES,
	PorLTV
FROM dbo.RW_R04H0491_2016
WHERE IdReporteLog = @IdReporteLog;
GO
