SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_108]
AS

BEGIN

-- Validar que la Fecha de Disposición (dat_fecha_disposición) sea una fecha válida (AAAAMM).

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion
FROM dbo.RW_VW_R04C0469_INC
WHERE LEN(ISNULL(FechaDisposicion,'')) <> 6 OR ISDATE(ISNULL(FechaDisposicion,'') + '15') = 0;

END


GO
