SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_028_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá anotar el monto de las comisiones devengadas. En caso de que no haya monto de comisiones devengadas se anotará 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoComisionDevengada,''),'-1') AS DECIMAL(23,2)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
