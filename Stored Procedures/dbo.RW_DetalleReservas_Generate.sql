SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_DetalleReservas_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodoHistorico BIGINT;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INTERNO' AND Nombre = '_DetalleReservas';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_DetalleReservas;


SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo=1;
-- Se selecciona mes del anio anterior cuando es inicio de anio
IF(MONTH(@FechaPeriodo)=1)
BEGIN
 SELECT @IdPeriodoHistorico=IdPeriodoHistorico FROM dbo.SICC_PeriodoHistorico WHERE Activo=1 AND YEAR(Fecha)=YEAR(@FechaPeriodo)-1 AND MONTH(Fecha)=12;
END
ELSE
-- Se selecciona mes anterior y anio anterior del historico 
BEGIN
 SELECT @IdPeriodoHistorico=IdPeriodoHistorico FROM dbo.SICC_PeriodoHistorico WHERE Activo=1 AND YEAR(Fecha)=YEAR(@FechaPeriodo) AND MONTH(Fecha)=MONTH(@FechaPeriodo)-1;
END


-- CARTERA COMERCIAL
-- Inserto los creditos que se encuentran en el Historico del mes anterior
INSERT INTO dbo.RW_DetalleReservas (IdReporteLog, CodigoCredito, ReservaInicial, ReservaFinal, Cargo, Abono)
SELECT
 @IdReporteLog,
 cr.Credito,
 CAST(cr.ReservaFinal AS DECIMAL(23,2)),
 0.00,
 0.00,
 0.00
FROM Historico.SICCMX_Credito_Reservas_Variables cr
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Inserto los creditos nuevos en el mes
INSERT INTO dbo.RW_DetalleReservas (IdReporteLog, CodigoCredito, ReservaInicial, ReservaFinal, Cargo, Abono)
SELECT
 @IdReporteLog,
 c.Codigo,
 0.00,
 0.00,
 0.00,
 0.00
FROM dbo.SICCMX_Credito c
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables cr ON cr.IdCredito = c.IdCredito
LEFT OUTER JOIN Historico.SICCMX_Credito_Reservas_Variables hcr ON c.Codigo = hcr.Credito AND hcr.IdPeriodoHistorico = @IdPeriodoHistorico
WHERE hcr.Credito IS NULL;


-- CARTERA CONSUMO
-- Inserto los creditos que se encuentran en el Historico del mes anterior
INSERT INTO dbo.RW_DetalleReservas (IdReporteLog, CodigoCredito, ReservaInicial, ReservaFinal, Cargo, Abono)
SELECT
 @IdReporteLog,
 cr.Consumo,
 CAST(ISNULL(cr.ReservaTotal,0) AS DECIMAL(23,2)),
 0.00,
 0.00,
 0.00
FROM Historico.SICCMX_Consumo_Reservas_Variables cr
WHERE cr.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Inserto los creditos nuevos en el mes
INSERT INTO dbo.RW_DetalleReservas (IdReporteLog, CodigoCredito, ReservaInicial, ReservaFinal, Cargo, Abono)
SELECT
 @IdReporteLog,
 c.Codigo,
 0.00,
 0.00,
 0.00,
 0.00
FROM dbo.SICCMX_Consumo c
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables cr ON cr.IdConsumo = c.IdConsumo
LEFT OUTER JOIN Historico.SICCMX_Consumo_Reservas_Variables hcr ON c.Codigo = hcr.Consumo AND hcr.IdPeriodoHistorico = @IdPeriodoHistorico
WHERE hcr.Consumo IS NULL;


-- Realizo Cargo o Abono dependiendo del movimiento de Reservas Cartera Comercial
UPDATE det
SET 
/*
 Cargo = CASE WHEN det.ReservaInicial<cr.ReservaFinal THEN CAST(cr.ReservaFinal-det.ReservaInicial AS DECIMAL(23,2)) ELSE 0 END,
 Abono = CASE WHEN det.ReservaInicial>cr.ReservaFinal THEN CAST(det.ReservaInicial-cr.ReservaFinal AS DECIMAL(23,2)) ELSE 0 END,
*/--31/JUL/15 PP/ER 
 Abono = CASE WHEN det.ReservaInicial<cr.ReservaFinal THEN CAST(cr.ReservaFinal-det.ReservaInicial AS DECIMAL(23,2)) ELSE 0 END,
 Cargo = CASE WHEN det.ReservaInicial>cr.ReservaFinal THEN CAST(det.ReservaInicial-cr.ReservaFinal AS DECIMAL(23,2)) ELSE 0 END,
 ReservaFinal = CAST(cr.ReservaFinal AS DECIMAL(23,2)),
 GradoRiesgo = cal.codigo,
 TipoProducto = ISNULL(tpo.Codigo,'')
FROM dbo.RW_DetalleReservas det
INNER JOIN dbo.SICCMX_Credito c ON det.CodigoCredito = c.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo ci ON c.IdCredito = ci.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tpo ON ci.IdTipoProductoSerie4 = tpo.IdTipoProducto
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables cr ON cr.IdCredito = c.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON cr.CalifFinal = cal.IdCalificacion;


-- Realizo Cargo o Abono dependiendo del movimiento de Reservas Cartera Consumo
UPDATE det
SET Cargo = CASE WHEN det.ReservaInicial<cr.ReservaTotal THEN CAST(cr.ReservaTotal-det.ReservaInicial AS DECIMAL(23,2)) ELSE 0 END,
 Abono = CASE WHEN det.ReservaInicial>cr.ReservaTotal THEN CAST(det.ReservaInicial-cr.ReservaTotal AS DECIMAL(23,2)) ELSE 0 END,
 ReservaFinal = CAST(cr.ReservaTotal AS DECIMAL(23,2)),
 GradoRiesgo = cal.codigo,
 TipoProducto = ISNULL(tpo.Codigo,'')
FROM dbo.RW_DetalleReservas det
INNER JOIN dbo.SICCMX_Consumo c ON det.CodigoCredito = c.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo ci ON c.IdConsumo = ci.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tpo ON ci.IdTipoProductoSerie4 = tpo.IdtipoProducto
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables cr ON cr.IdConsumo = c.IdConsumo
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON cr.IdCalificacion = cal.IdCalificacion;


UPDATE dbo.RW_DetalleReservas SET Fecha=CAST(YEAR(@FechaPeriodo) AS VARCHAR)+RIGHT('0'+CAST(MONTH(@FechaPeriodo) AS VARCHAR),2);


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_DetalleReservas WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
