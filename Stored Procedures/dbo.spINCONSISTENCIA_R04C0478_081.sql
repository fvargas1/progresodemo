SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_081]
AS

BEGIN

-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Nombre de Grupo de Riesgo (dat_nombre_grupo_riesgo) diferente.

SELECT
	CodigoCredito,
	REPLACE(GrupoRiesgo, ',', '') AS GrupoRiesgo,
	RFC
FROM dbo.RW_VW_R04C0478_INC
WHERE GrupoRiesgo IN (
	SELECT rep.GrupoRiesgo
	FROM dbo.RW_VW_R04C0478_INC rep
	INNER JOIN dbo.RW_VW_R04C0478_INC rep2 ON rep.RFC = rep2.RFC AND rep.GrupoRiesgo <> rep2.GrupoRiesgo
	GROUP BY rep.GrupoRiesgo, rep.RFC
);

END


GO
