SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Datos_A21]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=21;

EXEC SICCMX_Inicializa_Datos @IdMetodologia;
GO
