SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_Localidad_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_Localidad_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.Localidad) = loc.CodigoCNBV
WHERE loc.CodigoCNBV IS NULL AND LEN(f.Localidad) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'Localidad',
 f.Localidad,
 2,
 @Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.Localidad) = loc.CodigoCNBV
WHERE loc.CodigoCNBV IS NULL AND LEN(f.Localidad) > 0;
GO
