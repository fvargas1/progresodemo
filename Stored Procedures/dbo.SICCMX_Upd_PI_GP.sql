SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Upd_PI_GP]
AS
UPDATE aval
SET PIAval = ppi.[PI]
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON aval.IdAval = ppi.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante = esg.IdEsGarante
WHERE esg.Layout = 'AVAL';

UPDATE gar
SET PIGarante = ppi.[PI]
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON gar.IdGarantia = ppi.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON ppi.EsGarante = esg.IdEsGarante
WHERE esg.Layout = 'GARANTIA';

UPDATE aval
SET PIAval = ppi.[PI]
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.SICCMX_Persona_PI ppi ON aval.IdPersona = ppi.IdPersona;

UPDATE gar
SET PIGarante = ppi.[PI]
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_Persona_PI ppi ON gar.IdPersona = ppi.IdPersona;
GO
