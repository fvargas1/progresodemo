SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_ConsumoAval_Migracion]
AS
UPDATE cg
SET Porcentaje = NULLIF(f.Porcentaje,''),
	Monto = 0
FROM dbo.SICCMX_ConsumoAval cg
INNER JOIN dbo.SICCMX_Consumo c ON cg.IdConsumo = c.IdConsumo
INNER JOIN dbo.SICCMX_Aval a ON cg.IdAval = a.IdAval
INNER JOIN dbo.FILE_ConsumoAval f ON c.Codigo = LTRIM (f.CodigoCredito) AND a.Codigo = LTRIM(f.CodigoAval)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_ConsumoAval (
	IdConsumo,
	IdAval,
	Porcentaje
)
SELECT
	(SELECT IdConsumo FROM dbo.SICCMX_Consumo WHERE Codigo = LTRIM(f.CodigoCredito)),
	(SELECT IdAval FROM dbo.SICCMX_Aval WHERE Codigo = LTRIM(f.CodigoAval)),
	NULLIF(f.Porcentaje,'')
FROM dbo.FILE_ConsumoAval f
WHERE f.CodigoAval+'-'+f.CodigoCredito
NOT IN (
	SELECT a.Codigo+'-'+c.Codigo
	FROM dbo.SICCMX_ConsumoAval cg
	INNER JOIN dbo.SICCMX_Aval a ON cg.IdAval = a.IdAval
	INNER JOIN dbo.SICCMX_Consumo c ON c.IdConsumo = cg.IdConsumo
) AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
