SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_086_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo RFC acreditado (dat_rfc) no tenga más de un Municipio (cve_municipio_siti).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE Municipio IN (
	SELECT rep.Municipio
	FROM dbo.RW_VW_R04C0468_INC rep
	INNER JOIN dbo.RW_VW_R04C0468_INC rep2 ON rep.RFC = rep2.RFC AND rep.Municipio <> rep2.Municipio
	GROUP BY rep.Municipio, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
