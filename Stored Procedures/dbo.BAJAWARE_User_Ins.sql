SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_Ins]
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@Blocked BAJAWARE_utActivo,
	@Name BAJAWARE_utDescripcion100,
	@Email BAJAWARE_utDescripcion100,
	@Telefono BAJAWARE_utTelefono,
	@Mobil BAJAWARE_utTelefono,
	@NumeroControl BAJAWARE_utNombreCodigo,
	@Username BAJAWARE_utNombreCodigo,
	@Password BAJAWARE_utBinary,
	@UsuarioWindows BAJAWARE_utDescripcion100,
	@FechaNacimiento datetime
AS
INSERT INTO dbo.BAJAWARE_User (Active, Description, Blocked, Name, Email, Telefono, Mobil, NumeroControl, Username, Password, UsuarioWindows, FechaNacimiento, UltimaFechaLogin, NeverLogedIn)
VALUES (@Active, @Description, @Blocked, @Name, @Email, @Telefono, @Mobil, @NumeroControl, @Username, @Password, @UsuarioWindows, @FechaNacimiento, GETDATE(), 1);

SELECT IdUser, Active, Description, Blocked, Name, FechaNacimiento, Email, Telefono, Mobil, NumeroControl, Username, Password, UsuarioWindows, UltimaFechaLogin, NeverLogedIn
FROM dbo.BAJAWARE_User
WHERE IdUser=SCOPE_IDENTITY();
GO
