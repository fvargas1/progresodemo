SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0467_012_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0467_INC rep
LEFT OUTER JOIN dbo.RW_R04C0463 r63 ON rep.CodigoCreditoCNBV = r63.CodigoCreditoCNBV
LEFT OUTER JOIN Historico.RW_R04C0463 hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE hist.IdPeriodoHistorico IS NULL AND r63.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
