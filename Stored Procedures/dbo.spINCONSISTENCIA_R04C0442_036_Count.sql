SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_036_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si no hay banca de desarrollo que otorgó recursos (Col.32), entonces el monto apoyo banca de desarrollo (Col.31) deberá ser 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0442
WHERE LEN(ISNULL(CodigoBancoFondeador,'')) = 0 AND CAST(ISNULL(NULLIF(MontoFondeo,''),'0') AS DECIMAL(21)) > 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
