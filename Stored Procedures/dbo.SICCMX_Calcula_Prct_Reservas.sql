SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Prct_Reservas]
AS
UPDATE dbo.SICCMX_Credito_Reservas_Variables
SET
 PorReservaFinal = CASE WHEN EI_Total > 0 THEN ReservaFinal / EI_Total ELSE 0 END,
 PorExpuesta = CASE WHEN EI_Expuesta_GarPer > 0 THEN ReservaExpuesta_GarPer / EI_Expuesta_GarPer ELSE 0 END,
 PorCubierto = CASE WHEN EI_Cubierta_GarPer > 0 THEN ReservaCubierta_GarPer / EI_Cubierta_GarPer ELSE 0 END;
GO
