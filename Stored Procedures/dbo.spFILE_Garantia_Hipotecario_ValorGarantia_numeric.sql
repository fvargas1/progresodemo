SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Hipotecario_ValorGarantia_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_Hipotecario_ValorGarantia_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia_Hipotecario
SET errorFormato = 1
WHERE ISNUMERIC(ISNULL(ValorGarantia,'0')) = 0 OR ValorGarantia LIKE '%[^0-9 .]%';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'ValorGarantia',
 ValorGarantia,
 1,
 @Detalle
FROM dbo.FILE_Garantia_Hipotecario
WHERE ISNUMERIC(ISNULL(ValorGarantia,'0')) = 0 OR ValorGarantia LIKE '%[^0-9 .]%';
GO
