SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ReporteApoyo_ListByReporte]
	@IdReporte INT
AS
SELECT
	vw.IdReporteApoyo,
	vw.IdReporte,
	vw.IdReporteNombre,
	vw.Nombre,
	vw.Descripcion,
	vw.Activo,
	vw.OnDemand,
	vw.NombreArchivo,
	vw.SSRSTemplate,
	vw.IdUsuarioCreacion,
	vw.UsuarioCreacionNombre,
	vw.FechaCreacion,
	vw.IdUsuarioModificacion,
	vw.UsuarioModificacionNombre,
	vw.FechaModificacion
FROM dbo.RW_VW_ReporteApoyo vw
WHERE vw.IdReporte = @IdReporte;
GO
