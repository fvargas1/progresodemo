SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0481_003]
AS

BEGIN

-- El factor ajuste hfx debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	Hfx
FROM dbo.RW_VW_R04C0481_INC
WHERE ISNULL(Hfx,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END


GO
