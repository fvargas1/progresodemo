SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_Group_Group_RemoveGroup]
	@IdGroupParent BAJAWARE_utID,
	@IdGroupChild BAJAWARE_utID
AS
DELETE FROM dbo.BAJAWARE_Rel_Group_Group
WHERE IdGroupParent = @IdGroupParent AND IdGroupChild = @IdGroupChild;
GO
