SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_MunicipioLaboraAcreditado_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_MunicipioLaboraAcreditado_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Localidad2015 cat ON LTRIM(f.MunicipioLaboraAcreditado) = cat.CodigoMunicipio
WHERE cat.IdLocalidad IS NULL AND LEN(f.MunicipioLaboraAcreditado) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'MunicipioLaboraAcreditado',
 f.MunicipioLaboraAcreditado,
 2,
 @Detalle
FROM dbo.FILE_Hipotecario f
LEFT OUTER JOIN dbo.SICC_Localidad2015 cat ON LTRIM(f.MunicipioLaboraAcreditado) = cat.CodigoMunicipio
WHERE cat.IdLocalidad IS NULL AND LEN(f.MunicipioLaboraAcreditado) > 0;
GO
