SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_NumDiasAtrInfonavitUltBimestre_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_NumDiasAtrInfonavitUltBimestre_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo21
SET errorFormato = 1
WHERE LEN(NumDiasAtrInfonavitUltBimestre)>0 AND (ISNUMERIC(ISNULL(NumDiasAtrInfonavitUltBimestre,'0')) = 0 OR NumDiasAtrInfonavitUltBimestre LIKE '%[^0-9 .-]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'NumDiasAtrInfonavitUltBimestre',
 NumDiasAtrInfonavitUltBimestre,
 1,
 @Detalle
FROM dbo.FILE_Anexo21
WHERE LEN(NumDiasAtrInfonavitUltBimestre)>0 AND (ISNUMERIC(ISNULL(NumDiasAtrInfonavitUltBimestre,'0')) = 0 OR NumDiasAtrInfonavitUltBimestre LIKE '%[^0-9 .-]%');
GO
