SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_049]
AS

BEGIN

-- Si el Tipo de Alta (cve_tipo_alta_credito) es igual a 132, 136, 700, 701, 702, validar que el RFC contenido en el
-- ID metodología CNBV (dat_id_credito_met_cnbv) en las posiciones 14 a la 26, sea IGUAL al RFC que se reporta (dat_rfc).

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoAltaCredito,
	RFC,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('132','136','700','701','702') AND CHARINDEX(ISNULL(RFC,''), ISNULL(CodigoCreditoCNBV,'')) = 0;

END


GO
