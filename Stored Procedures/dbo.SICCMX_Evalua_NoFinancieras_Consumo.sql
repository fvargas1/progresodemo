SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Evalua_NoFinancieras_Consumo]
AS
-- INSERTAMOS EN LOG LAS GARANTIAS NO FINANCIERAS QUE NO CUMPLEN CON EL NIVEL MINIMO DE COBERTURA (CREDITO)
/*
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)
SELECT DISTINCT
 vwCredito.IdCredito,
 GETDATE(),
 '',
 'El % de Cobertura de la garantía "'
 + tpoGar.Nombre
 + '" es de '
 + CAST(CAST(canasta.PrctCobSinAju * 100 AS DECIMAL(18,2)) AS VARCHAR)
 + '%, ya que el "Nivel mínimo de cobertura admisible" es de '
 + CAST(CAST(vw.NivelMinimo AS INT) AS VARCHAR)
 + '%, el porcentaje y el monto cubierto de esta garantía se actualizan a "0"'
FROM dbo.SICCMX_Garantia_Canasta canasta
INNER JOIN dbo.SICCMX_VW_Credito_NMC AS vwCredito ON canasta.IdCredito = vwCredito.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantias_RNF vw ON canasta.IdCredito = vw.IdCredito AND vw.IdTipoGarantia=canasta.IdTipoGarantia
INNER JOIN dbo.SICC_TipoGarantia tpoGar ON vw.IdTipoGarantia = tpoGar.IdTipoGarantia
WHERE canasta.PrctCobSinAju < (vw.NivelMinimo/100);
*/

-- ACTUALIZAMOS LAS GARANTIAS NO FINANCIERAS QUE NO CUMPLEN CON EL NIVEL MINIMO DE COBERTURA (CREDITO)
UPDATE canasta
SET
	MontoCobAjust = 0,
	PrctCobAjust = 0
FROM dbo.SICCMX_Garantia_Canasta_Consumo canasta
INNER JOIN dbo.SICCMX_VW_Consumo vwCredito ON canasta.IdConsumo = vwCredito.IdConsumo
INNER JOIN dbo.SICCMX_VW_Garantias_RNF_Consumo vw ON canasta.IdConsumo = vw.IdConsumo AND vw.IdTipoGarantia=canasta.IdTipoGarantia
WHERE canasta.PrctCobSinAju < (vw.NivelMinimo/100);
GO
