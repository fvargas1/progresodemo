SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_043]
AS

BEGIN

-- La fecha de firma de la reestructura debe ser la misma que el periodo del reporte.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT CodigoCredito, CodigoCreditoCNBV, FechaFirmaReestructura, @FechaPeriodo
FROM dbo.RW_R04H0491
WHERE FechaFirmaReestructura <> '0' AND FechaFirmaReestructura <> @FechaPeriodo;

END
GO
