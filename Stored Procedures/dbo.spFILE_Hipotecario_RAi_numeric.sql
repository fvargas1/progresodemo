SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_RAi_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_RAi_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorCatalogo = 1
WHERE LEN(ISNULL(RAi,'')) > 0 AND (ISNUMERIC(ISNULL(RAi,'0')) = 0 OR RAi LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCredito,
 'RAi',
 RAi,
 2,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE LEN(ISNULL(RAi,'')) > 0 AND (ISNUMERIC(ISNULL(RAi,'0')) = 0 OR RAi LIKE '%[^0-9 .]%');
GO
