SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_029]
AS

BEGIN

-- La fecha de disposición debe ser una fecha válida

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion
FROM dbo.RW_VW_R04C0464_INC
WHERE LEN(ISNULL(FechaDisposicion,'')) <> 6 OR ISDATE(ISNULL(FechaDisposicion,'') + '15') = 0;

END

GO
