SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_083_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Categoría del Crédito (cve_categoria_credito) en el formulario de SEGUIMIENTO es IGUAL a 2,
-- entonces el registro del Tipo de Alta del Crédito (cve_tipo_alta_credito) en el formulariode ALTAS para el mismo
-- CRÉDITO (dat_id_credito_met_cnbv) debe estar entre 700 y 751. (Cruce entre el reporte de seguimiento y el reporte de altas).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT rep.NumeroDisposicion)
FROM dbo.RW_VW_R04C0479_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(rep.CategoriaCredito,'') = '2' AND CAST(ISNULL(NULLIF(vw.TipoAlta,''),'-1') AS INT) NOT BETWEEN 700 AND 751;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
