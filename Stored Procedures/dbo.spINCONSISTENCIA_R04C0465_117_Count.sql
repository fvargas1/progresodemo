SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_117_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- El PORCENTAJE DE PAGOS A INSTITUCIONES BANCARIAS DE 1 A 29 DÍAS DE ATRASO EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0465_INC

WHERE LEN(PorcPagoInstBanc29) > 0 AND PorcPagoInstBanc29 <> '-99'

	AND (CHARINDEX('.',PorcPagoInstBanc29) = 0 OR CHARINDEX('.',PorcPagoInstBanc29) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc29, CHARINDEX('.', PorcPagoInstBanc29) + 1, LEN(PorcPagoInstBanc29)))) <> 6);



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
