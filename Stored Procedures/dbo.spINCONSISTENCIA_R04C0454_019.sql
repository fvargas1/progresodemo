SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_019]
AS
BEGIN
-- El Monto Total Pagado Efectivamente por el Acreditado no cuadra con la suma de los montos pagados.

SELECT
	CodigoCredito,
	MontoTotalPagado,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(MontoTotalPagado,''),'0') AS DECIMAL) <> (
	CAST(ISNULL(NULLIF(MontoCapitalPagado,''),'0') AS DECIMAL) +
	CAST(ISNULL(NULLIF(MontoInteresPagado,''),'0') AS DECIMAL) +
	CAST(ISNULL(NULLIF(MontoComisionPagado,''),'0') AS DECIMAL) +
	CAST(ISNULL(NULLIF(MontoInteresMoratorio,''),'0') AS DECIMAL));

END
GO
