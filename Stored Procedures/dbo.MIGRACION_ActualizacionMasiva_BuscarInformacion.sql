SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_BuscarInformacion]
	@sessionID VARCHAR(100),
	@codigoBusqueda VARCHAR(100),
	@nombreView VARCHAR(100),
	@id VARCHAR(100),
	@campo1 VARCHAR(200),
	@campo2 VARCHAR(200),
	@campo3 VARCHAR(200)
AS
DECLARE @query VARCHAR(1000);

SET @query = 'UPDATE dbo.MIGRACION_ActualizacionMasiva_Temp SET valorEncontrado= 1, Id = o.'+@id+', campo1Original = o.'+@campo1+ ' ';

IF @campo2 <> ''
BEGIN
	SET @query = @query + ', campo2Original = o.'+@campo2 + ' ';
END

IF @campo3 <> ''
BEGIN
	SET @query = @query + ', campo3Original = o.'+@campo3+ ' ';
END

SET @query = @query + ' FROM MIGRACION_ActualizacionMasiva_Temp t INNER JOIN '+@nombreView+' o on o.'+@codigoBusqueda + ' = codigoBusqueda ';
SET @query = @query + ' where t.sessionID = '''+@sessionID+''' ';

EXEC(@query);
GO
