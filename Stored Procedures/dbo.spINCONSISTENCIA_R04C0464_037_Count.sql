SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_037_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Para créditos dispuestos en el mes, el Monto Dispuesto de la Línea de Crédito deberá ser mayor a 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(FechaDisposicion,'') = @FechaPeriodo AND CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
