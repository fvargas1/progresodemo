SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_Migracion]
AS
UPDATE hip
SET IdPersona = per.IdPersona,
 SaldoCapitalVigente = ISNULL(NULLIF(f.SaldoCapitalVigente,''),0),
 InteresVigente = ISNULL(NULLIF(f.SaldoInteresVigente,''),0),
 SaldoCapitalVencido = ISNULL(NULLIF(f.SaldoCapitalVencido,''),0),
 InteresVencido = ISNULL(NULLIF(f.SaldoInteresVencido,''),0),
 MontoGarantia = NULLIF(f.MontoGarantia,''),
 InteresCarteraVencida = NULLIF(f.InteresCarteraVencida,''),
 IdTipoCreditoR04A = tp4.IdTipoProducto,
 IdEntidadCobertura = ent.IdEntidadCobertura,
 IdTipoCreditoR04A_2016 = tp4_16.IdTipoProducto
FROM dbo.FILE_Hipotecario f
INNER JOIN dbo.SICCMX_Hipotecario hip ON LTRIM(f.CodigoCredito) = hip.Codigo
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON LTRIM(f.EntidadCobertura) = ent.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp4 ON LTRIM(f.TipoCreditoR04A) = tp4.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp4_16 ON LTRIM(f.TipoCreditoR04A_2016) = tp4_16.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Hipotecario (
 IdPersona, Codigo, SaldoCapitalVigente, InteresVigente, SaldoCapitalVencido, InteresVencido,
 MontoGarantia, InteresCarteraVencida, IdTipoCreditoR04A, IdEntidadCobertura, IdTipoCreditoR04A_2016
)
SELECT
 per.IdPersona,
 LTRIM(RTRIM(f.CodigoCredito)),
 NULLIF(f.SaldoCapitalVigente,''),
 NULLIF(f.SaldoInteresVigente,''),
 NULLIF(f.SaldoCapitalVencido,''),
 NULLIF(f.SaldoInteresVencido,''),
 NULLIF(f.MontoGarantia,''),
 NULLIF(f.InteresCarteraVencida,''),
 tp4.IdTipoProducto,
 ent.IdEntidadCobertura,
 tp4_16.IdTipoProducto
FROM dbo.FILE_Hipotecario f
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICC_EntidadCobertura ent ON LTRIM(f.EntidadCobertura) = ent.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp4 ON LTRIM(f.TipoCreditoR04A) = tp4.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp4_16 ON LTRIM(f.TipoCreditoR04A_2016) = tp4_16.Codigo
LEFT OUTER JOIN dbo.SICCMX_Hipotecario hip ON LTRIM(f.CodigoCredito) = hip.Codigo
WHERE hip.Codigo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


SET NOCOUNT ON;
-- HIPOTECARIO INFO
INSERT INTO dbo.SICCMX_HipotecarioInfo (IdHipotecario)
SELECT DISTINCT h.IdHipotecario
FROM dbo.SICCMX_Hipotecario h
LEFT OUTER JOIN dbo.SICCMX_HipotecarioInfo hi ON h.IdHipotecario = hi.IdHipotecario
WHERE hi.IdHipotecario IS NULL;

-- HIPOTECARIO ADICIONAL 
INSERT INTO dbo.SICCMX_HipotecarioAdicional (IdHipotecario)
SELECT DISTINCT h.IdHipotecario
FROM dbo.SICCMX_Hipotecario h
LEFT OUTER JOIN dbo.SICCMX_HipotecarioAdicional ha ON h.IdHipotecario = ha.IdHipotecario
WHERE ha.IdHipotecario IS NULL;

-- HIPOTECARIO RESERVAS VARIABLES PRELIMINARES
INSERT INTO dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares(IdHipotecario)
SELECT DISTINCT h.IdHipotecario
FROM dbo.SICCMX_Hipotecario h
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares hr ON h.IdHipotecario = hr.IdHipotecario
WHERE hr.IdHipotecario IS NULL;


-- UPDATE SICCMX_HipotecarioInfo 
UPDATE hInfo
SET
 AjusteTasaReferencia = LTRIM(RTRIM(f.AjusteTasaReferencia)),
 MontoApoyoBancaDesarrollo = NULLIF(f.MontoApoyoBancaDesarrollo,''),
 IdCategoria = cat.IdCategoriaHipotecario,
 ComisionesCobradas = NULLIF(f.ComisionesCobradas,''),
 TasaComisiones = NULLIF(f.TasaComisiones,''),
 GastosOriginacion = NULLIF(f.GastosOriginacion,''),
 IdMoneda = mon.IdMoneda,
 IdMonedaOriginal = monOri.IdMoneda,
 IdMonedaReestructurado = monRes.IdMoneda,
 IdDestinoHipotecario = dst.IdDestinoHipotecario,
 IdEntidadAseguradora = ent.IdEntidadAseguradora,
 IdEntidadCofinanciadora = cof.IdCofinanciamiento,
 FechaReestructura = NULLIF(f.FechaReestructura,''),
 FechaOtorgamiento = NULLIF(f.FechaOtorgamiento,''),
 FechaVencimiento = NULLIF(f.FechaVencimiento,''),
 FechaVencimientoReestructura = NULLIF(f.FechaVencimientoReestructura,''),
 FechaUltimoPagoCliente = NULLIF(f.FechaUltimoPagoCliente,''),
 IngresosMensuales = NULLIF(f.IngresosMensuales,''),
 IdLocalidad = (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadVivienda)),
 MontoSubcuenta = NULLIF(f.MontoSubcuenta,''),
 MontoBonificacion = NULLIF(f.MontoBonificacionAlCliente,''),
 MontoCofinanciador = NULLIF(f.MontoCofinanciador,''),
 MontoReestructura = NULLIF(f.MontoReestructura,''),
 MontoExigible = NULLIF(f.MontoExigible,''),
 MontoPagadoCliente = NULLIF(f.MontoPagadoCliente,''),
 MontoSubsidioFederal = NULLIF(f.MontoSubsidioFederal,''),
 MontoOriginal = NULLIF(f.MontoOriginal,''),
 FolioBuroCredito = LTRIM(RTRIM(f.FolioBuroCredito)),
 NumeroAvaluo = LTRIM(RTRIM(f.NumeroAvaluo)),
 PorcentajeAsegurado = NULLIF(f.PorcentajeAsegurado,''),
 IdProductoHipotecario = prod.IdProductoHipotecario,
 ResponsabilidadTotal = NULLIF(f.ResponsabilidadTotal,''),
 SaldoFinalCapital = NULLIF(f.SaldoFinalCapital,''),
 SaldoInicialCapital = NULLIF(f.SaldoInicialCapital,''),
 IdSeguroCargoCliente = seg.IdSeguroAcreditado,
 IdTasaReferencia = tasa.IdTasaReferenciaHipotecario,
 IdTipoAlta = tAlta.IdTipoAltaHipotecario,
 IdTipoBaja = tBaja.IdTipoBajaHipotecario,
 IdTipoSeguro = tpoSeg.IdTipoSeguro,
 IdTipoTasaInteres = tInt.IdTasaInteresHipotecario,
 ValorOriginal = NULLIF(f.ValorOriginal,''),
 ValorBienAdjudicado = NULLIF(f.ValorBienAdjudicado,''),
 ValorAvaluo = NULLIF(f.ValorAvaluo,''),
 TasaInteres = NULLIF(f.TasaInteres,''),
 CodigoCreditoReestructurado = LTRIM(RTRIM(f.CodigoCreditoReestructurado)),
 IdSituacionCredito = sit.IdSituacionHipotecario,
 MontoSubsidio = NULLIF(f.MontoSubsidioFederal,''),
 IdPeriodicidadCapital = perCap.IdPeriodicidadCapitalHipotecario,
 EntidadContratoCredito = LTRIM(RTRIM(f.EntidadContratoCredito)),
 ReservasSaldoFinal = NULLIF(f.ReservasSaldoFinal,''),
 ATR = CAST(NULLIF(f.Atr,'') AS DECIMAL),
 MaxATR = CAST(NULLIF(f.MaxAtr,'') AS DECIMAL),
 VPago = NULLIF(f.Vpago,''),
 PorPago = NULLIF(f.PorPago,''),
 PorCLTVi = NULLIF(f.PorCtlvi,''),
 PPagoIM = NULLIF(f.PPagoIM,''),
 TRi = NULLIF(f.Tri,''),
 MON = NULLIF(f.MON,''),
 DiasVencidos = NULLIF(f.DiasVencidos,''),
 IdTipoRegimenCredito = reg.IdTipoRegimen,
 PromedioDeRet = NULLIF(f.PromedioDeRet,'')
FROM dbo.FILE_Hipotecario f
INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.Codigo = f.CodigoCredito
INNER JOIN dbo.SICCMX_HipotecarioInfo hInfo ON hip.IdHipotecario = hInfo.IdHipotecario
LEFT OUTER JOIN dbo.SICC_CategoriaCreditoHipotecario cat ON LTRIM(f.Categoria) = cat.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda monOri ON LTRIM(f.MonedaOriginal) = monOri.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda monRes ON LTRIM(f.MonedaReestructurado) = monRes.Codigo
LEFT OUTER JOIN dbo.SICC_DestinoHipotecario dst ON LTRIM(f.Destino) = dst.Codigo
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora ent ON LTRIM(f.EntidadSeguro) = ent.Codigo
LEFT OUTER JOIN dbo.SICC_Cofinanciamiento cof ON LTRIM(f.EntidadCofinanciadora) = cof.Codigo
LEFT OUTER JOIN dbo.SICC_ProductoHipotecario prod ON LTRIM(f.ProductoHipotecario) = prod.Codigo
LEFT OUTER JOIN dbo.SICC_SeguroAcreditado seg ON LTRIM(f.SeguroCargoCliente) = seg.Codigo
LEFT OUTER JOIN dbo.SICC_TasaReferenciaHipotecario tasa ON LTRIM(f.TasaReferencia) = tasa.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario tAlta ON LTRIM(f.TipoAlta) = tAlta.Codigo
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario tBaja ON LTRIM(f.TipoBaja) = tBaja.Codigo
LEFT OUTER JOIN dbo.SICC_TipoSeguro tpoSeg ON LTRIM(f.TipoSeguro) = tpoSeg.Codigo
LEFT OUTER JOIN dbo.SICC_TasaInteresHipotecario tInt ON LTRIM(f.TipoTasaInteres) = tInt.Codigo
LEFT OUTER JOIN dbo.SICC_SituacionHipotecario sit ON LTRIM(f.SituacionCredito) = sit.Codigo
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalHipotecario perCap ON LTRIM(f.PeriodicidadCapital) = perCap.Codigo
LEFT OUTER JOIN dbo.SICC_TipoRegimen reg ON LTRIM(f.TipoRegimenCredito) = reg.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- UPDATE SICCMX_HipotecarioAdicional
UPDATE ha
SET SaldoPromedio = NULLIF(f.SaldoPromedio,''),
 InteresesDevengados = NULLIF(f.InteresDevengado,''),
 Comisiones = NULLIF(f.Comision,'')
FROM dbo.SICCMX_HipotecarioAdicional ha
INNER JOIN dbo.SICCMX_Hipotecario h ON ha.IdHipotecario = h.IdHipotecario
INNER JOIN dbo.FILE_Hipotecario f ON h.Codigo = LTRIM(f.CodigoCredito)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- UPDATE SICCMX_Hipotecario_Reservas_VariablesPreliminares
UPDATE pre
SET IdMetodologia = met.IdMetodologiaHipotecario,
 ValorVivienda = NULLIF(f.ValorVivienda,''),
 INTEXP = NULLIF(f.IntExp,''),
 SUBCVi = NULLIF(f.SUBCVi,''),
 SDESi = NULLIF(f.SDESi,''),
 IdConvenio = conv.IdConvenioJudicial,
 PorCOBPAMED = NULLIF(f.PorCobPaMed,''),
 PorCOBPP = NULLIF(f.PorCobPP,''),
 Estado = LTRIM(RTRIM(f.Estado)),
 Municipio = LTRIM(RTRIM(f.Municipio)),
 DiasAtraso = CAST(NULLIF(f.DiasAtraso,'') AS INT),
 Veces = NULLIF(f.Veces,''),
 PromedioDeRet = NULLIF(f.PromedioDeRet,'')
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Hipotecario h ON pre.IdHipotecario = h.IdHipotecario
INNER JOIN dbo.FILE_Hipotecario f ON h.Codigo = LTRIM(f.CodigoCredito)
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON LTRIM(f.EsProductoInfonavit) = met.Codigo
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial conv ON LTRIM(f.Convenio) = conv.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- Actualizamos Metodologia para REA y PRO
UPDATE pre
SET IdMetodologia = metREA.IdMetodologiaHipotecario
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON pre.IdHipotecario = inf.IdHipotecario
INNER JOIN dbo.SICC_TipoRegimen tr ON inf.IdTipoRegimenCredito = tr.IdTipoRegimen AND tr.Codigo IN ('2','3')
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo = '3'
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia metREA ON metREA.Codigo = '4';

-- CAMPOS CAMBIO REGULATORIO 2016
UPDATE hInfo
SET IdGeneroAcreditado = gen.IdGenero,
 EdadAcreditado = NULLIF(f.EdadAcreditado,''),
 IdEstadoCivilAcreditado = edo.IdEstadoCivil,
 MunicipioLaboraAcreditado = LTRIM(RTRIM(f.MunicipioLaboraAcreditado)),
 EstadoLaboraAcreditado = LTRIM(RTRIM(f.EstadoLaboraAcreditado)),
 IdAportaSubcuenta = aps.IdAportaSubcuenta,
 IdGeneroCoacreditado = genCo.IdGenero,
 EdadCoacreditado = NULLIF(f.EdadCoacreditado,''),
 IdEstadoCivilCoacreditado = edoCo.IdEstadoCivil,
 NombreCoacreditado = LTRIM(RTRIM(f.NombreCoacreditado)),
 RfcCoacreditado = LTRIM(RTRIM(f.RfcCoacreditado)),
 CurpCoacreditado = LTRIM(RTRIM(f.CurpCoacreditado)),
 MunicipioLaboraCoacreditado = LTRIM(RTRIM(f.MunicipioLaboraCoacreditado)),
 EstadoLaboraCoacreditado = LTRIM(RTRIM(f.EstadoLaboraCoacreditado)),
 ClaveDeCuv = LTRIM(RTRIM(f.ClaveDeCuv)),
 IdSegmentoDeVivienda = cc.IdClasificacionContableHip,
 IngresosBrutosCoacreditado = NULLIF(f.IngresosBrutosCoacreditado,''),
 IdComprobacionIngresosCoacreditado = ing.IdTipoIngreso,
 IdSectorLaboralCoacreditado = lab.IdSectorLaboral,
 NumeroCrediticiaCoacreditado = LTRIM(RTRIM(f.NumeroCrediticiaCoacreditado)),
 PrctLTV = NULLIF(f.PrctLTV,''),
 MontoQuitas = NULLIF(f.MontoQuitas,''),
 MontoCondonaciones = NULLIF(f.MontoCondonaciones,''),
 MontoDescuentos = NULLIF(f.MontoDescuentos,''),
 SaldoBaseCalculoInteres = NULLIF(f.SaldoBaseCalculoInteres,''),
 NumeroDiasCalculoIntereses = NULLIF(f.NumeroDiasCalculoIntereses,''),
 FactorDeVivienda = NULLIF(f.FactorDeVivienda,''),
 IdTipoActualizacion = act.IdTipoActualizacion,
 NumeroReAvaluoInmueble = LTRIM(RTRIM(f.NumeroReAvaluoInmueble)),
 ReservasCobroGarantias = NULLIF(f.ReservasCobroGarantias,''),
 IdGrupoRiesgoEstandar = gpo.IdRCGrupoRiesgoHip,
 PonderadorRiesgoEstandar = NULLIF(f.PonderadorRiesgoEstandar,''),
 ExposicionNetaReservas = NULLIF(f.ExposicionNetaReservas,''),
 RequerimientoCapital = NULLIF(f.RequerimientoCapital,''),
 MontoDacionPago = NULLIF(f.MontoDacionPago,'')
FROM dbo.FILE_Hipotecario f
INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.Codigo = f.CodigoCredito
INNER JOIN dbo.SICCMX_HipotecarioInfo hInfo ON hip.IdHipotecario = hInfo.IdHipotecario
LEFT OUTER JOIN dbo.SICC_Genero gen ON LTRIM(f.GeneroAcreditado) = gen.Codigo
LEFT OUTER JOIN dbo.SICC_EstadoCivil edo ON LTRIM(f.EstadoCivilAcreditado) = edo.Codigo
LEFT OUTER JOIN dbo.SICC_AportaSubcuenta aps ON LTRIM(f.AportaSubcuenta) = aps.Codigo
LEFT OUTER JOIN dbo.SICC_Genero genCo ON LTRIM(f.GeneroCoacreditado) = genCo.Codigo
LEFT OUTER JOIN dbo.SICC_EstadoCivil edoCo ON LTRIM(f.EstadoCivilCoacreditado) = edoCo.Codigo
LEFT OUTER JOIN dbo.SICC_ClasificacionContableHip cc ON LTRIM(f.SegmentoDeVivienda) = cc.Codigo
LEFT OUTER JOIN dbo.SICC_TipoIngreso ing ON LTRIM(f.ComprobacionIngresosCoacreditado) = ing.Codigo
LEFT OUTER JOIN dbo.SICC_SectorLaboral lab ON LTRIM(f.SectorLaboralCoacreditado) = lab.Codigo
LEFT OUTER JOIN dbo.SICC_TipoActualizacion act ON LTRIM(f.TipoActualizacion) = act.Codigo
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgoHip gpo ON LTRIM(f.GrupoRiesgoEstandar) = gpo.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
