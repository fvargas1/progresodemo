SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_034_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "VALOR DE LA VIVIENDA AL MOMENTO DE LA ORIGINACION" debe ser mayor a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE CAST(ISNULL(NULLIF(ValorOriginalVivienda,''),'0') AS DECIMAL) <= 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
