SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_080]
AS

BEGIN

-- Si el Puntaje Asignado por el Porcentaje de Pagos a Entidades Comerciales con 60 o más días de atraso en los últimos 12 meses (cve_ptaje_porc_pago_comercios) es = 42,
-- entonces el Porcentaje de Pagos a Entidades Comerciales con 60 o más días de atraso en los últimos 12 meses (dat_porcent_pago_comercios) debe ser > 0 y < 10

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoEntComer,
	P_PorcPagoEntComer AS Puntos_PorcPagoEntComer
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PorcPagoEntComer,'') = '42' AND (CAST(PorcPagoEntComer AS DECIMAL(10,6)) <= 0 OR CAST(PorcPagoEntComer AS DECIMAL(10,6)) >= 10);

END


GO
