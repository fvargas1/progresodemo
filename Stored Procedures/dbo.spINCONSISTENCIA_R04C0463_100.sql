SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_100]
AS
BEGIN
-- Validar que la Nacionalidad corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.Nacionalidad
FROM dbo.RW_VW_R04C0463_INC rep
LEFT OUTER JOIN dbo.SICC_Pais pais ON ISNULL(rep.Nacionalidad,'') = pais.CodigoCNBV
WHERE pais.IdPais IS NULL;

END

GO
