SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_GetHelp_PI]
 @IdMetodologia INT
AS
SELECT
 fact.Nombre
 + ' : ' + sub.Codigo + ' ' + sub.Nombre AS Factor,
 piVar.Descripcion + CASE WHEN piVar.Codigo LIKE '21SA%' THEN ' (Sin Atrasos)' WHEN piVar.Codigo LIKE '21CA%' THEN ' (Con Atrasos)' ELSE '' END AS Descripcion,
 help.Rango,
 help.Puntos
FROM dbo.SICCMX_Ayuda_Calculo_PI help
INNER JOIN dbo.SICCMX_PI_Variables piVar ON help.IdVariable = piVar.Id
INNER JOIN dbo.SICCMX_PI_SubFactor sub ON piVar.IdSubFactor = sub.Id
INNER JOIN dbo.SICCMX_PI_Factores fact ON sub.IdFactor = fact.Id
WHERE help.IdMetodologia = @IdMetodologia
ORDER BY help.IdVariable, help.Orden;
GO
