SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_DetalleReservas_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Fecha,
	CodigoCredito,
	TipoProducto,
	ReservaInicial,
	ReservaFinal,
	Cargo,
	Abono,
	GradoRiesgo
FROM Historico.RW_DetalleReservas
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoCredito;
GO
