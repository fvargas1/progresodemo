SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_029]
AS

BEGIN

-- Si el Tipo de Alta (cve_tipo_alta_credito) es igual a 132, 700, 701, 702, validar que la Fecha de Otorgamiento contenida en el
-- ID metodología CNBV (dat_id_credito_met_cnbv) en las posiciones 8 a la 13, sea IGUAL al periodo que se reporta (cve_periodo).

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoAltaCredito,
	CodigoCreditoCNBV,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('132','700','701','702') AND SUBSTRING(CodigoCreditoCNBV,8,6) <> @FechaPeriodo;

END


GO
