SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ProyectoLinea_ListErrores]
AS
SELECT
 NumeroLinea,
 CodigoProyecto,
 Fuente
FROM dbo.FILE_ProyectoLinea
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
