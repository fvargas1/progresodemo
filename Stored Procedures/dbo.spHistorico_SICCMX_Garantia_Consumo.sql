SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia_Consumo]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Garantia_Consumo WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia_Consumo (
	IdPeriodoHistorico,
	Codigo,
	TipoGarantia,
	ValorGarantia,
	Moneda,
	FechaValuacion,
	Descripcion,
	BancoGarantia,
	ValorGarantiaProyectado,
	Hc,
	VencimientoRestante,
	GradoRiesgo,
	AgenciaCalificadora,
	Calificacion,
	Emisor,
	Escala,
	EsIPC,
	PIGarante,
	CodigoCliente,
	IndPerMorales,
	Aplica
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	gar.Codigo,
	tg.Codigo,
	gar.ValorGarantia,
	mon.Codigo,
	gar.FechaValuacion,
	gar.Descripcion,
	ins.Codigo,
	gar.ValorGarantiaProyectado,
	gar.Hc,
	gar.VencimientoRestante,
	gar.IdGradoRiesgo,
	agen.Codigo,
	gar.Calificacion,
	emi.Codigo,
	esc.Codigo,
	gar.EsIPC,
	gar.PIGarante,
	per.Codigo,
	gar.IndPerMorales,
	gar.Aplica
FROM dbo.SICCMX_Garantia_Consumo gar
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON gar.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Institucion ins ON gar.IdBancoGarantia = ins.IdInstitucion
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON gar.IdAgenciaCalificadora = agen.IdAgencia
LEFT OUTER JOIN dbo.SICC_Emisor emi ON gar.IdEmisor = emi.IdEmisor
LEFT OUTER JOIN dbo.SICC_Escala esc ON gar.IdEscala = esc.IdEscala
LEFT OUTER JOIN dbo.SICCMX_Persona per ON gar.IdPersona = per.IdPersona;
GO
