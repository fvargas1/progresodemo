SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ProyectoLinea_errores_ClearLog]
AS
UPDATE dbo.FILE_ProyectoLinea
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_ProyectoLinea_errores;
GO
