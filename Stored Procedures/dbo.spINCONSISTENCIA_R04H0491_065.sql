SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_065]
AS

BEGIN

-- El "PORCENTAJE QUE CUBRE EL SEGURO DE CRÉDITO A LA VIVIENDA " debe ser en base 100 y a dos decimales.

SELECT CodigoCredito, CodigoCreditoCNBV, PorcentajeCubiertoSeguro
FROM dbo.RW_R04H0491
WHERE LEN(SUBSTRING(PorcentajeCubiertoSeguro,CHARINDEX('.',PorcentajeCubiertoSeguro)+1,LEN(PorcentajeCubiertoSeguro))) <> 2;

END
GO
