SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_025]
AS

BEGIN

-- El porcentaje cubierto por una garantía real no financiera debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctGarRealNoFin
FROM dbo.RW_VW_R04C0471_INC
WHERE ISNULL(PrctGarRealNoFin,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END


GO
