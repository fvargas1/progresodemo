SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Anexo31Consumo2011_Generate]
AS

TRUNCATE TABLE dbo.RW_Anexo31Consumo2011;

--CONSUMO NO REVOLVENTE
/*
INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
SELECT
	calif.Codigo,
	SUM(ISNULL(reser.Monto,0)) Importe,
	SUM(ISNULL(reser.Reserva,0)) Reserva,
	CASE cal.Codigo
	WHEN 'A' THEN '0 a 0.99 %'
	WHEN 'B' THEN '1 a 19.99 %'
	WHEN 'C' THEN '20 a 59.99 %'
	WHEN 'D' THEN '60 a 89.99 %'
	WHEN 'E' THEN '90 a 100 %'
	END AS PorcentajeReservas,
	met.Nombre,
	SUM( CASE WHEN ISNULL(per.Nombre,'') = 'SEMANAL' THEN reser.Monto ELSE 0 END) ImporteSemanal,
	SUM( CASE WHEN ISNULL(per.Nombre,'') = 'QUINCENAL' THEN reser.Monto ELSE 0 END) ImporteQuincenal,
	SUM( CASE WHEN ISNULL(per.Nombre,'') = 'SEMANAL' THEN reser.Reserva ELSE 0 END) ReservaSemanal,
	SUM( CASE WHEN ISNULL(per.Nombre,'') = 'QUINCENAL' THEN reser.Reserva ELSE 0 END) ReservaQuincenal,
	'CONSUMO_NR'
FROM dbo.SICC_CalificacionConsumo2011 cal
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON met.IdMetodologiaConsumo = cal.IdMetodologia
LEFT OUTER JOIN SICCMX_Consumo_VW_Reservas reser ON reser.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICCMX_Consumo con ON con.IdConsumo = reser.IdConsumo
LEFT OUTER JOIN dbo.SICC_Periocidad per ON per.IdPeriocidad = con.Periodicidad
GROUP BY met.Nombre, cal.Codigo, per.Nombre;
*/

--CONSUMO REVOLVENTE
/*
INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('A',0,0,'0 a 0.99 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-1',0,0,'1 a 2.5 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-2',0,0,'2.51 a 19.99 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('C',0,0,'20 a 59.99 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('D',0,0,'60 a 89.99 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('E',0,0,'90 a 100 %','SEMANAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('A',0,0,'0 a 0.99 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-1',0,0,'1 a 2.5 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-2',0,0,'2.51 a 19.99 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('C',0,0,'20 a 59.99 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('D',0,0,'60 a 89.99 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('E',0,0,'90 a 100 %','QUINCENAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('A',0,0,'0 a 0.99 %','MENSUAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-1',0,0,'1 a 2.5 %','MENSUAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('B-2',0,0,'2.51 a 19.99 %','MENSUAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('C',0,0,'20 a 59.99 %','MENSUAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('D',0,0,'60 a 89.99 %','MENSUAL',0,0,0,0,'CONSUMO_R');

INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
VALUES ('E',0,0,'90 a 100 %','MENSUAL',0,0,0,0,'CONSUMO_R');
*/

--HIPOTECARIO
INSERT INTO dbo.RW_Anexo31Consumo2011 (Calificacion, Importe, Reserva, PorcentajeReservas, Metodologia, ImporteSemanal, ImporteQuincenal, ReservaSemanal, ReservaQuincenal, TipoCredito)
SELECT
	cal.Codigo,
	SUM(ISNULL(hrv.E,0)) Importe,
	SUM(ISNULL(hrv.Reserva,0)) Reserva,
	CAST(CONVERT(DOUBLE PRECISION, CAST(cal.RangoMenor * 100 AS DECIMAL(6,3))) AS VARCHAR) + ' a ' + CAST(CONVERT(DOUBLE PRECISION, CAST(cal.RangoMayor * 100 AS DECIMAL(6,3))) AS VARCHAR) + ' %' AS PorcentajeReservas,
	'HIPOTECARIO',
	0 ImporteSemanal,
	0 ImporteQuincenal,
	0 ReservaSemanal,
	0 ReservaQuincenal,
	'HIPOTECARIO'
FROM dbo.SICC_CalificacionHipotecario2011 cal
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON hrv.IdCalificacion = cal.IdCalificacion
GROUP BY cal.Codigo, cal.RangoMenor, cal.RangoMayor;
GO
