SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO ASIGNADO POR LA ENTIDAD" deberá ser único e irrepetible dentro del archivo que se reporta.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM (
	SELECT CodigoCredito
	FROM dbo.RW_R04H0492
	GROUP BY CodigoCredito
	HAVING COUNT(CodigoCredito) > 1
) AS tb;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
