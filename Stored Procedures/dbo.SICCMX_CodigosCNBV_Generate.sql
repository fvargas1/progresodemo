SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CodigosCNBV_Generate]
AS
DECLARE @CodigoInstitucion VARCHAR(50);
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;

SELECT @CodigoInstitucion=[Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @FechaPeriodo=Fecha, @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;


INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 [_Consecutivo],
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT
 tab.NumeroLinea,
 tab.CodigoCredito,
 tab.CodigoPersona,
 dbo.fnCNBV_Comercial(@CodigoInstitucion, @FechaPeriodo, UPPER(tab.RFC), (tab.Consecutivo + (DENSE_RANK() OVER(PARTITION BY tab.CodigoPersona ORDER BY tab.NumeroLinea) - 1))),
 tab.Consecutivo + (DENSE_RANK() OVER(PARTITION BY tab.CodigoPersona ORDER BY tab.NumeroLinea) - 1),
 GETDATE(),
 @IdPeriodo,
 tab.MontoLinea,
 tab.IdMoneda,
 tab.IdTipoProducto,
 tab.IdTipoOperacion,
 tab.IdTasaReferencia,
 tab.FechaVencimiento
FROM (
SELECT DISTINCT
 lin.Codigo AS NumeroLinea,
 cre.Codigo AS CodigoCredito,
 per.Codigo AS CodigoPersona,
 ISNULL(con.Seq,1) AS Consecutivo,
 per.RFC,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial AS IdTipoProducto,
 lin.TipoOperacion AS IdTipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea AS FechaVencimiento
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN (
 SELECT cn.CodigoPersona, ISNULL(MAX(cn.[_Consecutivo]),1) AS Seq
 FROM dbo.SICCMX_CreditoCNBV cn
 GROUP BY cn.CodigoPersona
) AS con ON per.Codigo = con.CodigoPersona
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE ta.GeneraCNBV = 1 AND cnbv.NumeroLinea IS NULL AND LEN(ISNULL(lin.CodigoCNBV,'')) = 0
) AS tab
UNION
SELECT
 lin.Codigo AS NumeroLinea,
 cre.Codigo AS CodigoCredito,
 per.Codigo AS CodigoPersona,
 lin.CodigoCNBV,
 RIGHT(lin.CodigoCNBV,3),
 GETDATE(),
 @IdPeriodo,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial AS IdTipoProducto,
 lin.TipoOperacion AS IdTipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea AS FechaVencimiento
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE ta.GeneraCNBV = 1 AND cnbv.NumeroLinea IS NULL AND LEN(ISNULL(lin.CodigoCNBV,'')) > 0;


-- EL CONSECUTIVO EN EL CODIGO CNBV NO PUEDE SER '000'
UPDATE cnbv
SET CNBV = SUBSTRING(cnbv.CNBV,1,LEN(cnbv.CNBV)-3) + RIGHT('000' + CAST(cnbv.[_Consecutivo] + 1 AS VARCHAR(10)),3),
 [_Consecutivo] = cnbv.[_Consecutivo] + 1
FROM dbo.SICCMX_CreditoCNBV cnbv
INNER JOIN (
 SELECT cn.CodigoPersona, cn.[_Consecutivo]
 FROM dbo.SICCMX_CreditoCNBV cn
 WHERE RIGHT(cn.CNBV,3) = '000'
) con ON cnbv.CodigoPersona = con.CodigoPersona AND cnbv.[_Consecutivo] >= con.[_Consecutivo];


-- ACTUALIZAMOS CODIGOS CNBV DE NUEVAS DISPOSICIONES CUYA LINEA YA CUENTA CON CODIGO CNBV
INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 [_Consecutivo],
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT DISTINCT
 lin.Codigo,
 cre.Codigo,
 per.Codigo,
 cc.CNBV,
 cc.[_Consecutivo],
 GETDATE(),
 @IdPeriodo,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial AS IdTipoProducto,
 lin.TipoOperacion AS IdTipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea AS FechaVencimiento
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
CROSS APPLY(
 SELECT TOP 1 cnbvLin.CNBV, cnbvLin.[_Consecutivo]
 FROM dbo.SICCMX_CreditoCNBV cnbvLin
 WHERE cnbvLin.NumeroLinea = lin.Codigo
) AS cc
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON cre.Codigo = cnbv.CodigoCredito
WHERE cnbv.IdCreditoCNBV IS NULL;


-- INSERTAMOS CODIGOS CNBV DE LINEAS QUE NO SE HAN DISPUESTO
INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 [_Consecutivo],
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT
 tab.NumeroLinea,
 tab.CodigoCredito,
 tab.CodigoPersona,
 dbo.fnCNBV_Comercial(@CodigoInstitucion, @FechaPeriodo, UPPER(tab.RFC), (tab.Consecutivo + (DENSE_RANK() OVER(PARTITION BY tab.CodigoPersona ORDER BY tab.NumeroLinea) - 1))),
 tab.Consecutivo + (DENSE_RANK() OVER(PARTITION BY tab.CodigoPersona ORDER BY tab.NumeroLinea) - 1),
 GETDATE(),
 @IdPeriodo,
 tab.MontoLinea,
 tab.IdMoneda,
 tab.IdTipoProducto,
 tab.IdTipoOperacion,
 tab.IdTasaReferencia,
 tab.FechaVencimiento
FROM (
SELECT DISTINCT
 lin.Codigo AS NumeroLinea,
 NULL AS CodigoCredito,
 per.Codigo AS CodigoPersona,
 ISNULL(con.Seq,1) AS Consecutivo,
 per.RFC,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial AS IdTipoProducto,
 lin.TipoOperacion AS IdTipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea AS FechaVencimiento
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN (
 SELECT cn.CodigoPersona, CASE WHEN MAX(cn.[_Consecutivo]) IS NULL THEN 1 ELSE MAX(cn.[_Consecutivo]) + 1 END AS Seq
 FROM dbo.SICCMX_CreditoCNBV cn
 GROUP BY cn.CodigoPersona
) AS con ON per.Codigo = con.CodigoPersona
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv ON lin.Codigo = cnbv.NumeroLinea
WHERE ta.GeneraCNBV = 1 AND cnbv.NumeroLinea IS NULL AND LEN(ISNULL(lin.CodigoCNBV,'')) = 0
) AS tab;
GO
