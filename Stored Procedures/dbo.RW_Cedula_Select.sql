SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Cedula_Select]
	@IdCedula BIGINT
AS
SELECT
	vw.IdCedula,
	vw.Nombre,
	vw.Codename,
	vw.ReportFolder,
	vw.Reportname,
	vw.Parameters,
	vw.OutputParams,
	vw.Prefix,
	vw.Activo
FROM dbo.RW_VW_Cedula vw
WHERE vw.IdCedula = @IdCedula;
GO
