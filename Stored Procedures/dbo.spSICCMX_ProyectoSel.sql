SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoSel]
	@IdPersona BIGINT
AS
SELECT
	ISNULL(p.Nombre,'') AS Nombre,
	ISNULL(p.Descripcion,'') AS Descripcion,
	ISNULL(pc.Sobrecosto,0) AS Sobrecosto,
	ISNULL(pc.MontoCubierto,0) AS MontoCubierto,
	ISNULL(pc.ExposicionSobrecosto,0) AS ExposicionSobrecosto,
	CAST(ISNULL(pc.PorcenajeSobrecosto,0) * 100 AS DECIMAL(8,2)) AS PorcenajeSobrecosto,
	pc.IdCalificacionSobrecosto,
	0 AS PorcentajeRetraso,
	NULL AS IdCalificacionRetraso,
	CAST(ISNULL(pc.PorcentajeCorrida,0) * 100 AS DECIMAL(8,2)) AS PorcentajeCorrida,
	pc.IdCalificacionCorrida,
	pc.IdCalificacionFinal,
	pc.VpTotal,
	pc.UpAcumulada,
	calSC.Codigo AS GradoSobrecosto,
	calCR.Codigo AS GradoCorrida,
	calFN.Codigo AS GradoFinal,
	pc.IdEtapa AS Etapa,
	CASE WHEN pc.IdEtapa=1 THEN ISNULL(pc.ReservaConstruccion,0) ELSE ISNULL(pc.ReservaOperacion,0) END AS Reservas,
	'' AS PorcentajeMaximo,
	per.Codigo AS PersonaCodigo,
	per.Nombre AS PersonaNombre,
	p.IdProyecto
FROM dbo.SICCMX_Proyecto p
INNER JOIN SICCMX_ProyectoCalificacion pc ON p.IdProyecto = pc.IdProyecto
INNER JOIN SICCMX_Persona per ON per.IdPersona = p.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calSC ON pc.IdCalificacionSobrecosto = calSC.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCR ON pc.IdCalificacionCorrida = calCR.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calFN ON pc.IdCalificacionFinal = calFN.IdCalificacion
WHERE p.IdPersona = @IdPersona;
GO
