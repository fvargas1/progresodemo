SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_116_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Operación (cve_tipo_operacion) es igual a 254 (Crédito Puente), el Destino del Crédito (cve_destino_credito) deberá ser: 330, 332, 333, 334, 338, 430, 432, 433, 434 ó 438.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(TipoOperacion,'') = '254' AND ISNULL(DestinoCredito,'') NOT IN ('330','332','333','334','338','430','432','433','434','438')

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
