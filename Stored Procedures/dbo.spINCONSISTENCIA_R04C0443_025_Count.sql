SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_025_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá anotar el monto pagado efectivamente por el acreditado. En caso de que no se hayan efectuado pagos, se anotará 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoPagado,''),'-1') AS DECIMAL(23,2)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
