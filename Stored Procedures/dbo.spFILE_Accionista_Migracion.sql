SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Accionista_Migracion]
AS
UPDATE acc
SET Nombre = LTRIM(RTRIM(f.NombreAccionista)),
	Porcentaje = NULLIF(f.Porcentaje,'')
FROM dbo.SICCMX_Accionista acc
INNER JOIN dbo.SICCMX_Persona per ON acc.IdPersona = per.IdPersona
INNER JOIN dbo.FILE_Accionista f ON per.Codigo = LTRIM(f.Deudor)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Accionista (
	Nombre,
	Porcentaje,
	IdPersona
)
SELECT
	LTRIM(RTRIM(f.NombreAccionista)),
	NULLIF(f.Porcentaje,''),
	per.IdPersona
FROM dbo.FILE_Accionista f
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.Deudor)
LEFT OUTER JOIN dbo.SICCMX_Accionista acc ON per.IdPersona = acc.IdPersona
WHERE acc.IdPersona IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
