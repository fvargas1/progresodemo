SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_022]
AS

BEGIN

-- El factor de ajuste (hfx) de la garantía real financiera debe estar entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	Hfx
FROM dbo.RW_VW_R04C0471_INC
WHERE CAST(ISNULL(NULLIF(Hfx,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
