SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_SaveIteration]
	@idUser AS INTEGER,
	@password AS BAJAWARE_utBinary
AS
DECLARE @iterCount INTEGER, @maxDate DATETIME;

SELECT @maxDate = MIN(FechaCambio), @iterCount = COUNT(IdPasswordIteration) FROM dbo.BAJAWARE_UserPasswordIteration WHERE IdUser=@idUser;

IF(@iterCount > 5)
BEGIN
 UPDATE dbo.BAJAWARE_UserPasswordIteration 
 SET FechaCambio = GETDATE(),
 OldPassword = @password
 WHERE IdUser=@idUser AND FechaCambio=@maxDate;
END
ELSE
BEGIN
 INSERT INTO dbo.BAJAWARE_UserPasswordIteration (IdUser, OldPassword, FechaCambio)
 VALUES (@idUser, @password, GETDATE());
END
GO
