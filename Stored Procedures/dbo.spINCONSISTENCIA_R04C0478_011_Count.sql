SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_011_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se revisará que la clave de la institución dentro del ID sea correcta.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @IdInstitucion INT;

SELECT @IdInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE SUBSTRING(CodigoCreditoCNBV,2,6) <> @IdInstitucion;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
