SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Personales]
AS
-- GARANTIAS PERSONALES
UPDATE can
SET
 C = ca.Monto,
 PrctCobSinAju = ca.Porcentaje,
 PrctCobAjust = ca.Porcentaje,
 MontoCobAjust = ca.Monto,
 [PI] = av.PIAval
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_CreditoAval ca ON can.IdCredito = ca.IdCredito AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Aval av ON ca.IdAval = av.IdAval AND av.Aplica=1
INNER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo='GP';
GO
