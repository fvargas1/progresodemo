SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_Moneda]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	CAST(cat.Codigo AS INT) AS Codigo,
	cat.Nombre,
	cat.CodigoCNBV,
	cat.CodigoCNBV_Hipo,
	cat.Codigo_Consumo,
	cat.CodigoISO,
	CAST(cat.AplicaTC AS INT) AS Activo,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_Moneda cat
LEFT OUTER JOIN dbo.FILE_Credito_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'Moneda' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'')+'|'+ISNULL(cat.CodigoCNBV,'')
	+'|'+ISNULL(cat.CodigoCNBV_Hipo,'')+'|'+ISNULL(cat.Codigo_Consumo,'')+'|'+ISNULL(cat.CodigoISO,'') LIKE '%' + @filtro + '%'
ORDER BY CAST(cat.Codigo AS INT);
GO
