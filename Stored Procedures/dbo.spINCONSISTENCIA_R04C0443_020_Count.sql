SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_020_Count]


 @IdInconsistencia BIGINT


AS


BEGIN


-- Se validará que los saldos del principal al inicio del periodo de cada crédito comercial que se reporta,


-- sea el mismo que se reportó como saldo del principal al final de periodo en el mes anterior para el mismo crédito que se indica.





DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;





DECLARE @count INT;





SELECT @count = COUNT(DISTINCT r.CodigoCredito)


FROM dbo.RW_R04C0443 r


OUTER APPLY (SELECT TOP 1 vw.SaldoFinal FROM dbo.SICCMX_VW_Datos_0442 vw WHERE vw.CodigoCreditoCNBV = r.CodigoCreditoCNBV AND vw.NumeroDisposicion = r.NumeroDisposicion ORDER BY vw.IdPeriodoHistorico DESC) AS hst


WHERE hst.SaldoFinal IS NOT NULL AND CAST(ISNULL(NULLIF(r.SaldoInicial,''),'0') AS DECIMAL(23,2)) <> CAST(ISNULL(NULLIF(hst.SaldoFinal,''),'0') AS DECIMAL(23,2));





INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());





SELECT @count;





END
GO
