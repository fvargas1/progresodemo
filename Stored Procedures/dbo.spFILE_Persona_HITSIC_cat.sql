SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Persona_HITSIC_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Persona_HITSIC_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET errorCatalogo = 1
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_HitSic hs ON LTRIM(f.HITSIC) = hs.Codigo
WHERE hs.IdHitSic IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Persona_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoCliente,
	'HITSIC',
	f.HITSIC,
	1,
	@Detalle
FROM dbo.FILE_Persona f
LEFT OUTER JOIN dbo.SICC_HitSic hs ON LTRIM(f.HITSIC) = hs.Codigo
WHERE hs.IdHitSic IS NULL;
GO
