SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_TipoCambio_List_ByPeriodo]
	@IdPeriodo INT
AS
SELECT
 tc.IdTipoCambio,
 m.IdMoneda,
 m.Nombre AS MonedaNombre,
 p.IdPeriodo,
 p.Fecha AS PeriodoNombre,
 tc.Valor
FROM dbo.SICC_Periodo p
INNER JOIN dbo.SICC_TipoCambio tc ON p.IdPeriodo = tc.IdPeriodo
INNER JOIN dbo.SICC_Moneda m ON tc.IdMoneda = m.IdMoneda
WHERE p.IdPeriodo = @IdPeriodo AND m.AplicaTC = 1
ORDER BY m.MonedaOficial DESC, m.Nombre;
GO
