SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_046]

AS



BEGIN



-- Validar que el Porcentaje de Pagos en Tiempo con Instituciones Financieras NO Bancarias en los Últimos 12 meses (dat_porcent_pgo_no_bcos) sea >= 0 pero <= 100



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoInstNoBanc

FROM dbo.RW_VW_R04C0470_INC

WHERE CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100 AND CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) <> -99;



END
GO
