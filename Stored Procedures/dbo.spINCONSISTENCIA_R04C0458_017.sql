SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_017]
AS
BEGIN
-- El Nombre del Acreditado (dat_nombre) no debe incluir las terminaciones SA DE CV, entre otras.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor
FROM dbo.RW_VW_R04C0458_INC
WHERE UPPER(NombrePersona) LIKE '%SC DE RL DE CV%' OR
UPPER(NombrePersona) LIKE '%S.C. DE R.L. DE C.V.%' OR
UPPER(NombrePersona) LIKE '%S.A. DE C.V.%' OR
UPPER(NombrePersona) LIKE '%SA DE CV%' OR
UPPER(NombrePersona) LIKE '%A.C.%' OR
UPPER(NombrePersona) LIKE '%S.A.%';

END
GO
