SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Pond_Riesgo]
AS
UPDATE inf
SET PonderadorRiesgo = ISNULL(rel.PonderadorRiesgo,1)
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_Credito cre ON inf.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICC_Rel_RCCalif_TipoPers rel ON pInfo.IdTipoPersona = rel.IdTipoPersona AND pInfo.IdRCCalificacion = rel.IdRCCalificacion
WHERE inf.PonderadorRiesgo IS NULL;

UPDATE inf
SET PonderadorRiesgo = 1.15
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_Credito cre ON inf.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado dr ON per.IdDeudorRelacionado = dr.IdDeudorRelacionado
WHERE ISNULL(dr.Codigo,'8') <> '8';
GO
