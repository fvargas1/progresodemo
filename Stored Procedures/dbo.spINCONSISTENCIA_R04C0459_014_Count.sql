SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_014_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El campo SEVERIDAD NO CUBIER GTIA PERSO debe ser mayor o igual a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(SPExpuesta,''),'-1') AS DECIMAL(10,6)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
