SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_ConsDesc_Reservas_Hipotecario]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
TRUNCATE TABLE dbo.SICCMX_Reservas_Variacion_Hipotecario;

INSERT INTO dbo.SICCMX_Reservas_Variacion_Hipotecario (IdHipotecario)
SELECT DISTINCT IdHipotecario
FROM dbo.SICCMX_Hipotecario;

-- CALCULAMOS LA VARIACION EN LAS RESERVAS EN COMPARACION CON EL MES ANTERIOR
UPDATE res
SET ReservaCalConsDesc = hrv.Reserva - ISNULL(hst.Reserva,0),
	ReservaAdicConsDesc = ISNULL(inf.ReservasAdicionales,0) - ISNULL(hst.ReservasAdicionales,0),
	ReservaPeriodoAnt = ISNULL(hst.Reserva,0),
	ReservaAdicPeriodoAnt = ISNULL(hst.ReservasAdicionales,0)
FROM dbo.SICCMX_Reservas_Variacion_Hipotecario res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario hip ON hrv.IdHipotecario = hip.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioInfo inf ON hip.IdHipotecario = inf.IdHipotecario
OUTER APPLY (
	SELECT TOP 1 hhrv.Reserva, hinf.ReservasAdicionales
	FROM Historico.SICCMX_Hipotecario_Reservas_Variables hhrv
	INNER JOIN Historico.SICCMX_HipotecarioInfo hinf ON hhrv.IdPeriodoHistorico = hinf.IdPeriodoHistorico AND hhrv.Hipotecario = hinf.Hipotecario
	INNER JOIN dbo.SICC_PeriodoHistorico ph ON hhrv.IdPeriodoHistorico = ph.IdPeriodoHistorico
	WHERE hip.Codigo = hhrv.Hipotecario AND ph.Fecha < @FechaPeriodo AND ph.Activo = 1
	ORDER BY ph.Fecha DESC
) AS hst;
GO
