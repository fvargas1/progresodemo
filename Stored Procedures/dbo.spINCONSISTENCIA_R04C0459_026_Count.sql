SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_026_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo debe ser diferente de 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT NumeroDisposicion)
FROM dbo.RW_VW_R04C0459_INC
WHERE ISNULL(NULLIF(InstitucionFondeo,''),'0') = '0' AND CAST(ISNULL(NULLIF(MontoBancaDesarrollo,''),'0') AS DECIMAL(23,2)) <> 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
