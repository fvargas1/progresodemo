SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_097]
AS

BEGIN

-- Validar que la Forma de Disposicion corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.FormaDisposicion
FROM dbo.RW_VW_R04C0473_INC rep
LEFT OUTER JOIN dbo.SICC_DisposicionCredito disp ON ISNULL(rep.FormaDisposicion,'') = disp.CodigoCNBV
WHERE disp.IdDisposicion IS NULL;

END


GO
