SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0481_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0481';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0481;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0481 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, CodigoPersona, RFC, NombrePersona, PrctExpuesto,
 SP, EI_SinGarantia, [PI], NumeroGarRealFin, PrctGarRealFin, He, Hfx, Hc, ValorGarRealFin, SP_GarRealFin, EI_GarRealFin, NumeroGarRealNoFin,
 PrctGarRealNoFin, ValorDerechosCobro, ValorBienesInmuebles, ValorBienesMuebles, ValorFidPartFed, ValorFidIngProp, ValorOtrasGar, SP_DerechosCobro,
 SP_BienesInmuebles, SP_BienesMuebles, SP_FidPartFed, SP_FidIngProp, SP_OtrasGar, SP_GarRealNoFin, NumeroGarPersonales, PrctGarPersonales,
 NombreAval, PrctAval, TipoAval, RFCAval, TipoGarante, PI_Garante, ValuacionDerCredito, MonedaGarPersonal, NombreGaranteECPM, NombreGarantePP,
 PrctECPM, PrctPP, MontoPP
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0481',
 cnbv.CNBV AS CodigoCreditoCNBV,
 cre.NumeroLinea AS CodigoCredito,
 cre.CodigoCredito AS NumeroDisposicion,
 per.Codigo AS CodigoPersona,
 per.RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 canExp.PrctCobAjust * 100 AS PrctExpuesto,
 canExp.SeveridadCorresp * 100 AS SP,
 canExp.MontoCobAjust AS EI_SinGarantia,
 canExp.[PI] * 100 AS [PI],
 garantias.NumeroGarRealFin AS NumeroGarRealFin,
 canFin.PrctCobAjust * 100 AS PrctGarRealFin,
 canFin.He * 100 AS He,
 canFin.Hfx * 100 AS Hfx,
 canFin.Hc * 100 AS Hc,
 garantias.ValorGarRealFin AS ValorGarRealFin,
 canFin.SeveridadCorresp * 100 AS SP_GarRealFin,
 canFin.EI_Ajust AS EI_GarRealFin,
 garantias.NumeroGarRealNoFin AS NumeroGarRealNoFin,
 garSP.Porcentaje * 100 AS PrctGarRealNoFin,
 garantias.ValorDerechosCobro AS ValorDerechosCobro,
 garantias.ValorBienesInmuebles AS ValorBienesInmuebles,
 garantias.ValorBienesMuebles AS ValorBienesMuebles,
 garantias.ValorFidPartFed AS ValorFidPartFed,
 garantias.ValorFidIngProp AS ValorFidIngProp,
 garantias.ValorOtrasGar AS ValorOtrasGar,
 garSP.SP_DerechosCobro * 100 AS SP_DerechosCobro,
 garSP.SP_BienesInmuebles * 100 AS SP_BienesInmuebles,
 garSP.SP_BienesMuebles * 100 AS SP_BienesMuebles,
 garSP.SP_FidPartFed * 100 AS SP_FidPartFed,
 garSP.SP_FidIngProp * 100 AS SP_FidIngProp,
 garSP.SP_OtrasGar * 100 AS SP_OtrasGar,
 garSP.SP_GarRealNoFin * 100 AS SP_GarRealNoFin,
 garPer.NumeroGarPersonales AS NumeroGarPersonales,
 garPer.PrctGarPersonales * 100 AS PrctGarPersonales,
 NULLIF(garPer.NombreAval,'') AS NombreAval,
 garPer.PrctAval * 100 AS PrctAval,
 NULLIF(garPer.TipoAval,'') AS TipoAval,
 NULLIF(garPer.RFCAval,'') AS RFCAval,
 garPer.TipoGarante AS TipoGarante,
 garPer.PI_Garante * 100 AS PI_Garante,
 '' AS ValuacionDerCredito,
 garPer.IdMoneda AS MonedaGarPersonal,
 NULLIF(gPM.NomGar,'') AS NombreGaranteECPM,
 NULLIF(gPP.NomGar,'') AS NombreGarantePP,
 gPM.Porcentaje * 100 AS PrctECPM,
 gPP.Porcentaje * 100 AS PrctPP,
 gPP.Monto AS MontoPP
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pInfo.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Anexo22 anx ON cre.IdPersona = anx.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta canExp ON crv.IdCredito = canExp.IdCredito AND canExp.EsDescubierto = 1
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta canFin ON crv.IdCredito = canFin.IdCredito AND canFin.IdTipoGarantia IS NULL AND ISNULL(canFin.EsDescubierto,0) <> 1
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasReportes garantias ON cre.IdCredito = garantias.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasSPReportes garSP ON cre.IdCredito = garSP.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasPersReportes garPer ON cre.IdCredito = garPer.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PM gPM ON cre.IdCredito = gPM.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PP gPP ON cre.IdCredito = gPP.IdCredito
WHERE met.Codigo = '22' AND cre.MontoValorizado > 0 AND ISNULL(anx.OrgDescPartidoPolitico,0) = 1;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0481 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
