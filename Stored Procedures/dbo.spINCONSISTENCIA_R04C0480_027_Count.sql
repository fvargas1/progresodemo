SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_027_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Total de Pagos al Infonavit en el Último Bimestre (dat_tot_pagos_infonavit) es >= 2400 entonces el
-- Puntaje Asignado por el Total de Pagos al Infonavit en el Último Bimestre (cve_ptaje_pgos_infonav) debe ser = 69

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PagosInfonavit AS DECIMAL) >= 2400 AND ISNULL(P_PagosInfonavit,'') <> '69';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
