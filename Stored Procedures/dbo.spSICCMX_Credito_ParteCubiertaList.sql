SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Credito_ParteCubiertaList]
	@IdCredito BIGINT
AS
SELECT
	'' AS Codigo,
	cal.Codigo AS Calificacion,
	crv.EI_Cubierta AS Monto,
	crv.ReservaCubierta AS Reserva,
	crv.PorCubierto AS Porcentaje,
	'' AS IdGarantia,
	'' AS IdAval,
	0 AS Manual,
	crv.IdCredito
FROM dbo.SICCMX_Credito_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Credito cre ON crv.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifCubierta = cal.IdCalificacion
WHERE crv.IdCredito = @IdCredito;
GO
