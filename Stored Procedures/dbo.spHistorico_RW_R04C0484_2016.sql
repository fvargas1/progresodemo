SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0484_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0484_2016);

DELETE FROM Historico.RW_R04C0484_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0484_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, TamanoAcreditado, VentasNetasTotales, NumeroEmpleados, CodigoCreditoCNBV, ClasContable, ConcursoMercantil, FechaDisposicion,
	FechaVencDisposicion, Moneda, NumeroDisposicion, SaldoInicial, TasaInteres, TasaInteresDisp, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa, MontoDispuesto,
	MontoPagoExigible, MontoCapitalPagado, MontoInteresPagado, MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado, MontoCondonacion, MontoQuitas, MontoBonificado,
	MontoDescuentos, MontoAumentosDec, SaldoFinal, SaldoCalculoInteres, DiasCalculoInteres, MontoInteresAplicar, SaldoInsoluto, SituacionCredito, DiasAtraso, FechaUltimoPago,
	MontoBancaDesarrollo, InstitucionFondeo, Etapa, FechaInicioOper, SobreCosto, MontoCubiertoTerceros, MontoExposicion, PrctExposicion, PrctDescubierto, FlujoEfctivo,
	TasaDescuento, Deficit, PrctDescubiertoCorrida, PrctProvisionamiento, GradoRiesgo, ReservasTotales, ReservasMI, SP_MI, EI_MI, PI_MI, Mitigante, GrupoRiesgo, FactorConversion,
	ExpMitigantes, ExpNetaReservas, TablaAdeudo, GradoRiesgoME, EscalaCalificacion, AgenciaCalificadora, Calificacion, PonderadorRiesgo, RequerimientoCapital, EnfoqueBasico,
	SP_ReqCap_MI, PI_ReqCap_MI, EI_ReqCap_MI, Vencimiento, Correlacion, Ponderador_ReqCap_MI, ReqCap_MI
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	TamanoAcreditado,
	VentasNetasTotales,
	NumeroEmpleados,
	CodigoCreditoCNBV,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	MontoBancaDesarrollo,
	InstitucionFondeo,
	Etapa,
	FechaInicioOper,
	SobreCosto,
	MontoCubiertoTerceros,
	MontoExposicion,
	PrctExposicion,
	PrctDescubierto,
	FlujoEfctivo,
	TasaDescuento,
	Deficit,
	PrctDescubiertoCorrida,
	PrctProvisionamiento,
	GradoRiesgo,
	ReservasTotales,
	ReservasMI,
	SP_MI,
	EI_MI,
	PI_MI,
	Mitigante,
	GrupoRiesgo,
	FactorConversion,
	ExpMitigantes,
	ExpNetaReservas,
	TablaAdeudo,
	GradoRiesgoME,
	EscalaCalificacion,
	AgenciaCalificadora,
	Calificacion,
	PonderadorRiesgo,
	RequerimientoCapital,
	EnfoqueBasico,
	SP_ReqCap_MI,
	PI_ReqCap_MI,
	EI_ReqCap_MI,
	Vencimiento,
	Correlacion,
	Ponderador_ReqCap_MI,
	ReqCap_MI
FROM dbo.RW_R04C0484_2016
WHERE IdReporteLog = @IdReporteLog;
GO
