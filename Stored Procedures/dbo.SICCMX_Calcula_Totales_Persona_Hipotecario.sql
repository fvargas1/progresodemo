SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Totales_Persona_Hipotecario]
AS
INSERT INTO dbo.SICCMX_PersonaCalificacion (IdPersona)
SELECT DISTINCT p.IdPersona
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_Hipotecario h ON p.IdPersona = h.IdPersona
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON h.IdHipotecario = hrv.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_PersonaCalificacion pc ON p.IdPersona = pc.IdPersona
WHERE pc.IdPersona IS NULL;

UPDATE pc
SET EI_Hipotecario = tot.EI_Total,
 Reserva_Hipotecario = tot.ReservaFinal,
 PrctReserva_Hipotecario = CASE WHEN tot.EI_Total > 0 THEN tot.ReservaFinal / tot.EI_Total ELSE NULL END
FROM dbo.SICCMX_PersonaCalificacion pc
INNER JOIN (
 SELECT pc.IdPersona, CAST(SUM(ISNULL(hrv.E,0)) AS DECIMAL(23,2)) AS EI_Total, CAST(SUM(ISNULL(hrv.Reserva,0)) AS DECIMAL(23,2)) AS ReservaFinal
 FROM dbo.SICCMX_PersonaCalificacion pc
 INNER JOIN dbo.SICCMX_Hipotecario h ON pc.IdPersona = h.IdPersona
 INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON h.IdHipotecario = hrv.IdHipotecario
 GROUP BY pc.IdPersona
) tot ON pc.IdPersona = tot.IdPersona;

UPDATE pc
SET IdCalificacion_Hipotecario = cal.IdCalificacion
FROM dbo.SICCMX_PersonaCalificacion pc
CROSS APPLY (
	SELECT TOP 1 pc2.IdPersona, cal.IdCalificacion
	FROM dbo.SICCMX_PersonaCalificacion pc2
	INNER JOIN dbo.SICCMX_Hipotecario hip ON pc2.IdPersona = hip.IdPersona
	INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario
	LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON pre.IdMetodologia = cal.IdMetodologia AND pc2.PrctReserva_Hipotecario BETWEEN cal.RangoMenor AND cal.RangoMayor
	WHERE pc.IdPersona = pc2.IdPersona
	ORDER BY cal.Codigo DESC
) AS cal;
GO
