SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_073_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Vaidar que la Periodicidad Pagos Interes corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON ISNULL(rep.PeriodicidadPagosInteres,'') = perInt.CodigoCNBV
WHERE perInt.IdPeriodicidadInteres IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
