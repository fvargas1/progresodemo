SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_035]
AS
BEGIN
-- Validar que el Tipo de Operacion corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.TipoOperacion
FROM dbo.RW_VW_R04C0453_INC rep
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpo ON ISNULL(rep.TipoOperacion,'') = tpo.CodigoCNBV
WHERE tpo.IdTipoOperacion IS NULL;

END
GO
