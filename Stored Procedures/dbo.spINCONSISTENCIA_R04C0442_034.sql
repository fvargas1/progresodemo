SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_034]
AS

BEGIN

-- El monto de apoyo a la banca de desarrollo debe ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, MontoFondeo
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(MontoFondeo,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
