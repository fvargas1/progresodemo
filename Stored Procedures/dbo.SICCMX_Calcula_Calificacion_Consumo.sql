SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Calificacion_Consumo]
AS
-- CALCULAMOS LA PARTE CUBIERTA Y EXPUESTA
UPDATE crv
SET ECubierta = ISNULL(canCub.Monto,0),
	EExpuesta = canExp.MontoCobAjust,
	ReservaCubierta = ISNULL(canCub.Reserva,0),
	ReservaExpuesta = ISNULL(canExp.Reserva,0)
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo canExp ON crv.IdConsumo = canExp.IdConsumo AND canExp.EsDescubierto = 1
LEFT OUTER JOIN (
	SELECT tcan.IdConsumo, SUM(tcan.MontoCobAjust) AS Monto, SUM(tcan.Reserva) AS Reserva
	FROM dbo.SICCMX_Garantia_Canasta_Consumo tcan
	WHERE tcan.EsDescubierto IS NULL
	GROUP BY tcan.IdConsumo
) AS canCub ON crv.IdConsumo = canCub.IdConsumo;

-- CALCULAMOS LA RESERVA TOTAL
UPDATE dbo.SICCMX_Consumo_Reservas_Variables SET ReservaTotal = ReservaCubierta + ReservaExpuesta;
 
-- CALCULAMOS LOS PORCENTAJES DE RESERVA
UPDATE dbo.SICCMX_Consumo_Reservas_Variables
SET PorReservaExpuesta = CASE WHEN EExpuesta = 0 THEN 0 ELSE ReservaExpuesta / EExpuesta END,
	PorReservaCubierta = CASE WHEN ECubierta = 0 THEN 0 ELSE ReservaCubierta / ECubierta END;

-- CALCULAMOS EL PORCENTAJE DE RESERVA TOTAL
UPDATE dbo.SICCMX_Consumo_Reservas_Variables SET PorReserva = CASE WHEN E = 0 THEN 0 ELSE ReservaTotal / E END;

-- CALCULAMOS LA CALIFICACIONES
UPDATE crv
SET IdCalificacionExpuesta = calExp.IdCalificacion,
	IdCalificacionCubierta = calCub.IdCalificacion,
	IdCalificacion = cal.IdCalificacion
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON crv.IdConsumo = pre.IdConsumo
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON pre.IdMetodologia = calExp.IdMetodologia AND crv.PorReservaExpuesta BETWEEN calExp.RangoMenor AND calExp.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON pre.IdMetodologia = calCub.IdMetodologia AND crv.PorReservaCubierta BETWEEN calCub.RangoMenor AND calCub.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON pre.IdMetodologia = cal.IdMetodologia AND crv.PorReserva BETWEEN cal.RangoMenor AND cal.RangoMayor;
GO
