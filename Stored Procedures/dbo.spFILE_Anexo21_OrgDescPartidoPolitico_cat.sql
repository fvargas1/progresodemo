SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_OrgDescPartidoPolitico_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_OrgDescPartidoPolitico_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo21
SET errorCatalogo = 1
WHERE LTRIM(ISNULL(OrgDescPartidoPolitico,'')) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCliente,
 'OrgDescPartidoPolitico',
 OrgDescPartidoPolitico,
 2,
 @Detalle
FROM dbo.FILE_Anexo21
WHERE LTRIM(ISNULL(OrgDescPartidoPolitico,'')) NOT IN ('0','1');
GO
