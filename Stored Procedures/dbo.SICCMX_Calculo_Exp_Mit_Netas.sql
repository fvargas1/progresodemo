SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Exp_Mit_Netas]
AS
UPDATE inf
SET ExposicionAjuMit = cre.MontoValorizado
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON inf.IdCredito = cre.IdCredito;

UPDATE inf
SET ExposicionNeta = inf.ExposicionAjuMit - crv.ReservaFinal
FROM dbo.SICCMX_CreditoInfo inf
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON inf.IdCredito = crv.IdCredito;
GO
