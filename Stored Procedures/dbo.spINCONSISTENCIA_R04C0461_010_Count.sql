SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_010_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si se reporta algún número de garantías financieras, entonces el valor de éstas debe ser mayor a cero.
-- Si dat_num_gtias_reales_financ>0, entonces  dat_valor_conta_gtia_real_fin > 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0461_INC
WHERE CAST(NumeroGarRealFin AS DECIMAL) > 0 AND CAST(ValorGarRealFin AS DECIMAL) = 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
