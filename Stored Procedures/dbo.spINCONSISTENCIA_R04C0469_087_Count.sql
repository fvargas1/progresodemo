SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_087_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Moneda de la disposición es Pesos (cve_moneda = 0), validar que para el crédito (dat_id_credito_met_cnbv)
-- que se reporta, el Saldo del Principal al Inicio del Periodo (dat_saldo_princ_iniciot) actual sea IGUAL al Saldo
-- del Principal al Final del Periodo (dat_saldo_princ_finalt-1) del mes anterior.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
DECLARE @IdPeriodo INT;

SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Fecha = (SELECT DATEADD(MONTH,-1,Fecha) FROM dbo.SICC_Periodo WHERE Activo=1);

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0469_INC rep
INNER JOIN Historico.RW_VW_R04C0469_INC hst ON rep.CodigoCreditoCNBV = hst.CodigoCreditoCNBV AND hst.Periodo = @IdPeriodo
WHERE ISNULL(rep.Moneda,'') = '0' AND ISNULL(rep.SaldoInicial,'') <> ISNULL(hst.SaldoFinal,'')

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
