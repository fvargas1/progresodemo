SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_ListActivosFile]
AS

DECLARE @rutaFileConfig VARCHAR(300);

SELECT @rutaFileConfig = Value
FROM dbo.BAJAWARE_Config
WHERE Codename='RutaDatos';

SELECT
 vw.IdArchivo,
 vw.Nombre AS Nombre,
 vw.Codename,
 vw.JobCodename,
 vw.ErrorCodename,
 vw.LogCodename,
 vw.ClearLogCodename,
 vw.Position,
 vw.Activo,
 vw.ViewName,
 vw.ValidationStatus,
 vw.LastValidationExecuted,
 vw.InitCodename,
 1 AS existeFile
FROM dbo.MIGRACION_VW_Archivo vw
WHERE vw.Activo = 1
ORDER BY vw.Position
GO
