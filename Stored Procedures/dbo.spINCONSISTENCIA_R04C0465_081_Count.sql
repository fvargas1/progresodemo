SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_081_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por el Porcentaje de Pagos a Entidades Comerciales con 60 o más días de atraso en los últimos 12 meses (cve_ptaje_porc_pago_comercios) es = 57,
-- entonces el Porcentaje de Pagos a Entidades Comerciales con 60 o más días de atraso en los últimos 12 meses (dat_porcent_pago_comercios) debe ser = 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoEntComer,'') = '57' AND CAST(PorcPagoEntComer AS DECIMAL(10,6)) <> 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
