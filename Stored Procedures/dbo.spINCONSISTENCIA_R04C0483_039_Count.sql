SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_039_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Nombre de Grupo de Riesgo (dat_nombre_grupo_riesgo) diferente.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC
WHERE GrupoRiesgo IN (
	SELECT rep.GrupoRiesgo
	FROM dbo.RW_VW_R04C0483_INC rep
	INNER JOIN dbo.RW_VW_R04C0483_INC rep2 ON rep.RFC = rep2.RFC AND rep.GrupoRiesgo <> rep2.GrupoRiesgo
	GROUP BY rep.GrupoRiesgo, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
