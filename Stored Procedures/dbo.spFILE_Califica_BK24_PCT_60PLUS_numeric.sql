SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_BK24_PCT_60PLUS_numeric]  
AS  
DECLARE @Detalle VARCHAR(1000);  
DECLARE @Requerido BIT;  
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_BK24_PCT_60PLUS_numeric';  
  
IF @Requerido = 1  
BEGIN  
UPDATE dbo.FILE_Califica  
SET errorFormato = 1  
WHERE LEN(BK24_PCT_60PLUS)>0 AND (ISNUMERIC(ISNULL(BK24_PCT_60PLUS,'0')) = 0 OR BK24_PCT_60PLUS LIKE '%[^0-9 .]%');  
  
SET NOCOUNT ON;  
END  
  
INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)  
SELECT DISTINCT  
 CodigoDeudor,  
 'BK24_PCT_60PLUS',  
 BK24_PCT_60PLUS,  
 1,  
 @Detalle  
FROM dbo.FILE_Califica  
WHERE LEN(BK24_PCT_60PLUS)>0 AND (ISNUMERIC(ISNULL(BK24_PCT_60PLUS,'0')) = 0 OR BK24_PCT_60PLUS LIKE '%[^0-9 .]%');
GO
