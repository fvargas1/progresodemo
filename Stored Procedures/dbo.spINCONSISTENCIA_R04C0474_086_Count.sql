SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_086_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) no tenga mas de un ID Acreditado Asigando por la Institución (dat_id_acreditado_institucion).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC
WHERE CodigoPersona IN (
	SELECT rep.CodigoPersona
	FROM dbo.RW_VW_R04C0474_INC rep
	INNER JOIN dbo.RW_VW_R04C0474_INC rep2 ON rep.CodigoCreditoCNBV = rep2.CodigoCreditoCNBV AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.CodigoCreditoCNBV
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
