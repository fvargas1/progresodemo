SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_028]
AS

BEGIN

-- Si el Puntaje Asignado por el Monto Máximo de Crédito Ototgado por Inst Bcarias en los últimos 12 meses (cve_ptaje_mont_max_cred_otorg) es = 112,
-- entonces el Monto Máximo de Crédito Otorgado por Inst Bcarias en los últimos 12 meses (dat_monto_max_cred_bco_udis) debe ser >= 1000000

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MonMaxCred,
	P_MonMaxCred AS Puntos_MonMaxCred
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_MonMaxCred,'') = '112' AND CAST(MonMaxCred AS DECIMAL) < 1000000;

END


GO
