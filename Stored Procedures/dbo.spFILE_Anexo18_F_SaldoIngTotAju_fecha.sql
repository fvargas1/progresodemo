SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo18_F_SaldoIngTotAju_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo18_F_SaldoIngTotAju_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo18
SET errorFormato = 1
WHERE (LEN(F_SaldoIngTotAju)>0 AND ISDATE(F_SaldoIngTotAju) = 0) OR F_SaldoIngTotAju > @FechaPeriodo;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_SaldoIngTotAju',
 F_SaldoIngTotAju,
 1,
 @Detalle
FROM dbo.FILE_Anexo18
WHERE (LEN(F_SaldoIngTotAju)>0 AND ISDATE(F_SaldoIngTotAju) = 0) OR F_SaldoIngTotAju > @FechaPeriodo;
GO
