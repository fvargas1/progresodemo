SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_008]
AS

BEGIN

-- El campo PROBA INCUMPLI METOD INT debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PIInterna AS PI_MetodologiaInterna
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(PIInterna,''),'-1') AS DECIMAL(10,6)) < 0;

END
GO
