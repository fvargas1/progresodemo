SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_052]
AS

BEGIN

-- Validar que ID Acreditado Asignado por la Institución (dat_id_acreditado_instit) se haya reportado previamente en el reporte de altas R04-C 478 (Documento SITI 1855).

SELECT
 rep.CodigoPersona AS CodigoDeudor,
 REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor
FROM dbo.RW_VW_R04C0480_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0478_INC r78 ON rep.CodigoPersona = r78.CodigoPersona
LEFT OUTER JOIN Historico.RW_VW_R04C0478_INC hist ON rep.CodigoPersona = hist.CodigoPersona
WHERE hist.IdPeriodoHistorico IS NULL AND r78.CodigoPersona IS NULL;

END


GO
