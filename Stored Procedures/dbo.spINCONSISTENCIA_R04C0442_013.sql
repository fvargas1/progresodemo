SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_013]
AS

BEGIN

-- Se validará que la actividad económica exista en el catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.ActividadEconomica
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_ActividadEconomica cat ON ISNULL(r.ActividadEconomica,'') = cat.CodigoCNBV
WHERE cat.IdActividadEconomica IS NULL;

END
GO
