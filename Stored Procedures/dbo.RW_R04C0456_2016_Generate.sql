SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0456_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0456_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0456_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0456_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, NumeroDisposicion, SaldoInsolutoFinal, PrctExpuesto, SP_Expuesto, EI_SinGarantia, NumeroGarRealFin,
 PrctGarRealFin, He, Hfx, Hc, ValorGarRealFin, SP_GarRealFin, EI_GarRealFin, NumeroGarRealNoFin, PrctGarRealNoFin, ValorDerechosCobro, ValorBienesInmuebles,
 ValorBienesMuebles, ValorFidPartFed, ValorFidIngProp, ValorOtrasGar, FlujoEstimado, SP_DerechosCobro, SP_BienesInmuebles, SP_BienesMuebles, SP_FidPartFed,
 SP_FidIngProp, SP_OtrasGar, SP_GarRealNoFin, NumeroGarPersonales, PrctGarPersonales, NombreAval, PrctAval, MontoGarPersonal, TipoAval, RFCAval, TipoGarante,
 ValuacionDerCredito, MonedaGarPersonal, NombreGaranteECPM, NombreGarantePP, PrctECPM, PrctPP, MontoECPM, MontoPP, ID_ECPM, ID_PP, ReservasCalificacion,
 ReservasGarante, ReservasAcreditado, ReservasAdicionales, ReservasConstDesc, ReservasAdicConstDesc, [PI], PI_Garante, PI_Acreditado, SP, SP_Garante,
 SP_Acreditado, EI, EI_Garante, EI_Acreditado, CreditoReserva5Prct, CreditoSustPI, MesesSP100, GradoRiesgo, ReservasMI, SP_MI, EI_MI, PI_MI, Mitigante,
 GrupoRiesgo, FactorConversion, ExpMitigantes, ExpNetaReservas, TablaAdeudo, GradoRiesgoME, EscalaCalificacion, AgenciaCalificadora, Calificacion,
 PonderadorRiesgo, RequerimientoCapital, EnfoqueBasico, PI_ReqCap_MI, SP_ReqCap_MI, EI_ReqCap_MI, Vencimiento, Correlacion, Ponderador_ReqCap_MI, ReqCap_MI
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '456',
 cnbv.CNBV AS CodigoCreditoCNBV,
 cre.CodigoCredito AS NumeroDisposicion,
 cre.MontoValorizado AS SaldoInsolutoFinal,
 canExp.PrctCobAjust * 100 AS PrctExpuesto,
 canExp.SeveridadCorresp * 100 AS SP_Expuesto,
 canExp.MontoCobAjust AS EI_SinGarantia,
 garantias.NumeroGarRealFin AS NumeroGarRealFin,
 canFin.PrctCobAjust * 100 AS PrctGarRealFin,
 canFin.He * 100 AS He,
 canFin.Hfx * 100 AS Hfx,
 canFin.Hc * 100 AS Hc,
 garantias.ValorGarRealFin AS ValorGarRealFin,
 canFin.SeveridadCorresp * 100 AS SP_GarRealFin,
 canFin.EI_Ajust AS EI_GarRealFin,
 garantias.NumeroGarRealNoFin AS NumeroGarRealNoFin,
 garSP.Porcentaje * 100 AS PrctGarRealNoFin,
 garantias.ValorDerechosCobro AS ValorDerechosCobro,
 garantias.ValorBienesInmuebles AS ValorBienesInmuebles,
 garantias.ValorBienesMuebles AS ValorBienesMuebles,
 garantias.ValorFidPartFed AS ValorFidPartFed,
 garantias.ValorFidIngProp AS ValorFidIngProp,
 garantias.ValorOtrasGar AS ValorOtrasGar,
 inf.ExposicionIncumplimiento AS FlujoEstimado,
 garSP.SP_DerechosCobro * 100 AS SP_DerechosCobro,
 garSP.SP_BienesInmuebles * 100 AS SP_BienesInmuebles,
 garSP.SP_BienesMuebles * 100 AS SP_BienesMuebles,
 garSP.SP_FidPartFed * 100 AS SP_FidPartFed,
 garSP.SP_FidIngProp * 100 AS SP_FidIngProp,
 garSP.SP_OtrasGar * 100 AS SP_OtrasGar,
 garSP.SP_GarRealNoFin * 100 AS SP_GarRealNoFin,
 garPer.NumeroGarPersonales AS NumeroGarPersonales,
 garPer.PrctGarPersonales * 100 AS PrctGarPersonales,
 garPer.NombreAval AS NombreAval,
 garPer.PrctAval * 100 AS PrctAval,
 garPer.MontoGarPersonales AS MontoGarPersonal,
 garPer.TipoAval AS TipoAval,
 garPer.RFCAval AS RFCAval,
 garPer.TipoGarante AS TipoGarante,
 inf.ValMercDerivCred AS ValuacionDerCredito,
 garPer.IdMoneda AS MonedaGarPersonal,
 gPM.NomGar AS NombreGaranteECPM,
 gPP.NomGar AS NombreGarantePP,
 gPM.Porcentaje * 100 AS PrctECPM,
 gPP.Porcentaje * 100 AS PrctPP,
 gPM.Monto AS MontoECPM,
 gPP.Monto AS MontoPP,
 gPM.CodigoGarantia AS ID_ECPM,
 gPP.CodigoGarantia AS ID_PP,
 crv.ReservaFinal AS ReservasCalificacion,
 crv.ReservaCubierta_GarPer AS ReservasGarante,
 crv.ReservaExpuesta_GarPer AS ReservasAcreditado,
 adic.ReservasAdicTotal AS ReservasAdicionales,
 res.ReservaCalConsDesc AS ReservasConstDesc,
 res.ReservaAdicConsDesc AS ReservasAdicConstDesc,
 crv.PI_Total * 100 AS [PI],
 crv.PI_Cubierta * 100 AS PI_Garante,
 crv.PI_Expuesta * 100 AS PI_Acreditado,
 crv.SP_Total * 100 AS SP,
 crv.SP_Cubierta * 100 AS SP_Garante,
 crv.SP_Expuesta * 100 AS SP_Acreditado,
 crv.EI_Total AS EI,
 crv.EI_Cubierta_GarPer AS EI_Garante,
 crv.EI_Expuesta_GarPer AS EI_Acreditado,
 res.CreditoRes5Prct AS CreditoReserva5Prct,
 res.CreditoSustPI AS CreditoSustPI,
 res.MesesSP100 AS MesesSP100,
 cal.Codigo AS GradoRiesgo,
 '' AS ReservasMI,
 '' AS SP_MI,
 '' AS EI_MI,
 '' AS PI_MI,
 mit.CodigoCNBV AS Mitigante,
 gpo.CodigoCNBV AS GrupoRiesgo,
 inf.FactorConversion AS FactorConversion,
 inf.ExposicionAjuMit AS ExpMitigantes,
 inf.ExposicionNeta AS ExpNetaReservas,
 rcCal.TablaAdeudoCNBV AS TablaAdeudo,
 rcCal.GradoRiesgoCNBV AS GradoRiesgoME,
 rcCal.EscalaCNBV AS EscalaCalificacion,
 rcCal.AgenciaCNBV AS AgenciaCalificadora,
 rcCal.CodigoCNBV AS Calificacion,
 inf.PonderadorRiesgo * 100 AS PonderadorRiesgo,
 inf.ReqCapital AS RequerimientoCapital,
 '' AS EnfoqueBasico,
 '' AS PI_ReqCap_MI,
 '' AS SP_ReqCap_MI,
 '' AS EI_ReqCap_MI,
 '' AS Vencimiento,
 '' AS Correlacion,
 '' AS Ponderador_ReqCap_MI,
 '' AS ReqCap_MI
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON cre.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_Anexo18 anx ON cre.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON inf.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Reservas_Variacion res ON crv.IdCredito = res.IdCredito
INNER JOIN dbo.SICCMX_VW_Credito_ReservasAdicionales adic ON cre.IdCredito = adic.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta canExp ON cre.IdCredito = canExp.IdCredito AND canExp.EsDescubierto = 1
LEFT OUTER JOIN dbo.SICCMX_Garantia_Canasta canFin ON cre.IdCredito = canFin.IdCredito AND canFin.IdTipoGarantia IS NULL AND ISNULL(canFin.EsDescubierto,0) <> 1
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasReportes garantias ON cre.IdCredito = garantias.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasSPReportes garSP ON cre.IdCredito = garSP.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_GarantiasPersReportes garPer ON cre.IdCredito = garPer.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PM gPM ON cre.IdCredito = gPM.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_PP gPP ON cre.IdCredito = gPP.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_Mitigante mit ON inf.IdMitigante = mit.IdMitigante
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgo gpo ON pInfo.IdRCGrupoRiesgo = gpo.IdRCGrupoRiesgo
LEFT OUTER JOIN dbo.SICCMX_VW_Credito_RCCalificacion vwCreCal ON cre.IdCredito = vwCreCal.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_RCCalificacion rcCal ON vwCreCal.IdRCCalificacion = rcCal.IdRCCalificacion
WHERE met.Codigo = '18' AND cre.MontoValorizado > 0;
EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0456_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
