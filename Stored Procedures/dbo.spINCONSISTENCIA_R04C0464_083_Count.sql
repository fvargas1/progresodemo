SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_083_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Situacion del Credito corresponda a Catalogo de CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC rep
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON ISNULL(rep.SituacionCredito,'') = sit.Codigo
WHERE sit.IdSituacionCredito IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
