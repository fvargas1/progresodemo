SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_069]
AS

BEGIN

-- Un mismo RFC Acreditado (dat_rfc) no debe tener más de un Nombre de Acreditado (dat_nombre) diferentes.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	RFC
FROM dbo.RW_VW_R04C0473_INC
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_VW_R04C0473_INC rep
	INNER JOIN dbo.RW_VW_R04C0473_INC rep2 ON rep.RFC = rep2.RFC AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.RFC
);

END


GO
