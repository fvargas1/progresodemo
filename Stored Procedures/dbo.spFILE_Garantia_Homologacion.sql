SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Homologacion]

AS

DECLARE @Campo VARCHAR(50);

DECLARE @Tipo INT;

DECLARE @Fuente VARCHAR(50);


-- AGREGAMOS "_" A RFC CUANDO ESTE TIENE SOLO 12 POSICIONES (PERSONAS MORALES)

UPDATE dbo.FILE_Garantia SET RFCGarante = '_' + LTRIM(RTRIM(RFCGarante)) WHERE LEN(LTRIM(RTRIM(RFCGarante))) = 12;

-- AGREGAMOS "0" AL CODIGO POSTAL CUANDO ESTE TIENE SOLO 4 POSICIONES

UPDATE dbo.FILE_Garantia SET CodigoPostalGarante = '0' + LTRIM(RTRIM(CodigoPostalGarante)) 
WHERE LEN(LTRIM(RTRIM(CodigoPostalGarante))) = 4;

-- ACTUALIZAMOS PARA HOMOLOGAR MUNICIPIO Y ESTADO BASADO EN LOCALIDAD CNBV

UPDATE dbo.FILE_Garantia SET MunicipioGarante = '';

UPDATE dbo.FILE_Garantia SET EstadoGarante = '';

-- ACTUALIZAMOS LA LOCALIDAD CUANDO RECIBIMOS EL CP

UPDATE f

SET LocalidadGarante = ISNULL(NULLIF(f.LocalidadGarante,''), mun.CodigoCNBV)

FROM dbo.FILE_Garantia f

INNER JOIN dbo.SICC_Municipio mun ON LTRIM(f.CodigoPostalGarante) = mun.CodigoMunicipio;

-- ACTUALIZAMOS EL ESTADO Y EL MUNICIPIO CUANDO RECIBIMOS SOLO LA LOCALIDAD

UPDATE f

SET EstadoGarante = ISNULL(NULLIF(f.EstadoGarante,''), loc.CodigoEstado),

 MunicipioGarante = ISNULL(NULLIF(f.MunicipioGarante,''), loc.CodigoMunicipio)

FROM dbo.FILE_Garantia f

INNER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.LocalidadGarante) = loc.CodigoCNBV;

-- ACTUALIZA EL TipoGarantiaMA cuando no la pasen

UPDATE f

SET TipoGarantiaMA = ISNULL(NULLIF(f.TipoGarantia,''),'')

FROM dbo. FILE_Garantia f;

-- EJECUTAMOS VALIDACIONES CONFIGURADAS

DECLARE crs CURSOR FOR

SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente

FROM dbo.FILE_Garantia_Homologacion

WHERE Activo=1


ORDER BY Tipo;

OPEN crs;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

WHILE @@FETCH_STATUS = 0


BEGIN


EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Garantia', 'FILE_Garantia_Homologacion', @Campo, @Tipo, @Fuente;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

END

CLOSE crs;

DEALLOCATE crs;

UPDATE dbo.FILE_Garantia SET Homologado = 1;
GO
