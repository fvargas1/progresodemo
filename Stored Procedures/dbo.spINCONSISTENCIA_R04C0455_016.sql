SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_016]
AS
BEGIN
-- Si el Saldo de la Deuda a Corto Plazo (dat_deuda_cp) es = 0
-- entonces el Puntaje Deuda Corto Plazo a Deuda Total (cve_puntaje_deuda_cp) debe ser = 105 ó = 74

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoDeudaCortoPlazo,
	P_DeudaCortoPlazoTotal AS Puntos_SdoDeudaCortoPlazo
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoDeudaCortoPlazo,''),'0') AS DECIMAL(18,6)) = 0 AND ISNULL(P_DeudaCortoPlazoTotal,'') NOT IN ('105','74');

END
GO
