SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_014]
AS

BEGIN

-- El Numero de Empleados debe anotarse con cifra positiva.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroEmpleados
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(NumeroEmpleados,''),'-1') AS DECIMAL) < 0;

END
GO
