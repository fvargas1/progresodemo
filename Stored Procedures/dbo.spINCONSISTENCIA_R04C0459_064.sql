SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_064]
AS

BEGIN

-- Validar que la Probabilidad de Incumplimiento Parte Cubierta por Garantías Personales (dat_p_i_cubier_persona) se encuentre entre 0 y 100.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumeroDisposicion,
	PICubierta AS PI_Cubierta
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(PICubierta,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
