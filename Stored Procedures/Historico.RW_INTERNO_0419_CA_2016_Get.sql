SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_INTERNO_0419_CA_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	Producto,
	ReservaInicial,
	Cargos,
	Abonos,
	ReservaActual,
	ReservaActualCalif,
	Diferencia
FROM Historico.RW_INTERNO_0419_CA_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Codigo;
GO
