SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Credito_Garantias]
	@IdPersona BIGINT
AS
SELECT
	gar.IdGarantia AS IdGarantia,
	gar.Codigo AS CodigoGarantia,
	cre.Codigo AS Codigo,
	ISNULL(gar.Descripcion, 'Sin Descripcion') AS Garantia,
	ISNULL(tpoNM.Nombre, '') AS TipoGarantia,
	cg.MontoUsadoGarantia AS ValorGarantia,
	cg.PorcUsadoGarantia AS Porcentaje,
	'' AS Observaciones
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Garantia gar ON gar.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_TipoGarantia tpoGar ON gar.IdTipoGarantia = tpoGar.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_TipoGarantiaNvaMet tpoNM ON tpoGar.IdClasificacionNvaMet = tpoNM.Codigo
LEFT OUTER JOIN (
	SELECT cg.IdGarantia
	FROM dbo.SICCMX_CreditoGarantia cg
	GROUP BY cg.IdGarantia
	HAVING COUNT(cg.IdGarantia) > 1
) AS comp ON cg.IdGarantia = comp.IdGarantia
WHERE cre.IdPersona = @IdPersona AND met.Codigo IN ('7','20','21','22') AND cg.MontoUsadoGarantia > 0
ORDER BY gar.Codigo;
GO
