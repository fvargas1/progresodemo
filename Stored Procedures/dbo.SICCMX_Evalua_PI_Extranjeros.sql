SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Evalua_PI_Extranjeros]
AS

UPDATE ppi
SET [PI] = 0.005
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN (
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20
 UNION ALL
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21
 UNION ALL
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22
) AS cal ON ppi.IdPersona = cal.IdPersona
WHERE pais.Codigo <> '484' AND cal.CalCredAgenciaCal = 1;


-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT ppi.IdPersona, GETDATE(), '', 'Se actualizó la PI al 0.5% ya que es extranjero y cuenta con una calificación de crédito emitida por una agencia calificadora según el Anexo 1-B de la CUB'
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN (
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo20
 UNION ALL
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo21
 UNION ALL
 SELECT IdPersona, CalCredAgenciaCal FROM dbo.SICCMX_Anexo22
) AS cal ON ppi.IdPersona = cal.IdPersona
WHERE pais.Codigo <> '484' AND cal.CalCredAgenciaCal = 1;
GO
