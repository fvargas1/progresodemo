SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_105_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el crédito es calificado como Proyecto De Inversión Con Fuente De Pago Propia (cve_fuente_pago = 1),
-- entonces la Severidad de la Pérdida (dat_severidad_perdida), la Probabilidad de Incumplimiento y la Exposición al Incumplimiento deben ser igual a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(ProyectoInversion,'') = '1'
	AND (ISNULL(CAST(SPTotal AS VARCHAR),'') <> '0.000000' OR ISNULL(CAST(PITotal AS VARCHAR),'') <> '0.000000' OR ISNULL(CAST(EITotal AS VARCHAR),'') <> '0.00');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
