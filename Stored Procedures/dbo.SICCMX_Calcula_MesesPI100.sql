SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_MesesPI100]
AS
DECLARE @FechaPeriodo DATETIME;
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

UPDATE ppi
SET MesesPI100 = DATEDIFF(MONTH, ISNULL(hst.Fecha,@FechaPeriodo), @FechaPeriodo) + 1
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_Persona per ON ppi.IdPersona = per.IdPersona
LEFT OUTER JOIN (
	SELECT hppi.Persona, MAX(ph.Fecha) AS Fecha
	FROM Historico.SICCMX_Persona_PI hppi
	INNER JOIN dbo.SICC_PeriodoHistorico ph ON hppi.IdPeriodoHistorico = ph.IdPeriodoHistorico AND ph.Activo = 1 AND ph.Fecha < @FechaPeriodo
	WHERE hppi.[PI] < 1
	GROUP BY hppi.Persona
) AS hst ON per.Codigo = hst.Persona
WHERE ppi.[PI] = 1;
GO
