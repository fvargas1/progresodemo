SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_013_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el Saldo de los Ingresos Totales Ajustados (dat_saldo_total_ajustado) es >= 0 y <= 1.5
-- entonces el Puntaje Servicio de Deuda a Ingresos Totales Ajustados (cve_puntaje_serv_deuda_ing) debe ser = 67

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngTotales,''),'0') AS DECIMAL(18,6)) BETWEEN 0 AND 1.5 AND ISNULL(P_ServDeudaIngAjust,'') <> '67';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
