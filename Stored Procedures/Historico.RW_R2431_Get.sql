SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R2431_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Periodo_Actual,
	Nombre_Per_Relacionada,
	RFC_Per_Rel,
	Per_Juridica,
	Tipo_Rel_Inst,
	Tipo_Ope_Bal,
	Clase_Oper_Rel,
	Tipo_Moneda,
	Importe,
	Monto_Ope_Rel,
	Concepto_PRR,
	Nom_Grupo_Emp,
	ID_Ope_Rel,
	Caracteristicas,
	Fecha_Originacion,
	Fecha_Vencimiento,
	Monto_Autorizado,
	Fecha_Disposicion,
	Monto_Dispuesto,
	Origen_Otros_Activos  
FROM Historico.RW_R2431
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO
