SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_033_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- La Tasa de Interés Bruta del Periodo (dat_tasa_bruta_periodo) sea MAYOR o IGUAL a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(TasaInteres,''),'0') AS DECIMAL(10,6)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
