SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_035]
AS

BEGIN

-- Si hay banca de desarrollo que otorgó recursos (Col.32), debe indicarse el monto que apoyó la banca de desarrollo (Col.31).

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, CodigoBancoFondeador, MontoFondeo
FROM dbo.RW_R04C0442
WHERE LEN(CodigoBancoFondeador) > 0 AND CodigoBancoFondeador <> '0' AND CAST(ISNULL(NULLIF(MontoFondeo,''),'0') AS DECIMAL(21)) = 0;

END
GO
