SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_A22_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @query VARCHAR(8000);
DECLARE @Ids VARCHAR(2000);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_Analitico_A22';
SELECT @Ids = STUFF((SELECT'],[' + LTRIM(C.Nombre) + '_V],[' + LTRIM(C.Nombre) + '_P'
FROM SICCMX_PI_Variables C
WHERE C.IdMetodologia=22 GROUP BY C.Nombre ORDER BY MIN(C.Orden) FOR XML PATH('')), 1, 2, '') + ']';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();

SET @query =
'SELECT * FROM(
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, piv.Nombre+''_V'' AS Nombre,
CASE WHEN cat.Nombre IS NULL THEN CASE WHEN det.PuntosActual IS NULL THEN '''' ELSE ISNULL(CAST(CAST(det.ValorActual AS DECIMAL(22,4)) AS VARCHAR), ''Sin información'') END ELSE
cat.Codigo + ''. '' + REPLACE(cat.Nombre,'','','''') END AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Persona per
INNER JOIN SICCMX_Persona_PI_Detalles det ON det.IdPersona=per.IdPersona
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI ppi ON ppi.IdPersona=per.IdPersona
INNER JOIN SICCMX_VW_Credito_NMC cre ON per.IdPersona=cre.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Rel_Variable_Catalogo rel ON det.IdVariable=rel.IdVariable
LEFT OUTER JOIN dbo.SICCMX_VW_ItemCatalogos cat ON rel.IdCatalogo=cat.IdCatalogo AND SUBSTRING(REPLACE(CAST(det.ValorActual AS VARCHAR),''.'',''''),1,3)=cat.Codigo
WHERE piv.IdMetodologia=22 AND cre.MontoValorizado>0
UNION ALL
SELECT ' + CAST(@IdReporteLog AS VARCHAR(10)) + ' AS IdReporteLog, per.Codigo, REPLACE(per.Nombre,'','','''') AS Cte_Nombre, piv.Nombre+''_P'' AS Nombre,
ISNULL(CAST(det.PuntosActual AS VARCHAR),'''') AS Dato,
CAST(ppi.PonderadoCuantitativo AS INT) AS PonderadoCuantitativo,
CAST(ppi.PonderadoCualitativo AS INT) AS PonderadoCualitativo,
CAST(ppi.FactorTotal AS INT) AS FactorTotal,
CAST(ppi.PI * 100 AS DECIMAL(10,6)) AS PI
FROM dbo.SICCMX_Persona per
INNER JOIN SICCMX_Persona_PI_Detalles det ON det.IdPersona=per.IdPersona
INNER JOIN SICCMX_PI_Variables piv ON piv.Id=det.IdVariable
INNER JOIN SICCMX_Persona_PI ppi ON ppi.IdPersona=per.IdPersona
INNER JOIN SICCMX_VW_Credito_NMC cre ON per.IdPersona=cre.IdPersona
WHERE piv.IdMetodologia=22 AND cre.MontoValorizado>0
) t PIVOT (max(Dato) FOR Nombre IN ('+@Ids+')) AS pvt';


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Analitico_A22;

-- Informacion para reporte
INSERT INTO dbo.RW_Analitico_A22 (
 IdReporteLog, Codigo, Nombre, PonderadoCuantitativo, PonderadoCualitativo, FactorTotal, [PI], DiasMoraInstFinBanc_V, DiasMoraInstFinBanc_P,
 PrctPagoInstFinBanc_V, PrctPagoInstFinBanc_P, NumInstRep_V, NumInstRep_P, PrctPagoInstFinNoBanc_V, PrctPagoInstFinNoBanc_P,
 TotalPagosInfonavit_V, TotalPagosInfonavit_P, DiasAtrInfonavit_V, DiasAtrInfonavit_P, TasaRetLab_V, TasaRetLab_P, RotActTotal_V, RotActTotal_P,
 RotCapTrabajo_V, RotCapTrabajo_P, ROE_V, ROE_P, EstEconomica_V, EstEconomica_P, IntCarComp_V, IntCarComp_P, Proveedores_V, Proveedores_P,
 Clientes_V, Clientes_P, EstFinAudit_V, EstFinAudit_P, NumAgenCalif_V, NumAgenCalif_P, IndepConAdmon_V, IndepConAdmon_P, EstructOrg_V,
 EstructOrg_P, CompAccionaria_V, CompAccionaria_P, LiquidezOper_V, LiquidezOper_P, UAFIRGastosFin_V, UAFIRGastosFin_P
)
EXECUTE (@query);


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_A22 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
