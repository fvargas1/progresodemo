SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Categoria_Select]
	@IdCategoria BIGINT
AS
SELECT
	vw.IdCategoria,
	vw.Nombre,
	vw.NombreTabla
FROM dbo.MIGRACION_VW_Categoria vw
WHERE vw.IdCategoria = @IdCategoria;
GO
