SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_011]
AS

BEGIN

-- El campo RESERV NO CUBIERT GTIA PERSONA debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaExpuesta
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(ReservaExpuesta,''),'-1') AS DECIMAL) < 0;

END
GO
