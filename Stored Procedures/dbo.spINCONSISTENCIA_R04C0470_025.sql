SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_025]
AS

BEGIN

-- Si el Total de Pagos al Infonavit en el Último Bimestre (dat_tot_pagos_infonavit) es = 0 entonces el
-- Puntaje Asignado por el Total de Pagos al Infonavit en el Último Bimestre (cve_ptaje_pgos_infonav) debe ser = 21 ó = 59

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PagosInfonavit,
	P_PagosInfonavit AS Puntos_PagosInfonavit
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(PagosInfonavit AS DECIMAL) = 0 AND ISNULL(P_PagosInfonavit,'') NOT IN ('21','59');

END


GO
