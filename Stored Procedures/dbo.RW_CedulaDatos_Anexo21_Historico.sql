SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_CedulaDatos_Anexo21_Historico]
 @IdPeriodoHistorico BIGINT
AS
DECLARE @NombreInstitucion VARCHAR(150);
DECLARE @FechaPeriodo VARCHAR(10);

SELECT @NombreInstitucion = Nombre FROM dbo.SICC_Institucion WHERE Codigo = (SELECT [Value] FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion');
SELECT @FechaPeriodo = CONVERT(CHAR(10), Fecha,126) FROM dbo.SICC_PeriodoHistorico WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

SELECT DISTINCT
p.Codigo AS IdPersona,
c.Metodologia AS IdMetodologia,
dbo.fnFormat_Name(p.Nombre) AS 'Description',
dbo.fnFormat_Name(p.Nombre) AS NombrePersona,
p.Codigo AS CodigoPersona,
m.Nombre AS Metodologia,
m.Codigo AS CodigoMetodologia,
'Default' AS Filtrouno,
'Default' AS Filtrodos,
ISNULL(@IdPeriodoHistorico, -1 ) AS IdPeriodoHistorico,
@NombreInstitucion AS Institucion,
@FechaPeriodo AS FechaPeriodo
FROM Historico.SICCMX_Persona p
INNER JOIN Historico.SICCMX_Credito c ON p.Codigo = c.Persona AND p.IdPeriodoHistorico = c.IdPeriodoHistorico
INNER JOIN dbo.SICCMX_Metodologia m ON c.Metodologia = m.Codigo
WHERE c.Metodologia='21' AND p.IdPeriodoHistorico = @IdPeriodoHistorico
ORDER BY NombrePersona ASC;
GO
