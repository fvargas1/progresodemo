SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0458_2016_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActEconomica,
	GrupoRiesgo,
	EntFinAcreOtorgantesCre,
	Localidad,
	Municipio,
	Estado,
	Nacionalidad,
	IdBuroCredito,
	LEI,
	TipoAltaCredito,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoGlobalCNBV,
	MonLineaCredValorizado,
	MonLineaCred,
	FecMaxDis,
	FecVenLin,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	Posicion,
	RegGarantiaMob,
	IdDeudorRelacionado,
	IdInstitucionOrigen,
	TasaInteres,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	NumMesesAmortCap,
	NumMesesPagoInt,
	ComisionAperturaTasa,
	ComisionAperturaMonto,
	ComisionDisposicionTasa,
	ComisionDisposicionMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_VW_R04C0458_2016;
GO
