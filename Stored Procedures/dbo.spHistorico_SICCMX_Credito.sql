SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Credito]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Credito WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Credito (
 IdPeriodoHistorico,
 Codigo,
 PorcentajeFondeo,
 SaldoCapitalVigente,
 InteresVigente,
 SaldoCapitalVencido,
 InteresVencido,
 InteresRefinanciado,
 FechaTraspasoVigente,
 FechaTraspasoVencida,
 FechaProximaAmortizacion,
 FechaAutorizacionOriginal,
 MontoOriginal,
 Reestructura,
 SituacionCredito,
 Persona,
 ClasificacionContable,
 Moneda,
 Destino,
 TasaReferencia,
 TasaBruta,
 MontoLineaCredito,
 NumeroAgrupacion,
 FechaDisposicion,
 FechaVencimiento,
 FrecuenciaRevisionTasa,
 LineaCredito,
 NumeroReestructura,
 Formalizda,
 Recuperacion,
 Expediente,
 MesesIncumplimiento,
 Metodologia,
 Fondeo,
 InteresCarteraVencida,
 EsMultimoneda,
 DestinoCreditoMA
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 cre.Codigo,
 cre.PorcentajeFondeo,
 cre.SaldoCapitalVigente,
 cre.InteresVigente,
 cre.SaldoCapitalVencido,
 cre.InteresVencido,
 cre.InteresRefinanciado,
 cre.FechaTraspasoVigente,
 cre.FechaTraspasoVencida,
 cre.FechaProximaAmortizacion,
 cre.FechaAutorizacionOriginal,
 cre.MontoOriginal,
 rees.Codigo, -- cre.IdReestructura
 sitCre.Codigo, -- cre.IdSituacionCredito
 per.Codigo, -- cre.IdPersona
 clasCon.Codigo, -- cre.IdClasificacionContable
 mon.Codigo, -- cre.IdMoneda
 dest.Codigo, -- cre.IdDestino
 tasaRef.Codigo, -- cre.IdTasaReferencia
 cre.TasaBruta,
 cre.MontoLineaCredito,
 cre.NumeroAgrupacion,
 cre.FechaDisposicion,
 cre.FechaVencimiento,
 cre.FrecuenciaRevisionTasa,
 lin.Codigo, -- cre.IdLineaCredito
 cre.NumeroReestructura,
 cre.Formalizda,
 cre.Recuperacion,
 cre.Expediente,
 cre.MesesIncumplimiento,
 met.Codigo, -- cre.IdMetodologia
 fond.Codigo, -- cre.IdFondeo
 cre.InteresCarteraVencida,
 cre.EsMultimoneda,
 destMA.Codigo
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_Reestructura rees ON rees.IdReestructura = cre.IdReestructura
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON cre.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clasCon ON cre.IdClasificacionContable = clasCon.IdClasificacionContable
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Destino dest ON cre.IdDestino = dest.IdDestino
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON cre.IdTasaReferencia = tasaRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_Fondeo fond ON cre.IdFondeo = fond.IdFondeo
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON cre.IdDestinoCreditoMA = destMA.IdDestinoCredito;
GO
