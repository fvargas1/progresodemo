SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0420Datos_CargaInicial_2016]
AS
-- ACTUALIZAMOS INFORMACION HISTORICA PARA CREDITOS QUE NO TIENEN TIPO DE ALTA Y QUE NO CUENTAN CON SALDO HISTORICO
-- ESTO ES PARA CARGAS INICIALES
UPDATE dat
SET SaldoHistorico = dat.SaldoActual - ISNULL(mov.Monto,0)
FROM R04.[0420Datos_2016] dat
INNER JOIN dbo.SICCMX_Credito cre ON dat.Codigo = cre.Codigo
INNER JOIN dbo.SICCMX_CreditoInfo inf ON cre.IdCredito = inf.IdCredito AND inf.IdTipoAlta IS NULL
LEFT OUTER JOIN (
	SELECT DISTINCT m.IdCredito, m.Monto
	FROM dbo.SICCMX_Credito_Movimientos m
	INNER JOIN dbo.SICC_TipoMovimiento tm ON m.IdTipoMovimiento = tm.IdTipoMovimiento
	INNER JOIN R04.[0420Configuracion_2016] c ON tm.Codigo = c.TipoMovimiento
) AS mov ON inf.IdCredito = mov.IdCredito
WHERE dat.SaldoHistorico = 0;
GO
