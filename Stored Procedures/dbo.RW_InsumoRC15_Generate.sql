SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_InsumoRC15_Generate] 
AS 
DECLARE @IdPeriodo BIGINT;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'RC15';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_InsumoRC15;

INSERT INTO dbo.RW_InsumoRC15 (
	IdReporteLog,
	Periodo,
	CodigoCredito,
	CodigoPersona,
	RFC,
	MontoGarantiasReales,
	MontoReservas,
	MontoAvales,
	FechaDisposicion,
	SituacionCredito,
	Exposicion,
	ImporteRC3,
	ClienteRelacionado
)
SELECT
	@IdReporteLog,
	@IdPeriodo,
	cre.Codigo AS CodigoCredito,
	per.Codigo AS CodigoPerosona,
	per.RFC,
	CAST (ISNULL(GarReales.MontoGarantiasReales,0) AS DECIMAL(23,2)) AS MontoGarantiasReales,
	CAST (ISNULL(crevar.ReservaFinal,0) AS DECIMAL(23,2)) AS MontoReservas, 
	CAST (ISNULL(aval.MontoCubierto,0) AS DECIMAL(23,2)) AS MontoAvales,
	CONVERT(char(10),cre.FechaDisposicion, 126) AS FechaDisposicion,
	sitcre.Codigo AS SituacionCredito,
	CAST(ISNULL(crevar.EI_Total,0) AS DECIMAL(23,2))AS Exposicion,
	CAST(ISNULL(cre.SaldoCapitalVigente + cre.SaldoCapitalVencido + cre.InteresVigente + cre.InteresVencido, 0) AS DECIMAL(23,2)) AS ImporteRC3,
	CASE WHEN ISNULL(prel.Codigo, '0') = '0' OR ISNULL(prel.Codigo, '0') = '8'THEN 'No' ELSE 'Si' END AS ClienteRelacionado
FROM dbo.SICCMX_Credito cre
INNER JOIN dbo.SICCMX_Persona per ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crevar ON crevar.IdCredito = cre.IdCredito
LEFT OUTER JOIN SICCMX_VW_Cobertura_Personales aval ON aval.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitcre ON sitcre.IdSituacionCredito = cre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado prel ON prel.IdDeudorRelacionado = per.IdDeudorRelacionado
LEFT OUTER JOIN (
	SELECT IdCredito, CAST(SUM(ValorGarantia) AS DECIMAL(23,2)) AS MontoGarantiasReales
	FROM dbo.SICCMX_VW_Garantias_NM
	WHERE IdClasificacionNvaMet IN (1,2)
	GROUP BY IdCredito, MontoCredito
) GarReales ON GarReales.IdCredito = cre.IdCredito;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_InsumoRC15 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;

SET @FechaMigracion = ( SELECT MAX( Fecha ) FROM MIGRACION_ProcesoLog );

UPDATE dbo.RW_ReporteLog
SET
	TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = @FechaMigracion,
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
