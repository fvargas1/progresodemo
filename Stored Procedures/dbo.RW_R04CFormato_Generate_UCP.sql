SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04CFormato_Generate_UCP]  
AS  
UPDATE dbo.RW_R04C0458 SET AjusteTasaReferencia = REPLACE(REPLACE(AjusteTasaReferencia,'+',''),'*','')
UPDATE dbo.RW_R04C0463 SET AjusteTasaReferencia = REPLACE(REPLACE(AjusteTasaReferencia,'+',''),'*','')
UPDATE dbo.RW_R04C0468 SET AjusteTasaReferencia = REPLACE(REPLACE(AjusteTasaReferencia,'+',''),'*','')
UPDATE dbo.RW_R04C0473 SET AjusteTasaReferencia = REPLACE(REPLACE(AjusteTasaReferencia,'+',''),'*','')
UPDATE dbo.RW_R04C0478 SET AjusteTasaReferencia = REPLACE(REPLACE(AjusteTasaReferencia,'+',''),'*','')
GO
