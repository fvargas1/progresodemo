SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_026]
AS

BEGIN

-- El monto de "COMISIONES Y GASTOS DE ORIGINACIÓN DEL CRÉDITO" deberá anotarse cero (0) si y solo si "Tipo de Alta del Crédito" es igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, Comisiones, TipoAlta
FROM dbo.RW_R04H0491
WHERE Comisiones = '0' AND TipoAlta <> '3';

END
GO
