SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_054]
AS

BEGIN

-- La "PERIDIOCIDAD DE LAS AMORTIZACIONES" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.PeriodicidadAmortizaciones
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalHipotecario cat ON ISNULL(r.PeriodicidadAmortizaciones,'') = cat.CodigoCNBV
WHERE cat.IdPeriodicidadCapitalHipotecario IS NULL;

END
GO
