SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_CreditoGarantia_errores_ClearLog]
AS
UPDATE dbo.FILE_CreditoGarantia
SET
errorFormato = NULL,
errorCatalogo = NULL;

TRUNCATE TABLE dbo.FILE_CreditoGarantia_errores;
GO
