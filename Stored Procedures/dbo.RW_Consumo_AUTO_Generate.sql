SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_AUTO_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'CONSUMO' AND Nombre = '-AUTO_Seguimiento';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Consumo_AUTO;

INSERT INTO dbo.RW_Consumo_AUTO (
 IdReporteLog, IdProducto, FolioCredito, FolioCliente, Reestructura, FechaInicioCredito, FechaTeoricaVencimientoCredito, PeriodosFacturacionCredito,
 PlazoTotal, ImporteOriginalCredito, ValorOriginalBien, TasaInteresAnualCredito, MecanismoPago, CodigoPostal, FechaCorte, PlazoRemanente, SaldoCredito,
 MontoExigible, PagoRealizado, PromedioPorcentajePagosRealizados, DiasAtraso, NumeroAtrasos, MaximoNumeroAtrasos, IndicadorAtraso,
 SumaExigiblesTeoricosCredito, TipoGarantia, ImporteGarantia, RegistroUnicoGarantiasMobiliarias, ProbabilidadIncumplimiento,
 SeveridadPerdidaParteNoCubierta, SeveridadPerdidaParteCubierta, ExposicionIncumplimientoParteNoCubierta, ExposicionIncumplimientoParteCubierta,
 MontoReservasConstituirCredito, ClasificacionCredito, QuitasCondonacionesBonificacionesDescuentos, RelacionAcreditadoInstitucion,
 ClaveConsultaSociedadInformacionCrediticia, MetodologiaUtilizadaCalculoReservas, ProbabilidadIncumplimientoInterna, SeveridadPerdidaInterna,
 ExposicionIncumplimientoInterna, MontoReservasConstituirCreditoInterno, Aforo, Enganche, ValorTotalBien, CAT, MontoExigibleSinSeg, ModalidadCAT,
 PrimasSeguros, PrimaDanos
)
SELECT DISTINCT
 @IdReporteLog,
 con.IdProductoNombre AS IdProducto,
 info.CreditoCodificado AS FolioCredito,
 info.ClienteCodificado AS FolioCliente,
 rst.CodigoBanxico AS Reestructura,
 CASE WHEN info.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaDisposicion,102),'.','/'),1,10) END AS FechaInicioCredito,
 CASE WHEN info.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaVencimiento,102),'.','/'),1,10) END AS FechaTeoricaVencimientoCredito,
 perCap.CodigoBanxico AS PeriodosFacturacionCredito,
 info.PlazoTotal AS PlazoTotal,
 info.MontoLineaAutorizada AS ImporteOriginalCredito,
 info.ValorOriginalBien AS ValorOriginalBien,
 con.TasaInteres AS TasaInteresAnualCredito,
 fpc.CodigoBanxico AS MecanismoPago,
 pInfo.CodigoPostal AS CodigoPostal,
 CASE WHEN info.FechaCorte IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,info.FechaCorte,102),'.','/'),1,10) END AS FechaCorte,
 info.PlazoRemanente AS PlazoRemanente,
 con.SaldoTotalValorizado AS SaldoCredito,
 info.MontoExigible AS MontoExigible,
 info.PagoRealizado AS PagoRealizado,
 pre.PorPago * 100 AS PromedioPorcentajePagosRealizados,
 info.DiasAtraso AS DiasAtraso,
 pre.ATR AS NumeroAtrasos,
 pre.MaxATR AS MaximoNumeroAtrasos,
 pre.INDATR AS IndicadorAtraso,
 info.ExigibleTeorico AS SumaExigiblesTeoricosCredito,
 tgc.CodigoBanxico AS TipoGarantia,
 info.ImporteGarantia AS ImporteGarantia,
 info.RegistroUnicoGarantia AS RegistroUnicoGarantiasMobiliarias,
 crv.[PI] * 100 AS ProbabilidadIncumplimiento,
 crv.SPExpuesta * 100 AS SeveridadPerdidaParteNoCubierta,
 crv.SPCubierta * 100 AS SeveridadPerdidaParteCubierta,
 crv.EExpuesta AS ExposicionIncumplimientoParteNoCubierta,
 crv.ECubierta AS ExposicionIncumplimientoParteCubierta,
 crv.ReservaTotal AS MontoReservasConstituirCredito,
 sit.CodigoBanxico AS ClasificacionCredito,
 ISNULL(info.QuitasCondonacionesValorizado,0) + ISNULL(info.BonificacionesDescuentosValorizado,0) AS QuitasCondonacionesBonificacionesDescuentos,
 dr.CodigoBanxico AS RelacionAcreditadoInstitucion,
 info.FolioConsultaBuro AS ClaveConsultaSociedadInformacionCrediticia,
 '' AS MetodologiaUtilizadaCalculoReservas,
 '' AS ProbabilidadIncumplimientoInterna,
 '' AS SeveridadPerdidaInterna,
 '' AS ExposicionIncumplimientoInterna,
 '' AS MontoReservasConstituirCreditoInterno,
 info.Aforo AS Aforo,
 info.Enganche AS Enganche,
 info.ValorTotalCredito AS ValorTotalBien,
 info.CAT * 100 AS CAT,
 info.ExigibleSinSegurosComisiones AS MontoExigibleSinSeg,
 modCat.Codigo AS ModalidadCAT,
 info.PrimaSeguroObligatorio AS PrimasSeguros,
 info.PrimaSeguroDanos AS PrimaDanos
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_VW_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON con.IdConsumo = pre.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON pre.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICC_TipoCreditoConsumo tcc ON con.IdTipoCredito = tcc.IdTipoCreditoConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo rst ON con.IdReestructura = rst.IdReestructuraConsumo
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo perCap ON con.IdPeriodicidadCapital = perCap.IdPeriodicidadPagoConsumo
LEFT OUTER JOIN dbo.SICC_FormaPagoConsumo fpc ON info.IdMecanismoPago = fpc.IdFormaPagoConsumo
LEFT OUTER JOIN dbo.SICC_TipoGarantiaConsumo tgc ON info.IdTipoGarantia = tgc.IdTipoGarantiaConsumo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON con.IdSituacionCredito = sit.IdSituacionConsumo
LEFT OUTER JOIN dbo.SICC_DeudorRelacionado dr ON per.IdDeudorRelacionado = dr.IdDeudorRelacionado
LEFT OUTER JOIN dbo.SICC_ModalidadCAT modCat ON info.IdModalidadCAT = modCat.IdModalidadCAT
WHERE tcc.ClasificacionReportes = 'AUTO' AND met.Codigo IN ('1','2','3') AND info.IdTipoBaja IS NULL;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Consumo_AUTO WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
