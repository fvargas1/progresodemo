SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_025]
AS
BEGIN
-- Si el Saldo de Inversión (dat_saldo_inversion) es >= 0 y <= 8, entonces el Puntaje Inversión a Ingresos Totales (cve_puntaje_inver_a_ingreso) debe ser = 29

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoInversion,
	P_InvIngTotales AS Puntos_SdoInversion
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoInversion,''),'0') AS DECIMAL(18,6)) BETWEEN 0 AND 8 AND ISNULL(P_InvIngTotales,'') <> '29';

END
GO
