SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_LineaCredito]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_LineaCredito WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO [Historico].[SICCMX_LineaCredito] (
 IdPeriodoHistorico,
 CodigoLinea,
 CodigoPersona,
 FechaDisposicion,
 Moneda,
 DestinoCreditoComercial,
 TasaReferencia,
 MontoLinea,
 FrecuenciaRevisionTasa,
 TipoAlta,
 DisposicionCredito,
 AjusteTasaReferencia,
 InstitucionFondea,
 ComisionesCobradas,
 TipoBaja,
 ImporteQuebranto,
 SaldoInicial,
 PeriodicidadCapital,
 PeriodicidadInteres,
 CodigoCreditoReestructurado,
 ProductoComercial,
 Posicion,
 TipoLinea,
 TipoOperacion,
 FecMaxDis,
 PorcPartFederal,
 MontoPagEfeCap,
 MontoPagEfeInt,
 MontoPagEfeCom,
 MontoPagEfeIntMor,
 ComDispTasa,
 ComDispMonto,
 GastosOrigTasa,
 ResTotalInicioPer,
 MontoQuitasCastQue,
 MontoBonificacionDesc,
 MesesGraciaCap,
 MesesGraciaIntereses,
 NumEmpLoc,
 NumEmpLocFedSHCP,
 FecVenLinea,
 CAT,
 MontoLineaSinA,
 MontoPrimasAnuales,
 EsPadre,
 MunicipioDestino,
 ActividadEconomicaDestino,
 FolioConsultaBuro,
 CodigoCNBV,
 Fuente,
 EsMultimoneda,
 DestinoCreditoMA,
 TipoAltaMA,
 DisposicionMA,
 PeriodicidadCapitalMA,
 PeriodicidadInteresMA,
 TipoBajaMA,
 NumeroAgrupacion,
 MontoBancaDesarrollo,
 ComisionDelMes,
 GrupalCNBV,
 MontoCastigos,
 MontoQuebrantos,
 MontoDescuentos,
 MontoDacion,
 LocalidadDestino,
 EstadoDestino,
 MontoCondonacion
)
SELECT
 @IdPeriodoHistorico,
 lin.Codigo,
 per.Codigo,
 lin.FechaDisposicion,
 mon.Codigo,
 dest.Codigo,
 tasaRef.Codename,
 lin.MontoLinea,
 lin.FrecuenciaRevisionTasa,
 tipoAl.Codigo,
 dispCre.Codigo,
 lin.AjusteTasaReferencia,
 fond.Codigo,
 lin.ComisionesCobradas,
 tipoBa.Codigo,
 lin.ImporteQuebranto,
 lin.SaldoInicial,
 perCap.Codigo,
 perInt.Codigo,
 lin.CodigoCreditoReestructurado,
 tcc.Codigo,
 pos.Codigo,
 tln.Codigo,
 tpoOper.Codigo,
 lin.FecMaxDis,
 lin.PorcPartFederal,
 lin.MontoPagEfeCap,
 lin.MontoPagEfeInt,
 lin.MontoPagEfeCom,
 lin.MontoPagEfeIntMor,
 lin.ComDispTasa,
 lin.ComDispMonto,
 lin.GastosOrigTasa,
 lin.ResTotalInicioPer,
 lin.MontoQuitasCastQue,
 lin.MontoBonificacionDesc,
 lin.MesesGraciaCap,
 lin.MesesGraciaIntereses,
 lin.NumEmpLoc,
 lin.NumEmpLocFedSHCP,
 lin.FecVenLinea,
 lin.CAT,
 lin.MontoLineaSinA,
 lin.MontoPrimasAnuales,
 lin.EsPadre,
 loc.CodigoCNBV,
 actEco.Codigo,
 lin.FolioConsultaBuro,
 lin.CodigoCNBV,
 lin.Fuente,
 lin.EsMultimoneda,
 destMA.Codigo,
 tipoAltaMA.Codigo,
 dispCreMA.Codigo,
 perCapMA.Codigo,
 perIntMA.Codigo,
 tipoBaMA.Codigo,
 lin.NumeroAgrupacion,
 lin.MontoBancaDesarrollo,
 lin.ComisionDelMes,
 lin.GrupalCNBV,
 lin.MontoCastigos,
 lin.MontoQuebrantos,
 lin.MontoDescuentos,
 lin.MontoDacion,
 lin.LocalidadDestino,
 lin.EstadoDestino,
 lin.MontoCondonacion
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_Moneda mon ON lin.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Destino dest ON lin.IdDestino = dest.IdDestino
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON lin.IdTasaReferencia = tasaRef.IdTasaReferencia
LEFT OUTER JOIN dbo.SICC_TipoAlta tipoAl ON lin.IdTipoAlta = tipoAl.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_DisposicionCredito dispCre ON lin.IdDisposicion = dispCre.IdDisposicion
LEFT OUTER JOIN dbo.SICC_Fondeo fond ON lin.IdInstitucionFondea = fond.IdFondeo
LEFT OUTER JOIN dbo.SICC_TipoBaja tipoBa ON lin.IdTipoBaja = tipoBa.IdTipoBaja
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON lin.IdPeriodicidadCapital = perCap.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON lin.IdPeriodicidadInteres = perInt.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON lin.ProductoComercial = tcc.IdTipoCredito
LEFT OUTER JOIN dbo.SICC_Posicion pos ON lin.Posicion = pos.IdPosicion
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON lin.TipoLinea = tln.IdTipoLinea
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON lin.TipoOperacion = tpoOper.IdTipoOperacion
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON lin.IdActividadEconomicaDestino = actEco.IdActividadEconomica
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON lin.IdDestinoCreditoMA = destMA.IdDestinoCredito
LEFT OUTER JOIN dbo.SICC_TipoAltaMA tipoAltaMA ON lin.IdTipoAltaMA = tipoAltaMA.IdTipoAlta
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dispCreMA ON lin.IdDisposicionMA = dispCreMA.IdDisposicion
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCapMA ON lin.IdPeriodicidadCapitalMA = perCapMA.IdPeriodicidadCapital
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perIntMA ON lin.IdPeriodicidadInteresMA = perIntMA.IdPeriodicidadInteres
LEFT OUTER JOIN dbo.SICC_TipoBajaMA tipoBaMA ON lin.IdTipoBajaMA = tipoBaMA.IdTipoBaja
LEFT OUTER JOIN dbo.SICC_Localidad2015 loc ON lin.IdMunicipioDestino = loc.IdLocalidad;
GO
