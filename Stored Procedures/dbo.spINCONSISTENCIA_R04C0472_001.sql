SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0472_001]
AS

BEGIN

-- El Monto Total Pagado Ef. por el Acreditado en el Periodo debe ser > a 0.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoPagado
FROM dbo.RW_VW_R04C0472_INC
WHERE CAST(ISNULL(NULLIF(MontoPagado,''),'0') AS DECIMAL) <= 0;

END


GO
