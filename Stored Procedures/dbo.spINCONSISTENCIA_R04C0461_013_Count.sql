SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_013_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo ID Crédito Metodología CNBV no tenga más de un Nombre de Acreditado.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0461
WHERE CodigoCreditoCNBV IN (
	SELECT rep.CodigoCreditoCNBV
	FROM dbo.RW_R04C0461 rep
	INNER JOIN dbo.RW_R04C0461 rep2 ON rep.CodigoCreditoCNBV = rep2.CodigoCreditoCNBV AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.CodigoCreditoCNBV
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
