SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_ProyectoCalificacionDatosCedula_Historico]
	@CodigoProyecto VARCHAR(50),
	@IdPeriodoHistorico BIGINT
AS
SELECT
	pc.VpTotal,
	pc.UpAcumulada,
	pc.ReservaConstruccion,
	pc.ReservaOperacion,
	pc.SobreCosto,
	pc.MontoCubierto,
	pc.ExposicionSobrecosto,
	pc.CalificacionSobrecosto,
	pc.PorcenajeSobrecosto AS PorcentajeSobrecosto,
	pc.CalificacionCorrida,
	pc.PorcentajeCorrida AS PorcentajeCorrida,
	pc.CalificacionFinal,
	pc.SaldoInsoluto AS SaldoCredito
FROM Historico.SICCMX_Proyecto pry
INNER JOIN Historico.SICCMX_ProyectoCalificacion pc ON pry.Proyecto = pc.Proyecto AND pry.IdPeriodoHistorico = pc.IdPeriodoHistorico
WHERE pry.Proyecto = @CodigoProyecto AND pry.IdPeriodoHistorico = @IdPeriodoHistorico;
GO
