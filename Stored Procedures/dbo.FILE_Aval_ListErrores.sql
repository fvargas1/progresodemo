SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Aval_ListErrores]
AS
SELECT
 CodigoAval,
 NombreAval,
 RFC,
 Domicilio,
 PersonalidadJuridica,
 TipoAval,
 MunAval,
 PIAval,
 Moneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 ActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 CodigoCliente,
 Localidad,
 Estado,
 CodigoPostal,
 Fuente
FROM dbo.FILE_Aval
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
