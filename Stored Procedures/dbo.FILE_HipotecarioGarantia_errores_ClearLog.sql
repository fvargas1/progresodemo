SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_HipotecarioGarantia_errores_ClearLog]
AS
UPDATE dbo.FILE_HipotecarioGarantia
SET
errorFormato = NULL,
errorCatalogo = NULL
WHERE errorFormato IS NOT NULL OR errorCatalogo IS NOT NULL;

TRUNCATE TABLE dbo.FILE_HipotecarioGarantia_errores;
GO
