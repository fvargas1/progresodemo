SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04H0493_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoCredito,
	CodigoCreditoCNBV,
	NumeroAvaluo,
	TipoBaja,
	SaldoPrincipalInicial,
	MontoTotalLiquidacion,
	MontoPagoLiquidacion,
	MontoBonificaciones,
	ValorBienAdjudicado
FROM Historico.RW_R04H0493
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY NumeroSecuencia;
GO
