SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Proyecto_VPTotal_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Proyecto_VPTotal_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Proyecto
SET errorFormato = 1
WHERE ISNUMERIC(ISNULL(VPTotal,'0')) = 0 OR VPTotal LIKE '%[^0-9 .]%';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Proyecto_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoProyecto,
 'VPTotal',
 VPTotal,
 1,
 @Detalle
FROM dbo.FILE_Proyecto
WHERE ISNUMERIC(ISNULL(VPTotal,'0')) = 0 OR VPTotal LIKE '%[^0-9 .]%';
GO
