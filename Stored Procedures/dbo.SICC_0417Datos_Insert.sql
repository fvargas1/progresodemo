SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0417Datos_Insert]
AS

TRUNCATE TABLE R04.[0417Datos];

-- COMERCIAL
INSERT INTO R04.[0417Datos] (
 Codigo,
 Moneda,
 CodigoProducto,
 Calificacion,
 TerminacionCuenta,
 Saldo,
 Estimacion,
 InteresCarteraVencida,
 CartaDeCredito,
 ReservasAdicOrdenadasCNBV
)
SELECT DISTINCT
 cr.CodigoCredito,
 mon.ClasSerieA,
 tp.Codigo,
 cal.Codigo,
 '',
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(cr.SaldoCapitalVigente AS DECIMAL)
 + CAST(cr.InteresVigente AS DECIMAL)
 + CAST(cr.SaldoCapitalVencido AS DECIMAL)
 + CAST(cr.InteresVencido + ISNULL(cr.InteresCarteraVencida,0) AS DECIMAL)
 ELSE cr.MontoValorizado + ISNULL(cr.InteresCarteraVencidaValorizado,0) END,
 crv.ReservaFinal,
 ISNULL(cr.InteresCarteraVencidaValorizado,0),
 CASE WHEN cr.Posicion = '181' OR cr.TipoLinea = '181' THEN 1 ELSE 0 END,
 ISNULL(crv.ReservaAdicional,0)
FROM dbo.SICCMX_VW_Credito_NMC cr
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON info.IdCredito = crv.IdCredito
INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cr.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON info.IdTipoProductoSerie4 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion
WHERE cal.Codigo <> 'NA';


-- CONSUMO
INSERT INTO R04.[0417Datos] (
 Codigo,
 Moneda,
 CodigoProducto,
 Calificacion,
 TerminacionCuenta,
 Saldo,
 Estimacion,
 InteresCarteraVencida,
 ReservasAdicOrdenadasCNBV
)
SELECT
 cr.Codigo,
 mon.ClasSerieA,
 tp.Codigo,
 cal.Codigo,
 '',
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(cr.SaldoCapitalVigente AS DECIMAL)
 + CAST(cr.InteresVigente AS DECIMAL)
 + CAST(cr.SaldoCapitalVencido AS DECIMAL)
 + CAST(cr.InteresVencido + ISNULL(cr.InteresCarteraVencida,0) AS DECIMAL)
 ELSE cr.SaldoTotalValorizado + ISNULL(cr.InteresCarteraVencidaValorizado,0) END,
 crv.ReservaTotal,
 ISNULL(cr.InteresCarteraVencidaValorizado,0),
 ISNULL(crv.ReservaAdicional,0)
FROM dbo.SICCMX_VW_Consumo cr
INNER JOIN dbo.SICCMX_ConsumoInfo info ON cr.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON info.IdTipoProductoSerie4 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON crv.IdCalificacion = cal.IdCalificacion;


-- HIPOTECARIO
INSERT INTO R04.[0417Datos] (
 Codigo,
 Moneda,
 CodigoProducto,
 Calificacion,
 TerminacionCuenta,
 Saldo,
 Estimacion,
 InteresCarteraVencida,
 ReservasAdicOrdenadasCNBV
)
SELECT
 hip.Codigo,
 mon.ClasSerieA,
 tp.Codigo,
 cal.Codigo,
 '',
 CASE WHEN mon.CodigoISO = 'MXN' THEN CAST(hip.SaldoCapitalVigente AS DECIMAL)
 + CAST(hip.InteresVigente AS DECIMAL)
 + CAST(hip.SaldoCapitalVencido AS DECIMAL)
 + CAST(hip.InteresVencido + ISNULL(hip.InteresCarteraVencida,0) AS DECIMAL)
 ELSE hip.SaldoTotalValorizado + ISNULL(hip.InteresCarteraVencidaValorizado,0) END,
 hrv.Reserva,
 ISNULL(hip.InteresCarteraVencidaValorizado,0),
 ISNULL(res.ReservaAdicional,0)
FROM dbo.SICCMX_VW_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON info.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON hrv.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON hip.IdTipoCreditoR04A = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 cal ON hrv.IdCalificacion = cal.IdCalificacion;
GO
