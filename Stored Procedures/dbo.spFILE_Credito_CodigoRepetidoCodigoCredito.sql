SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_CodigoRepetidoCodigoCredito]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_CodigoRepetidoCodigoCredito';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Credito f
WHERE f.CodigoCredito IN (
 SELECT f.CodigoCredito
 FROM dbo.FILE_Credito f
 GROUP BY f.CodigoCredito
 HAVING COUNT(f.CodigoCredito )>1
) AND LEN(f.CodigoCredito) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	f.CodigoCredito,
	'CodigoCredito',
	f.CodigoCredito,
	1,
	@Detalle
FROM dbo.FILE_Credito f
WHERE f.CodigoCredito IN (
 SELECT f.CodigoCredito
 FROM dbo.FILE_Credito f
 GROUP BY f.CodigoCredito
 HAVING COUNT(f.CodigoCredito )>1
) AND LEN(f.CodigoCredito) > 0;
GO
