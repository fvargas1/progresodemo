SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_045]
AS
BEGIN
-- Validar que el Monto de Capital Pagado Efectivamente por el Acreditado en el Periodo (dat_monto_pagado_capital) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoCapitalPagado
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(MontoCapitalPagado,''),'-1') AS DECIMAL) < 0;

END


GO
