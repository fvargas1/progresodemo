SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_011_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "PRODUCTO HIPOTECARIO DE LA ENTIDAD" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_ProductoHipotecario cat ON ISNULL(r.ProductoHipotecario,'') = cat.CodigoCNBV
WHERE cat.IdProductoHipotecario IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
