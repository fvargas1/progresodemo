SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_097_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si las Reservas Totales (dat_reservas_totales) son MAYORES o IGUALES a 0, validar que el valor absoluto de la
-- siguiente operación sea menor a 1000, cuando se trate de un crédito sin fuente de pago propia (cve_proy_pago_anexo_19 = 2):
-- ABS(dat_reservas_totales - (dat_sever_cubier_persona /100* dat_exp_cubier_persona * dat_p_i_cubier_persona/100 + dat_sever_no_cubier_persona /100* dat_exp_no_cubier_persona * dat_p_i_no_cubier_persona/100))<1000

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0479
WHERE CAST(ReservaTotal AS DECIMAL) >= 0 AND ISNULL(ProyectoInversion,'') = '2'
	AND ABS(CAST(ReservaTotal AS DECIMAL) - (
	CAST((CAST(PICubierta AS DECIMAL(12,8))/100.00) * (CAST(SPCubierta AS DECIMAL(12,8))/100.00) * CAST(EICubierta AS DECIMAL) AS DECIMAL) +
	CAST((CAST(PIExpuesta AS DECIMAL(12,8))/100.00) * (CAST(SPExpuesta AS DECIMAL(12,8))/100.00) * CAST(EIExpuesta AS DECIMAL) AS DECIMAL)
	)) >= 1000;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
