SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R2431_Generate]        
AS        
      
SET LANGUAGE english;      
SET DATEFORMAT YMD;      
DECLARE @IdPeriodo BIGINT;        
DECLARE @FechaPeriodo DATETIME;        
DECLARE @IdReporte AS BIGINT;        
DECLARE @IdReporteLog AS BIGINT;        
DECLARE @TotalRegistros AS INT;        
DECLARE @TotalSaldos AS DECIMAL;        
DECLARE @TotalIntereses AS DECIMAL;        
DECLARE @FechaMigracion DATETIME;        
        
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;        
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INSUMO_RR' AND Nombre = '2431';        
        
INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)        
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());        
        
SET @IdReporteLog = SCOPE_IDENTITY();        
        
        
TRUNCATE TABLE dbo.RW_R2431;        
        
INSERT INTO dbo.RW_R2431 (        
 IdReporteLog,        
 Periodo,        
 Periodo_Actual,        
 Nombre_Per_Relacionada,      
 RFC_Per_Rel,      
 Per_Juridica,      
 Tipo_Rel_Inst,      
 Tipo_Ope_Bal,      
 Clase_Oper_Rel,      
 Tipo_Moneda,      
 Importe,      
 Concepto_PRR,      
 Nom_Grupo_Emp,      
 ID_Ope_Rel,      
 Caracteristicas,      
 Fecha_Originacion,      
 Fecha_Vencimiento,      
 Monto_Autorizado,      
 Fecha_Disposicion,      
 Monto_Dispuesto,      
 Origen_Otros_Activos       
)        
SELECT        
 @IdReporteLog,        
 @IdPeriodo,        
 CONVERT(CHAR(10), @FechaPeriodo, 126),        
 NombreCNBV,      
 RFC,        
 PER_JURIDICA,      
 TIPO_REL_INST,      
 TIPO_OPE_BAL,      
 CLASE_OPER_REL,      
 TIPO_MONEDA,      
 IMPORTE,      
 CONCEPTO_PRR,      
 NOM_GRUPO_EMP,      
 ID_OPE_REL,      
 CARACTERISTICAS,      
 CONVERT(CHAR(10), FECHA_ORIGINACION, 126),       
 CONVERT(CHAR(10), FECHA_VENCIMIENTO, 126),         
 MONTO_AUTORIZADO,       
 CONVERT(CHAR(10), FECHA_DISPOSICION, 126),        
 MONTO_DISPUESTO,        
 ORIGEN_OTROS_ACTIVOS       
FROM (        
 SELECT      
 pinf.NombreCNBV,       
 per.RFC,      
 ISNULL(PJMA.Codigo,TP.Codigo) as PER_JURIDICA,      
 DRelMA.Codigo AS TIPO_REL_INST,       
 '1' AS TIPO_OPE_BAL,      
 '10201' AS CLASE_OPER_REL,       
 mon.CodigoISO AS TIPO_MONEDA,      
 ISNULL(SUM(SaldoCapitalVigente + InteresVigente + SaldoCapitalVencido + InteresVencido),0) AS IMPORTE ,      
 DRel.Codigo AS CONCEPTO_PRR,      
 pinf.GrupoRiesgo AS NOM_GRUPO_EMP,      
 linea.Codigo AS ID_OPE_REL,      
 CASE WHEN tipoLinea.Codigo NOT IN ('1','2','3','4','5','6','7','8','9')       
 THEN '0' ELSE tipoLinea.Codigo END AS CARACTERISTICAS,      
 MIN(FechaAutorizacionOriginal) FECHA_ORIGINACION,      
 linea.FecVenLinea AS FECHA_VENCIMIENTO,      
 ISNULL(linea.MontoLinea,0) AS MONTO_AUTORIZADO,      
 MIN(cre.FechaDisposicion) AS FECHA_DISPOSICION,      
 SUM(ISNULL(cinf.MontoDispuesto,0)) AS MONTO_DISPUESTO,      
 '2' AS ORIGEN_OTROS_ACTIVOS      
      
 FROM dbo.SICCMX_Persona per      
 INNER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona      
 INNER JOIN dbo.SICCMX_LineaCredito linea ON linea.IdPersona = per.IdPersona        
 INNER JOIN dbo.SICCMX_Credito cre ON cre.IdLineaCredito=linea.IdLineaCredito        
 INNER JOIN dbo.SICCMX_CreditoInfo cinf ON cinf.IdCredito = cre.IdCredito       
 INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = linea.IdMoneda    
 INNER JOIN SICC_DeudorRelacionado DRel on DRel.IdDeudorRelacionado=per.IdDeudorRelacionado       
 LEFT OUTER JOIN SICC_TipoPersonaMA PJMA on PJMA.IdTipoPersona=pinf.IdTipoPersonaMA        
 LEFT OUTER JOIN SICC_TipoPersona TP on TP.IdTipoPersona=pinf.IdTipoPersona        
 LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA DRelMA on DRelMA.IdDeudorRelacionado=per.IdDeudorRelacionadoMA      
       
 LEFT OUTER JOIN SICC_TipoLinea tipoLinea ON tipoLinea.IdTipoLinea=linea.TipoLinea      
 WHERE DRel.Codigo<>'8' /*8 - Persona No Relacionada*/      
GROUP BY pinf.NombreCNBV,       
 per.RFC,      
 PJMA.Codigo,  
 TP.Codigo,      
 DRelMA.Codigo,      
 mon.CodigoISO,      
 DRel.Codigo,      
 pinf.GrupoRiesgo,      
 linea.Codigo,      
 tipoLinea.Codigo,           
 linea.FecVenLinea,      
 linea.MontoLinea
) AS INFO      
UNION ALL    
SELECT        
 @IdReporteLog,        
 @IdPeriodo,        
 CONVERT(CHAR(10), @FechaPeriodo, 126),        
 NombreCNBV,      
 RFC,        
 PER_JURIDICA,      
 TIPO_REL_INST,      
 TIPO_OPE_BAL,      
 CLASE_OPER_REL,      
 TIPO_MONEDA,      
 IMPORTE,      
 CONCEPTO_PRR,      
 NOM_GRUPO_EMP,      
 ID_OPE_REL,      
 CARACTERISTICAS,      
 CONVERT(CHAR(10), FECHA_ORIGINACION, 126),       
 CONVERT(CHAR(10), FECHA_VENCIMIENTO, 126),         
 MONTO_AUTORIZADO,       
 CONVERT(CHAR(10), FECHA_DISPOSICION, 126),        
 MONTO_DISPUESTO,        
 ORIGEN_OTROS_ACTIVOS       
FROM (        
 SELECT      
 pinf.NombreCNBV,       
 per.RFC,      
 ISNULL(PJMA.Codigo,TP.Codigo) as PER_JURIDICA,      
 DRelMA.Codigo AS TIPO_REL_INST,       
 '1' AS TIPO_OPE_BAL,      
 '10201' AS CLASE_OPER_REL,       
 mon.CodigoISO AS TIPO_MONEDA,      
 ISNULL(SUM(SaldoCapitalVigente + InteresVigente + SaldoCapitalVencido + InteresVencido),0) AS IMPORTE ,      
 DRel.Codigo AS CONCEPTO_PRR,      
 pinf.GrupoRiesgo AS NOM_GRUPO_EMP,      
 con.Codigo AS ID_OPE_REL,      
 '0' AS CARACTERISTICAS,      
 FechaOtorgamiento FECHA_ORIGINACION,      
 FechaVencimiento AS FECHA_VENCIMIENTO,      
 ISNULL(MontoLineaAutorizada,0) AS MONTO_AUTORIZADO,      
 FechaDisposicion AS FECHA_DISPOSICION,      
 ISNULL(cinf.ValorOriginalBien,0) AS MONTO_DISPUESTO,      
 '2' AS ORIGEN_OTROS_ACTIVOS      
      
 FROM dbo.SICCMX_Persona per      
 INNER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona        
 INNER JOIN dbo.SICCMX_Consumo con ON con.IdPersona=per.IdPersona     
 INNER JOIN dbo.SICCMX_ConsumoInfo cinf ON cinf.IdConsumo = con.IdConsumo       
 INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = cinf.IdMoneda    
 INNER JOIN SICC_DeudorRelacionado DRel on DRel.IdDeudorRelacionado=per.IdDeudorRelacionado       
 LEFT OUTER JOIN SICC_TipoPersonaMA PJMA on PJMA.IdTipoPersona=pinf.IdTipoPersonaMA  
 LEFT OUTER JOIN SICC_TipoPersona TP on TP.IdTipoPersona=pinf.IdTipoPersona        
 LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA DRelMA on DRelMA.IdDeudorRelacionado=per.IdDeudorRelacionadoMA      
       
 WHERE DRel.Codigo<>'8' /*8 - Persona No Relacionada*/      
GROUP BY pinf.NombreCNBV,       
 per.RFC,      
 PJMA.Codigo,   
 TP.Codigo,     
 DRelMA.Codigo,      
 mon.CodigoISO,      
 DRel.Codigo,      
 pinf.GrupoRiesgo,    
 con.Codigo,    
 FechaOtorgamiento,    
 FechaVencimiento,    
 MontoLineaAutorizada,    
 FechaDisposicion ,      
 cinf.ValorOriginalBien     
       
) AS INFO        
     
UNION ALL    
SELECT        
 @IdReporteLog,        
 @IdPeriodo,        
 CONVERT(CHAR(10), @FechaPeriodo, 126),        
 NombreCNBV,      
 RFC,        
 PER_JURIDICA,      
 TIPO_REL_INST,      
 TIPO_OPE_BAL,      
 CLASE_OPER_REL,      
 TIPO_MONEDA,      
 IMPORTE,      
 CONCEPTO_PRR,      
 NOM_GRUPO_EMP,      
 ID_OPE_REL,      
 CARACTERISTICAS,      
 CONVERT(CHAR(10), FECHA_ORIGINACION, 126),       
 CONVERT(CHAR(10), FECHA_VENCIMIENTO, 126),         
 MONTO_AUTORIZADO,       
 CONVERT(CHAR(10), FECHA_DISPOSICION, 126),        
 MONTO_DISPUESTO,        
 ORIGEN_OTROS_ACTIVOS       
FROM (        
 SELECT      
 pinf.NombreCNBV,       
 per.RFC,      
 ISNULL(PJMA.Codigo,TP.Codigo) as PER_JURIDICA,      
 DRelMA.Codigo AS TIPO_REL_INST,       
 '1' AS TIPO_OPE_BAL,      
 '10201' AS CLASE_OPER_REL,       
 mon.CodigoISO AS TIPO_MONEDA,      
 ISNULL(SUM(SaldoCapitalVigente + InteresVigente + SaldoCapitalVencido + InteresVencido),0) AS IMPORTE ,      
 DRel.Codigo AS CONCEPTO_PRR,      
 pinf.GrupoRiesgo AS NOM_GRUPO_EMP,      
 hip.Codigo AS ID_OPE_REL,      
 '0' AS CARACTERISTICAS,      
 FechaOtorgamiento FECHA_ORIGINACION,      
 FechaVencimiento AS FECHA_VENCIMIENTO,      
 ISNULL(MontoOriginal,0) AS MONTO_AUTORIZADO,      
 FechaOtorgamiento AS FECHA_DISPOSICION,      
 ISNULL(MontoOriginal,0) AS MONTO_DISPUESTO,      
 '2' AS ORIGEN_OTROS_ACTIVOS      
      
 FROM dbo.SICCMX_Persona per      
 INNER JOIN dbo.SICCMX_PersonaInfo pinf ON pinf.IdPersona = per.IdPersona        
 INNER JOIN dbo.SICCMX_Hipotecario hip ON hip.IdPersona=per.IdPersona     
 INNER JOIN dbo.SICCMX_HipotecarioInfo hipInf ON hipInf.IdHipotecario = hip.IdHipotecario    
 INNER JOIN dbo.SICC_Moneda mon ON mon.IdMoneda = hipInf.IdMoneda    
 INNER JOIN SICC_DeudorRelacionado DRel on DRel.IdDeudorRelacionado=per.IdDeudorRelacionado       
 LEFT OUTER JOIN SICC_TipoPersonaMA PJMA on PJMA.IdTipoPersona=pinf.IdTipoPersonaMA  
 LEFT OUTER JOIN SICC_TipoPersona TP on TP.IdTipoPersona=pinf.IdTipoPersona        
 LEFT OUTER JOIN dbo.SICC_DeudorRelacionadoMA DRelMA on DRelMA.IdDeudorRelacionado=per.IdDeudorRelacionadoMA      
       
 WHERE DRel.Codigo<>'8' /*8 - Persona No Relacionada*/      
GROUP BY pinf.NombreCNBV,       
 per.RFC,      
 PJMA.Codigo,   
 TP.Codigo,     
 DRelMA.Codigo,      
 mon.CodigoISO,      
 DRel.Codigo,      
 pinf.GrupoRiesgo,    
  hip.Codigo,    
 FechaOtorgamiento,    
 FechaVencimiento,    
 FechaOtorgamiento ,      
 MontoOriginal    
       
) AS INFO          
      
UPDATE dbo.RW_R2431      
SET Monto_Ope_Rel=(SELECT SUM(CAST(Importe AS MONEY)) FROM dbo.RW_R2431)        
        
        
SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R2431 WHERE IdReporteLog = @IdReporteLog;        
SELECT @TotalSaldos = 0;        
SELECT @TotalIntereses = 0;        
        
SET @FechaMigracion = ( SELECT MAX( Fecha ) FROM MIGRACION_ProcesoLog );        
        
UPDATE dbo.RW_ReporteLog        
SET        
 TotalRegistros = @TotalRegistros,        
 TotalSaldos = @TotalSaldos,        
 TotalIntereses = @TotalIntereses,        
 FechaCalculoProcesos = GETDATE(),        
 FechaImportacionDatos = @FechaMigracion,        
 IdFuenteDatos = 1        
WHERE IdReporteLog = @IdReporteLog; 
GO
