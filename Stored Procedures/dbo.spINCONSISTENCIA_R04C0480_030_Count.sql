SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_030_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Rotación de Activos Totales (dat_rotac_activos_tot) es >= 2.16, entonces el Puntaje Asignado por Rotación de Activos Totales (cve_ptaje_rotac_activo_tot) debe ser = 81

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(RotActTot AS DECIMAL(10,6)) >= 2.16 AND ISNULL(P_RotActTot,'') <> '81';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
