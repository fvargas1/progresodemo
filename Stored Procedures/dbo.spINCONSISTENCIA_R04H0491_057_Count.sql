SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_057_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se deberá validar la correspondencia para entre las claves "Tasa de Referencia" y "Tipo de Tasa de Interés del Crédito":
-- - Si la "Tasa de Referencia" es 600, el "Tipo de Tasa de Interés del Crédito" debe ser 1.
-- - Si la "Tasa de Referencia" es diferente de 600, el "Tipo de Tasa de Interés del Crédito" debe ser 2 o 3.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE (TasaRef = '600' AND TipoTasaInteres <> '1') OR (TasaRef <> '600' AND TipoTasaInteres NOT IN ('2','3'));

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
