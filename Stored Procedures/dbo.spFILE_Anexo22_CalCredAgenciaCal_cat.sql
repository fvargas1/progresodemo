SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_CalCredAgenciaCal_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_CalCredAgenciaCal_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo22
SET errorCatalogo = 1
WHERE LTRIM(CalCredAgenciaCal) NOT IN ('','0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCliente,
 'CalCredAgenciaCal',
 CalCredAgenciaCal,
 2,
 @Detalle
FROM dbo.FILE_Anexo22
WHERE LTRIM(CalCredAgenciaCal) NOT IN ('','0','1');
GO
