SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_043]
AS

BEGIN

-- El Tipo de garantía real debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.TipoGarantia
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA cat ON ISNULL(r.TipoGarantia,'') = cat.Codigo
WHERE cat.IdTipoGarantia IS NULL;

END
GO
