SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_SP_Hipotecario]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '1';


-- Calculamos la SP cuando el ATR es mayor a lo maximo para cada Metodologia
UPDATE hrv
SET SP = 1
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR >= const.ATRSP
WHERE const.IdMetodologia = @IdMetodologia;


-- Calculamos la SP cuando el ATR es menor a lo maximo para cada Metodologia
UPDATE hrv
SET SP = CASE WHEN ((1 - vp.TRi) * 0.8) > 0.10 THEN (1 - vp.TRi) * 0.8 ELSE 0.10 END
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp ON vp.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR < const.ATRSP
WHERE const.IdMetodologia = @IdMetodologia;
GO
