SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_TasaRetLab_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_TasaRetLab_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo22
SET errorFormato = 1
WHERE LEN(TasaRetLab)>0 AND (ISNUMERIC(ISNULL(TasaRetLab,'0')) = 0 OR TasaRetLab LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCliente,
	'TasaRetLab',
	TasaRetLab,
	1,
	@Detalle
FROM dbo.FILE_Anexo22
WHERE LEN(TasaRetLab)>0 AND (ISNUMERIC(ISNULL(TasaRetLab,'0')) = 0 OR TasaRetLab LIKE '%[^0-9 .]%');
GO
