SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_057]
AS

BEGIN

-- Se deberá validar la correspondencia para entre las claves "Tasa de Referencia" y "Tipo de Tasa de Interés del Crédito":
-- - Si la "Tasa de Referencia" es 600, el "Tipo de Tasa de Interés del Crédito" debe ser 1.
-- - Si la "Tasa de Referencia" es diferente de 600, el "Tipo de Tasa de Interés del Crédito" debe ser 2 o 3.

SELECT CodigoCredito, CodigoCreditoCNBV, TasaRef, TipoTasaInteres
FROM dbo.RW_R04H0491
WHERE (TasaRef = '600' AND TipoTasaInteres <> '1') OR (TasaRef <> '600' AND TipoTasaInteres NOT IN ('2','3'));

END
GO
