SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_027_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Sí es un crédito sin fuente de pago propia (cve_proy_pago_anexo_19 = 2), entonces la Exposición al Incumplimiento
-- (dat_exposicion_incumplimiento) debe ser MAYOR O IGUAL a la Responsabilidad Total (dat_responsabilidad_total).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(ProyectoInversion,'') = '2' AND CAST(ISNULL(NULLIF(EITotal,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
