SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0417Datos_CalculandoConceptos]
AS
UPDATE dat
SET Concepto = conf.Concepto
FROM R04.[0417Datos] dat
INNER JOIN R04.[0417Configuracion] conf ON conf.TipoProducto = dat.CodigoProducto AND dat.Calificacion LIKE conf.Calificacion + '%' AND conf.FueraBalance = 0
WHERE ISNULL(dat.CartaDeCredito,0) = 0;

-- OPERACIONES FUERA DE BALANCE
UPDATE dat
SET Concepto = conf.Concepto
FROM R04.[0417Datos] dat
INNER JOIN R04.[0417Configuracion] conf ON conf.TipoProducto = dat.CodigoProducto AND dat.Calificacion LIKE conf.Calificacion + '%' AND conf.FueraBalance = 1
WHERE ISNULL(dat.CartaDeCredito,0) = 1;
GO
