SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_F_MonTotPagInfonavitUltBimestre_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_F_MonTotPagInfonavitUltBimestre_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo21
SET errorFormato = 1
WHERE (LEN(F_MonTotPagInfonavitUltBimestre)>0 AND ISDATE(F_MonTotPagInfonavitUltBimestre) = 0) OR F_MonTotPagInfonavitUltBimestre > @FechaPeriodo;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_MonTotPagInfonavitUltBimestre',
 F_MonTotPagInfonavitUltBimestre,
 1,
 @Detalle
FROM dbo.FILE_Anexo21
WHERE (LEN(F_MonTotPagInfonavitUltBimestre)>0 AND ISDATE(F_MonTotPagInfonavitUltBimestre) = 0) OR F_MonTotPagInfonavitUltBimestre > @FechaPeriodo;
GO
