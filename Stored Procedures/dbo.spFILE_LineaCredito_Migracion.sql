SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_Migracion]
AS

UPDATE lna
SET IdPersona = per.IdPersona,
 FechaDisposicion = NULLIF(f.FechaDisposicion,''),
 IdMoneda = mon.IdMoneda,
 IdDestino = dest.IdDestino,
 IdTasaReferencia = tasaRef.IdTasaReferencia,
 MontoLinea = NULLIF(f.MontoLinea,''),
 FrecuenciaRevisionTasa = NULLIF(f.FrecuenciaRevisionTasa,''),
 IdTipoAlta = tipoAl.IdTipoAlta,
 IdDisposicion = dispCre.IdDisposicion,
 AjusteTasaReferencia = NULLIF(f.AjusteTasa,''),
 IdInstitucionFondea = inst.IdInstitucion,
 ComisionesCobradas = NULLIF(f.ComisionesCobradas,''),
 IdTipoBaja = tipoBa.IdTipoBaja,
 ImporteQuebranto = NULLIF(f.ImporteQuebranto,''),
 SaldoInicial = NULLIF(f.SaldoInicial,''),
 IdPeriodicidadCapital = perCap.IdPeriodicidadCapital,
 IdPeriodicidadInteres = perInt.IdPeriodicidadInteres,
 CodigoCreditoReestructurado = LTRIM(RTRIM(f.CodigoCreditoReestructurado)),
 ProductoComercial = tcc.IdTipoCredito,
 Posicion = pos.IdPosicion,
 TipoLinea = tln.IdTipoLinea,
 TipoOperacion = tpoOper.IdTipoOperacion,
 FecMaxDis = NULLIF(f.FecMaxDis,''),
 PorcPartFederal = NULLIF(f.PorcPartFederal,''),
 MontoPagEfeCap = NULLIF(f.MontoPagEfeCap,''),
 MontoPagEfeInt = NULLIF(f.MontoPagEfeInt,''),
 MontoPagEfeCom = NULLIF(f.MontoPagEfeCom,''),
 MontoPagEfeIntMor = NULLIF(f.MontoPagEfeIntMor,''),
 ComDispTasa = NULLIF(f.ComDispTasa,''),
 ComDispMonto = NULLIF(f.ComDispMonto,''),
 GastosOrigTasa = NULLIF(f.GastosOrigTasa,''),
 ResTotalInicioPer = NULLIF(f.ResTotalInicioPer,''),
 MontoQuitasCastQue = NULLIF(f.MontoQuitasCastQue,''),
 MontoBonificacionDesc = NULLIF(f.MontoBonificacionDesc,''),
 MesesGraciaCap = CAST(NULLIF(f.MesesGraciaCap,'') AS DECIMAL),
 MesesGraciaIntereses = CAST(NULLIF(f.MesesGraciaIntereses,'') AS DECIMAL),
 NumEmpLoc = LTRIM(RTRIM(f.NumEmpLoc)),
 NumEmpLocFedSHCP = LTRIM(RTRIM(f.NumEmpLocFedSHCP)),
 FecVenLinea = NULLIF(f.FecVenLinea,''),
 CAT = NULLIF(f.CAT,''),
 MontoLineaSinA = NULLIF(f.MontoLineaSinA,''),
 MontoPrimasAnuales = NULLIF(f.MontoPrimasAnuales,''),
 EsPadre = LTRIM(RTRIM(f.EsPadre)),
 IdMunicipioDestino = (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadDestino)),
 IdActividadEconomicaDestino = actEco.IdActividadEconomica,
 FolioConsultaBuro = LTRIM(RTRIM(f.FolioConsultaBuro)),
 CodigoCNBV = LTRIM(RTRIM(f.CodigoCNBV)),
 Fuente = 'LN',
 EsMultimoneda = NULLIF(f.EsMultimoneda,''),
 IdDestinoCreditoMA = destMA.IdDestinoCredito,
 IdTipoAltaMA = tipoAlMA.IdTipoAlta,
 IdDisposicionMA = dispCreMA.IdDisposicion,
 IdPeriodicidadCapitalMA = perCapMA.IdPeriodicidadCapital,
 IdPeriodicidadInteresMA = perIntMA.IdPeriodicidadInteres,
 IdTipoBajaMA = tipoBaMA.IdTipoBaja,
 NumeroAgrupacion = LTRIM(RTRIM(f.NumeroAgrupacion)),
 MontoBancaDesarrollo = NULLIF(f.MontoBancaDesarrollo, ''),
 ComisionDelMes = NULLIF(f.Comision,'')
FROM dbo.SICCMX_LineaCredito lna
INNER JOIN dbo.FILE_LineaCredito f ON lna.Codigo = LTRIM(f.NumeroLinea)
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
LEFT OUTER JOIN dbo.SICC_Destino dest ON dest.Codigo = LTRIM(f.DestinoCreditoComercial)
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON LTRIM(f.TasaReferencia) = tasaRef.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAlta tipoAl ON tipoAl.Codigo = LTRIM(f.TipoAlta)
LEFT OUTER JOIN dbo.SICC_DisposicionCredito dispCre ON dispCre.Codigo = LTRIM(f.DisposicionCredito)
LEFT OUTER JOIN dbo.SICC_Institucion inst ON inst.Codigo = LTRIM(f.InstitutoFondea)
LEFT OUTER JOIN dbo.SICC_TipoBaja tipoBa ON tipoBa.Codigo = LTRIM(f.TipoBaja)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON perCap.Codigo = LTRIM(f.PeriodicidadCapital)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON perInt.Codigo = LTRIM(f.PeriodicidadInteres)
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON tcc.Codigo = LTRIM(f.ProductoComercial)
LEFT OUTER JOIN dbo.SICC_Posicion pos ON pos.Codigo = LTRIM(f.Posicion)LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON tln.Codigo = LTRIM(f.TipoLinea)
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON tpoOper.Codigo = LTRIM(f.TipoOperacion)
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON LTRIM(f.ActividadEconomicaDestino) = actEco.Codigo
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON destMA.Codigo = LTRIM(f.DestinoCreditoMA)
LEFT OUTER JOIN dbo.SICC_TipoAltaMA tipoAlMA ON tipoAlMA.Codigo = LTRIM(f.TipoAltaMA)
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dispCreMA ON dispCreMA.Codigo = LTRIM(f.DisposicionCreditoMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCapMA ON perCapMA.Codigo = LTRIM(f.PeriodicidadCapitalMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perIntMA ON perIntMA.Codigo = LTRIM(f.PeriodicidadInteresMA)
LEFT OUTER JOIN dbo.SICC_TipoBajaMA tipoBaMA ON tipoBaMA.Codigo = LTRIM(f.TipoBajaMA)
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;


INSERT INTO dbo.SICCMX_LineaCredito (
 Codigo,
 IdPersona,
 FechaDisposicion,
 IdMoneda,
 IdDestino,
 IdTasaReferencia,
 MontoLinea,
 FrecuenciaRevisionTasa,
 IdTipoAlta,
 IdDisposicion,
 AjusteTasaReferencia,
 IdInstitucionFondea,
 ComisionesCobradas,
 IdTipoBaja,
 ImporteQuebranto,
 SaldoInicial,
 IdPeriodicidadCapital,
 IdPeriodicidadInteres,
 CodigoCreditoReestructurado,
 ProductoComercial,
 Posicion,
 TipoLinea,
 TipoOperacion,
 FecMaxDis,
 PorcPartFederal,
 MontoPagEfeCap,
 MontoPagEfeInt,
 MontoPagEfeCom,
 MontoPagEfeIntMor,
 ComDispTasa,
 ComDispMonto,
 GastosOrigTasa,
 ResTotalInicioPer,
 MontoQuitasCastQue,
 MontoBonificacionDesc,
 MesesGraciaCap,
 MesesGraciaIntereses,
 NumEmpLoc,
 NumEmpLocFedSHCP,
 FecVenLinea,
 CAT,
 MontoLineaSinA,
 MontoPrimasAnuales,
 EsPadre,
 IdMunicipioDestino,
 IdActividadEconomicaDestino,
 FolioConsultaBuro,
 CodigoCNBV,
 Fuente,
 EsMultimoneda,
 IdDestinoCreditoMA,
 IdTipoAltaMA,
 IdDisposicionMA,
 IdPeriodicidadCapitalMA,
 IdPeriodicidadInteresMA,
 IdTipoBajaMA,
 NumeroAgrupacion,
 MontoBancaDesarrollo,
 ComisionDelMes
)
SELECT
 f.NumeroLinea,
 per.IdPersona,
 NULLIF(f.FechaDisposicion,''),
 mon.IdMoneda,
 dest.IdDestino,
 tasaRef.IdTasaReferencia,
 NULLIF(f.MontoLinea,''),
 NULLIF(f.FrecuenciaRevisionTasa,''),
 tipoAl.IdTipoAlta,
 dispCre.IdDisposicion,
 NULLIF(f.AjusteTasa,''),
 inst.IdInstitucion,
 NULLIF(f.ComisionesCobradas,''),
 tipoBa.IdTipoBaja,
 NULLIF(f.ImporteQuebranto,''),
 NULLIF(f.SaldoInicial,''),
 perCap.IdPeriodicidadCapital,
 perInt.IdPeriodicidadInteres,
 LTRIM(RTRIM(f.CodigoCreditoReestructurado)),
 tcc.IdTipoCredito,
 pos.IdPosicion,
 tln.IdTipoLinea,
 tpoOper.IdTipoOperacion,
 NULLIF(f.FecMaxDis,''),
 NULLIF(f.PorcPartFederal,''),
 NULLIF(f.MontoPagEfeCap,''),
 NULLIF(f.MontoPagEfeInt,''),
 NULLIF(f.MontoPagEfeCom,''),
 NULLIF(f.MontoPagEfeIntMor,''),
 NULLIF(f.ComDispTasa,''),
 NULLIF(f.ComDispMonto,''),
 NULLIF(f.GastosOrigTasa,''),
 NULLIF(f.ResTotalInicioPer,''),
 NULLIF(f.MontoQuitasCastQue,''),
 NULLIF(f.MontoBonificacionDesc,''),
 CAST(NULLIF(f.MesesGraciaCap,'') AS DECIMAL),
 CAST(NULLIF(f.MesesGraciaIntereses,'') AS DECIMAL),
 LTRIM(RTRIM(f.NumEmpLoc)),
 LTRIM(RTRIM(f.NumEmpLocFedSHCP)),
 NULLIF(f.FecVenLinea,''),
 NULLIF(f.CAT,''),
 NULLIF(f.MontoLineaSinA,''),
 NULLIF(f.MontoPrimasAnuales,''),
 LTRIM(RTRIM(f.EsPadre)),
 (SELECT TOP 1 IdLocalidad FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadDestino)),
 actEco.IdActividadEconomica,
 LTRIM(RTRIM(f.FolioConsultaBuro)),
 LTRIM(RTRIM(f.CodigoCNBV)),
 'LN',
 NULLIF(f.EsMultimoneda,''),
 destMA.IdDestinoCredito,
 tipoAlMA.IdTipoAlta,
 dispCreMA.IdDisposicion,
 perCapMA.IdPeriodicidadCapital,
 perIntMA.IdPeriodicidadInteres,
 tipoBaMA.IdTipoBaja,
 LTRIM(RTRIM(f.NumeroAgrupacion)),
 NULLIF(f.MontoBancaDesarrollo, ''),
 NULLIF(f.Comision,'')
FROM dbo.FILE_LineaCredito f
INNER JOIN dbo.SICCMX_Persona per ON per.Codigo = LTRIM(f.CodigoCliente)
LEFT OUTER JOIN dbo.SICC_Moneda mon ON mon.Codigo = LTRIM(f.Moneda)
LEFT OUTER JOIN dbo.SICC_Destino dest ON dest.Codigo = LTRIM(f.DestinoCreditoComercial)
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasaRef ON LTRIM(f.TasaReferencia) = tasaRef.Codigo
LEFT OUTER JOIN dbo.SICC_TipoAlta tipoAl ON tipoAl.Codigo = LTRIM(f.TipoAlta)
LEFT OUTER JOIN dbo.SICC_DisposicionCredito dispCre ON dispCre.Codigo = LTRIM(f.DisposicionCredito)
LEFT OUTER JOIN dbo.SICC_Institucion inst ON inst.Codigo = LTRIM(f.InstitutoFondea)
LEFT OUTER JOIN dbo.SICC_TipoBaja tipoBa ON tipoBa.Codigo = LTRIM(f.TipoBaja)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON perCap.Codigo = LTRIM(f.PeriodicidadCapital)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteres perInt ON perInt.Codigo = LTRIM(f.PeriodicidadInteres)
LEFT OUTER JOIN dbo.SICC_TipoCreditoComercial tcc ON tcc.Codigo = LTRIM(f.ProductoComercial)
LEFT OUTER JOIN dbo.SICC_Posicion pos ON pos.Codigo = LTRIM(f.Posicion)
LEFT OUTER JOIN dbo.SICC_TipoLinea tln ON tln.Codigo = LTRIM(f.TipoLinea)
LEFT OUTER JOIN dbo.SICC_TipoOperacion tpoOper ON tpoOper.Codigo = LTRIM(f.TipoOperacion)
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEco ON LTRIM(f.ActividadEconomicaDestino) = actEco.Codigo
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA destMA ON destMA.Codigo = LTRIM(f.DestinoCreditoMA)
LEFT OUTER JOIN dbo.SICC_TipoAltaMA tipoAlMA ON tipoAlMA.Codigo = LTRIM(f.TipoAltaMA)
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA dispCreMA ON dispCreMA.Codigo = LTRIM(f.DisposicionCreditoMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapitalMA perCapMA ON perCapMA.Codigo = LTRIM(f.PeriodicidadCapitalMA)
LEFT OUTER JOIN dbo.SICC_PeriodicidadInteresMA perIntMA ON perIntMA.Codigo = LTRIM(f.PeriodicidadInteresMA)
LEFT OUTER JOIN dbo.SICC_TipoBajaMA tipoBaMA ON tipoBaMA.Codigo = LTRIM(f.TipoBajaMA)
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lna ON lna.Codigo = f.NumeroLinea
WHERE lna.Codigo IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;

-- CAMPOS CAMBIO REGULATORIO 2016
UPDATE lin
SET
 MontoCastigos = NULLIF(f.MontoCastigos,''),
 MontoQuebrantos = NULLIF(f.MontoQuebrantos,''),
 MontoDescuentos = NULLIF(f.MontoDescuentos,''),
 MontoDacion = NULLIF(f.MontoDacion,''),
 LocalidadDestino = LTRIM(RTRIM(f.LocalidadDestino)),
 EstadoDestino = LTRIM(RTRIM(f.EstadoDestino)),
 MontoCondonacion = NULLIF(f.MontoCondonacion,'')
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.FILE_LineaCredito f ON lin.Codigo = LTRIM(f.NumeroLinea)
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO
