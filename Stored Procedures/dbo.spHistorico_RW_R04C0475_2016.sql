SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0475_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0475_2016);

DELETE FROM Historico.RW_R04C0475_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0475_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
	HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_AntSocCred, P_QuitCastReest, P_PorcPagoInstNoBanc,
	P_PorcPagoEntComer, P_CredAbInstBanc, P_MonMaxCred, P_MesesUltCredAb, P_PorcPagoInstBanc, P_PorcPagoInstBanc29, P_PorcPagoInstBanc90, P_DiasMoraInstBanc,
	P_PagosInstBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_IndPersFid, P_ProcOrigAdmon, AntSocCred, PorcPagoInstNoBanc, PorcPagoEntComer,
	CredAbInstBanc, MonMaxCred, MesesUltCredAb, PorcPagoInstBanc, PorcPagoInstBanc29, PorcPagoInstBanc90, DiasMoraInstBanc, PagosInstBanc, PagosInfonavit,
	DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, IndPersFid, VentasNetasTotales
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	SinAtrasos,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	ID_PI100,
	GarantiaLeyFederal,
	CumpleCritContGral,
	P_AntSocCred,
	P_QuitCastReest,
	P_PorcPagoInstNoBanc,
	P_PorcPagoEntComer,
	P_CredAbInstBanc,
	P_MonMaxCred,
	P_MesesUltCredAb,
	P_PorcPagoInstBanc,
	P_PorcPagoInstBanc29,
	P_PorcPagoInstBanc90,
	P_DiasMoraInstBanc,
	P_PagosInstBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_IndPersFid,
	P_ProcOrigAdmon,
	AntSocCred,
	PorcPagoInstNoBanc,
	PorcPagoEntComer,
	CredAbInstBanc,
	MonMaxCred,
	MesesUltCredAb,
	PorcPagoInstBanc,
	PorcPagoInstBanc29,
	PorcPagoInstBanc90,
	DiasMoraInstBanc,
	PagosInstBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	IndPersFid,
	VentasNetasTotales
FROM dbo.RW_R04C0475_2016
WHERE IdReporteLog = @IdReporteLog;
GO
