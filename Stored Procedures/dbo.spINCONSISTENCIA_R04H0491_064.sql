SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_064]
AS

BEGIN

-- Se deberá validar la correspondencia entre el "Porcentaje que Cubre el Seguro de Crédito a la Vivienda" y la clave de la "Entidad que Otorga el Seguro de Crédito a la Vivienda":
-- - Si el "Porcentaje que cubre el seguro de crédito a la vivienda" es mayor a 0% y menor o igual a 100%, la "Entidad que Otorga el Seguro" debe ser 022601, 022602, 022603, 031001 ó 022023.

SELECT CodigoCredito, CodigoCreditoCNBV, PorcentajeCubiertoSeguro, EntidadSeguro
FROM dbo.RW_R04H0491
WHERE CAST(ISNULL(NULLIF(PorcentajeCubiertoSeguro,''),'0') AS DECIMAL(8,2)) BETWEEN 0.01 AND 100 AND EntidadSeguro NOT IN ('022601','022602','022603','031001','022023');

END
GO
