SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_056]
AS

BEGIN

-- La "TASA DE REFERENCIA" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TasaRef
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_TasaReferenciaHipotecario cat ON ISNULL(r.TasaRef,'') = cat.Codigo
WHERE cat.IdTasaReferenciaHipotecario IS NULL;

END
GO
