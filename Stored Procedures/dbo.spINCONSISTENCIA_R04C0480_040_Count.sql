SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_040_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Tasa de Retención Laboral (dat_tasa_retencion_laboral) es >=35 y < 66.6,
-- entonces el Puntaje Asignado por Tasa de Retención Laboral (cve_ptaje_tasa_retenc_laboral) debe ser = 51

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(TasaRetLab AS DECIMAL(10,6)) >= 35 AND CAST(TasaRetLab AS DECIMAL(10,6)) < 66.6 AND ISNULL(P_TasaRetLab,'') <> '51';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
