SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_ValorGarantiaProyectado_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Garantia_ValorGarantiaProyectado_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Garantia
SET errorFormato = 1
WHERE LEN(ValorGarantiaProyectado)>0 AND (ISNUMERIC(ISNULL(ValorGarantiaProyectado,'0')) = 0 OR ValorGarantiaProyectado LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Garantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoGarantia,
 'ValorGarantiaProyectado',
 ValorGarantiaProyectado,
 1,
 @Detalle
FROM dbo.FILE_Garantia
WHERE LEN(ValorGarantiaProyectado)>0 AND (ISNUMERIC(ISNULL(ValorGarantiaProyectado,'0')) = 0 OR ValorGarantiaProyectado LIKE '%[^0-9 .]%');
GO
