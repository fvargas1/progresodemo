SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Detalle_Canasta_Deudor_H]
	@CodigoPersona VARCHAR(50),
	@IdPeriodoHistorico BIGINT
AS
SELECT
 rw.NumeroLinea,
 rw.CodigoCredito,
 rw.TipoGarantia AS Tipo,
 rw.PrctSinAjust AS PorcCobSinAjust,
 rw.PrctAjust AS PorcCobAjust,
 rw.[PI],
 rw.SP,
 rw.MontoRecuperacion,
 rw.Reserva
FROM Historico.RW_CedulaNMC_Canasta rw
WHERE rw.IdPeriodoHistorico = @IdPeriodoHistorico AND rw.CodigoPersona = @CodigoPersona
ORDER BY NumeroLinea, CodigoCredito, Posicion;
GO
