SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_117]

AS



BEGIN



-- El PORCENTAJE DE PAGOS A INSTITUCIONES BANCARIAS DE 1 A 29 DÍAS DE ATRASO EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoInstBanc29

FROM dbo.RW_VW_R04C0465_INC

WHERE LEN(PorcPagoInstBanc29) > 0 AND PorcPagoInstBanc29 <> '-99'

	AND (CHARINDEX('.',PorcPagoInstBanc29) = 0 OR CHARINDEX('.',PorcPagoInstBanc29) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc29, CHARINDEX('.', PorcPagoInstBanc29) + 1, LEN(PorcPagoInstBanc29)))) <> 6);



END
GO
