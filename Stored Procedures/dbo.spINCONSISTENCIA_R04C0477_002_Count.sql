SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0477_002_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La Resp. Total al Inicio del Periodo debe ser >= Saldo del Principal al Inicio del Periodo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0477_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
