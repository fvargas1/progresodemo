SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_MetodologiaDuplicada]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_MetodologiaDuplicada';

IF @Requerido = 1
BEGIN
UPDATE a20
SET errorCatalogo = 1
FROM dbo.FILE_Anexo20 a20
INNER JOIN (
 SELECT CodigoCliente FROM dbo.FILE_Anexo18
 UNION
 SELECT CodigoCliente FROM dbo.FILE_Anexo21
 UNION
 SELECT CodigoCliente FROM dbo.FILE_Anexo22
) anx ON LTRIM(a20.CodigoCliente) = LTRIM(anx.CodigoCliente);

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a20.CodigoCliente,
 'CodigoCliente',
 a20.CodigoCliente,
 2,
 @Detalle + 'Anexo18'
FROM dbo.FILE_Anexo20 a20
INNER JOIN dbo.FILE_Anexo18 a18 ON LTRIM(a20.CodigoCliente) = LTRIM(a18.CodigoCliente);


INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a20.CodigoCliente,
 'CodigoCliente',
 a20.CodigoCliente,
 2,
 @Detalle + 'Anexo21'
FROM dbo.FILE_Anexo20 a20
INNER JOIN dbo.FILE_Anexo21 a21 ON LTRIM(a20.CodigoCliente) = LTRIM(a21.CodigoCliente);


INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 a20.CodigoCliente,
 'CodigoCliente',
 a20.CodigoCliente,
 2,
 @Detalle + 'Anexo22'
FROM dbo.FILE_Anexo20 a20
INNER JOIN dbo.FILE_Anexo22 a22 ON LTRIM(a20.CodigoCliente) = LTRIM(a22.CodigoCliente);
GO
