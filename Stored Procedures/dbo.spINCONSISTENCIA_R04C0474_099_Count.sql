SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_099_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que si la Situación del Crédito (cve_situacion_credito) es IGUAL a 1, el dato reportado en la columna de Número de días Vencidos (dat_num_vencidos) sea MENOR o IGUAL a 90.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;
SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC
WHERE ISNULL(SituacionCredito,'') = '1' AND CAST(ISNULL(NULLIF(DiasAtraso,''),'-1') AS DECIMAL) > 90

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
