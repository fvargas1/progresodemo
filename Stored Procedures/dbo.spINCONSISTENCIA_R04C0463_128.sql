SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_128]
AS

BEGIN

-- Validar que la Clave de la Institución contenida en el ID Metodología CNBV (dat_id_credito_met_cnbv) posiciones 2 a la 7, exista en el catálogo de instituciones.

SELECT
	r04.CodigoCredito,
	r04.CodigoCreditoCNBV,
	SUBSTRING(r04.CodigoCreditoCNBV,2,6) AS CodigoInstitucion
FROM dbo.RW_VW_R04C0463_INC r04
LEFT OUTER JOIN dbo.SICC_Institucion ins ON SUBSTRING(r04.CodigoCreditoCNBV,2,6) = ins.Codigo
WHERE ins.IdInstitucion IS NULL

END

GO
