SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_052]
AS

BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Inst Financ Bcarias con 60 o más días de atraso en los últimos 24 meses (cve_ptaje_pgo_bco_60_dias_atra) es = 54,
-- entonces el Porcentaje de Pagos a Inst Financ Bcarias con 60 o más días de atraso en los últimos 24 meses (dat_porcen_pgo_bcos_60_dias) debe ser = 0

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstBanc,
	P_PorcPagoInstBanc AS Puntos_PorcPagoInstBanc
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoInstBanc,'') = '54' AND CAST(PorcPagoInstBanc AS DECIMAL(10,6)) <> 0;

END

GO
