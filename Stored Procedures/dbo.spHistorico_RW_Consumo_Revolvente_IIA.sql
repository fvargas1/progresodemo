SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_Revolvente_IIA]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_Revolvente_IIA);

DELETE FROM Historico.RW_Consumo_Revolvente_IIA WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_Revolvente_IIA (
 IdPeriodoHistorico, Producto, FolioCredito, LimCredito, SaldoTot, PagoNGI, SaldoRev, InteresRev, SaldoPMSI, Meses, PagoMin, TasaRev, SaldoPCI,
 InteresPCI, SaldoPagar, PagoReal, LimCreditoA, PagoExige, ImpagosC, ImpagoSum, MesApert, SaldoCont, Situacion, ProbInc, SevPer, ExpInc,
 MReserv, Relacion, ClasCont, CveCons, limcreditocalif, montopagarinst, montopagarsic, mesantig, mesesic, segmento, gveces, garantia, Catcuenta,
 indicadorcat, FolioCliente, CP, comtotal, comtardio, pagonginicio, pagoexigepsi
)
SELECT
	@IdPeriodoHistorico,
	Producto,
	FolioCredito,
	LimCredito,
	SaldoTot,
	PagoNGI,
	SaldoRev,
	InteresRev,
	SaldoPMSI,
	Meses,
	PagoMin,
	TasaRev,
	SaldoPCI,
	InteresPCI,
	SaldoPagar,
	PagoReal,
	LimCreditoA,
	PagoExige,
	ImpagosC,
	ImpagoSum,
	MesApert,
	SaldoCont,
	Situacion,
	ProbInc,
	SevPer,
	ExpInc,
	MReserv,
	Relacion,
	ClasCont,
	CveCons,
	limcreditocalif,
	montopagarinst,
	montopagarsic,
	mesantig,
	mesesic,
	segmento,
	gveces,
	garantia,
	Catcuenta,
	indicadorcat,
	FolioCliente,
	CP,
	comtotal,
	comtardio,
	pagonginicio,
	pagoexigepsi
FROM dbo.RW_Consumo_Revolvente_IIA
WHERE IdReporteLog = @IdReporteLog;
GO
