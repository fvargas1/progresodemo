SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0476_010]
AS

BEGIN

-- Si se reporta algún número de garantías financieras, entonces el valor de éstas debe ser mayor a cero.
-- Si dat_num_gtias_reales_financ>0, entonces  dat_valor_conta_gtia_real_fin > 0

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumeroGarRealFin,
	ValorGarRealFin
FROM dbo.RW_VW_R04C0476_INC
WHERE CAST(NumeroGarRealFin AS DECIMAL) > 0 AND CAST(ValorGarRealFin AS DECIMAL) = 0;

END


GO
