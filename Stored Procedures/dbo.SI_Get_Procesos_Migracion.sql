SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Get_Procesos_Migracion]
AS
SELECT DISTINCT
 arc.Codename,
 arc.Nombre,
 REPLACE(CONVERT(VARCHAR, CAST(SUM(CASE WHEN val.Estatus=2 THEN 1 ELSE 0 END) OVER(PARTITION BY arc.Codename) AS MONEY), 1),'.00','') AS wrn,
 REPLACE(CONVERT(VARCHAR, CAST(SUM(CASE WHEN val.Estatus=3 THEN 1 ELSE 0 END) OVER(PARTITION BY arc.Codename) AS MONEY), 1),'.00','') AS req,
 REPLACE(CONVERT(VARCHAR, CAST(SUM(CASE WHEN val.Estatus=4 THEN 1 ELSE 0 END) OVER(PARTITION BY arc.Codename) AS MONEY), 1),'.00','') AS err,
 REPLACE(CONVERT(VARCHAR, CAST(arc.ErroresMigracion AS MONEY), 1),'.00','') AS NumErrores,
 arc.ValidationStatus,
 arc.Position
FROM dbo.MIGRACION_Archivo arc
INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
WHERE arc.Activo=1 AND val.Activo=1 AND val.Estatus NOT IN (0,-1)
ORDER BY arc.Position
GO
