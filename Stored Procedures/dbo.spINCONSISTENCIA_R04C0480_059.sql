SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_059]
AS

BEGIN

-- Validar que los Nombres no contengan caracteres especiales

SELECT
	CodigoPersona,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor
FROM dbo.RW_R04C0480
WHERE NombrePersona LIKE '%[^a-z,.0-9 ]%' OR NombrePersona LIKE '%[áéíóú]%';

END
GO
