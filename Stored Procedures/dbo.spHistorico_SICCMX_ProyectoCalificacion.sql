SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_ProyectoCalificacion]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_ProyectoCalificacion WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_ProyectoCalificacion (
 IdPeriodoHistorico,
 Proyecto,
 Fecha,
 SobreCosto,
 MontoCubierto,
 Meses,
 MesesAdicionales,
 VPTotal,
 UpAcumulada,
 Etapa,
 ValorGarantia,
 SobrecostoGarantia,
 ReservaConstruccion,
 ReservaOperacion,
 ExposicionSobrecosto,
 PorcenajeSobrecosto,
 CalificacionSobrecosto,
 PorcentajeCorrida,
 CalificacionCorrida,
 PorcentajeFinal,
 CalificacionFinal,
 SaldoInsoluto
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 pry.CodigoProyecto,
 pc.Fecha,
 pc.SobreCosto,
 pc.MontoCubierto,
 pc.Meses,
 pc.MesesAdicionales,
 pc.VPTotal,
 pc.UpAcumulada,
 etp.Codigo,
 pc.ValorGarantia,
 pc.SobrecostoGarantia,
 pc.ReservaConstruccion,
 pc.ReservaOperacion,
 pc.ExposicionSobrecosto,
 pc.PorcenajeSobrecosto,
 calSC.Codigo,
 pc.PorcentajeCorrida,
 calCR.Codigo,
 pc.PorcentajeFinal,
 calFN.Codigo,
 pc.SaldoInsoluto
FROM dbo.SICCMX_ProyectoCalificacion pc
INNER JOIN dbo.SICCMX_Proyecto pry ON pc.IdProyecto = pry.IdProyecto
INNER JOIN dbo.SICC_Etapa etp ON pc.IdEtapa = etp.IdEtapa
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calSC ON pc.IdCalificacionSobrecosto = calSC.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCR ON pc.IdCalificacionCorrida = calCR.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calFN ON pc.IdCalificacionFinal = calFN.IdCalificacion;
GO
