SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_GRUPAL_Seg_005_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se debe reportar el Código Postal del domicilio del acreditado, según el correspondiente registro de la Institución.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_GRUPAL rep
LEFT OUTER JOIN dbo.SICC_Municipio cat ON rep.CodigoPostal = cat.CodigoMunicipio
WHERE cat.IdMunicipio IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
