SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_056]
AS
BEGIN
-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas R04-C 483

SELECT
	rep.CodigoCreditoCNBV,
	rep.NumeroDisposicion
FROM dbo.RW_VW_R04C0484_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0483_INC alt ON ISNULL(rep.CodigoCreditoCNBV,'') = ISNULL(alt.CodigoCreditoCNBV,'')
LEFT OUTER JOIN Historico.RW_VW_R04C0483_INC hst ON ISNULL(rep.CodigoCreditoCNBV,'') = hst.CodigoCreditoCNBV
WHERE hst.IdPeriodoHistorico IS NULL AND alt.CodigoCreditoCNBV IS NULL;

END
GO
