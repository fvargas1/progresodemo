SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_049]
AS

BEGIN

-- Validar que el Monto de la Comisión por Apertura del Crédito (dat_comisiones_apertura_cred) no sea mayor al 15% del Monto Autorizado de la Línea de Crédito (dat_monto_credito_linea_aut).

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionAperturaMonto,
	MonLineaCred
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(ComisionAperturaMonto,'0') AS DECIMAL) > CAST(ISNULL(MonLineaCred,'0') AS DECIMAL) * 0.15;

END


GO
