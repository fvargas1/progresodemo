SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_100_Rep_SIC]
AS
DECLARE @IdPeriodo INT;
DECLARE @FechaPeriodo DATETIME;

-- SE INSERTAN CREDITOS NO REPORTADOS A SIC EN [dbo].[SICCMX_Creditos_Rep_SIC]
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo=1;

DELETE FROM dbo.SICCMX_Creditos_Rep_SIC WHERE IdPeriodo = @IdPeriodo;
DELETE FROM dbo.SICCMX_Creditos_Castigados_SIC WHERE IdPeriodo = @IdPeriodo;

INSERT INTO dbo.SICCMX_Creditos_Rep_SIC (IdPersona, CodigoCredito, IdPeriodo, FechaIngreso)
SELECT DISTINCT credito.IdPersona, credito.CodigoCredito, @IdPeriodo, @FechaPeriodo
FROM dbo.SICCMX_VW_Credito_NMC credito
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON credito.IdCredito = cInfo.IdCredito
WHERE ISNULL(cInfo.CredRepSIC, 0) = 0 AND credito.MontoValorizado > 0;


INSERT INTO dbo.SICCMX_Creditos_Castigados_SIC (IdPersona, CodigoCredito, IdPeriodo, FechaIngreso)
SELECT DISTINCT crs.IdPersona, crs.CodigoCredito, @IdPeriodo, @FechaPeriodo
FROM dbo.SICCMX_Creditos_Rep_SIC crs
WHERE crs.IdPeriodo BETWEEN @IdPeriodo-2 AND @IdPeriodo
GROUP BY crs.IdPersona, crs.CodigoCredito
HAVING COUNT(crs.CodigoCredito) >= 3;


-- SE ACTUALIZA PI AL 100% PARA PERSONAS CON CREDITOS CASTIGADOS POR NO REPORTARSE A LA SIC POR 3 MESES CONSECUTIVOS
UPDATE ppi
SET [PI] = 1
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_CreditosRepSIC_12 vwRep ON ppi.IdPersona = vwRep.IdPersona
INNER JOIN dbo.SICC_Periodo periodo ON vwRep.IdPeriodo = periodo.IdPeriodo
WHERE DATEDIFF(MONTH, periodo.Fecha, @FechaPeriodo) <= 12;

-- SE INSERTA EN EL LOG DEL CALCULO DE LA PI
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT ppi.IdPersona, GETDATE(), '', 'Se actualizó la PI al 100% ya que cuenta con Creditos No Reportados a la SIC 3 meses consecutivos en los últimos 12 meses'
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_VW_CreditosRepSIC_12 vwRep ON ppi.IdPersona = vwRep.IdPersona
INNER JOIN dbo.SICC_Periodo periodo ON vwRep.IdPeriodo = periodo.IdPeriodo
WHERE DATEDIFF(MONTH, periodo.Fecha, @FechaPeriodo) <= 12;

UPDATE per
SET PI100 = pi100.IdPI100
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_VW_CreditosRepSIC_12 vwRep ON per.IdPersona = vwRep.IdPersona
INNER JOIN dbo.SICC_Periodo periodo ON vwRep.IdPeriodo = periodo.IdPeriodo
INNER JOIN dbo.SICCMX_Credito cre ON vwRep.CodigoCredito = cre.Codigo
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICC_PI100 pi100 ON pi100.Codigo = '30'
WHERE DATEDIFF(MONTH, periodo.Fecha, @FechaPeriodo) <= 12;
GO
