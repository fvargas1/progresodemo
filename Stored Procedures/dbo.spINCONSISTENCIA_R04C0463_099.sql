SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_099]
AS
BEGIN
-- Validar que el Tipo de Cartera corresponda a Catalogo CNBV

SELECT
 rep.CodigoCredito,
 REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
 rep.TipoCartera
FROM dbo.RW_VW_R04C0463_INC rep
LEFT OUTER JOIN dbo.SICC_TipoPersona tpo ON ISNULL(rep.TipoCartera,'') = tpo.CodigoCNBV AND tpo.A21 = 1
WHERE tpo.IdTipoPersona IS NULL;

END

GO
