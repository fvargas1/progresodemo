SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_CRLiquidadosPorPersona]
	@IdPeriodoHistorico BIGINT,
	@IdReporteLog BIGINT
AS

CREATE TABLE #TEMP_Monto (
	Calificacion VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)

CREATE TABLE #TEMP_Tempo (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Calificacion VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)

CREATE TABLE #TEMP_Diff (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
	Monto DECIMAL(23,2)
)

CREATE TABLE #TEMP_Anterior (
	Credito VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
)


INSERT INTO #TEMP_Anterior
SELECT
	c.Codigo
FROM dbo.SICCMX_Credito_Reservas_Variables ha
INNER JOIN dbo.SICCMX_Credito c ON ha.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON c.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clas ON clas.IdClasificacionContable = c.IdClasificacionContable
WHERE met.Codigo IN ('20','21','22')
AND (clas.Codigo IS NULL OR clas.Codigo NOT IN ('816000', '139151010400', '139151010500')); -- para descartar los fideicomisos


--INSERTO exposicion
INSERT INTO #TEMP_Tempo
SELECT
	cr.Credito,
	cr.CalifFinal,
	ISNULL(cr.EI_Total,0) AS Monto
FROM Historico.SICCMX_Credito_Reservas_Variables cr
INNER JOIN Historico.SICCMX_Credito c ON cr.Credito = c.Codigo AND cr.IdPeriodoHistorico = c.IdPeriodoHistorico
WHERE c.Metodologia in('20','21','22') AND cr.EI_Total > 0
AND cr.IdPeriodoHistorico = @IdPeriodoHistorico
AND (c.ClasificacionContable IS NULL OR c.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500')); -- para descartar los fideicomisos


INSERT INTO #TEMP_Monto
SELECT
	t.Calificacion,
	SUM(ISNULL(t.Monto,0)) AS Monto
FROM #TEMP_Tempo t
LEFT OUTER JOIN #TEMP_Anterior ta ON t.Credito = ta.Credito
WHERE ta.Credito IS NULL
GROUP BY t.Calificacion;


TRUNCATE TABLE #TEMP_Tempo;

-- INSERTO LOS CREDITOS QUE EL MONTO DEL PERIODO ACTUAL ES < AL PERIODO ANTERIOR, NUEVOS PARCIAL
INSERT INTO #TEMP_Diff
SELECT
	hant.Credito,
	SUM(ISNULL(hant.EI_Total,0)) - SUM(ISNULL(hact.EI_Total,0)) AS Monto
FROM Historico.SICCMX_Credito_Reservas_Variables hant
INNER JOIN Historico.SICCMX_Credito hantCredito ON hant.Credito = hantCredito.Codigo AND hant.IdPeriodoHistorico = hantCredito.IdPeriodoHistorico
INNER JOIN dbo.SICCMX_Credito c ON hantCredito.Codigo = c.Codigo
INNER JOIN dbo.SICCMX_Metodologia met ON c.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_ClasificacionContable clas ON c.IdClasificacionContable = clas.IdClasificacionContable
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables hact ON c.IdCredito = hact.IdCredito
WHERE met.Codigo IN ('20','21','22') AND hantCredito.Metodologia in('20','21','22')
AND hant.IdPeriodoHistorico = @IdPeriodoHistorico
AND (clas.Codigo IS NULL OR clas.Codigo NOT IN ('816000', '139151010400', '139151010500'))
AND (hantCredito.ClasificacionContable IS NULL OR hantCredito.ClasificacionContable NOT IN ('816000', '139151010400', '139151010500'))
GROUP BY hant.Credito
HAVING (SUM(ISNULL(hant.EI_Total,0)) - SUM(ISNULL(hact.EI_Total,0))) > 0;


INSERT INTO #TEMP_Monto
SELECT
	(SELECT MAX(vars.CalifFinal)
	FROM Historico.SICCMX_Credito_Reservas_Variables vars
	WHERE vars.Credito = t.Credito AND vars.IdPeriodoHistorico = @IdPeriodoHistorico),
	ISNULL(t.Monto,0)
FROM #TEMP_Diff t;


SELECT
	@IdReporteLog,
	con.Codigo AS Calificacion,
	'0451',
	'1', -- Moneda
	'1', -- Metodo calificacion
	'1', -- Calificaciones de riesgo del deudor
	'91', -- Creditos Liquidados
	CAST(SUM(ISNULL(tm.Monto,0)) AS DECIMAL) AS Monto
FROM dbo.SICC_CalificacionNvaMet cal
INNER JOIN dbo.ReportWare_VW_R04D0451Concepto con ON con.Nombre = cal.Codigo
LEFT OUTER JOIN #TEMP_Monto tm ON cal.Codigo = tm.Calificacion
WHERE cal.Codigo <> 'NA' AND cal.Codigo <> 'SC'
GROUP BY con.Codigo;


DROP TABLE #TEMP_Monto;
DROP TABLE #TEMP_Anterior;
DROP TABLE #TEMP_Tempo;
DROP TABLE #TEMP_Diff;
GO
