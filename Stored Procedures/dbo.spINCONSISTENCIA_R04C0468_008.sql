SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_008]
AS

BEGIN

-- El Número de Consulta Realizada a la SIC debe ser mayor o igual a 8 caracteres.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	IdBuroCredito
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(IdBuroCredito,'') IN ('0','1');

END


GO
