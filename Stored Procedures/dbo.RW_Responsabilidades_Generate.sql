SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Responsabilidades_Generate]
AS
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaMigracion DATETIME;
DECLARE @CodigoInstitucion VARCHAR(6);
DECLARE @LocInstitucion VARCHAR(8);

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @CodigoInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';
SELECT @LocInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='LocInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='BANXICO' AND Nombre = '-Responsabilidades';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Responsabilidades;

INSERT INTO dbo.RW_Responsabilidades (
 IdReporteLog, INSTITUCIO, LOCINSTITU, ANO_INFO, MES_INFO, BLANCO_UNO, SECTOR, MONEDA, RFC, NOMBRE, LOCDEU, ACTDEU, BLANCO_DOS, CAL_RIESGO,
 BLANCO_TRE, TPO_CREDIT, SALDOTPOCR, IMPTE_6101, DDOR_6101, IMPTE_6102, DDOR_6102, CVE_IMPTE1, IMPORTE_01, CVE_IMPTE2, IMPORTE_02,
 CVE_IMPTE3, IMPORTE_03, CVE_IMPTE4, IMPORTE_04
)
SELECT DISTINCT
 @IdReporteLog,
 RIGHT(ISNULL(@CodigoInstitucion,''),3) AS INSTITUCIO,
 ISNULL(@LocInstitucion,'') AS LOCINSTITU,
 ISNULL(CAST(YEAR(@FechaPeriodo) AS CHAR(4)),'') AS ANO_INFO,
 ISNULL(RIGHT('0' + CAST(MONTH(@FechaPeriodo) AS VARCHAR(2)),2),'') AS MES_INFO,
 '' AS BLANCO_UNO,
 ISNULL(sec.CodigoRR,'') AS SECTOR,
 ISNULL(mon.CodigoRR,'') AS MONEDA,
 SUBSTRING(ISNULL(REPLACE(per.RFC,'_','0'),''),1,13) AS RFC,
 ISNULL(pInfo.NombreCNBV,'') AS NOMBRE,
 ISNULL(mun.CodigoRR,'') AS LOCDEU,
 ISNULL(act.CodigoRR,'') AS ACTDEU,
 '' AS BLANCO_DOS,
 CASE
 WHEN ISNULL(cal.Codigo,'') > ISNULL(calHip.Codigo,'') AND ISNULL(cal.Codigo,'') > ISNULL(calCon.Codigo,'') THEN cal.Codigo
 WHEN ISNULL(calHip.Codigo,'') > ISNULL(cal.Codigo,'') AND ISNULL(calHip.Codigo,'') > ISNULL(calCon.Codigo,'') THEN calHip.Codigo
 ELSE calCon.Codigo
 END AS CAL_RIESGO,
 '' AS BLANCO_TRE,
 ISNULL(cre.TipoCreditoRR,'') AS TPO_CREDIT,
 CAST(CASE WHEN cre.Saldo BETWEEN 0.00001 AND 1 THEN 1 ELSE cre.Saldo END AS DECIMAL) AS SALDOTPOCR,
 '' AS IMPTE_6101,
 '' AS DDOR_6101,
 '' AS IMPTE_6102,
 '' AS DDOR_6102,
 'B' AS CVE_IMPTE1,
 CAST(CASE WHEN cre.Saldo BETWEEN 0.00001 AND 1 THEN 1 ELSE cre.Saldo END AS DECIMAL) AS IMPORTE_01,
 '' AS CVE_IMPTE2,
 '' AS IMPORTE_02,
 '' AS CVE_IMPTE3,
 '' AS IMPORTE_03,
 '' AS CVE_IMPTE4,
 '' AS IMPORTE_04
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Saldos_RR cre ON pInfo.IdPersona = cre.IdPersona
LEFT OUTER JOIN dbo.SICCMX_PersonaCalificacion perCal ON pInfo.IdPersona = perCal.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON perCal.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCon ON perCal.IdCalificacion_Consumo = calCon.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 calHip ON perCal.IdCalificacion_Hipotecario = calHip.IdCalificacion
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sec ON pInfo.IdSectorEconomico = sec.IdSectorEconomico
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_Municipio mun ON per.IdLocalidad = mun.IdMunicipio
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON per.IdActividadEconomica = act.IdActividadEconomica;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Responsabilidades WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;

SET @FechaMigracion = ( SELECT MAX( Fecha ) FROM MIGRACION_ProcesoLog );

UPDATE dbo.RW_ReporteLog
SET
 TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
