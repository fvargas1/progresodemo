SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_NoFinancieras_Consumo]
AS
-- CALCULAMOS SP PARA GARANTIAS REALES NO FINANCIERAS
UPDATE can
SET SeveridadCorresp = CASE
	WHEN ISNULL(can.PrctCobSinAju, 0) < (vw.NivelMinimo/100) THEN canExp.SeveridadCorresp
	ELSE vw.SeveridadPerdida END 
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_VW_Garantias_RNF_Consumo vw ON can.IdConsumo = vw.IdConsumo AND vw.IdTipoGarantia=can.IdTipoGarantia
INNER JOIN dbo.SICCMX_Garantia_Canasta_Consumo canExp ON can.IdConsumo = canExp.IdConsumo AND ISNULL(canExp.EsDescubierto, 0) = 1;
GO
