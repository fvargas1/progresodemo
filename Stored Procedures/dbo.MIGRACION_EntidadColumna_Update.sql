SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumna_Update]
	@IdEntidadColumns INT,
	@Nombre VARCHAR(25),
	@Descripcion VARCHAR(100) = NULL,
	@IdEntidad INT,
	@NombreCampo VARCHAR(50),
	@TipoDato VARCHAR(50),
	@IsRequerido BIT = NULL,
	@IsKey BIT = NULL,
	@IsIdentidadBusqueda BIT = NULL,
	@IdCatalogo INT = NULL,
	@NombreIdCatalogo VARCHAR(50) = NULL
AS
UPDATE dbo.MIGRACION_EntidadColumns
SET
	Nombre = @Nombre,
	Descripcion = @Descripcion,
	IdEntidad = @IdEntidad,
	NombreCampo = @NombreCampo,
	TipoDato = @TipoDato,
	IsRequerido = @IsRequerido,
	IsKey = @IsKey,
	IsIdentidadBusqueda = @IsIdentidadBusqueda,
	IdCatalogo = @IdCatalogo,
	NombreIdCatalogo = @NombreIdCatalogo
WHERE IdEntidadColumns= @IdEntidadColumns;
GO
