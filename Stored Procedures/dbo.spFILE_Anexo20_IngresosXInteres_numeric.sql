SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_IngresosXInteres_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_IngresosXInteres_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo20
SET errorFormato = 1
WHERE LEN(IngresosXInteres)>0 AND (ISNUMERIC(ISNULL(IngresosXInteres,'0')) = 0 OR IngresosXInteres LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'IngresosXInteres',
 IngresosXInteres,
 1,
 @Detalle
FROM dbo.FILE_Anexo20
WHERE LEN(IngresosXInteres)>0 AND (ISNUMERIC(ISNULL(IngresosXInteres,'0')) = 0 OR IngresosXInteres LIKE '%[^0-9 .]%');
GO
