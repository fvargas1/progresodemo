SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_087]
AS
BEGIN
-- Si la Categoría del Crédito (cve_categoria_credito) en el formulario de SEGUIMIENTO es IGUAL a 1,
-- entonces el registro del Tipo de Alta del Crédito (cve_tipo_alta_credito) en el formulariode ALTAS para el mismo
-- CRÉDITO (dat_id_credito_met_cnbv) debe estar entre 131 y 150. (Cruce entre el reporte de seguimiento y el reporte de altas).

SELECT DISTINCT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.CategoriaCredito,
	vw.CNBV AS CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0459_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(rep.CategoriaCredito,'') = '1' AND CAST(ISNULL(NULLIF(vw.TipoAlta,''),'-1') AS INT) NOT BETWEEN 131 AND 150;

END


GO
