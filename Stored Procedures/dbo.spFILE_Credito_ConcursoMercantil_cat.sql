SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_ConcursoMercantil_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_ConcursoMercantil_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Credito
SET errorCatalogo = 1
WHERE LEN(ConcursoMercantil)>0 AND LTRIM(ConcursoMercantil) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	CodigoCredito,
	'ConcursoMercantil',
	ConcursoMercantil,
	2,
	@Detalle
FROM dbo.FILE_Credito
WHERE LEN(ConcursoMercantil)>0 AND LTRIM(ConcursoMercantil) NOT IN ('0','1');
GO
