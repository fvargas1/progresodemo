SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0450_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0450_2016);

DELETE FROM Historico.RW_R04C0450_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0450_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, RFC_Garante, NombreGarante, CodigoGarante, PI_Garante, SP_Garante, EI_Garante, ActEconomica,
	Localidad, Municipio, Estado, LEI, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, NombreAcreditado, TipoGarantia, CodigoGarantia,
	MonedaGarantia, MontoGarantia, PrctGarantia
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	RFC_Garante,
	NombreGarante,
	CodigoGarante,
	PI_Garante,
	SP_Garante,
	EI_Garante,
	ActEconomica,
	Localidad,
	Municipio,
	Estado,
	LEI,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	NombreAcreditado,
	TipoGarantia,
	CodigoGarantia,
	MonedaGarantia,
	MontoGarantia,
	PrctGarantia
FROM dbo.RW_R04C0450_2016
WHERE IdReporteLog = @IdReporteLog;
GO
