SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_104]
AS

BEGIN

-- Si la clave de Revocable (cve_revocable=2) en el formulario de ALTAS es igual a 2,
-- entonces en el formulario de SEGUIMIENTO la Exposición al Incumplimiento (dat_exposicion_incumplimiento)
-- debe ser igual a la Responsabilidad Total (dat_responsabilidad_total*(max(100%,(100*dat_responsabilidad_total/dat_monto_credito_linea_aut)^ -0.5794))).
-- El monto de línea de crédito autorizada debe tomarse del formulario de altas.

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	vw.TipoLinea,
	rep.EITotal,
	rep.ResponsabilidadFinal,
	vw.MontoLineaAut,
	CAST(CAST(ISNULL(NULLIF(rep.ResponsabilidadFinal,''),0) AS DECIMAL) * (
	CASE WHEN ISNULL(NULLIF(vw.MontoLineaAut,''),0) = 0 THEN 1
	ELSE dbo.MAX2VAR(1, POWER(dbo.NoN(NULLIF(rep.ResponsabilidadFinal,''), NULLIF(vw.MontoLineaAut,'')), -0.5794)) END) AS DECIMAL) AS [Resp_Total * (max(100%,(100*Resp_Total/MontoLineaAut)^ -0.5794))]
FROM dbo.RW_R04C0479 rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(vw.TipoLinea,'') = '2'
	AND CAST(ISNULL(NULLIF(rep.EITotal,''),0) AS DECIMAL) <> CAST(CAST(ISNULL(NULLIF(rep.ResponsabilidadFinal,''),0) AS DECIMAL) * (
	CASE WHEN ISNULL(NULLIF(vw.MontoLineaAut,''),0) = 0 THEN 1
	ELSE dbo.MAX2VAR(1, POWER(dbo.NoN(NULLIF(rep.ResponsabilidadFinal,''), NULLIF(vw.MontoLineaAut,'')), -0.5794)) END) AS DECIMAL);

END
GO
