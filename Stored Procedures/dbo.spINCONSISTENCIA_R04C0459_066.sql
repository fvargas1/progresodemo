SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_066]
AS
BEGIN
-- Validar que la Responsabilidad Total al Final del Periodo (dat_responsabilidad_total) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInsoluto
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'-1') AS DECIMAL) < 0;

END


GO
