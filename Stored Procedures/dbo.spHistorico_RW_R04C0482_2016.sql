SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0482_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0482_2016);

DELETE FROM Historico.RW_R04C0482_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0482_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, TipoBaja, SaldoPrincipalInicio, SaldoInsoluto, MontoPagado, MontoCastigos,
	MontoCondonacion, MontoQuitas, MontoBonificaciones, MontoDescuentos, MontoBienDacion, ReservasCalifCanceladas, ReservasAdicCanceladas
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	TipoBaja,
	SaldoPrincipalInicio,
	SaldoInsoluto,
	MontoPagado,
	MontoCastigos,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificaciones,
	MontoDescuentos,
	MontoBienDacion,
	ReservasCalifCanceladas,
	ReservasAdicCanceladas
FROM dbo.RW_R04C0482_2016
WHERE IdReporteLog = @IdReporteLog;
GO
