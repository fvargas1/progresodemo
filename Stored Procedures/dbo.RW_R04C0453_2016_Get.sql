SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0453_2016_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActEconomica,
	GrupoRiesgo,
	Localidad,
	Municipio,
	Estado,
	IdBuroCredito,
	LEI,
	TipoAltaCredito,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoGlobalCNBV,
	MonLineaCredValorizado,
	MonLineaCred,
	FecMaxDis,
	FecVenLin,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	Posicion,
	NumEmpLoc,
	NumEmpFedSHCP,
	NumRPPC,
	RegGarantiaMob,
	PorcPartFederal,
	TasaInteres,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	NumMesesAmortCap,
	NumMesesPagoInt,
	ComisionAperturaTasa,
	ComisionAperturaMonto,
	ComisionDisposicionTasa,
	ComisionDisposicionMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_VW_R04C0453_2016;
GO
