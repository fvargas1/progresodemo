SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Accionista]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_Accionista WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Accionista (
	IdPeriodoHistorico,
	Nombre,
	Porcentaje,
	Persona
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	acc.Nombre,
	acc.Porcentaje,
	per.Codigo
FROM dbo.SICCMX_Accionista acc
INNER JOIN dbo.SICCMX_Persona per ON acc.IdPersona = per.IdPersona;
GO
