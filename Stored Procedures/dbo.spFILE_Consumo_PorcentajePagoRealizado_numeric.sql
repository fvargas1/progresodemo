SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_PorcentajePagoRealizado_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_PorcentajePagoRealizado_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Consumo
SET errorFormato = 1
WHERE LEN(PorcentajePagoRealizado)>0 AND (ISNUMERIC(ISNULL(PorcentajePagoRealizado,'0')) = 0 OR PorcentajePagoRealizado LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'PorcentajePagoRealizado',
 PorcentajePagoRealizado,
 1,
 @Detalle
FROM dbo.FILE_Consumo
WHERE LEN(PorcentajePagoRealizado)>0 AND (ISNUMERIC(ISNULL(PorcentajePagoRealizado,'0')) = 0 OR PorcentajePagoRealizado LIKE '%[^0-9 .]%');
GO
