SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0424_Display]
	@IdReporteLog BIGINT
AS

WITH Reporte (Codigo, Concepto, Padre, Total, SaldoMN, SaldoUSD, SaldoUDIS, LEVEL)
AS (
	SELECT Codigo, Concepto, Padre, Total, SaldoMN, SaldoUSD, SaldoUDIS, 0 AS LEVEL
	FROM dbo.RW_R04A0424_VW con
	WHERE Padre = ''
	UNION ALL
	SELECT con.Codigo, con.Concepto, con.Padre, con.Total, con.SaldoMN, con.SaldoUSD, con.SaldoUDIS, LEVEL + 1
	FROM dbo.RW_R04A0424_VW con
	INNER JOIN Reporte rep ON rep.Codigo = con.Padre
)
SELECT
	Codigo,
	REPLICATE(' ',LEVEL*5) +Concepto Concepto,
	Padre,
	Total,
	SaldoMN,
	SaldoUSD,
	SaldoUDIS,
	LEVEL
FROM Reporte
ORDER BY Codigo;
GO
