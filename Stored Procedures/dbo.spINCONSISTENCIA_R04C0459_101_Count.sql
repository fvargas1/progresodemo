SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_101_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que si la Responsabilidad Total (dat_responsabilidad_total) es menor o igual a 0, entonces las Reservas Totales (dat_reservas_totales) son igual a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0459
WHERE CAST(ISNULL(NULLIF(ResponsabilidadFinal,''),'0') AS DECIMAL) <= 0 AND CAST(ISNULL(NULLIF(ReservaTotal,''),'0') AS DECIMAL) > 0

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
