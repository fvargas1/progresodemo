SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_MecanismoPago_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_MecanismoPago_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_FormaPagoConsumo rst ON LTRIM(f.MecanismoPago) = rst.Codigo
WHERE rst.IdFormaPagoConsumo IS NULL AND LEN(f.MecanismoPago) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'MecanismoPago',
 f.MecanismoPago,
 2,
 @Detalle
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_FormaPagoConsumo rst ON LTRIM(f.MecanismoPago) = rst.Codigo
WHERE rst.IdFormaPagoConsumo IS NULL AND LEN(f.MecanismoPago) > 0;
GO
