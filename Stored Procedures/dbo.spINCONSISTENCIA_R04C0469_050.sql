SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_050]
AS

BEGIN

-- Validar que el Saldo del Principal al Inicio del Periodo (dat_saldo_princ_inicio) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInicial
FROM dbo.RW_VW_R04C0469_INC
WHERE CAST(ISNULL(NULLIF(SaldoInicial,''),'-1') AS DECIMAL) < 0;

END


GO
