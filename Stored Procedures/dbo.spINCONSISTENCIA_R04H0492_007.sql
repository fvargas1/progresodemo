SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_007]
AS

BEGIN

-- El monto del saldo del principal al inicio del periodo deberá ser mayor o igual a cero.

SELECT CodigoCredito, CodigoCreditoCNBV, SaldoPrincipalInicio
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'-1') AS DECIMAL) < 0;

END
GO
