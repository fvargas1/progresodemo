SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_038]
AS

BEGIN

-- El monto de comisiones cobradas por apertura de crédito deberá ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, Comisiones
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(Comisiones,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
