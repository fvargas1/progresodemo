SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_DiasAtraso_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_DiasAtraso_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Consumo
SET errorFormato = 1
WHERE (LEN(ISNULL(ATR,''))=0 AND LEN(ISNULL(DiasAtraso,''))=0) OR (LEN(DiasAtraso)>0 AND (ISNUMERIC(ISNULL(DiasAtraso,'0')) = 0 OR DiasAtraso LIKE '%[^0-9 ]%'));

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoCredito,
	'DiasAtraso',
	DiasAtraso,
	1,
	@Detalle
FROM dbo.FILE_Consumo
WHERE (LEN(ISNULL(ATR,''))=0 AND LEN(ISNULL(DiasAtraso,''))=0) OR (LEN(DiasAtraso)>0 AND (ISNUMERIC(ISNULL(DiasAtraso,'0')) = 0 OR DiasAtraso LIKE '%[^0-9 ]%'));
GO
