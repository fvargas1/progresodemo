SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_018_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El tipo de alta del crédito debe existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TipoAltaMA cat ON ISNULL(r.TipoAltaCredito,'') = cat.CodigoCNBV
WHERE cat.IdTipoAlta IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
