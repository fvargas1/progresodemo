SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_051]
AS
BEGIN
-- Validar que el Nombre Grupo de Riesgo (dat_nombre_grupo_riesgo) acepte valores alfanuméricos y los siguientes caracteres especiales: "_", "/" o ".".

SELECT
	CodigoCredito,
	REPLACE(GrupoRiesgo, ',', '') AS GrupoRiesgo
FROM dbo.RW_VW_R04C0458_INC
WHERE GrupoRiesgo LIKE '%[^a-z,.0-9 _/]%' OR GrupoRiesgo LIKE '%[áéíóú]%';

END
GO
