SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_Revolvente_CAT_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'CONSUMO' AND Nombre = '-Revolvente_CAT';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_Consumo_Revolvente_CAT;

-- Informacion para reporte
INSERT INTO dbo.RW_Consumo_Revolvente_CAT (
 IdReporteLog, Fecha, FolioCredito, CatContrato, LimCredito, Producto
)
SELECT DISTINCT
 @IdReporteLog,
 SUBSTRING(REPLACE(CONVERT(VARCHAR, inf.FechaOtorgamiento,102),'.',''),1,8) AS Fecha,
 inf.CreditoCodificado AS FolioCredito,
 CAST(ISNULL(inf.CAT,0) AS DECIMAL(5,1)) AS CatContrato,
 CAST(ISNULL(inf.LimCredito,0) AS DECIMAL) AS LimCredito,
 con.IdProductoNombre AS Producto
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo inf ON con.IdConsumo = inf.IdConsumo
WHERE con.Revolvente = 1 AND ISNULL(inf.indicadorcat,0) = 1;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Consumo_Revolvente_CAT WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
