SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_059]
AS
BEGIN
-- Si el crédito está dentro de balance (tipo de alta del crédito entre 131 y 150), entonces la Tasa de Referencia (cve_tasa) debe ser diferente de 0.

SELECT
	CodigoCredito,
	TipoAltaCredito,
	TasaInteres AS TasaReferencia
FROM dbo.RW_VW_R04C0458_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139') AND ISNULL(TasaInteres,'') = '0'

END
GO
