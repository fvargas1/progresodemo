SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0419_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @SaldoFinalTotalConsumo DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'A-0419_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Calificación de la Cartera de Crédito y Estimación Preventiva para Riesgos Crediticios', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();

TRUNCATE TABLE dbo.RW_R04A0419_2016;

-- Calculamos los totales de los conceptos.
INSERT INTO dbo.RW_R04A0419_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Concepto,
 '419',
 CASE
 WHEN dat.Moneda = '0' THEN '14'
 WHEN dat.Moneda IN ('20','200','202') THEN '9'
 ELSE '4' END, -- Moneda
 '10', -- Tipo de Cartera
 CASE
 WHEN con.CargoAbono IS NULL THEN '11'
 WHEN con.CargoAbono = 'A' THEN '10'
 WHEN con.CargoAbono = 'C' THEN '9'
 END, -- Saldo total
 SUM(con.Monto)
FROM R04.[0419Datos_2016] dat
INNER JOIN R04.[0419Conceptos_2016] con ON con.Codigo = dat.Codigo
GROUP BY con.Concepto, CASE WHEN dat.Moneda = '0' THEN '14' WHEN dat.Moneda IN ('20','200','202') THEN '9' ELSE '4' END, con.CargoAbono;


INSERT INTO dbo.RW_R04A0419_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 Concepto,
 MAX(SubReporte),
 '15',
 MAX(TipoDeCartera),
 MAX(TipoDeSaldo),
 CAST(SUM(CAST(Dato AS DECIMAL(23,2))) AS DECIMAL)
FROM dbo.RW_R04A0419_2016
GROUP BY Concepto;


--CALCULAMOS LAS CUENTAS PADRES
DECLARE @maxLevel INT;
CREATE TABLE #tree (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
 nivel INT
);

WITH Conceptos (Codigo, LEVEL)
AS (
 SELECT Codigo, 0 AS LEVEL
 FROM dbo.ReportWare_VW_0419Concepto_2016
 WHERE Padre = ''
 UNION ALL
 SELECT vw.Codigo, LEVEL + 1
 FROM dbo.ReportWare_VW_0419Concepto_2016 vw
 INNER JOIN Conceptos con ON vw.Padre = con.Codigo
)

INSERT INTO #tree (Codigo,nivel)
SELECT Codigo, [LEVEL] FROM Conceptos;

SELECT @maxLevel = MAX (nivel) FROM #tree;

WHILE @maxLevel >= 0
BEGIN

 INSERT INTO dbo.RW_R04A0419_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
 SELECT rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo,
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
 FROM dbo.ReportWare_VW_0419Concepto_2016 conceptos
 INNER JOIN dbo.RW_R04A0419_2016 rep ON rep.Concepto = conceptos.Codigo
 INNER JOIN #tree con ON rep.Concepto = con.Codigo
 WHERE con.nivel = @maxLevel AND
 conceptos.Padre NOT IN (SELECT ISNULL(Concepto,'') FROM dbo.RW_R04A0419_2016)
 AND conceptos.Padre <> ''
 GROUP BY rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeSaldo, rep.TipoDeCartera;
 
 SET @maxLevel = @maxLevel - 1;
END

DROP TABLE #tree;

UPDATE dbo.RW_R04A0419_2016 SET Dato = '-' + Dato WHERE Concepto IN('139200000000','139000000000','139400000000') AND CAST(Dato AS DECIMAL) <> 0;
UPDATE dbo.RW_R04A0419_2016 SET TipoDeSaldo = '11' WHERE Concepto ='139300000000';
UPDATE dbo.RW_R04A0419_2016 SET TipoDeSaldo = '11' WHERE Concepto ='139400000000';
UPDATE dbo.RW_R04A0419_2016 SET Dato = CAST(Dato AS DECIMAL);


INSERT INTO dbo.RW_R04A0419_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 sal.Concepto,
 sal.SubReporte,
 sal.Moneda,
 sal.TipoCartera,
 sal.TipoSaldo,
 '0'
FROM dbo.BAJAWARE_R04A_Salida_2016 sal
LEFT OUTER JOIN dbo.RW_R04A0419_2016 r04 ON sal.Concepto = r04.Concepto AND sal.Moneda = r04.Moneda AND sal.TipoCartera = r04.TipoDeCartera AND sal.TipoSaldo = r04.TipoDeSaldo
WHERE sal.Subreporte = '419' AND sal.Visible = 1 AND r04.Concepto IS NULL;

DELETE r04
FROM dbo.RW_R04A0419_2016 r04
INNER JOIN dbo.BAJAWARE_R04A_Salida_2016 sal ON r04.Concepto = sal.Concepto AND r04.Moneda = sal.Moneda AND r04.TipoDeCartera = sal.TipoCartera AND r04.TipoDeSaldo = sal.TipoSaldo
WHERE sal.SubReporte = '419' AND sal.Visible = 0;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0419_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = ISNULL(MAX( Fecha ),GETDATE()) FROM dbo.MIGRACION_ProcesoLog WHERE Fecha IS NOT NULL;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
