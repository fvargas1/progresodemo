SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_100]
AS

BEGIN

-- Si la Tasa de Referencia es a tasa cero (560), entonces la Diferencia sobre la Tasa de Referencia (dat_diferenc_tasa_referencia) sea igual a 0.

SELECT
	CodigoCredito,
	TipoAltaCredito,
	TasaInteres AS TasaInteres,
	DifTasaRef
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139') AND ISNULL(TasaInteres,'') = '560' AND ISNULL(DifTasaRef,'') <> '0.000000'

END


GO
