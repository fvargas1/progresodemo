SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo21_ProcOriAdmonCred_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo21_ProcOriAdmonCred_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo21 f
LEFT OUTER JOIN dbo.CAT_VW_Procesos_Orig cat ON LTRIM(f.ProcOriAdmonCred) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo21_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
	f.CodigoCliente,
	'ProcOriAdmonCred',
	f.ProcOriAdmonCred,
	2,
	@Detalle
FROM dbo.FILE_Anexo21 f
LEFT OUTER JOIN dbo.CAT_VW_Procesos_Orig cat ON LTRIM(f.ProcOriAdmonCred) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL;
GO
