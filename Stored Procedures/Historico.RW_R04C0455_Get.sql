SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0455_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITSIC,
	FechaConsultaSIC,
	FechaInfoFinanc,
	MesesPI100,
	GarantiaLeyFederal,
	CumpleCritContGral,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_PorcPagoInstNoBanc,
	P_NumInstCalif,
	P_DeudaTotalPartEleg,
	P_ServDeudaIngAjust,
	P_DeudaCortoPlazoTotal,
	P_IngTotGastoCorriente,
	P_InvIngTotales,
	P_IngPropIngTotales,
	SdoDeudaTotal,
	SdoPartEleg,
	SdoIngTotalesAjust,
	SdoDeudaCortoPlazo,
	SdoIngTotales,
	SdoGastosCorrientes,
	SdoInversion,
	SdoIngPropios,
	P_TasaDesempLocal,
	P_ServFinEntReg,
	P_ObligConting,
	P_BalanceOpPIB,
	P_NivEficRecauda,
	P_SolFlexEjecPres,
	P_SolFlexImpLoc,
	P_TranspFinPublicas,
	P_EmisionDeuda
FROM Historico.RW_R04C0455
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoPersona
GO
