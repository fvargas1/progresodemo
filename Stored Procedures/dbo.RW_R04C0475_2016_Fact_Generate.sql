SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0475_2016_Fact_Generate]
	@IdReporteLog BIGINT,
	@IdPeriodo BIGINT,
	@Entidad VARCHAR(50)
AS
INSERT INTO dbo.RW_R04C0475_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
 HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_AntSocCred, P_QuitCastReest, P_PorcPagoInstNoBanc,
 P_PorcPagoEntComer, P_CredAbInstBanc, P_MonMaxCred, P_MesesUltCredAb, P_PorcPagoInstBanc, P_PorcPagoInstBanc29, P_PorcPagoInstBanc90, P_DiasMoraInstBanc,
 P_PagosInstBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_IndPersFid, P_ProcOrigAdmon, AntSocCred, PorcPagoInstNoBanc, PorcPagoEntComer,
 CredAbInstBanc, MonMaxCred, MesesUltCredAb, PorcPagoInstBanc, PorcPagoInstBanc29, PorcPagoInstBanc90, DiasMoraInstBanc, PagosInstBanc, PagosInfonavit,
 DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, IndPersFid, VentasNetasTotales
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '475',
 'FACTORADO' + per.Codigo AS CodigoPersona,
 CASE WHEN anx.SinAtrasos = 1 THEN '1' ELSE '2' END AS SinAtrasos,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 '750' AS CreditoReportadoSIC,
 '2' AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 '0' AS MesesPI100,
 '0' AS ID_PI100,
 '790' AS GarantiaLeyFederal,
 '810' AS CumpleCritContGral,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_ANT_SOC_CRED] ELSE ptNMC.[21CA_ANT_SOC_CRED] END AS P_AntSocCred,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_QUIT_CAST_REEST] ELSE ptNMC.[21CA_QUIT_CAST_REEST] END AS P_QuitCastReest,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_PORC_PAGO_INST_NOBANC] ELSE ptNMC.[21CA_PORC_PAGO_INST_NOBANC] END AS P_PorcPagoInstNoBanc,
 ptNMC.[21SA_PORC_PAGO_ENT_COMER] AS P_PorcPagoEntComer,
 ptNMC.[21SA_CRED_AB_INST_BANC] AS P_CredAbInstBanc,
 ptNMC.[21SA_MON_MAX_CRED] AS P_MonMaxCred,
 ptNMC.[21SA_MESES_ULT_CRED_AB] AS P_MesesUltCredAb,
 ptNMC.[21SA_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[21CA_PORC_PAGO_INST_BANC29] AS P_PorcPagoInstBanc29,
 ptNMC.[21CA_PORC_PAGO_INST_BANC90] AS P_PorcPagoInstBanc90,
 ptNMC.[21CA_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[21CA_PAGOS_INST_BANC] AS P_PagosInstBanc,
 ptNMC.[21SA_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ISNULL(ptNMC.[21SA_DIAS_ATR_INFONAVIT], ptNMC.[21CA_DIAS_ATR_INFONAVIT]) AS P_DiasAtrInfonavit,
 ISNULL(ptNMC.[21SA_TASA_RET_LAB], ptNMC.[21CA_TASA_RET_LAB]) AS P_TasaRetLab,
 ptNMC.[21CA_IND_PERS_FID] AS P_IndPersFid,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_PROC_ORIG_ADMON] ELSE ptNMC.[21CA_PROC_ORIG_ADMON] END AS P_ProcOrigAdmon,
 CASE WHEN anx.SinAtrasos = 1 THEN vlNMC.[21SA_ANT_SOC_CRED] ELSE vlNMC.[21CA_ANT_SOC_CRED] END AS AntSocCred,
 CASE WHEN anx.SinAtrasos = 1 THEN vlNMC.[21SA_PORC_PAGO_INST_NOBANC] ELSE vlNMC.[21CA_PORC_PAGO_INST_NOBANC] END AS PorcPagoInstNoBanc,
 vlNMC.[21SA_PORC_PAGO_ENT_COMER] AS PorcPagoEntComer,
 vlNMC.[21SA_CRED_AB_INST_BANC] AS CredAbInstBanc,
 vlNMC.[21SA_MON_MAX_CRED] AS MonMaxCred,
 vlNMC.[21SA_MESES_ULT_CRED_AB] AS MesesUltCredAb,
 vlNMC.[21SA_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[21CA_PORC_PAGO_INST_BANC29] AS PorcPagoInstBanc29,
 vlNMC.[21CA_PORC_PAGO_INST_BANC90] AS PorcPagoInstBanc90,
 vlNMC.[21CA_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[21CA_PAGOS_INST_BANC] AS PagosInstBanc,
 vlNMC.[21SA_PAGOS_INFONAVIT] AS PagosInfonavit,
 ISNULL(vlNMC.[21SA_DIAS_ATR_INFONAVIT], vlNMC.[21CA_DIAS_ATR_INFONAVIT]) AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 ISNULL(vlNMC.[21SA_TASA_RET_LAB], vlNMC.[21CA_TASA_RET_LAB]) AS TasaRetLab,
 ISNULL(vlNMC.[21CA_IND_PERS_FID], anx.IndPerMorales) AS IndPersFid,
 anx.VentNetTotAnuales AS VentasNetasTotales
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval aval ON ca.IdAval = aval.IdAval
INNER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura AND fig.ReportePI = 1
INNER JOIN dbo.SICCMX_VW_Anexo21_GP anx ON aval.IdAval = anx.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON anx.EsGarante = esg.IdEsGarante AND esg.Layout = 'AVAL'
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON anx.IdGP = ppi.IdGP AND anx.EsGarante = ppi.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A21_GP ptNMC ON ppi.IdGP = ptNMC.IdGP AND ppi.EsGarante = ptNMC.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasValor_A21_GP vlNMC ON ppi.IdGP = vlNMC.IdGP AND ppi.EsGarante = vlNMC.EsGarante
WHERE ISNULL(anx.OrgDescPartidoPolitico,0) = 1;
GO
