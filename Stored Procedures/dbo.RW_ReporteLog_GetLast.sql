SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ReporteLog_GetLast]
	@IdReporte BIGINT
AS
SELECT TOP 1
	IdReporteLog,
	IdReporte,
	Descripcion,
	FechaCreacion,
	UsuarioCreacion,
	FolderArchivo,
	IdFuenteDatos,
	NombreArchivo,
	FechaImportacionDatos,
	FechaCalculoProcesos,
	TotalRegistros,
	TotalSaldos,
	TotalIntereses
FROM dbo.RW_ReporteLog
WHERE IdReporte = @IdReporte
ORDER BY FechaCreacion DESC;
GO
