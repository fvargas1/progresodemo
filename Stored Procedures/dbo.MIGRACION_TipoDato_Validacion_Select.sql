SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_TipoDato_Validacion_Select]
AS
SELECT
	TipoDato,
	storedProcedure
FROM dbo.MIGRACION_TipoDato_Validacion;
GO
