SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_SetJobInitTime]
	@codename VARCHAR(50)
AS
DECLARE @SENT VARCHAR(150);

DELETE FROM dbo.SysSSISlog WHERE source=@codename AND LEN(message)=0;

INSERT INTO dbo.SysSSISlog (event, computer, operator, source, sourceid, executionid, starttime, endtime, datacode , databytes, message)
VALUES ('PackageStart', 'SERVER', 'BAJAWARE', @codename, NEWID(), NEWID(), GETDATE(), GETDATE(), 0, 0x, '');


UPDATE dbo.MIGRACION_Archivo
SET NumRegistros = 0
WHERE Codename=@codename;


GO
