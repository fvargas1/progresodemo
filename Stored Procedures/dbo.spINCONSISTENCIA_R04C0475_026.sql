SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_026]
AS

BEGIN

-- Si el Puntaje Asignado por Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (cve_ptaje_dias_mora_bcos) es = 55,
-- entonces los Días de mora promedio con Inst Financ Bcarias en los últimos 12 meses (dat_num_dias_mora_prom_bco) deben ser >= 2.54 y < 10.12

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasMoraInstBanc,
	P_DiasMoraInstBanc AS Puntos_DiasMoraInstBanc
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_DiasMoraInstBanc,'') = '55' AND (CAST(DiasMoraInstBanc AS DECIMAL(10,6)) < 2.54 OR CAST(DiasMoraInstBanc AS DECIMAL(10,6)) >= 10.12);

END


GO
