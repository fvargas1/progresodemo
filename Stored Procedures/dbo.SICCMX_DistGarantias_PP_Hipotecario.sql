SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_PP_Hipotecario]
AS

TRUNCATE TABLE dbo.SICCMX_Garantia_Hipotecario_PP;
TRUNCATE TABLE dbo.SICCMX_HipotecarioGarantia_PP;

DECLARE @IdTipoGarantia BIGINT;
SELECT @IdTipoGarantia=IdTipoGarantia FROM dbo.SICC_TipoGarantia WHERE Codigo='NMC-04';

-- SE INSERTAN CREDITOS CON GARANTIAS DE PRIMERAS PERDIDAS
INSERT INTO dbo.SICCMX_HipotecarioGarantia_PP (IdHipotecario, IdGarantia)
SELECT cg.IdHipotecario, cg.IdGarantia
FROM dbo.SICCMX_HipotecarioGarantia cg
INNER JOIN dbo.SICCMX_Garantia_Hipotecario g ON cg.IdGarantia = g.IdGarantia
WHERE g.IdTipoGarantia=@IdTipoGarantia;

-- SE CALCULA EL PORCENTAJE CORRESPONDIENTE A CADA CREDITO
UPDATE cg
SET PrctReserva = CASE WHEN ISNULL(tcg.Reserva,0)=0 THEN 0 ELSE dbo.NoN(rv.Reserva, tcg.Reserva)END
FROM dbo.SICCMX_HipotecarioGarantia_PP cg
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables rv ON cg.IdHipotecario = rv.IdHipotecario
INNER JOIN (
 SELECT cgPP.IdGarantia, SUM(resVar.Reserva) AS Reserva
 FROM dbo.SICCMX_HipotecarioGarantia_PP cgPP
 INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables resVar ON cgPP.IdHipotecario = resVar.IdHipotecario
 GROUP BY cgPP.IdGarantia
) AS tcg ON cg.IdGarantia = tcg.IdGarantia;


-- SE INSERTAN TOTALES DE GARANTIAS DE PRIMERAS PERDIDAS
INSERT INTO dbo.SICCMX_Garantia_Hipotecario_PP (IdGarantia, MontoCubierto, MontoExpuesto, PIGarante, SPGarante)
SELECT
 gar.IdGarantia,
 CASE WHEN gar.ValorGarantia > tcg.Reserva THEN tcg.Reserva ELSE gar.ValorGarantia END,
 CASE WHEN tcg.Reserva - gar.ValorGarantia < 0 THEN 0 ELSE tcg.Reserva - gar.ValorGarantia END,
 gar.PIGarante,
 .45
FROM dbo.SICCMX_VW_Garantia_Hipotecario gar
INNER JOIN (
 SELECT cgPP.IdGarantia, SUM(ISNULL(resVar.Reserva,0)) AS Reserva
 FROM dbo.SICCMX_HipotecarioGarantia_PP cgPP
 INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables resVar ON cgPP.IdHipotecario = resVar.IdHipotecario
 GROUP BY cgPP.IdGarantia
) AS tcg ON gar.IdGarantia = tcg.IdGarantia
WHERE gar.IdTipoGarantia = @IdTipoGarantia;
GO
