SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Info_Ced_Individual]
 @filtro VARCHAR(100)
AS
SELECT DISTINCT
 per.IdPersona AS IdPersona,
 per.Codigo AS Codigo,
 per.Nombre AS Nombre,
 met.IdMetodologia,
 met.Nombre AS Metodologia
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
WHERE per.Codigo + '|' + per.Nombre LIKE '%' + @filtro + '%' AND met.Codigo IN ('18','20','21','22');
GO
