SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_063_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La "ENTIDAD QUE OTORGA EL SEGURO DE CRÉDITO A LA VIVIENDA" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_EntidadAseguradora cat ON ISNULL(r.EntidadSeguro,'') = cat.CodigoCNBV
WHERE cat.IdEntidadAseguradora IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
