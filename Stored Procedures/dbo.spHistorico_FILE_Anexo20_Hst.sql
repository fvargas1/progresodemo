SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Anexo20_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Anexo20_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Anexo20_Hst (
 IdPeriodoHistorico,
 CodigoCliente,
 ActivoTotal,
 NumDiasMoraPromInstBanc,
 PorPagoTiemInstBanc,
 PorPagoTiemInstNoBanc,
 EntFinSujRegBan,
 PasLargoPlazo,
 PasExiInmediata,
 UtilNetaTrimestre,
 CapitalContable,
 CapitalContableProm,
 InstitucionCreditoSiNo,
 CapitalNeto,
 ActivoSujetoRiesgo,
 GastoAdmonPromo,
 IngresosXInteres,
 ComisionesNetas,
 ResXIntermediacion,
 MontoCarteraVencida,
 MontoReserva,
 ReservaResultados,
 GastosXInteres,
 MontoDisponibilidades,
 MontoInverValores,
 MontoCarteraVigente,
 MontoValoresDerivados,
 EmiTitDeuPub,
 MonTotPagInfonavitUltBimestre,
 NumDiasAtrInfonavitUltBimestre,
 Solvencia,
 Liquidez,
 Eficiencia,
 DivLinNeg,
 DivTipFueFin,
 MontoAcreMayores,
 ConsejerosIndependientes,
 TotalConsejeros,
 CompAccionaria,
 CalGobCorp,
 ExpFunAdmon,
 ExpPolProc,
 EdoFinAudit,
 EntFinAcreOtorgantesCre,
 FechaInfoBuro,
 FechaInfoFinanc,
 OrgDescPartidoPolitico,
 CalCredAgenciaCal,
 ExpNegativasPag,
 EsGarante,
 F_NumDiasMoraPromInstBanc,
 F_PorPagoTiemInstBanc,
 F_PorPagoTiemInstNoBanc,
 F_EmiTitDeuPub,
 F_PropPasLargoPlazo,
 F_RendCapitalROE,
 F_IndCapitalizacion,
 F_GastoAdmonPromoIngTot,
 F_CartVencReservCC,
 F_MargenFinancieroAjustado,
 F_MonTotPagInfonavitUltBimestre,
 F_NumDiasAtrInfonavitUltBimestre,
 F_Solvencia,
 F_Liquidez,
 F_Eficiencia,
 F_DivLinNeg,
 F_DivTipFueFin,
 F_ConcentracionActivos,
 F_IndepConsejoAdmon,
 F_CompAccionaria,
 F_CalGobCorp,
 F_ExpFunAdmon,
 F_ExpPolProc,
 F_EdoFinAudit,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoCliente,
 ActivoTotal,
 NumDiasMoraPromInstBanc,
 PorPagoTiemInstBanc,
 PorPagoTiemInstNoBanc,
 EntFinSujRegBan,
 PasLargoPlazo,
 PasExiInmediata,
 UtilNetaTrimestre,
 CapitalContable,
 CapitalContableProm,
 InstitucionCreditoSiNo,
 CapitalNeto,
 ActivoSujetoRiesgo,
 GastoAdmonPromo,
 IngresosXInteres,
 ComisionesNetas,
 ResXIntermediacion,
 MontoCarteraVencida,
 MontoReserva,
 ReservaResultados,
 GastosXInteres,
 MontoDisponibilidades,
 MontoInverValores,
 MontoCarteraVigente,
 MontoValoresDerivados,
 EmiTitDeuPub,
 MonTotPagInfonavitUltBimestre,
 NumDiasAtrInfonavitUltBimestre,
 Solvencia,
 Liquidez,
 Eficiencia,
 DivLinNeg,
 DivTipFueFin,
 MontoAcreMayores,
 ConsejerosIndependientes,
 TotalConsejeros,
 CompAccionaria,
 CalGobCorp,
 ExpFunAdmon,
 ExpPolProc,
 EdoFinAudit,
 EntFinAcreOtorgantesCre,
 FechaInfoBuro,
 FechaInfoFinanc,
 OrgDescPartidoPolitico,
 CalCredAgenciaCal,
 ExpNegativasPag,
 EsGarante,
 F_NumDiasMoraPromInstBanc,
 F_PorPagoTiemInstBanc,
 F_PorPagoTiemInstNoBanc,
 F_EmiTitDeuPub,
 F_PropPasLargoPlazo,
 F_RendCapitalROE,
 F_IndCapitalizacion,
 F_GastoAdmonPromoIngTot,
 F_CartVencReservCC,
 F_MargenFinancieroAjustado,
 F_MonTotPagInfonavitUltBimestre,
 F_NumDiasAtrInfonavitUltBimestre,
 F_Solvencia,
 F_Liquidez,
 F_Eficiencia,
 F_DivLinNeg,
 F_DivTipFueFin,
 F_ConcentracionActivos,
 F_IndepConsejoAdmon,
 F_CompAccionaria,
 F_CalGobCorp,
 F_ExpFunAdmon,
 F_ExpPolProc,
 F_EdoFinAudit,
 Fuente
FROM dbo.FILE_Anexo20_Hst;
GO
