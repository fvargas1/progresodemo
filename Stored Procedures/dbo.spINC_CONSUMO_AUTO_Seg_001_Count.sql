SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_AUTO_Seg_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Tipo de Reestructura de ser 50 en el caso de créditos originales (que no provienen de una reestructura);
-- 200 en el caso de créditos de AUTO que ya fueron objeto de una reestructura y continúan siendo de AUTO.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_Consumo_AUTO rep
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo cat ON rep.Reestructura = cat.CodigoBanxico AND ISNULL(cat.ClasificacionReporte,'AUTO') = 'AUTO'
WHERE cat.IdReestructuraConsumo IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
