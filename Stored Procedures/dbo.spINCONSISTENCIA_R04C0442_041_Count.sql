SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_041_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Fondo de Fomento o banco de desarrollo que otorgó la garantía debe existir en catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Institucion cat ON ISNULL(r.CodigoBancoGarantia,'') = cat.Codigo
WHERE LEN(CodigoBancoGarantia) > 0 AND cat.IdInstitucion IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
