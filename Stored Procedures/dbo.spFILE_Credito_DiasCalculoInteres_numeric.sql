SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Credito_DiasCalculoInteres_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Credito_DiasCalculoInteres_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Credito
SET errorFormato = 1
WHERE LEN(DiasCalculoInteres)>0 AND (ISNUMERIC(ISNULL(DiasCalculoInteres,'0')) = 0 OR DiasCalculoInteres LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Credito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCredito,
 'DiasCalculoInteres',
 DiasCalculoInteres,
 1,
 @Detalle
FROM dbo.FILE_Credito
WHERE LEN(DiasCalculoInteres)>0 AND (ISNUMERIC(ISNULL(DiasCalculoInteres,'0')) = 0 OR DiasCalculoInteres LIKE '%[^0-9 .]%');
GO
