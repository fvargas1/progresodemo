SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ReporteHistorico_ListByGrupo]
	@Grupo VARCHAR(50)
AS
SELECT
	vw.IdReporte,
	vw.GrupoReporte,
	vw.Nombre
FROM dbo.RW_VW_Reporte vw
WHERE Activo=1 AND ( vw.GrupoReporte = @Grupo OR @Grupo IS NULL )
ORDER BY vw.Nombre ASC;
GO
