SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_051_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Nombre Grupo de Riesgo (dat_nombre_grupo_riesgo) acepte valores alfanuméricos y los siguientes caracteres especiales: "_", "/" o ".".

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC
WHERE GrupoRiesgo LIKE '%[^a-z,.0-9 _/]%' OR GrupoRiesgo LIKE '%[áéíóú]%';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
