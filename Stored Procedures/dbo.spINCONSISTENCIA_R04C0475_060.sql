SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_060]
AS

BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_90_dias_atra) es = 4,
-- entonces el Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (dat_porcen_pgo_bcos_90_dias) debe ser >= 80

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstBanc90,
	P_PorcPagoInstBanc90 AS Puntos_PorcPagoInstBanc90
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PorcPagoInstBanc90,'') = '4' AND CAST(PorcPagoInstBanc90 AS DECIMAL(10,6)) < 80;

END


GO
