SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_Liquidez_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_Liquidez_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo20 f
LEFT OUTER JOIN dbo.CAT_VW_Liquidez cat ON LTRIM(f.Liquidez) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL AND LTRIM(f.EntFinAcreOtorgantesCre)='0';

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'Liquidez',
 f.Liquidez,
 2,
 @Detalle
FROM dbo.FILE_Anexo20 f
LEFT OUTER JOIN dbo.CAT_VW_Liquidez cat ON LTRIM(f.Liquidez) = cat.Codigo
WHERE cat.IdItemCatalogo IS NULL AND LTRIM(f.EntFinAcreOtorgantesCre)='0';
GO
