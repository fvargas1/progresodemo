SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_DetalleReservas]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_DetalleReservas);

DELETE FROM Historico.RW_DetalleReservas WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_DetalleReservas (
	IdPeriodoHistorico, Fecha, CodigoCredito, TipoProducto, ReservaInicial, ReservaFinal, Cargo, Abono, GradoRiesgo
)
SELECT
	@IdPeriodoHistorico,
	Fecha,
	CodigoCredito,
	TipoProducto,
	ReservaInicial,
	ReservaFinal,
	Cargo,
	Abono,
	GradoRiesgo
FROM dbo.RW_DetalleReservas
WHERE IdReporteLog = @IdReporteLog;
GO
