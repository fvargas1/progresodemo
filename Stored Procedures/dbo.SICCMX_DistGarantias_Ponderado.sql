SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_Ponderado]
AS
UPDATE cg
SET PorcUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN cre.EI_Total / cgTotal.Total ELSE 0 END,
	MontoUsadoGarantia = CASE WHEN cgTotal.Total > 0 THEN ISNULL(NULLIF(cg.MontoBanco,0), gar.ValorGtiaValorizado) * (cre.EI_Total / cgTotal.Total) ELSE 0 END
FROM dbo.SICCMX_CreditoGarantia AS cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC AS cre ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia gar ON cg.IdGarantia = gar.IdGarantia
INNER JOIN (
 SELECT cg2.IdGarantia, CAST(SUM(ISNULL(vwc.EI_Total, 0)) AS DECIMAL(23,2)) AS Total
 FROM dbo.SICCMX_CreditoGarantia cg2
 INNER JOIN dbo.SICCMX_VW_Credito_NMC vwc ON cg2.IdCredito = vwc.IdCredito
 GROUP BY cg2.IdGarantia
) AS cgTotal ON gar.IdGarantia = cgTotal.IdGarantia
WHERE gar.AplicaDist = 1 AND cg.Aplica = 1;


UPDATE cg
SET MontoUsadoGarantia = cre.EI_Total * dbo.MIN2VAR(1, cg.PorcCubiertoCredito)
FROM dbo.SICCMX_CreditoGarantia AS cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC AS cre ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia gar ON cg.IdGarantia = gar.IdGarantia
WHERE gar.AplicaDist = 0 AND cg.Aplica = 1;

UPDATE cg
SET PorcCubiertoCredito = dbo.MIN2VAR(1, cg.MontoUsadoGarantia / cre.MontoValorizado),
	MontoCubiertoCredito = cre.MontoValorizado * dbo.MIN2VAR(1, cg.MontoUsadoGarantia / cre.MontoValorizado)
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON cg.IdCredito = cre.IdCredito
WHERE cre.MontoValorizado > 0 AND cg.Aplica = 1;
GO
