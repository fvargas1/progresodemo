SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_050]
AS

BEGIN

-- Validar que el Monto Fondeado por Banco de Desarrollo o Fondo de Fomento (dat_monto_fondea_b_desarrollo) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoBancaDesarrollo
FROM dbo.RW_VW_R04C0474_INC
WHERE CAST(ISNULL(NULLIF(MontoBancaDesarrollo,''),'-1') AS DECIMAL) < 0;

END


GO
