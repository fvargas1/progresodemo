SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_PerdidaEsperada_Hipotecario]
AS
UPDATE dbo.SICCMX_Hipotecario_Reservas_Variables SET PerdidaEsperada = [PI] * SP;
GO
