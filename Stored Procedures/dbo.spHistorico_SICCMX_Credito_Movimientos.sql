SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Credito_Movimientos]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Credito_Movimientos WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Credito_Movimientos (
	IdPeriodoHistorico,
	Credito,
	TipoMovimiento,
	Monto,
	Fecha,
	CalificacionAfecto,
	Concepto0419,
	Concepto0420,
	Concepto0424
)
SELECT
	@IdPeriodoHistorico,
	cre.Codigo,
	tm.Codigo,
	cm.Monto,
	cm.Fecha,
	cm.CalificacionAfecto,
	cm.Concepto0419,
	cm.Concepto0420,
	cm.Concepto0424
FROM dbo.SICCMX_Credito_Movimientos cm
INNER JOIN dbo.SICCMX_Credito cre ON cm.IdCredito = cre.IdCredito
INNER JOIN dbo.SICC_TipoMovimiento tm ON cm.IdTipoMovimiento = tm.IdTipoMovimiento;
GO
