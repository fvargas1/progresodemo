SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_018]
AS

BEGIN

-- El saldo del principal al final del periodo debe ser mayor o igual a 0.

SELECT CodigoCredito, CodigoCreditoCNBV, SaldoPrincipalFinal
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(SaldoPrincipalFinal,''),'-1') AS DECIMAL) < 0;

END
GO
