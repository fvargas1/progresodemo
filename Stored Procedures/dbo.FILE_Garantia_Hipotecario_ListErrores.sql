SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Garantia_Hipotecario_ListErrores]
AS
SELECT
 CodigoGarantia,
 TipoGarantia,
 ValorGarantia,
 Moneda,
 Descripcion,
 PIGarante,
 Fuente
FROM dbo.FILE_Garantia_Hipotecario
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
