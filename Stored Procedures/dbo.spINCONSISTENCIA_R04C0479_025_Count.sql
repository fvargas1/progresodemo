SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_025_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Saldo del Principal al Final del Período presenta no cuadra con la suma de saldos y montos.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(SaldoFinal,0) AS MONEY) < (
	CAST(ISNULL(SaldoInicial,0) AS MONEY) +
	CAST(ISNULL(MontoDispuesto,0) AS MONEY) -
	CAST(ISNULL(MontoCapitalPagado,0) AS MONEY))

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
