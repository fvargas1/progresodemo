SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_035]
AS

BEGIN

-- Si el Tipo de Cartera (cve_tipo_cartera) es IGUAL a 410 ó 420, la Clave de Estado (cve_estado) debe ser mayor a 32.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	Estado AS Estado
FROM dbo.RW_VW_R04C0478_INC
WHERE ISNULL(TipoCartera,'') IN ('410','420') AND CAST(ISNULL(Estado,'0') AS INT) <= 32;

END


GO
