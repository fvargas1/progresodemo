SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_063_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El PORCENTAJE DE PAGOS EN TIEMPO CON INSTITUCIONES BANCARIAS EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE LEN(PorcPagoInstBanc) > 0 AND PorcPagoInstBanc <> '-999'
	AND (CHARINDEX('.',PorcPagoInstBanc) = 0 OR CHARINDEX('.',PorcPagoInstBanc) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstBanc, CHARINDEX('.', PorcPagoInstBanc) + 1, LEN(PorcPagoInstBanc)))) <> 6);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
