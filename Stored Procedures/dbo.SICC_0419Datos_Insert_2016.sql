SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0419Datos_Insert_2016]
AS
DECLARE @IdPeriodoHistorico BIGINT;
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdPeriodoHistorico = IdPeriodoHistorico
FROM dbo.SICC_PeriodoHistorico
WHERE YEAR(Fecha) = YEAR(DATEADD(MONTH, -1, @FechaPeriodo)) AND MONTH(Fecha) = MONTH(DATEADD(MONTH, -1, @FechaPeriodo)) AND Activo = 1;


TRUNCATE TABLE R04.[0419Datos_2016];

-- Insertamos la Informacion Historica Comercial
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoHistorico,
 ReservaHistorico,
 IntCarVencHistorico,
 ReservasAdicHistorico,
 PrctReservasHistorico
)
SELECT
 crv.Credito,
 info.TipoProductoSerie4_2016,
 cre.Moneda,
 CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
 ISNULL(crv.ReservaFinal,0),
 ISNULL(val.InteresCarteraVencida,0),
 ISNULL(crv.ReservaAdicional,0),
 ISNULL(crv.PorReservaFinal,0)
FROM Historico.SICCMX_Credito_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Credito cre ON crv.IdPeriodoHistorico = cre.IdPeriodoHistorico AND crv.Credito = cre.Codigo
INNER JOIN Historico.SICCMX_CreditoInfo info ON cre.IdPeriodoHistorico = info.IdPeriodoHistorico AND cre.Codigo = info.Credito
INNER JOIN Historico.SICCMX_Credito_Valorizado val ON crv.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Credito = val.Credito
WHERE crv.IdPeriodoHistorico = @IdPeriodoHistorico AND info.Posicion <> '181' AND info.TipoLinea <> '181';


-- Historial Consumo
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoHistorico,
 ReservaHistorico,
 IntCarVencHistorico,
 ReservasAdicHistorico,
 PrctReservasHistorico
)
SELECT
 crv.Consumo,
 info.TipoProductoSerie4_2016,
 info.Moneda,
 CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
 ISNULL(crv.ReservaTotal,0),
 ISNULL(val.InteresCarteraVencida,0),
 ISNULL(crv.ReservaAdicional,0),
 ISNULL(crv.PorReserva,0)
FROM Historico.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN Historico.SICCMX_Consumo con ON crv.IdPeriodoHistorico = con.IdPeriodoHistorico AND crv.Consumo = con.Codigo
INNER JOIN Historico.SICCMX_ConsumoInfo info ON con.IdPeriodoHistorico = info.IdPeriodoHistorico AND con.Codigo = info.Consumo
INNER JOIN Historico.SICCMX_Consumo_Valorizado val ON info.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Consumo = val.Consumo
WHERE crv.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Historial Hipotecario
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoHistorico,
 ReservaHistorico,
 IntCarVencHistorico,
 ReservasAdicHistorico,
 PrctReservasHistorico
)
SELECT
 res.Hipotecario,
 hip.TipoCreditoR04A_2016,
 info.Moneda,
 CAST(val.SaldoCapitalVigente AS DECIMAL)+CAST(val.InteresVigente AS DECIMAL)+CAST(val.SaldoCapitalVencido AS DECIMAL)+CAST(val.InteresVencido AS DECIMAL),
 ISNULL(hrv.Reserva,0),
 ISNULL(val.InteresCarteraVencida,0),
 ISNULL(res.ReservaAdicional,0),
 ISNULL(hrv.PorReserva,0)
FROM Historico.SICCMX_HipotecarioReservas res
INNER JOIN Historico.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdPeriodoHistorico = hrv.IdPeriodoHistorico AND res.Hipotecario = hrv.Hipotecario
INNER JOIN Historico.SICCMX_Hipotecario hip ON res.IdPeriodoHistorico = hip.IdPeriodoHistorico AND res.Hipotecario = hip.Hipotecario
INNER JOIN Historico.SICCMX_HipotecarioInfo info ON hip.IdPeriodoHistorico = info.IdPeriodoHistorico AND hip.Hipotecario = info.Hipotecario
INNER JOIN Historico.SICCMX_Hipotecario_Valorizado val ON info.IdPeriodoHistorico = val.IdPeriodoHistorico AND info.Hipotecario = val.Hipotecario
WHERE res.IdPeriodoHistorico = @IdPeriodoHistorico;


-- Actualizar Reservas Creditos Actual(comercial)
UPDATE dat
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NULL THEN dat.CodigoProducto ELSE tp.Codigo END,
 SaldoActual = cre.MontoValorizadoRound,
 ReservaActual = ISNULL(crv.ReservaFinal,0),
 IntCarVencActual = ISNULL(cre.InteresCarteraVencidaValorizado,0),
 ReservasAdicActual = ISNULL(crv.ReservaAdicional,0),
 PrctReservasActual = ISNULL(crv.PorReservaFinal,0),
 SaldoActualOrigen = cre.SaldoCapitalVigente + cre.InteresVigente + cre.SaldoCapitalVencido + cre.InteresVencido,
 TipoBaja = CASE WHEN info.IdTipoBaja IS NULL THEN NULL ELSE CASE WHEN tb.Codigo = '136' THEN 'CESIONES' ELSE 'PAGO' END END
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON dat.Codigo = cre.CodigoCredito
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON info.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON info.IdTipoProductoSerie4_2016 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_TipoBaja tb ON info.IdTipoBaja = tb.IdTipoBaja;


-- Insert de nuevos creditos
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoActual,
 ReservaActual,
 IntCarVencActual,
 ReservasAdicActual,
 PrctReservasActual,
 SaldoActualOrigen,
 TipoAlta
)
SELECT
 cre.CodigoCredito,
 tp.Codigo,
 mon.Codigo,
 cre.MontoValorizadoRound,
 ISNULL(crv.ReservaFinal,0),
 ISNULL(cre.InteresCarteraVencidaValorizado,0),
 ISNULL(crv.ReservaAdicional,0),
 ISNULL(crv.PorReservaFinal,0),
 cre.SaldoCapitalVigente + cre.InteresVigente + cre.SaldoCapitalVencido + cre.InteresVencido,
 CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '135' THEN 'COMPRA' ELSE 'ALTA' END END
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_CreditoInfo info ON cre.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON info.IdCredito = crv.IdCredito 
INNER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN dbo.SICC_TipoAlta ta ON info.IdTipoAlta = ta.IdTipoAlta
LEFT OUTER JOIN R04.[0419Datos_2016] dat ON cre.CodigoCredito = dat.Codigo
WHERE dat.Id IS NULL AND cre.Posicion <> '181' AND cre.TipoLinea <> '181';


-- Actualizar Consumo que ya tengan historico
UPDATE dat
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NULL THEN dat.CodigoProducto ELSE tp.Codigo END,
 SaldoActual = CAST(ISNULL(con.SaldoTotalValorizadoRound,0) AS DECIMAL),
 ReservaActual = ISNULL(crv.ReservaTotal,0),
 IntCarVencActual = ISNULL(con.InteresCarteraVencidaValorizado,0),
 ReservasAdicActual = ISNULL(crv.ReservaAdicional,0),
 PrctReservasActual = ISNULL(crv.PorReserva,0),
 SaldoActualOrigen = con.SaldoCapitalVigente + con.InteresVigente + con.SaldoCapitalVencido + con.InteresVencido,
 TipoBaja = CASE WHEN info.IdTipoBaja IS NULL THEN NULL ELSE CASE WHEN tb.Codigo = '50' THEN 'CESIONES' ELSE 'PAGO' END END
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_Consumo con ON dat.Codigo = con.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON info.IdTipoProductoSerie4_2016 = tp.IdTipoProducto
LEFT OUTER JOIN dbo.SICC_TipoBajaConsumo tb ON info.IdTipoBaja = tb.IdTipoBajaConsumo;


-- Insert nuevos Registros Reserva Consumo
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoActual,
 ReservaActual,
 IntCarVencActual,
 ReservasAdicActual,
 PrctReservasActual,
 SaldoActualOrigen,
 TipoAlta
)
SELECT
 con.Codigo,
 tp.Codigo AS CodigoProducto,
 mon.Codigo,
 CAST(ISNULL(con.SaldoTotalValorizadoRound,0) AS DECIMAL),
 ISNULL(crv.ReservaTotal,0),
 ISNULL(con.InteresCarteraVencidaValorizado,0),
 ISNULL(crv.ReservaAdicional,0),
 ISNULL(crv.PorReserva,0),
 con.SaldoCapitalVigente + con.InteresVigente + con.SaldoCapitalVencido + con.InteresVencido,
 CASE WHEN YEAR(@FechaPeriodo) = YEAR(info.FechaOtorgamiento) AND MONTH(@FechaPeriodo) = MONTH(info.FechaOtorgamiento) THEN '1' ELSE NULL END
FROM dbo.SICCMX_VW_Consumo con
INNER JOIN dbo.SICCMX_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON info.IdConsumo = crv.IdConsumo
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4_2016
LEFT OUTER JOIN R04.[0419Datos_2016] dat ON con.Codigo = dat.Codigo
WHERE dat.Id IS NULL;


-- Actualizar registros Hipotecario
UPDATE dat
SET
 CodigoProducto = CASE WHEN tp.Codigo IS NULL THEN dat.CodigoProducto ELSE tp.Codigo END,
 SaldoActual = CAST(ISNULL(hip.SaldoTotalValorizadoRound,0) AS DECIMAL),
 ReservaActual = ISNULL(hrv.Reserva,0),
 IntCarVencActual = ISNULL(hip.InteresCarteraVencidaValorizado,0),
 ReservasAdicActual = ISNULL(res.ReservaAdicional,0),
 PrctReservasActual = ISNULL(hrv.PorReserva,0),
 SaldoActualOrigen = hip.SaldoCapitalVigente + hip.InteresVigente + hip.SaldoCapitalVencido + hip.InteresVencido,
 TipoBaja = CASE WHEN info.IdTipoBaja IS NULL THEN NULL ELSE CASE WHEN tb.Codigo = '5' THEN 'CESIONES' ELSE 'PAGO' END END
FROM R04.[0419Datos_2016] dat
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON dat.Codigo = hip.Codigo
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON info.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A_2016
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario tb ON info.IdTipoBaja = tb.IdTipoBajaHipotecario;


-- Insert Registros Reserva Hipotecario
INSERT INTO R04.[0419Datos_2016] (
 Codigo,
 CodigoProducto,
 Moneda,
 SaldoActual,
 ReservaActual,
 IntCarVencActual,
 ReservasAdicActual,
 PrctReservasActual,
 SaldoActualOrigen,
 TipoAlta
)
SELECT
 hip.Codigo,
 tp.Codigo AS CodigoProducto,
 mon.Codigo,
 CAST(ISNULL(hip.SaldoTotalValorizadoRound,0) AS DECIMAL),
 ISNULL(hrv.Reserva,0),
 ISNULL(hip.InteresCarteraVencidaValorizado,0),
 ISNULL(res.ReservaAdicional,0),
 ISNULL(hrv.PorReserva,0),
 hip.SaldoCapitalVigente + hip.InteresVigente + hip.SaldoCapitalVencido + hip.InteresVencido,
 CASE WHEN ta.Codigo IS NOT NULL THEN CASE WHEN ta.Codigo = '5' THEN 'COMPRA' ELSE 'ALTA' END END
FROM dbo.SICCMX_VW_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON info.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN sicc_moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tp ON tp.IdTipoProducto = hip.IdTipoCreditoR04A_2016
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario ta ON info.IdTipoAlta = ta.IdTipoAltaHipotecario
LEFT OUTER JOIN R04.[0419Datos_2016] dat ON hip.Codigo = dat.Codigo
WHERE dat.Id IS NULL;
GO
