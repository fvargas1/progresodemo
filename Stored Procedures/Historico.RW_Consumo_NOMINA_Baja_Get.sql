SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Consumo_NOMINA_Baja_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	FolioCredito,
	TipoCredito,
	FechaBajaCredito,
	TipoBajaCredito,
	QuitasCondonacionesBonificacionesDescuentos
FROM Historico.RW_Consumo_NOMINA_Baja
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY FolioCredito;
GO
