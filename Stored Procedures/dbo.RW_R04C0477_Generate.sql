SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0477_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0477';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0477;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0477 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, NombrePersona, TipoBaja,
 SaldoInicial, ResponsabilidadInicial, MontoPagado, MontoQuitCastReest, MontoBonificaciones
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0477',
 cnbv.CNBV AS CodigoCreditoCNBV,
 lin.Codigo AS CodigoCredito,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 tpoBaja.CodigoCNBV AS TipoBaja,
 lin.SaldoInicial AS SaldoInicial,
 lin.ResTotalInicioPer AS ResponsabilidadInicial,
 ISNULL(lin.MontoPagEfeCap,0)+ISNULL(lin.MontoPagEfeInt,0)+ISNULL(lin.MontoPagEfeCom,0)+ISNULL(lin.MontoPagEfeIntMor,0) AS MontoPagado,
 lin.MontoQuitasCastQue AS MontoQuitCastReest,
 lin.MontoBonificacionDesc AS MontoBonificaciones
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
LEFT OUTER JOIN dbo.SICCMX_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICMCX_VW_Clientes_A19_Rep anx19 ON per.IdPersona = anx19.IdPersona AND anx19.Metodologia = '21OD'
LEFT OUTER JOIN dbo.SICC_TipoBaja tpoBaja ON lin.IdTipoBaja = tpoBaja.IdTipoBaja
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
WHERE ((met.Codigo='21' AND ISNULL(anx.OrgDescPartidoPolitico,0) = 1) OR (met.Codigo='4' AND anx19.IdPersona IS NOT NULL)) AND lin.IdTipoBaja IS NOT NULL

UNION
-- BAJAS POR CAMBIO DE METODOLOGIA
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0477',
 cnbv.CNBV AS CodigoCreditoCNBV,
 lin.Codigo AS CodigoCredito,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 '140' AS TipoBaja,
 lin.SaldoInicial AS SaldoInicial,
 lin.ResTotalInicioPer AS ResponsabilidadInicial,
 ISNULL(lin.MontoPagEfeCap,0)+ISNULL(lin.MontoPagEfeInt,0)+ISNULL(lin.MontoPagEfeCom,0)+ISNULL(lin.MontoPagEfeIntMor,0) AS MontoPagado,
 lin.MontoQuitasCastQue AS MontoQuitCastReest,
 lin.MontoBonificacionDesc AS MontoBonificaciones
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Persona_CambioMetodologia cm ON lin.Codigo = cm.NumeroDeLineaActual AND cm.MetodologiaAnterior='21'
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
WHERE ISNULL(cm.OrgDescAnterior,0) = 1;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0477 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
