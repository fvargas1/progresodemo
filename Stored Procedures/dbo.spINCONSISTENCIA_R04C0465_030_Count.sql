SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_030_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por el Monto Máximo de Crédito Ototgado por Inst Bcarias en los últimos 12 meses (cve_ptaje_mont_max_cred_otorg) es = 53,
-- entonces el Monto Máximo de Crédito Otorgado por Inst Bcarias en los últimos 12 meses (dat_monto_max_cred_bco_udis) debe ser = -999 (Sin Informacion)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_MonMaxCred,'') = '53' AND CAST(MonMaxCred AS DECIMAL) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
