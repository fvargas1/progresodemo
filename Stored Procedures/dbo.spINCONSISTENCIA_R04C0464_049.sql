SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_049]
AS

BEGIN

-- Validar que el Monto Dispuesto de la Línea de Crédito en el Mes (dat_monto_credito_dispuesto) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoDispuesto
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL) < 0;

END

GO
