SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Anexo18_GP_Preliminares]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Anexo18_GP_Preliminares WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Anexo18_GP_Preliminares (
 IdPeriodoHistorico,
 CodigoGP,
 EsGarante,
 DeudaTotalPartEleg,
 ServicioDeuda,
 ServicioDeudaIngresosTotales,
 DeudaCortoPlazoDeudaTotal,
 IngresosTotalesGastoCorr,
 InvIngresosTotales,
 IngPropiosIngTotales,
 ObligContDerivadasBenef,
 BalanceOperativoPIB,
 NivelEficienciaRec
)
SELECT
 @IdPeriodoHistorico,
 p.Codigo, -- Persona
 g.Codigo,
 a.DeudaTotalPartEleg,
 a.ServicioDeuda,
 a.ServicioDeudaIngresosTotales,
 a.DeudaCortoPlazoDeudaTotal,
 a.IngresosTotalesGastoCorr,
 a.InvIngresosTotales,
 a.IngPropiosIngTotales,
 a.ObligContDerivadasBenef,
 a.BalanceOperativoPIB,
 a.NivelEficienciaRec
FROM dbo.SICCMX_Anexo18_GP_Preliminares a
INNER JOIN dbo.SICCMX_Aval p ON p.IdAval = a.IdGP
INNER JOIN dbo.SICC_EsGarante g ON a.EsGarante = g.IdEsGarante
WHERE g.Layout = 'AVAL'
UNION
SELECT
 @IdPeriodoHistorico,
 p.Codigo, -- Persona
 g.Codigo,
 a.DeudaTotalPartEleg,
 a.ServicioDeuda,
 a.ServicioDeudaIngresosTotales,
 a.DeudaCortoPlazoDeudaTotal,
 a.IngresosTotalesGastoCorr,
 a.InvIngresosTotales,
 a.IngPropiosIngTotales,
 a.ObligContDerivadasBenef,
 a.BalanceOperativoPIB,
 a.NivelEficienciaRec
FROM dbo.SICCMX_Anexo18_GP_Preliminares a
INNER JOIN dbo.SICCMX_Garantia p ON p.IdGarantia = a.IdGP
INNER JOIN dbo.SICC_EsGarante g ON a.EsGarante = g.IdEsGarante
WHERE g.Layout = 'GARANTIA';
GO
