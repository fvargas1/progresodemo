SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_ValidaFechas_GP]
AS
DECLARE @Tabla VARCHAR(50);
DECLARE @IdVar INT;
DECLARE @Campo VARCHAR(50);
DECLARE @Antiguedad INT;
DECLARE @SENT VARCHAR(2000);
DECLARE @IdMetodologia20 INT;
DECLARE @IdMetodologia21 INT;
DECLARE @IdMetodologia22 INT;
DECLARE @ValorDefault VARCHAR(5);
DECLARE @AntiguedadNoReg INT;
DECLARE @CONDICION VARCHAR(300);
DECLARE @CONDICIONLOG VARCHAR(300);
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo=1;
SELECT @IdMetodologia20 = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=20;
SELECT @IdMetodologia21 = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=21;
SELECT @IdMetodologia22 = IdMetodologia FROM dbo.SICCMX_Metodologia WHERE Codigo=22;


INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT IdGP, EsGarante, GETDATE(), '', '********** SE VALIDA LA ANTIGÜEDAD DE LA INFORMACIÓN **********'
FROM dbo.SICCMX_Persona_PI_GP
WHERE IdMetodologia IN (@IdMetodologia20,@IdMetodologia21,@IdMetodologia22);
WAITFOR DELAY '000:00:00:05';


-- PARA ENTIDADES FINANCIERAS ACREDITADAS, QUE SEAN A SU VEZ OTORGANTES DE CRÉDITO
DECLARE curAnexo CURSOR FOR
SELECT 'SICCMX_Anexo20_GP', varVal.IdVariable, varVal.CampoValidar, varVal.AntiguedadValidar, varVal.ValorDefault, varVal.AntiguedadNoReg
FROM dbo.SICCMX_PI_Variables piVar
INNER JOIN dbo.SICCMX_PI_Variables_Validacion varVal ON piVar.Id = varVal.IdVariable
WHERE piVar.IdMetodologia=@IdMetodologia20 AND varVal.Activo=1
UNION ALL
SELECT 'SICCMX_Anexo21_GP', varVal.IdVariable, varVal.CampoValidar, varVal.AntiguedadValidar, varVal.ValorDefault, varVal.AntiguedadNoReg
FROM dbo.SICCMX_PI_Variables piVar
INNER JOIN dbo.SICCMX_PI_Variables_Validacion varVal ON piVar.Id = varVal.IdVariable
WHERE piVar.IdMetodologia=@IdMetodologia21 AND varVal.Activo=1
UNION ALL
SELECT 'SICCMX_Anexo22_GP', varVal.IdVariable, varVal.CampoValidar, varVal.AntiguedadValidar , varVal.ValorDefault, varVal.AntiguedadNoReg
FROM dbo.SICCMX_PI_Variables piVar
INNER JOIN dbo.SICCMX_PI_Variables_Validacion varVal ON piVar.Id = varVal.IdVariable
WHERE piVar.IdMetodologia=@IdMetodologia22 AND varVal.Activo=1;

OPEN curAnexo;

FETCH NEXT FROM curAnexo INTO @Tabla, @IdVar, @Campo, @Antiguedad , @ValorDefault, @AntiguedadNoReg;

WHILE @@FETCH_STATUS = 0
BEGIN
IF @Tabla = 'SICCMX_Anexo20_GP'
BEGIN
 SET @CONDICION = 'CASE WHEN anx.EntFinSujRegBan IN (''1'',''2'') THEN ' + CONVERT(VARCHAR(3), @Antiguedad) + ' ELSE ' + CONVERT(VARCHAR(3), @AntiguedadNoReg) + ' END';
 SET @CONDICIONLOG = 'CASE WHEN anx.EntFinSujRegBan IN (''1'',''2'') THEN ''' + CONVERT(VARCHAR(3), @Antiguedad) + ''' ELSE ''' + CONVERT(VARCHAR(3), @AntiguedadNoReg) + ''' END';
END
ELSE
BEGIN
 SET @CONDICION = CONVERT(VARCHAR(3), @Antiguedad);
 SET @CONDICIONLOG = '''' + CONVERT(VARCHAR(3), @Antiguedad) + '''';
END

SET @SENT =
'UPDATE piDet ' +
'SET ValorActual = ' + ISNULL(@ValorDefault, 'NULL') + ' ' +
'FROM dbo.SICCMX_Persona_PI_Detalles_GP piDet ' +
'INNER JOIN dbo.' + @Tabla + ' anx ON piDet.IdGP = anx.IdGP AND piDet.EsGarante = anx.EsGarante ' +
'WHERE piDet.IdVariable=' + CONVERT(VARCHAR(3), @IdVar) + '	AND DATEDIFF(MONTH, anx.' + @Campo + ', ''' + CONVERT(CHAR(10), @FechaPeriodo, 126) + ''') > ' + @CONDICION;

EXEC(@SENT);

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Log_GP (IdGP, EsGarante, FechaCalculo, Usuario, Descripcion) ' +
'SELECT anx.IdGP, anx.EsGarante, GETDATE(), '''', ''Se actualizó el valor "'' + variables.Nombre + ''" ya que la antigüedad de la información es mayor a la permitida. (' + @Campo + ' > '' + ' + @CONDICIONLOG + ' + '' mes(es))'' ' +
'FROM dbo.' + @Tabla + ' anx ' +
'INNER JOIN dbo.SICCMX_Persona_PI_Detalles_GP piDet ON anx.IdGP = piDet.IdGP AND piDet.EsGarante = anx.EsGarante ' +
'INNER JOIN dbo.SICCMX_PI_Variables variables ON piDet.IdVariable = variables.Id ' +
'WHERE piDet.IdVariable=' + CONVERT(VARCHAR(3), @IdVar) + ' AND DATEDIFF(MONTH, anx.' + @Campo + ', ''' + CONVERT(CHAR(10), @FechaPeriodo, 126) + ''') > ' + @CONDICION;

EXEC(@SENT);

FETCH NEXT FROM curAnexo INTO @Tabla, @IdVar, @Campo, @Antiguedad , @ValorDefault, @AntiguedadNoReg;

END

CLOSE curAnexo;
DEALLOCATE curAnexo;
GO
