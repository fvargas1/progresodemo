SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioReservasVariablesPreliminares_Select]
 @IdHipotecario BIGINT
AS
SELECT
 vw.IdHipotecario,
 vw.IdMetodologia,
 vw.IdMetodologiaNombre,
 vw.IdMetodologiaCodigo,
 vw.IdMetodologiaDesc,
 vw.ATR,
 vw.MAXATR,
 vw.PorPago AS PorVPago,
 vw.PorCLTVi AS PorCLTV,
 vw.INTEXP,
 vw.MON,
 vw.ValorVivienda,
 vw.TRi,
 vw.SUBCVi,
 vw.SDESi,
 NULL AS ValorATRi,
 vw.PorCOBPAMED,
 vw.PorCOBPP,
 vw.PorCOBEC,
 vw.DiasAtraso,
 vw.ATR_Info,
 vw.MAXATR_Info,
 vwHst.ATR_AC,
 vwHst.ATR_H1,
 vwHst.ATR_H2,
 vwHst.ATR_H3,
 vwHst.ATR_H4,
 vwHst.ATR_H5,
 vwHst.ATR_H6,
 vw.PorPago_Info,
 vwHst.PorPago_AC,
 vwHst.PorPago_H1,
 vwHst.PorPago_H2,
 vwHst.PorPago_H3,
 vwHst.PorPago_H4,
 vwHst.PorPago_H5,
 vwHst.PorPago_H6,
 vw.PorCLTV_Info,
 hip.SaldoTotalValorizado AS SaldoInsoluto,
 vw.MON_Info,
 vw.MonedaNombre,
 CAST(vw.Veces AS DECIMAL(10,2)) AS Veces,
 vw.PPagoIM_Info,
 vw.PPagoIM,
 vw.TRi_Info,
 val.VariableA,
 val.VariableB,
 val.VariableC,
 vw.RAi,
 vw.RAi_Info,
 vw.PromedioDeRet,
 vw.PromedioDeRet_Info
FROM dbo.SICCMX_VW_HipotecarioReservasVariablesPreliminares vw
INNER JOIN (
 SELECT
 tmp.IdHipotecario,
 MAX(CASE WHEN tmp.RN = 1 THEN tmp.ATR ELSE NULL END) AS ATR_AC,
 MAX(CASE WHEN tmp.RN = 2 THEN tmp.ATR ELSE NULL END) AS ATR_H1,
 MAX(CASE WHEN tmp.RN = 3 THEN tmp.ATR ELSE NULL END) AS ATR_H2,
 MAX(CASE WHEN tmp.RN = 4 THEN tmp.ATR ELSE NULL END) AS ATR_H3,
 MAX(CASE WHEN tmp.RN = 5 THEN tmp.ATR ELSE NULL END) AS ATR_H4,
 MAX(CASE WHEN tmp.RN = 6 THEN tmp.ATR ELSE NULL END) AS ATR_H5,
 MAX(CASE WHEN tmp.RN = 7 THEN tmp.ATR ELSE NULL END) AS ATR_H6,
 MAX(CASE WHEN tmp.RN = 1 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_AC,
 MAX(CASE WHEN tmp.RN = 2 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H1,
 MAX(CASE WHEN tmp.RN = 3 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H2,
 MAX(CASE WHEN tmp.RN = 4 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H3,
 MAX(CASE WHEN tmp.RN = 5 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H4,
 MAX(CASE WHEN tmp.RN = 6 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H5,
 MAX(CASE WHEN tmp.RN = 7 THEN CAST(tmp.PrctPagoEx * 100 AS DECIMAL(10,2)) ELSE NULL END) AS PorPago_H6
 FROM dbo.SICCMX_VW_Hipotecario_Var_Historico tmp
 GROUP BY tmp.IdHipotecario
) vwHst ON vw.IdHipotecario = vwHst.IdHipotecario
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON vw.IdHipotecario = hip.IdHipotecario
LEFT OUTER JOIN dbo.SICC_EstadoRegion_Anx16 reg ON vw.Estado = reg.Estado
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Metodologia_ValoresCalculoTR val ON reg.Region = val.Region AND vw.IdConvenio = val.Convenio AND vw.IdMetodologia = val.IdMetodologia
WHERE vw.IdHipotecario = @IdHipotecario;
GO
