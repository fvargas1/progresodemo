SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UserModificationLog_List]
AS
SELECT
	ul.IdUserLog,
	u.Username AS [User],
	um.Username AS UserModified,
	ul.FechaModificacion,
	CASE ul.ModificationType
	WHEN 0 THEN 'Alta'
	WHEN 1 THEN 'Modificacin'
	WHEN 2 THEN 'Baja'
	WHEN 3 THEN 'Cambio de Contrasea'
	END AS ModificationType
FROM dbo.BAJAWARE_UserLog ul
INNER JOIN dbo.BAJAWARE_User u ON ul.IdUser = u.IdUser
INNER JOIN dbo.BAJAWARE_User um ON ul.IdUserModified = um.IdUser
ORDER BY ul.FechaModificacion DESC;
GO
