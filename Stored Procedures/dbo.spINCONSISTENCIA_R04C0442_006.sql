SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_006]

AS



BEGIN



-- Si el acreditado es persona moral para el RFC, se validará que:

-- - La primera posición sea: "_" (guión bajo).

-- - Las siguientes 3 posiciones son alfanuméricos.

-- - Las siguientes 6 posiciones deben ser números (año, mes y día válidos).

-- - Las últimas 3 posiciones son alfanuméricos.



SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PersonalidadJuridica, RFC

FROM dbo.RW_R04C0442

WHERE PersonalidadJuridica = '2' AND (SUBSTRING(RFC,1,1) <> '_' OR 
SUBSTRING(RFC,2,3) LIKE '%[^A-Z0-9]%'   AND SUBSTRING(RFC,2,3) NOT LIKE '%&%' OR 
SUBSTRING(RFC,5,6) LIKE '%[^0-9]%' AND SUBSTRING(RFC,5,6) NOT LIKE '%&%' OR
SUBSTRING(RFC,11,3) LIKE '%[^A-Z0-9]%' AND SUBSTRING(RFC,11,3) NOT LIKE '%&%' );



END
GO
