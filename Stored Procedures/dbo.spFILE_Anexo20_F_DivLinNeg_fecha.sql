SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_F_DivLinNeg_fecha]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @FechaPeriodo DATETIME;

SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_F_DivLinNeg_fecha';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo20
SET errorFormato = 1
WHERE (LEN(F_DivLinNeg)>0 AND ISDATE(F_DivLinNeg) = 0) OR F_DivLinNeg > @FechaPeriodo;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'F_DivLinNeg',
 F_DivLinNeg,
 1,
 @Detalle
FROM dbo.FILE_Anexo20
WHERE (LEN(F_DivLinNeg)>0 AND ISDATE(F_DivLinNeg) = 0) OR F_DivLinNeg > @FechaPeriodo;
GO
