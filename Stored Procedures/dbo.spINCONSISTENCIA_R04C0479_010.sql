SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_010]
AS

BEGIN

-- El campo RESERV CUBIERT GTIA PERSONAL debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaCubierta
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(ReservaCubierta,''),'-1') AS DECIMAL) < 0;

END
GO
