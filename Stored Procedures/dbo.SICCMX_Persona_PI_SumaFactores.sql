SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_PI_SumaFactores]
AS

--Se calcula el valor ponderado de los factores
UPDATE per
SET
 per.PonderadoCuantitativo = (per.FactorCuantitativo * val.FactorCuantitativo) / 100,
 per.PonderadoCualitativo = (per.FactorCualitativo * val.FactorCualitativo) / 100
FROM dbo.SICCMX_Persona_PI per
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = per.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(per.IdClasificacion, '');


--Se calcula el valor sumado de los factores ponderados
UPDATE dbo.SICCMX_Persona_PI
SET FactorTotal = PonderadoCuantitativo + PonderadoCualitativo;


-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'El valor del factor cuantitativo fue de : '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
 + ' y se pondera al '
 + CONVERT(VARCHAR, CONVERT(INT, val.FactorCuantitativo))
 + '% quedando como resultado '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.PonderadoCuantitativo))
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = ppi.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(ppi.IdClasificacion, '');


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'El valor del factor cualitativo fue de : '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCualitativo))
 + ' y se pondera al '
 + CONVERT(VARCHAR, CONVERT(INT, val.FactorCualitativo))
 + '% quedando como resultado '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.PonderadoCualitativo))
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_PI_ValoresDelta val ON val.IdMetodologia = ppi.IdMetodologia AND ISNULL(val.IdClasificacion, '') = ISNULL(ppi.IdClasificacion, '');


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'El valor del factor total fue de : '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.FactorTotal))
 + ' que da por sumar el factor cuantitativo ponderado : '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.PonderadoCuantitativo))
 + ' mas el factor cualitativo ponderado : '
 + CONVERT(VARCHAR, CONVERT(INT, ppi.PonderadoCualitativo))
FROM dbo.SICCMX_Persona_PI ppi;
GO
