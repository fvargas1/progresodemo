SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Reporte_Select]
	@IdReporte BIGINT
AS
SELECT
 vw.IdReporte,
 vw.GrupoReporte,
 vw.Nombre,
 vw.Descripcion,
 vw.ReglamentoOficial,
 vw.ReglamentoOficialPath,
 vw.Periodicidad,
 vw.NombreTabla,
 vw.SProcGenerate,
 vw.SProcGet,
 vw.LayoutPath,
 vw.NombreSSRS, 
 dbo.FN_GetNombreReporteCNBV(@IdReporte) AS NOMBRECNBV 
FROM dbo.RW_VW_Reporte vw
WHERE vw.IdReporte = @IdReporte;
GO
