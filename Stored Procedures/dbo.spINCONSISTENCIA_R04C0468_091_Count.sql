SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_091_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que si la Clave del Estado (cve_estado) está entre 1 y 32, entonces la Localidad del Acreditado (cve_localidad) debe iniciar con 484.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(NULLIF(Estado,'') AS INT) BETWEEN 1 AND 32 AND SUBSTRING(Localidad,1,3) <> '484'

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
