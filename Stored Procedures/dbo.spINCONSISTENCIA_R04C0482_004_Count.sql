SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0482_004_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 134, validar que el mismo ID Metodología CNBV
-- (dat_id_credito_met_cnbv) se encuentre en el subreporte 478 con Tipo de Alta (cve_tipo_alta_credito) = 134.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0482_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0478_INC hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE rep.TipoBaja = '134' AND hist.TipoAltaCredito <> '134';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
