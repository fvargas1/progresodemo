SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Canasta_Consumo]
AS
-- INICIALIZA CANASTA DE GARANTIAS CREDITO
TRUNCATE TABLE dbo.SICCMX_Garantia_Canasta_Consumo;

INSERT INTO dbo.SICCMX_Garantia_Canasta_Consumo (IdConsumo, IdTipoGarantia)
SELECT DISTINCT
	cg.IdConsumo,
	CASE WHEN gar.IdClasificacionNvaMet = 1 THEN NULL ELSE gar.IdTipoGarantia END
FROM dbo.SICCMX_ConsumoGarantia cg
INNER JOIN dbo.SICCMX_VW_Garantia_Consumo gar ON cg.IdGarantia = gar.IdGarantia
INNER JOIN dbo.SICCMX_VW_Consumo con ON cg.IdConsumo = con.IdConsumo
WHERE gar.IdClasificacionNvaMet IN (1,2,3) AND gar.Aplica = 1;


INSERT INTO dbo.SICCMX_Garantia_Canasta_Consumo (IdConsumo, IdTipoGarantia)
SELECT DISTINCT ca.IdConsumo, tg.IdTipoGarantia
FROM dbo.SICCMX_ConsumoAval ca
INNER JOIN dbo.SICC_TipoGarantia tg ON 1=1 AND tg.Codigo='GP'
WHERE ca.Aplica = 1;


-- INSERTANDO EN LOG DE GARANTIAS
--EXEC dbo.SICCMX_Garantia_Log_Insert;
GO
