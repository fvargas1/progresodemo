SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_049_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (cve_ptaje_num_pagos_bcos) es = 52,
-- entonces el Número de pagos en tiempo que la empresa realizó a inst financ bcarias en los últimos 12 meses (dat_num_pgos_tiempo_bcos) debe ser >= 10

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PagosInstBanc,'') = '52' AND CAST(PagosInstBanc AS DECIMAL) < 10;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
