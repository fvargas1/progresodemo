SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_HipotecarioAdicional]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_HipotecarioAdicional WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_HipotecarioAdicional (
	IdPeriodoHistorico,
	Hipotecario,
	SaldoPromedio,
	InteresesDevengados,
	Comisiones
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	hip.Codigo,
	ha.SaldoPromedio,
	ha.InteresesDevengados,
	ha.Comisiones
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioAdicional ha ON hip.IdHipotecario = ha.IdHipotecario;
GO
