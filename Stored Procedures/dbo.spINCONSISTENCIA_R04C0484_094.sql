SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_094]
AS
BEGIN
-- Validar que el Monto Dispuesto de la Línea de Crédito en el Mes (dat_monto_credito_dispuesto) sea MAYOR O IGUAL a 0.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	MontoDispuesto
FROM dbo.RW_VW_R04C0484_INC
WHERE CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL) < 0

END
GO
