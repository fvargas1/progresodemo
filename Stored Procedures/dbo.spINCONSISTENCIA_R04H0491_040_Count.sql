SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_040_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- En el "NUMERO DEL AVALUO DEL INMUEBLE" el valor de los dígitos 8 y 9 en conjunto tomara valores entre 01 y 32.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE ISNUMERIC(SUBSTRING(NumeroAvaluo,8,2)) = 1 AND CAST(SUBSTRING(NumeroAvaluo,8,2) AS INT) NOT BETWEEN 1 AND 32;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
