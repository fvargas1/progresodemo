SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_ByGrupoReporte]
	@GrupoReporte VARCHAR(50)
AS
SELECT
	inc.IdInconsistencia,
	cat.Nombre AS Reporte,
	inc.InconsistenciaNombre,
	inc.Numero,
	inc.Fecha
FROM dbo.RW_Reporte rep
INNER JOIN dbo.MIGRACION_Categoria cat ON rep.GrupoReporte + rep.Nombre = cat.Nombre
INNER JOIN dbo.MIGRACION_VW_InconsistenciaResumen inc ON cat.IdCategoria = inc.IdCategoria
WHERE GrupoReporte = @GrupoReporte AND inc.Numero > 0
ORDER BY cat.Nombre;
GO
