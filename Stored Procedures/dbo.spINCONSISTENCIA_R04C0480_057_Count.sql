SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_057_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo RFC Acreditado no tenga más de un Nombre del Acreditado diferente.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0480
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_R04C0480 rep
	INNER JOIN dbo.RW_R04C0480 rep2 ON rep.RFC = rep2.RFC
	AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
