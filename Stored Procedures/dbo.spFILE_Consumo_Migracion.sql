SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_Migracion]
AS

UPDATE con
SET
 IdPersona = per.IdPersona,
 SaldoCapitalVigente = NULLIF(f.SaldoCapitalVigente,''),
 InteresVigente = NULLIF(f.SaldoInteresVigente,''),
 SaldoCapitalVencido = NULLIF(f.SaldoCapitalVencido,''),
 InteresVencido = NULLIF(f.SaldoInteresVencido,''),
 InteresCarteraVencida = NULLIF(f.InteresCarteraVencida,''),
 Producto = LTRIM(RTRIM(f.Producto)),
 IdReestructura = reest.IdReestructuraConsumo,
 TasaInteres = NULLIF(f.TasaInteres,''),
 IdSituacionCredito = sit.IdSituacionConsumo,
 IdPeriodicidadCapital = perCon.IdPeriodicidadPagoConsumo,
 IdTipoCredito = tcc.IdTipoCreditoConsumo,
 EsRevolvente = ISNULL(NULLIF(f.EsRevolvente,''),0),
 IdPeriodicidadCalificacion = perCal.IdPeriodicidadCalificacionConsumo
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.FILE_Consumo f ON con.Codigo = LTRIM(f.CodigoCredito)
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo reest ON LTRIM(f.TipoReestructura) = reest.Codigo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON LTRIM(f.SituacionCredito) = sit.Codigo
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo perCon ON LTRIM(f.PeriodicidadPagoCapital) = perCon.Codigo
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tcc ON LTRIM(f.TipoCredito) = tcc.Codigo
LEFT OUTER JOIN dbo.SICC_PeriodicidadCalificacionConsumo perCal ON LTRIM(f.Periodicidad) = perCal.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Consumo (
 Codigo,
 IdPersona,
 SaldoCapitalVigente,
 InteresVigente,
 SaldoCapitalVencido,
 InteresVencido,
 InteresCarteraVencida,
 Producto,
 IdReestructura,
 TasaInteres,
 IdSituacionCredito,
 IdPeriodicidadCapital,
 IdTipoCredito,
 EsRevolvente,
 IdPeriodicidadCalificacion
)
SELECT
 LTRIM(RTRIM(f.CodigoCredito)),
 per.IdPersona,
 NULLIF(f.SaldoCapitalVigente,''),
 NULLIF(f.SaldoInteresVigente,''),
 NULLIF(f.SaldoCapitalVencido,''),
 NULLIF(f.SaldoInteresVencido,''),
 NULLIF(f.InteresCarteraVencida,''),
 LTRIM(RTRIM(f.Producto)),
 reest.IdReestructuraConsumo,
 NULLIF(f.TasaInteres,''),
 sit.IdSituacionConsumo,
 perCon.IdPeriodicidadPagoConsumo,
 tcc.IdTipoCreditoConsumo,
 ISNULL(NULLIF(f.EsRevolvente,''),0),
 perCal.IdPeriodicidadCalificacionConsumo
FROM dbo.FILE_Consumo f
INNER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo reest ON LTRIM(f.TipoReestructura) = reest.Codigo
LEFT OUTER JOIN dbo.SICC_SituacionConsumo sit ON LTRIM(f.SituacionCredito) = sit.Codigo
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo perCon ON LTRIM(f.PeriodicidadPagoCapital) = perCon.Codigo
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tcc ON LTRIM(f.TipoCredito) = tcc.Codigo
LEFT OUTER JOIN dbo.SICC_PeriodicidadCalificacionConsumo perCal ON LTRIM(f.Periodicidad) = perCal.Codigo
LEFT OUTER JOIN dbo.SICCMX_Consumo con ON LTRIM(f.CodigoCredito) = con.Codigo
WHERE con.Codigo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


SET NOCOUNT ON;
-- CONSUMO INFO
INSERT INTO dbo.SICCMX_ConsumoInfo (IdConsumo)
SELECT DISTINCT c.IdConsumo
FROM dbo.SICCMX_Consumo c
LEFT OUTER JOIN dbo.SICCMX_ConsumoInfo ci ON c.IdConsumo = ci.IdConsumo
WHERE ci.IdConsumo IS NULL;

-- CONSUMO ADICIONAL
INSERT INTO dbo.SICCMX_ConsumoAdicional (IdConsumo)
SELECT DISTINCT c.IdConsumo
FROM dbo.SICCMX_Consumo c
LEFT OUTER JOIN dbo.SICCMX_ConsumoAdicional ca ON c.IdConsumo = ca.IdConsumo
WHERE ca.IdConsumo IS NULL;

-- CONSUMO RESERVAS VARIABLES PRELIMINARES
INSERT INTO dbo.SICCMX_Consumo_Reservas_VariablesPreliminares(IdConsumo)
SELECT DISTINCT c.IdConsumo
FROM dbo.SICCMX_Consumo c
LEFT OUTER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares cr ON cr.IdConsumo = c.IdConsumo
WHERE cr.IdConsumo IS NULL;



-- UPDATE SICCMX_ConsumoInfo
UPDATE cInfo
SET
 FechaOtorgamiento = NULLIF(f.FechaAlta,''),
PagoExigible = NULLIF(f.PagoExigible,''),
 CodigoCreditoReestructurado = LTRIM(RTRIM(f.CodigoCreditoReestructurado)),
 FechaDisposicion = NULLIF(f.FechaDisposicion,''),
 FechaVencimiento = NULLIF(f.FechaVencimiento,''),
 PlazoTotal = NULLIF(f.PlazoTotal,''),
 MontoLineaAutorizada = NULLIF(f.ImporteOriginal,''),
 ValorOriginalBien = NULLIF(f.ValorOriginal,''),
 IdMecanismoPago = frm.IdFormaPagoConsumo,
 FechaCorte = NULLIF(f.FechaCorte,''),
 PlazoRemanente = NULLIF(f.PlazoRemanente,''),
 MontoExigible = NULLIF(f.MontoExigible,''),
 PagoRealizado = NULLIF(f.PagoRealizado,''),
 DiasAtraso = NULLIF(f.DiasAtraso,''),
 ExigibleTeorico = NULLIF(f.ExigibleTeorico,''),
 CodigoAgrupacion = LTRIM(RTRIM(f.CodigoAgrupacion)),
 IdTipoGarantia = tg.IdTipoGarantiaConsumo,
 ImporteGarantia = NULLIF(f.ImporteGarantia,''),
 RegistroUnicoGarantia = LTRIM(RTRIM(f.RegistroUnicoGarantia)),
 QuitasCondonaciones = NULLIF(f.QuitasCondonaciones,''),
 FechaBaja = NULLIF(f.FechaBaja,''),
 IdTipoBaja = tpoBj.IdTipoBajaConsumo,
 BonificacionesDescuentos = NULLIF(f.BonificacionesDescuentos,''),
 NuevoFolioCredito = LTRIM(RTRIM(f.NuevoFolioCredito)),
 CreditoCodificado = LTRIM(RTRIM(f.CreditoCodificado)),
 ClienteCodificado = LTRIM(RTRIM(f.ClienteCodificado)),
 IdTipoProductoSerie4 = tcR04A.IdTipoProducto,
 FolioConsultaBuro = LTRIM(RTRIM(f.FolioConsultaBuro)),
 PagoRealizadoCal = NULLIF(f.PagoRealizadoCal,''),
 LimCredito = NULLIF(f.LimCredito,''),
 PagoNGI = NULLIF(f.PagoNGI,''),
 SaldoRev = NULLIF(f.SaldoRev,''),
 SaldoPMSI = NULLIF(f.SaldoPMSI,''),
 PagoMin = NULLIF(f.PagoMin,''),
 SaldoPCI = NULLIF(f.SaldoPCI,''),
 InteresPCI = NULLIF(f.InteresPCI,''),
 IdMoneda = mon.IdMoneda,
 PorcentajePagoRealizado = NULLIF(f.PorcentajePagoRealizado,''),
 ATR = NULLIF(f.ATR,''),
 MaxATR = NULLIF(f.MaxATR,''),
 INDATR = NULLIF(f.INDATR,''),
 VECES = NULLIF(f.VECES,''),
 Impago = NULLIF(f.Impago,''),
 NumeroImpagosConsecutivos = NULLIF(f.NumeroImpagosConsecutivos,''),
 NumeroImpagosHistoricos = NULLIF(f.NumeroImpagosHistoricos,''),
 MesesTranscurridos = NULLIF(f.MesesTranscurridos,''),
 PorPago = NULLIF(f.PorPago,''),
 PorSDOIMP = NULLIF(f.PorSDOIMP,''),
 PorPR = NULLIF(f.PorPR,''),
 PorcentajeUso = NULLIF(f.PorcentajeUso,''),
 TarjetaActiva = ISNULL(NULLIF(f.TarjetaActiva,''),0),
 Aforo = NULLIF(f.Aforo,''),
 Enganche = NULLIF(f.Enganche,''),
 ValorTotalCredito = NULLIF(f.ValorTotalCredito,''),
 CAT = NULLIF(f.CAT,''),
 ExigibleSinSegurosComisiones = NULLIF(f.ExigibleSinSegurosComisiones,''),
 IdModalidadCAT = modCAT.IdModalidadCAT,
 PrimaSeguroObligatorio = NULLIF(f.PrimaSeguroObligatorio,''),
 PrimaSeguroDanos = NULLIF(f.PrimaSeguroDanos,''),
 IdTipoProductoSerie4_2016 = tipoPro4_16.IdTipoProducto,
 DiasVencidos = NULLIF(f.DiasVencidos,''),
 AntiguedadAcreditado = NULLIF(f.AntiguedadAcreditado,''),
 MonExiRepSIC = NULLIF(f.MonExiRepSIC,''),
 MonExiIns = NULLIF(f.MonExiIns,''),
 Meses_desde_ult_atr_bk = NULLIF(f.Meses_desde_ult_atr_bk,''),
 FechaBuro = NULLIF(f.FechaBuro,''),
 GVeces1 = CAST(NULLIF(f.GVeces1,'') AS DECIMAL),
 GVeces2 = CAST(NULLIF(f.GVeces2,'') AS DECIMAL),
 GVeces3 = CAST(NULLIF(f.GVeces3,'') AS DECIMAL),
 VecesMontoBanco = NULLIF(f.VecesMontoBanco,''),
 Alto = CAST(NULLIF(f.Alto,'') AS DECIMAL),
 Medio = CAST(NULLIF(f.Medio,'') AS DECIMAL),
 Bajo = CAST(NULLIF(f.Bajo,'') AS DECIMAL),
 limcreditocalif = NULLIF(f.limcreditocalif,''),
 idgarantia = ind.IdIndicador,
 indicadorcat = NULLIF(f.indicadorcat,''),
 comtotal = NULLIF(f.comtotal,''),
 comtardio = NULLIF(f.comtardio,''),
 pagonginicio = NULLIF(f.pagonginicio,''),
 pagoexigepsi = NULLIF(f.pagoexigepsi,'')
FROM dbo.FILE_Consumo f
INNER JOIN dbo.SICCMX_Consumo con ON LTRIM(f.CodigoCredito) = con.Codigo
INNER JOIN dbo.SICCMX_ConsumoInfo cInfo ON con.IdConsumo = cInfo.IdConsumo
LEFT OUTER JOIN dbo.SICC_FormaPagoConsumo frm ON LTRIM(f.MecanismoPago) = frm.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarantiaConsumo tg ON LTRIM(f.TipoGarantia) = tg.Codigo
LEFT OUTER JOIN dbo.SICC_TipoBajaConsumo tpoBj ON LTRIM(f.TipoBaja) = tpoBj.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tcR04A ON LTRIM(f.TipoCreditoR04A) = tcR04A.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_ModalidadCAT modCAT ON LTRIM(f.ModalidadCAT) = modCAT.Codigo
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4_2016 tipoPro4_16 ON tipoPro4_16.Codigo = LTRIM(f.TipoCreditoR04A_2016)
LEFT OUTER JOIN dbo.SICC_IndicadorGarantia_Consumo ind ON LTRIM(f.garantia) = ind.Codigo
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- UPDATE SICCMX_ConsumoAdicional
UPDATE ca
SET
 SaldoPromedio = NULLIF(f.SaldoPromedio,''),
 InteresDevengado = NULLIF(f.InteresDevengado,''),
 Comision = NULLIF(f.Comision,'')
FROM dbo.SICCMX_ConsumoAdicional ca
INNER JOIN dbo.SICCMX_Consumo con ON ca.IdConsumo = con.IdConsumo
INNER JOIN dbo.FILE_Consumo f ON con.Codigo = LTRIM(f.CodigoCredito)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- UPDATE SICCMX_Consumo_Reservas_VariablesPreliminares
UPDATE pre
SET
 NumeroIntegrantes = NULLIF(f.NumeroIntegrantes,''),
 Ciclos = NULLIF(f.Ciclos,''),
 LimiteCredito = NULLIF(f.LimiteCredito,''),
 SaldoFavor = NULLIF(f.SaldoFavor,''),
 PorcentajePagoRealizado = NULLIF(f.PorcentajePagoRealizado,''),
 ATR = NULLIF(f.ATR,''),
 MaxATR = NULLIF(f.MaxATR,''),
 INDATR = NULLIF(f.INDATR,''),
 VECES = NULLIF(f.VECES,''),
 Impago = NULLIF(f.Impago,''),
 NumeroImpagosConsecutivos = NULLIF(f.NumeroImpagosConsecutivos,''),
 NumeroImpagosHistoricos = NULLIF(f.NumeroImpagosHistoricos,''),
 MesesTranscurridos = NULLIF(f.MesesTranscurridos,''),
 PorPago = NULLIF(f.PorPago,''),
 PorSDOIMP = NULLIF(f.PorSDOIMP,''),
 PorPR = NULLIF(f.PorPR,''),
 PorcentajeUso = NULLIF(f.PorcentajeUso,''),
 TarjetaActiva = ISNULL(NULLIF(f.TarjetaActiva,''),0)
FROM dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre
INNER JOIN dbo.SICCMX_Consumo con ON pre.IdConsumo = con.IdConsumo
INNER JOIN dbo.FILE_Consumo f ON con.Codigo = LTRIM(f.CodigoCredito)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
