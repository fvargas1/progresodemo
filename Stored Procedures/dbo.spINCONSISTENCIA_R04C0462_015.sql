SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0462_015]
AS
BEGIN
-- Validar que el Tipo de Baja corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
	rep.TipoBaja
FROM dbo.RW_VW_R04C0462_INC rep
LEFT OUTER JOIN dbo.SICC_TipoBaja tpo ON ISNULL(rep.TipoBaja,'') = tpo.CodigoCNBV
WHERE tpo.IdTipoBaja IS NULL;

END

GO
