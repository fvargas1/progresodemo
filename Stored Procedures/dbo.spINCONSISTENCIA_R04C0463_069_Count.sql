SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_069_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Un mismo RFC Acreditado (dat_rfc) no debe tener más de un Nombre de Acreditado (dat_nombre) diferentes.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE NombrePersona IN (
	SELECT rep.NombrePersona
	FROM dbo.RW_VW_R04C0463_INC rep
	INNER JOIN dbo.RW_VW_R04C0463_INC rep2 ON rep.RFC = rep2.RFC AND rep.NombrePersona <> rep2.NombrePersona
	GROUP BY rep.NombrePersona, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
