SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_Ins]
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@Name BAJAWARE_utDescripcion100,
	@CodeName BAJAWARE_utDescripcion100,
	@Visible BAJAWARE_utActivo,
	@PageLink BAJAWARE_utDescripcion250,
	@IdUseCaseGroup BAJAWARE_utID,
	@SortOrder BAJAWARE_utInt,
	@StringKey BAJAWARE_utDescripcion100
AS
INSERT INTO dbo.BAJAWARE_UseCase (Active, Description, Name, CodeName, Visible, PageLink, IdUseCaseGroup, SortOrder, StringKey)
VALUES (@Active, @Description, @Name, @CodeName, @Visible, @PageLink, @IdUseCaseGroup, @SortOrder, @StringKey);

SELECT IdUseCase, Active, Description, Name, CodeName, Visible, PageLink, IdUseCaseGroup, SortOrder, StringKey
FROM dbo.BAJAWARE_UseCase
WHERE IdUseCase=SCOPE_IDENTITY();
GO
