SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_004]
AS

BEGIN

-- El "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV" deberá reportarse con letras mayúsculas y números, sin caracteres distintos a estos.

SELECT CodigoCredito, CodigoCreditoCNBV
FROM dbo.RW_R04H0491
WHERE CodigoCreditoCNBV LIKE '%[^A-Z0-9]%' OR BINARY_CHECKSUM(CodigoCreditoCNBV) <> BINARY_CHECKSUM(UPPER(CodigoCreditoCNBV));

END
GO
