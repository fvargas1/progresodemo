SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0477_003_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 133, validar que el mismo ID Metodología CNBV (dat_id_credito_met_cnbv)
-- se encuentre en el subreporte 473 con Tipo de Alta (cve_tipo_alta_credito) = 133.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0477_INC rep
LEFT OUTER JOIN Historico.RW_VW_R04C0473_INC hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE rep.TipoBaja = '133' AND hist.TipoAltaCredito <> '133';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
