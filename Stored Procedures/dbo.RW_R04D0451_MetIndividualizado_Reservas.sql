SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_MetIndividualizado_Reservas]
AS
SELECT
	vw.Nombre AS Calificacion,
	SUM(CASE WHEN rep.TipoSaldo = '1' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS ReservasCierre,
	SUM(CASE WHEN rep.TipoSaldo = '19' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS ReservasCierreHistorico,
	SUM(CASE WHEN rep.TipoSaldo = '115' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS Variacion
FROM dbo.ReportWare_VW_R04D0451Concepto vw
LEFT OUTER JOIN dbo.RW_R04D0451 rep ON vw.Codigo = rep.Concepto AND rep.MetodoCalificacion = 1 AND rep.TipoCalificacion IN ('3')
WHERE vw.Codigo IN (
	'841100000000',
	'841200000000',
	'841500000000',
	'841600000000',
	'841700000000',
	'841800000000',
	'845000000000',
	'842100000000',
	'842200000000',
	'842300000000'
)
GROUP BY vw.Nombre, TipoCalificacion
ORDER BY vw.Nombre;
GO
