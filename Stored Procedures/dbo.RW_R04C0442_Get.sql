SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0442_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	NumeroSecuencia,
	CodigoPersona,
	RFC,
	NombrePersona,
	GrupoRiesgo,
	TipoAcreditadoRel,
	PersonalidadJuridica,
	SectorEconomico,
	ActividadEconomica,
	NumeroEmpleados,
	IngresosBrutos,
	Localidad,
	NumeroConsulta,
	TipoAltaCredito,
	SpecsDisposicionCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoAgrupacion,
	DestinoCredito,
	SaldoCredito,
	DenominacionCredito,
	FechaVencimiento,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	TasaRef,
	AjusteTasaRef,
	FrecuenciaRevisionTasa,
	MontoFondeo,
	CodigoBancoFondeador,
	Comisiones,
	PorcentajeCubierto,
	PorcentajeCubiertoFondeo,
	CodigoBancoGarantia,
	PorcentajeCubiertoAval,
	TipoGarantia,
	Municipio,
	Estado
FROM dbo.RW_VW_R04C0442
ORDER BY NombrePersona;
GO
