SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CedulaNMC_Info_Get]
AS
TRUNCATE TABLE dbo.RW_CedulaNMC_Info;

INSERT INTO dbo.RW_CedulaNMC_Info(
	IdPersona,
	Codigo,
	Nombre,
	RFC,
	MetodologiaNombre,
	MetodologiaDesc,
	Promotor,
	Domicilio,
	Telefono,
	Correo,
	ActividadEconomica,
	SocioPrincipal,
	GrupoEconomico,
	EI,
	Reserva,
	Calificacion,
	FactorCuantitativo,
	FactorCualitativo,
	Alpha,
	MAlpha,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	IdAval,
	CodigoAval,
	NombreAval,
	MetodologiaAval,
	FactorCuantitativoAval,
	FactorCualitativoAval,
	AlphaAval,
	MAlphaAval,
	PonderadoCuantitativoAval,
	PonderadoCualitativoAval,
	FactorTotalAval,
	PIAval
)
SELECT
	p.IdPersona,
	p.Codigo AS CodigoDeudor,
	p.Nombre AS Nombre,
	REPLACE(p.RFC,'_','') AS RFC,
	met.Nombre,
	met.Descripcion,
	ISNULL(e.Nombre,'') AS Promotor,
	p.Domicilio as Domicilio,
	ISNULL(p.Telefono,'NA') AS Telefono,
	ISNULL(p.Correo,'NA') AS Correo,
	ISNULL(ae.Nombre,'NA') AS ActividadEconomica,
	acc.Nombre AS SocioPrincipal,
	pInfo.GrupoRiesgo AS GrupoEconomico,
	pc.EI,
	pc.Reserva,
	cal.Codigo,
	CAST(ppi.FactorCuantitativo AS DECIMAL) AS FactorCuantitativo,
	CAST(ppi.FactorCualitativo AS DECIMAL) AS FactorCualitativo,
	CAST(del.FactorCuantitativo AS DECIMAL) AS Alpha,
	CAST(del.FactorCualitativo AS DECIMAL) AS MAlpha,
	CAST(ppi.PonderadoCuantitativo AS DECIMAL) AS PonderadoCuantitativo,
	CAST(ppi.PonderadoCualitativo AS DECIMAL) AS PonderadoCualitativo,
	CAST(ppi.FactorTotal AS DECIMAL) AS FactorTotal,
	ppi.[PI],
	aval.IdAval,
	aval.Codigo,
	aval.Nombre AS NombreAval,
	aval.Metodologia AS MetodologiaAval,
	aval.FCn AS FactorCuantitativoAval,
	aval.FCl AS FactorCualitativo,
	aval.Alp AS AlphaAval,
	aval.MAlp AS MAlphaAval,
	aval.PCn AS PonderadoCuantitativo,
	aval.PCl AS PonderadoCualitativo,
	aval.FT AS FactorTotal,
	aval.[PI] AS PIAval
FROM dbo.SICCMX_Persona p
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON p.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_ValoresDelta del ON ppi.IdMetodologia = del.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(del.IdClasificacion,0)
INNER JOIN dbo.SICCMX_Metodologia met ON ppi.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICCMX_PersonaCalificacion pc ON ppi.IdPersona = pc.IdPersona
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_ActividadEconomica ae ON ae.IdActividadEconomica = p.IdActividadEconomica
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo e ON e.IdEjecutivo = p.IdEjecutivo
LEFT OUTER JOIN dbo.SICCMX_Accionista acc ON p.IdPersona = acc.IdPersona
OUTER APPLY (
	SELECT TOP 1 a_a.IdAval, a_a.Codigo, a_a.Nombre, met_a.Nombre AS Metodologia,
	CAST(ppi_a.FactorCuantitativo AS DECIMAL) AS FCn,
	CAST(ppi_a.FactorCualitativo AS DECIMAL) AS FCl,
	CAST(del_a.FactorCuantitativo AS DECIMAL) AS Alp,
	CAST(del_a.FactorCualitativo AS DECIMAL) AS MAlp,
	CAST(ppi_a.PonderadoCuantitativo AS DECIMAL) AS PCn,
	CAST(ppi_a.PonderadoCualitativo AS DECIMAL) AS PCl,
	CAST(ppi_a.FactorTotal AS DECIMAL) AS FT,
	ppi_a.[PI]
	FROM dbo.SICCMX_Credito c_a
	INNER JOIN dbo.SICCMX_CreditoAval ca_a ON c_a.IdCredito = ca_a.IdCredito AND ca_a.Aplica = 1
	INNER JOIN dbo.SICCMX_Aval a_a ON ca_a.IdAval = a_a.IdAval
	INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv_a ON ca_a.IdCredito = crv_a.IdCredito
	INNER JOIN dbo.SICCMX_Persona_PI_GP ppi_a ON a_a.IdAval = ppi_a.IdGP
	INNER JOIN dbo.SICCMX_PI_ValoresDelta del_a ON ppi_a.IdMetodologia = del_a.IdMetodologia AND ISNULL(ppi_a.IdClasificacion,0) = ISNULL(del_a.IdClasificacion,0)
	INNER JOIN dbo.SICCMX_Metodologia met_a ON ppi_a.IdMetodologia = met_a.IdMetodologia
	WHERE a_a.Aplica = 1 AND crv_a.PIAval = 1 AND c_a.IdPersona = p.IdPersona
	ORDER BY a_a.PIAval DESC
) AS aval;
GO
