SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_005_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Puntaje Crediticio Cualitativo (dat_puntaje_cred_cualitativo) sea >= 281 pero <= 835.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCualitativo,''),'-1') AS DECIMAL) NOT BETWEEN 281 AND 835;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
