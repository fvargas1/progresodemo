SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_PersonalidadJuridica_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_PersonalidadJuridica_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Aval f
LEFT OUTER JOIN dbo.SICC_TipoPersona cat ON LTRIM(f.PersonalidadJuridica) = cat.Codigo AND cat.Activo = 1
WHERE cat.IdTipoPersona IS NULL AND LEN(f.PersonalidadJuridica) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoAval,
 'PersonalidadJuridica',
 f.PersonalidadJuridica,
 2,
 @Detalle
FROM dbo.FILE_Aval f
LEFT OUTER JOIN dbo.SICC_TipoPersona cat ON LTRIM(f.PersonalidadJuridica) = cat.Codigo AND cat.Activo = 1
WHERE cat.IdTipoPersona IS NULL AND LEN(f.PersonalidadJuridica) > 0;
GO
