SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_032]
AS
BEGIN
-- Validar que la Tasa de Interes corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.TasaInteres
FROM dbo.RW_VW_R04C0453_INC rep
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasa ON ISNULL(rep.TasaInteres,'') = tasa.Codigo
WHERE tasa.IdTasaReferencia IS NULL;

END
GO
