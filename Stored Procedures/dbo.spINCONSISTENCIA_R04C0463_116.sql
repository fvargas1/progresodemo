SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_116]
AS

BEGIN

-- Si el Tipo de Operación (cve_tipo_operacion) es igual a 254 (Crédito Puente), el Destino del Crédito (cve_destino_credito) deberá ser: 330, 332, 333, 334, 338, 430, 432, 433, 434 ó 438.

SELECT
	CodigoCredito,
	TipoOperacion,
	DestinoCredito
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(TipoOperacion,'') = '254' AND ISNULL(DestinoCredito,'') NOT IN ('330','332','333','334','338','430','432','433','434','438')

END

GO
