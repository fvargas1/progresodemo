SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_List]
AS
SELECT
	vw.IdProcesoCalificacion,
	vw.Nombre,
	vw.Descripcion,
	vw.Codename,
	vw.IdMetodologia,
	vw.MetodologiaNombre,
	vw.Position,
	vw.Activo,
	vw.IdGrupoProceso,
	vw.GrupoProcesoNombre,
	CASE WHEN vw.Activo = 1 THEN 'on' ELSE 'blank' END AS ActivoImg,
	vw.Params
FROM dbo.SICC_VW_ProcesoCalificacion vw
ORDER BY vw.MetodologiaNombre, vw.Position;
GO
