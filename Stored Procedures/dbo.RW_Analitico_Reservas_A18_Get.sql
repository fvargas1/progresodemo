SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_Reservas_A18_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Fecha,
	CodigoPersona,
	Nombre,
	CodigoCredito,
	MontoCredito,
	FechaVencimiento,
	[PI],
	Moneda,
	ActividadEconomica,
	MontoGarantia,
	Prct_Reserva,
	SP,
	MontoReserva,
	PI_Aval,
	SP_Aval,
	Calificacion,
	MontoGarantiaAjustado
FROM dbo.RW_VW_Analitico_Reservas_A18
ORDER BY Nombre;
GO
