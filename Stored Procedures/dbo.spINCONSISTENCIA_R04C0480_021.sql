SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_021]
AS

BEGIN

-- Si el Rendimiento Sobre Capital ROE (dat_roe) es = 0, entonces el Puntaje Asignado por Rendimiento Sobre Capital ROE (cve_ptaje_roe) debe ser = 54

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RendCapROE AS ROE,
	P_RendCapROE AS Puntos_ROE
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(RendCapROE AS DECIMAL(10,6)) = 0 AND ISNULL(P_RendCapROE,'') <> '54';

END


GO
