SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0460_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0460';

INSERT INTO dbo.RW_ReporteLog(IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES(@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0460;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0460 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TamanoEntidad, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo,
 CreditoReportadoSIC, HITSIC, FechaConsultaSIC, FechaInfoFinanc, MesesPI100, GarantiaLeyFederal, CumpleCritContGral, LugarRadica, P_DiasMoraInstBanc,
 P_PorcPagoInstBanc, P_PorcPagoInstNoBanc, P_EntFinancRegul, P_PasivoCartCred, P_ROE, P_Capitalizacion, P_GastosAdmon, P_CartVencida, P_MargenFinanciero,
 P_EmisDeudaEntOtor, DiasMoraInstBanc, PorcPagoInstBanc, PorcPagoInstNoBanc, PasivoLargoPlazo, PasivoCortoPlazo, CarteraCredito, RendCapROE, UtilidadNeta,
 CapitalContableProm, CapitalContable, CapitalNeto, ActivoTotalAnual, ActivoSujetoRiesgo, IndiceCapitalizacion, GastosAdmonPromo, IngresosTotales,
 CarteraVencida, ReservasCalifCartera, MargenFinanciero, EstimPreventRiesgo, ActivosProductivos, P_PagosInfonavit, P_DiasAtrasoInfonavit, P_DiasMoraPromBanc,
 P_PagosTiempoBanc, P_PagosTiempoNoBanc, P_Solvencia, P_Liquidez, P_Eficiencia, P_EmisDeudaEntNoOtorg, P_RendimientoCapital, P_NoBancosReg,
 MontoPagosInfonavitUltBim, DiasMoraInfonavitUltBim, DiasMoraPromBancos, PorcenPagoTiempBanc, PorcenPagoTiempNoBanc, RoeEntFinanNoOtorgCred, P_DiversifLineaNeg,
 P_DiversifFuenteFianc, P_ConcentraActivos, P_IndepConsejo, P_ComposicAccionaria, P_GobCorp, P_ExpFuncionarios, P_PoliticasProced, P_EdosFinancAudit
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '0460',
 per.Codigo AS CodigoPersona,
 per.RFC AS RFC,
 REPLACE(pInfo.NombreCNBV, ',', '') AS NombrePersona,
 CASE WHEN ppi.IdClasificacion = 2 THEN '112' WHEN ppi.IdClasificacion = 3 THEN '113' ELSE '111' END AS TamanoEntidad,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.PonderadoCuantitativo AS PuntajeCuantitativo,
 ppi.PonderadoCualitativo AS PuntajeCualitativo,
 crs.CodigoCNBV AS CreditoReportadoSIC,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 CASE WHEN ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) > 12 THEN 0 ELSE ISNULL(DATEDIFF(MONTH,castigados.FechaIngreso,@FechaPeriodo),0) END AS MesesPI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 ccc.CodigoCNBV AS CumpleCritContGral,
 anx.LugarRadica AS LugarRadica,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_DIAS_MORA_INST_BANC] ELSE 0 END AS P_DiasMoraInstBanc,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_POR_PAGO_INST_BANC] ELSE 0 END AS P_PorcPagoInstBanc,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_POR_PAGO_INST_NOBANC] ELSE 0 END AS P_PorcPagoInstNoBanc,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_ENT_FIN] ELSE 0 END AS P_EntFinancRegul,
 ptNMC.[20IA_PROP_PASIVO_LAR_PZO] AS P_PasivoCartCred,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_REND_CAP_ROE] ELSE 0 END AS P_ROE,
 ptNMC.[20IA_IND_CAP] AS P_Capitalizacion,
 ptNMC.[20IA_GAS_ADMON_ING_TOT] AS P_GastosAdmon,
 ptNMC.[20IA_CART_VENC_CC] AS P_CartVencida,
 ptNMC.[20IA_MARG_FIN_AJUS] AS P_MargenFinanciero,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN ptNMC.[20IA_EMI_TIT_DEUD] ELSE 0 END AS P_EmisDeudaEntOtor,
 vlNMC.[20IA_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[20IA_POR_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[20IA_POR_PAGO_INST_NOBANC] AS PorcPagoInstNoBanc,
 anx.PasLargoPlazo AS PasivoLargoPlazo,
 anx.PasExiInmediata AS PasivoCortoPlazo,
 anx.CarteraCredito AS CarteraCredito,
 vlNMC.[20IA_REND_CAP_ROE] AS RendCapROE,
 anx.UtilNetaTrimestre AS UtilidadNeta,
 anx.CapitalContableProm AS CapitalContableProm,
 anx.CapitalContable AS CapitalContable,
 anx.CapitalNeto AS CapitalNeto,
 anx.ActivoTotal AS ActivoTotalAnual,
 anx.ActivoSujetoRiesgo AS ActivoSujetoRiesgo,
 vlNMC.[20IA_IND_CAP] AS IndiceCapitalizacion,
 anx.GastoAdmonPromo AS GastosAdmonPromo,
 anx.IngresosTotales AS IngresosTotales,
 anx.MontoCarteraVencida AS CarteraVencida,
 anx.MontoReserva AS ReservasCalifCartera,
 anx.MargenFinanciero AS MargenFinanciero,
 anx.ReservaResultados AS EstimPreventRiesgo,
 anx.ActivosProductivos AS ActivosProductivos,
 ptNMC.[20IB_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ptNMC.[20IB_DIAS_ATR_INFONAVIT] AS P_DiasAtrasoInfonavit,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN 0 ELSE ptNMC.[20IB_DIAS_MORA_INST_BANC] END AS P_DiasMoraPromBanc,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN 0 ELSE ptNMC.[20IB_POR_PAGO_INST_BANC] END AS P_PagosTiempoBanc,
 ptNMC.[20IB_POR_PAGO_INST_NOBANC] AS P_PagosTiempoNoBanc,
 ptNMC.[20IB_SOLVENCIA] AS P_Solvencia,
 ptNMC.[20IB_LIQUIDEZ] AS P_Liquidez,
 ptNMC.[20IB_EFICIENCIA] AS P_Eficiencia,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN 0 ELSE ptNMC.[20IB_EMI_TIT_DEUD] END AS P_EmisDeudaEntNoOtorg,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN 0 ELSE ptNMC.[20IB_REND_CAP_ROE] END AS P_RendimientoCapital,
 CASE WHEN ISNULL(anx.EntFinAcreOtorgantesCre, 0)=1 THEN 0 ELSE ptNMC.[20IB_ENT_FIN] END AS P_NoBancosReg,
 vlNMC.[20IB_PAGOS_INFONAVIT] AS MontoPagosInfonavitUltBim,
 vlNMC.[20IB_DIAS_ATR_INFONAVIT] AS DiasMoraInfonavitUltBim,
 vlNMC.[20IB_DIAS_MORA_INST_BANC] AS DiasMoraPromBancos,
 vlNMC.[20IB_POR_PAGO_INST_BANC] AS PorcenPagoTiempBanc,
 vlNMC.[20IB_POR_PAGO_INST_NOBANC] AS PorcenPagoTiempNoBanc,
 vlNMC.[20IB_REND_CAP_ROE] AS RoeEntFinanNoOtorgCred,
 ptNMC.[20_DIV_LINEA_NEG] AS P_DiversifLineaNeg,
 ptNMC.[20_DIV_TIPO_FUENTE] AS P_DiversifFuenteFianc,
 ptNMC.[20_CONC_ACTIVOS] AS P_ConcentraActivos,
 ptNMC.[20_INDEP_CONSEJO_ADMON] AS P_IndepConsejo,
 ptNMC.[20_COMP_ACC] AS P_ComposicAccionaria,
 ptNMC.[20_CALIDAD_GOB_CORP] AS P_GobCorp,
 ptNMC.[20_EXP_FUNC] AS P_ExpFuncionarios,
 ptNMC.[20_EXIST_POL_PROC] AS P_PoliticasProced,
 ptNMC.[20_EST_FIN_AUDIT] AS P_EdosFinancAudit
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo20 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasValor_A20 vlNMC ON ppi.IdPersona = vlNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A20 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
LEFT OUTER JOIN dbo.SICC_CredRepSIC crs ON cInfo.CredRepSIC = crs.IdCredRepSIC
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN (
 SELECT IdPersona, MAX(FechaIngreso) AS FechaIngreso
 FROM dbo.SICCMX_Creditos_Castigados_SIC
 GROUP BY IdPersona
) AS castigados ON per.IdPersona = castigados.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
LEFT OUTER JOIN dbo.SICC_CumpleCritContGral ccc ON cInfo.CumpleCritContGral = ccc.IdCumpleCritContGral
WHERE cre.MontoValorizado > 0;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0460 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
