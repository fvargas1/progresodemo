SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_019]
AS

BEGIN

-- La responsabilidad total deberá ser mayor o igual al saldo del principal al final del periodo.

SELECT CodigoCredito, CodigoCreditoCNBV, ResponsabilidadTotal, SaldoPrincipalFinal
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(ResponsabilidadTotal,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoPrincipalFinal,''),'0') AS DECIMAL);

END
GO
