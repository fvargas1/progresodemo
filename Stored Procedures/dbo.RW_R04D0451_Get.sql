SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Concepto,
	Subreporte,
	Moneda,
	MetodoCalificacion,
	TipoCalificacion,
	TipoSaldo,
	Dato
FROM dbo.RW_VW_R04D0451
ORDER BY Concepto;
GO
