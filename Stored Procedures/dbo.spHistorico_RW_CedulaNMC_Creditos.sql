SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_CedulaNMC_Creditos]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.RW_CedulaNMC_Creditos WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_CedulaNMC_Creditos (
	IdPeriodoHistorico, CodigoPersona, NumeroLinea, CodigoCredito, SaldoTotal, MontoCubierto, MontoExpuesto, EI, TipoLinea, PI_Ponderada,
	SP_Ponderada, Reserva, PrctReserva, Calificacion
)
SELECT
	@IdPeriodoHistorico,
	p.Codigo,
	rw.NumeroLinea,
	rw.CodigoCredito,
	rw.SaldoTotal,
	rw.MontoCubierto,
	rw.MontoExpuesto,
	rw.EI,
	rw.TipoLinea,
	rw.PI_Ponderada,
	rw.SP_Ponderada,
	rw.Reserva,
	rw.PrctReserva,
	rw.Calificacion
FROM dbo.RW_CedulaNMC_Creditos rw
INNER JOIN dbo.SICCMX_Persona p ON rw.IdPersona = p.IdPersona;
GO
