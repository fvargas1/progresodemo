SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_008]
AS
BEGIN
-- Si el Saldo de las Participaciones Elegibles (dat_saldo_particip_elegibles) es > 25 y <= 70
-- entonces el Puntaje Deuda Total a Participaciones Elegibles (cve_puntaje_deuda_particip) debe ser = 139

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoPartEleg,
	P_DeudaTotalPartEleg AS Puntos_SdoPartEleg
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoPartEleg,''),'0') AS DECIMAL(18,6)) BETWEEN 25.000001 AND 70 AND ISNULL(P_DeudaTotalPartEleg,'') <> '139';

END
GO
