SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_032]
AS
BEGIN
-- Las comisiones por apertura deben ser mayores o iguales a cero.

SELECT
 CodigoCredito,
 REPLACE(NombrePersona, ',', '') AS NombreDeudor,
 ComisionAperturaMonto
FROM dbo.RW_VW_R04C0463_INC
WHERE CAST(ISNULL(NULLIF(ComisionAperturaMonto,''),-1) AS DECIMAL(23,2)) < 0;

END
GO
