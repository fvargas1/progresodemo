SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_037]
AS
BEGIN
-- Validar que el ID Acreditado Asigando (dat_id_acreditado_institucion) no tenga más de un RFC (dat_rfc).

SELECT
	CodigoCredito,
	REPLACE(CodigoPersona, ',', '') AS CodigoPersona,
	RFC
FROM dbo.RW_VW_R04C0483_INC
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_VW_R04C0483_INC rep
	INNER JOIN dbo.RW_VW_R04C0483_INC rep2 ON rep.CodigoPersona = rep2.CodigoPersona AND rep.RFC <> rep2.RFC
	GROUP BY rep.RFC, rep.CodigoPersona
);

END
GO
