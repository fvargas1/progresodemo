SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0475_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0475_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0475_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0475_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
 HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, CumpleCritContGral, P_AntSocCred, P_QuitCastReest, P_PorcPagoInstNoBanc,
 P_PorcPagoEntComer, P_CredAbInstBanc, P_MonMaxCred, P_MesesUltCredAb, P_PorcPagoInstBanc, P_PorcPagoInstBanc29, P_PorcPagoInstBanc90, P_DiasMoraInstBanc,
 P_PagosInstBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_IndPersFid, P_ProcOrigAdmon, AntSocCred, PorcPagoInstNoBanc, PorcPagoEntComer,
 CredAbInstBanc, MonMaxCred, MesesUltCredAb, PorcPagoInstBanc, PorcPagoInstBanc29, PorcPagoInstBanc90, DiasMoraInstBanc, PagosInstBanc, PagosInfonavit,
 DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, IndPersFid, VentasNetasTotales
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '475',
 per.Codigo AS CodigoPersona,
 CASE WHEN anx.SinAtrasos = 1 THEN '1' ELSE '2' END AS SinAtrasos,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 crs.CredRepSICCNBV AS CreditoReportadoSIC,
 htc.CodigoCNBV AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 ppi.MesesPI100 AS MesesPI100,
 pi100.Codigo AS ID_PI100,
 CASE WHEN garLey.IdPersona IS NULL THEN 790 ELSE 770 END AS GarantiaLeyFederal,
 crs.CumpleCritCNBV AS CumpleCritContGral,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_ANT_SOC_CRED] ELSE ptNMC.[21CA_ANT_SOC_CRED] END AS P_AntSocCred,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_QUIT_CAST_REEST] ELSE ptNMC.[21CA_QUIT_CAST_REEST] END AS P_QuitCastReest,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_PORC_PAGO_INST_NOBANC] ELSE ptNMC.[21CA_PORC_PAGO_INST_NOBANC] END AS P_PorcPagoInstNoBanc,
 ptNMC.[21SA_PORC_PAGO_ENT_COMER] AS P_PorcPagoEntComer,
 ptNMC.[21SA_CRED_AB_INST_BANC] AS P_CredAbInstBanc,
 ptNMC.[21SA_MON_MAX_CRED] AS P_MonMaxCred,
 ptNMC.[21SA_MESES_ULT_CRED_AB] AS P_MesesUltCredAb,
 ptNMC.[21SA_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[21CA_PORC_PAGO_INST_BANC29] AS P_PorcPagoInstBanc29,
 ptNMC.[21CA_PORC_PAGO_INST_BANC90] AS P_PorcPagoInstBanc90,
 ptNMC.[21CA_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[21CA_PAGOS_INST_BANC] AS P_PagosInstBanc,
 ptNMC.[21SA_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ISNULL(ptNMC.[21SA_DIAS_ATR_INFONAVIT], ptNMC.[21CA_DIAS_ATR_INFONAVIT]) AS P_DiasAtrInfonavit,
 ISNULL(ptNMC.[21SA_TASA_RET_LAB], ptNMC.[21CA_TASA_RET_LAB]) AS P_TasaRetLab,
 ptNMC.[21CA_IND_PERS_FID] AS P_IndPersFid,
 CASE WHEN anx.SinAtrasos = 1 THEN ptNMC.[21SA_PROC_ORIG_ADMON] ELSE ptNMC.[21CA_PROC_ORIG_ADMON] END AS P_ProcOrigAdmon,
 CASE WHEN anx.SinAtrasos = 1 THEN vlNMC.[21SA_ANT_SOC_CRED] ELSE vlNMC.[21CA_ANT_SOC_CRED] END AS AntSocCred,
 CASE WHEN anx.SinAtrasos = 1 THEN vlNMC.[21SA_PORC_PAGO_INST_NOBANC] ELSE vlNMC.[21CA_PORC_PAGO_INST_NOBANC] END AS PorcPagoInstNoBanc,
 vlNMC.[21SA_PORC_PAGO_ENT_COMER] AS PorcPagoEntComer,
 vlNMC.[21SA_CRED_AB_INST_BANC] AS CredAbInstBanc,
 vlNMC.[21SA_MON_MAX_CRED] AS MonMaxCred,
 vlNMC.[21SA_MESES_ULT_CRED_AB] AS MesesUltCredAb,
 vlNMC.[21SA_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[21CA_PORC_PAGO_INST_BANC29] AS PorcPagoInstBanc29,
 vlNMC.[21CA_PORC_PAGO_INST_BANC90] AS PorcPagoInstBanc90,
 vlNMC.[21CA_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[21CA_PAGOS_INST_BANC] AS PagosInstBanc,
 vlNMC.[21SA_PAGOS_INFONAVIT] AS PagosInfonavit,
 ISNULL(vlNMC.[21SA_DIAS_ATR_INFONAVIT], vlNMC.[21CA_DIAS_ATR_INFONAVIT]) AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 ISNULL(vlNMC.[21SA_TASA_RET_LAB], vlNMC.[21CA_TASA_RET_LAB]) AS TasaRetLab,
 ISNULL(vlNMC.[21CA_IND_PERS_FID], anx.IndPerMorales) AS IndPersFid,
 anx.VentNetTotAnuales AS VentasNetasTotales
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON anx.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasValor_A21 vlNMC ON ppi.IdPersona = vlNMC.IdPersona
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A21 ptNMC ON ppi.IdPersona = ptNMC.IdPersona
LEFT OUTER JOIN dbo.SICCMX_VW_Persona_CredRepSIC crs ON per.IdPersona = crs.IdPersona
LEFT OUTER JOIN dbo.SICC_HitSic htc ON pInfo.HITSIC = htc.IdHitSic
LEFT OUTER JOIN dbo.SICCMX_VW_Personas_GarantiaLey garLey ON per.IdPersona = garLey.IdPersona
LEFT OUTER JOIN dbo.SICC_PI100 pi100 ON per.PI100 = pi100.IdPI100
WHERE ISNULL(anx.OrgDescPartidoPolitico,0) = 1;

EXEC dbo.RW_R04C0475_2016_Fact_Generate @IdReporteLog, @IdPeriodo, @Entidad; -- FACTORADO
EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0475_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
