SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_109_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Estado (cve_estado).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0463_INC
WHERE Estado IN (
	SELECT rep.Estado
	FROM dbo.RW_VW_R04C0463_INC rep
	INNER JOIN dbo.RW_VW_R04C0463_INC rep2 ON rep.RFC = rep2.RFC AND rep.Estado <> rep2.Estado
	GROUP BY rep.Estado, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
