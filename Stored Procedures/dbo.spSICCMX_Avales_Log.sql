SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Avales_Log]
	@IdAval BIGINT
AS
SELECT
	IdAval,
	FechaCalculo,
	Usuario,
	Descripcion
FROM dbo.SICCMX_Avales_Log
WHERE IdAval = @IdAval
ORDER BY FechaCalculo;
GO
