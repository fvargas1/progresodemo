SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_031]
AS

BEGIN

-- Si el Puntaje Asignado por el Número de Meses desde el Último Crédito Abierto en los últimos 12 meses (cve_ptaje_meses_ultim_cred) es = 46,
-- entonces el Número de Meses desde el Último Crédito Abierto en los últimos 12 meses (dat_num_meses_desde_ult_cred) debe ser >= 0 y < 6

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MesesUltCredAb,
	P_MesesUltCredAb AS Puntos_MesesUltCredAb
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_MesesUltCredAb,'') = '46' AND (CAST(MesesUltCredAb AS DECIMAL) < 0 OR CAST(MesesUltCredAb AS DECIMAL) >= 6);

END


GO
