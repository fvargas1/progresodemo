SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_061_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que la Clave de la Institución contenida en el ID Metodología CNBV (dat_id_credito_met_cnbv) posiciones 2 a la 7, exista en el catálogo de instituciones.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC r04
LEFT OUTER JOIN dbo.SICC_Institucion ins ON SUBSTRING(r04.CodigoCreditoCNBV,2,6) = ins.Codigo
WHERE ins.IdInstitucion IS NULL

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
