SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_007]

AS



BEGIN



-- El Nombre del Acreditado sólo se aceptan letras en mayúsculas y números, sin caracteres distintos a estos, sin puntos ni comas.



SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, NombrePersona

FROM dbo.RW_R04C0443

WHERE NombrePersona LIKE '%[^A-Z0-9 ]%' AND NombrePersona NOT LIKE '%&%' OR BINARY_CHECKSUM(NombrePersona) <> BINARY_CHECKSUM(UPPER(NombrePersona));



END
GO
