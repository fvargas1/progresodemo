SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_ComisionesCobradas_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_ComisionesCobradas_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_LineaCredito
SET errorFormato = 1
WHERE LEN(ComisionesCobradas)>0 AND (ISNUMERIC(ISNULL(ComisionesCobradas,'0')) = 0 OR ComisionesCobradas LIKE '%[^0-9 .]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 NumeroLinea,
 'ComisionesCobradas',
 ComisionesCobradas,
 1,
 @Detalle
FROM dbo.FILE_LineaCredito
WHERE LEN(ComisionesCobradas)>0 AND (ISNUMERIC(ISNULL(ComisionesCobradas,'0')) = 0 OR ComisionesCobradas LIKE '%[^0-9 .]%');
GO
