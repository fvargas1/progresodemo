SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioReservas_Select]
	@IdHipotecario BIGINT
AS
SELECT
	vw.IdHipotecario,
	vw.FechaCalificacion,
	vw.IdCalificacionExpuesto,
	vw.IdCalificacionExpuestoNombre,
	vw.MontoExpuesto,
	vw.SPExpuesto,
	vw.ReservaExpuesto,
	vw.PorcentajeExpuesto,
	vw.IdCalificacionCubierto,
	vw.IdCalificacionCubiertoNombre,
	vw.MontoCubierto,
	vw.SPCubierto,
	vw.ReservaCubierto,
	vw.PorcentajeCubierto,
	vw.IdCalificacionAdicional,
	vw.IdCalificacionAdicionalNombre,
	vw.MontoAdicional,
	vw.ReservaAdicional,
	vw.PorcentajeAdicional
FROM dbo.SICCMX_VW_HipotecarioReservas vw
WHERE vw.IdHipotecario = @IdHipotecario;
GO
