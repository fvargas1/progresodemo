SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Entidad_List]
AS
SELECT
	vw.IdEntidad,
	vw.Nombre,
	vw.Descripcion,
	vw.NombreView
FROM dbo.MIGRACION_VW_Entidad vw
ORDER BY Nombre ASC;
GO
