SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_040]
AS

BEGIN

-- La "PÉRDIDA ESPERADA" debe estar expresado a seis decimales.

SELECT CodigoCredito, CodigoCreditoCNBV, PerdidaEsperada
FROM dbo.RW_R04H0492
WHERE LEN(SUBSTRING(PerdidaEsperada,CHARINDEX('.',PerdidaEsperada)+1,LEN(PerdidaEsperada))) <> 6;

END
GO
