SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_TipoCambio_Migracion]
AS

SET DATEFORMAT YMD;

DECLARE @IdPeriodo BIGINT;

SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;



UPDATE tc

SET Valor = f.ValorTipoCambio

FROM dbo.SICC_TipoCambio tc

INNER JOIN dbo.SICC_Moneda m ON tc.IdMoneda = m.IdMoneda

INNER JOIN dbo.SICC_Periodo p ON tc.IdPeriodo = @IdPeriodo

INNER JOIN dbo.FILE_TipoCambio f ON LTRIM(RTRIM(f.CodigoMonedaISO)) = m.CodigoISO

WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO
