SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_034]
AS

BEGIN

-- Si la Rotación de Capital de Trabajo (dat_rotac_capital_trabajo) es > 3.94 y <= 8.81 ,
-- entonces el Puntaje Asignado por Rotación de Capital de Trabajo (cve_ptaje_rotac_capit_trabajo) debe ser = 71

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	RotCapTrabajo,
	P_RotCapTrabajo AS Puntos_RRotCapTrabajo
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(RotCapTrabajo AS DECIMAL(14,6)) > 3.94 AND CAST(RotCapTrabajo AS DECIMAL(14,6)) <= 8.81 AND ISNULL(P_RotCapTrabajo,'') <> '71';

END


GO
