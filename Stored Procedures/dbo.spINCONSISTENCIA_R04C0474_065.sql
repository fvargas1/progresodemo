SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_065]
AS

BEGIN

-- Validar que la Probabilidad de Incumplimiento Parte no Cubierta por Garantías Personales (dat_p_i_no_cubier_persona) se encuentre entre 0 y 100.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	NumeroDisposicion,
	PIExpuesta AS PI_Expuesta
FROM dbo.RW_R04C0474
WHERE CAST(ISNULL(NULLIF(PIExpuesta,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
