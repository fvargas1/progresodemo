SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_029]
AS
BEGIN
-- La fecha de disposición debe ser una fecha válida

SELECT
	CodigoCredito,
	FechaDisposicion
FROM dbo.RW_VW_R04C0454_INC
WHERE LEN(ISNULL(FechaDisposicion,'')) <> 6 OR ISDATE(ISNULL(FechaDisposicion,'') + '15') = 0;

END
GO
