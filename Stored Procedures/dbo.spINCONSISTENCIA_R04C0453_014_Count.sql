SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_014_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- El Monto de la línea de Crédito Autorizado (dat_monto_credito_linea_aut) debe ser  MAYOR a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0453_INC
WHERE CAST(ISNULL(NULLIF(MonLineaCred,''),'-1') AS DECIMAL) <= 0
	AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','137','138','139','700','701','702','731','732','733','741','742','743','744','751');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
