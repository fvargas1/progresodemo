SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Hipotecario_Reservas_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	SaldoCapitalVigente,
	InteresVigente,
	SaldoCapitalVencido,
	InteresVencido,
	Constante,
	FactorATR,
	ATR,
	FactorMAXATR,
	MAXATR,
	FactorProPago,
	PorPago,
	FactorPorCLTV,
	PorCLTVi,
	FactorINTEXP,
	INTEXP,
	FactorMON,
	MON,
	[PI],
	SP,
	E,
	Reserva,
	PorReserva,
	Calificacion
FROM Historico.RW_Hipotecario_Reservas
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Codigo;
GO
