SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0450_008]

AS

BEGIN

-- Validar que la Localidad del Garante corresponda a Catalogo de CNBV.



SELECT DISTINCT

 r50.CodigoCredito,

 REPLACE(r50.NombreAcreditado, ',', '') AS NombreDeudor,

 r50.CodigoGarante,

 r50.NombreGarante,

 r50.Localidad

FROM dbo.RW_VW_R04C0450_INC r50

LEFT OUTER JOIN dbo.SICC_Localidad2015 mun ON r50.Localidad = mun.CodigoCNBV

WHERE mun.IdLocalidad IS NULL;



END
GO
