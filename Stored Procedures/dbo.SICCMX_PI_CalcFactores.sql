SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_CalcFactores]
AS

--Actualizamos valores.
UPDATE per
SET
 per.FactorCuantitativo = fact.FAC_CUANTI,
 per.FactorCualitativo = fact.FAC_CUALI
FROM dbo.SICCMX_Persona_PI per
INNER JOIN SICCMX_VW_Persona_PI_Factores fact ON fact.IdPersona = per.IdPersona;

-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdPersona,
 GETDATE(),
 '',
 'El valor del factor cuantitativo fue de : '+ CAST(CAST(FactorCuantitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 IdPersona,
 GETDATE(),
 '',
 'El valor del factor cualitativo fue de : '+ CAST(CAST(FactorCualitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI;


UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 90
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se suman 90 puntos al factor cuantitativo quedando como resultado '+ CAST(CAST(ppi.FactorCuantitativo AS DECIMAL) AS VARCHAR(5))
FROM dbo.SICCMX_Persona_PI ppi
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona;
GO
