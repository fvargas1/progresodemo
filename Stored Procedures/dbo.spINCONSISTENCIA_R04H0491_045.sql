SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_045]
AS

BEGIN

-- La fecha de vencimiento del crédito es cero (0) si y solo si el crédito no es reestructurado ("Tipo de Alta del Crédito" distinto de 3 en columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, FechaVencimientoReestructura, TipoAlta
FROM dbo.RW_R04H0491
WHERE FechaVencimientoReestructura = '0' AND TipoAlta = '3';

END
GO
