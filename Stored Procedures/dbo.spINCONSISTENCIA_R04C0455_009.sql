SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_009]
AS
BEGIN
-- Si el Saldo de las Participaciones Elegibles (dat_saldo_particip_elegibles) es > 70 y <= 100
-- entonces el Puntaje Deuda Total a Participaciones Elegibles (cve_puntaje_deuda_particip) debe ser = 113

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoPartEleg,
	P_DeudaTotalPartEleg AS Puntos_SdoPartEleg
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoPartEleg,''),'0') AS DECIMAL(18,6)) BETWEEN 70.000001 AND 100 AND ISNULL(P_DeudaTotalPartEleg,'') <> '113';

END
GO
