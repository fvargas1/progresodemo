SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_107_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Presencia de Quitas, castigos y reestructuras con instituciones bancarias sin atraso
-- (cve_puntaje_quitas_castig_bancos) es = 55, entonces la Presencia de Quitas, castigos y reestructuras con instituciones bancarias es = 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC rep
INNER JOIN dbo.SICCMX_Persona per ON rep.CodigoPersona = per.Codigo
INNER JOIN dbo.SICCMX_Persona_PI_Detalles piDet ON per.IdPersona = piDet.IdPersona
INNER JOIN dbo.SICCMX_PI_Variables piVar ON piDet.IdVariable = piVar.Id
WHERE piVar.Codigo='21SA_QUIT_CAST_REEST' AND ISNULL(rep.P_QuitCastReest,'') = '55' AND CAST(piDet.ValorActual AS DECIMAL) <> 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
