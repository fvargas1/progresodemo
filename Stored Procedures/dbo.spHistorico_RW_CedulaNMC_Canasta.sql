SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_CedulaNMC_Canasta]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.RW_CedulaNMC_Canasta WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_CedulaNMC_Canasta (
 IdPeriodoHistorico, CodigoPersona, NumeroLinea, CodigoCredito, TipoGarantia, PrctSinAjust, PrctAjust, [PI], SP, MontoRecuperacion,
 Reserva, Posicion
)
SELECT
 @IdPeriodoHistorico,
 p.Codigo,
 rw.NumeroLinea,
 rw.CodigoCredito,
 rw.TipoGarantia,
 rw.PrctSinAjust,
 rw.PrctAjust,
 rw.[PI],
 rw.SP,
 rw.MontoRecuperacion,
 rw.Reserva,
 rw.Posicion
FROM dbo.RW_CedulaNMC_Canasta rw
INNER JOIN dbo.SICCMX_Persona p ON rw.IdPersona = p.IdPersona;
GO
