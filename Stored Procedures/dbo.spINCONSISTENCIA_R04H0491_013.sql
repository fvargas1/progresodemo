SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_013]
AS

BEGIN

-- El "TIPO DE ALTA DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

SELECT r.CodigoCredito, r.CodigoCreditoCNBV, r.TipoAlta
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICC_TipoAltaHipotecario cat ON ISNULL(r.TipoAlta,'') = cat.CodigoCNBV
WHERE cat.IdTipoAltaHipotecario IS NULL;

END
GO
