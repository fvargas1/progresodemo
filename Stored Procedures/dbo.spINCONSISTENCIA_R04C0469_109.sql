SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_109]
AS

BEGIN

-- Si el Número de Días Vencidos (dat_num_dias_vencidos) es MAYOR a 90, entonces la Situación del Crédito (cve_situacion_credito) es IGUAL a 2.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	DiasAtraso,
	SituacionCredito
FROM dbo.RW_VW_R04C0469_INC
WHERE CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) > 90 AND ISNULL(SituacionCredito,'') <> '2';

END
GO
