SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Get_PersonasProyectos]
	@keywords VARCHAR(100)
AS
SELECT * FROM (
SELECT DISTINCT
	per.IdPersona AS Id,
	per.Codigo,
	per.Nombre AS DESCRIPTION,
	cal.Codigo AS Calificacion,
	cal.Color AS Color
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Proyecto pry ON per.IdPersona = pry.IdPersona
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON pc.IdCalificacionFinal = cal.IdCalificacion
WHERE per.Codigo + '|' + per.Nombre LIKE '%'+@keywords+'%'
) AS tb
ORDER BY tb.DESCRIPTION
GO
