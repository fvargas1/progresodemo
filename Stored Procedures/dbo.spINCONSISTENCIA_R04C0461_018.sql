SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_018]
AS
BEGIN
-- La probabilidad de incumplimiento debe encontrarse entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	[PI]
FROM dbo.RW_VW_R04C0461_INC
WHERE CAST(ISNULL(NULLIF([PI],''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
