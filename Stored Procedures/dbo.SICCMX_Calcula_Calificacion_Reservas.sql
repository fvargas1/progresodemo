SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Calificacion_Reservas]
AS
UPDATE crv
SET
	CalifFinal = calFinal.IdCalificacion,
	CalifCubierta = calCub.IdCalificacion,
	CalifExpuesta = calExp.IdCalificacion
FROM dbo.SICCMX_Credito_Reservas_Variables crv
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calFinal ON crv.PorReservaFinal BETWEEN calFinal.RangoMenor AND calFinal.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calExp ON crv.PorExpuesta BETWEEN calExp.RangoMenor AND calExp.RangoMayor
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calCub ON crv.PorCubierto BETWEEN calCub.RangoMenor AND calCub.RangoMayor;
GO
