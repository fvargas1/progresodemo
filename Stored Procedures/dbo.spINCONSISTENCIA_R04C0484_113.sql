SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_113]
AS
BEGIN
-- Si la Fecha de Otorgamiento contenida en el ID metodología CNBV (dat_id_credito_met_cnbv) posiciones 8 a la 13
-- es igual al periodo que se reporta (cve_periodo), validar que el Monto Dispuesto de la Línea de Crédito en el
-- Mes (dat_monto_credito_dispuesto) sea DIFERENTE de cero.

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	@FechaPeriodo AS FechaPeriodo,
	MontoDispuesto
FROM dbo.RW_VW_R04C0484_INC
WHERE SUBSTRING(ISNULL(CodigoCreditoCNBV,''),8,6) = @FechaPeriodo AND ISNULL(NULLIF(MontoDispuesto,''),'0') = '0';

END
GO
