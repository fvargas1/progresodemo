SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_ProcesoCalificacion_SelectByGrupo]
	@IdGrupoProceso INT
AS

SET @IdGrupoProceso = NULLIF(@IdGrupoProceso, 0);

SELECT
	vw.IdProcesoCalificacion,
	vw.Nombre,
	vw.Descripcion,
	vw.Codename,
	vw.IdMetodologia,
	vw.MetodologiaNombre,
	vw.Position,
	vw.Activo,
	vw.IdGrupoProceso,
	vw.GrupoProcesoNombre,
	vw.Params
FROM dbo.SICC_VW_ProcesoCalificacion vw
WHERE @IdGrupoProceso IS NULL OR vw.IdGrupoProceso = @IdGrupoProceso
ORDER BY vw.Position;
GO
