SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spReporte_ConsumoAnexo31]
AS
SELECT
	Calificacion AS Codigo,
	Importe,
	Reserva,
	PorcentajeReservas,
	Metodologia AS Nombre,
	ImporteSemanal,
	ImporteQuincenal,
	ReservaSemanal,
	ReservaQuincenal,
	TipoCredito
FROM dbo.RW_Anexo31Consumo2011;
GO
