SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_MON_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Hipotecario_MON_cat';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Hipotecario
SET errorCatalogo = 1
WHERE LEN(MON)>0 AND LTRIM(MON) NOT IN ('0','1');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Hipotecario_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoCredito,
 'MON',
 MON,
 2,
 @Detalle
FROM dbo.FILE_Hipotecario
WHERE LEN(MON)>0 AND LTRIM(MON) NOT IN ('0','1');
GO
