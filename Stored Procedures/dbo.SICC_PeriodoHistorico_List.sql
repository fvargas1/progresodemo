SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_PeriodoHistorico_List]
AS
SELECT
	vw.IdPeriodoHistorico,
	CONVERT( VARCHAR( 50 ), vw.Fecha, 105 ) AS Fecha,
	vw.Version,
	vw.FechaCreacion,
	vw.Username,
	CONVERT(VARCHAR(10),vw.Fecha,120) + ' - ' + vw.Version AS Descripcion
FROM dbo.SICC_VW_PeriodoHistorico vw
ORDER BY vw.FechaCreacion DESC;
GO
