SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_043_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si los Días Atrasados Infonavit en el Último Bimestre (dat_dias_mora_infonavit) es > 0 y < 28.33,
-- entonces el Puntaje Asignado por Días Atrasados Infonavit en el Último Bimestre (cve_ptaje_dias_atraso_infonav) debe ser = 37

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(DiasAtrInfonavit AS DECIMAL(10,6)) > 0 AND CAST(DiasAtrInfonavit AS DECIMAL(10,6)) < 28.33 AND ISNULL(P_DiasAtrInfonavit,'') <> '37';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
