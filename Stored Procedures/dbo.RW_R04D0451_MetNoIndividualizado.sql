SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_MetNoIndividualizado]
AS
SELECT
	vw.Nombre AS Calificacion,
	SUM(CASE WHEN rep.TipoSaldo = '1' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS SaldoTotalCierre,
	SUM(CASE WHEN rep.TipoSaldo = '19' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS SaltoTotalCierreHistorico,
	SUM(CASE WHEN rep.TipoSaldo = '115' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS Variacion,
	SUM(CASE WHEN rep.TipoSaldo = '116' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS MigracionMejoras,
	SUM(CASE WHEN rep.TipoSaldo = '117' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS MigracionDescensos,
	SUM(CASE WHEN rep.TipoSaldo = '118' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS NuevasCalificaciones,
	SUM(CASE WHEN rep.TipoSaldo = '91' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS CreditosLiquidados
FROM dbo.ReportWare_VW_R04D0451Concepto vw
LEFT OUTER JOIN dbo.RW_R04D0451 rep ON vw.Codigo = rep.Concepto AND rep.MetodoCalificacion = 2 --metodo no individualizado
AND TipoCalificacion IN ('2')
WHERE vw.Nombre IN ('A1','A2','B1','B2','B3','C1','C2','D','E')
GROUP BY vw.Nombre, TipoCalificacion
ORDER BY VW.Nombre;
GO
