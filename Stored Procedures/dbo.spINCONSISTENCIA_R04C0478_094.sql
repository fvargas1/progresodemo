SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_094]
AS

BEGIN

-- Validar que si el campo no es nulo, entonces el ID de Crédito Línea Grupal o Multimoneda asignado metodología
-- CNBV (dat_id_credito_grupal_met_cnbv) debe ser de 29 posiciones y para su llenado deberá utilizarse la metodología
-- de ID de Crédito Metodología CNBV.

SELECT
	CodigoCredito,
	CodigoGlobalCNBV
FROM dbo.RW_VW_R04C0478_INC
WHERE LEN(ISNULL(CodigoGlobalCNBV,'')) > 0 AND LEN(ISNULL(CodigoGlobalCNBV,'')) <> 29;

END


GO
