SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_012]
AS

BEGIN

-- El sector económico del acreditado debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.SectorEconomico
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor cat ON ISNULL(r.SectorEconomico,'') = cat.Codigo
WHERE cat.IdSectorEconomico IS NULL;

END
GO
