SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_ReporteInterno_List]
AS
SELECT
 vw.IdReporteInterno,
 vw.GrupoReporte,
 vw.Nombre,
 vw.Descripcion,
 vw.SProc,
 vw.Activo
FROM dbo.RW_VW_ReporteInterno vw
WHERE Activo=1;
GO
