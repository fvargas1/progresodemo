SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_052_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Validar que ID Acreditado Asignado por la Institución (dat_id_acreditado_instit) se haya reportado previamente en el reporte de altas R04-C 478 (Documento SITI 1855).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC rep
LEFT OUTER JOIN dbo.RW_VW_R04C0478_INC r78 ON rep.CodigoPersona = r78.CodigoPersona
LEFT OUTER JOIN Historico.RW_VW_R04C0478_INC hist ON rep.CodigoPersona = hist.CodigoPersona
WHERE hist.IdPeriodoHistorico IS NULL AND r78.CodigoPersona IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
