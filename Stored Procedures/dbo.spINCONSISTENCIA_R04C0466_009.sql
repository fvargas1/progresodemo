SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_009]
AS

BEGIN

-- El porcentaje no cubierto del crédito deben encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctExpuesto
FROM dbo.RW_R04C0466
WHERE ISNULL(PrctExpuesto,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END

GO
