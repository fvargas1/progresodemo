SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_079_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el ID Acreditado Asigando (dat_id_acreditado_institucion) no tenga más de un RFC (dat_rfc).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_VW_R04C0468_INC rep
	INNER JOIN dbo.RW_VW_R04C0468_INC rep2 ON rep.CodigoPersona = rep2.CodigoPersona AND rep.RFC <> rep2.RFC
	GROUP BY rep.RFC, rep.CodigoPersona
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
