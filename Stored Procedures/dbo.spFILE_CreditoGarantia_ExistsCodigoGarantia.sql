SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_CreditoGarantia_ExistsCodigoGarantia]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_CreditoGarantia_ExistsCodigoGarantia';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_CreditoGarantia
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoGarantia, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_CreditoGarantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoGarantia,
 'CodigoGarantia',
 CodigoGarantia,
 1,
 @Detalle
FROM dbo.FILE_CreditoGarantia
WHERE LEN(ISNULL(CodigoGarantia, '')) = 0;
GO
