SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Anexo22_errores_ListErrorLog]
AS
SELECT
f.identificador,
f.nombreCampo,
f.valor,
f.tipoError,
f.description
FROM dbo.FILE_Anexo22_errores f
GO
