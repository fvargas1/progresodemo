SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_PersonaInfo]
 @IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_PersonaInfo WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_PersonaInfo (
 IdPeriodoHistorico,
 Codigo,
 TipoPersonaMA,
 TipoPersona,
 NumeroEmpleados,
 IngresosBrutos,
 SectorLaboral,
 GrupoRiesgo,
 TipoIngreso,
 NombreCNBV,
 ReservasAdicOrdenadasCNBV,
 Estado,
 Municipio,
 Nacionalidad,
 CURP,
 LEI,
 HITSIC,
 PadreGrupoEconomico,
 SectorEconomico,
 TamanoDeudor,
 RCGrupoRiesgo,
 RCCalificacion,
 TopeTamano,
 CalificacionLargoPlazo,
 AgenciaLargoPlazo,
 CalificacionCortoPlazo,
 AgenciaCortoPlazo,
 RCCalificacionCortoPlazo,
 CodigoPostal
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 per.Codigo,--info.IdPersona,
 tpoPerMA.Codigo AS TipoPersonaMA, --info.IdTipoPersonaMA,
 tpoPer.Codigo AS TipoPersona, --info.IdTipoPersona,
 info.NumeroEmpleados,
 info.IngresosBrutos,
 lab.Codigo AS SectorLaboral, --info.IdSectorLaboral,
 info.GrupoRiesgo,
 tpoIng.Codigo AS TipoIngreso, --info.IdTipoIngreso,
 info.NombreCNBV,
 info.ReservasAdicOrdenadasCNBV,
 info.Estado,
 info.Municipio,
 pais.Codigo,
 info.CURP,
 info.LEI,
 htc.Codigo, --info.HITSIC,
 info.PadreGrupoEconomico,
 sect.Codigo,
 tam.Codigo,
 gpo.Codigo,
 cal.Codigo,
 info.TopeTamano,
 info.CalificacionLargoPlazo,
 agLP.Codigo,
 info.CalificacionCortoPlazo,
 agCP.Codigo,
 calCP.Codigo,
 info.CodigoPostal
FROM dbo.SICCMX_PersonaInfo info
INNER JOIN dbo.SICCMX_Persona per ON info.IdPersona = per.IdPersona
LEFT OUTER JOIN dbo.SICC_TipoPersonaMA tpoPerMA ON info.IdTipoPersonaMA = tpoPerMA.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON info.IdTipoPersona = tpoPer.IdTipoPersona
LEFT OUTER JOIN dbo.SICC_SectorLaboral lab ON info.IdSectorLaboral = lab.IdSectorLaboral
LEFT OUTER JOIN dbo.SICC_TipoIngreso tpoIng ON info.IdTipoIngreso = tpoIng.IdTipoIngreso
LEFT OUTER JOIN dbo.SICC_Pais pais ON info.IdNacionalidad = pais.IdPais
LEFT OUTER JOIN dbo.SICC_HitSic htc ON info.HITSIC = htc.IdHitSic
LEFT OUTER JOIN dbo.SICC_SectorEconomicoDeudor sect ON info.IdSectorEconomico = sect.IdSectorEconomico
LEFT OUTER JOIN dbo.SICC_TamanoDeudor tam ON info.IdTamanoDeudor = tam.IdTamanoDeudor
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgo gpo ON info.IdRCGrupoRiesgo = gpo.IdRCGrupoRiesgo
LEFT OUTER JOIN dbo.SICC_RCCalificacion cal ON info.IdRCCalificacion = cal.IdRCCalificacion
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agLP ON info.IdAgenciaLargoPlazo = agLP.IdAgencia
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agCP ON info.IdAgenciaCortoPlazo = agCP.IdAgencia
LEFT OUTER JOIN dbo.SICC_RCCalificacion calCP ON info.IdRCCalificacionCortoPlazo = calCP.IdRCCalificacion;
GO
