SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_047]
AS
BEGIN
-- Validar que el Monto de Intereses Pagados Efectivamente por el Acreditado en el Periodo (dat_monto_intereses_pagados) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoInteresPagado
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(MontoInteresPagado,''),'-1') AS DECIMAL) < 0;

END


GO
