SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_HipotecarioGarantia_Garantia_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_HipotecarioGarantia_Garantia_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_HipotecarioGarantia f
LEFT OUTER JOIN dbo.SICCMX_Garantia_Hipotecario gar ON LTRIM(f.CodigoGarantia) = gar.Codigo
WHERE gar.Codigo IS NULL;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_HipotecarioGarantia_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoGarantia,
 'CodigoGarantia',
 f.CodigoGarantia,
 2,
 @Detalle
FROM dbo.FILE_HipotecarioGarantia f
LEFT OUTER JOIN dbo.SICCMX_Garantia_Hipotecario gar ON LTRIM(f.CodigoGarantia) = gar.Codigo
WHERE gar.Codigo IS NULL;
GO
