SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_079]
AS
BEGIN
-- Validar que si el Monto Fondeado por Banco de Desarrollo o Fondo de Fomento (dat_monto_fondea_b_desarrollo)
-- es DIFERENTE de cero, entonces la columna de Institución Banca de Desarrollo o Fondo de Fomento que otorgó
-- el Fondeo (cve_instituciones) debe ser DIFERENTE de cero.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	MontoBancaDesarrollo,
	InstitucionFondeo
FROM dbo.RW_VW_R04C0484_INC
WHERE CAST(MontoBancaDesarrollo AS DECIMAL) > 0 AND InstitucionFondeo = '0';

END
GO
