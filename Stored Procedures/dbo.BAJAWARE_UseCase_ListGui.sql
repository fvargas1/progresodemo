SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_UseCase_ListGui]
AS
SELECT
	uc.IdUseCase,
	uc.Active,
	uc.Description, 
	uc.Name,
	uc.CodeName,
	uc.Visible,
	uc.PageLink,
	ucg.Name AS IdUseCaseGroup,
	uc.SortOrder,
	uc.StringKey
FROM dbo.BAJAWARE_UseCase uc
LEFT OUTER JOIN dbo.BAJAWARE_UseCaseGroup ucg ON uc.IdUseCaseGroup = ucg.IdUseCaseGroup;
GO
