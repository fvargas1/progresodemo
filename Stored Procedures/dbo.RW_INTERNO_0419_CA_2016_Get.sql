SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INTERNO_0419_CA_2016_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Codigo,
	Producto,
	ReservaInicial,
	Cargos,
	Abonos,
	ReservaActual,
	ReservaActualCalif,
	Diferencia
FROM dbo.RW_VW_INTERNO_0419_CA_2016
ORDER BY Codigo;
GO
