SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_CreditoGarantia_Portafolio]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_CreditoGarantia_Portafolio WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_CreditoGarantia_Portafolio (
	IdPeriodoHistorico,
	CodigoPortafolio,
	CodigoCredito,
	CodigoGarantia
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	CodigoPortafolio,
	CodigoCredito,
	CodigoGarantia
FROM dbo.SICCMX_CreditoGarantia_Portafolio;
GO
