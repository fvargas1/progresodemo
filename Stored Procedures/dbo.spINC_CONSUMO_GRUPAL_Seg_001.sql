SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_GRUPAL_Seg_001]
AS

BEGIN

-- El Tipo de Reestructura de ser 50 en el caso de créditos originales (que no provienen de una reestructura);
-- y 400 = en el caso de créditos Grupales que ya fueron objeto de una reestructura y continúan siendo Grupales.

SELECT DISTINCT
	rep.FolioCredito,
	rep.Reestructura
FROM dbo.RW_Consumo_GRUPAL rep
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo cat ON rep.Reestructura = cat.CodigoBanxico AND ISNULL(cat.ClasificacionReporte,'GRUPAL') = 'GRUPAL'
WHERE cat.IdReestructuraConsumo IS NULL;

END
GO
