SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_034]
AS

BEGIN

-- El tipo de baja del crédito deberá existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.TipoBaja
FROM dbo.RW_R04C0443 r
LEFT OUTER JOIN dbo.SICC_TipoBajaMA cat ON ISNULL(r.TipoBaja,'') = cat.CodigoCNBV
WHERE LEN(r.TipoBaja) > 0 AND cat.IdTipoBaja IS NULL;

END
GO
