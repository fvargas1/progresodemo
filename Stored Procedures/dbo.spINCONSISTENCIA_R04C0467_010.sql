SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0467_010]
AS

BEGIN

-- Validar que el Monto Total Pagado Efectivamente por el Acreditado en el Periodo (dat_monto_pagado_total) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoPagado
FROM dbo.RW_VW_R04C0467_INC
WHERE CAST(ISNULL(NULLIF(MontoPagado,''),'-1') AS DECIMAL) < 0;

END

GO
