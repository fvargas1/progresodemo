SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Config_Ins]
	@Name BAJAWARE_utDescripcion50,
	@Description BAJAWARE_utDescripcion100,
	@CodeName BAJAWARE_utDescripcion50,
	@Value BAJAWARE_utDescripcion250
AS
INSERT INTO dbo.BAJAWARE_Config ([Name], Description, CodeName, Value)
VALUES (@Name, @Description, @CodeName, @Value);

SELECT IdConfig, [Name], Description, CodeName, Value
FROM dbo.BAJAWARE_Config
WHERE IdConfig=SCOPE_IDENTITY();
GO
