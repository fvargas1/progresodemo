SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Cedula_List]
AS
SELECT
 vw.IdCedula,
 vw.Nombre,
 vw.Codename,
 vw.ReportFolder,
 vw.Reportname,
 vw.Parameters,
 vw.OutputParams,
 vw.Prefix,
 vw.Activo
FROM dbo.RW_VW_Cedula vw
WHERE vw.Activo=1
ORDER BY vw.Nombre;
GO
