SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_048_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Puntaje Crediticio Cuantitativo (dat_ptaje_credit_cuantit) sea >= 497 pero <=1029

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PuntajeCuantitativo AS DECIMAL) NOT BETWEEN 497 AND 1029;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
