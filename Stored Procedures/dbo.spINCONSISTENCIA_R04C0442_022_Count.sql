SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_022_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El ID met CNBV deberá ser único e irrepetible para cada crédito de la entidad.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCreditoCNBV)
FROM (
	SELECT CodigoCreditoCNBV
	FROM dbo.RW_R04C0442
	GROUP BY CodigoCreditoCNBV
	HAVING COUNT(CodigoCreditoCNBV) > 1
) AS tb;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
