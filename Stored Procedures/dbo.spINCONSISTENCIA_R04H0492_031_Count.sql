SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_031_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El dato "CONVENIO JUDICIAL O FIDEICOMISO DE GARANTÍA" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0492 r
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial cat ON ISNULL(r.ConvenioJudicial,'') = cat.CodigoCNBV
WHERE cat.IdConvenioJudicial IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
