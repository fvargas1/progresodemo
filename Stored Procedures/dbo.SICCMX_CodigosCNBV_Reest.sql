SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CodigosCNBV_Reest]
AS
DECLARE @IdPeriodo INT;
DECLARE @IdTipoBaja INT;
DECLARE @IdTipoBajaCons INT;
SELECT @IdPeriodo = IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdTipoBaja = IdTipoBaja FROM dbo.SICC_TipoBaja WHERE Codigo = '133';
SELECT @IdTipoBajaCons = IdTipoBaja FROM dbo.SICC_TipoBaja WHERE Codigo = '142';

INSERT INTO dbo.SICCMX_CreditoCNBV (
 NumeroLinea,
 CodigoCredito,
 CodigoPersona,
 CNBV,
 [_Consecutivo],
 FechaCreacion,
 IdPeriodo,
 MontoLinea,
 IdMoneda,
 IdTipoProducto,
 IdTipoOperacion,
 IdTasaReferencia,
 FechaVencimiento
)
SELECT
 lin.Codigo,
 cre.Codigo,
 per.Codigo,
 cnbv.CNBV,
 cnbv.[_Consecutivo],
 GETDATE(),
 @IdPeriodo,
 lin.MontoLinea,
 lin.IdMoneda,
 lin.ProductoComercial AS IdTipoProducto,
 lin.TipoOperacion AS IdTipoOperacion,
 lin.IdTasaReferencia,
 lin.FecVenLinea AS FechaVencimiento
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona per ON lin.IdPersona = per.IdPersona
INNER JOIN dbo.SICC_TipoAlta ta ON lin.IdTipoAlta = ta.IdTipoAlta
INNER JOIN dbo.SICCMX_LineaCredito linReest ON lin.Codigo = linReest.CodigoCreditoReestructurado
INNER JOIN dbo.SICCMX_CreditoCNBV cnbv ON linReest.Codigo = cnbv.NumeroLinea
LEFT OUTER JOIN dbo.SICCMX_CreditoCNBV cnbv2 ON lin.Codigo = cnbv2.NumeroLinea
WHERE ta.GeneraCNBV = 2 AND linReest.IdTipoBaja IN (@IdTipoBaja, @IdTipoBajaCons) AND cnbv2.NumeroLinea IS NULL;
GO
