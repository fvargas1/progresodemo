SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_004_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- El RFC solo debe contener letras en mayúsculas y números, sin caracteres distintos a estos y sin espacios.



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(CodigoCredito)

FROM dbo.RW_R04C0442

WHERE RFC LIKE '%[^A-Z0-9_]%'  AND RFC NOT LIKE '%&%'  OR BINARY_CHECKSUM(RFC) <> BINARY_CHECKSUM(UPPER(RFC));



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
