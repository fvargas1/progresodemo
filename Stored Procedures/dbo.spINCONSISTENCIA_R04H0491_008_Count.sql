SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_008_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Para créditos reestructurados se validará que el "IDENTIFICADOR DEL CRÉDITO METODOLOGÍA CNBV"
-- se haya dado de alta previamente a su registro como crédito reestructurado.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0491 r
LEFT OUTER JOIN dbo.SICCMX_VW_Datos_Altas_SerieH vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV AND vw.TipoRegistro = 'H'
WHERE r.TipoAlta='3' AND vw.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
