SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_037]
AS

BEGIN

-- Para créditos nuevos y cartas de crédito la clave de la institución dentro del ID MET CNBV debe ser igual a la clave de institución que reporta

DECLARE @IdInstitucion INT;
SELECT @IdInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	@IdInstitucion AS CodigoInstitucion,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0473_INC
WHERE SUBSTRING(CodigoCreditoCNBV,2,6) <> @IdInstitucion;

END


GO
