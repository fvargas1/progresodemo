SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0453_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Si el RFC (dat_rfc) inicia con CNBF,  se valida que sí corresponda a un Fideicomiso y haya sido registrado en CNBV.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0453_INC
WHERE ISNULL(RFC,'') LIKE 'CNBF%' AND ISNULL(NombrePersona,'') NOT LIKE 'FIDEICOMISO%';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
