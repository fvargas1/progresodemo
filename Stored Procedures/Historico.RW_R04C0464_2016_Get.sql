SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0464_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NombreFactorado,
	RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	MontoBancaDesarrollo,
	InstitucionFondeo
FROM Historico.RW_R04C0464_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO
