SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_057]
AS
BEGIN
-- Si el crédito está fuera de balance (tipo de alta del crédito entre 700 y 750), entonces la Tasa de Referencia (cve_tasa) debe ser igual a 0.

SELECT
	CodigoCredito,
	TipoAltaCredito,
	TasaInteres AS TasaReferencia
FROM dbo.RW_VW_R04C0483_INC
WHERE CAST(ISNULL(TipoAltaCredito,'0') AS INT) BETWEEN 700 AND 750 AND ISNULL(TasaInteres,'') <> '0'

END
GO
