SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_005_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Número de Días de Mora Promedio con Instituciones Bancarias en los Últimos meses (dat_dias_mora_prom_bcos)
-- es >= 21.18, entonces el Puntaje Asignado por los Días de Mora Promedio con Instituciones Bancarias en los Últimos
-- 12 meses (cve_ptaje_dias_mora_bcos) debe ser = -40

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(DiasMoraInstBanc AS DECIMAL(10,6)) >= 21.18 AND ISNULL(P_DiasMoraInstBanc,'') <> '-40';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
