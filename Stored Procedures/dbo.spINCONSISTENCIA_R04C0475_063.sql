SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_063]
AS

BEGIN

-- Si el Puntaje Asignado por Antigüedad en SIC (cve_ptaje_antig_sic) es = 35, entonces la Antigüedad en SIC (dat_antig_sic) debe ser < 20

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	AntSocCred,
	P_AntSocCred AS Puntos_AntSocCred
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_AntSocCred,'') = '35' AND CAST(AntSocCred AS DECIMAL) >= 20;

END


GO
