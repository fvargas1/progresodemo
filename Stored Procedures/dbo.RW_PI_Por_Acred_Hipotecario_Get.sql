SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_PI_Por_Acred_Hipotecario_Get]
	@IdPersona BIGINT
AS
SELECT tmp.FechaPeriodo, tmp.[PI], tmp.EI, tmp.Reserva
FROM (
	SELECT TOP 6
	SUBSTRING(REPLACE(CONVERT(VARCHAR,FechaPeriodo,102),'.','-'),1,7) AS FechaPeriodo,
	CAST([PI] * 100 AS DECIMAL(5,2)) AS [PI],
	EI,
	Reserva
	FROM dbo.RW_Totales_Por_Periodo_Hipotecario
	WHERE IdPersona = @IdPersona
	ORDER BY FechaPeriodo DESC
) tmp
ORDER BY tmp.FechaPeriodo;
GO
