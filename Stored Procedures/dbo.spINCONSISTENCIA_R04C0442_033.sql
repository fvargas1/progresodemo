SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_033]
AS

BEGIN

-- Si el crédito no es a tasa fija entonces se deberá indicar la frecuencia de revisión de la tasa.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, TasaRef, FrecuenciaRevisionTasa
FROM dbo.RW_R04C0442
WHERE TasaRef <> '600' AND FrecuenciaRevisionTasa = '0';

END
GO
