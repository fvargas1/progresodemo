SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_004_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El Número de Avalúo debe corresponder con el dato reportado en la columna 23 del formulario H-0491 Altas y reestructuras de créditos a la vivienda.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0493 r
INNER JOIN dbo.SICCMX_VW_Datos_Altas_SerieH vw ON r.CodigoCreditoCNBV = vw.CodigoCreditoCNBV
WHERE r.NumeroAvaluo <> vw.NumeroAvaluo;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
