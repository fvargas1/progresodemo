SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_026]
AS

BEGIN

-- La fecha de otorgamiento dentro del ID MET CNBV no puede ser mayor al periodo que se reporta

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	@FechaPeriodo AS FechaPeriodo,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0473_INC
WHERE SUBSTRING(CodigoCreditoCNBV,8,6) > @FechaPeriodo;

END


GO
