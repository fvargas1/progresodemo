SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_020]
AS
BEGIN
-- El porcentaje de cobertura de la garantía real financiera debe estar entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PrctGarRealFin
FROM dbo.RW_VW_R04C0461_INC
WHERE CAST(ISNULL(NULLIF(PrctGarRealFin,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
