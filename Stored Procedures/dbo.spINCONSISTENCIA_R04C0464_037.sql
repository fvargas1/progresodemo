SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_037]
AS

BEGIN

-- Para créditos dispuestos en el mes, el Monto Dispuesto de la Línea de Crédito deberá ser mayor a 0

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaDisposicion,
	MontoDispuesto,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(FechaDisposicion,'') = @FechaPeriodo AND CAST(ISNULL(NULLIF(MontoDispuesto,''),'-1') AS DECIMAL) < 0;

END

GO
