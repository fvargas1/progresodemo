SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_095]
AS

BEGIN

-- Validar que el Puntaje Crediticio Cualitativo (dat_ptaje_credit_cualit) sea = 0

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PuntajeCualitativo
FROM dbo.RW_VW_R04C0465_INC
WHERE CAST(ISNULL(NULLIF(PuntajeCualitativo,''),'-1') AS DECIMAL) <> 0;

END

GO
