SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_056_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que ID Metodología CNBV (dat_id_credito_met_cnbv) se haya reportado previamente en el reporte de altas R04-C 463

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC rep
LEFT OUTER JOIN dbo.RW_R04C0463 alt ON ISNULL(rep.CodigoCreditoCNBV,'') = ISNULL(alt.CodigoCreditoCNBV,'')
LEFT OUTER JOIN Historico.RW_R04C0463 hst ON ISNULL(rep.CodigoCreditoCNBV,'') = hst.CodigoCreditoCNBV
WHERE hst.IdPeriodoHistorico IS NULL AND alt.CodigoCreditoCNBV IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
