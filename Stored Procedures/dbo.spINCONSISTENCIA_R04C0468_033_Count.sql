SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_033_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Cartera (cve_tipo_cartera) es DIFERENTE de 299, 410 ó 420, la Nacionalidad del Acreditado (cve_pais) debe ser igual a 484.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('299','410','420') AND ISNULL(Nacionalidad,'') <> '484';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
