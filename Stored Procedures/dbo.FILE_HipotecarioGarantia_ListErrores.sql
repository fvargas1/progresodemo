SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_HipotecarioGarantia_ListErrores]
AS
SELECT
 CodigoGarantia,
 CodigoCredito,
 Fuente
FROM dbo.FILE_HipotecarioGarantia
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
