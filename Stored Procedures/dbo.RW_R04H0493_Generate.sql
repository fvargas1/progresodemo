SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04H0493_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = 'H-0493';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04H0493;

INSERT INTO dbo.RW_R04H0493 (
	IdReporteLog, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoCredito, CodigoCreditoCNBV, NumeroAvaluo, TipoBaja, SaldoPrincipalInicial,
	MontoTotalLiquidacion, MontoPagoLiquidacion, MontoBonificaciones, ValorBienAdjudicado
)
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'493',
	ROW_NUMBER() OVER ( ORDER BY hip.Codigo ASC ),
	hip.Codigo AS CodigoCredito,
	cnbv.CNBV AS CodigoCreditoCNBV,
	REPLACE(info.NumeroAvaluo, '-', '') AS NumeroAvaluo,
	tpoBaja.CodigoCNBV AS TipoBaja,
	CAST(ISNULL(info.SaldoInicialCapital,0) AS DECIMAL) AS SaldoPrincipalInicial,
	CAST(ISNULL(info.ResponsabilidadTotal,0) AS DECIMAL) AS MontoTotalLiquidacion,
	CAST(ISNULL(info.MontoPagadoCliente,0) AS DECIMAL) AS MontoPagoLiquidacion,
	CAST(ISNULL(info.MontoBonificacion,0) AS DECIMAL) AS MontoBonificaciones,
	CAST(ISNULL(info.ValorBienAdjudicado,0) AS DECIMAL) AS ValorBienAdjudicado
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_VW_HipotecarioCNBV cnbv ON hip.IdHipotecario = cnbv.IdHipotecario
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario tpoBaja ON info.IdTipoBaja = tpoBaja.IdTipoBajaHipotecario
WHERE info.IdTipoBaja IS NOT NULL;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04H0493 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
