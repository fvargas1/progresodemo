SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0480]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0480);

DELETE FROM Historico.RW_R04C0480 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0480 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, [PI], PuntajeTotal, PuntajeCuantitativo,
	PuntajeCualitativo, CreditoReportadoSIC, HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, GarantiaLeyFederal,
	CumpleCritContGub, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_NumInstRep, P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit,
	P_TasaRetLab, P_RotActTot, P_RotCapTrabajo, P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep, PorcPagoInstNoBanc,
	PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual,
	VentasNetasTotales, RotActTot, ActivoCirculante, RotCapTrabajo, RendCapROE
)
SELECT 
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	[PI],
	PuntajeTotal,
	PuntajeCuantitativo,
	PuntajeCualitativo,
	CreditoReportadoSIC,
	HITenSIC,
	FechaConsultaSIC,
	FechaInfoFinanciera,
	MesesPI100,
	GarantiaLeyFederal,
	CumpleCritContGub,
	P_DiasMoraInstBanc,
	P_PorcPagoInstBanc,
	P_NumInstRep,
	P_PorcPagoInstNoBanc,
	P_PagosInfonavit,
	P_DiasAtrInfonavit,
	P_TasaRetLab,
	P_RotActTot,
	P_RotCapTrabajo,
	P_RendCapROE,
	DiasMoraInstBanc,
	PorcPagoInstBanc,
	NumInstRep,
	PorcPagoInstNoBanc,
	PagosInfonavit,
	DiasAtrInfonavit,
	NumeroEmpleados,
	TasaRetLab,
	PasivoCirculante,
	UtilidadNeta,
	CapitalContable,
	ActivoTotalAnual,
	VentasNetasTotales,
	RotActTot,
	ActivoCirculante,
	RotCapTrabajo,
	RendCapROE
FROM dbo.RW_R04C0480
WHERE IdReporteLog = @IdReporteLog;
GO
