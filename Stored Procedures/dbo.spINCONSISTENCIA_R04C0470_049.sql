SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_049]
AS

BEGIN

-- Validar que el resultado de la siguiente operación
-- (Ventas Netas Totales Anuales(dat_ventas_tot_anuales) / (Activo Circulante(dat_activo_circulante) - Pasivo Circulante (dat_pasivo_circulante)))
-- sea igual al dato reportado en la Rotación de Capital de Trabajo (dat_rotac_capital_trabajo)

SELECT
 CodigoPersona AS CodigoDeudor,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 VentasNetasTotales,
 ActivoCirculante,
 PasivoCirculante,
 CASE WHEN (CAST(ActivoCirculante AS DECIMAL) - CAST(PasivoCirculante AS DECIMAL)) = 0 THEN -999
 ELSE CAST(CAST(VentasNetasTotales AS DECIMAL) / (CAST(ActivoCirculante AS DECIMAL) - CAST(PasivoCirculante AS DECIMAL)) AS DECIMAL(14,2)) END AS [VentasNetasTotales/(ActivoCirculante-PasivoCirculante)],
 RotCapTrabajo
FROM dbo.RW_VW_R04C0470_INC
WHERE CASE WHEN (CAST(ActivoCirculante AS DECIMAL) - CAST(PasivoCirculante AS DECIMAL)) = 0 THEN -999
ELSE CAST(CAST(VentasNetasTotales AS DECIMAL) / (CAST(ActivoCirculante AS DECIMAL) - CAST(PasivoCirculante AS DECIMAL)) AS DECIMAL(14,2)) END <> CAST(RotCapTrabajo AS DECIMAL(14,2));

END


GO
