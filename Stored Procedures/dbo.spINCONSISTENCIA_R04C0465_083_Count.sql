SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_083_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por el Porcentaje de Pagos en Tiempo con Entidades Financ No Bcarias en los últimos 12 meses (cve_ptaje_porc_pagos_no_bcos) es = 17,
-- entonces el Porcentaje de Pago en Tiempo con Entidades Financieras No Bcarias en los últimos 12 meses (dat_porcent_pgos_no_bcos) debe ser >= 0 y < 81

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoInstNoBanc,'') = '17' AND (CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) < 0 OR CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) >= 81);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
