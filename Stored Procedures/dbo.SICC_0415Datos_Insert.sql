SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0415Datos_Insert]
AS

TRUNCATE TABLE R04.[0415Datos];

INSERT INTO R04.[0415Datos] (Codigo, TipoProducto, Moneda, SituacionCredito, SaldoPromedio, SaldoPromedioUSD, InteresMes, InteresMesUSD, ComisionMes, ComisionMesUSD, Concepto)
SELECT
	'' AS Codigo,
	TipoProducto AS CodigoProducto,
	'MN' AS Moneda,
	SituacionCredito AS SituacionCredito,
	0 AS SaldoPromedio,
	0 AS SaldoPromedioUSD,
	0 AS InteresMes,
	0 AS InteresMesUSD,
	0 AS ComisionMes,
	0 AS ComisionMesUSD,
	Concepto AS Concepto
FROM R04.[0415Configuracion]
UNION
SELECT
	'' AS Codigo,
	TipoProducto AS CodigoProducto,
	'ME' AS Moneda,
	SituacionCredito AS SituacionCredito,
	0 AS SaldoPromedio,
	0 AS SaldoPromedioUSD,
	0 AS InteresMes,
	0 AS InteresMesUSD,
	0 AS ComisionMes,
	0 AS ComisionMesUSD,
	Concepto AS Concepto
FROM R04.[0415Configuracion];

-- COMERCIAL
INSERT R04.[0415Datos] (
 Codigo,
 TipoProducto,
 Moneda,
 SituacionCredito,
 SaldoPromedio,
 SaldoPromedioUSD,
 InteresMes,
 InteresMesUSD,
 ComisionMes,
 ComisionMesUSD
)
SELECT
 cr.CodigoCredito,
 tp.Codigo,
 mon.ClasSerieA,
 sic.Codigo,
 ISNULL(adi.SaldoPromedioValorizado,0),
 ISNULL(adi.SaldoPromedioValorizadoUSD,0),
 ISNULL(adi.InteresDevengadoValorizado,0),
 ISNULL(adi.InteresDevengadoValorizadoUSD,0),
 ISNULL(adi.ComisionValorizado,0),
 ISNULL(adi.ComisionValorizadoUSD,0)
FROM dbo.SICCMX_VW_Credito_NMC cr
INNER JOIN dbo.SICCMX_CreditoInfo info ON cr.IdCredito = info.IdCredito
INNER JOIN dbo.SICCMX_VW_CreditoAdicional adi ON adi.IdCredito = info.IdCredito
INNER JOIN dbo.SICC_Moneda mon ON cr.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = cr.IdSituacionCredito
WHERE cr.Posicion <> '181' AND cr.TipoLinea <> '181'; -- No se consideran Cartas de Crédito


-- CONSUMO
INSERT R04.[0415Datos] (
 Codigo,
 TipoProducto,
 Moneda,
 SituacionCredito,
 SaldoPromedio,
 SaldoPromedioUSD,
 InteresMes,
 InteresMesUSD,
 ComisionMes,
 ComisionMesUSD
)
SELECT
 cr.Codigo,
 tp.Codigo,
 mon.ClasSerieA,
 sic.Codigo,
 ISNULL(cadic.SaldoPromedioValorizado,0),
 ISNULL(cadic.SaldoPromedioValorizadoUSD,0),
 ISNULL(cadic.InteresDevengadoValorizado,0),
 ISNULL(cadic.InteresDevengadoValorizadoUSD,0),
 ISNULL(cadic.ComisionValorizado,0),
 ISNULL(cadic.ComisionValorizadoUSD,0)
FROM dbo.SICCMX_VW_Consumo cr 
INNER JOIN dbo.SICCMX_ConsumoInfo info ON cr.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICCMX_VW_ConsumoAdicional cadic ON cadic.IdConsumo = info.IdConsumo
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = info.IdTipoProductoSerie4
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = cr.IdSituacionCredito;


-- HIPOTECARIO
INSERT R04.[0415Datos] (
 Codigo,
 TipoProducto,
 Moneda,
 SituacionCredito,
 SaldoPromedio,
 SaldoPromedioUSD,
 InteresMes,
 InteresMesUSD,
 ComisionMes,
 ComisionMesUSD
)
SELECT
 cr.Codigo,
 tp.Codigo,
 mon.ClasSerieA,
 sic.Codigo,
 ISNULL(cadic.SaldoPromedioValorizado,0),
 ISNULL(cadic.SaldoPromedioValorizadoUSD,0),
 ISNULL(cadic.InteresesDevengadosValorizado,0),
 ISNULL(cadic.InteresesDevengadosValorizadoUSD,0),
 ISNULL(cadic.ComisionesValorizado,0),
 ISNULL(cadic.ComisionesValorizadoUSD,0)
FROM dbo.SICCMX_VW_Hipotecario cr
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON cr.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICCMX_VW_HipotecarioAdicional cadic ON cadic.IdHipotecario = info.IdHipotecario
INNER JOIN dbo.SICC_Moneda mon ON info.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_TipoProductoSerie4 tp ON tp.IdTipoProducto = cr.IdTipoCreditoR04A
LEFT OUTER JOIN dbo.SICC_SituacionCredito sic ON sic.IdSituacionCredito = info.IdSituacionCredito;
GO
