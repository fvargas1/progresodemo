SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_User_Sel_Username_Password]
	@Username BAJAWARE_utDescripcion100,
	@Password BAJAWARE_utBinary
AS
SELECT
IdUser, Active, Description, Blocked, Name, FechaNacimiento, Email, Telefono, Mobil, NumeroControl, Username, Password, UsuarioWindows, UltimaFechaLogin, NeverLogedIn
FROM dbo.BAJAWARE_User
WHERE Username = @Username AND Password = @Password;
GO
