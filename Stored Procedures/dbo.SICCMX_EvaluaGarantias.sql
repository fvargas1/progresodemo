SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_EvaluaGarantias]
AS
UPDATE dbo.SICCMX_Garantia SET Aplica = 1;
UPDATE dbo.SICCMX_CreditoGarantia SET Aplica = 1;

-- SE ACTUALIZAN GARANTIAS QUE NO APLICAN
UPDATE dbo.SICCMX_Garantia SET Aplica=0 WHERE ISNULL(ValorGarantia, 0)=0;


-- SE ACTUALIZAN GARANTIAS QUE NO APLICAN POR CREDITOS CON SP AL 100%
UPDATE cg
SET Aplica = 0
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Credito cre ON cg.IdCredito = cre.IdCredito
INNER JOIN dbo.SICCMX_Garantia gar ON cg.IdGarantia = gar.IdGarantia
INNER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia AND tg.AplicaSP100 = 0
WHERE cre.MesesIncumplimiento >= 18;
GO
