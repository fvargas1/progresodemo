SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0462_011]
AS
BEGIN
-- Validar que el Saldo del Principal al Inicio del Periodo (dat_saldo_ppal_inicio_period) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoPrincipalInicio
FROM dbo.RW_VW_R04C0462_INC
WHERE CAST(ISNULL(NULLIF(SaldoPrincipalInicio,''),'-1') AS DECIMAL) < 0;

END

GO
