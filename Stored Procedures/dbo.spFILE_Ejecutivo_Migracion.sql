SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Ejecutivo_Migracion]
AS
UPDATE eje
SET Nombre = LTRIM(RTRIM(f.Nombre)),
	Telefono = LTRIM(RTRIM(f.Telefono)),
	Correo = LTRIM(RTRIM(f.Correo))
FROM dbo.SICCMX_Ejecutivo eje
INNER JOIN dbo.FILE_Ejecutivo f ON eje.Codigo = LTRIM(f.CodigoEjecutivo)
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Ejecutivo (
	Codigo,
	Nombre,
	Telefono,
	Correo
)
SELECT
	LTRIM(RTRIM(f.CodigoEjecutivo)),
	LTRIM(RTRIM(f.Nombre)),
	LTRIM(RTRIM(f.Telefono)),
	LTRIM(RTRIM(f.Correo))
FROM dbo.FILE_Ejecutivo f
LEFT OUTER JOIN dbo.SICCMX_Ejecutivo eje ON LTRIM(f.CodigoEjecutivo) = eje.Codigo
WHERE eje.IdEjecutivo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;
GO
