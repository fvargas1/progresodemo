SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_007]
AS

BEGIN

-- Si el Puntaje Asignado por Aportaciones al Infonavit en el último bimestre (cve_ptaje_aport_infonavit) es = 48,
-- entonces las Aportaciones al Infonavit en el último bimestre (dat_aportaciones_infonavit) deben ser = -999 (Sin Informacion)

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PagosInfonavit,
	P_PagosInfonavit AS Puntos_PagosInfonavit
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PagosInfonavit,'') = '48' AND CAST(PagosInfonavit AS DECIMAL) <> -999;

END


GO
