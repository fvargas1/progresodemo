SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_CodigoDeudorRepetido]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_CodigoDeudorRepetido';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Califica f
WHERE f.CodigoDeudor IN (
 SELECT f.CodigoDeudor
 FROM dbo.FILE_Califica f
 GROUP BY f.CodigoDeudor
 HAVING COUNT(f.CodigoDeudor)>1
) AND LEN(f.CodigoDeudor) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.CodigoDeudor,
 'CodigoDeudor',
 f.CodigoDeudor,
 1,
 @Detalle
FROM dbo.FILE_Califica f
WHERE f.CodigoDeudor IN (
 SELECT f.CodigoDeudor
 FROM dbo.FILE_Califica f
 GROUP BY f.CodigoDeudor
 HAVING COUNT(f.CodigoDeudor)>1
) AND LEN(f.CodigoDeudor) > 0;
GO
