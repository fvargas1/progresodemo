SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_076]
AS

BEGIN

-- Si el Puntaje Asignado por Cuentas o Créditos Abiertos con Inst Financ Bcarias en los últimos 12 meses (cve_ptaje_cuentas_con_bcos) es = 50,
-- entonces las Cuentas o Créditos Abiertos con Inst Financ Bcarias en los últimos 12 meses (dat_ctas_abiertas_bancos) debe ser >= 1 y < 4

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	CredAbInstBanc,
	P_CredAbInstBanc AS Puntos_CredAbInstBanc
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_CredAbInstBanc,'') = '50' AND (CAST(CredAbInstBanc AS DECIMAL) < 1 OR CAST(CredAbInstBanc AS DECIMAL) >= 4);

END

GO
