SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_112]
AS

BEGIN

-- Si el Puntaje por Presencia de Quitas, castigos y reestructuras con instituciones bancarias con atraso (cve_puntaje_quitas_castig_bancos) es = 49,
-- entonces la Presencia de Quitas, castigos y reestructuras con instituciones bancarias es = Sin Info

SELECT
	rep.CodigoPersona AS CodigoDeudor,
	REPLACE(rep.NombrePersona, ',', '' ) AS NombreDeudor,
	piDet.ValorActual AS QuitCastReest,
	rep.P_QuitCastReest AS Puntos_QuitCastReest
FROM dbo.RW_VW_R04C0465_INC rep
INNER JOIN dbo.SICCMX_Persona per ON rep.CodigoPersona = per.Codigo
INNER JOIN dbo.SICCMX_Persona_PI_Detalles piDet ON per.IdPersona = piDet.IdPersona
INNER JOIN dbo.SICCMX_PI_Variables piVar ON piDet.IdVariable = piVar.Id
WHERE piVar.Codigo='21CA_QUIT_CAST_REEST' AND ISNULL(rep.P_QuitCastReest,'') = '49' AND piDet.ValorActual IS NOT NULL;

END

GO
