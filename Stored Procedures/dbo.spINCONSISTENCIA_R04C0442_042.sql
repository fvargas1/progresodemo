SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_042]
AS

BEGIN

-- El porcentaje total cubierto por el aval deberá ser mayor o igual a 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, PorcentajeCubiertoAval
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(PorcentajeCubiertoAval,''),'-1') AS DECIMAL(10,2)) < 0;

END
GO
