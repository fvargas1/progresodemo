SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo20_CodigoCliente_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo20_CodigoCliente_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo20 f
LEFT OUTER JOIN dbo.SICCMX_Persona cat ON LTRIM(f.CodigoCliente) = cat.Codigo
WHERE cat.IdPersona IS NULL AND LEN(ISNULL(f.EsGarante,'')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo20_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'CodigoCliente',
 f.CodigoCliente,
 2,
 @Detalle
FROM dbo.FILE_Anexo20 f
LEFT OUTER JOIN dbo.SICCMX_Persona cat ON LTRIM(f.CodigoCliente) = cat.Codigo
WHERE cat.IdPersona IS NULL AND LEN(ISNULL(f.EsGarante,'')) = 0;
GO
