SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_100]
AS
BEGIN
-- La clave de la Institución de Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo (cve_inst_bca_des_fondo) debe ser diferente de la Clave de la Institución (cve_institución).

DECLARE @CodigoInstitucion VARCHAR(10);

SELECT @CodigoInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion'

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	InstitucionFondeo,
	@CodigoInstitucion
FROM dbo.RW_VW_R04C0484_INC
WHERE ISNULL(InstitucionFondeo,'') = @CodigoInstitucion

END
GO
