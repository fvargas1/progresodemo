SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_050]
AS

BEGIN

-- Validar que el Monto de la Comisión por Apertura del Crédito (dat_comisiones_apertura_cred) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	ComisionAperturaMonto
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(ISNULL(NULLIF(ComisionAperturaMonto,''),'-1') AS DECIMAL) < 0;

END


GO
