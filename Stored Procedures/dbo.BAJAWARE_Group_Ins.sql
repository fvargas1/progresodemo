SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Group_Ins]
	@Active BAJAWARE_utActivo,
	@Description BAJAWARE_utDescripcion100,
	@Codename BAJAWARE_utDescripcion50,
	@IdGroupType BAJAWARE_utID
AS
INSERT INTO dbo.BAJAWARE_Group (Active, Description, Codename, IdGroupType)
VALUES (@Active, @Description, @Codename, @IdGroupType);

SELECT IdGroup, Active, Description, Codename, IdGroupType
FROM dbo.BAJAWARE_Group
WHERE IdGroup=SCOPE_IDENTITY();
GO
