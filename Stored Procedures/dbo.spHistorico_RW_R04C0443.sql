SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0443]
 @IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0443);

DELETE FROM Historico.RW_R04C0443 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0443 (
 IdPeriodoHistorico, Periodo, Entidad, Formulario, NumeroSecuencia, CodigoPersona, RFC, NombrePersona, CodigoCredito, CodigoCreditoCNBV,
 CodigoAgrupacion, NumeroDisposicion, FechaDisposicion, CalificacionPersona, CalificacionCreditoCubierta, CalificacionCreditoExpuesta,
 CalificacionPersonaCNBV, CalificacionCreditoCubiertaCNBV, CalificacionCreditoExpuestaCNBV, SituacionCredito, SaldoInicial, TasaInteres,
 MontoDispuesto, MontoExigible, MontoPagado, MontoInteresPagado, MontoComisionDevengada, DiasVencido, SaldoFinal, ResponsabilidadTotal,
 TipoBaja, MontoReconocido, ProductoComercial, PersonalidadJuridica
)
SELECT
 @IdPeriodoHistorico,
 Periodo,
 Entidad,
 Formulario,
 NumeroSecuencia,
 CodigoPersona,
 RFC,
 NombrePersona,
 CodigoCredito,
 CodigoCreditoCNBV,
 CodigoAgrupacion,
 NumeroDisposicion,
 FechaDisposicion,
 CalificacionPersona,
 CalificacionCreditoCubierta,
 CalificacionCreditoExpuesta,
 CalificacionPersonaCNBV,
 CalificacionCreditoCubiertaCNBV,
 CalificacionCreditoExpuestaCNBV,
 SituacionCredito,
 SaldoInicial,
 TasaInteres,
 MontoDispuesto,
 MontoExigible,
 MontoPagado,
 MontoInteresPagado,
 MontoComisionDevengada,
 DiasVencido,
 SaldoFinal,
 ResponsabilidadTotal,
 TipoBaja,
 MontoReconocido,
 ProductoComercial,
 PersonalidadJuridica
FROM dbo.RW_R04C0443
WHERE IdReporteLog = @IdReporteLog;
GO
