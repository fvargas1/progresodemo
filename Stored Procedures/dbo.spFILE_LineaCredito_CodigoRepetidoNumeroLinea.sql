SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_CodigoRepetidoNumeroLinea]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_LineaCredito_CodigoRepetidoNumeroLinea';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_LineaCredito f
WHERE f.NumeroLinea IN (
 SELECT f.NumeroLinea
 FROM dbo.FILE_LineaCredito f
 GROUP BY f.NumeroLinea
 HAVING COUNT(f.NumeroLinea )>1
) AND LEN(f.NumeroLinea) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_LineaCredito_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 f.NumeroLinea,
 'NumeroLinea',
 f.NumeroLinea,
 1,
 @Detalle
FROM dbo.FILE_LineaCredito f
WHERE f.NumeroLinea IN (
 SELECT f.NumeroLinea
 FROM dbo.FILE_LineaCredito f
 GROUP BY f.NumeroLinea
 HAVING COUNT(f.NumeroLinea )>1
) AND LEN(f.NumeroLinea) > 0;
GO
