SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_DesempenoCalidad]
AS
SELECT
	vw.Nombre AS Calificacion,
	SUM(CASE WHEN rep.TipoSaldo = '1' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS SaldoTrimestreActual,
	CASE
	WHEN vw.Nombre ='FINALReservas para creditos vigentes' THEN 0
	ELSE SUM(CASE WHEN rep.TipoSaldo = '19' THEN CONVERT(MONEY, rep.Dato)
	ELSE 0 END ) END AS SaldoTrimestreAnterior,
	CASE
	WHEN vw.Nombre ='FINALReservas para creditos vigentes' THEN 0
	ELSE SUM(CASE WHEN rep.TipoSaldo = '115' THEN CONVERT(MONEY, rep.Dato)
	ELSE 0 END ) END AS Variacion,
	Orden
FROM dbo.ReportWare_VW_R04D0451Concepto vw
LEFT OUTER JOIN dbo.RW_R04D0451 rep ON vw.Codigo = rep.Concepto AND MetodoCalificacion = 3
WHERE vw.Nombre LIKE 'Reservas%' OR vw.Nombre LIKE 'Monto de cr%' OR vw.Nombre LIKE 'Castigos%'
GROUP BY Orden,vw.Nombre, TipoCalificacion;
GO
