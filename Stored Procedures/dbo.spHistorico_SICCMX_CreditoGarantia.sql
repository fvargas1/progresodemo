SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_CreditoGarantia]
	@IdPeriodoHistorico INT
AS
DELETE FROM Historico.SICCMX_CreditoGarantia WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_CreditoGarantia (
	IdPeriodoHistorico,
	Credito,
	Garantia,
	PorcCubiertoCredito,
	MontoCubiertoCredito,
	PorcUsadoGarantia,
	MontoUsadoGarantia,
	Aplica,
	PorcBanco,
	MontoBanco
)
SELECT
	@IdPeriodoHistorico,
	c.Codigo,
	g.Codigo,
	cg.PorcCubiertoCredito,
	cg.MontoCubiertoCredito,
	cg.PorcUsadoGarantia,
	cg.MontoUsadoGarantia,
	cg.Aplica,
	cg.PorcBanco,
	cg.MontoBanco
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_Credito c ON cg.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_Garantia g ON cg.IdGarantia = g.IdGarantia;
GO
