SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_029_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La SP Ajustada Por Fideicomisos De Garantía Y De Admón. Con Part. Fed. Y Aport. Federales Como Fuente De Pago debe estar entre 1 y 100.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0471_INC
WHERE CAST(ISNULL(NULLIF(SP_FidPartFed,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
