SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_069]
AS

BEGIN

-- Si el Puntaje Asignado por Antigüedad en SIC (cve_ptaje_antig_sic) es = 53, entonces la Antigüedad en SIC (dat_antig_sic) debe ser >= 44 y < 120

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	AntSocCred,
	P_AntSocCred AS Puntos_AntSocCred
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_AntSocCred,'') = '53' AND SinAtrasos='0' AND (CAST(AntSocCred AS DECIMAL) < 44 OR CAST(AntSocCred AS DECIMAL) >= 120);

END

GO
