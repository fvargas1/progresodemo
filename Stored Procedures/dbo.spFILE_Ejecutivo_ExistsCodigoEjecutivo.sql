SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Ejecutivo_ExistsCodigoEjecutivo]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Ejecutivo_ExistsCodigoEjecutivo';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Ejecutivo
SET errorFormato = 1
WHERE LEN(ISNULL(CodigoEjecutivo, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Ejecutivo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	CodigoEjecutivo,
	'CodigoEjecutivo',
	CodigoEjecutivo,
	1,
	@Detalle
FROM dbo.FILE_Ejecutivo
WHERE LEN(ISNULL(CodigoEjecutivo, '')) = 0;
GO
