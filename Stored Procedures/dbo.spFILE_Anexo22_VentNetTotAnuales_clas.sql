SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_VentNetTotAnuales_clas]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
DECLARE @UDI DECIMAL(18,10);
DECLARE @Rango DECIMAL(23,2);
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_VentNetTotAnuales_clas';

SELECT @UDI = tc.Valor
FROM dbo.SICC_TipoCambio tc
INNER JOIN dbo.SICC_Moneda mon ON tc.IdMoneda = mon.IdMoneda
INNER JOIN dbo.SICC_Periodo per ON tc.IdPeriodo = per.IdPeriodo AND per.Activo = 1
INNER JOIN dbo.BAJAWARE_Config cfg ON mon.Codigo = cfg.[Value] AND cfg.CodeName = 'MonedaUdis';

SELECT @Rango = [Value] FROM dbo.BAJAWARE_Config WHERE CodeName = 'A22_UDIS_MENOR1';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo22
SET errorFormato = 1
WHERE ISNUMERIC(VentNetTotAnuales) = 1 AND ((CAST(VentNetTotAnuales AS DECIMAL(23,2)) / @UDI) < @Rango);

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'VentNetTotAnuales',
 '[Valor=' + LTRIM(RTRIM(VentNetTotAnuales)) + '][UDI=' + CAST(@UDI AS VARCHAR) + '][ValorEnUDIS=' + CAST(CAST(VentNetTotAnuales AS DECIMAL(23,2)) / @UDI AS VARCHAR) + ']',
 1,
 @Detalle
FROM dbo.FILE_Anexo22
WHERE ISNUMERIC(VentNetTotAnuales) = 1 AND ((CAST(VentNetTotAnuales AS DECIMAL(23,2)) / @UDI) < @Rango);
GO
