SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_099_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el crédito está fuera de balance (tipo de alta del crédito entre 700 y 750), entonces la Tasa de Referencia (cve_tasa) debe ser igual a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(TipoAltaCredito,'0') AS INT) BETWEEN 700 AND 750 AND ISNULL(TasaInteres,'') <> '0'

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
