SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_Reservas_A20_Get]
 @IdPeriodoHistorico BIGINT
AS
SELECT
 Fecha,
 CodigoPersona,
 Nombre,
 CodigoCredito,
 MontoCredito,
 FechaVencimiento,
 [PI],
 Moneda,
 ActividadEconomica,
 MontoGarantia,
 MontoGarantiaAjustado,
 Prct_Reserva,
 SP,
 MontoReserva,
 Calificacion,
 PI_Aval,
 SP_Aval,
 ReservaAdicional
FROM Historico.RW_Analitico_Reservas_A20
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
