SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo22_UtilidadNeta_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo22_UtilidadNeta_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Anexo22
SET errorFormato = 1
WHERE LEN(UtilidadNeta)>0 AND (ISNUMERIC(ISNULL(UtilidadNeta,'0')) = 0 OR UtilidadNeta LIKE '%[^0-9 -.]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo22_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoCliente,
 'UtilidadNeta',
 UtilidadNeta,
 1,
 @Detalle
FROM dbo.FILE_Anexo22
WHERE LEN(UtilidadNeta)>0 AND (ISNUMERIC(ISNULL(UtilidadNeta,'0')) = 0 OR UtilidadNeta LIKE '%[^0-9 -.]%');
GO
