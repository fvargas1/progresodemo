SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_115]

AS



BEGIN



-- El PORCENTAJE DE PAGOS A ENTIDADES COMERCIALES CON 60 O MÁS DÍAS DE ATRASO EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoEntComer

FROM dbo.RW_VW_R04C0465_INC

WHERE LEN(PorcPagoEntComer) > 0 AND PorcPagoEntComer <> '-99'

	AND (CHARINDEX('.',PorcPagoEntComer) = 0 OR CHARINDEX('.',PorcPagoEntComer) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoEntComer, CHARINDEX('.', PorcPagoEntComer) + 1, LEN(PorcPagoEntComer)))) <> 6);



END
GO
