SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_INSUMOS_R01_SICC_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	FechaPeriodo,
	Concepto,
	MonedaISO,
	SaldoValorizado
FROM Historico.RW_INSUMOS_R01_SICC
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY FechaPeriodo, Concepto;
GO
