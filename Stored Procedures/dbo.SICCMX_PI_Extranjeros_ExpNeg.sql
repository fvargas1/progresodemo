SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_PI_Extranjeros_ExpNeg]
AS

/************************************************
 ***** ANEXO 20 *****
*************************************************/
-- SE SUMAN 113 PUNTOS A DEUDORES DE ANEXO 20 SI SON EXTRANJEROS Y MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 113
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo20 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;

-- SE INSERTA LOG
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se suman 113 puntos al factor cuantitativo ya que es extranjero y muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo20 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;


-- SE SUMAN 230 PUNTOS A DEUDORES DE ANEXO 20 SI SON EXTRANJEROS Y NO MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 230
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo20 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;

-- SE INSERTA LOG
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se suman 230 puntos al factor cuantitativo ya que es extranjero y no muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo20 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;




/************************************************
 ***** ANEXO 21 *****
*************************************************/
DECLARE @IdVar INT;
DECLARE @Campo VARCHAR(50);
DECLARE @SENT VARCHAR(500);

DELETE FROM dbo.SICCMX_Persona_PI_Detalles WHERE IdPersona IN (
 SELECT pInfo.IdPersona
 FROM dbo.SICCMX_PersonaInfo pInfo
 INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
 INNER JOIN dbo.SICCMX_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
 WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0
);

-- SE ACTUALIZAN VARIABLES PARA CALCULO DE PI A DEUDORES DE ANEXO 21 QUE SON EXTRANJEROS Y MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE anx
SET
 SinAtrasos = 0,
 AntSocInfCredCredito = NULL,
 QuitasCastRest = NULL,
 PorPagoTiemInstNoBanc = NULL,
 PorPagIntBan1a29Atraso = NULL,
 PorPagIntBan90oMasAtraso = NULL,
 NumDiasMoraPromInstBanc = NULL,
 NumPagTiemIntBanUlt12Meses = NULL,
 NumDiasAtrInfonavitUltBimestre = NULL,
 TasaRetLab = NULL,
 ProcOriAdmonCred = 1
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;


DECLARE curAnx CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=21 AND Campo IS NOT NULL AND Codigo LIKE '21CA[_]%';

OPEN curAnx;

FETCH NEXT FROM curAnx INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles (IdPersona, IdVariable, ValorActual) ' +
'SELECT anx.IdPersona, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo21 anx ' +
'INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON anx.IdPersona = pInfo.IdPersona ' +
'INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais ' +
'WHERE pais.Codigo <> ''484'' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.SinAtrasos, 0) <> 1';
EXEC(@SENT);

FETCH NEXT FROM curAnx INTO @IdVar, @Campo;

END

CLOSE curAnx;
DEALLOCATE curAnx;


-- SE ACTUALIZAN VARIABLES PARA CALCULO DE PI A DEUDORES DE ANEXO 21 QUE SON EXTRANJEROS Y NO MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE anx
SET
 SinAtrasos = 1,
 AntSocInfCredCredito = NULL,
 QuitasCastRest = NULL,
 PorPagoTiemInstNoBanc = NULL,
 PorPagIntBan1a29Atraso = NULL,
 PorPagIntBan90oMasAtraso = NULL,
 NumDiasMoraPromInstBanc = NULL,
 NumPagTiemIntBanUlt12Meses = NULL,
 NumDiasAtrInfonavitUltBimestre = NULL,
 TasaRetLab = NULL,
 ProcOriAdmonCred = 1
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;


DECLARE curAnx2 CURSOR FOR
SELECT Id, Campo
FROM dbo.SICCMX_PI_Variables
WHERE IdMetodologia=21 AND Campo IS NOT NULL AND Codigo LIKE '21SA[_]%';

OPEN curAnx2;

FETCH NEXT FROM curAnx2 INTO @IdVar, @Campo;

WHILE @@FETCH_STATUS = 0
BEGIN

SET @SENT =
'INSERT INTO dbo.SICCMX_Persona_PI_Detalles (IdPersona, IdVariable, ValorActual) ' +
'SELECT anx.IdPersona, ' + CONVERT(VARCHAR(3), @IdVar) + ', ' + @Campo + ' ' +
'FROM dbo.SICCMX_VW_Anexo21 anx ' +
'INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON anx.IdPersona = pInfo.IdPersona ' +
'INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais ' +
'WHERE pais.Codigo <> ''484'' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.SinAtrasos, 0) = 1';
EXEC(@SENT);

FETCH NEXT FROM curAnx2 INTO @IdVar, @Campo;

END

CLOSE curAnx2;
DEALLOCATE curAnx2;



--Primero se calcula el puntaje para aquellas variables donde no hay informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_Persona_PI ppi ON det.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable
INNER JOIN dbo.SICCMX_Metodologia met ON met.IdMetodologia = ppi.IdMetodologia
WHERE det.ValorActual IS NULL AND mat.RangoMenor IS NULL AND met.Codigo='21';


--Se calculan los valores cuando existe informacion
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_Persona_PI ppi ON det.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual >= mat.RangoMenor AND ValorActual < mat.RangoMayor
INNER JOIN SICCMX_Metodologia met ON met.IdMetodologia = ppi.IdMetodologia
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor <> mat.RangoMayor AND met.Codigo='21';


--Se calculan las variables cuando no existen diferencias entre el menor y mayor
UPDATE det
SET
 det.PuntosActual = mat.Puntos,
 det.FechaCalculo = GETDATE(),
 det.UsuarioCalculo = ''
FROM dbo.SICCMX_Persona_PI_Detalles det
INNER JOIN dbo.SICCMX_Persona_PI ppi ON det.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICCMX_PI_MatrizPuntaje mat ON mat.IdVariable = det.IdVariable AND ValorActual = mat.RangoMenor
INNER JOIN SICCMX_Metodologia met ON met.IdMetodologia = ppi.IdMetodologia
WHERE det.ValorActual IS NOT NULL AND mat.RangoMenor = mat.RangoMayor AND met.Codigo='21';



-- SE SUMAN DE NUEVO LOS PUNTOS
UPDATE per
SET
 per.FactorCuantitativo = fact.FAC_CUANTI,
 per.FactorCualitativo = fact.FAC_CUALI
FROM dbo.SICCMX_Persona_PI per
INNER JOIN SICCMX_VW_Persona_PI_Factores fact ON fact.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo21 anx ON pInfo.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0;



-- LOG de calculo...
INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se actualiza cliente a [Sin Atrasos] y sus variables a [Sin información] ya que es extranjero y no muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo21 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;

INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se actualiza cliente a [Con Atrasos] y sus variables a [Sin información] ya que es extranjero y muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo21 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;




/************************************************
 ***** ANEXO 22 *****
*************************************************/
-- SE SUMAN 349 PUNTOS A DEUDORES DE ANEXO 22 SI SON EXTRANJEROS Y MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 349
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se suman 349 puntos al factor cuantitativo ya que es extranjero y muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 1;


-- SE SUMAN 515 PUNTOS A DEUDORES DE ANEXO 22 SI SON EXTRANJEROS Y NO MUESTRAN EXPERIENCIAS NEGATIVAS DE PAGO
UPDATE ppi
SET FactorCuantitativo = FactorCuantitativo + 515
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;


INSERT INTO dbo.SICCMX_Persona_PI_Log (IdPersona, FechaCalculo, Usuario, Descripcion)
SELECT DISTINCT
 ppi.IdPersona,
 GETDATE(),
 '',
 'Se suman 515 puntos al factor cuantitativo ya que es extranjero y no muestra experiencias negativas de pago, quedando como resultado '+ CONVERT(VARCHAR, CONVERT(INT, ppi.FactorCuantitativo))
FROM dbo.SICCMX_PersonaInfo pInfo
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pInfo.IdPersona = ppi.IdPersona
INNER JOIN dbo.SICC_Pais pais ON pInfo.IdNacionalidad = pais.IdPais
INNER JOIN dbo.SICCMX_Anexo22 anx ON ppi.IdPersona = anx.IdPersona
WHERE pais.Codigo <> '484' AND ISNULL(anx.CalCredAgenciaCal,0) = 0 AND ISNULL(anx.ExpNegativasPag,0) = 0;
GO
