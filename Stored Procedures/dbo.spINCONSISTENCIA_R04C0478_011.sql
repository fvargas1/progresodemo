SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_011]
AS

BEGIN

-- Se revisará que la clave de la institución dentro del ID sea correcta.

DECLARE @IdInstitucion INT;
SELECT @IdInstitucion = Value FROM dbo.BAJAWARE_Config WHERE CodeName='CodigoInstitucion';

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	@IdInstitucion AS CodigoInstitucion,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0478_INC
WHERE SUBSTRING(CodigoCreditoCNBV,2,6) <> @IdInstitucion;

END


GO
