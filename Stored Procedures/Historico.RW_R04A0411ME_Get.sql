SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04A0411ME_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato
FROM Historico.RW_R04A0411ME
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Concepto;
GO
