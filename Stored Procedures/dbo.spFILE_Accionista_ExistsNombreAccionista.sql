SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Accionista_ExistsNombreAccionista]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Accionista_ExistsNombreAccionista';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Accionista
SET errorFormato = 1
WHERE LEN(ISNULL(NombreAccionista, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Accionista_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
	Deudor,
	'NombreAccionista',
	NombreAccionista,
	1,
	@Detalle
FROM dbo.FILE_Accionista
WHERE LEN(ISNULL(NombreAccionista, '')) = 0;
GO
