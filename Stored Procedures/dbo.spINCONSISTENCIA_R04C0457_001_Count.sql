SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0457_001_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- El Monto Total Pagado Ef. por el Acreditado en el Periodo debe ser > a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0457_INC
WHERE CAST(ISNULL(NULLIF(MontoPagado,''),'0') AS DECIMAL) <= 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
