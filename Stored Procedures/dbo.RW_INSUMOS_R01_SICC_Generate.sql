SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INSUMOS_R01_SICC_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaMigracion DATETIME;
DECLARE @FechaPeriodo DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INSUMO_RR' AND Nombre = '_R01';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Insumos de SICC para R01', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_INSUMOS_R01_SICC;

INSERT INTO dbo.RW_INSUMOS_R01_SICC (
 IdReporteLog, FechaPeriodo, Concepto, MonedaISO, SaldoValorizado
)
SELECT
 @IdReporteLog,
 @FechaPeriodo,
 map.Cuenta,
 mon.CodigoISO,
 cre.MontoValorizado
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pif ON per.IdPersona = pif.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON cre.IdSituacionCredito = sit.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Destino dsp ON cre.IdDestino = dsp.IdDestino
LEFT OUTER JOIN dbo.SICC_TipoPersona tpr ON pif.IdTipoPersona = tpr.IdTipoPersona
LEFT OUTER JOIN dbo.RW_MAPEO_R01 map ON sit.Codigo = map.SituacionCredito AND dsp.Codigo = map.TipoCredito AND met.CodigoMapR01 = map.Anexo AND tpr.Codigo = map.PersonalidadJuridica
WHERE map.TipoSaldo = 'SALDO'

UNION ALL
SELECT
 @IdReporteLog,
 @FechaPeriodo,
 map.Cuenta,
 mon.CodigoISO,
 crv.PorReservaFinal
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Persona per ON cre.IdPersona = per.IdPersona
INNER JOIN dbo.SICCMX_PersonaInfo pif ON per.IdPersona = pif.IdPersona
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_Moneda mon ON cre.IdMoneda = mon.IdMoneda
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON cre.IdSituacionCredito = sit.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Destino dsp ON cre.IdDestino = dsp.IdDestino
LEFT OUTER JOIN dbo.SICC_TipoPersona tpr ON pif.IdTipoPersona = tpr.IdTipoPersona
LEFT OUTER JOIN dbo.RW_MAPEO_R01 map ON sit.Codigo = map.SituacionCredito AND dsp.Codigo = map.TipoCredito AND met.CodigoMapR01 = map.Anexo AND tpr.Codigo = map.PersonalidadJuridica
WHERE map.TipoSaldo = 'RESERVA';


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_INSUMOS_R01_SICC WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos= @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
