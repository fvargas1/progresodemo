SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_DistGarantias_PM]
AS
-- GARANTIAS PASO Y MEDIDA
UPDATE can
SET
 C = gar.Monto,
 PrctCobSinAju = CASE WHEN cre.EI_Total > 0 THEN CAST(gar.Monto / cre.EI_Total AS DECIMAL(18,8)) ELSE 0 END,
 PrctCobAjust = CASE WHEN cre.EI_Total > 0 THEN dbo.MIN2VAR(1, CAST(gar.Monto / cre.EI_Total AS DECIMAL(18,8))) ELSE 0 END,
 MontoCobAjust = dbo.MIN2VAR(cre.EI_Total, gar.Monto),
 [PI] = gar.PIGarante
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
INNER JOIN (
	SELECT cg.IdCredito, g.IdTipoGarantia, MIN(g.PIGarante) AS PIGarante, SUM(cg.MontoCubiertoCredito) AS Monto
	FROM dbo.SICCMX_CreditoGarantia cg
	INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia
	WHERE g.CodigoTipoGarantia = 'NMC-05'
	GROUP BY cg.IdCredito, g.IdTipoGarantia
) AS gar ON cre.IdCredito = gar.IdCredito AND can.IdTipoGarantia = gar.IdTipoGarantia;
GO
