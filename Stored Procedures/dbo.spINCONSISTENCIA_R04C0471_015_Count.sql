SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_015_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se identifican créditos reportados en el formulario de severidad que no fueron reportados en el formulario de seguimiento.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rSP.IdReporteLog)
FROM dbo.RW_R04C0471 rSP
LEFT OUTER JOIN dbo.RW_R04C0469 rSE ON rSP.NumeroDisposicion = rSE.NumeroDisposicion
WHERE rSE.NumeroDisposicion IS NULL

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
