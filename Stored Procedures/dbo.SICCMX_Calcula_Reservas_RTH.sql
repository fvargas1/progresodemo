SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_Reservas_RTH]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '2';


UPDATE res
SET ReservaExpuesto = ROUND(hrv.[PI] * res.SPExpuesto * res.MontoExpuesto, 2),
	ReservaCubierto = ROUND(hrv.[PI] * res.SPCubierto * res.MontoCubierto, 2)
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
WHERE pre.IdMetodologia = @IdMetodologia;

UPDATE hrv
SET Reserva = res.ReservaExpuesto + res.ReservaCubierto
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON hrv.IdHipotecario = res.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON res.IdHipotecario = pre.IdHipotecario
WHERE pre.IdMetodologia = @IdMetodologia;


UPDATE res
SET PorcentajeExpuesto = CASE WHEN MontoExpuesto > 0 THEN ReservaExpuesto / MontoExpuesto ELSE 0 END,
	PorcentajeCubierto = CASE WHEN MontoCubierto > 0 THEN ReservaCubierto / MontoCubierto ELSE 0 END
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON res.IdHipotecario = pre.IdHipotecario
WHERE pre.IdMetodologia = @IdMetodologia;

UPDATE hrv
SET PorReserva = hrv.Reserva / hrv.E
FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
WHERE pre.IdMetodologia = @IdMetodologia AND hrv.E > 0;
GO
