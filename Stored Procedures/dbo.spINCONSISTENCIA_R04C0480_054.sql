SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_054]
AS

BEGIN

-- Validar que la Fecha de los Estados Financieros Utilizados para el Cálculo de los Puntajes (dat_fecha_consulta_sic) sea MENOR O IGUAL al periodo que se reporta (cve_periodo).

DECLARE @FechaPeriodo VARCHAR(6);
SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	FechaInfoFinanciera,
	@FechaPeriodo AS FechaPeriodo
FROM dbo.RW_VW_R04C0480_INC
WHERE ISNULL(FechaInfoFinanciera,'') > @FechaPeriodo;

END


GO
