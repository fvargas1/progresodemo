SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0472_008]
AS

BEGIN

-- Validar que el Monto Reconocido por Bonificaciones y Descuentos en el Periodo (dat_monto_bonificacion) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoBonificaciones
FROM dbo.RW_VW_R04C0472_INC
WHERE CAST(ISNULL(NULLIF(MontoBonificaciones,''),'-1') AS DECIMAL) < 0;

END


GO
