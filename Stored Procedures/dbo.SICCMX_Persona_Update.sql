SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Update](
 @IdPersona bigint,
 @Codigo varchar(50),
 @Nombre varchar(150),
 @RFC varchar(50),
 @Domicilio varchar(150) = NULL,
 @IdGrupoEconomico bigint = NULL,
 @Telefono varchar(50) = NULL,
 @Correo varchar(50) = NULL,
 @IdEjecutivo bigint = NULL,
 @IdInstitucion bigint,
 @IdActividadEconomica bigint = NULL,
 @IdLocalidad bigint = NULL,
 @IdTamanoDeudor bigint = NULL,
 @IdSucursal bigint = NULL,
 @IdUsuario bigint = NULL,
 @DiasIncumplimiento int = NULL,
 @IdIndustria bigint = NULL,
 @Estado varchar(50) = NULL,
 @IdEntidad bigint = NULL,
 @FechaCreacion datetime = NULL,
 @FechaActualizacion datetime = NULL,
 @Username varchar(50) = NULL)
AS

UPDATE dbo.SICCMX_Persona
SET 
 Codigo = @Codigo,
 Nombre = @Nombre,
 RFC = @RFC,
 Domicilio = @Domicilio,
 --IdGrupoEconomico = @IdGrupoEconomico,
 Telefono = @Telefono,
 Correo = @Correo,
 IdEjecutivo = @IdEjecutivo,
 IdInstitucion = @IdInstitucion,
 IdActividadEconomica = @IdActividadEconomica,
 IdLocalidad = @IdLocalidad,
 --IdDeudorRelacionado = @IdDeudorRelacionado,
 --IdTamanoDeudor = @IdTamanoDeudor,
 IdSucursal = @IdSucursal,
 --IdUsuario = @IdUsuario,
 --DiasIncumplimiento = @DiasIncumplimiento,
 --IdIndustria = @IdIndustria,
 --Estado = @Estado,
 --IdEntidad = @IdEntidad,
 FechaActualizacion = GETDATE(),
 Username = @Username
WHERE IdPersona= @IdPersona
GO
