SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Inconsistencia_Update]
	@IdInconsistencia INT,
	@Nombre VARCHAR(150),
	@Descripcion VARCHAR(2000),
	@CodenameCount VARCHAR(150),
	@Codename VARCHAR(150),
	@IdCategoria INT,
	@Activo BIT,
	@Regulatorio BIT,
	@FechaCreacion DATETIME,
	@FechaActualizacion DATETIME = NULL,
	@Username VARCHAR(50)
AS
UPDATE dbo.MIGRACION_Inconsistencia
SET
	Nombre = @Nombre,
	Descripcion = @Descripcion,
	CodenameCount = @CodenameCount,
	Codename = @Codename,
	IdCategoria = @IdCategoria,
	Activo = @Activo,
	Regulatorio = @Regulatorio,
	FechaCreacion = @FechaCreacion,
	FechaActualizacion = @FechaActualizacion,
	Username = @Username
WHERE IdInconsistencia= @IdInconsistencia;
GO
