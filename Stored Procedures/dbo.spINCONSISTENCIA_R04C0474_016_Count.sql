SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_016_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El campo SEVERIDAD PERDIDA METOD INT debe ser mayor o igual a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0474
WHERE CAST(ISNULL(NULLIF(SPInterna,''),'-1') AS DECIMAL(10,6)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
