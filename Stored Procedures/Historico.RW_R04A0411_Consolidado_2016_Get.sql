SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04A0411_Consolidado_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Concepto,
	SubReporte,
	Moneda,
	TipoDeCartera,
	TipoDeSaldo,
	Dato
FROM Historico.RW_R04A0411_Consolidado_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Concepto;
GO
