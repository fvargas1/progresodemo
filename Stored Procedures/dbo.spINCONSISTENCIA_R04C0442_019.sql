SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_019]
AS

BEGIN

-- La clave de características de disposición del crédito debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.SpecsDisposicionCredito
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_DisposicionCreditoMA cat ON ISNULL(r.SpecsDisposicionCredito,'') = cat.CodigoCNBV
WHERE cat.IdDisposicion IS NULL;

END
GO
