SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_105_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Se identifican acreditados (dat_id_acreditado_institucion) en el formulario de Probabilidad que cuentan
-- con mas de un RFC del Acreditado (dat_rfc). (No aplica cuando se trata de una sustitución de deudor).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0475
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_R04C0475 rep
	INNER JOIN dbo.RW_R04C0475 rep2 ON rep.CodigoPersona = rep2.CodigoPersona AND rep.RFC <> rep2.RFC
	GROUP BY rep.RFC, rep.CodigoPersona
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
