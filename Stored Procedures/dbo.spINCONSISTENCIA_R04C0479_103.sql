SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_103]
AS

BEGIN

-- Validar que la SP No Cubierta por Garantias Personales esté entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPExpuesta
FROM dbo.RW_R04C0479
WHERE CAST(ISNULL(NULLIF(SPExpuesta,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
