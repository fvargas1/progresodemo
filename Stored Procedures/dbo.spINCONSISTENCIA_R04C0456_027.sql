SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_027]
AS
BEGIN
-- La SP Ajustada Por Bienes Inmuebles debe estar entre 1 y 100.

SELECT
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_BienesInmuebles
FROM dbo.RW_VW_R04C0456_INC
WHERE CAST(ISNULL(NULLIF(SP_BienesInmuebles,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
