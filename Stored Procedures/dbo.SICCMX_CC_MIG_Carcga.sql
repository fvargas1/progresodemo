SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CC_MIG_Carcga]
AS
SELECT
	arc.Nombre,
	arc.NumRegistros,
	tm.starttime AS FechaInicio,
	tm.endtime AS FechaFin
FROM dbo.MIGRACION_Archivo arc
CROSS APPLY (
	SELECT TOP 1 vwInc.starttime, vwF.endtime
	FROM dbo.MIGRACION_VW_DtsLog vwInc
	CROSS APPLY (
	SELECT TOP 1 vwFin.endtime
	FROM dbo.MIGRACION_VW_DtsLog vwFin
	WHERE vwFin.executionid = vwInc.executionid AND vwFin.event = 'PackageEnd'
	ORDER BY vwFin.executionid, vwFin.starttime, vwFin.endtime, vwFin.id
	) AS vwF
	WHERE vwInc.event = 'PackageStart' AND vwInc.source = arc.Codename
	ORDER BY vwInc.starttime DESC
) AS tm
WHERE arc.Activo=1
ORDER BY arc.Position
GO
