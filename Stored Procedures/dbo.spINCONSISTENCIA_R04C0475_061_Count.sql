SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_061_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_90_dias_atra) es = 49,
-- entonces el Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (dat_porcen_pgo_bcos_90_dias) debe ser = -999 (Sin Informacion)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_PorcPagoInstBanc90,'') = '49' AND CAST(PorcPagoInstBanc90 AS DECIMAL(10,6)) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
