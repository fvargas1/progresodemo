SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_044]
AS

BEGIN

-- El Municipio debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.Municipio
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_Municipio cat ON ISNULL(r.Municipio,'') = cat.CodigoMunicipio
WHERE cat.IdMunicipio IS NULL;

END
GO
