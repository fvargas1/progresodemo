SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_032]
AS

BEGIN

-- La SP total Por Garantías Reales No Financieras debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_GarRealNoFin
FROM dbo.RW_R04C0466
WHERE CAST(ISNULL(NULLIF(SP_GarRealNoFin,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
