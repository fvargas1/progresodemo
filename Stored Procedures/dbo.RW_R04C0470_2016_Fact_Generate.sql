SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0470_2016_Fact_Generate]
	@IdReporteLog BIGINT,
	@IdPeriodo BIGINT,
	@Entidad VARCHAR(50)
AS
INSERT INTO dbo.RW_R04C0470_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, CodigoPersona, Clasificacion, SinAtrasos, [PI], PuntajeTotal, PuntajeCuantitativo, PuntajeCualitativo, CreditoReportadoSIC,
 Alfa, HITenSIC, FechaConsultaSIC, FechaInfoFinanciera, MesesPI100, ID_PI100, GarantiaLeyFederal, LugarRadica, P_DiasMoraInstBanc, P_PorcPagoInstBanc, P_NumInstRep,
 P_PorcPagoInstNoBanc, P_PagosInfonavit, P_DiasAtrInfonavit, P_TasaRetLab, P_RotActTot, P_RotCapTrabajo, P_RendCapROE, DiasMoraInstBanc, PorcPagoInstBanc, NumInstRep,
 PorcPagoInstNoBanc, PagosInfonavit, DiasAtrInfonavit, NumeroEmpleados, TasaRetLab, PasivoCirculante, UtilidadNeta, CapitalContable, ActivoTotalAnual, VentasNetasTotales,
 IngresosBrutosAnuales, AnioIngresosBrutosAnuales, RotActTot, ActivoCirculante, RotCapTrabajo, RendCapROE, P_EstEco, P_IntCarComp, P_Proveedores, P_Clientes, P_EstFinAudit,
 P_NumAgenCalif, P_IndepConsejoAdmon, P_EstOrg, P_CompAcc, P_LiqOper, P_UafirGastosFin
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '470',
 'FACTORADO' + per.Codigo AS CodigoPersona,
 CASE ppi.IdClasificacion WHEN 1 THEN 111 WHEN 2 THEN 112 WHEN 3 THEN 113 ELSE NULL END AS Clasificacion,
 CASE WHEN anx.SinAtrasos = 1 THEN '1' ELSE '2' END AS SinAtrasos,
 ppi.[PI] * 100 AS [PI],
 ppi.FactorTotal AS PuntajeTotal,
 ppi.FactorCuantitativo AS PuntajeCuantitativo,
 ppi.FactorCualitativo AS PuntajeCualitativo,
 '750' AS CreditoReportadoSIC,
 dlt.FactorCuantitativo AS Alfa,
 '2' AS HITenSIC,
 CASE WHEN anx.FechaInfoBuro IS NULL THEN '0' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoBuro,102),'.',''),1,6) END AS FechaConsultaSIC,
 CASE WHEN anx.FechaInfoFinanc IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,anx.FechaInfoFinanc,102),'.',''),1,6) END AS FechaInfoFinanciera,
 '0' AS MesesPI100,
 '0' AS ID_PI100,
 '790' AS GarantiaLeyFederal,
 anx.LugarRadica AS LugarRadica,
 ptNMC.[22_DIAS_MORA_INST_BANC] AS P_DiasMoraInstBanc,
 ptNMC.[22_PORC_PAGO_INST_BANC] AS P_PorcPagoInstBanc,
 ptNMC.[22_NUM_INST_REP] AS P_NumInstRep,
 ptNMC.[22_PORC_PAGO_INST_NOBANC] AS P_PorcPagoInstNoBanc,
 ptNMC.[22_PAGOS_INFONAVIT] AS P_PagosInfonavit,
 ptNMC.[22_DIAS_ATR_INFONAVIT] AS P_DiasAtrInfonavit,
 ptNMC.[22_TASA_RET_LAB] AS P_TasaRetLab,
 ptNMC.[22_ROT_ACT_TOT] AS P_RotActTot,
 ptNMC.[22_ROT_CAP_TRABAJO] AS P_RotCapTrabajo,
 ptNMC.[22_REND_CAP_ROE] AS P_RendCapROE,
 vlNMC.[22_DIAS_MORA_INST_BANC] AS DiasMoraInstBanc,
 vlNMC.[22_PORC_PAGO_INST_BANC] AS PorcPagoInstBanc,
 vlNMC.[22_NUM_INST_REP] AS NumInstRep,
 vlNMC.[22_PORC_PAGO_INST_NOBANC] AS PorcPagoInstNoBanc,
 vlNMC.[22_PAGOS_INFONAVIT] AS PagosInfonavit,
 vlNMC.[22_DIAS_ATR_INFONAVIT] AS DiasAtrInfonavit,
 anx.NumeroEmpleados AS NumeroEmpleados,
 vlNMC.[22_TASA_RET_LAB] AS TasaRetLab,
 anx.PasivoCirculante AS PasivoCirculante,
 anx.UtilidadNeta AS UtilidadNeta,
 anx.CapitalContableProm AS CapitalContable,
 anx.ActTotalAnual AS ActivoTotalAnual,
 anx.VentNetTotAnuales AS VentasNetasTotales,
 '0' AS IngresosBrutosAnuales,
 YEAR(anx.FechaInfoFinanc) AS AnioIngresosBrutosAnuales,
 vlNMC.[22_ROT_ACT_TOT] AS RotActTot,
 anx.ActivoCirculante AS ActivoCirculante,
 vlNMC.[22_ROT_CAP_TRABAJO] AS RotCapTrabajo,
 vlNMC.[22_REND_CAP_ROE] AS RendCapROE,
 ptNMC.[22_EST_ECO] AS P_EstEco,
 ptNMC.[22_INT_CAR_COMP] AS P_IntCarComp,
 ptNMC.[22_PROVEEDORES] AS P_Proveedores,
 ptNMC.[22_CLIENTES] AS P_Clientes,
 ptNMC.[22_EST_FIN_AUDIT] AS P_EstFinAudit,
 ptNMC.[22_NUM_AGEN_CALIF] AS P_NumAgenCalif,
 ptNMC.[22_INDEP_CONSEJO_ADMON] AS P_IndepConsejoAdmon,
 ptNMC.[22_EST_ORG] AS P_EstOrg,
 ptNMC.[22_COMP_ACC] AS P_CompAcc,
 ptNMC.[22_LIQ_OPER] AS P_LiqOper,
 ptNMC.[22_UAFIR_GASTOS_FIN] AS P_UafirGastosFin
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoAval ca ON cre.IdCredito = ca.IdCredito
INNER JOIN dbo.SICCMX_Aval aval ON ca.IdAval = aval.IdAval
INNER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura AND fig.ReportePI = 1
INNER JOIN dbo.SICCMX_VW_Anexo22_GP anx ON aval.IdAval = anx.IdGP
INNER JOIN dbo.SICC_EsGarante esg ON anx.EsGarante = esg.IdEsGarante AND esg.Layout = 'AVAL'
INNER JOIN dbo.SICCMX_Persona_PI_GP ppi ON anx.IdGP = ppi.IdGP AND anx.EsGarante = ppi.EsGarante
INNER JOIN dbo.SICCMX_PI_ValoresDelta dlt ON ppi.IdMetodologia = dlt.IdMetodologia AND ISNULL(ppi.IdClasificacion,0) = ISNULL(dlt.IdClasificacion,0)
INNER JOIN dbo.SICCMX_VW_PersonasPuntaje_A22_GP ptNMC ON ppi.IdGP = ptNMC.IdGP AND ppi.EsGarante = ptNMC.EsGarante
INNER JOIN dbo.SICCMX_VW_PersonasValor_A22_GP vlNMC ON ppi.IdGP = vlNMC.IdGP AND ppi.EsGarante = vlNMC.EsGarante
WHERE ISNULL(anx.OrgDescPartidoPolitico,0) = 0;
GO
