SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_083]
AS

BEGIN

-- Validar que la Situacion del Credito corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.SituacionCredito
FROM dbo.RW_VW_R04C0464_INC rep
LEFT OUTER JOIN dbo.SICC_SituacionCredito sit ON ISNULL(rep.SituacionCredito,'') = sit.Codigo
WHERE sit.IdSituacionCredito IS NULL;

END

GO
