SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_009]

AS



BEGIN



-- Si el Puntaje Asignado por Aportaciones al Infonavit en el último bimestre (cve_ptaje_aport_infonavit) es = 73,

-- entonces las Aportaciones al Infonavit en el último bimestre (dat_aportaciones_infonavit) deben ser >= 741 y < 1239



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PagosInfonavit,

	P_PagosInfonavit AS Puntos_PagosInfonavit

FROM dbo.RW_VW_R04C0465_INC

WHERE ISNULL(P_PagosInfonavit,'') = '73' AND (CAST(PagosInfonavit AS DECIMAL) < 741 OR CAST(PagosInfonavit AS DECIMAL (18,2)) >= 1239);



END
GO
