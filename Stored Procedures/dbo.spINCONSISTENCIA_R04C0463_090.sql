SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_090]

AS

BEGIN

-- Validar que el Destino del Credito corresponda a Catalogo de CNBV



SELECT

	rep.CodigoCredito,

	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,

	rep.DestinoCredito

FROM dbo.RW_VW_R04C0463_INC rep

LEFT OUTER JOIN dbo.SICC_Destino dest ON ISNULL(rep.DestinoCredito,'') = dest.CNBV_2016

WHERE dest.IdDestino IS NULL;



END
GO
