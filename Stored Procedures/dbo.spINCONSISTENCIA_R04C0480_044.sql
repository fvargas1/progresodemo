SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_044]
AS

BEGIN

-- Si los Días Atrasados Infonavit en el Último Bimestre (dat_dias_mora_infonavit) es >=  28.33,
-- entonces el Puntaje Asignado por Días Atrasados Infonavit en el Último Bimestre (cve_ptaje_dias_atraso_infonav) debe ser = 22

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasAtrInfonavit,
	P_DiasAtrInfonavit AS Puntos_DiasAtrInfonavit
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(DiasAtrInfonavit AS DECIMAL(10,6)) >= 28.33 AND ISNULL(P_DiasAtrInfonavit,'') <> '22';

END


GO
