SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CreditosSinClasificar_Generate]
AS
TRUNCATE TABLE dbo.SICCMX_CreditosSinClasificar;

INSERT INTO dbo.SICCMX_CreditosSinClasificar (CodigoPersona, NombrePersona, NumeroLinea, CodigoCredito)
SELECT
	per.Codigo,
	per.Nombre,
	lin.Codigo,
	cre.Codigo
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Credito cre ON per.IdPersona = cre.IdPersona
LEFT OUTER JOIN dbo.SICCMX_LineaCredito lin ON cre.IdLineaCredito = lin.IdLineaCredito
WHERE cre.IdMetodologia IS NULL;
GO
