SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Archivo_Update]
	@IdArchivo int,
	@Nombre varchar(250) = NULL,
	@Codename varchar(50) = NULL,
	@JobCodename varchar(50) = NULL,
	@ErrorCodename varchar(50) = NULL,
	@LogCodename varchar(50) = NULL,
	@ClearLogCodename varchar(50) = NULL,
	@Position int = NULL,
	@Activo bit = NULL,
	@ViewName varchar(250) = NULL,
	@ValidationStatus varchar(50) = NULL,
	@LastValidationExecuted datetime = NULL,
	@InitCodename varchar(50) = NULL
AS
UPDATE dbo.MIGRACION_Archivo
SET
 Nombre = @Nombre,
 Codename = @Codename,
 JobCodename = @JobCodename,
 ErrorCodename = @ErrorCodename,
 LogCodename = @LogCodename,
 ClearLogCodename = @ClearLogCodename,
 Position = @Position,
 Activo = @Activo,
 ViewName = @ViewName,
 ValidationStatus = @ValidationStatus,
 LastValidationExecuted = @LastValidationExecuted,
 InitCodename = @InitCodename
WHERE IdArchivo= @IdArchivo;
GO
