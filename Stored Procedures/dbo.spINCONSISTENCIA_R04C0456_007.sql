SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0456_007]
AS
BEGIN
-- El porcentaje cubierto pp debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	PrctPP
FROM dbo.RW_VW_R04C0456_INC
WHERE ISNULL(PrctPP,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END
GO
