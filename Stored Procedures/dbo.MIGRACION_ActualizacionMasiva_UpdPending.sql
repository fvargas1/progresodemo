SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_UpdPending]
	@sessionID VARCHAR(100),
	@Id VARCHAR(50)
AS
UPDATE dbo.MIGRACION_ActualizacionMasiva_Temp
SET seActualiza = 1
WHERE Id= @Id AND sessionID = @sessionID;
GO
