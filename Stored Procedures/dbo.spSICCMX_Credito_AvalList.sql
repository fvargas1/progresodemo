SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Credito_AvalList]
 @IdCredito BIGINT
AS
SELECT
 ca.IdAval,
 a.Codigo,
 a.Nombre,
 ta.Nombre AS TipoAval,
 ISNULL(ca.Porcentaje,0) AS Porcentaje,
 'SC' AS CalAval
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICCMX_Aval a ON a.IdAval = ca.IdAVal
INNER JOIN dbo.SICC_TipoGarante ta ON a.IdTipoAval = ta.IdTipoGarante
WHERE ca.IdCredito = @IdCredito AND ca.Aplica = 1
ORDER BY a.Codigo;
GO
