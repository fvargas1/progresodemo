SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_ProyectoLinea_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_ProyectoLinea_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_ProyectoLinea_Hst (
 IdPeriodoHistorico,
 NumeroLinea,
 CodigoProyecto,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 NumeroLinea,
 CodigoProyecto,
 Fuente
FROM dbo.FILE_ProyectoLinea_Hst;
GO
