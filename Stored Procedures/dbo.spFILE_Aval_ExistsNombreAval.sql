SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_ExistsNombreAval]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Aval_ExistsNombreAval';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Aval
SET errorFormato = 1
WHERE LEN(ISNULL(NombreAval, '')) = 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Aval_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 CodigoAval,
 'NombreAval',
 NombreAval,
 1,
 @Detalle
FROM dbo.FILE_Aval
WHERE LEN(ISNULL(NombreAval, '')) = 0;
GO
