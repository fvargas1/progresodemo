SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_INSUMOS_R01_SICC]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_INSUMOS_R01_SICC);

DELETE FROM Historico.RW_INSUMOS_R01_SICC WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_INSUMOS_R01_SICC (
	IdPeriodoHistorico, FechaPeriodo, Concepto, MonedaISO, SaldoValorizado
)
SELECT
	@IdPeriodoHistorico,
	FechaPeriodo,
	Concepto,
	MonedaISO,
	SaldoValorizado
FROM dbo.RW_INSUMOS_R01_SICC
WHERE IdReporteLog = @IdReporteLog;
GO
