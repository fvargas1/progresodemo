SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_030]
AS
BEGIN
-- La SP Ajustada Por Fideicomisos De Garantía Y De Admón. Con Ingresos Propios Como Fuente De Pago debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_FidIngProp
FROM dbo.RW_VW_R04C0461_INC
WHERE CAST(ISNULL(NULLIF(SP_FidIngProp,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
