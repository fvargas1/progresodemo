SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_045]

AS



BEGIN



-- Validar que el Porcentaje de Pagos en Tiempo con Instituciones Financiera Bancarias en los Últimos 12 meses (dat_porcent_pgo_bcos) sea >= 0 pero <= 100



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoInstBanc

FROM dbo.RW_VW_R04C0470_INC

WHERE CAST(PorcPagoInstBanc AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100 AND CAST(PorcPagoInstBanc AS DECIMAL(10,6)) <> -99;



END
GO
