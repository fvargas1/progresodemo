SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Credito_Select_NMC]
	@IdPersona BIGINT
AS
SELECT DISTINCT
	cre.IdCredito,
	cre.NumeroLinea AS Linea,
	cre.CodigoCredito AS Codigo,
	ISNULL(crv.EI_Total,0) AS Saldo,
	ISNULL(crv.EI_Cubierta,0) AS MontoCubierto,
	ISNULL(crv.EI_Expuesta,0) AS MontoExpuesto,
	ISNULL(crv.SP_Total,0) AS SeveridadPerdida,
	ISNULL(crv.ReservaFinal,0) AS Reserva,
	ISNULL(crv.PorReservaFinal, 0) AS PorReserva,
	calif.Codigo AS Calificacion,
	cre.IdMetodologia
FROM dbo.SICCMX_VW_Credito_NMC cre
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet calif ON calif.IdCalificacion = crv.CalifFinal
WHERE cre.IdPersona = @IdPersona AND met.Codigo IN ('18','20','21','22') AND crv.EI_Total > 0
ORDER BY cre.NumeroLinea, cre.CodigoCredito;
GO
