SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_Consumo_GRUPAL_Baja]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_Consumo_GRUPAL_Baja);

DELETE FROM Historico.RW_Consumo_GRUPAL_Baja WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_Consumo_GRUPAL_Baja (
	IdPeriodoHistorico, FolioCredito, TipoCredito, FechaBajaCredito, TipoBajaCredito, QuitasCondonacionesBonificacionesDescuentos
)
SELECT
	@IdPeriodoHistorico,
	FolioCredito,
	TipoCredito,
	FechaBajaCredito,
	TipoBajaCredito,
	QuitasCondonacionesBonificacionesDescuentos
FROM dbo.RW_Consumo_GRUPAL_Baja
WHERE IdReporteLog = @IdReporteLog;
GO
