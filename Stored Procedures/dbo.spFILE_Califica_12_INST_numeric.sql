SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Califica_12_INST_numeric]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Califica_12_INST_numeric';

IF @Requerido = 1
BEGIN
UPDATE dbo.FILE_Califica
SET errorFormato = 1
WHERE LEN([12_INST])>0 AND (ISNUMERIC(ISNULL([12_INST],'0')) = 0 OR [12_INST] LIKE '%[^0-9 ]%');

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Califica_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT DISTINCT
 CodigoDeudor,
 '12_INST',
 [12_INST],
 1,
 @Detalle
FROM dbo.FILE_Califica
WHERE LEN([12_INST])>0 AND (ISNUMERIC(ISNULL([12_INST],'0')) = 0 OR [12_INST] LIKE '%[^0-9 ]%');
GO
