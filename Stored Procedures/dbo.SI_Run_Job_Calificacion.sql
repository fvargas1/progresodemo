SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SI_Run_Job_Calificacion]
 @xml VARCHAR(MAX)
AS
DECLARE @init INT;

IF NOT EXISTS (
 SELECT *
 FROM msdb.dbo.sysjobactivity AS sja
 INNER JOIN msdb.dbo.sysjobs AS sj ON sja.job_id = sj.job_id
 WHERE sja.start_execution_date IS NOT NULL AND sja.stop_execution_date IS NULL AND sj.name='Calificacion'
)
BEGIN

BEGIN TRY
 DECLARE @DocHandle AS INT;
 -- Create an internal representation
 EXEC sys.sp_xml_preparedocument @DocHandle OUTPUT, @xml;
 
 SELECT @init = tmp.valor
 FROM OPENXML (@DocHandle, 'Procesos/inicializa',11) WITH (valor VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS) AS tmp
 
 IF @init = 1
 BEGIN
 UPDATE dbo.MIGRACION_Inicializador SET Estatus=-1;
 END

 UPDATE arc
 SET ValidationStatus=-1, FechaInicio=NULL, FechaFin=NULL
 FROM dbo.MIGRACION_Archivo arc
 INNER JOIN OPENXML (@DocHandle, 'Procesos/sp',11) WITH (codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS) AS tmp ON arc.Codename = tmp.codigo
 WHERE arc.Activo=1;
 
 UPDATE val
 SET Estatus=-1, FechaInicio=NULL, FechaFin=NULL
 FROM dbo.MIGRACION_Archivo arc
 INNER JOIN dbo.MIGRACION_Validacion val ON arc.Codename = val.CategoryName
 WHERE arc.Activo=1 AND ValidationStatus='-1' AND val.Activo=1;

 EXEC msdb.dbo.sp_start_job @job_name = N'Calificacion';

 EXEC sys.sp_xml_removedocument @DocHandle;
 SELECT 'OK';
END TRY
BEGIN CATCH
 SELECT 'ERR: ' +ERROR_MESSAGE();
END CATCH;

END
ELSE
BEGIN
 SELECT 'ERR: Debe el proceso de calificación actual para volver a ejecutarlo';
END
GO
