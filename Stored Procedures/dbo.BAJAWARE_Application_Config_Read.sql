SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Application_Config_Read]
	@CodeName BAJAWARE_utNombreCodigo
AS
SELECT
	Description,
	Name,
	CodeName,
	Folder,
	HomePage,
	SortOrder,
	StringKey,
	Version
FROM dbo.BAJAWARE_Application
WHERE CodeName LIKE '%' + @CodeName + '%';
GO
