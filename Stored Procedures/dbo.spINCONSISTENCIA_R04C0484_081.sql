SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0484_081]
AS
BEGIN
-- Validar que si la Situación del Crédito (cve_situacion_credito) es IGUAL a 2, el dato reportado en la columna de Número de días Vencidos (dat_num_vencidos) sea DIFERENTE de cero.

SELECT
	CodigoCreditoCNBV,
	NumeroDisposicion,
	SituacionCredito,
	DiasAtraso
FROM dbo.RW_VW_R04C0484_INC
WHERE SituacionCredito = '2' AND CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) = 0;

END
GO
