SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_090]
AS

BEGIN

-- Si la Fecha de la Disposición del Acreditado (dat_fecha_disposicion) es IGUAL al periodo que se reporta (cve_periodo),
-- entonces el Monto Dispuesto de la Línea de Crédito en el mes es MAYOR a 0.

DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(Fecha,0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	NumeroDisposicion,
	FechaDisposicion,
	@FechaPeriodo AS FechaPeriodo,
	MontoDispuesto
FROM dbo.RW_VW_R04C0479_INC
WHERE ISNULL(FechaDisposicion,'') = @FechaPeriodo AND CAST(ISNULL(NULLIF(MontoDispuesto,''),'0') AS DECIMAL) <= 0

END


GO
