SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_ConsumoReservasVariablesPreliminares_Select]
 @IdConsumo BIGINT
AS
SELECT
 vw.IdConsumo,
 vw.IdConsumoNombre,
 vw.IdMetodologia,
 vw.IdMetodologiaNombre,
 vw.Metodologia,
 vw.PorcentajePagoRealizado,
 vw.ATR,
 vw.NumeroImpagosConsecutivos,
 vw.NumeroImpagosHistoricos,
 vw.MesesTranscurridos,
 vw.PorPago AS ProPago,
 vw.INDATR,
 vw.PorPR AS ProPR,
 vw.PorcentajeUso,
 vw.VECES,
 vw.MAXATR,
 vw.PorSDOIMP AS PorSDOIMPTotal,
 vw.PorSDOIMP AS PorSDOIMP,
 vw.Ciclos AS CiclosPromedio,
 vw.Ciclos AS CiclosAcum,
 vw.NumeroIntegrantes AS NumIntegrantes,
 vw.ClasificacionReportes,
 vwHst.PorPago_AC,
 vwHst.PorPago_H1,
 vwHst.PorPago_H2,
 vwHst.PorPago_H3,
 vw.Alto,
 vw.Medio,
 vw.Bajo,
 vw.LimiteCredito
FROM dbo.SICCMX_VW_ConsumoReservasVariablesPreliminares vw
INNER JOIN (
 SELECT
 tmp.IdConsumo,
 CASE WHEN tmp.RN = 1 THEN tmp.PorcentajePagoRealizado ELSE NULL END AS PorPago_AC,
 CASE WHEN tmp.RN = 2 THEN tmp.PorcentajePagoRealizado ELSE NULL END AS PorPago_H1,
 CASE WHEN tmp.RN = 3 THEN tmp.PorcentajePagoRealizado ELSE NULL END AS PorPago_H2,
 CASE WHEN tmp.RN = 4 THEN tmp.PorcentajePagoRealizado ELSE NULL END AS PorPago_H3
 FROM dbo.SICCMX_VW_Consumo_Var_Historico tmp
) vwHst ON vw.IdConsumo = vwHst.IdConsumo
WHERE vw.IdConsumo = @IdConsumo;
GO
