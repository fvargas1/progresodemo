SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_091]
AS

BEGIN

-- Validar que el Monto Dispuesto de la Línea de Crédito en el Mes (dat_monto_credito_dispuesto)
-- menos los Pagos a Capital del Periodo (dat_pagos_cacpital) sea menor al Monto De La Línea De
-- Credito Autorizado (dat_monto_credito_linea_aut), con una diferencia absoluta de $1,000.-

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	rep.MontoDispuesto,
	rep.MontoCapitalPagado,
	vw.MontoLineaAut
FROM dbo.RW_VW_R04C0479_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE (CAST(ISNULL(NULLIF(rep.MontoDispuesto,''),'0') AS DECIMAL)-CAST(ISNULL(NULLIF(rep.MontoCapitalPagado,''),'0') AS DECIMAL)) < CAST(ISNULL(NULLIF(vw.MontoLineaAut,''),'0') AS DECIMAL)

END


GO
