SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_089_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que el Nombre Grupo de Riesgo (dat_nombre_grupo_riesgo) venga en mayúsculas.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE CAST(GrupoRiesgo AS VARBINARY(250)) != CAST(UPPER(GrupoRiesgo) AS VARBINARY(120));

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
