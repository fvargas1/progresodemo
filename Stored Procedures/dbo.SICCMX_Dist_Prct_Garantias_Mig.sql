SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Dist_Prct_Garantias_Mig]
AS
UPDATE cg
SET MontoCubiertoCredito = cg.PorcCubiertoCredito * c.MontoValorizado,
 MontoUsadoGarantia = cg.PorcCubiertoCredito * c.MontoValorizado,
 PorcUsadoGarantia = CAST(cg.PorcCubiertoCredito * c.MontoValorizado AS DECIMAL(18,10)) / g.ValorGtiaValorizado
FROM dbo.SICCMX_CreditoGarantia cg
INNER JOIN dbo.SICCMX_VW_Credito_NMC c ON cg.IdCredito = c.IdCredito
INNER JOIN dbo.SICCMX_VW_Garantia g ON cg.IdGarantia = g.IdGarantia;
GO
