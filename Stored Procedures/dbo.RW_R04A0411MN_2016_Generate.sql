SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0411MN_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @SaldoFinalTotalConsumo DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'A-0411MN_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Cartera por tipo de crédito', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();

TRUNCATE TABLE dbo.RW_R04A0411MN_2016;

-- Calculamos el capital
INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 tbl.Concepto,
 '411',
 '14', -- Todo se va como Moneda Nacional
 tbl.Situacion,
 '2', -- Principal
 CAST(SUM(tbl.Monto) AS DECIMAL)
FROM (
 SELECT
 dat.Concepto,
 CASE WHEN dat.SituacionCredito = 1 AND dat.Morosidad = 0 THEN '3' -- Cartera vigente sin pagos vencidos
 WHEN dat.SituacionCredito = 1 AND dat.Morosidad > 0 THEN '4' -- Cartera vigente con pagos vencidos
 ELSE '5' -- Cartera vencida
 END AS Situacion,
 CAST(dat.CapitalVencido AS DECIMAL) + CAST(dat.CapitalVigente AS DECIMAL) AS Monto
 FROM R04.[0411Datos_2016] dat
 WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'MN'
) AS tbl
GROUP BY tbl.Concepto, tbl.Situacion;


-- Calculamos el interes
INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 tbl.Concepto,
 '411',
 '14', -- Todo se va como moneda nacional
 tbl.Situacion,
 '3', -- interes
 CAST(SUM(tbl.Monto) AS DECIMAL)
FROM (
 SELECT
 dat.Concepto,
 CASE WHEN dat.SituacionCredito = 1 AND dat.Morosidad = 0 THEN '3' -- Cartera vigente sin pagos vencidos
 WHEN dat.SituacionCredito = 1 AND dat.Morosidad > 0 THEN '4' -- Cartera vigente con pagos vencidos
 ELSE '5' -- Cartera vencida
 END AS Situacion,
 CAST(dat.InteresVencido AS DECIMAL) + CAST(dat.InteresVigente AS DECIMAL) AS Monto
 FROM R04.[0411Datos_2016] dat
 WHERE dat.Concepto IS NOT NULL AND dat.Moneda = 'MN'
) AS tbl
GROUP BY tbl.Concepto, tbl.Situacion;


-- Calculamos el tipo saldo = total
INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, 1, SUM(CAST(Dato AS DECIMAL))
FROM dbo.RW_R04A0411MN_2016
GROUP BY IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera;

-- Calculamos el total de cartera vigente
INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT IdReporteLog, Concepto, SubReporte, Moneda, 2, TipoDeSaldo, SUM(CAST(Dato AS DECIMAL))
FROM dbo.RW_R04A0411MN_2016
WHERE TipoDeCartera IN ('3','4')
GROUP BY IdReporteLog, Concepto, SubReporte, Moneda, TipoDeSaldo;

-- Calculamos el total de cartera
INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT IdReporteLog, Concepto, SubReporte, Moneda, 1, TipoDeSaldo, SUM(CAST(Dato AS DECIMAL))
FROM dbo.RW_R04A0411MN_2016
WHERE TipoDeCartera IN ('2','5') -- Cartera vigente y vencida
AND TipoDeSaldo = 1 -- Saldo total
GROUP BY IdReporteLog, Concepto, SubReporte, Moneda, TipoDeSaldo;

-- CALCULAMOS LAS CUENTAS PADRES
DECLARE @maxLevel INT;
CREATE TABLE #tree (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
 nivel INT
);

WITH Conceptos (Codigo, LEVEL)
AS (
 SELECT Codigo, 0 AS LEVEL
 FROM dbo.ReportWare_VW_0411Concepto_2016
 WHERE Padre = ''
 UNION ALL
 SELECT vw.Codigo, LEVEL + 1
 FROM dbo.ReportWare_VW_0411Concepto_2016 vw
 INNER JOIN Conceptos con ON vw.Padre = con.Codigo)

INSERT INTO #tree (Codigo, nivel)
SELECT Codigo, [LEVEL] FROM Conceptos;

SELECT @maxLevel = MAX (nivel) FROM #tree;

WHILE @maxLevel >= 0
BEGIN

 INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
 SELECT rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo,
 SUM(CAST(rep.Dato AS DECIMAL))
 FROM dbo.ReportWare_VW_0411Concepto_2016 conceptos
 INNER JOIN dbo.RW_R04A0411MN_2016 rep ON rep.Concepto = conceptos.Codigo
 INNER JOIN #tree con ON rep.Concepto = con.Codigo 
 WHERE con.nivel = @maxLevel AND 
 conceptos.Padre NOT IN (SELECT Concepto FROM dbo.RW_R04A0411MN_2016)
 AND conceptos.Padre <> ''
 GROUP BY rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeSaldo, rep.TipoDeCartera;

 SET @maxLevel = @maxLevel - 1;
END 

DROP TABLE #tree


INSERT INTO dbo.RW_R04A0411MN_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 sal.Concepto,
 sal.SubReporte,
 sal.Moneda,
 sal.TipoCartera,
 sal.TipoSaldo,
 '0'
FROM dbo.BAJAWARE_R04A_Salida_2016 sal
LEFT OUTER JOIN dbo.RW_R04A0411MN_2016 r04 ON sal.Concepto = r04.Concepto AND sal.Moneda = r04.Moneda AND sal.TipoCartera = r04.TipoDeCartera AND sal.TipoSaldo = r04.TipoDeSaldo
WHERE sal.Subreporte = '411' AND sal.Moneda = '14' AND sal.Visible = 1 AND r04.Concepto IS NULL;

DELETE r04
FROM dbo.RW_R04A0411MN_2016 r04
INNER JOIN dbo.BAJAWARE_R04A_Salida_2016 sal ON r04.Concepto = sal.Concepto AND r04.Moneda = sal.Moneda AND r04.TipoDeCartera = sal.TipoCartera AND r04.TipoDeSaldo = sal.TipoSaldo
WHERE sal.SubReporte = '411' AND sal.Moneda = '14' AND sal.Visible = 0;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0411MN_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos= @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
