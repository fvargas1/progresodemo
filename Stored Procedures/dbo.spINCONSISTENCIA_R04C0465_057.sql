SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_057]
AS

BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_29_dias_atra) es = -58,
-- entonces el Porcentaje de Pagos a Inst Financ Bcarias con un máximo de 29 días de atraso en los últimos 12 meses (dat_porcen_pgo_bco_29_dias) debe ser = 0

SELECT
 CodigoPersona AS CodigoDeudor,
 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
 PorcPagoInstBanc29,
 P_PorcPagoInstBanc29 AS Puntos_PorcPagoInstBanc29
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoInstBanc29,'') = '-58' AND CAST(PorcPagoInstBanc29 AS DECIMAL(18,6)) <> 0;

END

GO
