SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Consumo_Reservas_Variables]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Consumo_Reservas_Variables WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Consumo_Reservas_Variables (
 IdPeriodoHistorico,
 Consumo,
 FechaCalificacion,
 [PI],
 E,
 MontoGarantia,
 ReservaTotal,
 PorReserva,
 Calificacion,
 ECubierta,
 SPCubierta,
 ReservaCubierta,
 PorReservaCubierta,
 CalificacionCubierta,
 EExpuesta,
 SPExpuesta,
 ReservaExpuesta,
 PorReservaExpuesta,
 CalificacionExpuesta,
 MontoAdicional,
 ReservaAdicional,
 PorcentajeAdicional,
 CalificacionAdicional,
 Tipo_EX
)
SELECT
 @IdPeriodoHistorico AS IdPeriodoHistorico,
 con.Codigo,
 crv.FechaCalificacion,
 crv.[PI],
 crv.E,
 crv.MontoGarantia,
 crv.ReservaTotal,
 crv.PorReserva,
 cal.Codigo,
 crv.ECubierta,
 crv.SPCubierta,
 crv.ReservaCubierta,
 crv.PorReservaCubierta,
 calCub.Codigo,
 crv.EExpuesta,
 crv.SPExpuesta,
 crv.ReservaExpuesta,
 crv.PorReservaExpuesta,
 calExp.Codigo,
 crv.MontoAdicional,
 crv.ReservaAdicional,
 crv.PorcentajeAdicional,
 calAdi.Codigo,
 crv.Tipo_EX
FROM dbo.SICCMX_Consumo con
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON crv.IdCalificacion = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calCub ON crv.IdCalificacionCubierta = calCub.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calExp ON crv.IdCalificacionExpuesta = calExp.IdCalificacion
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 calAdi ON crv.IdCalificacionAdicional = calAdi.IdCalificacion;
GO
