SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0463_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActividadEconomica,
	GrupoRiesgo,
	LocalidadDeudor,
	DomicilioMunicipio,
	DomicilioEstado,
	Nacionalidad,
	NumInfoCrediticia,
	CURP,
	LEI,
	TipoAlta,
	TipoProducto,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	GrupalCNBV,
	MontoLineaAutorizado,
	FechaMaxDisponer,
	FechaVencLinea,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	PrelacionPago,
	NoRUGM,
	AcredRelacionado,
	InstitucionOrigen,
	TasaReferencia,
	AjusteTasaReferencia,
	OperacionTasaReferencia,
	FrecRevTasa,
	PeriodicidadCapital,
	PeriodicidadInteres,
	MesesGraciaCapital,
	MesesGraciaInteres,
	ComAperturaTasa,
	ComAperturaMonto,
	ComDispTasa,
	ComDispMonto,
	CAT,
	MontoSinAccesorios,
	MontoPrimasAnuales,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_VW_R04C0463
ORDER BY CodigoCredito;
GO
