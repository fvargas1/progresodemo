SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_031_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La tasa de referencia debe existir en el catálogo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.CodigoCredito)
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_TasaReferencia cat ON ISNULL(r.TasaRef,'') = cat.Codigo
WHERE cat.IdTasaReferencia IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
