SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_025]
AS

BEGIN

-- El Saldo del Principal al Final del Período presenta no cuadra con la suma de saldos y montos.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoInicial,
	MontoDispuesto,
	MontoCapitalPagado,
	SaldoFinal AS 'SaldoFinal [SaldoInicial+MontoDispuesto-MontoCapitalPagado]'
FROM dbo.RW_VW_R04C0479_INC
WHERE CAST(ISNULL(SaldoFinal,0) AS MONEY) < (
	CAST(ISNULL(SaldoInicial,0) AS MONEY) +
	CAST(ISNULL(MontoDispuesto,0) AS MONEY) -
	CAST(ISNULL(MontoCapitalPagado,0) AS MONEY))

END


GO
