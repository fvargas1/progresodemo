SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_031]
AS

BEGIN

-- La SP Ajustada Con Otras Garantías Reales No Financieras debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_OtrasGar
FROM dbo.RW_R04C0466
WHERE CAST(ISNULL(NULLIF(SP_OtrasGar,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
