SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_011]


AS





BEGIN





-- El ID met CNBV deberá venir en letras mayúsculas y números, sin caracteres distintos a estos.





SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito


FROM dbo.RW_R04C0443


WHERE CodigoCreditoCNBV LIKE '%[^A-Z0-9_]%' AND CodigoCreditoCNBV NOT LIKE '%&%' OR BINARY_CHECKSUM(CodigoCreditoCNBV) <> BINARY_CHECKSUM(UPPER(CodigoCreditoCNBV));





END
GO
