SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_052]
AS
BEGIN
-- Validar que si la Clave del Estado (cve_estado) está entre 1 y 32, entonces la Localidad del Acreditado (cve_localidad) debe iniciar con 484.

SELECT
	CodigoCredito,
	Estado,
	Localidad
FROM dbo.RW_VW_R04C0458_INC
WHERE CAST(NULLIF(Estado,'') AS INT) BETWEEN 1 AND 32 AND SUBSTRING(Localidad,1,3) <> '484'

END
GO
