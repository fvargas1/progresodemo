SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_059_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Tipo de Cartera (cve_tipo_cartera) es IGUAL a 410 ó 420, la Clave de Estado (cve_estado) debe ser mayor a 32.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TipoCartera,'') IN ('410','420') AND CAST(ISNULL(Estado,'0') AS INT) <= 32;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
