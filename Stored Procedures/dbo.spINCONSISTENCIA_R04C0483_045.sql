SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_045]
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Estado (cve_estado).

SELECT
	CodigoCredito,
	Estado,
	RFC
FROM dbo.RW_VW_R04C0483_INC
WHERE Estado IN (
	SELECT rep.Estado
	FROM dbo.RW_VW_R04C0483_INC rep
	INNER JOIN dbo.RW_VW_R04C0483_INC rep2 ON rep.RFC = rep2.RFC AND rep.Estado <> rep2.Estado
	GROUP BY rep.Estado, rep.RFC
);

END
GO
