SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Datos]
	@IdMetodologia INT
AS

DELETE lg
FROM dbo.SICCMX_Credito_Reservas_Variables_Log lg
INNER JOIN dbo.SICCMX_Credito cr ON lg.IdCredito = cr.IdCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cr.IdPersona = ppi.IdPersona
WHERE ppi.IdMetodologia=@IdMetodologia;

DELETE lg
FROM dbo.SICCMX_LineaCredito_Reservas_Variables_Log lg
INNER JOIN dbo.SICCMX_Credito cr ON lg.IdLineaCredito = cr.IdLineaCredito
INNER JOIN dbo.SICCMX_Persona_PI ppi ON cr.IdPersona = ppi.IdPersona
WHERE ppi.IdMetodologia=@IdMetodologia;

DELETE pl
FROM dbo.SICCMX_Persona_PI_Log pl
INNER JOIN dbo.SICCMX_Persona_PI ppi ON pl.IdPersona = ppi.IdPersona
WHERE ppi.IdMetodologia=@IdMetodologia;

DELETE piDet
FROM dbo.SICCMX_Persona_PI_Detalles piDet
INNER JOIN dbo.SICCMX_Persona_PI ppi ON piDet.IdPersona = ppi.IdPersona
WHERE ppi.IdMetodologia=@IdMetodologia;

UPDATE dbo.SICCMX_Credito SET IdMetodologia=NULL WHERE IdMetodologia=@IdMetodologia;

DELETE FROM dbo.SICCMX_Persona_PI WHERE IdMetodologia=@IdMetodologia;
GO
