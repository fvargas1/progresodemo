SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_015_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La Fecha Máxima para Disponer de los Recursos (dat_fecha_max_disposicion) debe ser MENOR o IGUAL a la Fecha Vencimiento de Línea de Crédito (dat_fecha_vencimiento).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE FecMaxDis > FecVenLin AND ISNULL(TipoAltaCredito,'') IN ('132','133','134','139','700','701','702','731','732','733','741','742','743');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
