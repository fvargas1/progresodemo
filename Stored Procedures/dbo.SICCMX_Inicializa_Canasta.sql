SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Inicializa_Canasta]
AS
-- INICIALIZA CANASTA DE GARANTIAS CREDITO
TRUNCATE TABLE dbo.SICCMX_Garantia_Canasta;

INSERT INTO dbo.SICCMX_Garantia_Canasta (IdCredito, IdTipoGarantia)
SELECT DISTINCT
 crGar.IdCredito,
 CASE WHEN gar.IdClasificacionNvaMet = 1 THEN NULL ELSE gar.IdTipoGarantia END
FROM dbo.SICCMX_CreditoGarantia crGar
INNER JOIN dbo.SICCMX_VW_Garantia gar ON crGar.IdGarantia = gar.IdGarantia
INNER JOIN dbo.SICCMX_Credito credito ON crGar.IdCredito = credito.IdCredito
WHERE gar.IdClasificacionNvaMet IN (1,2,3) AND gar.Aplica = 1 AND crGar.Aplica = 1;


INSERT INTO dbo.SICCMX_Garantia_Canasta (IdCredito, IdTipoGarantia)
SELECT DISTINCT ca.IdCredito, tg.IdTipoGarantia
FROM dbo.SICCMX_CreditoAval ca
INNER JOIN dbo.SICC_TipoGarantia tg ON tg.Codigo='GP'
WHERE ca.Aplica = 1;


-- INSERTANDO EN LOG DE GARANTIAS
EXEC dbo.SICCMX_Garantia_Log_Insert;
GO
