SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0492_019_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La responsabilidad total deberá ser mayor o igual al saldo del principal al final del periodo.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0492
WHERE CAST(ISNULL(NULLIF(ResponsabilidadTotal,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoPrincipalFinal,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
