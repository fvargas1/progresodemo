SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0467_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0467_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0467_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0467_2016 (
	IdReporteLog, Periodo, Entidad, Formulario, CodigoCreditoCNBV, CodigoCredito, TipoBaja, SaldoPrincipalInicio, SaldoInsoluto, MontoPagado, MontoCastigos,
	MontoCondonacion, MontoQuitas, MontoBonificaciones, MontoDescuentos, MontoBienDacion, ReservasCalifCanceladas, ReservasAdicCanceladas
)
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'467',
	cnbv.CNBV AS CodigoCreditoCNBV,
	lin.Codigo AS CodigoCredito,
	tpoBaja.CodigoCNBV AS TipoBaja,
	lin.SaldoInicial AS SaldoPrincipalInicio,
	lin.ResTotalInicioPer AS SaldoInsoluto,
	ISNULL(lin.MontoPagEfeCap,0)+ISNULL(lin.MontoPagEfeInt,0)+ISNULL(lin.MontoPagEfeCom,0)+ISNULL(lin.MontoPagEfeIntMor,0) AS MontoPagado,
	lin.MontoCastigos AS MontoCastigos,
	lin.MontoCondonacion AS MontoCondonacion,
	lin.MontoQuitasCastQue AS MontoQuitas,
	lin.MontoBonificacionDesc AS MontoBonificaciones,
	lin.MontoDescuentos AS MontoDescuentos,
	lin.MontoDacion AS MontoBienDacion,
	res.ReservaPeriodoAnt AS ReservasCalifCanceladas,
	res.ReservaAdicPeriodoAnt AS ReservasAdicCanceladas
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Credito cre ON lin.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Anexo21 anx ON cre.IdPersona = anx.IdPersona
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN (
	SELECT
	tcre.IdLineaCredito,
	SUM(ISNULL(tres.ReservaPeriodoAnt,0)) AS ReservaPeriodoAnt,
	SUM(ISNULL(tres.ReservaAdicPeriodoAnt,0)) AS ReservaAdicPeriodoAnt
	FROM dbo.SICCMX_Credito tcre
	INNER JOIN dbo.SICCMX_Reservas_Variacion tres ON tcre.IdCredito = tres.IdCredito
	GROUP BY tcre.IdLineaCredito
) res ON lin.IdLineaCredito = res.IdLineaCredito
LEFT OUTER JOIN dbo.SICC_TipoBaja tpoBaja ON lin.IdTipoBaja = tpoBaja.IdTipoBaja
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
WHERE (met.Codigo='21' AND ISNULL(anx.OrgDescPartidoPolitico,0) = 0) AND lin.IdTipoBaja IS NOT NULL

UNION
-- BAJAS POR CAMBIO DE METODOLOGIA
SELECT DISTINCT
	@IdReporteLog,
	@IdPeriodo,
	@Entidad,
	'467',
	cnbv.CNBV AS CodigoCreditoCNBV,
	lin.Codigo AS CodigoCredito,
	'140' AS TipoBaja,
	lin.SaldoInicial AS SaldoPrincipalInicio,
	lin.ResTotalInicioPer AS SaldoInsoluto,
	ISNULL(lin.MontoPagEfeCap,0)+ISNULL(lin.MontoPagEfeInt,0)+ISNULL(lin.MontoPagEfeCom,0)+ISNULL(lin.MontoPagEfeIntMor,0) AS MontoPagado,
	lin.MontoCastigos AS MontoCastigos,
	lin.MontoCondonacion AS MontoCondonacion,
	lin.MontoQuitasCastQue AS MontoQuitas,
	lin.MontoBonificacionDesc AS MontoBonificaciones,
	lin.MontoDescuentos AS MontoDescuentos,
	lin.MontoDacion AS MontoBienDacion,
	res.ReservaPeriodoAnt AS ReservasCalifCanceladas,
	res.ReservaAdicPeriodoAnt AS ReservasAdicCanceladas
FROM dbo.SICCMX_LineaCredito lin
INNER JOIN dbo.SICCMX_Persona_CambioMetodologia cm ON lin.Codigo = cm.NumeroDeLineaActual AND cm.MetodologiaAnterior='21'
INNER JOIN (
	SELECT
	tcre.IdLineaCredito,
	SUM(ISNULL(tres.ReservaPeriodoAnt,0)) AS ReservaPeriodoAnt,
	SUM(ISNULL(tres.ReservaAdicPeriodoAnt,0)) AS ReservaAdicPeriodoAnt
	FROM dbo.SICCMX_Credito tcre
	INNER JOIN dbo.SICCMX_Reservas_Variacion tres ON tcre.IdCredito = tres.IdCredito
	GROUP BY tcre.IdLineaCredito
) res ON lin.IdLineaCredito = res.IdLineaCredito
OUTER APPLY (SELECT TOP 1 CNBV FROM dbo.SICCMX_VW_CreditosCNBV WHERE NumeroLinea = lin.Codigo) AS cnbv
WHERE ISNULL(cm.OrgDescAnterior,0) = 0;

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0467_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
