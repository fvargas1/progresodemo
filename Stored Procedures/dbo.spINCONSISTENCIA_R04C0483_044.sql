SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_044]
AS
BEGIN
-- Validar que un mismo RFC acreditado (dat_rfc) no tenga más de un Municipio (cve_municipio_siti).

SELECT
	CodigoCredito,
	Municipio,
	RFC
FROM dbo.RW_VW_R04C0483_INC
WHERE Municipio IN (
	SELECT rep.Municipio
	FROM dbo.RW_VW_R04C0483_INC rep
	INNER JOIN dbo.RW_VW_R04C0483_INC rep2 ON rep.RFC = rep2.RFC AND rep.Municipio <> rep2.Municipio
	GROUP BY rep.Municipio, rep.RFC
);

END
GO
