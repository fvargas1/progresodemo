SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spSICCMX_Detalle_CanastaCred]
 @IdCredito BIGINT
AS
SELECT 
 can.IdCredito,
 CASE
 WHEN can.IdTipoGarantia IS NULL THEN 'Garantias Financieras' ELSE tpoGar.Nombre END AS Tipo,
 ISNULL(can.PrctCobSinAju, 0) AS PorcCobSinAjust,
 ISNULL(can.PrctCobAjust, 0) AS PorcCobAjust,
 ISNULL(can.[PI],0) AS [PI],
 ISNULL(can.SeveridadCorresp, 0) AS SP,
 ISNULL(can.MontoRecuperacion, 0) AS MontoRecuperacion,
 '0' AS MontoRecuperacionAval,
 ISNULL(can.Reserva, 0) AS Reserva
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoGarantia tpoGar ON can.IdTipoGarantia = tpoGar.IdTipoGarantia
WHERE can.IdCredito = @IdCredito AND can.EsDescubierto IS NULL AND can.PrctCobSinAju > 0
UNION ALL
SELECT
 can.IdCredito,
 'Expuesto' AS Tipo,
 can.PrctCobSinAju AS PorcCobSinAjust,
 ISNULL(can.PrctCobAjust, 0) AS PorcCobAjust,
 ISNULL(can.[PI],0) AS [PI],
 ISNULL(can.SeveridadCorresp, 0) AS SP,
 ISNULL(can.MontoRecuperacion, 0) AS MontoRecuperacion,
 '0' AS MontoRecuperacionAval,
 ISNULL(can.Reserva, 0) AS Reserva
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON can.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoGarantia tpoGar ON can.IdTipoGarantia = tpoGar.IdTipoGarantia
WHERE can.IdCredito = @IdCredito AND can.EsDescubierto = 1;
GO
