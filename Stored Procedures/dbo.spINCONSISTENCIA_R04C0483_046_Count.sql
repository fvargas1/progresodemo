SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_046_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Las claves de Actividad Económica (cve_actividad_economica) válidas para los formularios de créditos otorgados a
-- Estados y Municipios son: 93122, 93123, 93132, 93133, 93142, 93143, 93152, 93153, 93162, 93163.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC
WHERE ActEconomica NOT IN ('93122','93123','93132','93133','93142','93143','93152','93153','93162','93163')

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
