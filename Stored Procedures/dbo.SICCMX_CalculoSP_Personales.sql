SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Personales]
AS
-- CALCULAMOS SP PARA GARANTIAS PERSONALES
UPDATE can
SET SeveridadCorresp = tg.SP
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_CreditoAval ca ON can.IdCredito = ca.IdCredito AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
INNER JOIN dbo.SICC_TipoGarantia tgtia ON can.IdTipoGarantia = tgtia.IdTipoGarantia
WHERE tgtia.Codigo='GP';
GO
