SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_026_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Total de Pagos al Infonavit en el Último Bimestre (dat_tot_pagos_infonavit) es > 0 y < 2400 entonces el
-- Puntaje Asignado por el Total de Pagos al Infonavit en el Último Bimestre (cve_ptaje_pgos_infonav) debe ser = 28

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(PagosInfonavit AS DECIMAL) > 0 AND CAST(PagosInfonavit AS DECIMAL) < 2400 AND ISNULL(P_PagosInfonavit,'') <> '28';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
