SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_026_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "MONTO DE LA LÍNEA DEL CRÉDITO AUTORIZADA" deberá ser mayor a 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(CodigoCredito)
FROM dbo.RW_R04C0442
WHERE CAST(ISNULL(NULLIF(SaldoCredito,''),'-1') AS DECIMAL(23,2)) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
