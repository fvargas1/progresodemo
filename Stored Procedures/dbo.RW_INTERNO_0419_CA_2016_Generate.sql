SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_INTERNO_0419_CA_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INTERNO' AND Nombre = '-0419-Cargo_Abono_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_INTERNO_0419_CA_2016;

INSERT INTO dbo.RW_INTERNO_0419_CA_2016 (
	IdReporteLog,
	Codigo,
	Producto,
	ReservaInicial,
	Cargos,
	Abonos,
	ReservaActual,
	ReservaActualCalif,
	Diferencia
)
SELECT
	@IdReporteLog,
	dat.Codigo,
	dat.CodigoProducto,
	CAST(dat.ReservaHistorico AS DECIMAL) AS ReservaInicial,
	CAST(SUM(CASE WHEN con.CargoAbono = 'C' THEN con.Monto ELSE 0 END) AS DECIMAL) AS Cargos,
	CAST(SUM(CASE WHEN con.CargoAbono = 'A' THEN con.Monto ELSE 0 END) AS DECIMAL) AS Abonos,
	CAST(dat.ReservaHistorico
	- SUM(CASE WHEN con.CargoAbono = 'C' THEN con.Monto ELSE 0 END)
	+ SUM(CASE WHEN con.CargoAbono = 'A' THEN con.Monto ELSE 0 END) AS DECIMAL) AS ReservaActual,
	CAST(dat.ReservaActual AS DECIMAL) AS ReservaActualCalif,
	CAST(dat.ReservaActual AS DECIMAL) - CAST(
	dat.ReservaHistorico
	- SUM(CASE WHEN con.CargoAbono = 'C' THEN con.Monto ELSE 0 END)
	+ SUM(CASE WHEN con.CargoAbono = 'A' THEN con.Monto ELSE 0 END)
	AS DECIMAL) AS Diff
FROM R04.[0419Datos_2016] dat
LEFT OUTER JOIN R04.[0419Conceptos_2016] con ON dat.Codigo = con.Codigo AND con.CargoAbono IS NOT NULL
GROUP BY dat.Codigo, dat.CodigoProducto, dat.ReservaHistorico, dat.ReservaActual;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_INTERNO_0419_CA_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
