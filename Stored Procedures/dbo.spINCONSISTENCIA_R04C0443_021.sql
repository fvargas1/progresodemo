SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_021]
AS

BEGIN

-- La tasa de interés bruta del periodo debe ser > 0

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, TasaInteres
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(TasaInteres,''),'0') AS DECIMAL(10,2)) <= 0;

END
GO
