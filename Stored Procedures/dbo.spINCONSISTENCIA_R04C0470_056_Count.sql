SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_056_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo RFC Acreditado no tenga más de un ID Acred. Asignado por la Inst. diferente.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04C0470
WHERE RFC IN (
	SELECT rep.RFC
	FROM dbo.RW_R04C0470 rep
	INNER JOIN dbo.RW_R04C0470 rep2 ON rep.RFC = rep2.RFC
	AND rep.CodigoPersona <> rep2.CodigoPersona
	GROUP BY rep.CodigoPersona, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
