SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_VariablesReservas_NoRevolventesGrupal]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaConsumo FROM dbo.SICCMX_Consumo_Metodologia WHERE Codigo = '4';

UPDATE crv
SET [PI] =
	CASE
	WHEN vp.ATR >= ct.ATRPI THEN 1.00
	ELSE 1 / ( 1 + EXP( -1 * ( ct.Constante + (ct.ATR * vp.ATR) + (ct.ProPagoGrupal * vp.PorPago) + (ct.NumIntegrantes * vp.NumeroIntegrantes) + (ct.CiclosAcum * vp.Ciclos))))
	END,
	SPExpuesta = CASE WHEN vp.ATR >= ct.ATRSP THEN 1 ELSE ct.SPVALOR END,
	E = con.SaldoTotalValorizado,
	MontoGarantia = info.ImporteGarantiaValorizado
FROM dbo.SICCMX_Consumo_Reservas_Variables crv
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares vp ON crv.IdConsumo = vp.IdConsumo
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON vp.IdMetodologia = ct.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Consumo con ON vp.IdConsumo = con.IdConsumo
INNER JOIN dbo.SICCMX_VW_ConsumoInfo info ON con.IdConsumo = info.IdConsumo
LEFT OUTER JOIN dbo.SICCMX_Consumo_Metodologia_ConstantesPorTipoCredito ctc ON ctc.IdMetodologia = ct.IdMetodologia AND ctc.IdTipoCredito = con.IdTipoCredito
WHERE vp.IdMetodologia = @IdMetodologia;
GO
