SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BW_CAT_TipoGarantia]
	@filtro VARCHAR(50)
AS
SELECT DISTINCT
	hm.CodigoBanco,
	cat.Codigo,
	cat.Nombre,
	cat.CodigoCNBV,
	SUM(CASE WHEN hm.CodigoBanco IS NULL THEN 0 ELSE 1 END) OVER() AS vwCB
FROM dbo.SICC_TipoGarantia cat
LEFT OUTER JOIN dbo.FILE_Garantia_Homologacion hm ON cat.Codigo = hm.CodigoBW AND hm.Campo = 'TipoGarantia' AND hm.Activo = 1
WHERE ISNULL(hm.CodigoBanco,'')+'|'+ISNULL(cat.Codigo,'')+'|'+ISNULL(cat.Nombre,'')+'|'+ISNULL(cat.CodigoCNBV,'') LIKE '%' + @filtro + '%'
ORDER BY cat.Codigo;
GO
