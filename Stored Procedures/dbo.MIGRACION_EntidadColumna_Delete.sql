SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_EntidadColumna_Delete]
	@IdEntidadColumns INT
AS
DELETE FROM dbo.MIGRACION_EntidadColumns
WHERE IdEntidadColumns = @IdEntidadColumns;
GO
