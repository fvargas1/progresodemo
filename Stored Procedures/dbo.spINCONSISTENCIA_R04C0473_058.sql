SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_058]
AS

BEGIN

-- Si el Tipo de Cartera (cve_tipo_cartera) es DIFERENTE de 410 ó 420, la Clave de Estado (cve_estado) debe estar dentro 1 a 32

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	TipoCartera,
	Estado
FROM dbo.RW_VW_R04C0473_INC
WHERE ISNULL(TipoCartera,'') NOT IN ('410','420') AND CAST(ISNULL(Estado,'0') AS INT) NOT BETWEEN 1 AND 32;

END


GO
