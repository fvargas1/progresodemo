SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Periodo_Select]
	@IdPeriodo INT
AS
SELECT
	vw.IdPeriodo,
	vw.Fecha,
	vw.Trimestral,
	vw.Activo
FROM dbo.SICC_VW_Periodo vw
WHERE vw.IdPeriodo = @IdPeriodo;
GO
