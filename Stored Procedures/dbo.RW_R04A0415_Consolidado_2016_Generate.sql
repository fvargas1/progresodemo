SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0415_Consolidado_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'A-0415-Consolidado_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_R04A0415_Consolidado_2016;

INSERT INTO dbo.RW_R04A0415_Consolidado_2016 (
 IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato
)
SELECT @IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato FROM dbo.RW_R04A0415ME_2016
UNION ALL
SELECT @IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato FROM dbo.RW_R04A0415MN_2016;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0415_Consolidado_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
