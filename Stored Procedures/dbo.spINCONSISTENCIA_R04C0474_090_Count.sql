SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_090_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si la Moneda de la disposición (cve_moneda = 0) en el formulario de SEGUIMIENTO es IGUAL a 0,
-- entonces el registro de la Moneda (cve_moneda) en el formulario de ALTAS para el mismo CRÉDITO
-- (dat_id_credito_met_cnbv) debe ser IGUAL a 0. (Cruce entre el reporte de seguimiento y el reporte de altas).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(DISTINCT rep.NumeroDisposicion)
FROM dbo.RW_VW_R04C0474_INC rep
INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV
WHERE ISNULL(rep.Moneda,'') = '0' AND ISNULL(vw.Moneda,'') <> '0';

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
