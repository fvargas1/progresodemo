SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_112]
AS

BEGIN

-- Validar que el Grado de Riesgo (cve_grado_riesgo) sea IGUAL a A1, A2, B1, B2, B3, C1, C2, D ó E.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	GradoRiesgo
FROM dbo.RW_R04C0474
WHERE ISNULL(GradoRiesgo,'') NOT IN ('A1','A2','B1','B2','B3','C1','C2','D','E');

END
GO
