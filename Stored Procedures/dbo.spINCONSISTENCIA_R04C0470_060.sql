SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_060]

AS



BEGIN



-- Se identifican acreditados (dat_id_acreditado_institucion) en el formulario de Probabilidad que no fueron identificados en el reporte de Seguimiento.



SELECT

	rPI.CodigoPersona,

	rPI.NombrePersona

FROM dbo.RW_R04C0470 rPI

LEFT OUTER JOIN dbo.RW_R04C0469 rSE ON rPI.CodigoPersona = rSE.CodigoPersona

WHERE rSE.CodigoPersona IS NULL



END
GO
