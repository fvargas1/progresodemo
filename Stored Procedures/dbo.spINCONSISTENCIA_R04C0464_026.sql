SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_026]

AS



BEGIN



-- Institución Banca de Desarrollo o Fondo de Fomento que otorgó el Fondeo debe ser diferente de 0.



SELECT

 CodigoCredito,

 NumeroDisposicion,

 REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

 InstitucionFondeo,

 MontoBancaDesarrollo

FROM dbo.RW_VW_R04C0464_INC

WHERE ISNULL(NULLIF(InstitucionFondeo,''),'0') = '0' AND ISNULL(NULLIF(MontoBancaDesarrollo,''),'0') <> '0.00';



END
GO
