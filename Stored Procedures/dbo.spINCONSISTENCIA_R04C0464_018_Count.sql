SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_018_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Valdar que el Monto Total Pagado Efectivamente por el Acreditado (dat_monto_pagado_total) en el periodo debe ser MAYOR o IGUAL a cero.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(MontoTotalPagado,''),'0') AS DECIMAL) < 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
