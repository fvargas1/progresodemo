SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_062_Count]
 @IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje Asignado por Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (cve_ptaje_pgo_bco_90_dias_atra) es = 63,
-- entonces el Porcentaje de Pagos a Instituciones Financieras Bancarias con 90 o más días de atraso en los últimos 12 meses (dat_porcen_pgo_bcos_90_dias) debe ser = 0

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_PorcPagoInstBanc90,'') = '63' AND CAST(PorcPagoInstBanc90 AS DECIMAL(18,6)) <> 0;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
