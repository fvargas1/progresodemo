SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0454_052]
AS
BEGIN
-- Validar que el Número de días vencidos (dat_num_vencidos) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	DiasAtraso
FROM dbo.RW_VW_R04C0454_INC
WHERE CAST(ISNULL(NULLIF(DiasAtraso,''),'-1') AS DECIMAL) < 0;

END
GO
