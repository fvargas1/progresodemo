SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Moneda_List]
AS
SELECT
 vw.IdMoneda,
 vw.Nombre,
 vw.Codigo,
 vw.MonedaOficial
FROM dbo.SICC_VW_Moneda vw
WHERE vw.AplicaTC = 1
ORDER BY vw.MonedaOficial DESC, vw.Nombre;
GO
