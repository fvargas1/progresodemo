SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0476_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	RFC,
	NombrePersona,
	PrctExpuesto,
	SP,
	EI_SinGarantia,
	[PI],
	NumeroGarRealFin,
	PrctGarRealFin,
	He,
	Hfx,
	Hc,
	ValorGarRealFin,
	SP_GarRealFin,
	EI_GarRealFin,
	NumeroGarRealNoFin,
	PrctGarRealNoFin,
	ValorDerechosCobro,
	ValorBienesInmuebles,
	ValorBienesMuebles,
	ValorFidPartFed,
	ValorFidIngProp,
	ValorOtrasGar,
	SP_DerechosCobro,
	SP_BienesInmuebles,
	SP_BienesMuebles,
	SP_FidPartFed,
	SP_FidIngProp,
	SP_OtrasGar,
	SP_GarRealNoFin,
	NumeroGarPersonales,
	PrctGarPersonales,
	NombreAval,
	PrctAval,
	TipoAval,
	RFCAval,
	TipoGarante,
	PI_Garante,
	ValuacionDerCredito,
	MonedaGarPersonal,
	NombreGaranteECPM,
	NombreGarantePP,
	PrctECPM,
	PrctPP,
	MontoPP
FROM dbo.RW_VW_R04C0476
ORDER BY CodigoCredito;
GO
