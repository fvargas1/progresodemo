SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04D0451_Proporciones]
AS
SELECT
 vw.Nombre AS Calificacion,
 SUM(CASE WHEN rep.TipoSaldo = '1' THEN CONVERT(MONEY, rep.Dato) ELSE 0 END ) AS SaldoTrimestreActual,
 CASE WHEN vw.Nombre = 'FINALCartera Vencida (>365 dias)' THEN 0
 ELSE SUM(CASE WHEN rep.TipoSaldo = '19' THEN CONVERT(MONEY, rep.Dato)
 ELSE 0 END ) END AS SaldoTrimestreAnterior,
 CASE WHEN vw.Nombre = 'FINALCartera Vencida (>365 dias)' THEN 0
 ELSE SUM(CASE WHEN rep.TipoSaldo = '115' THEN CONVERT(MONEY, rep.Dato)
 ELSE 0 END ) END AS Variacion,
 Orden
FROM dbo.ReportWare_VW_R04D0451Concepto vw
LEFT OUTER JOIN dbo.RW_R04D0451 rep ON vw.Codigo = rep.Concepto AND rep.MetodoCalificacion = 3
WHERE vw.Codigo IN (
 '843201000000',
 '843202000000',
 '843203000000',
 '843204000000',
 '843205000000',
 '843206000000',
 '844000000000',
 '844001000000',
 '844002000000',
 '844003000000'/*,
 '848000000000'*/
)
GROUP BY Orden,vw.Nombre, TipoCalificacion;
GO
