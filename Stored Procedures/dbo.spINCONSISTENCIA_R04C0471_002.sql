SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_002]
AS

BEGIN

-- El factor ajuste he debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	He
FROM dbo.RW_VW_R04C0471_INC
WHERE ISNULL(He,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END


GO
