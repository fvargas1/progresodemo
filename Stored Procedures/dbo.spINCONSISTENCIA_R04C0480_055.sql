SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_055]
AS

BEGIN

-- Validar que la Probabilidad de Incumplimiento Total (dat_probabilidad_incump) se encuentre en 0 y 100.

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	[PI]
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST([PI] AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
