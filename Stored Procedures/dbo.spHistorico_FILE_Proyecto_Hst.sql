SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Proyecto_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Proyecto_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Proyecto_Hst (
 IdPeriodoHistorico,
 CodigoCliente,
 CodigoProyecto,
 NombreDelProyecto,
 DescripciondelProyecto,
 NumeroLinea,
 Sobrecosto,
 MontoCubiertoPorTerceros,
 MesesContemplados,
 MesesAdicionales,
 VPTotal,
 UtilidadPerdidaAcumulada,
 Etapa,
 FechaInicioOper,
 TasaDescuentoVP,
 VentNetTotAnuales,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoCliente,
 CodigoProyecto,
 NombreDelProyecto,
 DescripciondelProyecto,
 NumeroLinea,
 Sobrecosto,
 MontoCubiertoPorTerceros,
 MesesContemplados,
 MesesAdicionales,
 VPTotal,
 UtilidadPerdidaAcumulada,
 Etapa,
 FechaInicioOper,
 TasaDescuentoVP,
 VentNetTotAnuales,
 Fuente
FROM dbo.FILE_Proyecto_Hst;
GO
