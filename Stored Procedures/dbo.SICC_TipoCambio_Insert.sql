SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_TipoCambio_Insert]
	@IdMoneda INT,
	@IdPeriodo INT,
	@Valor DECIMAL(18,6)
AS
INSERT INTO dbo.SICC_TipoCambio (IdMoneda, IdPeriodo, Valor)
VALUES (@IdMoneda, @IdPeriodo, @Valor);

SELECT SCOPE_IDENTITY();
GO
