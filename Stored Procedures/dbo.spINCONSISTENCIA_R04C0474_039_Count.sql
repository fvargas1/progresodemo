SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_039_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el crédito es vigente, entonces el número de días vencido (Col.27) debe ser menor a 91.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC
WHERE ISNULL(SituacionCredito,'') ='1' AND CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) >= 91;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
