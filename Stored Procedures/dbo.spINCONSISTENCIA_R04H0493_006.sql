SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_006]
AS

BEGIN

-- Los créditos que se den de baja por reestructura o bursatilización, deberán darse de Alta por el mismo motivo que origina la baja.
-- (Revisar catálogo de tipo de alta de crédito del formulario H-0491).

SELECT r93.CodigoCredito, r93.CodigoCreditoCNBV, r93.TipoBaja, r91.TipoAlta
FROM dbo.RW_R04H0493 r93
LEFT OUTER JOIN dbo.RW_R04H0491 r91 ON r93.CodigoCreditoCNBV = r91.CodigoCreditoCNBV AND r91.TipoAlta='10'
WHERE r93.TipoBaja = '2' AND r91.CodigoCreditoCNBV IS NULL;

END
GO
