SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_048]
AS

BEGIN

-- Validar que el Puntaje Crediticio Cuantitativo (dat_ptaje_credit_cuantit) sea >= 497 pero <=1029

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PuntajeCuantitativo
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PuntajeCuantitativo AS DECIMAL) NOT BETWEEN 497 AND 1029;

END


GO
