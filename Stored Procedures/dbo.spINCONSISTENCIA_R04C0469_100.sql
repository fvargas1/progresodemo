SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_100]
AS

BEGIN

-- Si el crédito es calificado como Proyecto De Inversión Con Fuente De Pago Propia (cve_fuente_pago = 1),
-- entonces la Severidad de la Pérdida (dat_severidad_perdida), la Probabilidad de Incumplimiento y la Exposición al Incumplimiento deben ser igual a 0.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	SPTotal,
	PITotal,
	EITotal
FROM dbo.RW_R04C0469
WHERE ISNULL(ProyectoInversion,'') = '1'
	AND (ISNULL(SPTotal,'') <> '0.000000' OR ISNULL(PITotal,'') <> '0.000000' OR ISNULL(EITotal,'') <> '0');

END
GO
