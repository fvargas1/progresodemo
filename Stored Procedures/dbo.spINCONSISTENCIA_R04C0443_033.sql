SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_033]

AS



BEGIN



-- Se verificará que la responsabilidad total no sea mayor que la línea de crédito autorizada reportada en el formulario de Altas.



SELECT DISTINCT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.NumeroDisposicion, r.ResponsabilidadTotal, cat.MontoLineaAutorizado

FROM dbo.RW_R04C0443 r

INNER JOIN dbo.SICCMX_VW_Datos_0442 cat ON ISNULL(r.CodigoCreditoCNBV,'') = cat.CodigoCreditoCNBV

WHERE CAST(ISNULL(NULLIF(r.SaldoFinal,''),'-1') AS DECIMAL(23,2)) > CAST(ISNULL(NULLIF(cat.MontoLineaAutorizado,''),'-1') AS DECIMAL(23,2));



END
GO
