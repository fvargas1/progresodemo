SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_114_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El PORCENTAJE DE PAGOS EN TIEMPO CON ENTIDADES FINANCIERAS NO BANCARIAS EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0475_INC
WHERE LEN(PorcPagoInstNoBanc) > 0 AND PorcPagoInstNoBanc <> '-999'
	AND (CHARINDEX('.',PorcPagoInstNoBanc) = 0 OR CHARINDEX('.',PorcPagoInstNoBanc) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstNoBanc, CHARINDEX('.', PorcPagoInstNoBanc) + 1, LEN(PorcPagoInstNoBanc)))) <> 6);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
