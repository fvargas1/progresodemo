SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Persona_Totales_Hipotecario_Gen]
AS
DECLARE @IdPeriodo INT;
DECLARE @FechaPeriodo DATETIME;
SELECT @IdPeriodo = IdPeriodo, @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

DELETE FROM dbo.RW_Totales_Por_Periodo_Hipotecario WHERE IdPeriodo = @IdPeriodo;

INSERT INTO dbo.RW_Totales_Por_Periodo_Hipotecario (IdPeriodo, FechaPeriodo, IdPersona, [PI], EI, EI_Cubierta, EI_Expuesta, Reserva, Reserva_Cubierta, Reserva_Expuesta)
SELECT
	@IdPeriodo,
	@FechaPeriodo,
	per.IdPersona,
	hrv.[PI],
	SUM(hrv.E),
	SUM(res.MontoCubierto),
	SUM(res.MontoExpuesto),
	SUM(hrv.Reserva),
	SUM(res.ReservaCubierto),
	SUM(res.ReservaExpuesto)
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Hipotecario hip ON per.IdPersona = hip.IdPersona
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON hip.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_HipotecarioReservas res ON hrv.IdHipotecario = res.IdHipotecario
GROUP BY per.IdPersona, hrv.[PI];
GO
