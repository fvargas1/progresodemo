SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_085_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Localidad (cve_localidad_acreditado).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC
WHERE Localidad IN (
	SELECT rep.Localidad
	FROM dbo.RW_VW_R04C0468_INC rep
	INNER JOIN dbo.RW_VW_R04C0468_INC rep2 ON rep.RFC = rep2.RFC AND rep.Localidad <> rep2.Localidad
	GROUP BY rep.Localidad, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
