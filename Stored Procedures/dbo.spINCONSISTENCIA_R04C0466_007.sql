SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_007]
AS

BEGIN

-- El porcentaje cubierto pp debe encontrarse en formato de porcentaje y no en decimal

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctPP
FROM dbo.RW_R04C0466
WHERE ISNULL(PrctPP,'') NOT LIKE '%.[0-9][0-9][0-9][0-9][0-9][0-9]';

END

GO
