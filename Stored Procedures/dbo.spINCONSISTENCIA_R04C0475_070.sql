SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0475_070]
AS

BEGIN

-- Si el Puntaje Asignado por Antigüedad en SIC (cve_ptaje_antig_sic) es = 60, entonces la Antigüedad en SIC (dat_antig_sic) debe ser >= 36 y < 98

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	AntSocCred,
	P_AntSocCred AS Puntos_AntSocCred
FROM dbo.RW_VW_R04C0475_INC
WHERE ISNULL(P_AntSocCred,'') = '60' AND (CAST(AntSocCred AS DECIMAL) < 36 OR CAST(AntSocCred AS DECIMAL) >= 98);

END


GO
