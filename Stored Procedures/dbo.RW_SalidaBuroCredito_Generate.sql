SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_SalidaBuroCredito_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @FechaPeriodoReporte VARCHAR(6);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'INTERNO' AND Nombre = '_SalidaBuroCredito';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();
SET @FechaPeriodoReporte = SUBSTRING(REPLACE(CONVERT(VARCHAR,@FechaPeriodo,102),'.',''),1,6);

TRUNCATE TABLE dbo.RW_SalidaBuroCredito;

INSERT INTO dbo.RW_SalidaBuroCredito (
	IdReporteLog,
	Periodo,
	BP,
	Cuenta,
	Calificacion,
	Cartera
)
SELECT @IdReporteLog, @FechaPeriodoReporte, BP, Cuenta, Calificacion, Cartera
FROM (
SELECT
	per.Codigo AS BP,
	c.Codigo AS Cuenta,
	califCredito.Codigo AS Calificacion,
	'Comercial' AS Cartera
	FROM dbo.SICCMX_Credito_Reservas_Variables crv
	INNER JOIN dbo.SICCMX_Credito c ON crv.IdCredito = c.IdCredito
	INNER JOIN dbo.SICCMX_Persona per ON c.IdPersona = per.IdPersona
	LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet califCredito ON crv.CalifFinal = califCredito.IdCalificacion
UNION ALL
SELECT
	per.Codigo AS BP,
	c.Codigo AS Cuenta,
	califConsumo.Codigo AS Calificacion,
	'Consumo' AS Cartera
	FROM dbo.SICCMX_Consumo_Reservas_Variables crv
	INNER JOIN dbo.SICCMX_Consumo c ON crv.IdConsumo = c.IdConsumo
	LEFT OUTER JOIN dbo.SICCMX_Persona per ON c.IdPersona = per.IdPersona
	LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 califConsumo ON crv.IdCalificacion = califConsumo.IdCalificacion
UNION ALL
SELECT
	per.Codigo AS BP,
	h.Codigo AS Cuenta,
	califHipotecario.Codigo AS Calificacion,
	'Hipotecario' AS Cartera
	FROM dbo.SICCMX_Hipotecario_Reservas_Variables hrv
	INNER JOIN dbo.SICCMX_Hipotecario h ON hrv.IdHipotecario = h.IdHipotecario
	INNER JOIN dbo.SICCMX_Persona per ON h.IdPersona = per.IdPersona
	LEFT OUTER JOIN dbo.SICC_CalificacionHipotecario2011 califHipotecario ON hrv.IdCalificacion = califHipotecario.IdCalificacion
) A
ORDER BY Cartera, Calificacion, Cuenta


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_SalidaBuroCredito WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
