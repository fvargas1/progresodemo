SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Movimientos_Back_Hst]
AS

INSERT INTO dbo.FILE_Movimientos_Hst (
	CodigoCredito,
	TipoMovimiento,
	Monto,
	Fecha
)
SELECT
	mov.CodigoCredito,
	mov.TipoMovimiento,
	mov.Monto,
	mov.Fecha
FROM dbo.FILE_Movimientos mov
LEFT OUTER JOIN dbo.FILE_Movimientos_Hst hst ON LTRIM(mov.CodigoCredito) = LTRIM(hst.CodigoCredito)
WHERE hst.CodigoCredito IS NULL;
GO
