SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_046]
AS
BEGIN
-- Validar que el Monto de Comisiones Pagadas Efectivamente por el Acreditado en el Periodo (dat_monto_pagado_comisiones) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	MontoComisionPagado
FROM dbo.RW_VW_R04C0459_INC
WHERE CAST(ISNULL(NULLIF(MontoComisionPagado,''),'-1') AS DECIMAL) < 0;

END


GO
