SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_TipoAlta_CargaInicial]
AS
DECLARE @FechaInicial VARCHAR(50);
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdTipoAltaComercial INT;
DECLARE @IdTipoAltaHipotecario INT;

SELECT @FechaInicial = [Value] + '-01' FROM dbo.BAJAWARE_Config WHERE CodeName = 'FECHA_INIT_2016';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @IdTipoAltaComercial = IdTipoAlta FROM dbo.SICC_TipoAlta WHERE Codigo = '131';
SELECT @IdTipoAltaHipotecario = IdTipoAltaHipotecario FROM dbo.SICC_TipoAltaHipotecario WHERE Codigo = '27';

-- INICIALIZAMOS CARGA INICIAL
UPDATE dbo.SICCMX_LineaCredito SET IdTipoAlta = NULL WHERE IdTipoAlta = @IdTipoAltaComercial;
UPDATE dbo.SICCMX_HipotecarioInfo SET IdTipoAlta = NULL WHERE IdTipoAlta = @IdTipoAltaHipotecario;

IF ISDATE(@FechaInicial) = 1 AND (YEAR(@FechaInicial) = YEAR(@FechaPeriodo) AND MONTH(@FechaInicial) = MONTH(@FechaPeriodo))
BEGIN
UPDATE dbo.SICCMX_LineaCredito SET IdTipoAlta = @IdTipoAltaComercial WHERE IdTipoAlta IS NULL;
UPDATE dbo.SICCMX_HipotecarioInfo SET IdTipoAlta = @IdTipoAltaHipotecario WHERE IdTipoAlta IS NULL;
END
GO
