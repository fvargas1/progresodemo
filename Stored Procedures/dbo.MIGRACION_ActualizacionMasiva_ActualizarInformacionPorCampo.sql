SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_ActualizacionMasiva_ActualizarInformacionPorCampo]
	@sessionID VARCHAR(100),
	@nombreView VARCHAR(100),
	@id VARCHAR(100),
	@numeroCampo INT,
	@campoDestino VARCHAR(100),
	@nombreEntidad VARCHAR(100),
	@nombreCampo VARCHAR(100),
	@userName VARCHAR(100)
AS
DECLARE @oldValue VARCHAR(100);
DECLARE @newValue VARCHAR(100);
DECLARE @query VARCHAR(2000);
DECLARE @queryAudit VARCHAR(2000);

SET @oldValue = 'campo'+CAST(@numeroCampo AS VARCHAR)+'Original';
SET @newValue = 'campo'+CAST(@numeroCampo AS VARCHAR)+'Usuario';

SET @query = 'UPDATE '+@nombreView+' SET '+@campoDestino+' = t.'+@newValue;

SET @query = @query + ' FROM '+@nombreView+' o INNER JOIN MIGRACION_ActualizacionMasiva_Temp t on o.'+@id+ ' = t.Id';
SET @query = @query + ' where t.seActualiza = 1 and t.sessionID = '''+@sessionID+'''';

EXEC (@query);

SET @queryAudit = 'INSERT INTO dbo.BAJAWARE_Audit ([Object],Field,ObjectId,OldValue,NewValue,Username,DateCreated,Summary) ';
SET @queryAudit = @queryAudit + 'SELECT '''+@nombreEntidad+''','''+@nombreCampo+''',Id,'+@oldValue+','+@newValue+','''+@userName+''',''';
SET @queryAudit = @queryAudit + CONVERT(VARCHAR, GETDATE(),101)+''',''El '+@nombreEntidad+' con id ''+CONVERT(VARCHAR,Id)+'' se le modifico el campo '+@nombreCampo;
set @queryAudit = @queryAudit + ' el dia '+CONVERT(VARCHAR, GETDATE(),101)+' por el usuario '+@userName+' en actualizacion masiva. ';
SET @queryAudit = @queryAudit + ' El Valor anterior es "''+'+@oldValue+'+''" y el valor nuevo es "''+'+@newValue+'+''"''';
SET @queryAudit = @queryAudit + ' from MIGRACION_ActualizacionMasiva_Temp where seActualiza = 1 and sessionID = '''+@sessionID+''' and '+@oldValue+ ' <> '+@newValue;

EXEC (@queryAudit);
GO
