SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_030_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que el Destino del Credito corresponda a Catalogo de CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC rep
LEFT OUTER JOIN dbo.SICC_Destino dest ON ISNULL(rep.DestinoCredito,'') = dest.CodigoCNBV
WHERE dest.IdDestino IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
