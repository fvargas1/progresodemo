SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_Reservas_A19_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='INTERNO' AND Nombre = '_Analitico_Reservas_A19';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Analitico_Reservas_A19;

INSERT INTO dbo.RW_Analitico_Reservas_A19 (
	IdReporteLog, Fecha, CodigoPersona, NombrePersona, CodigoProyecto, NombreProyecto, CodigoCredito,
	SaldoCredito, FechaVecimiento, Moneda, MontoGarantia, MontoReserva, PrctReserva, Calificacion
)
SELECT DISTINCT
	@IdReporteLog,
	CONVERT(CHAR(10), GETDATE(),126) AS Fecha,
	per.Codigo,
	per.Nombre,
	pry.CodigoProyecto,
	pry.Nombre,
	cre.CodigoCredito,
	CAST(crv.EI_Total AS DECIMAL(23,2)),
	CONVERT(CHAR(10), cre.FechaVencimiento,126) AS FechaVencimiento,
	cre.Moneda,
	pc.ValorGarantia,
	CAST(crv.ReservaFinal AS DECIMAL(23,2)),
	CAST(crv.PorReservaFinal * 100 AS DECIMAL(14,6)),
	cal.Codigo
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_Proyecto pry ON per.IdPersona = pry.IdPersona
INNER JOIN dbo.SICCMX_ProyectoCalificacion pc ON pry.IdProyecto = pc.IdProyecto
INNER JOIN (
	SELECT DISTINCT prln.IdProyecto, prln.IdLineaCredito
	FROM (
	SELECT IdProyecto, IdLineaCredito FROM dbo.SICCMX_Proyecto WHERE IdLineaCredito IS NOT NULL
	UNION
	SELECT IdProyecto, IdLineaCredito FROM dbo.SICCMX_ProyectoLinea
	) AS prln
) AS pl ON pry.IdProyecto = pl.IdProyecto
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pl.IdLineaCredito = cre.IdLineaCredito
INNER JOIN dbo.SICCMX_Credito_Reservas_Variables crv ON cre.IdCredito = crv.IdCredito
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON crv.CalifFinal = cal.IdCalificacion;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_Reservas_A19 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
