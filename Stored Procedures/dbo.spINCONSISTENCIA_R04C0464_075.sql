SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_075]
AS

BEGIN

-- Validar que las Reservas Totales (dat_reservas_totales) sean MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ReservaTotal
FROM dbo.RW_VW_R04C0464_INC
WHERE CAST(ISNULL(NULLIF(ReservaTotal,''),'-1') AS DECIMAL) < 0;

END

GO
