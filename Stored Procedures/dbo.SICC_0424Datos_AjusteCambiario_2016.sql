SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_0424Datos_AjusteCambiario_2016]
AS
UPDATE dat
SET AjusteCambiario = ((ISNULL(dat.SaldoActual,0) + ISNULL(InteresVigente,0) + ISNULL(InteresVencido,0)) * tc.Valor)
 - ((ISNULL(dat.SaldoActual,0) + ISNULL(InteresVigente,0) + ISNULL(InteresVencido,0)) * tcHst.Valor)
FROM R04.[0424Datos_2016] dat
INNER JOIN dbo.SICC_Moneda mon ON dat.Moneda = mon.Codigo
INNER JOIN dbo.SICC_TipoCambio tc ON mon.IdMoneda = tc.IdMoneda
INNER JOIN dbo.SICC_Periodo prd ON tc.IdPeriodo = prd.IdPeriodo AND prd.Activo = 1
LEFT OUTER JOIN (
 SELECT mon1.Codigo AS Moneda, tc1.Valor
 FROM dbo.SICC_TipoCambio tc1
 INNER JOIN dbo.SICC_Moneda mon1 ON tc1.IdMoneda = mon1.IdMoneda
 INNER JOIN dbo.SICC_Periodo prdAct ON prdAct.Activo = 1
 INNER JOIN dbo.SICC_Periodo prdHst ON tc1.IdPeriodo = prdHst.IdPeriodo AND YEAR(prdHst.Fecha) = YEAR(DATEADD(MONTH, -1, prdAct.Fecha)) AND MONTH(prdHst.Fecha) = MONTH(DATEADD(MONTH, -1, prdAct.Fecha))
) AS tcHst ON dat.Moneda = tcHst.Moneda
WHERE dat.SituacionActual = '1' AND dat.SituacionHistorica IS NOT NULL;
GO
