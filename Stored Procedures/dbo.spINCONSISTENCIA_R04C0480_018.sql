SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0480_018]
AS

BEGIN

-- Si el Porcentaje de Pagos en Tiempo con Instituciones Financieras NO Bancarias en los Últimos 12 meses
-- (dat_porcent_pgo_no_bcos) es >= 0 y < 37, entonces el Puntaje Asignado por Porcentaje de Pagos en Tiempo
-- con Instituciones Financieras NO Bancarias en los Últimos 12 meses (cve_ptaje_porc_pgo_no_banco) debe ser = 22

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PorcPagoInstNoBanc,
	P_PorcPagoInstNoBanc AS Puntos_PorcPagoInstNoBanc
FROM dbo.RW_VW_R04C0480_INC
WHERE CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) >= 0 AND CAST(PorcPagoInstNoBanc AS DECIMAL(10,6)) < 37 AND ISNULL(P_PorcPagoInstNoBanc,'') <> '22';

END


GO
