SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_047_Count]

 @IdInconsistencia BIGINT

AS

BEGIN



-- Validar que el Puntaje Crediticio Cualitativo (dat_ptaje_credit_cualit) sea >= 0 pero <= 263



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(IdReporteLog)

FROM dbo.RW_VW_R04C0470_INC

WHERE CAST(PuntajeCualitativo AS DECIMAL) NOT BETWEEN 0 AND 752;



INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
