SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_027]
AS

BEGIN

-- Sí es un crédito sin fuente de pago propia (cve_proy_pago_anexo_19 = 2), entonces la Exposición al Incumplimiento
-- (dat_exposicion_incumplimiento) debe ser MAYOR O IGUAL a la Responsabilidad Total (dat_responsabilidad_total).

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	EITotal AS EI,
	SaldoInsoluto AS ResponsabilidadTotal
FROM dbo.RW_VW_R04C0464_INC
WHERE ISNULL(ProyectoInversion,'') = '2' AND CAST(ISNULL(NULLIF(EITotal,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL);

END

GO
