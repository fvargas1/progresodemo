SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0473_2016]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0473_2016);

DELETE FROM Historico.RW_R04C0473_2016 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0473_2016 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActEconomica, GrupoRiesgo, Localidad, Municipio, Estado,
	IdBuroCredito, LEI, TipoAltaCredito, TipoProducto, TipoOperacion, DestinoCredito, CodigoCredito, CodigoCreditoCNBV, CodigoGlobalCNBV, MonLineaCredValorizado,
	MonLineaCred, FecMaxDis, FecVenLin, Moneda, FormaDisposicion, TipoLinea, Posicion, RegGarantiaMob, PorcPartFederal, IdInstitucionOrigen, TasaInteres,
	DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa, PeriodicidadPagosCapital, PeriodicidadPagosInteres, NumMesesAmortCap, NumMesesPagoInt,
	ComisionAperturaTasa, ComisionAperturaMonto, ComisionDisposicionTasa, ComisionDisposicionMonto,  LocalidadDestinoCredito, MunicipioDestinoCredito,
	EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActEconomica,
	GrupoRiesgo,
	Localidad,
	Municipio,
	Estado,
	IdBuroCredito,
	LEI,
	TipoAltaCredito,
	TipoProducto,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoGlobalCNBV,
	MonLineaCredValorizado,
	MonLineaCred,
	FecMaxDis,
	FecVenLin,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	Posicion,
	RegGarantiaMob,
	PorcPartFederal,
	IdInstitucionOrigen,
	TasaInteres,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	NumMesesAmortCap,
	NumMesesPagoInt,
	ComisionAperturaTasa,
	ComisionAperturaMonto,
	ComisionDisposicionTasa,
	ComisionDisposicionMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_R04C0473_2016
WHERE IdReporteLog = @IdReporteLog;
GO
