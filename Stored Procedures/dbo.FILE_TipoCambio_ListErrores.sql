SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_TipoCambio_ListErrores]
AS
SELECT
	CodigoMonedaISO,
	FechaTipoCambio,
	ValorTipoCambio
FROM dbo.FILE_TipoCambio
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
