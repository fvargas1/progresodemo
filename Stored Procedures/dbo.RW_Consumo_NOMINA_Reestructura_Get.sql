SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Consumo_NOMINA_Reestructura_Get]
	@IdReporteLog BIGINT
AS
SELECT
	FolioCredito,
	FechaReestructuraCredito,
	Reestructura,
	QuitasCondonacionesBonificacionesDescuentos,
	Folio2Credito
FROM dbo.RW_VW_Consumo_NOMINA_Reestructura
ORDER BY FolioCredito;
GO
