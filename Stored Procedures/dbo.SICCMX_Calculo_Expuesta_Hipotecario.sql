SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calculo_Expuesta_Hipotecario]
AS
UPDATE res
SET MontoExpuesto = hrv.E,
 SPExpuesto = hrv.SP,
 ReservaExpuesto = hrv.Reserva,
 PorcentajeExpuesto = hrv.PorReserva,
 IdCalificacionExpuesto = hrv.IdCalificacion,
 MontoCubierto = 0,
 ReservaCubierto = 0,
 PorcentajeCubierto = 0,
 MontoAdicional = 0,
 ReservaAdicional = 0,
 PorcentajeAdicional = 0
FROM dbo.SICCMX_HipotecarioReservas res
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_Variables hrv ON res.IdHipotecario = hrv.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hrv.IdHipotecario = pre.IdHipotecario
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario AND met.Codigo <> '2';
GO
