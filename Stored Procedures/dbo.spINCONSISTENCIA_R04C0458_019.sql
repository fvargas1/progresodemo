SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_019]
AS
BEGIN
-- El número de meses de gracia a capital debe ser mayor o igual a 0

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	NumMesesAmortCap
FROM dbo.RW_VW_R04C0458_INC
WHERE CAST(ISNULL(NULLIF(NumMesesAmortCap,''),'-1') AS DECIMAL) < 0;

END
GO
