SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_034]
AS

BEGIN

-- El porcentaje cubierto por aval debe estar entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	PrctAval
FROM dbo.RW_R04C0466
WHERE CAST(ISNULL(NULLIF(PrctAval,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END

GO
