SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_031_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que la Tasa de Interes corresponda a Catalogo de CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0458_INC rep
LEFT OUTER JOIN dbo.SICC_TasaReferencia tasa ON ISNULL(rep.TasaInteres,'') = tasa.Codigo
WHERE tasa.IdTasaReferencia IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
