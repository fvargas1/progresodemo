SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_079_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Institucion de Fondeo corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0479_INC rep
LEFT OUTER JOIN dbo.SICC_Institucion inst ON ISNULL(rep.InstitucionFondeo,'') = inst.Codigo
WHERE inst.IdInstitucion IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
