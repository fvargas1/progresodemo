SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_027]
AS
BEGIN
-- Validar que la Actividad Economica corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.ActEconomica
FROM dbo.RW_VW_R04C0483_INC rep
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON ISNULL(rep.ActEconomica,'') = act.CodigoCNBV
WHERE act.IdActividadEconomica IS NULL;

END
GO
