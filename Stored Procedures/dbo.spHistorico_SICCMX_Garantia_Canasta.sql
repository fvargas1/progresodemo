SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Garantia_Canasta]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Garantia_Canasta WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Garantia_Canasta (
	IdPeriodoHistorico,
	Credito,
	TipoGarantia,
	He,
	Hc,
	Hfx,
	C,
	PrctCobSinAju,
	PrctCobAjust,
	MontoCobAjust,
	[PI],
	EI_Ajust,
	SeveridadCorresp,
	MontoRecuperacion,
	Reserva,
	EsDescubierto
)
SELECT
	@IdPeriodoHistorico,
	cre.Codigo,
	tg.Codigo,
	can.He,
	can.Hc,
	can.Hfx,
	can.C,
	can.PrctCobSinAju,
	can.PrctCobAjust,
	can.MontoCobAjust,
	can.[PI],
	can.EI_Ajust,
	can.SeveridadCorresp,
	can.MontoRecuperacion,
	can.Reserva,
	can.EsDescubierto
FROM dbo.SICCMX_Garantia_Canasta can
INNER JOIN dbo.SICCMX_Credito cre ON can.IdCredito = cre.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoGarantia tg ON can.IdTipoGarantia = tg.IdTipoGarantia;
GO
