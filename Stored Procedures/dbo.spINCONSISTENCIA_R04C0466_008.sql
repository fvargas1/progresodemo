SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0466_008]
AS

BEGIN

-- El porcentaje descubierto debe ser 100%, si el crédito no cuenta con ninguna garantía asociada. dat_porcent_no_cubierto = 100
-- si dat_hum_gtias_reales_financ=0 y dat_num_gtias_reales_no_fin=0 y dat_pctje_cubto_oblig_aval=0

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PrctGarRealFin,
	PrctGarRealNoFin,
	PrctAval,
	PrctExpuesto
FROM dbo.RW_R04C0466
WHERE CAST(PrctGarRealFin AS DECIMAL(10,6)) + CAST(PrctGarRealNoFin AS DECIMAL(10,6)) + CAST(PrctAval AS DECIMAL(10,6)) + CAST(PrctECPM AS DECIMAL(10,6)) = 0 AND CAST(PrctExpuesto AS DECIMAL(10,6)) <> 100;

END

GO
