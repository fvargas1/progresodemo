SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_LineaCredito_Homologacion]
AS
DECLARE @Campo VARCHAR(50);
DECLARE @FechaPeriodo DATETIME;
DECLARE @Tipo INT;
DECLARE @Fuente VARCHAR(50);

SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

-- ACTUALIZAMOS EL TIPO DE ALTA SI LA FECHA DE DISPOSICION NO ES IGUAL A LA DEL PERIODO
-- ESTO ES PARA LAS INSTITUCIONES QUE SUBEN LA INFORMACION DE LA LINEA SOLO PARA ALTA
UPDATE dbo.FILE_LineaCredito
SET TipoAlta = NULL, TipoAltaMA = NULL
WHERE ISDATE(FechaDisposicion) = 1 AND SUBSTRING(REPLACE(CONVERT(VARCHAR,FechaDisposicion,102),'-',''),1,6) <> SUBSTRING(REPLACE(CONVERT(VARCHAR,@FechaPeriodo,102),'.',''),1,6);

UPDATE dbo.SICCMX_LineaCredito
SET IdTipoAlta = NULL, IdTipoAltaMA = NULL
WHERE ISDATE(FechaDisposicion) = 1 AND SUBSTRING(REPLACE(CONVERT(VARCHAR,FechaDisposicion,102),'-',''),1,6) <> SUBSTRING(REPLACE(CONVERT(VARCHAR,@FechaPeriodo,102),'.',''),1,6);

-- AGREGAMOS "0" A MUNICIPIO CUANDO ESTE TIENE SOLO 4 POSICIONES
UPDATE dbo.FILE_LineaCredito SET MunicipioDestino = '0' + LTRIM(RTRIM(MunicipioDestino)) WHERE LEN(LTRIM(RTRIM(MunicipioDestino))) = 4;

-- ACTUALIZAMOS EL ESTADO Y EL MUNICIPIO CUANDO RECIBIMOS SOLO LA LOCALIDAD
UPDATE f
SET EstadoDestino = ISNULL(NULLIF(f.EstadoDestino,''), loc.CodigoEstado),
 MunicipioDestino = ISNULL(NULLIF(f.MunicipioDestino,''), loc.CodigoMunicipio)
FROM dbo.FILE_LineaCredito f
INNER JOIN dbo.SICC_Localidad2015 loc ON LTRIM(f.LocalidadDestino) = loc.CodigoCNBV;


DECLARE crs CURSOR FOR
SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente
FROM dbo.FILE_LineaCredito_Homologacion
WHERE Activo=1
ORDER BY Tipo;

OPEN crs;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

WHILE @@FETCH_STATUS = 0
BEGIN
EXEC dbo.SICCMX_Exec_Homologacion 'FILE_LineaCredito', 'FILE_LineaCredito_Homologacion', @Campo, @Tipo, @Fuente;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;
END

CLOSE crs;
DEALLOCATE crs;

UPDATE dbo.FILE_LineaCredito SET Homologado = 1;
GO
