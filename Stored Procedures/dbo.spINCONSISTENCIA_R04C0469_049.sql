SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_049]
AS

BEGIN

-- Validar que el Saldo del Principal al Final del Periodo (dat_saldo_princ_fin) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SaldoFinal
FROM dbo.RW_VW_R04C0469_INC
WHERE CAST(ISNULL(NULLIF(SaldoFinal,''),'-1') AS DECIMAL) < 0;

END


GO
