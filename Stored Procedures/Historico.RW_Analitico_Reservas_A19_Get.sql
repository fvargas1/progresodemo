SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_Reservas_A19_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Fecha,
	CodigoPersona,
	NombrePersona,
	CodigoProyecto,
	NombreProyecto,
	CodigoCredito,
	SaldoCredito,
	FechaVecimiento,
	Moneda,
	MontoGarantia,
	MontoReserva,
	PrctReserva,
	Calificacion
FROM Historico.RW_Analitico_Reservas_A19
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY CodigoPersona, CodigoProyecto, CodigoCredito;
GO
