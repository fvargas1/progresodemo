SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Rel_Group_User_ListUsers]
	@IdGroup BAJAWARE_utID
AS
SELECT
 u.IdUser, u.Active, u.Description, u.Blocked, u.Name, u.Email, u.Telefono, u.Mobil, u.NumeroControl, u.Username, u.Password, u.UsuarioWindows, u.FechaNacimiento, u.UltimaFechaLogin, u.NeverLogedIn
FROM dbo.BAJAWARE_Rel_Group_User rel
LEFT OUTER JOIN BAJAWARE_User u ON rel.IdUser = u.IdUser
WHERE rel.IdGroup = @IdGroup;
GO
