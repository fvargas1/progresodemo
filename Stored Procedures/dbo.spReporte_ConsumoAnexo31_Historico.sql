SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spReporte_ConsumoAnexo31_Historico]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Calificacion AS Codigo,
	Importe,
	Reserva,
	PorcentajeReservas,
	Metodologia AS Nombre,
	ImporteSemanal,
	ImporteQuincenal,
	ReservaSemanal,
	ReservaQuincenal,
	TipoCredito
FROM Historico.RW_Anexo31Consumo2011
WHERE IdPeriodoHistorico = @IdPeriodoHistorico;
GO
