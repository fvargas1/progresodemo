SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_064]

AS



BEGIN



-- El PORCENTAJE DE PAGOS EN TIEMPO CON ENTIDADES FINANCIERAS NO BANCARIAS EN LOS ÚLTIMOS 12 MESES debe tener formato Numérico (10,6)



SELECT

	CodigoPersona AS CodigoDeudor,

	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,

	PorcPagoInstNoBanc

FROM dbo.RW_VW_R04C0470_INC

WHERE LEN(PorcPagoInstNoBanc) > 0 AND PorcPagoInstNoBanc <> '-99'

	AND (CHARINDEX('.',PorcPagoInstNoBanc) = 0 OR CHARINDEX('.',PorcPagoInstNoBanc) > 5 OR LEN(LTRIM(SUBSTRING(PorcPagoInstNoBanc, CHARINDEX('.', PorcPagoInstNoBanc) + 1, LEN(PorcPagoInstNoBanc)))) <> 6);



END
GO
