SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0473_107]
AS

BEGIN

-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de una Localidad (cve_localidad_acreditado).

SELECT
	CodigoCredito,
	Localidad,
	RFC
FROM dbo.RW_VW_R04C0473_INC
WHERE Localidad IN (
	SELECT rep.Localidad
	FROM dbo.RW_VW_R04C0473_INC rep
	INNER JOIN dbo.RW_VW_R04C0473_INC rep2 ON rep.RFC = rep2.RFC AND rep.Localidad <> rep2.Localidad
	GROUP BY rep.Localidad, rep.RFC
);

END


GO
