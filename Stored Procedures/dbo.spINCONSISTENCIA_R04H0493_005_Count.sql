SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0493_005_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- El "TIPO DE BAJA DEL CRÉDITO" debe ser un código valido del catálogo disponible en el SITI.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(r.IdReporteLog)
FROM dbo.RW_R04H0493 r
LEFT OUTER JOIN dbo.SICC_TipoBajaHipotecario cat ON ISNULL(r.TipoBaja,'') = cat.CodigoCNBV
WHERE cat.IdTipoBajaHipotecario IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
