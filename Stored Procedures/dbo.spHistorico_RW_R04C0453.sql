SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_R04C0453]
	@IdPeriodoHistorico INT
AS
DECLARE @IdReporteLog BIGINT;
SET @IdReporteLog = (SELECT MAX(IdReporteLog) FROM dbo.RW_R04C0453);

DELETE FROM Historico.RW_R04C0453 WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.RW_R04C0453 (
	IdPeriodoHistorico, Periodo, Entidad, Formulario, CodigoPersona, RFC, NombrePersona, TipoCartera, ActEconomica, GrupoRiesgo, Localidad,
	Municipio, Estado, IdBuroCredito, LEI, TipoAltaCredito, TipoOperacion, DestinoCredito, CodigoCredito, CodigoCreditoCNBV,
	CodigoGlobalCNBV, MonLineaCred, FecMaxDis, FecVenLin, Moneda, FormaDisposicion, TipoLinea, Posicion, NumEmpLoc,NumEmpFedSHCP,
	NumRPPC, RegGarantiaMob, PorcPartFederal, TasaInteres, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa, PeriodicidadPagosCapital,
	PeriodicidadPagosInteres, NumMesesAmortCap, NumMesesPagoInt, ComisionAperturaTasa, ComisionAperturaMonto, ComisionDisposicionTasa,
	ComisionDisposicionMonto, LocalidadDestinoCredito, MunicipioDestinoCredito, EstadoDestinoCredito, ActividadDestinoCredito
)
SELECT
	@IdPeriodoHistorico,
	Periodo,
	Entidad,
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActEconomica,
	GrupoRiesgo,
	Localidad,
	Municipio,
	Estado,
	IdBuroCredito,
	LEI,
	TipoAltaCredito,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoGlobalCNBV,
	MonLineaCred,
	FecMaxDis,
	FecVenLin,
	Moneda,
	FormaDisposicion,
	TipoLinea,
	Posicion,
	NumEmpLoc,
	NumEmpFedSHCP,
	NumRPPC,
	RegGarantiaMob,
	PorcPartFederal,
	TasaInteres,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	NumMesesAmortCap,
	NumMesesPagoInt,
	ComisionAperturaTasa,
	ComisionAperturaMonto,
	ComisionDisposicionTasa,
	ComisionDisposicionMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM dbo.RW_R04C0453
WHERE IdReporteLog = @IdReporteLog;
GO
