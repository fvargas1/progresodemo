SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Analitico_CNR_Mensual_Generate]  
AS  
DECLARE @IdReporte AS BIGINT;  
DECLARE @IdReporteLog AS BIGINT;  
DECLARE @TotalRegistros AS INT;  
DECLARE @TotalSaldos AS DECIMAL;  
DECLARE @TotalIntereses AS DECIMAL;  
DECLARE @SaldoFinalTotalConsumo DECIMAL;  
DECLARE @FechaMigracion DATETIME;  
  
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE Nombre = '_Analitico_CNR_Mensual' AND GrupoReporte = 'INTERNO';  
  
INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)  
VALUES (@IdReporte, 'Cartera por tipo de crédito moneda extranjera', GETDATE(), 'Bajaware');  
  
SET @IdReporteLog = SCOPE_IDENTITY();  
  
TRUNCATE TABLE dbo.RW_Analitico_CNR_Mensual;  
  
INSERT INTO dbo.RW_Analitico_CNR_Mensual (  
 IdReporteLog, CodigoCredito, CodigoDeudor, Nombre, Periodicidad_Facturacion, TipoCredito, Metodologia, Constante, FactorATR,  
 ATR, FactorVECES, VECES, FactorProPago, ProPago, FactorTipoCredito, [PI], SP_Cubierta, SP_Expuesta, ECubierta, EExpuesta,  
 ETotal, MontoGarantiaTotal, MontoGarantiaUsado, ReservaCubierta, ReservaExpuesta, ReservaTotal, PorcentajeReservaTotal, Calificacion  
)  
SELECT DISTINCT  
 @IdReporteLog,  
 con.Codigo AS CodigoCredito,  
 per.Codigo AS CodigoDeudor,  
 REPLACE(per.Nombre, ',', '') AS Nombre,  
 perPago.Nombre AS Periodicidad_Facturacion,  
 tc.ClasificacionReportes AS TipoCredito,  
 met.Nombre AS Metodologia,  
 ct.Constante AS Constante,  
 ct.ATR AS FactorATR,  
 CAST(pre.ATR AS DECIMAL(18,6)) AS ATR,  
 ct.VECES AS FactorVECES,  
 CAST(pre.VECES AS DECIMAL(18,6)) AS VECES,  
 ct.ProPago AS FactorProPago,  
 CAST(pre.PorPago * 100 AS DECIMAL(18,6)) AS ProPago,  
 ISNULL(ctc.Factor, 0) AS FactorTipoCredito,  
 CAST(crv.[PI] * 100 AS DECIMAL(18,6)) AS [PI],  
 CAST(ISNULL(crv.SPCubierta,0) * 100 AS DECIMAL(18,6)) AS SP_Cubierta,  
 CAST(crv.SPExpuesta * 100 AS DECIMAL(18,6)) AS SP_Expuesta,  
 crv.ECubierta AS ECubierta,  
 crv.EExpuesta AS EExpuesta,  
 crv.E AS ETotal,  
 ISNULL(gar.MontoGarantia,0) AS MontoGarantiaTotal,  
 ISNULL(gar.MontoGarantiaUsado,0) AS MontoGarantiaUsado,  
 crv.ReservaCubierta AS ReservaCubierta,  
 crv.ReservaExpuesta AS ReservaExpuesta,  
 crv.ReservaTotal AS ReservaTotal,  
 CAST(crv.PorReserva * 100 AS DECIMAL(18,6)) AS PorcentajeReservaTotal,  
 cal.Codigo AS Calificacion  
FROM dbo.SICCMX_Consumo con  
INNER JOIN dbo.SICCMX_Persona per ON con.IdPersona = per.IdPersona  
INNER JOIN dbo.SICCMX_Consumo_Reservas_Variables crv ON con.IdConsumo = crv.IdConsumo  
INNER JOIN dbo.SICCMX_Consumo_Reservas_VariablesPreliminares pre ON crv.IdConsumo = pre.IdConsumo  
INNER JOIN dbo.SICCMX_Consumo_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaConsumo  
INNER JOIN dbo.SICCMX_Consumo_Metodologia_Constantes ct ON met.IdMetodologiaConsumo = ct.IdMetodologia  
LEFT OUTER JOIN dbo.SICC_PeriodicidadPagoConsumo perPago ON con.IdPeriodicidadCapital = perPago.IdPeriodicidadPagoConsumo  
LEFT OUTER JOIN dbo.SICC_TipoCreditoConsumo tc ON con.IdTipoCredito = tc.IdTipoCreditoConsumo  
LEFT OUTER JOIN dbo.SICCMX_VW_Garantias_Consumo_Analitico gar ON con.IdConsumo = gar.IdConsumo  
LEFT OUTER JOIN dbo.SICCMX_Consumo_Metodologia_ConstantesPorTipoCredito ctc ON ctc.IdMetodologia = ct.IdMetodologia AND ctc.IdTipoCredito = con.IdTipoCredito  
LEFT OUTER JOIN dbo.SICC_CalificacionConsumo2011 cal ON crv.IdCalificacion = cal.IdCalificacion  
WHERE met.Codigo = '3';  
  
  
SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Analitico_CNR_Mensual WHERE IdReporteLog = @IdReporteLog;  
SELECT @TotalSaldos = 0;  
SELECT @TotalIntereses = 0;  
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;  
  
UPDATE dbo.RW_ReporteLog  
SET TotalRegistros = @TotalRegistros,  
 TotalSaldos= @TotalSaldos,  
 TotalIntereses = @TotalIntereses,  
 FechaCalculoProcesos = GETDATE(),  
 FechaImportacionDatos = @FechaMigracion,  
 IdFuenteDatos = 1  
WHERE IdReporteLog = @IdReporteLog;   
GO
