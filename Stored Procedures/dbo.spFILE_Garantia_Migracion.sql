SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Garantia_Migracion]
AS

UPDATE gar
SET
 IdTipoGarantiaMA = tipoGarMA.IdTipoGarantia,
 IdTipoGarantia = tipoGar.IdTipoGarantia,
 ValorGarantia = NULLIF(f.ValorGarantia,''),
 IdMoneda = mon.IdMoneda,
 FechaValuacion = NULLIF(f.FechaValuacion,''),
 Descripcion = LTRIM(RTRIM(f.Descripcion)),
 IdBancoGarantia = ins.IdInstitucion,
 ValorGarantiaProyectado = NULLIF(f.ValorGarantiaProyectado,''),
 RegGarantiaMob = LTRIM(RTRIM(f.RegGarantiaMob)),
 Hc = NULLIF(f.Hc,''),
 VencimientoRestante = NULLIF(f.VencimientoRestante,''),
 IdGradoRiesgo = NULLIF(f.GradoRiesgo,''),
 IdAgenciaCalificadora = agen.IdAgencia,
 Calificacion = LTRIM(RTRIM(f.Calificacion)),
 IdEmisor = emi.IdEmisor,
 IdEscala = esc.IdEscala,
 EsIPC = NULLIF(f.EsIPC,''),
 PIGarante = NULLIF(f.PIGarante,''),
 NumRPPC = LTRIM(RTRIM(f.NumRPPC)),
 RFCGarante = LTRIM(RTRIM(f.RFCGarante)),
 NombreGarante = LTRIM(RTRIM(f.NombreGarante)),
 IdGarante = LTRIM(RTRIM(f.IdGarante)),
 IdActividaEcoGarante = actEc.IdActividadEconomica,
 IdLocalidadGarante = mun.IdLocalidad,
 MunicipioGarante = mun.CodigoMunicipio,
 EstadoGarante = mun.CodigoEstado,
 IdTipoGarante = tpoGte.IdTipoGarante,
 LEI = LTRIM(RTRIM(f.LEI)),
 IndPerMorales = NULLIF(f.IndPerMorales,''),
 IdPersona = per.IdPersona,
 CodigoPostalGarante = LTRIM(RTRIM(f.CodigoPostalGarante))
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.FILE_Garantia f ON gar.Codigo = LTRIM(f.CodigoGarantia)
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA tipoGarMA ON LTRIM(f.TipoGarantiaMA) = tipoGarMA.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarantia tipoGar ON LTRIM(f.TipoGarantia) = tipoGar.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_Institucion ins ON LTRIM(f.BancoGarantia) = ins.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON LTRIM(f.AgenciaCalificadora) = agen.Codigo
LEFT OUTER JOIN dbo.SICC_Emisor emi ON LTRIM(f.Emisor) = emi.Codigo
LEFT OUTER JOIN dbo.SICC_Escala esc ON LTRIM(f.Escala) = esc.Codigo
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEc ON LTRIM(f.ActividaEcoGarante) = actEc.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarante tpoGte ON LTRIM(f.TipoGarante) = tpoGte.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
OUTER APPLY (SELECT TOP 1 IdLocalidad, CodigoEstado, CodigoMunicipio FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadGarante)) AS mun
WHERE ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


INSERT INTO dbo.SICCMX_Garantia(
 Codigo,
 IdTipoGarantiaMA,
 IdTipoGarantia,
 ValorGarantia,
 IdMoneda,
 FechaValuacion,
 Descripcion,
 IdBancoGarantia,
 ValorGarantiaProyectado,
 RegGarantiaMob,
 Hc,
 VencimientoRestante,
 IdGradoRiesgo,
 IdAgenciaCalificadora,
 Calificacion,
 IdEmisor,
 IdEscala,
 EsIPC,
 PIGarante,
 NumRPPC,
 RFCGarante,
 NombreGarante,
 IdGarante,
 IdActividaEcoGarante,
 IdLocalidadGarante,
 MunicipioGarante,
 EstadoGarante,
 IdTipoGarante,
 LEI,
 IndPerMorales,
 IdPersona,
 CodigoPostalGarante
)
SELECT
 LTRIM(RTRIM(f.CodigoGarantia)),
 tipoGarMA.IdTipoGarantia,
 tipoGar.IdTipoGarantia,
 NULLIF(f.ValorGarantia,''),
 mon.IdMoneda,
 NULLIF(f.FechaValuacion,''),
 LTRIM(RTRIM(f.Descripcion)),
 ins.IdInstitucion,
 NULLIF(f.ValorGarantiaProyectado,''),
 LTRIM(RTRIM(f.RegGarantiaMob)),
 NULLIF(f.Hc,''),
 NULLIF(f.VencimientoRestante,''),
 NULLIF(f.GradoRiesgo,''),
 agen.IdAgencia,
 LTRIM(RTRIM(f.Calificacion)),
 emi.IdEmisor,
 esc.IdEscala,
 NULLIF(f.EsIPC,''),
 NULLIF(f.PIGarante,''),
 LTRIM(RTRIM(f.NumRPPC)),
 LTRIM(RTRIM(f.RFCGarante)),
 LTRIM(RTRIM(f.NombreGarante)),
 LTRIM(RTRIM(f.IdGarante)),
 actEc.IdActividadEconomica,
 mun.IdLocalidad,
 mun.CodigoMunicipio,
 mun.CodigoEstado,
 tpoGte.IdTipoGarante,
 LTRIM(RTRIM(f.LEI)),
 NULLIF(f.IndPerMorales,''),
 per.IdPersona,
 LTRIM(RTRIM(f.CodigoPostalGarante))
FROM dbo.FILE_Garantia f
LEFT OUTER JOIN dbo.SICC_TipoGarantiaMA tipoGarMA ON LTRIM(f.TipoGarantiaMA) = tipoGarMA.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarantia tipoGar ON LTRIM(f.TipoGarantia) = tipoGar.Codigo
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_Institucion ins ON LTRIM(f.BancoGarantia) = ins.Codigo
LEFT OUTER JOIN dbo.SICC_AgenciaCalificadora agen ON LTRIM(f.AgenciaCalificadora) = agen.Codigo
LEFT OUTER JOIN dbo.SICC_Emisor emi ON LTRIM(f.Emisor) = emi.Codigo
LEFT OUTER JOIN dbo.SICC_Escala esc ON LTRIM(f.Escala) = esc.Codigo
LEFT OUTER JOIN dbo.SICC_ActividadEconomica actEc ON LTRIM(f.ActividaEcoGarante) = actEc.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarante tpoGte ON LTRIM(f.TipoGarante) = tpoGte.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
OUTER APPLY (SELECT TOP 1 IdLocalidad, CodigoEstado, CodigoMunicipio FROM dbo.SICC_Localidad2015 WHERE CodigoCNBV = LTRIM(f.LocalidadGarante)) AS mun
LEFT OUTER JOIN dbo.SICCMX_Garantia gar ON LTRIM(f.CodigoGarantia) = gar.Codigo
WHERE gar.Codigo IS NULL AND ISNULL(f.errorCatalogo,0) <> 1 AND ISNULL(f.errorFormato,0) <> 1;


-- ACTUALIZAMOS PI DEFAULT PARA GARANTIAS DE PP Y PM
UPDATE gar
SET PIGarante = 0.01111111
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICC_TipoGarantia tg ON gar.IdTipoGarantia = tg.IdTipoGarantia AND tg.Codigo IN ('NMC-04','NMC-05','NMC-04-C')
WHERE gar.PIGarante IS NULL;
GO
