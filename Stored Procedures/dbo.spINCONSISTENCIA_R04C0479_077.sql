SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_077]
AS

BEGIN

-- Validar que la Moneda corresponda a Catalogo de CNBV

SELECT
	rep.CodigoCredito,
	rep.NumeroDisposicion,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.Moneda
FROM dbo.RW_VW_R04C0479_INC rep
LEFT OUTER JOIN dbo.SICC_Moneda moneda ON ISNULL(rep.Moneda,'') = moneda.CodigoCNBV
WHERE moneda.IdMoneda IS NULL;

END


GO
