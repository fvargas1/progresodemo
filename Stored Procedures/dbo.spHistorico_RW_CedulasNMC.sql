SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_RW_CedulasNMC]
	 @IdPeriodoHistorico INT
AS
EXEC dbo.spHistorico_RW_CedulaNMC_Info @IdPeriodoHistorico;
EXEC dbo.spHistorico_RW_CedulaNMC_Creditos @IdPeriodoHistorico;
EXEC dbo.spHistorico_RW_CedulaNMC_Canasta @IdPeriodoHistorico;
GO
