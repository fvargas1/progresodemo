SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_037]
AS

BEGIN

-- Se deberá validar la correspondencia para el monto en "Valor de la Vivienda al Momento de la Originación" y la clave de "Tipo de Alta del Crédito":
-- - Cuando el "Valor del Inmueble según Avaluó" es mayor a 0, el "Tipo de Alta del Crédito" debe ser 2, 5, 6 o 10.

SELECT CodigoCredito, CodigoCreditoCNBV, ValorAvaluo, TipoAlta
FROM dbo.RW_R04H0491
WHERE CAST(ISNULL(NULLIF(ValorAvaluo,''),'0') AS DECIMAL) > 0 AND TipoAlta NOT IN ('2','5','6','10');

END
GO
