SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_AjusteSP_Expuesta]  
AS  
-- CALCULAMOS SEVERIDAD DE LA PERDIDA  
UPDATE crv  
SET SP_Expuesta = sp.SP  
FROM dbo.SICCMX_Credito_Reservas_Variables crv  
INNER JOIN dbo.SICCMX_VW_Credito_SP sp ON crv.IdCredito = sp.IdCredito;  
  
  
INSERT INTO dbo.SICCMX_Credito_Reservas_Variables_Log (IdCredito, Fecha, Usuario, Comentarios)  
SELECT DISTINCT  
 IdCredito,  
 GETDATE(),  
 '',  
 'Se ajusta SP Expuesta a ' + CONVERT(VARCHAR, CONVERT(DECIMAL(10,2), ISNULL(SP_Expuesta*100, 0)))  
FROM dbo.SICCMX_Credito_Reservas_Variables;
GO
