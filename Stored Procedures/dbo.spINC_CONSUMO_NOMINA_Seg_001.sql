SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_NOMINA_Seg_001]
AS

BEGIN

-- El Tipo de Reestructura de ser 50 en el caso de créditos originales (que no provienen de una reestructura);
-- 300 en el caso de créditos de Nómina que ya fueron objeto de una reestructura y continúan siendo de Nómina.

SELECT DISTINCT
	rep.FolioCredito,
	rep.Reestructura
FROM dbo.RW_Consumo_NOMINA rep
LEFT OUTER JOIN dbo.SICC_ReestructuraConsumo cat ON rep.Reestructura = cat.CodigoBanxico AND ISNULL(cat.ClasificacionReporte,'NOMINA') = 'NOMINA'
WHERE cat.IdReestructuraConsumo IS NULL;

END
GO
