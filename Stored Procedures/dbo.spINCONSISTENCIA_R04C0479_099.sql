SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0479_099]
AS

BEGIN

-- Si las Reservas por Parte del Acreditado (dat_reser_cubier_gtia_pers) son MAYORES o IGUALES a 0,
-- validar que el valor absoluto de la siguiente operación sea menor a 1000, cuando se trate de un crédito sin
-- fuente de pago propia (cve_proy_pago_anexo_19 = 2) :
-- ABS(dat_reser_no_cubier_gtia_pers - dat_sever_no_cubier_persona/100 * dat_exp_no_cubier_persona * dat_p_i_no_cubier_persona/100) <1000

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	ProyectoInversion,
	ReservaExpuesta,
	PIExpuesta,
	SPExpuesta,
	EIExpuesta
FROM dbo.RW_R04C0479
WHERE CAST(ReservaExpuesta AS DECIMAL) >= 0 AND ISNULL(ProyectoInversion,'') = '2'
	AND ABS(CAST(ReservaExpuesta AS DECIMAL) -
	CAST((CAST(PIExpuesta AS DECIMAL(12,8))/100.00) * (CAST(SPExpuesta AS DECIMAL(12,8))/100.00) * CAST(EIExpuesta AS DECIMAL) AS DECIMAL)) >= 1000;

END
GO
