SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_006_Count]

	@IdInconsistencia BIGINT

AS

BEGIN



-- Si el acreditado es persona moral para el RFC, se validará que:

-- - La primera posición sea: "_" (guión bajo).

-- - Las siguientes 3 posiciones son alfanuméricos.

-- - Las siguientes 6 posiciones deben ser números (año, mes y día válidos).

-- - Las últimas 3 posiciones son alfanuméricos.



DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;



DECLARE @count INT;



SELECT @count = COUNT(CodigoCredito)

FROM dbo.RW_R04C0442

WHERE PersonalidadJuridica = '2' AND (SUBSTRING(RFC,1,1) <> '_' OR 
SUBSTRING(RFC,2,3) LIKE '%[^A-Z0-9]%'   AND SUBSTRING(RFC,2,3) NOT LIKE '%&%' OR 
SUBSTRING(RFC,5,6) LIKE '%[^0-9]%' AND SUBSTRING(RFC,5,6) NOT LIKE '%&%' OR
SUBSTRING(RFC,11,3) LIKE '%[^A-Z0-9]%' AND SUBSTRING(RFC,11,3) NOT LIKE '%&%' );

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());



SELECT @count;



END
GO
