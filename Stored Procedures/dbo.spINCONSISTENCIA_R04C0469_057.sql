SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_057]
AS

BEGIN

-- Validar que la Probabilidad de Incumplimiento Total (dat_probabilidad_incump) se encuentre en 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	PITotal AS [PI]
FROM dbo.RW_R04C0469
WHERE CAST(ISNULL(NULLIF(PITotal,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
