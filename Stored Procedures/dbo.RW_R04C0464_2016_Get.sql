SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0464_2016_Get]
	@IdReporteLog BIGINT
AS
SELECT
	Formulario,
	CodigoCreditoCNBV,
	ClasContable,
	ConcursoMercantil,
	FechaDisposicion,
	FechaVencDisposicion,
	Moneda,
	NumeroDisposicion,
	NombreFactorado,
	RFC_Factorado,
	SaldoInicial,
	TasaInteres,
	TasaInteresDisp,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	MontoDispuesto,
	MontoPagoExigible,
	MontoCapitalPagado,
	MontoInteresPagado,
	MontoComisionPagado,
	MontoInteresMoratorio,
	MontoTotalPagado,
	MontoCondonacion,
	MontoQuitas,
	MontoBonificado,
	MontoDescuentos,
	MontoAumentosDec,
	SaldoFinal,
	SaldoCalculoInteres,
	DiasCalculoInteres,
	MontoInteresAplicar,
	SaldoInsoluto,
	SituacionCredito,
	DiasAtraso,
	FechaUltimoPago,
	MontoBancaDesarrollo,
	InstitucionFondeo
FROM dbo.RW_VW_R04C0464_2016;
GO
