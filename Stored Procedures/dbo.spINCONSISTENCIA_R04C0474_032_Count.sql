SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_032_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- La Responsabilidad Total debe ser mayor o igual al Saldo del Principal al Final del Periodo

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0474_INC
WHERE CAST(ISNULL(NULLIF(SaldoInsoluto,''),'0') AS DECIMAL) < CAST(ISNULL(NULLIF(SaldoFinal,''),'0') AS DECIMAL);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
