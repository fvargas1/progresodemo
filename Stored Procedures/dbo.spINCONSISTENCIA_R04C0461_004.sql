SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0461_004]
AS
BEGIN
-- El ID de Crédito Metodología CNBV debe de ser de 29 Posiciones

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	CodigoCreditoCNBV
FROM dbo.RW_VW_R04C0461_INC
WHERE LEN(ISNULL(CodigoCreditoCNBV,'')) <> 29;

END

GO
