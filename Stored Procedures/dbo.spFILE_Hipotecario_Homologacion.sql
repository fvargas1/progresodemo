SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Hipotecario_Homologacion]
AS
DECLARE @Campo VARCHAR(50);
DECLARE @Tipo INT;
DECLARE @Fuente VARCHAR(50);

-- AGREGAMOS "0" A MUNICIPIO CUANDO ESTE TIENE SOLO 4 POSICIONES
UPDATE dbo.FILE_Hipotecario SET Municipio = '0' + LTRIM(RTRIM(Municipio)) WHERE LEN(LTRIM(RTRIM(Municipio))) = 4;

-- EL NUMERO DE AVALUO DEBE SER DE 17 POSICIONES
UPDATE dbo.FILE_Hipotecario SET NumeroAvaluo = RIGHT('00000000000000000' + LTRIM(RTRIM(NumeroAvaluo)), 17) WHERE LEN(LTRIM(NumeroAvaluo)) < 17;

-- AGREGAMOS "0" A TIPO DE CREDITO R04A CUANDO ESTE TIENE SOLO 4 POSICIONES
UPDATE dbo.FILE_Hipotecario SET TipoCreditoR04A = '0' + LTRIM(RTRIM(TipoCreditoR04A)) WHERE LEN(TipoCreditoR04A) < 6;
UPDATE dbo.FILE_Hipotecario SET TipoCreditoR04A_2016 = '0' + LTRIM(RTRIM(TipoCreditoR04A_2016)) WHERE LEN(TipoCreditoR04A_2016) < 6;

DECLARE crs CURSOR FOR
SELECT DISTINCT Campo, ISNULL(Tipo,1) AS Tipo, Fuente
FROM dbo.FILE_Hipotecario_Homologacion
WHERE Activo=1
ORDER BY Tipo;

OPEN crs;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;

WHILE @@FETCH_STATUS = 0
BEGIN
EXEC dbo.SICCMX_Exec_Homologacion 'FILE_Hipotecario', 'FILE_Hipotecario_Homologacion', @Campo, @Tipo, @Fuente;

FETCH NEXT FROM crs INTO @Campo, @Tipo, @Fuente;
END

CLOSE crs;
DEALLOCATE crs;

UPDATE dbo.FILE_Hipotecario SET Homologado = 1;
GO
