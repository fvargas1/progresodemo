SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0470_041]
AS

BEGIN

-- Si los Días Atrasados Infonavit en el Último Bimestre (dat_dias_mora_infonavit) es < 0,
-- entonces el Puntaje Asignado por Días Atrasados Infonavit en el Último Bimestre (cve_ptaje_dias_atraso_infonav) debe ser = 72

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	DiasAtrInfonavit,
	P_DiasAtrInfonavit AS Puntos_DiasAtrInfonavit
FROM dbo.RW_VW_R04C0470_INC
WHERE CAST(DiasAtrInfonavit AS DECIMAL(10,6)) < 0 AND CAST(DiasAtrInfonavit AS DECIMAL(10,6)) <> -999 AND ISNULL(P_DiasAtrInfonavit,'') <> '72';

END


GO
