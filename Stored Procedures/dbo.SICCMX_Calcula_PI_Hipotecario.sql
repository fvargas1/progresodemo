SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_Calcula_PI_Hipotecario]
AS
DECLARE @IdMetodologia INT;
SELECT @IdMetodologia = IdMetodologiaHipotecario FROM dbo.SICCMX_Hipotecario_Metodologia WHERE Codigo = '1';


-- Calculamos la PI cuando el ATR es mayor a lo maximo para cada Metodologia
INSERT INTO dbo.SICCMX_Hipotecario_Reservas_Variables (IdHipotecario, [PI], E)
SELECT
	vp.IdHipotecario,
	1,
	hip.SaldoTotalValorizado
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR >= const.ATRPI
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON hip.IdHipotecario = vp.IdHipotecario
WHERE vp.IdMetodologia = @IdMetodologia;


-- Calculamos la PI cuando el ATR es menor a lo maximo para cada Metodologia
INSERT INTO dbo.SICCMX_Hipotecario_Reservas_Variables (IdHipotecario, [PI], E)
SELECT
	vp.IdHipotecario,
	1 /
	-- Esto es lo que se tiene que cambiar para cada Metodologia
	( 1 + EXP( -1 * ( const.Constante + (const.ATR * vp.ATR) + (const.MAXATR * vp.MAXATR) +
	(const.PorVPAGO * vp.PorPago) + (const.PorCLTV * vp.PorCLTVi) + (const.INTEXP * vp.INTEXP) + (const.MON * vp.MON )))),
	hip.SaldoTotalValorizado
FROM dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares vp
INNER JOIN dbo.SICCMX_Hipotecario_Metodologia_Constantes const ON const.IdMetodologia = vp.IdMetodologia AND vp.ATR < const.ATRPI
INNER JOIN dbo.SICCMX_VW_Hipotecario hip ON hip.IdHipotecario = vp.IdHipotecario
WHERE const.IdMetodologia = @IdMetodologia;
GO
