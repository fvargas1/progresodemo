SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_072]
AS

BEGIN

-- La clave de Estado es igual a cero (0) si y solo si "Tipo de Alta del Crédito" igual 3 (columna 9).

SELECT CodigoCredito, CodigoCreditoCNBV, Estado, TipoAlta
FROM dbo.RW_R04H0491
WHERE Estado = '0' AND TipoAlta <> '3';

END
GO
