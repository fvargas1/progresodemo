SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_Califica_ListErrores]
AS
SELECT
 CodigoDeudor,
 BK12_CLEAN,
 BK12_NUM_CRED,
 BK12_NUM_TC_ACT,
 NBK12_NUM_CRED,
 BK12_NUM_EXP_PAIDONTIME,
 BK12_PCT_PROMT,
 NBK12_PCT_PROMT,
 BK12_PCT_SAT,
 NBK12_PCT_SAT,
 BK24_PCT_60PLUS,
 NBK24_PCT_60PLUS,
 NBK12_COMM_PCT_PLUS,
 BK12_PCT_90PLUS,
 BK12_DPD_PROM,
 BK12_IND_QCRA,
 BK12_MAX_CREDIT_AMT,
 MONTHS_ON_FILE_BANKING,
 MONTHS_SINCE_LAST_OPEN_BANKING,
 BK_IND_PMOR,
 BK24_IND_EXP,
 12_INST,
 BK_DEUDA_TOT,
 BK_DEUDA_CP,
 NBK_DEUDA_TOT,
 NBK_DEUDA_CP,
 DEUDA_TOT,
 DEUDA_TOT_CP,
 FECHA_BURO,
 Fuente
FROM dbo.FILE_Califica
WHERE errorCatalogo = 1 OR errorFormato = 1;
GO
