SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_054]
AS

BEGIN

-- Validar que el Número de Meses de Gracia para el Pago de Intereses (dat_num_meses_gracia_interes) sea MAYOR O IGUAL a cero.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	NumMesesPagoInt
FROM dbo.RW_VW_R04C0478_INC
WHERE CAST(ISNULL(NULLIF(NumMesesPagoInt,''),'-1') AS DECIMAL) < 0;

END


GO
