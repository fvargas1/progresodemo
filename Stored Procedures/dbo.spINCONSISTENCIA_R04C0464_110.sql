SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0464_110]

AS



BEGIN



-- Si la clave Revocable (cve_revocable=1) en el formulario de ALTAS es igual a 1, entonces en el formulario de SEGUIMIENTO

-- la Exposición al Incumplimiento (dat_exposición_incumplimiento) debe ser igual a la Responsabilidad Total (dat_responsabilidad_total).



SELECT

	rep.CodigoCredito,

	rep.NumeroDisposicion,

	vw.TipoLinea,

	rep.EITotal,

	rep.SaldoInsoluto

FROM dbo.RW_VW_R04C0464_INC rep

INNER JOIN dbo.SICCMX_VW_Datos_Reportes_Altas vw ON rep.CodigoCreditoCNBV = vw.CNBV

WHERE ISNULL(vw.TipoLinea,'') = '1' AND ISNULL(rep.EITotal,'') <> '1';



END
GO
