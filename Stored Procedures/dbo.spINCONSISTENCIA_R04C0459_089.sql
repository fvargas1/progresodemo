SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0459_089]
AS
BEGIN
-- Si la Situación del Crédito (cve_situacion_credito) es igual a 1, entonces validar que la Fecha de Vencimiento
-- de la Disposición (dat_fecha_venc_disp_credito) no sea MENOR a tres meses inmediatos anteriores al periodo (cve_periodo) que se reporta.

DECLARE @FechaPeriodo VARCHAR(6);

SELECT @FechaPeriodo = SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(DATEADD(MONTH, -3, Fecha),0),102),'.',''),1,6) FROM dbo.SICC_Periodo WHERE Activo=1;

SELECT
	CodigoCredito,
	NumeroDisposicion,
	SituacionCredito,
	FechaVencDisposicion
FROM dbo.RW_VW_R04C0459_INC
WHERE ISNULL(SituacionCredito,'') = '1' AND ISNULL(FechaVencDisposicion,'') < @FechaPeriodo;

END


GO
