SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_082_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el RFC (dat_rfc) inicia con CNBV, CNBX o CNBF, validar que el Tipo de Cartera (cve_tipo_cartera) sea igual a 140, 141, 142, 270, 299, 330, 340, 410, 420, 540, 550 ó 560.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE SUBSTRING(ISNULL(RFC,''),1,4) IN ('CNBV','CNBX','CNBF') AND ISNULL(TipoCartera,'') NOT IN ('140','141','142','270','299','330','340','410','420','540','550','560')

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
