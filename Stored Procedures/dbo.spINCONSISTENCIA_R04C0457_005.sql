SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0457_005]
AS
BEGIN
-- Si el Tipo de Baja (cve_tipo_baja_credito) es = 139, validar que el mismo ID Metodología CNBV
-- (dat_id_credito_met_cnbv) se encuentre en el subreporte 453 con Tipo de Alta (cve_tipo_alta_credito) = 137.

SELECT
	rep.CodigoCredito,
	rep.TipoBaja,
	hist.TipoAltaCredito
FROM dbo.RW_VW_R04C0457_INC rep
LEFT OUTER JOIN Historico.RW_VW_R04C0453_INC hist ON rep.CodigoCreditoCNBV = hist.CodigoCreditoCNBV
WHERE rep.TipoBaja = '139' AND hist.TipoAltaCredito <> '137';

END
GO
