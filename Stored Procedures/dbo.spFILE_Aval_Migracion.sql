SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Aval_Migracion]
AS

UPDATE aval
SET
 Nombre = LTRIM(RTRIM(f.NombreAval)),
 RFC = LTRIM(RTRIM(f.RFC)),
 Domicilio = LTRIM(RTRIM(f.Domicilio)),
 IdPersonalidadJuridica = tpoPer.IdTipoPersona,
 IdTipoAval = tipoAv.IdTipoGarante,
 IdMunAval = mun.IdLocalidad,
 PIAval = NULLIF(f.PIAval,''),
 IdMoneda = mon.IdMoneda,
 Calif_Fitch = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Fitch,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 Calif_Moodys = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Moodys,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 Calif_SP = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_SP,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 Calif_HRRATINGS = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_HRRATINGS,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 Calif_Otras = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Otras,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 IdActividadEconomica = act.IdActividadEconomica,
 LEI = LTRIM(RTRIM(f.LEI)),
 TipoCobertura = ISNULL(cob.IdTipoCobertura,1),
 FiguraGarantiza = figGar.IdFigura,
 IdPersona = per.IdPersona,
 Localidad = LTRIM(RTRIM(f.Localidad)),
 Estado = mun.CodigoEstado,
 CodigoPostal = LTRIM(RTRIM(f.CodigoPostal))
FROM dbo.SICCMX_Aval aval
INNER JOIN dbo.FILE_Aval f ON aval.Codigo = f.CodigoAval
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON LTRIM(f.PersonalidadJuridica) = tpoPer.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarante tipoAv ON LTRIM(f.TipoAval) = tipoAv.Codigo
OUTER APPLY (SELECT TOP 1 m.IdLocalidad, m.CodigoEstado FROM dbo.SICC_Localidad2015 m WHERE m.CodigoCNBV = LTRIM(f.Localidad)) AS mun
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividadEconomica) = act.Codigo
LEFT OUTER JOIN dbo.SICC_TipoCobertura cob ON LTRIM(f.TipoCobertura) = cob.Codigo
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza figGar ON LTRIM(f.FiguraGarantiza) = figGar.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
WHERE f.errorCatalogo IS NULL AND f.errorFormato IS NULL;

INSERT INTO dbo.SICCMX_Aval( 
 Codigo,
 Nombre,
 RFC,
 Domicilio,
 IdPersonalidadJuridica,
 IdTipoAval,
 IdMunAval,
 PIAval,
 IdMoneda,
 Calif_Fitch,
 Calif_Moodys,
 Calif_SP,
 Calif_HRRATINGS,
 Calif_Otras,
 IdActividadEconomica,
 LEI,
 TipoCobertura,
 FiguraGarantiza,
 IdPersona,
 Localidad,
 Estado,
 CodigoPostal
)
SELECT
 LTRIM(RTRIM(f.CodigoAval)),
 LTRIM(RTRIM(f.NombreAval)),
 LTRIM(RTRIM(f.RFC)),
 LTRIM(RTRIM(f.Domicilio)),
 tpoPer.IdTipoPersona,
 tipoAv.IdTipoGarante,
 mun.IdLocalidad,
 NULLIF(f.PIAval,''),
 mon.IdMoneda,
 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Fitch,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Moodys,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_SP,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_HRRATINGS,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(f.Calif_Otras,' ',''),'(mex)',''),'.',''),'mx',''),'HR',''),
 act.IdActividadEconomica,
 LTRIM(RTRIM(f.LEI)),
 ISNULL(cob.IdTipoCobertura,1),
 figGar.IdFigura,
 per.IdPersona,
 LTRIM(RTRIM(f.Localidad)),
 mun.CodigoEstado,
 LTRIM(RTRIM(f.CodigoPostal))
FROM dbo.FILE_Aval f
LEFT OUTER JOIN dbo.SICC_TipoPersona tpoPer ON LTRIM(f.PersonalidadJuridica) = tpoPer.Codigo
LEFT OUTER JOIN dbo.SICC_TipoGarante tipoAv ON LTRIM(f.TipoAval) = tipoAv.Codigo
OUTER APPLY (SELECT TOP 1 m.IdLocalidad, m.CodigoEstado FROM dbo.SICC_Localidad2015 m WHERE m.CodigoCNBV = LTRIM(f.Localidad)) AS mun
LEFT OUTER JOIN dbo.SICC_Moneda mon ON LTRIM(f.Moneda) = mon.Codigo
LEFT OUTER JOIN dbo.SICC_ActividadEconomica act ON LTRIM(f.ActividadEconomica) = act.Codigo
LEFT OUTER JOIN dbo.SICC_TipoCobertura cob ON LTRIM(f.TipoCobertura) = cob.Codigo
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza figGar ON LTRIM(f.FiguraGarantiza) = figGar.Codigo
LEFT OUTER JOIN dbo.SICCMX_Persona per ON LTRIM(f.CodigoCliente) = per.Codigo
LEFT OUTER JOIN dbo.SICCMX_Aval aval ON aval.Codigo=f.CodigoAval
WHERE aval.Codigo IS NULL AND f.errorCatalogo IS NULL AND f.errorFormato IS NULL;
GO
