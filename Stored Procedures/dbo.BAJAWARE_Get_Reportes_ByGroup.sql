SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[BAJAWARE_Get_Reportes_ByGroup]
	@IdGroup INT
AS
SELECT DISTINCT
	rep.IdReporte,
	rep.GrupoReporte,
	rep.Nombre,
	rep.Descripcion,
	CASE WHEN rel.IdRel IS NULL THEN 'No configurado' ELSE 'Configurado' END AS Configurado
FROM dbo.RW_Reporte rep
INNER JOIN dbo.RW_ReporteCampos cam ON rep.IdReporte = cam.IdReporte
LEFT OUTER JOIN dbo.RW_Rel_Grupo_ReporteCampos rel ON cam.IdReporteCampos = rel.IdReporteCampos AND rel.IdGroup = @IdGroup
WHERE rep.Activo = 1
ORDER BY rep.GrupoReporte, rep.Nombre;
GO
