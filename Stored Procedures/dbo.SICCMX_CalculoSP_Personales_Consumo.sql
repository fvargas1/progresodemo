SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_CalculoSP_Personales_Consumo]
AS
-- CALCULAMOS SP PARA GARANTIAS PERSONALES
UPDATE can
SET SeveridadCorresp = tg.SP
FROM dbo.SICCMX_Garantia_Canasta_Consumo can
INNER JOIN dbo.SICCMX_ConsumoAval ca ON can.IdConsumo = ca.IdConsumo AND ca.Aplica = 1
INNER JOIN dbo.SICCMX_Aval a ON ca.IdAval = a.IdAval
INNER JOIN dbo.SICC_TipoGarante tg ON a.IdTipoAval = tg.IdTipoGarante
INNER JOIN dbo.SICC_TipoGarantia tgtia ON can.IdTipoGarantia = tgtia.IdTipoGarantia
WHERE tgtia.Codigo='GP';
GO
