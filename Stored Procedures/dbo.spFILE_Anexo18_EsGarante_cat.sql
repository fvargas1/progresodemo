SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Anexo18_EsGarante_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Anexo18_EsGarante_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Anexo18 f
LEFT OUTER JOIN dbo.SICC_EsGarante cat ON LTRIM(f.EsGarante) = cat.Codigo
WHERE cat.IdEsGarante IS NULL AND LEN(f.EsGarante) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Anexo18_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCliente,
 'EsGarante',
 f.EsGarante,
 2,
 @Detalle
FROM dbo.FILE_Anexo18 f
LEFT OUTER JOIN dbo.SICC_EsGarante cat ON LTRIM(f.EsGarante) = cat.Codigo
WHERE cat.IdEsGarante IS NULL AND LEN(f.EsGarante) > 0;
GO
