SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_SICCMX_Hipotecario_Reservas_VariablesPreliminares]
	@IdPeriodoHistorico INT
AS

DELETE FROM Historico.SICCMX_Hipotecario_Reservas_VariablesPreliminares WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.SICCMX_Hipotecario_Reservas_VariablesPreliminares (
	IdPeriodoHistorico,
	Hipotecario,
	Metodologia,
	ValorVivienda,
	INTEXP,
	SUBCVi,
	SDESi,
	Convenio,
	PorCOBPAMED,
	PorCOBPP,
	Estado,
	Municipio,
	DiasAtraso,
	Veces,
	ATR,
	MaxATR,
	VPago,
	PorPago,
	PorCLTVi,
	PPagoIM,
	TRi,
	MON,
	RAi
)
SELECT
	@IdPeriodoHistorico AS IdPeriodoHistorico,
	hip.Codigo,
	met.Codigo,
	pre.ValorVivienda,
	pre.INTEXP,
	pre.SUBCVi,
	pre.SDESi,
	conv.Codigo,
	pre.PorCOBPAMED,
	pre.PorCOBPP,
	pre.Estado,
	pre.Municipio,
	pre.DiasAtraso,
	pre.Veces,
	pre.ATR,
	pre.MaxATR,
	pre.VPago,
	pre.PorPago,
	pre.PorCLTVi,
	pre.PPagoIM,
	pre.TRi,
	pre.MON,
	pre.RAi
FROM dbo.SICCMX_Hipotecario hip
INNER JOIN dbo.SICCMX_Hipotecario_Reservas_VariablesPreliminares pre ON hip.IdHipotecario = pre.IdHipotecario
LEFT OUTER JOIN dbo.SICCMX_Hipotecario_Metodologia met ON pre.IdMetodologia = met.IdMetodologiaHipotecario
LEFT OUTER JOIN dbo.SICC_ConvenioJudicial conv ON pre.IdConvenio = conv.IdConvenioJudicial;
GO
