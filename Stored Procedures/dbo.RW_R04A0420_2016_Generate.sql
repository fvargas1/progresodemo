SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04A0420_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @SaldoFinalTotalConsumo DECIMAL;
DECLARE @FechaMigracion DATETIME;

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'A-0420_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion)
VALUES (@IdReporte, 'Movimientos en la cartera vencida', GETDATE(), 'Bajaware');

SET @IdReporteLog = SCOPE_IDENTITY();

TRUNCATE TABLE dbo.RW_R04A0420_2016;

INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 con.Concepto,
 '420',
 CASE
 WHEN dat.Moneda = '0' THEN '14'
 WHEN dat.Moneda IN ('20','200') THEN '9'
 ELSE '4' END,
 '5', -- Tipo de Cartera
 '1', -- Tipo de saldo
 SUM(CAST(con.Monto AS DECIMAL))
FROM R04.[0420Conceptos_2016] con
INNER JOIN R04.[0420Datos_2016] dat ON dat.Codigo = con.Codigo
GROUP BY con.Concepto, CASE
 WHEN dat.Moneda = '0' THEN '14'
 WHEN dat.Moneda IN ('20','200') THEN '9'
 ELSE '4' END;


/* CALCULAMOS EL TOTAL PARA CREDITOS LIQUIDADOS */
-- Totales Comeriales
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 '855002101100',
 '420',
 rep.Moneda, -- Moneda
 rep.TipoDeCartera, -- Tipo de Cartera
 rep.TipoDeSaldo, -- Tipo de saldo
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
WHERE rep.Concepto LIKE '855002%1011' -- creditos comerciales
GROUP BY rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo;


-- Totales Financieras
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 '855002102100',
 '420',
 rep.Moneda, -- Moneda
 rep.TipoDeCartera, -- Tipo de Cartera
 rep.TipoDeSaldo, -- Tipo de saldo
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
WHERE rep.Concepto LIKE '855002%1021' -- entidades financieras
GROUP BY rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo;


-- Totales gubernamentales
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 '855002103100',
 '420',
 rep.Moneda, -- Moneda
 rep.TipoDeCartera, -- Tipo de Cartera
 rep.TipoDeSaldo, -- Tipo de saldo
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
WHERE rep.Concepto LIKE '855002%1031' -- entidades gubernamentales
GROUP BY rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo;


-- Totales consumo
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 '855002200000',
 '420',
 rep.Moneda, -- Moneda
 rep.TipoDeCartera, -- Tipo de Cartera
 rep.TipoDeSaldo, -- Tipo de saldo
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
WHERE rep.Concepto LIKE '855002%2000' --consumo
GROUP BY rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo;


-- Totales hipotecario
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 '855002300000',
 '420',
 rep.Moneda,
 rep.TipoDeCartera,
 rep.TipoDeSaldo,
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
WHERE rep.Concepto LIKE '855002%3000' --hipotecario
GROUP BY rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo;


-- Calculamos los saldos totales
INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 rep.Concepto,
 '420',
 '15', -- Moneda
 '5', -- Tipo de Cartera
 '1', -- Tipo de saldo
 SUM(ISNULL(CAST(rep.Dato AS DECIMAL),0))
FROM dbo.RW_R04A0420_2016 rep
GROUP BY rep.Concepto;


-- CALCULAMOS LAS CUENTAS PADRES
DECLARE @maxLevel INT;

CREATE TABLE #tree (
 Codigo VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
 nivel INT
);

WITH Conceptos (Codigo, LEVEL)
AS (
 SELECT Codigo, 0 AS LEVEL
 FROM dbo.ReportWare_VW_0420Concepto_2016
 WHERE Padre = ''
 UNION ALL
 SELECT vw.Codigo, LEVEL + 1
 FROM dbo.ReportWare_VW_0420Concepto_2016 vw
 INNER JOIN Conceptos con ON vw.Padre = con.Codigo
)
INSERT INTO #tree (Codigo, nivel)
SELECT Codigo, [LEVEL] FROM Conceptos;

SELECT @maxLevel = MAX (nivel) FROM #tree;

WHILE @maxLevel >= 0
BEGIN

 INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
 SELECT rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeCartera, rep.TipoDeSaldo,
 SUM(CAST(ISNULL(rep.Dato,0) AS DECIMAL))
 FROM dbo.ReportWare_VW_0420Concepto_2016 conceptos
 INNER JOIN dbo.RW_R04A0420_2016 rep ON rep.Concepto = conceptos.Codigo
 INNER JOIN #tree con ON rep.Concepto = con.Codigo
 WHERE con.nivel = @maxLevel
 --AND conceptos.Padre NOT IN (SELECT Concepto FROM dbo.RW_R04A0420)
 AND conceptos.Padre <> ''
 --No sumar los siguientes conceptos porque ya se incluyen en conceptos anteriores correspondiente
 AND conceptos.Codigo NOT IN ('855002400000','855002500000','855002600000','855002700000')
 GROUP BY rep.IdReporteLog, conceptos.Padre, rep.SubReporte, rep.Moneda, rep.TipoDeSaldo, rep.TipoDeCartera;

 SET @maxLevel = @maxLevel - 1;
END 

DROP TABLE #tree;


INSERT INTO dbo.RW_R04A0420_2016 (IdReporteLog, Concepto, SubReporte, Moneda, TipoDeCartera, TipoDeSaldo, Dato)
SELECT
 @IdReporteLog,
 sal.Concepto,
 sal.SubReporte,
 sal.Moneda,
 sal.TipoCartera,
 sal.TipoSaldo,
 '0'
FROM dbo.BAJAWARE_R04A_Salida_2016 sal
LEFT OUTER JOIN dbo.RW_R04A0420_2016 r04 ON sal.Concepto = r04.Concepto AND sal.Moneda = r04.Moneda AND sal.TipoCartera = r04.TipoDeCartera AND sal.TipoSaldo = r04.TipoDeSaldo
WHERE sal.Subreporte = '420' AND sal.Visible = 1 AND r04.Concepto IS NULL;

DELETE r04
FROM dbo.RW_R04A0420_2016 r04
INNER JOIN dbo.BAJAWARE_R04A_Salida_2016 sal ON r04.Concepto = sal.Concepto AND r04.Moneda = sal.Moneda AND r04.TipoDeCartera = sal.TipoCartera AND r04.TipoDeSaldo = sal.TipoSaldo
WHERE sal.SubReporte = '420' AND sal.Visible = 0;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04A0420_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SELECT @TotalIntereses = 0;
SELECT @FechaMigracion = MAX( Fecha ) FROM dbo.MIGRACION_ProcesoLog;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = @FechaMigracion,
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
