SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0463_045]
AS
BEGIN
-- Si el RFC (dat_rfc) inicia con CNBX, validar que sí corresponda a personas extranjeras y haya sido registrado en CNBV.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor,
	Nacionalidad,
	RFC
FROM dbo.RW_VW_R04C0463_INC
WHERE ISNULL(RFC,'') LIKE 'CNBX%' AND Nacionalidad = '484';

END

GO
