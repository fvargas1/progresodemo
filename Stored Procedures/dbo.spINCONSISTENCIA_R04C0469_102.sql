SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_102]
AS

BEGIN

-- Validar que la SP Cubierta por Garantias Personales esté entre 0 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SPCubierta
FROM dbo.RW_R04C0469
WHERE CAST(ISNULL(NULLIF(SPCubierta,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END
GO
