SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MIGRACION_Entidad_Update]
	@IdEntidad INT,
	@Nombre VARCHAR(50),
	@Descripcion VARCHAR(100) = NULL,
	@NombreView VARCHAR(50) = NULL
AS
UPDATE MIGRACION_Entidad
SET Nombre = @Nombre,
	Descripcion = @Descripcion,
	NombreView = @NombreView
WHERE IdEntidad= @IdEntidad;
GO
