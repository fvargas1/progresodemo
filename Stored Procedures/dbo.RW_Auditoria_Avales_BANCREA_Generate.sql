SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_Auditoria_Avales_BANCREA_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @TC_UDI DECIMAL(10,6);
DECLARE @FechaPeriodo DATETIME;
DECLARE @SI VARCHAR(15);

SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte='AUDITORIA' AND Nombre = '_Avales';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

SELECT @TC_UDI=tipoC.Valor
FROM dbo.SICC_Periodo periodo
INNER JOIN dbo.SICC_TipoCambio tipoC ON periodo.IdPeriodo = tipoC.IdPeriodo
INNER JOIN dbo.SICC_Moneda moneda ON tipoC.IdMoneda = moneda.IdMoneda
WHERE periodo.Activo=1 AND moneda.Codigo=(SELECT VALUE FROM dbo.BAJAWARE_Config WHERE Codename='MonedaUdis');

SET @SI = '-999';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automaticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


TRUNCATE TABLE dbo.RW_Auditoria_Avales_BANCREA;

INSERT INTO dbo.RW_Auditoria_Avales_BANCREA (
	IdReporteLog, [ID], [ID_Linea], [LC], [S_Total], [IDevNC_B], [Estatus_B6], [IDC_C], [N_A_C],
	[ING_C], [PI_C], [IDC_G], [N_A_G], [OS_AG], [ING_G], [GP_PC], [PI_G], [TA_GTE], [B1]
)
SELECT
	@IdReporteLog,
	credito.CodigoCredito AS ID, -- 01
	credito.NumeroLinea AS ID_Linea, -- 02
	CAST(credito.MontoLineaCredito AS DECIMAL(23,2)) AS LC, -- 03
	CAST (credito.MontoValorizado AS DECIMAL(23,2)) AS S_Total, -- 04
	'0.00' AS IDevNC_B, -- 05
	sitCre.Codigo AS Estatus_B6, -- 06
	persona.Codigo AS IDC_C, -- 07
	REPLACE(pInfo.NombreCNBV,',','') AS N_A_C, -- 08
	CAST(ISNULL(anx.VentNetTotAnuales,0) AS DECIMAL(23,2)) AS ING_C, -- 09
	CAST(ISNULL(ppi.[PI],0) AS DECIMAL(10,6)) AS PI_C, -- 10
	ISNULL(aval.Codigo,'0') AS IDC_G, -- 11
	ISNULL(aval.Nombre,'0') AS N_A_G, -- 12
	CASE WHEN fig.Codigo = '2' THEN '2' ELSE '3' END AS OS_AG, -- 13
	CAST(ISNULL(anxGP.VentNetTotAnuales,0) AS DECIMAL(23,2)) AS ING_G, -- 14
	CAST(ISNULL(ca.Monto,0) AS DECIMAL(23,2)) AS GP_PC, -- 15
	CAST(ISNULL(aval.PIAval,0) AS DECIMAL(10,6)) AS PI_G, -- 16
	ISNULL(anxGP.Anexo,'0') AS TA_GTE, -- 16
	'0' AS B1 -- 17
FROM dbo.SICCMX_Persona persona
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON persona.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC credito ON persona.IdPersona = credito.IdPersona
INNER JOIN dbo.SICCMX_Persona_PI ppi ON persona.IdPersona = ppi.IdPersona
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON credito.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN (
	SELECT IdPersona, VentNetTotAnuales, 0 AS EntFinAcreOtorgantesCre, SinAtrasos FROM dbo.SICCMX_Anexo21
	UNION
	SELECT IdPersona, VentNetTotAnuales, 0, 0 FROM dbo.SICCMX_Anexo22
	UNION
	SELECT IdPersona, ActivoTotal, EntFinAcreOtorgantesCre, 0 FROM dbo.SICCMX_Anexo20
) AS anx ON persona.IdPersona = anx.IdPersona
LEFT OUTER JOIN dbo.SICCMX_CreditoAval ca ON credito.IdCredito = ca.IdCredito
LEFT OUTER JOIN dbo.SICCMX_Aval aval ON ca.IdAval = aval.IdAval
LEFT OUTER JOIN dbo.SICC_FiguraGarantiza fig ON aval.FiguraGarantiza = fig.IdFigura
LEFT OUTER JOIN (
	SELECT IdGP, VentNetTotAnuales, 'Anexo 21' AS Anexo FROM dbo.SICCMX_Anexo21_GP WHERE EsGarante IN (3,4)
	UNION
	SELECT IdGP, VentNetTotAnuales, 'Anexo 22' FROM dbo.SICCMX_Anexo22_GP WHERE EsGarante IN (3,4)
	UNION
	SELECT IdGP, ActivoTotal, 'Anexo 20' FROM dbo.SICCMX_Anexo20_GP WHERE EsGarante IN (3,4)
) AS anxGP ON aval.IdAval = anxGP.IdGP;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_Auditoria_Avales_BANCREA WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
	TotalSaldos = @TotalSaldos,
	TotalIntereses = @TotalIntereses,
	FechaCalculoProcesos = GETDATE(),
	FechaImportacionDatos = GETDATE(),
	IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
