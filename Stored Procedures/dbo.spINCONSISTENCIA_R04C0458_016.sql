SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0458_016]
AS
BEGIN
-- El Nombre del Acreditado (dat_nombre) debe venir en mayúsculas.

SELECT
	CodigoCredito,
	REPLACE(NombrePersona, ',', '') AS NombreDeudor
FROM dbo.RW_VW_R04C0458_INC
WHERE BINARY_CHECKSUM(NombrePersona) <> BINARY_CHECKSUM(UPPER(NombrePersona));

END
GO
