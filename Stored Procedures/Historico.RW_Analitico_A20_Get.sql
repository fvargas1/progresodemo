SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_A20_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Codigo,
	Nombre,
	PonderadoCuantitativo,
	PonderadoCualitativo,
	FactorTotal,
	[PI],
	DiasMoraPromedio_V,
	DiasMoraPromedio_P,
	PrctPagoInstBanc_V,
	PrctPagoInstBanc_P,
	PrctPagoInstNoBanc_V,
	PrctPagoInstNoBanc_P,
	EntFinSujRegBanc_V,
	EntFinSujRegBanc_P,
	PropPasivoLargoPlazo_V,
	PropPasivoLargoPlazo_P,
	ROE_V,
	ROE_P,
	IndCapitalizacion_V,
	IndCapitalizacion_P,
	GastosAdmon_V,
	GastosAdmon_P,
	CartVencCapContable_V,
	CartVencCapContable_P,
	MargenFinAjust_V,
	MargenFinAjust_P,
	EmiTitDeuda_V,
	EmiTitDeuda_P,
	TotalPagosInfonavit_V,
	TotalPagosInfonavit_P,
	DiasAtrInfonavit_V,
	DiasAtrInfonavit_P,
	Solvencia_V,
	Solvencia_P,
	Liquidez_V,
	Liquidez_P,
	Eficiencia_V,
	Eficiencia_P,
	DivLineasNegocio_V,
	DivLineasNegocio_P,
	DivTiposFuentes_V,
	DivTiposFuentes_P,
	ConcentActivos_V,
	ConcentActivos_P,
	IndepConsejoAdmon_V,
	IndepConsejoAdmon_P,
	CompAccionaria_V,
	CompAccionaria_P,
	CalidadGobCorp_V,
	CalidadGobCorp_P,
	AniosExpFunc_V,
	AniosExpFunc_P,
	ExistPolProc_V,
	ExistPolProc_P,
	EdoFinAudit_V,
	EdoFinAudit_P
FROM Historico.RW_Analitico_A20
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
