SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spFILE_Consumo_ModalidadCAT_cat]
AS
DECLARE @Detalle VARCHAR(1000);
DECLARE @Requerido BIT;
SELECT @Detalle=Detalle, @Requerido=ReqCalificacion FROM dbo.MIGRACION_Validacion WHERE Codename='spFILE_Consumo_ModalidadCAT_cat';

IF @Requerido = 1
BEGIN
UPDATE f
SET f.errorCatalogo = 1
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_ModalidadCAT cat ON LTRIM(f.ModalidadCAT) = cat.Codigo
WHERE cat.IdModalidadCAT IS NULL AND LEN(f.ModalidadCAT) > 0;

SET NOCOUNT ON;
END

INSERT INTO dbo.FILE_Consumo_errores (identificador, nombreCampo, valor, tipoError, description)
SELECT
 f.CodigoCredito,
 'ModalidadCAT',
 f.ModalidadCAT,
 2,
 @Detalle
FROM dbo.FILE_Consumo f
LEFT OUTER JOIN dbo.SICC_ModalidadCAT cat ON LTRIM(f.ModalidadCAT) = cat.Codigo
WHERE cat.IdModalidadCAT IS NULL AND LEN(f.ModalidadCAT) > 0;
GO
