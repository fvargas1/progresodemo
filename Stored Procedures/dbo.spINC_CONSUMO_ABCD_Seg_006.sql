SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINC_CONSUMO_ABCD_Seg_006]
AS

BEGIN

-- Si el "Tipo de Garantía" es "40" se debe reportar el "Registro Único de Garantías Mobiliarias"

SELECT DISTINCT
	rep.FolioCredito,
	rep.TipoGarantia,
	rep.RegistroUnicoGarantiasMobiliarias
FROM dbo.RW_Consumo_ABCD rep
WHERE rep.TipoGarantia = '40' AND LEN(ISNULL(rep.RegistroUnicoGarantiasMobiliarias,'')) = 0;

END
GO
