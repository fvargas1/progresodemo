SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0455_015]
AS
BEGIN
-- Si el Saldo de los Ingresos Totales Ajustados (dat_saldo_total_ajustado) es > 4
-- entonces el Puntaje Servicio de Deuda a Ingresos Totales Ajustados (cve_puntaje_serv_deuda_ing) debe ser = 24

SELECT
	CodigoPersona AS CodigoDeudor,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SdoIngTotales,
	P_ServDeudaIngAjust AS Puntos_SdoIngTotales
FROM dbo.RW_VW_R04C0455_INC
WHERE CAST(ISNULL(NULLIF(SdoIngTotales,''),'0') AS DECIMAL(18,6)) > 4 AND ISNULL(P_ServDeudaIngAjust,'') <> '24';

END
GO
