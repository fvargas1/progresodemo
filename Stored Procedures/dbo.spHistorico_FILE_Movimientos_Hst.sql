SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spHistorico_FILE_Movimientos_Hst]
 @IdPeriodoHistorico INT
AS

DELETE FROM Historico.FILE_Movimientos_Hst WHERE IdPeriodoHistorico = @IdPeriodoHistorico;

INSERT INTO Historico.FILE_Movimientos_Hst (
 IdPeriodoHistorico,
 CodigoCredito,
 TipoMovimiento,
 Monto,
 Fecha,
 Fuente
)
SELECT
 @IdPeriodoHistorico,
 CodigoCredito,
 TipoMovimiento,
 Monto,
 Fecha,
 Fuente
FROM dbo.FILE_Movimientos_Hst;
GO
