SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_LineaCredito_Garantia]
	@IdPersona BIGINT
AS
SELECT
 0 AS IdGarantia,
 '' AS CodigoGarantia,
 '' AS Codigo,
 '' AS Garantia,
 '' AS TipoGarantia,
 0 AS ValorGarantia,
 0 AS Porcentaje,
 '' AS Observaciones
FROM dbo.BAJAWARE_Config
WHERE 1=2
/*
SELECT
 gar.IdGarantia AS IdGarantia,
 gar.Codigo AS CodigoGarantia,
 cr.NumeroLinea AS Codigo,
 ISNULL(gar.Descripcion, 'Sin Garantia') AS Garantia,
 ISNULL(tpoNM.Nombre, '') AS TipoGarantia,
 SUM(ISNULL(cg.Monto,0)) AS ValorGarantia,
 SUM(ISNULL(cg.PorcentajeSaldo,0)) AS Porcentaje,
 CASE WHEN SUM(ISNULL(cg.PorcentajeSaldo,0)) < 1 THEN 'Garantía Compartida' ELSE '' END AS Observaciones
FROM dbo.SICCMX_Garantia gar
INNER JOIN dbo.SICCMX_CreditoGarantia cg ON gar.IdGarantia = cg.IdGarantia
INNER JOIN dbo.SICCMX_Credito cr ON cg.IdCredito = cr.IdCredito
LEFT OUTER JOIN dbo.SICC_TipoGarantia tpoGar ON gar.IdTipoGarantia = tpoGar.IdTipoGarantia
LEFT OUTER JOIN dbo.SICC_TipoGarantiaNvaMet tpoNM ON tpoGar.IdClasificacionNvaMet = tpoNM.Codigo
WHERE cr.IdPersona = @IdPersona AND cg.Monto > 0
GROUP BY gar.IdGarantia, gar.Codigo, cr.NumeroLinea, gar.Descripcion, tpoNM.Nombre
ORDER BY cr.NumeroLinea, gar.Codigo
*/
GO
