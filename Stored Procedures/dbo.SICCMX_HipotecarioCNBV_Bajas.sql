SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_HipotecarioCNBV_Bajas]
AS
DECLARE @IdPeriodo BIGINT;
SELECT @IdPeriodo=IdPeriodo FROM dbo.SICC_Periodo WHERE Activo = 1;

-- SE DAN DE BAJA CODIGOS CNBV CON TIPO DE BAJA EN CREDITO
UPDATE cnbv
SET IdPeriodoBaja = @IdPeriodo
FROM dbo.SICCMX_HipotecarioCNBV cnbv
INNER JOIN dbo.SICCMX_Hipotecario hip ON cnbv.Codigo = hip.Codigo
INNER JOIN dbo.SICCMX_HipotecarioInfo info ON hip.IdHipotecario = info.IdHipotecario
WHERE info.IdTipoBaja IS NOT NULL;
GO
