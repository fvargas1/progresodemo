SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0465_113_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Si el Puntaje por Meses desde el último crédito abierto bancarias en los últimos 12 meses (cve_ptaje_meses_ultim_cred) es = 58,
-- entonces el Meses desde el último crédito abierto bancarias en los últimos 12 meses es >= 6

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0465_INC
WHERE ISNULL(P_MesesUltCredAb,'') = '58' AND CAST(MesesUltCredAb AS DECIMAL) < 6 AND CAST(MesesUltCredAb AS DECIMAL) <> -999;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END

GO
