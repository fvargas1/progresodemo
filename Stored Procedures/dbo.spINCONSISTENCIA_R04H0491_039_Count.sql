SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04H0491_039_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- En el "NUMERO DEL AVALUO DEL INMUEBLE" el valor de los dígitos 6 y 7 en conjunto no pueden ser mayor al año reportado en la fecha de otorgamiento del crédito (col. 11).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_R04H0491
WHERE SUBSTRING(NumeroAvaluo,6,2) <> SUBSTRING(FechaOtorgamiento,3,2);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
