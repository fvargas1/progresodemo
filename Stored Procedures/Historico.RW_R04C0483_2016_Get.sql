SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_R04C0483_2016_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	Formulario,
	CodigoPersona,
	RFC,
	NombrePersona,
	TipoCartera,
	ActEconomica,
	GrupoRiesgo,
	Localidad,
	Municipio,
	Estado,
	Nacionalidad,
	IdBuroCredito,
	LEI,
	TipoAltaCredito,
	TipoOperacion,
	DestinoCredito,
	CodigoCredito,
	CodigoCreditoCNBV,
	CodigoGlobalCNBV,
	MonLineaCredValorizado,
	MonLineaCred,
	FecMaxDis,
	FecVenLin,
	Moneda,
	FormaDisposicion,
	RegGarantiaMob,
	IdDeudorRelacionado,
	IdInstitucionOrigen,
	TasaInteres,
	DifTasaRef,
	OperacionDifTasaRef,
	FrecuenciaRevisionTasa,
	PeriodicidadPagosCapital,
	PeriodicidadPagosInteres,
	NumMesesAmortCap,
	NumMesesPagoInt,
	ComisionAperturaTasa,
	ComisionAperturaMonto,
	ComisionDisposicionTasa,
	ComisionDisposicionMonto,
	LocalidadDestinoCredito,
	MunicipioDestinoCredito,
	EstadoDestinoCredito,
	ActividadDestinoCredito
FROM Historico.RW_R04C0483_2016
WHERE IdPeriodoHistorico=@IdPeriodoHistorico;
GO
