SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0443_025]
AS

BEGIN

-- Se deberá anotar el monto pagado efectivamente por el acreditado. En caso de que no se hayan efectuado pagos, se anotará 0.

SELECT CodigoPersona, CodigoCreditoCNBV, CodigoCredito, NumeroDisposicion, MontoPagado
FROM dbo.RW_R04C0443
WHERE CAST(ISNULL(NULLIF(MontoPagado,''),'-1') AS DECIMAL(23,2)) < 0;

END
GO
