SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0478_040_Count]
 @IdInconsistencia BIGINT
AS
BEGIN
-- Si la Tasa de Interés (cve_tasa) es IGUAL a 600 validar que el Ajuste de la tasa (dat_ajuste_tasa_referencia) sea IGUAL 0.

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0478_INC
WHERE ISNULL(TasaInteres,'') = '600' AND CAST(ISNULL(NULLIF(REPLACE(REPLACE(REPLACE(DifTasaRef,'*',''),'-',''),'+',''),''),'-1') AS DECIMAL(14,6)) <> 0
 AND ISNULL(TipoAltaCredito,'') IN ('131','132','133','134','135','136','137','138','139');

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
