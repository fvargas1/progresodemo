SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0474_040]
AS

BEGIN

-- Si el número de días de atraso es mayor a 90, entonces el crédito está vencido.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	SituacionCredito,
	DiasAtraso
FROM dbo.RW_VW_R04C0474_INC
WHERE CAST(ISNULL(NULLIF(DiasAtraso,''),'0') AS INT) > 90 AND ISNULL(SituacionCredito,'') <> '2';

END


GO
