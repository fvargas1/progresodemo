SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Auditoria_Avales_BANCREA_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	[ID],
	[ID_Linea],
	[LC],
	[S_Total],
	[IDevNC_B],
	[Estatus_B6],
	[IDC_C],
	[N_A_C],
	[ING_C],
	[PI_C],
	[IDC_G],
	[N_A_G],
	[OS_AG],
	[ING_G],
	[GP_PC],
	[PI_G],
	[TA_GTE],
	[B1]
FROM Historico.RW_Auditoria_Avales_BANCREA
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY [N_A_C];
GO
