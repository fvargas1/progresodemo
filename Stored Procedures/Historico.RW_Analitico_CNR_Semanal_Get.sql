SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Historico].[RW_Analitico_CNR_Semanal_Get]
	@IdPeriodoHistorico BIGINT
AS
SELECT
	CodigoCredito,
	CodigoDeudor,
	Nombre,
	Periodicidad_Facturacion,
	TipoCredito,
	Metodologia,
	Constante,
	FactorATR,
	ATR,
	FactorMAXATR,
	MAXATR,
	FactorProPago,
	ProPago,
	FactorSDOIMP,
	SDOIMP,
	FactorTipoCredito,
	[PI],
	SP_Cubierta,
	SP_Expuesta,
	ECubierta,
	EExpuesta,
	ETotal,
	MontoGarantiaTotal,
	MontoGarantiaUsado,
	ReservaCubierta,
	ReservaExpuesta,
	ReservaTotal,
	PorcentajeReservaTotal,
	Calificacion
FROM Historico.RW_Analitico_CNR_Semanal
WHERE IdPeriodoHistorico=@IdPeriodoHistorico
ORDER BY Nombre;
GO
