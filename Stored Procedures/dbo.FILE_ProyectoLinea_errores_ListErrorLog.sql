SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FILE_ProyectoLinea_errores_ListErrorLog]
AS
SELECT
f.identificador,
f.nombreCampo,
f.valor,
f.tipoError,
f.description
FROM dbo.FILE_ProyectoLinea_errores f
GO
