SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0442_025]
AS

BEGIN

-- El destino del crédito debe existir en catálogo.

SELECT r.CodigoPersona, r.CodigoCreditoCNBV, r.CodigoCredito, r.DestinoCredito
FROM dbo.RW_R04C0442 r
LEFT OUTER JOIN dbo.SICC_DestinoCreditoMA cat ON ISNULL(r.DestinoCredito,'') = cat.CodigoCNBV
WHERE cat.IdDestinoCredito IS NULL;

END
GO
