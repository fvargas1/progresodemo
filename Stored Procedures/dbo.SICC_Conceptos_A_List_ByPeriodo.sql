SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICC_Conceptos_A_List_ByPeriodo]
	@IdPeriodo INT,
	@SubReporte VARCHAR(3)
AS
SELECT
	cp.IdConceptoPeriodo,
	c.IdConcepto AS IdConcepto,
	c.Concepto + ' - ' + c.Descripcion AS Concepto,
	cp.IdPeriodo,
	cp.Monto
FROM dbo.SICC_Conceptos_A c
INNER JOIN dbo.SICC_Conceptos_Periodo cp ON c.IdConcepto = cp.IdConcepto
WHERE cp.IdPeriodo = @IdPeriodo AND c.SubReporte = @SubReporte AND c.Activo = 1
ORDER BY c.Orden;
GO
