SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_R04C0484_2016_Generate]
AS
DECLARE @IdReporte AS BIGINT;
DECLARE @IdReporteLog AS BIGINT;
DECLARE @TotalRegistros AS INT;
DECLARE @TotalSaldos AS DECIMAL;
DECLARE @TotalIntereses AS DECIMAL;
DECLARE @FechaPeriodo DATETIME;
DECLARE @IdPeriodo BIGINT;
DECLARE @Entidad VARCHAR(50);

SELECT @IdPeriodo=IdPeriodo, @FechaPeriodo=Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;
SELECT @Entidad = Value FROM dbo.BAJAWARE_Config WHERE CodeName = 'CodigoInstitucion';
SELECT @IdReporte = IdReporte FROM dbo.RW_Reporte WHERE GrupoReporte = 'R04' AND Nombre = 'C-0484_2016';

INSERT INTO dbo.RW_ReporteLog (IdReporte, Descripcion, FechaCreacion, UsuarioCreacion, IdFuenteDatos, FechaImportacionDatos, FechaCalculoProcesos)
VALUES (@IdReporte, 'Reporte Generado automáticamente por los sistemas Bajaware', GETDATE(), 'Bajaware', 1, GETDATE(), GETDATE());

SET @IdReporteLog = SCOPE_IDENTITY();


--Eliminar tabla de reporte
TRUNCATE TABLE dbo.RW_R04C0484_2016;

-- Informacion para reporte
INSERT INTO dbo.RW_R04C0484_2016 (
 IdReporteLog, Periodo, Entidad, Formulario, TamanoAcreditado, VentasNetasTotales, NumeroEmpleados, CodigoCreditoCNBV, ClasContable, ConcursoMercantil, FechaDisposicion,
 FechaVencDisposicion, Moneda, NumeroDisposicion, SaldoInicial, TasaInteres, TasaInteresDisp, DifTasaRef, OperacionDifTasaRef, FrecuenciaRevisionTasa, MontoDispuesto,
 MontoPagoExigible, MontoCapitalPagado, MontoInteresPagado, MontoComisionPagado, MontoInteresMoratorio, MontoTotalPagado, MontoCondonacion, MontoQuitas, MontoBonificado,
 MontoDescuentos, MontoAumentosDec, SaldoFinal, SaldoCalculoInteres, DiasCalculoInteres, MontoInteresAplicar, SaldoInsoluto, SituacionCredito, DiasAtraso, FechaUltimoPago,
 MontoBancaDesarrollo, InstitucionFondeo, Etapa, FechaInicioOper, SobreCosto, MontoCubiertoTerceros, MontoExposicion, PrctExposicion, PrctDescubierto, FlujoEfctivo,
 TasaDescuento, Deficit, PrctDescubiertoCorrida, PrctProvisionamiento, GradoRiesgo, ReservasTotales, ReservasMI, SP_MI, EI_MI, PI_MI, Mitigante, GrupoRiesgo,
 FactorConversion, ExpMitigantes, ExpNetaReservas, TablaAdeudo, GradoRiesgoME, EscalaCalificacion, AgenciaCalificadora, Calificacion, PonderadorRiesgo, RequerimientoCapital,
 EnfoqueBasico, SP_ReqCap_MI, PI_ReqCap_MI, EI_ReqCap_MI, Vencimiento, Correlacion, Ponderador_ReqCap_MI, ReqCap_MI
)
SELECT DISTINCT
 @IdReporteLog,
 @IdPeriodo,
 @Entidad,
 '484',
 tam.CodigoCNBV AS TamanoAcreditado,
 vwPry.VentNetTotAnuales AS VentasNetasTotales,
 pInfo.NumeroEmpleados AS NumeroEmpleados,
 cnbv.CNBV AS CodigoCreditoCNBV,
 cc.Codigo AS ClasContable,
 CASE WHEN cInfo.ConcursoMercantil = 1 THEN '1' ELSE '2' END AS ConcursoMercantil,
 CASE WHEN cre.FechaDisposicion IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaDisposicion,102),'.',''),1,6) END AS FechaDisposicion,
 CASE WHEN cre.FechaVencimiento IS NULL THEN '' ELSE SUBSTRING(REPLACE(CONVERT(VARCHAR,cre.FechaVencimiento,102),'.',''),1,6) END AS FechaVencDisposicion,
 mon.CodigoCNBVMoneda AS Moneda,
 cre.CodigoCredito AS NumeroDisposicion,
 cInfo.SaldoInicial AS SaldoInicial,
 cre.TasaBruta AS TasaInteres,
 tasaRef.CodigoCNBVTasaRef AS TasaInteresDisp,
 tasa.AjusteTasa AS DifTasaRef,
 tasa.OperacionDiferencial AS OperacionDifTasaRef,
 tasa.FrecuenciaRevisionTasa AS FrecuenciaRevisionTasa,
 cInfo.MontoDispuesto AS MontoDispuesto,
 cinfo.MontoPagoExigible AS MontoPagoExigible,
 cInfo.MontoPagEfeCap AS MontoCapitalPagado,
 cInfo.MontoPagEfeInt AS MontoInteresPagado,
 cInfo.MontoPagEfeCom AS MontoComisionPagado,
 cInfo.MontoPagEfeIntMor AS MontoInteresMoratorio,
 cInfo.MontoPagosRealizados AS MontoTotalPagado,
 cInfo.MontoCondonacion AS MontoCondonacion,
 cInfo.MontoQuitasCastQue AS MontoQuitas,
 cInfo.MontoBonificacionDesc AS MontoBonificado,
 cInfo.MontoDescuentos AS MontoDescuentos,
 cInfo.MontoOtrosPrincipal AS MontoAumentosDec,
 cInfo.SaldoFinal AS SaldoFinal,
 cInfo.SaldoParaCalculoInteres AS SaldoCalculoInteres,
 cInfo.DiasCalculoInteres AS DiasCalculoInteres,
 cInfo.InteresDelMes AS MontoInteresAplicar,
 cre.MontoValorizado AS SaldoInsoluto,
 sitCre.Codigo AS SituacionCredito,
 cInfo.DiasMorosidad AS DiasAtraso,
 SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(cInfo.FecPagExigibleRealizado,@FechaPeriodo),102),'.',''),1,6) AS FechaUltimoPago,
 cInfo.MontoBancaDesarrollo AS MontoBancaDesarrollo,
 inst.Codigo AS InstitucionFondeo,
 etp.CodigoCNBV AS Etapa,
 SUBSTRING(REPLACE(CONVERT(VARCHAR,ISNULL(vwPry.FechaInicioOper,@FechaPeriodo),102),'.',''),1,6) AS FechaInicioOper,
 vwPry.SobreCosto AS SobreCosto,
 vwPry.MontoCubiertoTerceros AS MontoCubiertoTerceros,
 vwPry.ExposicionSobrecosto AS MontoExposicion,
 vwPry.PorcenajeSobrecosto AS PrctExposicion,
 vwPry.PorcenajeSobrecosto * 100 AS PrctDescubierto,
 CASE WHEN vwPry.VPTotal > 0 THEN vwPry.VPTotal ELSE 0 END AS FlujoEfctivo,
 vwPry.TasaDescuentoVP * 100 AS TasaDescuento,
 CASE WHEN vwPry.VPTotal < 0 THEN vwPry.VPTotal ELSE 0 END AS Deficit,
 vwPry.PorcentajeCorrida * 100 AS PrctDescubiertoCorrida,
 vwPry.PorcentajeFinal * 100 AS PrctProvisionamiento,
 cal.Codigo AS GradoRiesgo,
 ISNULL(vwPry.ReservaConstruccion,0) + ISNULL(vwPry.ReservaOperacion,0) AS ReservasTotales,
 '' AS ReservasMI,
 '' AS SP_MI,
 '' AS EI_MI,
 '' AS PI_MI,
 mit.CodigoCNBV AS Mitigante,
 gpo.CodigoCNBV AS GrupoRiesgo,
 cInfo.FactorConversion AS FactorConversion,
 cInfo.ExposicionAjuMit AS ExpMitigantes,
 cInfo.ExposicionNeta AS ExpNetaReservas,
 rcCal.TablaAdeudoCNBV AS TablaAdeudo,
 rcCal.GradoRiesgoCNBV AS GradoRiesgoME,
 rcCal.EscalaCNBV AS EscalaCalificacion,
 rcCal.AgenciaCNBV AS AgenciaCalificadora,
 rcCal.CodigoCNBV AS Calificacion,
 cInfo.PonderadorRiesgo * 100 AS PonderadorRiesgo,
 cInfo.ReqCapital AS RequerimientoCapital,
 '' AS EnfoqueBasico,
 '' AS SP_ReqCap_MI,
 '' AS PI_ReqCap_MI,
 '' AS EI_ReqCap_MI,
 '' AS Vencimiento,
 '' AS Correlacion,
 '' AS Ponderador_ReqCap_MI,
 '' AS ReqCap_MI
FROM dbo.SICCMX_Persona per
INNER JOIN dbo.SICCMX_PersonaInfo pInfo ON per.IdPersona = pInfo.IdPersona
INNER JOIN dbo.SICCMX_VW_Credito_NMC cre ON pInfo.IdPersona = cre.IdPersona
INNER JOIN dbo.SICCMX_CreditoInfo cInfo ON cre.IdCredito = cInfo.IdCredito
INNER JOIN dbo.SICCMX_Metodologia met ON cre.IdMetodologia = met.IdMetodologia
INNER JOIN dbo.SICCMX_VW_Proyecto_Credito vwPry ON cInfo.IdCredito = vwPry.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_CreditosCNBV cnbv ON cre.IdCredito = cnbv.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Moneda_Credito_Rep mon ON cre.IdCredito = mon.IdCredito
LEFT OUTER JOIN dbo.SICC_SituacionCredito sitCre ON cre.IdSituacionCredito = sitCre.IdSituacionCredito
LEFT OUTER JOIN dbo.SICC_Institucion inst ON cInfo.IdInstitucionFondeo = inst.IdInstitucion
LEFT OUTER JOIN dbo.SICC_ClasificacionContable cc ON cre.IdClasificacionContable = cc.IdClasificacionContable
LEFT OUTER JOIN dbo.SICCMX_VW_TasaRef_Credito_Rep tasaRef ON cre.IdCredito = tasaRef.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_Credito_AjusteTasa tasa ON cre.IdCredito = tasa.IdCredito
LEFT OUTER JOIN dbo.SICC_Mitigante mit ON cInfo.IdMitigante = mit.IdMitigante
LEFT OUTER JOIN dbo.SICC_RCGrupoRiesgo gpo ON pInfo.IdRCGrupoRiesgo = gpo.IdRCGrupoRiesgo
LEFT OUTER JOIN dbo.SICCMX_VW_Credito_RCCalificacion vwCreCal ON cre.IdCredito = vwCreCal.IdCredito
LEFT OUTER JOIN dbo.SICCMX_VW_RCCalificacion rcCal ON vwCreCal.IdRCCalificacion = rcCal.IdRCCalificacion
LEFT OUTER JOIN dbo.SICC_Etapa etp ON vwPry.IdEtapa = etp.IdEtapa
LEFT OUTER JOIN dbo.SICC_CalificacionNvaMet cal ON vwPry.IdCalificacionFinal = cal.IdCalificacion
LEFT OUTER JOIN dbo.SICC_TamanoDeudor tam ON pInfo.IdTamanoDeudor = tam.IdTamanoDeudor
WHERE met.Codigo='4' AND (cre.MontoValorizado > 0 OR cInfo.IdTipoBaja IS NOT NULL OR cInfo.SaldoInicial > 0);

EXEC dbo.SICCMX_Formato_Reportes @IdReporte;


SELECT @TotalRegistros = COUNT( IdReporteLog ) FROM dbo.RW_R04C0484_2016 WHERE IdReporteLog = @IdReporteLog;
SELECT @TotalSaldos = 0;
SET @TotalIntereses = 0;

UPDATE dbo.RW_ReporteLog
SET TotalRegistros = @TotalRegistros,
 TotalSaldos = @TotalSaldos,
 TotalIntereses = @TotalIntereses,
 FechaCalculoProcesos = GETDATE(),
 FechaImportacionDatos = GETDATE(),
 IdFuenteDatos = 1
WHERE IdReporteLog = @IdReporteLog;
GO
