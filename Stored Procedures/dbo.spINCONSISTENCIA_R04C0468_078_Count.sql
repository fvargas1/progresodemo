SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_078_Count]
	@IdInconsistencia BIGINT
AS
BEGIN

-- Validar que la Nacionalidad corresponda a Catalogo CNBV

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(rep.IdReporteLog)
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_Pais pais ON ISNULL(rep.Nacionalidad,'') = pais.CodigoCNBV
WHERE pais.IdPais IS NULL;

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END


GO
