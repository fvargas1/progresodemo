SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SICCMX_TipoAlta_CargaInicial_R]
AS
DECLARE @FechaInicial VARCHAR(50);
DECLARE @FechaPeriodo DATETIME;

SELECT @FechaInicial = [Value] + '-01' FROM dbo.BAJAWARE_Config WHERE CodeName = 'FECHA_INIT_2016';
SELECT @FechaPeriodo = Fecha FROM dbo.SICC_Periodo WHERE Activo = 1;

IF ISDATE(@FechaInicial) = 1 AND (YEAR(@FechaInicial) = YEAR(@FechaPeriodo) AND MONTH(@FechaInicial) = MONTH(@FechaPeriodo))
BEGIN
UPDATE dbo.RW_R04C0453_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0458_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0463_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0468_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0473_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0478_2016 SET TipoAltaCredito = '131';
UPDATE dbo.RW_R04C0483_2016 SET TipoAltaCredito = '131';

UPDATE dbo.RW_R04H0491_2016 SET TipoAlta = '27';
END
GO
