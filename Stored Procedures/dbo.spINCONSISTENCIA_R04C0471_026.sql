SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0471_026]
AS

BEGIN

-- La SP Ajustada Por Derechos De Cobro debe estar entre 1 y 100.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	CodigoPersona,
	CodigoCreditoCNBV,
	SP_DerechosCobro
FROM dbo.RW_VW_R04C0471_INC
WHERE CAST(ISNULL(NULLIF(SP_DerechosCobro,''),'-1') AS DECIMAL(10,6)) NOT BETWEEN 0 AND 100;

END


GO
