SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0468_072]
AS

BEGIN

-- Validar que la Periodicidad Pagos Capital corresponda a Catalogo CNBV

SELECT
	rep.CodigoCredito,
	REPLACE(rep.NombrePersona, ',', '') AS NombreDeudor,
	rep.PeriodicidadPagosCapital
FROM dbo.RW_VW_R04C0468_INC rep
LEFT OUTER JOIN dbo.SICC_PeriodicidadCapital perCap ON ISNULL(rep.PeriodicidadPagosCapital,'') = perCap.CodigoCNBV
WHERE perCap.IdPeriodicidadCapital IS NULL;

END


GO
