SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0483_041_Count]
	@IdInconsistencia BIGINT
AS
BEGIN
-- Validar que un mismo RFC Acreditado (dat_rfc) no tenga más de un Tipo de Cartera (cve_tipo_cartera).

DELETE FROM dbo.MIGRACION_InconsistenciaResumen WHERE IdInconsistencia = @IdInconsistencia;

DECLARE @count INT;

SELECT @count = COUNT(IdReporteLog)
FROM dbo.RW_VW_R04C0483_INC
WHERE TipoCartera IN (
	SELECT rep.TipoCartera
	FROM dbo.RW_VW_R04C0483_INC rep
	INNER JOIN dbo.RW_VW_R04C0483_INC rep2 ON rep.RFC = rep2.RFC AND rep.TipoCartera <> rep2.TipoCartera
	GROUP BY rep.TipoCartera, rep.RFC
);

INSERT INTO dbo.MIGRACION_InconsistenciaResumen (IdInconsistencia, Numero, Fecha) VALUES (@IdInconsistencia, @count, GETDATE());

SELECT @count;

END
GO
