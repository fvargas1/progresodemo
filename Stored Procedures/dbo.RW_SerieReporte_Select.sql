SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RW_SerieReporte_Select]
	@SerieReporte VARCHAR(25)
AS
SELECT
	GrupoReporte,
	CASE WHEN GrupoReporte = 'R04' THEN 'Cartera de Credito'
	WHEN GrupoReporte = 'R04 Sin Validar' THEN 'Cartera de Credito sin validar'
	ELSE 'Reportes Regulatorios' END AS Descripcion,
	COUNT( DISTINCT reporte.IdReporte ) AS TotalReportes
FROM dbo.RW_ReporteLog serie
INNER JOIN dbo.RW_Reporte reporte ON serie.IdReporte = reporte.IdReporte
WHERE UPPER( GrupoReporte ) = UPPER( @SerieReporte )
GROUP BY GrupoReporte;
GO
