SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spINCONSISTENCIA_R04C0469_004]
AS

BEGIN

-- El campo EXPOSIC NO CUBIER GTIA PERSONA debe ser mayor o igual a cero.

SELECT
	CodigoCredito,
	NumeroDisposicion,
	REPLACE(NombrePersona, ',', '' ) AS NombreDeudor,
	EIExpuesta AS EI_NoCubierta
FROM dbo.RW_R04C0469
WHERE CAST(ISNULL(NULLIF(EIExpuesta,''),'-1') AS DECIMAL) < 0;

END
GO
